# Outline Pre Midterm

### Ordinary Differential Equations

#### Second Order Linear

$$
Ay''(x) + By'(x) + Cy(x) = f(x)
$$

#### Euler Type

$a_n x^n y^{(n)} + a_{n-1}x^{n-1} y^{(n-1)} + \cdots + a_0 y(x) = 0$ 

so $y(x) = x^m$ 

### Trigonometry

$$
\cos (a + b) = \cos(a)\cos(b) - \sin(a)\sin (b)
\\ \sin(a+b) = \sin(a)\cos(b) - \sin(b)\cos(a)
\\ \sin(a)\sin(b) = \frac{1}{2}[\cos(a-b)-\cos(a+b)]
\\ \cos(a)\cos(b) = \frac{1}{2}[\cos(a-b) + \cos(a+b)]
$$

$$
\int_0^l \cos(\frac{n \pi x}{l})\, dx = \frac{l}{n \pi} [\sin(n \pi) - \sin(0)] = 0
$$

##### Law of Cosines

If $\theta$ is the angle between $a$ and $b$ and opposite to side $c$ 

$|c|^2 = |a|^2 + |b|^2 - 2|a||b|\cos(\theta)$ 

### Real Analysis Bits

#### Inequalities

##### Schwarz

$$
\sum a_n b_n \leq (\sum a_n)^{1/2}(\sum b_n)^{1/2}
$$

##### Cauchy-Schwarz

$$
|(u, v)| \leq \|u||\|v\|
$$

##### Other 1

$ab \leq \frac{a^2 + b^2}{2}$ , $(a - b)^2 \geq 0$ $\equiv$ $a^2 -2ab + b^2 \geq 0$ 

#### Series

##### Geometric

$\sum_{k = 0}^\infin ar^k = \frac{a}{1-r}$             $|r| <1$ 

$\sum_{n = - N}^{N}z^n = \frac{z^{-N}- z^{N+1}}{1 - z}$ 

Noting: $(1 - x)(1+ x + x^2 + x^3 + \cdots+ x^n) = 1 - x^{n+1}$ 

$(1 - z)(1 + z^{-1} + z^{-2} + \cdots + z^{-n}) = 1 + z^{-1} + \cdots - z - 1 - z^{-1} + c\dots = z^{-n} - z$ 

$\sum_{n = 0}^N z^n = \frac{1 - z^{N+1}}{1 - z}$ $\sum_{n = -N}^{-1}z^n = \sum_{n = 1}^N z^{-n}$ $ = \frac{z^{-N}- z}{1 - z} - 1$ $ = \frac{z^{-N} - 1}{1- z}$ 

$\sum_{n = -N}^N z^n = \frac{z^{-N}- z^{N + 1}}{1 - z}$ 

## Appendices

There's a difference between:

$f(\bold x) \in \R$ and $\bold F(\bold x) \in \R^d$ and $\bold F(x) \in \R^d$ 

$f(\bold x)$ is a function from $\R^d \rightarrow \R$ 
$\bold F(\bold x)$ is a function from $\R^d \rightarrow \R^d$ 

​	$(F^1(\bold x), \ldots, F^d(\bold x))$ 

$\bold F(x)$ is a function from $\R \rightarrow \R^d$ (this is more along the lines of what we saw in Math 123: ODE)

​	$(F^1(x), \ldots, F^d(x))$ 

---

$\Delta f = \text{div } \nabla f = \nabla \cdot \nabla f$

---

### Some Geometry stuff

#### Areas and Volumes

Area of Circle: $\pi r^2$ 

$2 \pi r$: Circumference 

$4 \pi r^2$: surface area of a asphere

$\frac{4}{3}\pi r^3$: Volume of sphere

### Derivative Tests

for one variable: $f'(x) = 0$: 
$$
\text{maximum}: f''(x) \leq 0 
\\\text{minimum}:  f''(x) \geq 0
$$
for two variables:

if $(a, b)$ is a critical point: $f_x(a, b) = f_y(a, b) = 0$ 

we look at the determinant of the HEssian Matrix of $f$:

$f_{xx}f_{yy} - f_{xy}$ at $(a, b)$ which is then $f_{xx}(a, b)f_{yy}(a, b)$ 
For a maximum:  $f_{xx}(a,b)f_{yy}(a, b) \geq 0$ and $f_{xx}(a, b) \leq 0$ as well as $f_{yy}(a, b) \leq 0$ $\implies$ $f_{xx}(a, b) + f_{yy}(a, b) \leq 0$ 

### Limits

One-sided limits are interesting

$-$: limit coming from the left side

$+$: limit coming from the right

---

#### Intermediate and Mean Value Theorems

Require continuity/differentiability

---

### Vanishing Theorems

#### One dimension

Let $f(x)$ be a **continuous** function in a **finite closed interval** $[a,b]$. **Assume** that $f(x) \geq 0$ in the interval and that $\int_a^b f(x)dx = 0$ $\implies$ $f(x)$ is identically zero

#### Multiple Dimensions

##### First Vanishing Theorem 

$f(\vec x)$: continuous function in $\overline D$ where $D$ is a bounded domain

Assume: $f(\vec x) \geq 0$ in $\overline D$ and that $\int \int \int _D f(\vec x) d\vec x = 0$ 

$\implies$ $f(\vec x) \equiv 0$ 

##### Second Vanshing Theorem

$f(\vec x)$: continuous function in $D_0$ 

such that $\int \int \int _D f(\vec x)d\vec x = 0$ FOR ALL subdomains $D \sub D_0$ 

$\implies$ $f(\vec x) \equiv 0$ for all $\vec x\in D_0$ 

---

### Piecewise Continuous Function, Jump Discontinuities

**Jump discontinuity** at $x$: If $f(x-)$ and $f(x+)$ exists, but $f(x-) \neq f(x+)$ 

Piecewise continuous: **has jumps** at a finite number of points, but is otherwise continuous.

**Every piecewise continuous function is integrable** and the Integral of PW continuous functions are continuous

---

### Some Derivative stuff

#### Differentials

$du = \sum_{i} \frac{\partial u}{\partial x_i} dx_{i}$ 

#### Chain Rule

You know this

#### **directional derivative** of $f(\vec x)$ at a point $\vec a$ in the direction of the vector $\vec v$ 

$$
\lim_{t \rightarrow 0} \frac{f(\vec a + t \vec v)- f(\vec a)}{t} = \vec v \cdot \nabla f(\vec a)
$$

*example*: rate of change of quantity $f(\vec x)$ seen by a moving particle $\vec x = \vec x (t)$ 

is $\frac{d}{dt}f(\vec x) = \nabla f \cdot \frac{d\vec x}{dt}$ 

---

#### Jacobian Matrix

$\frac{d(x_1, \ldots, x_d)}{d(x'_1, \ldots, x'_d)} = $ $ \det \begin{pmatrix} \frac{\partial x_1}{\partial x_1'} & \cdots & \frac{\partial x_d}{\partial x_1'} \\ \frac{\partial x_1}{\partial x_2'} & \ddots & \vdots \\ \frac{\partial x_1}{\partial x_d'} & \cdots & \frac{\partial x_d}{\partial x_d'} \end{pmatrix}$ 

##### Curl

$\nabla \times G = 0$ if and only if $G = \nabla f$ for some scalar function $f$ (letting $G = [G^1, G^2, G^3])$ 

$\nabla \times G = \det \begin{pmatrix}  \vec i & \vec j & \vec k \\ \frac{\partial }{\partial x} & \frac{\partial}{\partial y} & \frac{\partial }{\partial z} \\ G^1 & G^2 & G^3 \end{pmatrix} = (G^3_y - G^2_z, G^1_z - G^3_x, G^2_x - G^1_y)$

$G$ is curl free if and only if it is a gradient. 

In two dimensions the same is true if we define $\text{curl }G = \frac{\partial g_2}{\partial x} - \frac{\partial g_1}{\partial y}$ , $G = [g_1, g_2]$ 

---

### Derivative of Integrals

If $I(t) = \int_{a(t)}^{b(t)} f(x, t)\, dx$

$\frac{d}{dt} \int_{a(t)}^{b(t)} f(x, t)\, dx$ $ = \int_{a(t)}^{b(t)} f_t(x, t)\, dx + b'(t)f(x, b(t)) - a'(t)f(x, a(t))$ 

Higher Dimensions: 

$\frac{d}{dt}\int_{D(t)}F(\bold x, t)\, dV = \int_{D(t)}F_t(\bold x, t)dV + \int_{\partial D(t)} F(\bold x, t)\bold v_b \cdot d \bold \Sigma$ 

$\bold v_b$: Eulerian velocity of the boundary

---

### Curves and Surfaces

#### Curve $s$ 

function from $\R \rightarrow \R^3$ (for example, i.e. parametrized curves)

##### Arc length:

$ds/dt = |d\bold x/ dt|$ $ = \sqrt{\frac{dx}{dt}^2 + \frac{dy}{dt}^2 + \frac{dz}{dt}^2}$ 

For  a normal $y(x)$ sort of situation:

$\sqrt{dx^2 + dy^2}$ $ = \sqrt{dx^2(1 + \frac{dy^2}{dx^2})}$ $ = \sqrt{1 + (\frac{dy}{dx})^2} dx$

#### Surface: 

From $ \R^2 \rightarrow \R^3$ 

Meh

---

### Integrals of Derivatives

#### Green's Theorem  (2D Divergence Theorem)

$\iint_D \frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y} \, dx \, dy$ $ = \int_C P \, dx + Q \, dy $ $ = \int_C P x'(t) \, dt + Q y'(t) \, dt$ $ = \int_C (P, Q)\cdot \vec t \, ds$

where $\vec t$ is the unit tangent vector along the curve, and $ds$ is the element of arc length

##### Equivalently 

$$
\iint_D \nabla \cdot \vec f\, dx \, dy = \int_C \vec f \cdot \vec n\, ds
$$



#### Divergence Theorem

$$
\iiint_D \nabla \cdot \vec f \, d \vec x = \iint_{\partial D} \vec f \cdot \vec n \, dS
$$

---

## Chapter 1: Where PDEs Come From

### 1.1 What is a Partial Differential Equation

##### Linearity

$L(au + bv) = a Lu + b L v$ 

allows us to say linear combinations of solutions are solutions. 

##### Integrating can be tricky:

###### Example 1:

$u_{xx} = 0$ $\implies$ $u_x = f(y)$ $\implies$ $u = xf(y) + g(y)$ 

###### Example 2:

$u = -u_{xx}$ Take the solution to the ODE $y(x) = -y''(x)$

which is $y(x) = C_1 \cos x + C_2 \sin x$, but make $C_1, C_2$ solutions of $y$ for 

$u(x, y) = f(y) \cos x + g(y) \sin x$ 

###### Example 3: 

$u_y(x, y) = f(y)$ $\implies$ $u(x, y) = \int f(y)\, dy + g(x)$ $ = F(y) + g(x)$ where $F' = f$ 

### Solving ODEs 

When going through homework, write down any ODEs you had to solve:

### 1.2 First-Order Linear Equation

#### $au_x + bu_y$ 

##### Gemoetric Method

$$
(a, b)\cdot \nabla u = au_x + bu_y = 0
$$

Solution: $f(bx - ay) = f((b, -a)\cdot (x, y)) = f(w \cdot (x, y))$  

$v = (a, b) \qquad w = (b, -a)$ 

##### Coordinate Method - change variables 

Define

$x' = f(x, y) = ax + by = v \cdot x$ 

$y' = g(x, y) = bx - ay = w \cdot x $ 

$u_x = \frac{\partial u}{\partial x'}\frac{\partial x'}{\partial x} + \frac{\partial u}{\partial y'}\frac{\partial y'}{\partial x}$ , and similalrly for $u_y$ 

gets us $au_x + bu_y = a^2 u_{x'} + ab u_{y'} + b(b)u_{x'} + b(-a)u_{y'} = (a^2 + b^2)u_{x'} = 0$

so $u_{x'} = 0$ $\implies$ so $u$ only depends on $y'$, i.e., $u = f(y') = f(bx - ay)$  

##### Variable Coefficients

Good example: $(1, y) \cdot \nabla u = u_x + y u_y = 0$ 

so $\frac{dy}{dx} = y$ $\implies$ $\implies$  $y(x) = Ce^{x} $ 

###### Characteristic Curves

Curves like $y(x)$ on which $u$ for example is constant or there's a special property regarding the function 

So $u(x, Ce^x) = u(0, Ce^0)$ letting $x = 0$ 

so $u(0, ye^{-x})$ 

### 1.3 Flows, Vibrations, and Diffusions

Check homework, it's just types of equations

### 1.4 Initial and Boundary Conditions

#### Boundary Conditions

##### Dirichlet 

$u = f$ on $\partial D$ 

##### Neumann

$\frac{\partial u}{\partial \bold n} = \bold n \cdot \nabla u = f$ On $\partial D$ 

##### Robin**

$u + a(\bold x) \frac{\partial u}{\partial \bold n} = f$ on $\partial D$ 

##### One Dimensional Problems

$$
(D) \; u(0, t) = g(t), \; u(l, t) = h(t) \text{ conditions at each end of string}
\\ \text{ }
\\(N) \; u_x(0, t) = g(t),\; u_x(l, t) = h(t)
\\ \text{ }
\\(R) \text{ my guess } u(0, t) + au_x(0, t) = g(t),\; u(l, t) + au_x(l, t) = h(t)
$$



### 1.5 Well-Posed Problems

1. Existence: THere exists at least one solution $u(x,t) $ satisfying all these conditions
2. Uniqueness: There is at most one solution
3. Stabilitiy: The unique solution $u(x,t)$ depends in a stable manner on the data of the problem. This means that if the data are changed a little, the corresponding solution changes only a little. 

##### Challenging Example: see Lecture 4

### 1.6 Types of Second-Order Equations**

$$
a_{11}u_{xx} + 2a_{12}u_{xy} + a_{22}u_{yy} + a_1 u_x + a_2 u_y + a_0 u = 0
$$

##### Elliptic case:

if $a_{12}^2 < a_{11}a_{22}$ We can reduce the equation to
$$
u_{xx} + u_{yy} + \cdots = 0
$$
(where $\cdots$ denote terms of order $1$ or $0$) 

##### Hyperbolic case:

if $a_{12}^2 > a_{11}a_{22}$ it is reducible
$$
u_{xx} - u_{yy} + \cdots = 0
$$

##### Parabolic case:

If $a_{12}^2 = a_{11}a_{22}$ 
$$
u_{xx}  + \cdots = 0
$$
$a_{11} = a_{12} = a_{22} = 0$ 

Think of it as a "discriminant": $= -\det \begin{pmatrix} a_{11} & a_{12} \\ a_{12} & a_{22} \end{pmatrix}$ $= a_{12}^2 - a_{11}a_{22}$  

#### General Case

$$
\sum_{i, j = 1}^n a_{ij}x_{i x_j} + \sum_{i = 1}^n a_i u_{x_i} + a_0 u = 0
$$

assume $a_{ij} = a_{ji}$ 

Would need to find all the eigenvalues of the matrix 

let $\bold x = (x_1, \ldots, x_n)$ and Consider any linear change of independent variables:

$(\xi_1, \ldots, \xi_n) = \bold \xi = B \bold x$, where $B$ is an $n \times n$ matrix

so $\xi_k = \sum_{m} b_{k m} x_m$ 

Convert to new variables using chain rule:

$\frac{\partial}{\partial x_i} = \sum_{k} \frac{\partial \xi_k}{\partial x_i}\frac{\partial }{\partial \bold \xi_k}$ 
$$
u_{x_i x_j} = (\sum_k b_{ki} \frac{\partial }{\partial \xi_k})(\sum_l b_{lj}\frac{\partial}{\partial \xi_l})u
$$

$$
\sum_{i, j}a_{ij}u_{x_i}u_{x_j} = \sum_{k, l}(\sum_{i, j} b_{ki} a_{ij}b_{lj}) u_{\xi_k \xi_l}
$$

And since $A$ is symmetric, we choose $B$ to be the  orthogonal matrix s.t. $\det B = 1$ and

$BAB^t$ $= D$, a diagonal matrix of the eigenvalues of $A$ 

##### Cases

* Elliptic: all the eigenvalues are all positive or are all negative
  * $A$ or $-A$ is positive definite
* Hyperbolic: none of the $d_1, \ldots, d_n$ vanish and one of them as the opposite sign from the $(n-1)$ others 
* ultrahyperbolic: none vanish, at least $2$ are positive and at least $2$ are negative
* parabolic: exactly one of the eigenvalues is zero and all the others have the same sign. 

## Chapter 2 Waves and Diffusions

### 2.1 The Wave Equation

$$
u_{tt} = c^2 u_{xx}
$$



##### General Solution

* factor: $(\frac{\partial}{\partial t} - c\frac{\partial}{\partial x})(\frac{\partial}{\partial t} + c\frac{\partial}{\partial x })u = 0$  

* System of first order PDEs

  $v_t - cv_x = 0 \\ u_t + cu_x = v$ 

  * Method of Characteristics $v = h((1, c)\cdot (x, t))$ $= h(x + ct)$ 

  * or **Characteristic Coordinates** 

* Ansatz: $u = f(x + ct)$ so that $cf'(x+ct) + cf'(x + ct) = h(x + ct)$ 

  * $f'(x+ct) = \frac{1}{2c}h(x + ct)$ 

* Superposition Principle: 

  * Solution to homogeneous: $u_t + cu_x = 0$ is $g(x - ct)$ 
  * $u(x, t) = f(x + ct) + g(x-ct)$ 

##### Initial Value Problem: Unique Solution

Problem: Memorizing the Equation
$$
u(x, t) = \frac{1}{2}[\phi(x + ct) + \phi(x-ct)]+ \frac{1}{2c}\int_{x-ct}^{x+ct} \psi(s)\, ds
$$

##### Some troublesome Examples that appear in homework as well

### 2.2 Causality and Energy

#### Causality

* the effect of an initial position $\phi(x)$: pair of waves traveling in either direction at speed $c$ and at half the original amplitude
* The effect of an initial velocity $\psi$: wave spreading out at speed $\leq c$ in both directions, so part of the wave may lag behind
* No part goes faster than speed $c$ 

##### Domain of Dependence

![image-20190504203547620](Math 126 Troublesome.assets/image-20190504203547620.png)

##### Domain of Influence

![image-20190504203603578](Math 126 Troublesome.assets/image-20190504203603578.png)

#### Energy

##### Conservation of Energy

$u_{tt} = \frac{T}{\rho}u_{xx}$ 

To show, used formulas for $KE$, $PE$

$KE = \frac{1}{2}mv^2 = \frac{1}{2}\rho \int_{-\infin}^\infin u_t^2 \, dx$ ,  $PE = \frac{1}{2}T \int u_x^2 \, dx$ 

* used derivative of an integral

  $\frac{\partial KE}{\partial t} = \int u_tu_{tt}$ , etcetera. 

* substituting the PDE

* Integration by parts

  * vanishing at endpoints

* trying to find how something is a derivative of something

  * e.g. $u_{tx}u_x = (\frac{1}{2}u_x^2)_t$ in general: for a function $f$, $ff_x$ $= (\frac{1}{2}f^2)_x$ 

#### Side Note: Heat

$H = \iiint_D ku \, d\bold x$ , so $\frac{dH}{dt} = \iiint_D k u_t \, d \bold x$ $ = \iiint_D k(\nabla \cdot \nabla u)\, d\bold x$ $ = k \iint_{\partial D}  \frac{\partial u}{\partial n} \, dS$  idk about the placement of $k$ though

### 2.3 Diffusion Equation

$$
u_t = ku_{xx}
$$

#### Maximum Principle

##### Proof

Defining $v(x, t) : = u(x, t) + \epsilon x^2$, $\epsilon > 0$ 

using the Diffusion equation to see what $v$ satisfies

Go through the cases of where $v(x, t)$ attains its max value

* interior
* top edge

#### Uniqueness

##### Use Maximum Principle

##### Energy Method

Still taking $w = u_1 - u_2$ … $u_i $ solution to the diffusion equation 

$\implies$ $w_t - kw_{xx} = 0$ 	$w(0, t) = w(l, t) = 0$ 
$w w_t - kww_{xx} = 0$  
$(\frac{1}{2}w^2)_t - k(ww_{x})_x + kw_x^2 = 0$ 
$\int_0^l (\frac{1}{2}w^2 )_t \, dx - \int_0^l (ww_x)_x \, dx + \int_0^l kw_x^2 \, dx = 0$ and since $ww_x(l) = ww_x = 0$ so

$\frac{\partial}{\partial t}\int_0^l \frac{1}{2}w^2 \, dx = - \int_0^l kw_x^2 \leq 0$ 

$\implies$ $\int_0^l w^2(x, t) \leq \int_0^l w^2(x, 0) \, dx$ for $t \geq 0$ 

#### Stability

A little hard to remember: 

$u_i$, $i = 1, 2$ solves $\begin{cases} u_t - ku_{xx} = 0 \\ u_i(x, 0) = \phi_i(x) \\ u(0, t) = g(t) \\ u(l, t) = h(t) \end{cases}$  

Given $f, g, h, \phi_1, \phi_2$ and suppose $u_1, u_2$ solve $(4)$ with $\phi = \phi_1$ or $\phi = \phi_2$ respectively. (we are essentialy dealing with $(5)$, but the initial conditoin is different: instead of $0$ it is $\phi_1(x) - \phi_2(x)$). Then
$$
\begin{align}
&(8)\qquad\int_0^l [u_1(x, t) - u_2(x,t)]^2\; dx \leq \int_0^l [\phi_1(x)-\phi_2(x)]^2 \; dx \qquad \forall \; t \geq 0
\\
&(9) \qquad \max_{0 \leq x \leq l} |u_1(x,t)- u_2(x,t)| \leq \max_{0\leq x \leq l}|\phi_1(x) - \phi_2(x)|
\end{align}
$$
Here we have that **the proof depends on some other proof from earlier** 
and the **Maximum Principle** on $w = u_1 - u_2$ 

### 2.4 Diffusion on the Whole Line

#### Invariance Properties

Translations, Derivatives, Linear combinations, Integrals, and Dilations are solutions

Integrals meaning: if $S(x, t)$ is a solution, then $\int S(x-y, t)g(y)\, dy$ is a solution

#### Formula

Sometimes we need to take advantage of components of the derivation of the formula...

like the initial conditions of $Q$ or $S$ 

Also: 

$\int_0^{-\infin} e^{-p^2} \, dp = - \frac{\sqrt{\pi}}{2}$ , $\int_{-\infin}^0 e^{-p^2}\, dp = \int_0^{\infin} e^{-p^2} dp$ $ = \frac{\sqrt{\pi}}{2}$ 

##### Solution for Diffusion Equation

$$
Q(x, t) = \frac{1}{2} + \frac{1}{\sqrt{\pi}}\int_0^{x/\sqrt{4 k t}} e^{-p^2}\, dp
\\S(x, t) = \frac{1}{\sqrt{4 \pi k t}}e^{-x^2/4kt}
$$

$$
u(x, t) = \frac{1}{\sqrt{4 \pi k t}}\int_{-\infin}^\infin e^{-(x - y)^2/4kt}\phi(y)\, dy
\\ = \int_{-\infin}^\infin S(x-y, t)\phi(y)\, dy
$$



### Error Function

$$
\frac{2}{\sqrt{\pi}}\int_0^x e^{-p^2}\, dp
$$

### 2.5 Comparison of Wave and Diffusions

#### Properties: 

| Property                               | Waves $u_{tt}-c^2u_{xx} = 0$                                 | Diffusion $u_t - ku_{xx} = 0$                                |
| -------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| (i) Speed of propogation?              | Finite ($\leq c$) [^1]                                       | Infinite($+\infin$)[^2]                                      |
| (ii) Singularities for $t > 0$?        | Transported along characteristic curves, (speed = $c$)       | Lost immediately e.g. $(Q(x,t))$ ("smooths out")             |
| (iii) Well-posed for $t> 0$?           | Yes                                                          | Yes (for bounded solution)                                   |
| (iv) Well-posed for $t< 0$?            | Yes                                                          | No                                                           |
| (v) Maximum principle                  | No                                                           | Yes                                                          |
| (vi) Behavior $t \rightarrow +\infin$? | Energy is constant/ preserved, so does not decay... Best we can hope for is that $u$ looks like a single wave moving w/ constant speed as $t \rightarrow +\infin$ | Decays to zero (if $\phi$ integrable) or convergence to steady state on boundary integral with boundary conditions. expect convergence to steady state |
| (vii) Information                      | Transported                                                  | Lost Gradually                                               |

## Chapter 3: Reflections & Sources

See Homework for what's hard, maybe write down the general method

### 3.1 Diffusion on the Half-Line

#### Diffusion on Half-Line

$$
\begin{align}
v_t - kv_{xx} &= 0 & \text{in } \{0 < x < \infin, 0 < t <\infin\}
\\ v(x, 0) &= \phi(x) \qquad  & t = 0
\\ v(0, t) &= 0 & x = 0
\end{align}
$$

##### Method of Odd Reflection

Define $\phi_{odd}(-x) = - \phi_{odd}(x)$ 
$$
\phi_{odd} (x) := \begin{cases} \phi(x) & x > 0 \\ - \phi(-x) & x < 0 \\ 0 & x = 0 \end{cases}
$$
Now we have a problem we know
$$
\begin{align}
u_t - ku_{xx} &= 0 & - \infin < x < \infin , t > 0
\\ u(x, 0) &= \phi_{odd}(x) & t =0 
\end{align}
$$
Solution: $u(x, t) = \int_{-\infin}^\infin S(x-y, t)\phi_{odd}(y)\, dy$ 

$u(x, t)$ is odd (see $u(-x, t)$  and check out $u(x, t) + u(-x, t)$ )

$u(x, 0) + u(-x, t) = \phi_{odd}(x) + \phi_{odd}(-x)$ $ = 0$ 
by uniqueness… etc. 

##### Method of Even Reflection

Neumann Condition
$$
\begin{align}
w_t(x, t) &= kw_{xx}(x, t) & 0 < x < \infin, t > 0 
\\ w(x, 0) &= \phi(x)
\\ w_x(x, t) &= 0

\end{align}
$$
$\phi_{even}(x) = \begin{cases} \phi(x) & x \geq 0 \\  \phi(-x) & x \leq 0 \end{cases}$ 

$w(x, t) = \int_0^\infin [S(x-y, t) + S(x+y, t)]\phi(y)\, dy$ 

### 3.2 Reflections of Waves

$$
\begin{align}
v_{tt}- c^2 v_{xx} &= 0 & 0 < x < \infin, -\infin < t < \infin 
\\ v(x, 0) & = \phi(x)
\\ v_t(x, 0) &= \psi(x)
\\ v(0, t) &= 0
\end{align}
$$

Make $\phi_{odd}, \psi_{odd}$ 

if $0 <x < c|t|$,  so for $t > 0$: $0 < x < ct$ and for $t < 0$: $0 <x <c(-t)$ 

$v(x, t) = \frac{1}{2}(\phi(x+ct) - \phi(ct - x)) + \frac{1}{2c}\int_{ct - x}^{ct+x}\psi(s)\, ds$ 

if $0 < c|t| < x$ 

$v(x, t) = \frac{1}{2}(\phi(x + ct) + \phi(x - ct))  + \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(s)\, ds$ 

Sign Change: if $x - ct < 0$:  solution now depends only on values of $\phi$ at $(ct \pm x)$ and on the values of $\psi$ in the interval between these points

![image-20190504222817502](Math 126 Troublesome.assets/image-20190504222817502.png)

#### Finite Interval

$$
\phi_{ext} (-x) = - \phi_{ext}(x) \qquad \phi_{ext}(2l-x) = - \phi_{ext}(x)
$$

$$
\phi_{ext}(x) \begin{cases} \phi(x) & 0 < x < l \\ - \phi(-x) & - l <x < 0 \\ \phi_{ext}(x) = \phi(x - 2lm) & -l + 2lm < x < l + 2lm \end{cases}
$$

$l(2m-1) < x < l(2m+1)$ 

$l(2m - 1) + 2l = l(2m - 1 + 2) = l(2m + 1)$ so this checks out 

### 3.3 Diffusion with a Source

$$
\begin{align}
u_t - ku_{xx} &= f(x, t) & - \infin < x < \infin, 0 < t < \infin
\\ u(x, 0) &= \phi(x)
\end{align}
$$

$f(x, t), \phi$ given

Solution: $u(x, t) = \int_{-\infin}^\infin S(x-y, t) \phi(y)\, dy + \int_0^t \int_{-\infin}^\infin S(x-y, t-s)f(y, s)\, dy \, ds$ 

Source Operator: $\int_{-\infin}^\infin S(x-y, t) \phi(y) \, dy$ 

$ = (\mathscr{S}(t)\phi)(x)$ 
$= \int_{-\infin}^\infin S(x-y, t)\phi(y)\, dy = \phi(y)$ $@ t = 0$ 

#### Reflection

extending $f(x, t)$ to be even or odd depending on BCs

For Example: Homogeneous Extension:

$\int_0^\infin [S(x-y, t)\pm S(x+y, t)\phi(y)]\, dy + \int_0^t \int_{-\infin}^{\infin}S(x-y, t-s) f_{ext}(y, s)\, dy \, ds$ 

#### Boundary Source Half-Line:

$$
\begin{align}
v_t -k v_{xx} &= f(x, t) & 0 < x < \infin, 0 < t <\infin 
\\  v(0, t) \text{ or }& v_x(0, t) = h(t)
\\ v(x, 0) &= \phi(x)
\end{align}
$$

##### Subtraction Method

$$
V = v(x, t) - h(t)
\\
\text{or}
\\ V(x, t) = v(x, t) - xh(t)
$$

$V_t - k V_{xx} = f(x, t) - xh'(t)$ 

Then followed by reflection method. 

### 3.4 Waves with a Source

$$
u_{tt} - c^2u_{xx} = f(x, t)
\\ u(x, 0) = \phi(x)
\\ u_t(x, 0) = \psi(x)
$$

##### Unique Solution: Theorem 1

$$
u(x, t) = \frac{1}{2} [\phi(x+ct) + \phi(x-ct)]+ \frac{1}{2c}\int_{x-ct}^{x+ct} \psi(s)\, ds + \frac{1}{2c}\int_0^t \int_{(x-ct)+cs}^{(x+ct)-cs} f(y, s)\, dy ds
$$

![image-20190504230757301](Math 126 Troublesome.assets/image-20190504230757301.png)

#### Theorem 1: Well-Posed

##### Uniform Norms:

$$
\| w\| : =  \max_{-\infin < x <\infin} | w(x)|
\\ \|w\|_T = \max_{-\infin < x <\infin, 0 \leq t \leq T} |w(x, t)| \qquad 0 <T<\infin
$$

SO:
$$
\| u_1 - u_2 \| _T \leq \| \phi_1 - \phi_2 \| + T \| \psi_1 - \psi_2 \| + \frac{T^2}{2}\| f_1 - f_2 \| 
$$

#### Derivation of Equation 

##### Method of Characteristic Coordinates

Each line is characterized by the $\xi = x + ct$ and $\eta = x - ct$ (since $x$ intercept uniquely defines each line)

$Lu \equiv u_{tt} - c^2 u_{xx} = - 4c^2 u_{\xi \eta} = f(x, t) = f(\frac{\xi + \eta}{2}, \frac{\xi - \eta}{2c})$ 

Integrate: $u = - \frac{1}{4c^2}\int^{\xi} \int^{\eta}f\, d \eta \, d \xi$  

![image-20190504232605962](Math 126 Troublesome.assets/image-20190504232605962.png)

![image-20190504232643705](Math 126 Troublesome.assets/image-20190504232643705.png)

etc. 

##### Method Using Green's Theorem

Lines $L_0, L_1, L_2$, Use Green's Theorem with $\iint_{\Delta} u_{tt} - c^2 u_{xx} \, dx \,dt$ and use differentials

$P = -c^2u_x$, $Q = -u_t$ $\implies$ $\int_C -c^2 u_x \, dt - u_t \, dx$ 

Each line is $x + ct = x + ct_0$ and $x - ct = x_0 - ct_0$ , keep track of what $dt$ is on each line or $d x \pm c dt = 0$ $\implies$ $dx = \mp cdt$  

$-c^2 u _x dt - u_t \, dx =  \pm cu_x dx \pm u_t \, dt = \pm c(du)$ 

Then use the Fundamental Theorem of Line Integrals: $\int_a^b dh = h(b)- h(a)$ 

##### Operator Method

Get analgous solution formula for ODE

$u(t) = S'(t) \phi + S(t)\psi + \int_0^t S(t-s)f(s)\, ds$ 

Let $\mathscr{S}$ repalce $S$ for our purposes, noting $S(t)\psi$ is the solution when $\phi = f = 0$ 

so for our equation: $\mathscr{S}(t)\psi = \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(y)\, dy$ 

Matching with the analogous formula: $(\partial/\partial t)(\mathscr{S}(t))\phi$ 

#### Source on a half-line

Tedious, Sum of four terms
$$
v_{tt}-c^2 v_{xx} = f(x, t) \qquad 0 < x <\infin 
\\ v(x, 0) = \phi(x) \qquad v_t(x, 0) = \psi(x)
\\ v(0, t) = h(t)
$$

* $0 < x <ct$ 

$$
v(x, t) = \phi \text{ term } + \psi \text{ term} + h(t - \frac{x}{c})+ \frac{1}{2c}\iint_D f
$$

* x > ct> 0 

  The normal way?

### 3.5 Diffusion Revisited**

$$
(1) \qquad u(x, t) = \int_{-\infin}^\infin S(x-y, t)\phi(y)\, dy = \int_{-\infin}^\infin S(z, t)\phi(x- z)\, dz
\\ = \frac{1}{\sqrt{4 \pi k t}}\int_{-\infin}^\infin e^{z^2/4kt}\phi(x-z)\, dz
$$

$p = \frac{z}{\sqrt{kt}}$ $\implies$ 
$$
\frac{1}{\sqrt{4 \pi }} \int_{-\infin}^\infin e^{-p^2/4}\phi(x - p \sqrt{kt})\, dp
$$

#### Theorem: 

If $\phi(x)$ is a bounded continuous function for $- \infin <x < \infin$ $\implies$

$(1)$ defines an infinitely differentiable function $u(x,t)$ for $- \infin <x < \infin$, $0 < t <\infin$, which satisfies $u_t - ku_{xx}$ and $\lim_{t \downarrow 0} u(x, t) = \phi(x)$ for all $x$ 

Noting:
$$
\int_{-\infin}^\infin S = 1
$$
all moments of $e^{-p^2/4}$ are finite, Note if a sequence $\rightarrow a$, "tails" of subsequences $\rightarrow 0$ 

##### Corollary

Solution has all derivatives of all orders for $ t>0 $ even if $\phi$ not differentiable, all solutions become smooth as diffusion takes effect, no singulariates 

#### Theorem:

$\phi(x)$: bounded function piecewise continuous $(1)$ is infinitely differentiable for $t > 0$ and $\lim_{t \downarrow 0}u(x, t) = \frac{1}{2}[\phi(x+) + \phi(x-)]$ for all $x$ at points of continuity this $= \phi(x)$ 

## Chapter 4: Boundary Problems

### 4.1 Separation of Variables, the Dirichlet Condition

#### Wave Equation

$$
\begin{align}
&u_{tt} - c^2u_{xx} = 0 & 0 <x < l
\\ &u(0, t)  = u(l,t) = 0 
\\ &u(x, 0) = \phi(x)  & u_t(x, 0) = \psi(x)
\end{align}
$$

##### Ansatz: $u$ is separated

$u = X(x)T(t)$

* Plug Into Wave Equation
* Look for as many separated solutions as possible
* Beware of Eigenvalues: use BCs and Initial Conditions

Important: $X'' + \beta^2 X = 0$, where our eigenvalue $\lambda = \beta^2$ or sometimes $\lambda = - \beta^2$ 

Try to find what eigenvalues exist

For the wave equation: $\lambda_n = (\frac{n\pi}{l})^2$ $X_n(x) = \sin (\frac{n \pi x}{l})$ 

Sum: $u(x, t) = \sum_{n = 1}^\infin (A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l})\sin (\frac{n \pi x}{l})$ 
$$
\begin{align}
\phi(x) &= \sum_{n=1}^\infin A_n  \sin \frac{n \pi x}{l}
\\ \psi(x) &= \sum_{n = 1}^\infin \frac{n \pi c}{l}B_n \sin \frac{n \pi x}{l}
\end{align}
$$

* Practically any function $\phi(x)$ can be exapnded in an infinite series
* $n \pi c/l$: are frequences $c = \sqrt{\frac{T}{\rho}}$ 
* Fundamental Note: smallest frequency: $n = 1$, 1st overtone: $n = 2$ 

#### Diffusion Equation

$$
\begin{align}
u_t - k u_{xx} = 0 && 0 <x < l
\\ u(0, t) = u(l, t)= 0  
\\ u(x, 0) = \phi(x)
\end{align}
$$

Again: the ODE: $X'' + \beta^2 X = 0$ 

$X_n(x) = \sin \frac{n \pi x}{l}$ 
$T' + k\beta^2T = 0$ $\implies$ $T(t) = Ce^{- k \beta^2 t} = Ce^{-kt n^2 \pi^2 /l^2}$ 

$u(x, t) = \sum_{n} A_n e^{-(n\pi/l)^2 kt} \sin \frac{n \pi x}{l}$

$\phi(x) = \sum_{n}A_n \sin \frac{n \pi x}{l}$ 

### 4.2 The Neumann Condition

$$
-X'' = \lambda X \qquad X'(0) = X'(l) = 0
$$

Eigenvalues: $(\frac{n \pi}{l})^2$ , Eigenfunction: $X_n = \cos(\frac{n \pi}{l}x)$ , $n = 0, 1, 2, \ldots$ 

##### Diffusion:

$$
u(x, t) = \frac{1}{2}A_0 + \sum_{n = 1}^\infin A_n e^{-(n\pi/l)^2 kt}\cos \frac{n \pi x}{l}
$$

Behavior of $u(x, t)$ as $t \rightarrow + \infin$: Decays to $\frac{1}{2}A_0$ (reaches steady-state)

##### Wave Equation:

$$
- X'' = \lambda X = 0
\\ T'' = \lambda c^2 T = 0
$$

with Neumann BC
$$
\lambda = 0 \implies X_0(x) = \text{constant} \qquad T_0(t) = A + Bt
$$
$u(x, t) = \frac{1}{2}A_0 + \frac{1}{2}B_0 t + \sum_{n = 1}^\infin (A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l})\cos \frac{n \pi x}{l}$ 

$\phi(x) = \frac{1}{2}A_0 + \sum_{n = 1}^\infin A_n \cos \frac{n \pi x}{l}$ 
$\psi(x) = \frac{1}{2}B_0 + \sum_{n = 1}^\infin \frac{n \pi c}{l}B_n \cos \frac{n \pi x}{l}$ 

## Chapter 5: The Fourier Series

### 5.1 The Coefficients

Pretty easy stuff
$$
\begin{align}
\int_0^l \sin \frac{n \pi x}{l} \sin \frac{m \pi x}{l}\, dx = 0 && m \neq n
\\ \int_0^l \sin^2 \frac{n \pi x}{l}\, dx = \frac{l}{2}
\\ 
\end{align} 
$$

#### Fourier Sine // Dirichlet Boundary Conditions

##### Diffusion

$$
A_n = \frac{2}{l} \int_0^l \phi(x) \sin \frac{n \pi x}{l}\, dx
$$

##### Wave

$$
A_n = \frac{2}{l} \int_0^l \phi(x) \sin \frac{n \pi x}{l}\, dx
\\ \frac{n \pi c}{l}B_n = \frac{2}{l}\int_0^l \psi(x) \sin \frac{n \pi x}{l}\, dx
$$

#### Fourier Cosine Series // Neumann Boundary Conditions

For all nonnegative $m$: 
$$
A_n = \frac{2}{l}\int_0^l \phi(x) \cos \frac{n \pi x}{l}\, dx
$$
My own work: 
$$
\psi(x) = \frac{1}{2}B_0 + \sum_{n=1}^\infin \frac{n \pi c}{l}B_n \cos \frac{n \pi x}{l}
$$

$$
\int_0^l\psi(x)\, dx = \frac{1}{2}B_0 \int_0^l 1^2 \, dx = \frac{l}{2}B_0
\\ B_0 = \frac{2}{l}\int_0^l \psi(x)\, dx
$$

$$
\int_0^l \psi(x) \cos \frac{n \pi x}{l}\, dx = \frac{n \pi c}{l} B_n\int_0^l \cos^2 \frac{n \pi x}{l}\, dx = \frac{n \pi c}{l}B_n (\frac{l}{2}) \implies
\\ \frac{n \pi c}{l} B_n = \frac{2}{l}\int_0^l \psi(x)\cos \frac{n \pi x}{l}\, dx
$$

#### Full Fourier Series, on interval $(-l, l)$ 

$$
\phi(x) = \frac{1}{2}A_0 + \sum_{n = 1}^\infin (A_n \cos \frac{n \pi x}{l} + B_n \sin \frac{n \pi x}{l})
$$

Eigenfunctions: $\{1, \cos(n \pi x/l), \sin(n \pi x/l) \}$, for $n = 1, 2, 3, \ldots$ 

##### Important Table of Integrals of trig functions

$$
\begin{align}
\int_{-l}^l \cos \frac{n\pi x}{l} \sin \frac{m \pi x}{l} \, dx &= 0 & \forall m, n 
\\ \int_{-l}^l \cos \frac{n \pi x}{l}\cos \frac{m \pi x}{l}\, dx &= 0  & \forall n \neq m
\\ \int_{-l}^l \sin \frac{n \pi x}{l} \sin \frac{m \pi x}{l} \, dx &= 0 & \forall n \neq m
\\ \int_{-l}^l 1 \cdot \cos \frac{n \pi x}{l}\, dx = 0 &= \int_{-l}^l 1\cdot \sin \frac{m \pi x}{l}\, dx
\end{align}
$$



##### Coefficient Formulas

$$
\begin{align}
A_n &= \frac{1}{l}\int_{-l}^l \phi(x) \cos \frac{n \pi x}{l} \, dx  & (n = 0, 1, 2,\ldots)
\\ B_n &= \frac{1}{l}\int_{-l}^l \phi(x)\sin \frac{n \pi x}{l}\, dx & (n = 1, 2, 3, \cdots)
\end{align}
$$

##### Careful:

Series converges for $x \in (0, l)$ or $x \in (-l, l)$ but not necessarily at endpoints

### 5.2 Even, Odd, Periodic, and Complex Functions

#### Definitions

For $x \in (-l, l)$ 

##### Even Function

$\phi(-x) = \phi(x)$ 

##### Odd Function

$\phi(-x) = - \phi(x)$ 

##### Periodic Function

$-\infin <x <\infin$ 

$\exists p > 0$: $\phi(x + p) = \phi(x)$ for all $x$ 

###### Extension

for $x \in (-l, l)$ 

$\phi_{per} = \phi(x - 2lm)$ for $-l + 2lm < x < l + 2lm$ 

#### Properties 

| Function                                   | Even/Odd                                                     |
| ------------------------------------------ | ------------------------------------------------------------ |
| $x^n$                                      | $\begin{cases} \text{even} & n \text{ even} \\ \text{odd} & n \text{ odd} \end{cases}$ |
| $\cos x, \cosh x, $ and functions of $x^2$ | even                                                         |
| $\sin x, \tan x, \sinh x$                  | odd                                                          |
| $\text{even} \times \text{even}$           | even                                                         |
| $\text{even} \times \text{odd}$            | odd                                                          |
| $\text{odd} \times \text{odd}$             | even                                                         |
| $\text{odd} + \text{odd}$                  | odd                                                          |
| $\text{even} + \text{even}$                | even                                                         |
| $\text{even} + \text{odd}$                 | inconclusive                                                 |
| $\text{per} + \text{per} $                 | periodic                                                     |
| $\text{per} \times \text{per}$             | periodic                                                     |

##### Relationship between BCs and even/odd/periodic

- $u(0,t) = u(l,t) = 0$: Dirichlet BCs correspond to the **odd** extension
- $u_x(0,t) = u_x(l,t) = 0$: Neumann BCs correspond to the **even** extension
- $u(l,t) = u(-l,t), u_x(l,t) = u_x(-l,t)$: Periodic BCs correspond to the **periodic** extension

#### Complex Form of the Full Fourier Series

##### DeMoivre Formulas

$$
\sin \theta = \frac{e^{i\theta} - e^{-i\theta}}{2i} \qquad \cos \theta = \frac{e^{i\theta} + e^{-i\theta}}{2}
$$

$\implies$ $\{e^{i n \pi x/l}\}$ is our set of eigenfunctions for **any** integer $n$ 
$$
\phi(x) = \sum_{n = -\infin}^\infin c_n e^{in\pi x/l}
$$
With
$$
\int_{-l}^l e^{in \pi x/l}e^{-im \pi x/l}\, dx = 0 \qquad n \neq m
$$

$$
\int_{-l}^l e^{-i(n-n)\pi x/l} \, dx = 2l
$$

So: 
$$
c_n = \frac{1}{2l} \int_{-l}^l\phi(x)e^{-in \pi x/l}\, dx
$$

### 5.3 Orthogonality and General Fourier Series

#### Orthogonality

$$
(f, g) := \int_a^b f(x)g(x)\, dx
\\ (f, f) = \int_a^b |f(x)|^2 \, dx
$$

$f$ and $g$ are orthogonal if $(f, g) = 0$ 

Let $X_1(x), X_2(x)$ be two different eigenfunctions with same BCs
$$
\lambda_1 \neq \lambda_2\qquad \begin{cases} -X_1'' = \lambda_1 X_1 \\ -X_2'' = \lambda_2 X_2 \end{cases} + BCs
$$

$$
(\lambda_1 - \lambda_2)\int_a^b X_1X_2 \, dx = \int_a^b (-X_1''X_2 + X_1 X_2'')\, dx  
\\ = (-X_1'X_2 + X_1X_2')|_a^b
$$

and depending on the Boundary Conditions may equal $0$

For our traditional BCs, they equal $0$ 

So for our Previous Cases, the Eigenfunctions are all Orthogonal

#### Symmetric Boundary Conditions

Pair of Boundary Conditions:
$$
\begin{cases} \alpha_1 X(a) + \beta_1 X(b) + \gamma_1 X'(a) + \delta_1 X'(b) = 0
\\ \alpha_2 X(a) + \beta_2 X(b) + \gamma_2 X'(a) + \delta_2 X'(b) = 0
\end{cases}
$$
is called **symmetric** (Hermitian) if $\forall f, g $ that satisfies the boundary conditions
$$
[f'(x)g(x)-f(x)g'(x)]_{x=a}^{x=b}= 0
$$

##### symmetric boundary conditions $\implies$ two eigenfunctions corr. to distinct eigenvalues are orthogonal

$$
\phi(x) = \sum_n A_n X_n(x)
$$

$$
A_n = \frac{(\phi, X_n)}{(X_n, X_n)}
$$

#### Complex Eigenvalues

$$
(f, g) = \int_a^b f(x)\overline{g(x)}\, dx
$$

And Symmetric/Hermitian BCs have for $f$, $g$ satisfying them:
$$
f'(x)\overline{g(x)} - f(x)\overline{g'(x)}|_a^b = 0
$$
We still have orthgonality for distinct eigenvalue eigenfunctions with symmetric BCs

##### Symmetric Boundary conditions $\implies$ all eigenvalues are real

Eigenfunctions can be chosen to be real-valued

#### Negative Eigenvalues

If symmetric BCs and $f(x)f'(x)|_a^b \leq 0$ for all real valued functions $f$ satisfying BCs $\implies$ 

no negative eigenvalues

### 5.4 Completeness

#### Inifnite number of eigenvalues

**Eigenvalue Problem**
$$ { }
X'' + \lambda X = 0 \qquad  x \in (a, b), \text{ and symmetric BCs}
$$
We know the eigenvalues are real, but also there are an **infinite** number of eigenvalues, which form a sequence $\lambda_n \rightarrow + \infin$ w/ corresponding (pairwise) orthogonal eigenfunctions

For $f$ a function, $A_n = \frac{(f, X_n)}{(X_n, X_n)} $ is the $n$th fourier coefficient of $f$ 
$$
A_n = \frac{\int_a^b f(x)\overline{X_n(x)}\, dx}{\int_a^b |X_n(x)|^2 \, dx}
\\f = \sum A_n X_n(x)
$$
is the fourier series of $f$ 

Note: There are integrable functions $f(x)$ whose Fourier series diverges at every point $x$ and there are even continuous functions whose Fourier Series diverges at many points

#### Three Notions of Convergence

##### Pointwise in $(a, b)$ 

for all $x \in (a, b)$ $|f(x) - \sum_{n = 1}^N f_n(x)| \rightarrow 0$ as $N \rightarrow \infin$ 

##### Uniformly in $[a, b]$ 

$\max_{x \in [a, b]}|f(x) - \sum_{n=1}^N f_n(x)| \rightarrow 0$ as $N \rightarrow \infin$  

##### Mean-Square Sense

$\int_a^b |f(x) - \sum_{n=1}^N f_n(x)|^2\, dx \rightarrow 0$ as $N \rightarrow \infin$ 

#### Convergence Theorems

Memorize these, but don't go overboard on using them, cause like in the Midterm, you actually needed to do Real analysis and take limits and what not i think

These are for Fourier series for $X'' = \lambda X$ with **any symmetric BC** 

##### Uniform Convergence

$\sum A_n X_n$ of $f(x)$ converges uniformly if

1. $f, f', f''$ exist and are continuous in $[a, b]$ 
2. $f$ satisfies the given boundary conditions

For Classical Fourier Series, don't need $f''$ to exist

##### $L^2$ Convergence

$\sum A_n X_n $ of $f(x)$ converges to $f(x)$ in $L^2$ sense if $\int_a^b |f(x)|^2\, dx < \infin$ (is finite)

##### Pointwise Convergence of Classical Fourier Series

1. The Classical Fouier series $\rightarrow f(x)$ pointwise on $(a, b)$ if

   1. $f$ Continuous on $[a, b]$ 
   2. $f'(x)$ is piecewise continous on $[a, b]$ 

2. if $f(x)$ is Piecewise Continuous on $[a, b]$ and $f'(x)$ is also piecewise continuous on $[a, b]$ $\implies$ Classical Fourier Series converges at every point $x$ $-\infin < x < \infin$ 
   $$
   \begin{align}
   \sum A_n X_n &= \frac{1}{2}[f(x-)+ f(x+)] & a <x < b
   \\ \sum A_n X_n(x) &= \frac{1}{2}[f_{ext}(x-) + f_{ext}(x+)]  & -\infin <x <\infin
   \end{align}
   $$
   ​	$f_{ext}$, depending on if full, sine, cosine, is the extended periodic, odd periodict, or even periodic 

   So at a jump discontinuity, series converges to the *average* of the limits from the left and from the right

   **Question**: Possible to extend a function define on $(0, l)$ to be a full fourier series without making it odd/even?

######  $\infin$ version of this one

If $f(x)$ is periodic of period $2l$ on the line for which $f(x), f'(x)$ are piecewise continuous $\implies$ the classical full Fourier Series converges to $\frac{1}{2}[f(x+)+ f(x-)]$ for $-\infin <x <\infin$ 

##### Continuous, but non differentiable not guaranteed to converge pointwise, but will converge in the $L^2$ sense

Guaranteeing PointWise converge requires knowing something about $f'(x)$ 

##### Cannot differentiate Fourier Series Term by Term

##### Integration term by term is usually ok

#### The $L^2$ Theory

Notation, Memorization
$$
\|f\| = (f, f)^{1/2} = [\int_a^b |f(x)|^2\, dx]^{1/2}
\\ \| f- g\| = (f-g, f-g)^{1/2} = [\int_a^b |f(x)-g(x)|^2\, dx]^{1/2}
$$

##### $L^2$ Convergence

If $\{X_n\}$ are eigenfunctions associated with a set of symmetric BCs and if $\| f\| < \infin$ $\implies$ 
$$
\| f - \sum_{n \leq N}A_n X_N \| \rightarrow 0\qquad \text{as }N \rightarrow \infin
$$
Partial sums get nearer and nearer to $f$ 

#### Least-Square Approximation

Let $\{X_n \}$ be any orthgonal set of functions. Let $\| f \| < \infin$, $N \in \N$ fixed $\implies$ Among all possible choices of $N$ constants $c_1, c_2, \ldots, c_N$, the choices that minimizes
$$
\| f- \sum_{n=1}^N c_n X_n \| 
$$
is $c_1 = A_1, \ldots, c_n = A_n$ (the Fourier Coefficients)

##### Proof: To Obtain Bessel's inequality 

$$
0 \leq E_N = \|f - \sum_{n \leq N}c_n X_n \|^2 = \|f\|^2 - 2c_n(f, X_n) + \|\sum_{n \leq N}c_n X_n\|^2
$$

use orthogonality on last term to reduce it to $\sum_{n \leq N} c_n^2 \|X_n\|^2$

"complete the square":

$E_N = \sum_{n \leq N}\| X_n \|^2[c_n - \frac{(f, X_n)}{\|X_n\|^2}]^2 + \| f\|^2 - \sum_{n \leq N} \frac{(f, X_n)^2}{\|X_n\|^2}$ 

the only variables are the $c_n$'s in this case, and so minimizing $D_N$ requires: $c_n = \frac{(f, X_n)}{\|X_n\|^2} \equiv A_n$ 

Then we obtain:

$0 \leq E_n = \| f\|^2 - \sum_{n \leq N} A_n^2 \|X_n\|^2$ $\implies$ $\sum_{n \leq N}A_n^2 \| X_n \|^2 \leq \| f\|^2$ 

**Real Analysis Bit**: The left side says that all partial sums of a series of positive terms (i.e., a monotone increasing sequence of partial sums) is bounded, so it must converge. 

##### Bessel's Inequlaity

$$
\sum_{n=1}^\infin A_n^2 \int_a^b |X_n|^2\, dx \leq \int_a^b |f(x)|^2\, dx
$$

**valid as long as $\int_a^b |f|^2 \,dx < \infin$ **  

Can use Bessel's Inequality for any set of orthogonal functions $X_n$ with $A_n = (f, X_n)/\|X_n\|^2$ 

#### Parseval's Equality

$L^2$ convergence of a Fourier Series means Parseval's Equality and vice versa: 

The Fourier series of $f(x)$ converges to $f(x)$ in $L^2$ $\iff$ $\sum_{n=1}^\infin |A_n|^2 \int_a^b |X_n|^2 \, dx = \int_a^b |f(x)|^2 \, dx$ i.e., Parseval's Equality 

#### $\int_a^b |f(x)|^2\, dx < \infin$ $\implies$ Parseval's Equality 

For the Full Fourier Series:

$\sum [|A_n|^2 \int_{-l}^l \cos^2 \frac{n \pi x}{l} \, dx + |B_n|^2 \int_{-l}^l \sin^2 \frac{n \pi x}{l}\, dx]$ $= \int_{-l}^l |f(x)|^2 \, dx$ 

Proof: $\| f(x) - \sum A_n \cos \lambda x + B_n \sin \lambda x\|$ $ = \| f(x)\|^2 - 2(f(x), A_n \cos \lambda x + B_n \sin \lambda_x) + \| \sum A_n \cos \lambda x + B_n \sin \lambda x\|^2$

$= \|f(x)\|^2 - \sum 2A_n(f, \cos) - 2 B_n (f, \sin) + \| \sum A_N \cos + B_n \sin \|^2$ 

and $\| \sum A_n \cos \lambda x + B_n \sin \lambda x\|$ = $\sum_{n \leq N}\sum_{m \leq N} \int_{-l}^l A_nA_m cos \lambda_n x\cos \lambda_m x +  A_n B_m \cos \lambda_n x \sin \lambda_m x  + A_m B_n \cos \lambda_m x \sin \lambda_n x + B_n B_m \sin \lambda_n x \sin \lambda_m x\, dx$

$ = \sum_{n \leq N} |A_n|^2  \|\cos\|^2+ |B_n|^2\|\sin\|^2$ 

$E_N = \sum \|\cos\|^2(A_n - \frac{(f, \cos)}{\|\cos\|^2}) + \| \sin\|^2 (B_n - \frac{f, \sin}{\| \sin\|^2})$ $+ \|f\|^2 -\sum(\frac{(f, \cos)^2}{\|\cos\|^2} + \frac{(f, \sin)^2}{\| \sin \|^2})$

using formulas for $A_n, B_n$: 

$E_N = \|f\|^2 - \sum (|A_n |^2\|\cos \frac{n \pi x}{l}\|^2 +| B_n|^2 \| \sin \frac{n \pi x}{l}\|^2)$ 

#### Complete

The Infinite orthgonal set of functions $\{X_1(x), X_2(x), \ldots \}$ is called **complete** if **Parseval's Equality** is true for all $f$ s.t. $\int_a^b |f|^2 \, dx < \infin$ 

The set of eigenfunctions coming from $X'' = \lambda X$ with any pair of symmetric BCs is always complete by Theorem 3: Finite Integral of $|f|^2$ $\implies$ $L^2$ convergence $\implies$ Parseval's Equality, thus complete. 

### 5.5 Completeness and the Gibbs Phenomenon

Lots of new formula and concepts introduced for this long ass proof

Point is to prove the pointwise convergence of the classical full series

---

This honestly is a lot of work, so I don't think it would be pertinent to remember it too much. 

* Fixed $N$, and took the partial sum $S_N(x)$ of the Fourier series

* Kept $x$ fixed 
* Stick formuls for coefficients into partial sum, rearranging terms
* difference of angle formulas: $\cos(a - b)$, $a = ny, b = nx$ 
* Defined the Dirichlet Kernel: $K_N(\theta) = 1 + 2 \sum_{n=1}^N \cos n\theta$ $ = \frac{ \sin (N + \frac{1}{2})}{\sin \frac{\theta}{2}}$ , proof of this using geometric series stuff and DeMoivre's Formula: $2 \cos n \theta = e^{i n \theta} + e^{- i n \theta}$ 
  * Even periodic
  * $\int_{-\pi}^\pi K_N(\theta) \frac{d \theta}{2 \pi} = 1 + 0 + \cdots + 0 = 1$ 

#### Gibbs Phenomenon

$S_N$ always differes from $f(x)$ near the jump discontinuity by an overshoot of about $9$ percent

##### ~~Fourier Series Solutions~~: Didn't Lecture

##### Relationship between Fourier coefficients of full series for $f$ and $f'$ 

$$
f: A_n, B_n\qquad f': A_n', B_n' \implies
\\ A_n = -\frac{1}{n}B_n' \qquad B_n = \frac{1}{n}A_n'
$$

## Chapter 6 Harmonic Functions

### 6.1 LaPlace's Equation

Memorize these formulas

Poisson's Equation: $\Delta u = f$ 

Laplace's Equation: $\Delta u = 0$ 

##### Harmonic Function

$\Delta u = 0$ 

#### Maximum/Minimum Principle

Let $D$ be a bounded connected set

If $\Delta u = 0$ in $D$ and $u$ is continuous on $\overline{D} = D \cup \partial D$ $\implies$ The maximum and minimum of $u$ occur on $\partial D$ 

if $u$ attains its maximum in the interior $\implies$ $u$ is constant

#### Uniqueness of The Dirichlet Problem

$$
\begin{align} 
\Delta u &= f  & \text{in }D
\\ u &= g & \text{ on }\partial D
\end{align}
$$

#### Invariance in Two Dimensions

$\Delta u = 0$ is invariant under rigid motions, tranlsations, rotations

$x' = x \cos \alpha + y \sin \alpha$, $y' = - x \sin \alpha + y \cos \alpha$ 

$u_{xx} + u_{yy} = u_{x'x'} + u_{y'y'}$ (Chain Rule)
$$
u_{xx} + u_{yy} = \frac{1}{r^2}u_{\theta \theta} + u_{rr} + \frac{1}{r}u_r
$$
Laplce Equation Solutions depending only on $r$: $0 = u_{xx} + u_{yy} = u_{rr} + \frac{1}{r}u_r$ 

let $v = u_r$: $v'(r) = - \frac{1}{r}v$ $\implies$ $\ln v = - \ln r +c_1$ $\implies$ $\ln v + \ln r = 0$ 

$\ln (vr) = c_1$ $\implies$ $vr =C_1 $ $\implies$ $v = \frac{C_1}{r}$ 

$u_r = \frac{C_1}{r}$ $\implies$ $u(r) = C_1 \log(r) + C_2$  

#### Invariance in Three Dimensions

$$
u_{xx} + u_{yy} + u_{zz} = u_{rr} + \frac{2}{r}u_r + \frac{1}{r^2}(u_{\theta \theta} + (\cot \theta)u_\theta  + \frac{1}{\sin^2 \theta}u_{\phi \phi})
$$

$r = \sqrt{x^2 + y^2 + z^2} = \sqrt{s^2 + z^2}$ 
$s = \sqrt{x^2 + y^2}$ 
$x = s \cos \phi$ 	$z = r \cos \theta$ 
$y = s \sin \phi$ 	$s = r \sin \theta$ 

![image-20190505185550880](Math 126 Troublesome.assets/image-20190505185550880.png)

use Two dimensional Laplace calculation on $z, s$ 

$u_{zz} + u_{ss} = u_{rr} + \frac{1}{r}u_r + \frac{1}{r^2}u_{\theta \theta}$ 
then on $x, y$ : $u_{xx} + u_{yy} = u_{ss} + \frac{1}{s}u_s + \frac{1}{s^2}u_{\phi \phi}$ 

then add: $u_{xx} + u_{yy} + u_{zz} = u_{rr} + \frac{1}{r}u_r + \frac{1}{r^2} + \frac{1}{s}u_s + \frac{1}{s^2}u_{\phi \phi}$ 

Substitue in $s^2 = r^2 \sin^2 \theta$ , then use chain rules on $u_s$

with $r = \sqrt{s^2 + z^2}$, so $\frac{\partial r}{\partial }

$\theta = \arctan(s/z)$ $\theta_s = \frac{1}{1 + s^2/z^2}\frac{1}{z}$ $ = \frac{1}{z + s^2/z}$ $= \frac{z}{z^2 + s^2}$ $ = \frac{r\cos \theta}{r^2} = \frac{\cos \theta}{r}$ 
$r = \sqrt{s^2 + z^2}$ $r_s = \frac{s}{r}$ , noting that the $s$ gets canceled out by $\frac{1}{s}$ 
and $\phi$ is independent of $s$

Here it is important to note: 

$(x, y, z)$ are independent of each other,  then

$(s, \phi , z)$ are independent of each other, then finally

$(r, \theta, \phi)$ are independent of each other. 

where $s$ depends on $x$ and $y$, $\phi$ depends on $x$ and $y$, $r$ depends on $s$ and $z$, which then means it depends on $x, y, z$, and $\theta$ depends on $s$ and $z$ 

### 6.2 Rectangles and Cubes

Separation of variables again, so not too bad

1. Look for separated solutions
2. Use Homogeneous BC's to get Eigenvalues
3. Build LC's, sum the series
4. Now use inhomogeneous BC's

#### Rectangle

$a < x < b$, $c<y < d$ 

$x = a$: $u = j(y)$ 
$x = b$: $u= k(y)$ 
$y = c$: $u = h(x)$ 
$y = d$: $u = g(x)$ 

let $u_1 = g(x), u_2 = h(x), u_3 = j(y), u_4 = k(y)$ 
$u = X(x)Y(y)$ 

#### Cube

Similar thing: $u = X(x)Y(y)Z(z)$ 

**If I were you, I'd look over homework and examples** 

There are double sums you gotta be careful of as well as double integrals 

### Poisson's Formula

Separation of variables used here

Periodic BCs are a thing

Memorization of the formulas is kind of a bitch

---

#### Dirichlet Problem for a Circle

Recall the rotational invariance of $\Delta$
$$
\begin{align}
u_{xx} + u_{yy} &= 0  &\text{for }x^2 + y^2 < a^2
\\ u &= h(\theta) & \text{for }x^2 + y^2 = a^2
\end{align}
$$
Let $u = R(r)\Theta(\theta)$ with $\Theta(\theta+2\pi) = \Theta(\theta)$ 

Write $0 = u_{rr} + \frac{1}{r}u_r + \frac{1}{r^2}u_{\theta \theta}$ $= R''\Theta + \frac{1}{r}R' \Theta + \frac{1}{r^2}R\Theta''$ 

Obtain:

$\Theta'' + \lambda \Theta = 0$ 
$r^2 R'' + rR' - \lambda R = 0$ 

$\Theta'' = - \lambda \Theta$ $\implies$ $\Theta = C_1e^{- \sqrt{\lambda} \theta} + C_2 e^{\sqrt{\lambda}\theta}$ 

Since periodic, would need $\sqrt{-\lambda} = i b$ 

so $C_1 \cos b \theta + C_2 \sin b \theta$ $= C_1 \cos(b\theta + 2 \pi b) + C_2 \sin(b \theta + 2 \pi b)$ 

$\implies$ $b\theta + 2\pi n= b\theta + 2 \pi b$ $\implies$ $n = b$, $n$ integers, so $\lambda = n^2$ 

##### Euler Type

so we have $R(r) = r^\alpha$: 

$r^2 \alpha(\alpha - 1)r^{\alpha - 2} + r \alpha r^{\alpha - 1} - n^2 r^\alpha = 0$ 
$\alpha(\alpha - 1)r^\alpha + \alpha r^\alpha - n^2 r^\alpha = 0$ 

so $\alpha^2 - n^2 = 0$ $\implies$ $\alpha = \pm n$ 

so $R(r) = C r^n + D r^{-n}$ 

$\implies$ $u = (Cr^n + Dr^{-n})(A \cos n \theta + B \sin n \theta)$ , for $n = 1, 2, 3, \ldots$ 

In case $n = 0$: we need $R = \log r$ (Book offers no real explanation )

$u = C + D \log r$ 

Since we must have $u_{xx} + u_{yy} = 0$ inside $D$, we need $u$ to be finite in $D$, in particular $r = 0$, so we reject all solutions with $r^{-n}$, $n > 0$ and $\log r$: 

So summing the remaining: $u = \frac{1}{2}A_0 + \sum_{n = 1}^\infin  r^n (A_n \cos n\theta + B_n \sin n\theta)$ 

Now use the BC: $h(\theta) = \frac{1}{2}A_0 + \sum_{n=1}^\infin a^n(A_n \cos n\theta + B_n \sin n\theta)$ 

$\implies$ $A_n = \frac{1}{ \pi a^n } \int_0^{2\pi} h(\phi) \cos n \phi \, d \phi$ 
and $B_n = \frac{1}{\pi a^n}\int_0^{2\pi} h(\phi) \sin n \phi \, d \phi$ 

But We Sum this explicitly: Plug coefficient formulas into the series
$$
u(r, \theta) = (a^2 - r^2) \int_0^{2\pi} \frac{h(\phi)}{a-2ar\cos(\theta - \phi) + r^2} \frac{d \phi}{2\pi}
$$
$\bold x$: $(r, \theta)$ 	$\bold x' = (a, \phi)$ on the boundary

![image-20190505211116584](Math 126 Troublesome.assets/image-20190505211116584.png)

and $|\bold x - \bold x'|^2 = a^2 + r^2 - 2ar \cos(\theta - \phi)$ 

and $ds' = a d\phi$ due to arclength $s = \frac{\phi}{2\pi}(2\pi a) = \phi a$
$$
u (r, \theta) = u(\bold x) = \frac{a^2 - |\bold x|^2}{2\pi a }\int_{|\bold x'| = a} \frac{h(\phi)}{|\bold x - \bold x'|^2}ds'
$$
if $h(\phi) = h(\bold x')$ continuous on $\partial D$ $\implies$

##### Poisson's Formula

$$
u(\bold x') = \frac{a^2 - |\bold x|^2}{2\pi a}\int_{|\bold x'| = a}\frac{u(\bold x)'}{|\bold x - \bold x'|^2} \, ds'
$$

Provides the unique harmonic functions in $D$ with $\lim_{\bold x \rightarrow \bold x_0} u(\bold x) = h(\bold x_0)$ for all $\bold x_0 \in \partial D$ 

$\implies$ $u$ is infinitely differentiable in $D$ 

#### Mean Value Property

The value of $u$ at the center of $D$ equals the average of $u$ on its circumference

Proof: $u(\bold 0) = \frac{a^2}{2 \pi a}\int_{|\bold x'| = a}\frac{u(\bold x')}{a^2}\, ds'$ 

#### Maxmimum Principle

Strong form Proof requires using the Mean Value Property and the fact that the average is no greater than the Maximum, we have that if we have an interior maximum, take a circle in $D$ cetnered on that maximum, and $M = u(\bold x_M) = $ average on circle $\leq M$ 

but since no point on the circle can have $u$ be larger than $M$, we can't have any points on the circle less than $M$, otherwise it'd bring down the average, therefore all points on the circle $= M$ and this is the case for any circle centered on $\bold x_M$ that is contained in $D$, then we can repeat the argument with a different center and fill the whole domain up with circles. 

#### Differentiability

Let $u$ be a harmonic function in any open set $D$ of the plane $\implies$ $u(\bold x) = u(x, y)$ possesses all partial derivatives of all orders in $D$ 

**Proof:**  Assume $D$ is a disk: The integrand of Poisson's formula in its second form is differentiable to all orders for $\bold x \in D$, and since $\bold x' \in \partial D$, $\bold x \neq \bold x'$ 

and so differentiating the integral, we can differentiate under the integral sign. 

now let $D$ be any domain with $\bold x_0 \in D$. Then let $B$ be a disk contained in $D$ with center at $\bold x_0$, and we already know that $u$ is differentiable inside $B$, and hence at $\bold x_0$, and since this was an arbitrary point in $D$, $u$ is differentiable at all points of $D$ 

#### Poisson Kernel

$P(r, \theta ) = \frac{a^2 - r^2}{a^2 - 2ar \cos \theta + r^2} = 1 + 2\sum_{n=1}^\infin (\frac{r}{a})^n \cos n\theta$ and $\Delta P(r, \theta) = 0$ 

$P(\bold x) = \frac{a^2 - |\bold x|^2}{|\bold x - \bold x'|^2}$ and $\int_0^{2\pi} P(r, \theta) \frac{d\theta}{2\pi} = 1$ (since $\int_0^{2\pi}\cos n\theta\, d \theta = 0$ )

## Chapter 7 Green's Identities and Green's Functions

Bunch of memorization sorta

and methodology

### 7.1 Green's First Identity

Based on the property that $\nabla \cdot (v \nabla u) $  $ = \nabla v \cdot \nabla u + v \Delta u$ and using Divergence Theorem
$$
(G1) \qquad \iint_{\partial D}v \frac{\partial u}{\partial n}\, dS = \iiint_D \nabla v \cdot \nabla u \, d\bold x + \iiint_{D}v \Delta u\, d \bold x
$$

#### Harmonic Function

letting $v = 1, \nabla v = 0$ 
$$
\iint_{\partial D}\frac{\partial u}{\partial n}\, d S = \iiint_D\Delta u \, d \bold x
$$

##### Neumann Problem for Poisson:

$\Delta u = f(\bold x)$ with Boundary Condition: $\frac{\partial u}{\partial n} = h(\bold x)$ 

Not Well Posed:

* For existence: REQUIRE $\iint h \, dS = \iiint_D f \, d \bold x$ 
* add any constant to any solution of $(3)$ and still get a solution

But The **Dirichlet Problem** is unique

#### Mean Value Problem

On Sphere $\frac{\partial u}{\partial n} = \frac{\partial u}{\partial r}$ 

Average value of any harmonic function over any sphere = value at its center
$$
\frac{1}{\text{area of }S} \iint_S u\, dS = u(\bold 0)
$$


#### Maximum Principle

nonconstant harmonic function in $D$ cannot take its maximum value inside $D$ but only on $\partial D$ 

Hopf Max Principle at Max Point: $\frac{\partial u}{\partial n} > 0$ on $\partial D$ 

#### Uniqueness of Dirichlet's Problem

##### Energy Method

Use (G1) with $v = u = u_1 - u_2$, which has zero boundary data

and we come to $|\nabla u|^2 = 0$, use a vanishing theorem to get $\nabla u = 0$, to get that $u$ is a constant, and the boundary data then tells us that $u = 0$ 

#### Uniquness of Neumann's Problem (homogeneous)

$\Delta u = 0$, $\partial u/\partial n = 0$ $\implies$ $u$ is a constant in $D$, so unique up to  a constant.

#### Dirichlet's Principle

$E[w] := \frac{1}{2}\int_D|\nabla w|^2 \, d\bold x$  
$$
\begin{align}
u &\text{ solves} & & E[u] \leq E[w]
\\ \Delta u &= 0 & \iff& \forall w , w = g \text{ on }\partial D
\\ u&= g
\end{align}
$$
Among all functions that satisfy the (D) BC: $w = g$ on $\partial D$, the harmonic function $u$ satisfying it has the lowest Energy

Prove using something we use a lot: 

Defining $v = u - w$, pluggin $w = u - v$ into $E[w]$ then using (G1) and using boundary conditions and the property of harmoinc functions to obtain $E[w] = E[u] + E[v]$ 
or letting $v(\bold x)$ be any function vanishing on $\partial D$, and doing:

$e(\epsilon ) = E[u + \epsilon v]$ and $e'(\epsilon = 0) = 0$ on this to get $\iiint_D \Delta u v \, d\bold x =0$ 

and the arbitraryness of $v$ allows us to say that $\Delta u$ must equal $0$ 

### 7.2 Green's Second Identity

Green's Second Identity comes from using Green's First identity on $u, v$ and $v, u$ 
$$
(G2) \qquad \iiint_D u \Delta v - v \Delta u\, d \bold x = \iint_{\partial D}(u \frac{\partial v}{\partial n} - v \frac{\partial u}{\partial n})\, dS
$$

#### Representation Formula

For any harmonic function $u$: if $\Delta u = 0$ in $D$

$u(\bold x_0) = \iint_{\partial D}[- u(\bold x)\frac{\partial}{\partial n}(\frac{1}{|\bold x - \bold x_0}) + \frac{1}{|\bold x- \bold x_0|}\frac{\partial u}{\partial n}]\frac{dS}{4 \pi }$ 

Proof involves

* We want to use G2 using $v = (-4\pi |\bold x - \bold x_0|)^{-1}$ 
* $v$ is infinite $\bold x_0 $ at $\implies$ we create new domain $D_{\epsilon}$ from region $D$ by cutting a ball of radius $\epsilon$ and center $\bold x_0$ and use G2 on $D_{\epsilon}$ then we obtain an integral we can split back into parts $D$ and $B_{\epsilon}(\bold x_0)$ 
* Keep track of what $\partial/\partial n$ or $\bold n$ is 
* Use average value concepts
  * with circles/spheres in particular, with radius $\epsilon \rightarrow 0 $ we would get the average value to be the value of $u$ at the center

In 2  Dimensions: 
$$
u(\bold x_0) = \frac{1}{2\pi}\int_{\partial D}[u(\bold x)\frac{\partial }{\partial n}(\log |\bold x - \bold x_0|) - \frac{\partial u}{\partial n}\log |\bold x - \bold x_0|]\, ds
$$
whenever $\Delta u = 0$ in the plane domain $D$ and $\bold x_0$ is a point within $D$ 

---

Symmetric BCs for $\Delta $ in $D$: 

$\int_{\partial D}u \frac{\partial v}{\partial n} - v \frac{\partial u}{\partial n}\, dS = 0$. for all $u, v$ satisfying the BC

### 7.3 Green's Functions

$G(\bold x) = G(\bold x, \bold x_0)$ for the operator $- \Delta $ and the domain $D$ at the point $\bold x_0$ $\in D$ is a function defined for $\bold x $ $\in D$ such that 

1. $G(\bold x)$  possesses continuous 2nd derivatives and $\Delta G = 0$ in $D/\{ \bold x_0 \}$ : $G$ is smooth and harmonic in $D$ without $\bold x_0$ 
2. $G(\bold x) = 0$ on $\partial D$ 
3. $H(\bold x) = G(\bold x) - (- \frac{1}{4 \pi |\bold x - \bold x_0|}) $ is finite @ $\bold x_0$, has continuous 2nd derivatives everywhere and $\Delta H = 0$ everywhere in $D$ 

It can be shown that such a function exists and is unique. 

So if we have green's function, the solution of the Dirichlet problem is given by
$$
u(\bold x_0) = \iint_{\partial D}u(\bold x)\frac{\partial G(\bold x, \bold x_0)}{\partial n}, dS
$$

$$
u(\bold x_0) = \iint_{\partial D}g(\bold x)\frac{\partial G}{\partial n} (\bold x, \bold x_0)\, dS
$$

##### Proof: 

* use representation formula from 7.2, use $v$ = $-(4 \pi |\bold x - \bold x_0|)^{-1}$ 
* write $G(\bold x, \bold x_0) = v(\bold x) + H(\bold x)$ (From the def. of $H$)
* use that $H$ is haronic in $D$ and use G2 on $u$ and $H$ 
* add representation formula and the G2 formula

#### Symmetry of Green's Function

$G(\bold x, \bold x_0) = G(\bold x_0, \bold x)$ 

I don't believe the Professor proved this in class. 

#### Poisson's Equation

$$
\Delta u = f \quad \text{in }D \quad u = h \quad \text{on }\partial D
$$

$$
u(\bold x_0) = \iint_{\partial D} h(\bold x)\frac{\partial G(\bold x, \bold x_0)}{\partial n} \, dS + \iiint_D f(\bold x)G(\bold x, \bold x_0)\, d \bold x
$$

### 7.4 Half-Space and Sphere

Green's Function for the Half Space $D \{ z > 0 \}$ 

$G(\bold x, \bold x_0) = - \frac{1}{4\pi |\bold x - \bold x_0|} + \frac{1}{4 \pi |\bold x - \bold x_0^*|}$ 

where $\bold x^*$ $= (x, y, -z)$ so $\bold x^*_0 = (x_0, y_0, -z_0)$ 

$\Delta u = 0$, $z > 0$ , $u = h$ on $(x, y, 0)$ 

note that $\partial G/\partial n = - \partial G/\partial z|_{z = 0}$ because $\bold n$ points downard, outward from the domain

Remember that $|\bold x - \bold x_0|^{-1} = ((x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2)^{-1/2}$

so the derivative w.r.t. to $z$: $(2(z-z_0))[(x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2]^{-3/2}$  

$u(\bold x_0) = \frac{z_0}{2 \pi } \iint_{\partial D}h(\bold x) \frac{1}{|\bold x - \bold x|^3}\, dS$ 
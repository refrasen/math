# Questions

##### 1. From Lecture 38:

Are we allowed to do $(u, v) = (v, u)$ for the inner product?

###### 1.1: Why is the $t$ still ok in the denominator for:

$\frac{1}{2t} \| v(t) - u^0 \|^2 + E[v(t)] \leq \frac{1}{2t} \| v(s) - u^0 \|^2 + E[v(s)]$ 

Why are we allowed to keep the $t$ there?

Because isn't $\frac{1}{2t} < \frac{1}{2s}$? 

I know that $\frac{1}{2s}\| v(s) - u^0\|^2 + E[v(s)]$ is larger than $e(t)$ 

###### 1.2: 

We had
$$
e(h) - e(0) = \int_0^h \frac{d}{dt}e(t)\, dt = - \frac{1}{2}\int_0^h \| \frac{v(t) - u^0}{t}\|^2 \, dt
$$
and
$$
\frac{1}{2h}\| \underset{ = u^1}{v(h) } - u^0\| + E[\underset{= u^1}{v(h)}] - (0 + E[u^0]) = e(h) - e(0)
$$
Confused about getting to here:
$$
\frac{E[u^1]- E[u^0]}{h} = - \frac{1}{2}\int_0^h \| \frac{v(t)- u^0}{t}\|^2 \, dt - \frac{1}{2}\|\frac{u^1 - u^0}{h}\|^2
$$
Question: Did we divide by $h$? 

###### 1.3:

In
$$
|\partial E|(u) := \underset{w \rightarrow u}{\lim \sup} \frac{(E(u)- E(w))_+}{|u - w|}
$$
What does the subscript $+$ mean?

###### 1.4:

![image-20190504142620426](Math 126 Questions.assets/image-20190504142620426.png)

where did the $1/h$ go on the left side?

##### 2. What is a weak solution?

https://en.wikipedia.org/wiki/Weak_solution

##### 3. What exactly does convex mean?

https://en.wikipedia.org/wiki/Convex_function

i.e.: $f(x) \geq f(y) + f'(y) (x-y)$ for all $x$ and $y$ in the interval

In particular, if $f'(c) = 0$, then $c$ is global minimum of $f(x)$ 

##### 4. There were times when we needed to reference sort of long involved proofs, would we need to do that?

##### 5. For Chapter 8, will we need to come up with our own "Ansatz" when using separation of variables?
# Math 126 Outline Part 2

## Chapter 9 Waves in Space

**Goal**: Study
$$
u_{tt} - c^2 \Delta u = 0
\\ u_t - k \Delta u = 0
$$
$u = u(\bold x, t)$, $\bold x = (x_1, \ldots, x_d)$, $d = 2$ or $d = 3$ 

### 9.1 Waves: Energy And Causality

#### Characteristic Cone at $(\bold x_0, t_0)$ 

$|\bold x - \bold x_0| = c|t-t_0|$ 

* light cone, for $c$ speed of light
* cone is the union of all light rays emanating from $(\bold x_0, t_0)$ 
* **Solid light cone**: inside of the cone $\{|\bold x - \bold x_0| < |t-t_0| \}$ 
  * union of the future and past half-cones
* Fixed time $t$: light cone is just an ordinary sphere 
* Letting $\phi(t, x, y, z) = -c(t-t_0)^2 + |\bold x - \bold x_0|^2$ , the light cone is a level surface of $\phi$, for $\phi = 0$ 
* unit normal vectors: $\bold n = \pm \frac{\text{grad }\phi}{|\text{grad }\phi|}$, from the fact that gradients are perpendicular to level surfaces  

#### Energy Conservation

$$
E(t) := \frac{1}{2}\int_{\R^2}u_t^2 + c^2 |\nabla u|^2\, d\bold x
$$

$E$ is constant if $u_{tt} = c^2 \Delta u$: 

##### Some Preliminary Algebra

$\nabla u \cdot \nabla u_t = u_{x}u_{tx}$ 

$\nabla \cdot u_t \nabla u = \nabla u \cdot \nabla u_t + u_t \Delta u$

so $\nabla u \cdot \nabla u_t = \nabla \cdot (u_t \nabla u) - u_t \Delta u$ 

---

$E'(t) = \iiint_{\R^3} (u_t u_{tt} + c^2 \nabla u \cdot \nabla u_t) \,  d \bold x$ 
$ = \iiint_{\R^3} u_t(u_{tt} - c^2 \Delta u) \, d\bold x + \iint_{\partial \R^3} c^2 u_t \frac{\partial u}{\partial n} dS$ 

the boundary is $\infin$, so if $u_t, \nabla u \rightarrow 0$ as $\bold x \rightarrow \infin$ in the approrpiate sense, and then using $u_{tt} - c^2 \Delta u =0$ 
$E'(t) = 0 + 0 = 0$ 

**Note**: Obtaining the Energy Equation requires multiplying the wave equation by $u_t$, doing some algebra with the gradients, the integrating over $3$-space, using that derivatives of $u$ should disappear as $|\bold x| \rightarrow \infin$ 

#### Causality and Finite Speed of Propagation

Notation: Fix $\bold x_0 \in \R^3$, $t_0 > 0$ 
Important Geometry Concept: 
![image-20190506160622438](Math 126 Outline Part 2.assets/image-20190506160622438.png)

**Theorem**: $u(\bold x_0, t_0)$ depends only on the values of $\phi(\bold x)$ and $\psi(\bold x)$ in the ball $\{ |\bold x - \bold x_0| \leq ct_0\}$ 

For $T(t) = \{ \bold x \in \R^3: |\bold x - \bold x_0| \leq c(t_0 - t) \}$ $ = B_{c(t_0 - t)}(\bold x_0)$ $(0 \leq t \leq t_0)$ 
$E(t) := $ energy inside $T(t)$ at time $t$ $ = \frac{1}{2} \int_{T(t)}u_t^2 + c^2 |\nabla u|^2 \, d\bold x$ 

##### Theorem

1. $\frac{d}{dt} E(t) \leq 0$ 
2. If $u = 0, u_t = 0$ on $T(0)$, then $u = 0$ in the whole backward light cone $\implies$ Causality, i.e., for solutions $u_1$ and $u_2$ for the wave equation that have the same initial conditions on the backwards light cone: $|\bold x - \bold x_0| \leq  c|t_0|$ , their values at $(\bold x_0, t_0)$ are the same, i.e., the solution at the point only depends on the initial conditions in the ball $B_{c(t_0)}(\bold x_0)$  
   1. might go as far to say $u_1, u_2$ are the same throughout the backwards light cone

The Proof of this is quite involved and has been used in some homework assignments. 

#### Exercises

##### Exercise 1

Find all solutions of the wave Equation of the form $u(\bold x, t) = f(\bold k \cdot \bold x - ct)$ : $f''(\bold k \cdot \bold x - ct) = |\bold k|^2f''(\bold k \cdot \bold x - ct)$
 This was a simple plug it in to the equation and find out that we either need: $|\bold k | = 1$ and $f$ can be <u>arbitrary</u>, or $f''(\bold k \cdot \bold x - ct) = 0$ $\implies$ $f'(s) = c_1$ $\implies$ $f(s) = c_1 s + c_2$, so $f(\bold k \cdot \bold x - ct) = c_1(\bold k \cdot \bold x - ct) + c_2$ 

##### Exercise 2

Verifying that $(c^2t^2 - x^2 - y^2 - z^2)^{-1}$ satsifies the wave equation except on the light cone: obviously, not differentiable when $x^2 + y^2 + z^2 = c^2t^2$, Which is the light cone, but otherwise we just plug it into the equation

##### Exercise 5

Prove the principle of causality in two dimensions

This just requires doing the same proof in the book but in 3 dimensions. 

##### Exercise 7

For the boundary condition $\partial u/\partial n + b \partial u/\partial t = 0$ with $b > 0$, show that the energy defined by $(6)$ decreases

* Take time derivative of $E$
* use integration of parts: $\nabla u \cdot \nabla u_t $ $= \nabla \cdot (u_t \nabla u) - u_t \Delta u$ 
* Plug in boundary conditions

##### Exercise 8

Uses the proof of Energy and Causality but on a different equation. 

This is a whole copy the same process as in the book type shtick, these are fine, except that it's hard to remember what tricks the book used on my own. 

### 9.2 Wave Equation in Space-Time

$$
u_{tt} = c^2 \Delta u \qquad \text{in }\R^3 \times (0, \infin)
\\
\begin{align}
u&= \phi 
\\ u_t &= \psi
\end{align}
\begin{cases} \text{ on } \R^2 \times \{ t = 0 \}\end{cases}
$$

**Trick**: Convert to 1-dimensional wave equation with spherical coordinates? 

Proof Involved: 

* Define $\overline{u}(r, t) := \frac{1}{4 \pi r^2} \int_{\partial B_r(\bold x)} u(\bold y, t)\, dS(\bold y)$ 
* $\Delta(\overline u) = \overline{(\Delta u)}$, the laplacian of the mean is the mean of the laplacian 
* When using $\Delta$ in terms of $(r, \theta, \phi)$, recognize when a solution only depends on $r$ 
* Spherical Coordinates, converting from $\iiint_D u \, d \bold x$ $= \iiint_D u \, r^2 \sin \theta \, d \theta, d \phi, dr$ 
* Using Divergence Theorem 
* substituting $v = r \overline u$ , seeing what $v$ satisfies 
* Recognizing when we have problems we already know how to solve: in this case $v$ satisfies the one dimensional wave equation
* Recovering $u$ from $v$, which is sorta tricky

##### Kirchhoff's Formula

Solution to the homogeneous wave equation above
$$
u(\bold x, t) = \frac{1}{4 \pi c^2 t}\int_{\partial B_{c t(\bold x)}}\psi \, dS + \frac{\partial }{\partial t}(\frac{1}{4 \pi c^2 t} \int_{\partial B_{ct(\bold x)}}\phi \, dS)
$$

#### Hyugen's Principle

Any solution of the wve equation in $(3 + 1)$ dimensions propagates at exactly the speed $c$ of light, no faster and no slower

so the solution at some point $(\bold x_0, t_0)$ only depends on $\psi(\bold x), \phi(\bold x)$ for $\bold x$ satisfying $|\bold x - \bold x_0| = ct_0$, similarly, 

The values of $\psi, \phi$ a point $(\bold x_1, 0)$ influence the solution only on the surface $\{|\bold x - \bold x_1| = ct \}$ 

#### Wave Solution in $\R^2$ $\times$ $(0, \infin)$ 

##### Method of descent

Regard $u = u(x_1, x_2, t)$ as a solution to the $(3 + 1)$ dim. wave equation, which doesn't depend on the $x_3$ coordinate. 

we use that on $x^2 + y^2 + z^2 = c^2t^2$, we are integrating twice over the top hemisphere: $z = \sqrt{c^2t^2 - x^2 - y^2}$, and so for this to be valid, we need $x^2 + y^2 \leq c^2t^2$, so we integrate over such $(x, y)$ 

then we use $dS = \sqrt{(\frac{dz}{dz})^2 + (\frac{dz}{dy})^2 + (\frac{dz}{dx})^2}$ 

Soon arrive at: 
$$
u(\bold x,  t) = \frac{1}{2 \pi c} \int_{B_{ct}(\bold x)} \frac{\psi(\bold y)}{\sqrt{c^2t^2 - |\bold x - \bold y|^2}}\, d \bold y + \frac{1}{2 \pi c} \frac{\partial }{\partial t}[\int_{B_{ct}(\bold x)}\frac{\phi(\bold y)}{\sqrt{c^2 t^2 - |\bold x - \bold y|^2}}d \bold y]
$$
Hyugen's Principle Fails for $d = 2$ , solution at $(\bold x, \bold t)$ depends on $\phi, \psi$ on the entire ball $B_{ct}(\bold x)$ 

#### Exercises

##### Exercise 5

Where does a three-dimensional wave have to vanish if its initial data $\phi$ and $\psi$ vanish outside of a sphere?

Let $R$ be the radius of the sphere

Use Hyugen's principle: a point $(\bold x_1, 0)$ affects only points satisfying $|\bold x - \bold x_1| = ct$, so the points on the sphere travel out and away at speed $c$ and so outside of the sphere of radius $R + ct$, the wave vanishes. 

And after time $R/c$, the waves from the surface of the sphere begin moving out from the center again, so inside the sphere of size $c(t - R/c)$ for $t > R/c$, we have the wave vanishing. 

##### Exercise 11 

Find solutions of the three-dimensional wave equation that depend only on $r$ and $t$ 

$u_{tt} = c^2 \Delta u$ $= c^2[\frac{2}{r}u_{r} +u_{rr}]$ 
$ru_{tt} = c^2[2u_r + ru_{rr}]$  

let $v(r, t) = r u(r, t)$ $v_{tt} = ru_{tt}$, $v_{r} = u + ru_r$ $v_{rr} =2 u_r + ru_{rr}$ 
so we end up with $v$ solving $v_{tt} = c^2 v_{rr}$ 

where $v(r, 0) = r\phi$, $v_t(r, 0) = r\psi$ , for $r \geq 0$ (half-line problem)

$v = \frac{r}{2}[\phi(r + ct) + \phi(ct - r)] + \frac{1}{2c}\int_{ct - r}^{ct+r}s\psi \, ds$ when $0 \leq r \leq ct$ 
$u = v/r = \frac{1}{2}[\phi(r + ct) + \phi(ct -r)] + \frac{1}{2c r} \int_{ct - r}^{ct + r} s \psi \, ds$ 

or more generally: $[f(r+ct) + g(r-ct)]/r$ 

### 9.3 Rays, Singularities, and Sources

#### Skipped Rays, Singularities

$$
\begin{align}
u_{tt} - c^2 \Delta u =f && \text{in }\R^2 \times (0, \infin)
\\ u = u_t = 0 && \text{on }\R^3 \times \{ t = 0 \}
\end{align}
$$

##### Duhamel's principle

To solve $(*)\begin{cases} u_{tt} - Au = f & t > 0 \\ u, u_t = 0 & t = 0 \end{cases}$ 

$v = v(t, s)$ Solves: $\begin{cases} v_{tt} - Av = 0 & t > s \\ v = 0 , v_t = f(s) & t = s \end{cases}$ 

$\implies$ $u(t) := \int_0^t v(t, s)\, ds$  solves $(*)$ 

Using **Kirchhoff's Formula** the solution $v$ of $\begin{cases} v_{tt} - c^2 \Delta v = 0 & t > s \\ v = 0, v_t = f(\bold x, s) & t = s \end{cases}$ 

$v(\bold x, t, s) = \frac{1}{4 \pi c^2(t-s)} \int_{\partial B_{c(t-s)}(\bold x)} f(\bold y, s) \, dS(\bold y)$ 
$u(\bold x, t) = \int_0^t v(\bold x, t, s) \, ds $, write this out, change variables: $r = c(t-s)$, $dr = - c\,  ds$, 
$$
u(\bold x, t) = \frac{1}{4 \pi c^2} \int_{B_{ct}(\bold x)}\frac{f(\bold y, t - \frac{|\bold y - \bold x|}{c})}{|\bold y - \bold x|}\, d \bold y
$$
Integral over the surface of the backward light cone

#### Exercises

##### Exercise 3

Prove theorem 2 in the one-dimensional case: i.e., you can uniquely solve the homogeneous wave equation with $u = \phi$ and $\partial u/\partial n = \psi$ on $S$ 

Copy proof in Section 9.1 about Causality, but replacing $B$ with curve $C$ as the base. 

##### Exercise 7

Solve $u_{tt} - c^2 \Delta u$, where $f(\bold x) = A$ for $|\bold x| < \rho$ and $f(\bold x) = 0$ for $|\bold x| > \rho$ , 

Follow hint, use formula $\frac{1}{4 \pi c^2} \iiint_{|\bold \xi - \bold x| \leq ct} \frac{f(\bold \xi, t - |\bold \xi - \bold x|/c)}{|\bold \xi - \bold x|}d\bold \xi$ 
Multivariable calculus area-finding problem… This was freakin hard. 

### 9.4 The Diffusion and Schrodinger Equations

#### Diffusion in Higher Dimensions 

$$
\begin{align}
u_t - k \Delta u = 0  && \text{in }\R^d \times (0, \infin)
\\ u = \phi && \text{on }\R^d \times \{t = 0\}
\end{align}
$$

**Theorem**: If $\phi$ is continuous and bounded $\implies$
$$
u(\bold x, t) = \frac{1}{(4 \pi k t)^{d/2}} \int_{\R^d} e^{- \frac{|\bold x - \bold y|^2}{4kt}} \phi(\bold y)\,d \bold y
$$
for all $ \bold x \in \R^d$< $t > 0$ 

Note: for $d = 1$, this is indeed the solution. 

Source Function:
$$
S(\bold x, t) := \prod_{i=1}^d S(x_i, t)
$$
So the theorem is essentially:
$$
u(\bold x, t) = \int_{\R^d} S(\bold x - \bold y, t)\phi(\bold y)\, d \bold y
$$
Proof:

* $S$ solves $S_t - k \Delta S = 0$ in $\R^d \times (0, \infin)$ 
* $\lim_{t \downarrow 0} \int_{\R^d}S(\bold x - \bold y, t)\phi(\bold y) \, d \bold y= \phi(\bold x)$ 
  * Uses $\int_{\R^d}S(\bold x, t) d \bold x =1$ 
  * One dimensional case for $\int_{\R}S(x_i - y_i, t)\phi(y_i)dy_i$ $\rightarrow \phi(x_i)$ as $t \downarrow 0$ 
  * so this also works for $\phi(\bold y) = \prod_{i = 1}^d \phi_i(y_i)$ 
  * and immeidately generalizes to any linear combination of such products $\implies$ Holds for any $\phi(\bold x)$ 

#### Schrödinger's Equation

$$
\begin{align}
ihu_t + \frac{h^2}{2m}\Delta u + V(x)u = 0 && \text{in }\R^3 \times \R
\\
u = \phi && \text{on }\R^3 \times \{t=0\}
\end{align}
$$

**Note**: $u$ is complex valued, $V = V(x) = $ real valued potential
$|u|^2 = u\overline u$ $(= uu^*)$ 

Important Observation: 

$\int_E |u|^2(\bold x, t)\, d \bold x$ = probability particle is within the region $E$ at time $t$ 

Simplest atom: Hydrogen atom as $V(x) = \frac{e^2}{r}$ 

##### Free Shrödinger equation

$-i \frac{\partial u}{\partial t} = \frac{1}{2}\Delta u$ in $\R^3 \times \R$, and $u = \phi$ on $\R^3 \times \{t=0\}$ 
**Solution**: $u(\bold x, t) = \frac{1}{(2 \pi i t)^{3/2}} \int_{\R^3} e^{-\frac{|\bold x - \bold y|^2}{2 i t}}\phi(\bold y)\, d \bold y$ 
resembes diff. eq. with $k = i/2$ , but behavior is wave-like 

But there's a question of what square root for a complex number we take in the first factor. 
**Assume** $\phi = 0$ outside some ball $B_{R}(\bold 0)$ 

We solve for $\epsilon > 0$:  $\frac{\partial u_\epsilon}{\partial t} = \frac{\epsilon + i}{2}\Delta u_\epsilon $ in $\R^3 \times \R$ and $u_\epsilon = \phi$ in $\R^3$ $\times $ $\{ t = 0\}$ 

**Solution**: $u_{\epsilon} = \frac{1}{(2 \pi t)^{3/2}(\epsilon + i)^{3/2}}\int_{\R^3} e^{- \frac{|\bold x - \bold y|^2}{2(\epsilon + i)t}}\phi(\bold y)\, d \bold y$ 
where $(\epsilon + i)^{1/2}$ is the square root of $\epsilon + i$ with the positive real part $\approx \frac{1 + i}{\sqrt{2}}$  (small $\epsilon \approx 0$)

So the exponential decays since $\epsilon > 0$ and the integral converges. 

Note: Separation of variables fails here. 

##### Conservation of Probability Density

$\frac{d}{dt}\int_{\R^3}|u|^2 (\bold x, t) \; d \bold x =0$ 

#### Exercises

##### Exercise 1

$u_t = k \Delta u$ 

$\phi(x, y, z) = xy^2 z$ 

Let $u = xz v(y, t)$, I claim that if $v$ satisfies $v_t = k v_{yy}$ with initial condition $v(y, 0) = y^2$, we have

$u_t = xz v_t$

$u_{x} = zv(y, t)$ $\implies$ $u_{xx} = 0$ and similarly: $u_{zz} = 0$ 
$u_y = xz v_y$ $\implies$ $u_{yy} = xz v_{yy}$ 
so $u_t = xz v_t = xz(kv_{yy}) = k(xzv_{yy}) = k(0 + xzv_{yy} + 0) = k\Delta u$ 

and $v_{yyy}$ satisfies $v_t = kv_{yy}$ 
with $v_y(y, 0) = 2y$ $v_{yy}(y, 0) = 2$ , $v_{yyy}(y, 0) = 0$ 
so $v_{yyy} = 0$ by uniqueness, so $v_{yy} = A(t)$, $v_y = A(t)y + B(t)$, $v = \frac{1}{2}A(t)y^2 + B(t)y + C(t)$ , $v_y = A(t)y + B(t)$, $v_{yy} = A(t)$ 

and $v_{t} = \frac{1}{2}A'(t)y^2 + B'(t)y + C'(t) = kA(t)$ 
so $A'(t) = B'(t) = 0$ and $C'(t) = kA(t)$ 
and $v(y, 0) = \frac{1}{2}A(0)y^2 + B(0)y + C(0) = y^2$ 
$A(0) = 2$, so $B(t) = 0$, $C(0) = 0$ 
$C'(t) = 2k$ $\implies$ $C(t) = 2kt $ 
so $v(y, t) = y^2 + 2kt$, so $u = xzy^2 + xz2kt$ 

##### Exercise 2

a. Easy to show: $S_3$ separates into three integrals in each of $x, y, z$ 

Note: needed to use some real analysis: 

b. $|\phi_n - \phi| \rightarrow 0$ so for any $\epsilon > 0$, there exists $n$ s.t. $|\phi_n - \phi| < \epsilon $ 

then we have $|\lim_{t \rightarrow 0}\int  S(x-y, t)\phi(y)dy - \phi(x)|$ $= |\lim_{t \rightarrow 0}\int S(x-y, t)[\phi(y)  - \phi_n(y)] \, dy  - (\phi - \phi_n)| $

etc. 

##### Exercise 3

Method Of Reflection.

### 9.5 The Hydrogen Atom

Idk how important this is:

$iu_t = - \frac{1}{2}\Delta u - \frac{1}{r}u$ , with $e = m = h = 1$ 
with $\iiint |u(\bold x, t)|^2 \, d \bold x < \infin$ 

* Separate Variables: $u(\bold x, t) = T(t)v(\bold x)$ 
* Potential term leads to eigenvalues 
* Look for spherically symmetric solutions, $v(\bold x) = R(r)$ 
* This is very involved...

#### Exercise 2

This just required

* letting $r \rightarrow \infin$ to $-R'' = \lambda R$ 
  with solutions $e^{\pm \beta r}$, where $\beta = \sqrt{- \lambda}$  
* but if $\lambda > 0$, we must have $\beta$ complex, which means oscillating solutions

## Chapter 8: Computation of Solutions

### Finite Difference Method

Approximate derivatives in terms of **discrete values of $u$**: "step size"

PDE $\sim \rightarrow$ "Recursive relation"

##### Taylor Expansion:

$f(x+h) = \sum_{k = 0}^\infin \frac{f^{(k)}(x)}{k!}h^k$ 
using this with $u(x+h), u(x-h)$ yields

$u'(x) = \frac{u(x+h)-u(x)}{h} + O(h)$ $= \frac{u(x)-u(x-h)}{h} + O(h)$ $ = \frac{u(x+h) - u(x-h)}{2h} + O(h^2)$ 

$u''(x) = \frac{u(x+h) + u(x-h) - 2u(x)}{h^2} + O(h^2)$ 

Note: $\frac{1}{h}O(h^k) = O(h^{k-1})$ , or $f_1 = O(g_1), f_2 = O(g_2)$ $\implies$ $f_1f_2 = O(g_1g_2)$ , $f\cdot O(g) = O(fg)$ 

Notation: $h = \Delta x$ (not Laplacian), step size for $x$ 
$u_j^n = u(j \Delta x, n \Delta t)$ 



#### Heat Equation

$\frac{u_j^{n+1} - u_j^n}{\Delta t} = \frac{u_{j+1}^n - 2u_j^n + u_{j-1}^n}{(\Delta x)^2}$ 

#### Initial Conditions:

$u_j^0 = \phi(j\Delta x)$ 

**Notes**: 

* Be careful of step sizes

#### Introducing $s$ 

Usually try $s := \frac{\Delta t}{(\Delta x)^2}$ 

New problem: $u_j^{n+1} = s(u_{j+1}^n -2u_j^n + u_{j-1}^n) + u_j^n$ 

### Choice of $s$: Separation of Variables

letting $u_j^n = u(j\Delta x, n\Delta t) = X(j\Delta x)T(n \Delta t)$ $= X_j T_n$ 

$u_j^{n+1} = s(u_{j+1}^n + u_{j-1}^n) + (1-2s)u_j^n$ 
$\frac{T_{n+1}}{T_n} = 1 - 2s + s(\frac{X_{j+1} + X_{j-1}}{X_j} )$ $ = \xi$, a constant

$T_{n+1} = \xi T_n$ $\implies$ $T_1 = \xi T_0$ $\implies$ $T_n = \xi^n T_0$ 

For the part involving $X$'s, we guessed $X_j = A \cos j\theta + B \sin j \theta$ 
with boundary conditions: $0 = X_0 = X_J$ 
$X_0 = 0 \implies A = 0$ and $X_J= 0$ $\implies$ $\sin J\theta = 0$ $\implies J \theta = \pi k$ 
$\implies$ because the boundary is $x = 0, x = \pi$ we have $J = \pi/\Delta x$ 
so $\theta = k \Delta x$ $\implies$ $X_j = \sin (j k \Delta x)$ 

Going back to our separated variable problems:

$\sin(j k \Delta x + k \Delta x) + \sin(j k \Delta x - k \Delta x)$ 
$= 2 \sin(j k \Delta x) \cos(k \Delta x)$ , then dividing by $\sin(jk \Delta x)$ 

gives: $1 - 2s(1 - \cos(k \Delta x)) = \xi = \xi(k)$ 
and $T_n = \xi(k)^n T_0$ $\implies$ exponential growth unless $|\xi(k)| \leq 1$ 

$-1 \leq \cos(k \Delta x) \leq 1$ $\implies$ $0 \leq 1 - \cos(k \Delta x) \leq 2$ $\implies$  
$-4s \leq -2s(1 - \cos(k \Delta x)) \leq 0$ $\implies$ $1 - 4s \leq \xi(k) \leq 1$ 
$\implies$ we need $1 - 4s \geq - 1$ $\implies$ $s \leq \frac{1}{2}$ 

### Boundary Conditions

$J$ = # of space intervals (when finite), $J = \frac{l}{\Delta x}$ 

#### Dirichlet Condition: $0 \leq x \leq l$ 

$u(t, 0) = g(t), u(t, l) = h(t)$ $\sim \rightarrow$ $u_0^n = g(n \Delta t)$, $u_J^n = h(n \Delta t)$ 

#### Neumann Condition

$u_x(t, 0) = g(t), u_x(t, l) = h(t)$ $\sim \rightarrow$ 
$\frac{u_{1}^n - u_{-1}^n}{2 \Delta x} = g(n\Delta t)$ and $\frac{u_{J+1}^n - u_{J-1}^n}{2 \Delta x} = h(n \Delta t)$ 

Ghost points: $u_{-1}^n, u_{J+1}^n$ 

### Wave Equation

$\frac{u_j^{n+1} + u_j^{n-1} - 2u_j^n}{(\Delta t)^2} = c^2 \frac{u_{j+1}^n - 2u_j^n + u_{j-1}^n}{(\Delta x)^2}$ 

here: $s = c^2 \frac{(\Delta t)^2}{(\Delta x)^2}$ 
$u_{j}^{n+1} = s(u_{j+1}^n + u_{j-1}^n ) + 2(1-s)u_j^n - u_{j}^{n-1}$ 

We now have: two steps before involved

#### Chooseing $s$: Separate Variables

Just want to study eigenvalue $\xi$, not solve BCs

Using the guess $u_j^n =( e^{ik \Delta x})^j \xi^n$, needing $|\xi| \leq 1$ 
substitute $u_j^n$ into $(*)$ 

$\xi + \frac{1}{\xi}  - 2 = 2s(\cos(k \Delta x) -1) = : 2p$ 

$\implies$ $\xi^2 - 2(1+p)\xi + 1 = 0$ , has roots $\xi = 1 + p \pm \sqrt{p^2 + 2p}$ 

Note: $p \leq 0$, so we have real roots $\xi$ if $p(p+2) \geq 0$ $\implies$ $p + 2 \leq 0$ $\implies$ $p \leq - 2$ , which means one of the real roots will be less than $-1$ $\implies$ we have one root with $|\xi| > 1$ so this is **unstable** 

But if we have complex roots: $p > -2$: $\xi = 1 + p \pm i \sqrt{-p^2 - 2p}$ 

$|\xi|^2 = (1+p)^2 + -p^2 - 2p = 1$ **(!!!)** This is surprising, but makes sense, solutions oscillate in time (wave equation)

so we must have $p \geq - 2$ for all $k$ $\implies$ 
$s(\cos (k \Delta x) - 1) \geq - 2$ 

**Be Careful With this Equation**

Idea: We use $\xi$ and $|\xi| \leq 1$ to find out what $s$ must be. 

### LaPlace Equation

$\frac{u_{j+1, k} + u_{j-1, k} - 2u_{j, k}}{(\Delta x)^2} + \frac{u_{j, k+1} + u_{j, k-1} - 2u_{j, k}}{(\Delta y)^2} = 0$ 

$\Delta x = \Delta y$ by symmetry 

$\implies$ $u_{j, k} = \frac{1}{4}[u_{j+1, k} + u_{j-1, k} + u_{j, k+1} + u_{j, k-1}]$ 

Average of neighbors: Mean Value Property

Requires solving $N \times N = N^2$ system $(j, k)$

### Other Schemes

Didn't really go over much:

#### Crank-Nicolson Scheme (Heat Eq.)

$\frac{u_j ^{n+1} - u_j^n}{\Delta t} = \frac{u_{j+1}^n - 2u_j^n + u_{j-1}^n}{(\Delta x)^2} (1 - \theta) + \theta ( \frac{u_{j+1}^{n+1}- 2u_j^{n+1}+u_{j-1}^{n+1}}{(\Delta x)^2})$ 

**Separate Variables**, Get $\xi(k)$ $ = \frac{1 - 2(1-\theta)s(1- \cos k \Delta x)}{1 + 2 \theta s(1- \cos k \Delta x)}$ 

get $\xi(k) \leq 1$ always, $\xi(k) \geq - 1$ if $\frac{1}{2}\leq \theta \leq 1$ 

#### (LaPlace) Jacobi Iteration

Meh..

$u_{j, k}^{n+1} =$ average step $n$ 

**Gauss - Seidel**: Traverse from bottom left, averaging: 

$u_{j, k}^{n+1}$ = average: $\begin{align}& &old & \\ new & & \times & &old \\ & &new   \end{align}$ $ = \frac{1}{4}[u_{j-1, k}^{n+1} + u_{j, k-1}^{n+1} + u_{j, k+1}^{n-1} + u_{j+1, k}^{(n+1)}]$ 

Overrelaxation: take Gauss method, multiply by $4\omega$ and add $(1 - 4\omega)u_{j, k}^n$ 

Note: Think of the grid like $(x, y) \equiv (j, k)$ coordinates, so up is $k+1$, down is $k - 1$ left is $j -1$ and right is $j + 1$ 

#### Exercises

##### 8.1.1-8.1.2: Easy

Only requires extending out the taylor polynomial as far as we know $u$ has derivatives, adding, subtracting, using Big O

##### 8.1.3: a little tricky

Requires using $u(x\pm 2h) = u((x\pm h)\pm h)$ 

and using differendce formulas on $u'''$ 

Somehow: $u^{(4)}(x+h) - u^{(4)}(x-h) = O(h)$  

##### 8.2.1: Computations and keeping track of formulas

##### 8.2.10: 

###### a. implicit or explicit scheme

Need to make sure you know difference formulas, or can at least easily derive them

###### b. Unstable/Stable: separation of variables, use an Ansatz

Kosher one seems to be $X_j = (e^{ik \Delta x})^j$ 

or $u_j^n = (e^{ik\Delta x})^j (\xi(k))^n$ 

Root finding for $\xi(k)$ ? Sometimes no:

USING A RECURRENCE RELATION for $T_{n+1} = T_{n-1} + \xi T_n$ 

$a_n = c_1 a_{n-1} + c_2 a_{n-2} + \cdots + c_d a_{n-d}$ 

yields: $p(t) = t^d - c_1t^{d-1} - c^2 t^{d-2} - \cdots -  c_d$ 
so $T_{n+1} = T_{n-1} + \xi T_n$ 

$T_{N} = T_{N-2} + \xi T_{N-1}$ 

$x^2 - \xi x - 1 = 0$

$x = \frac{\xi \pm \sqrt{\xi^2 - 4}}{2}$ ??

##### 8.2.12

$\frac{u_j^{n+1} - u_{j}^{n}}{\Delta t} = \frac{u_{j+1}^n + u_{j-1}^n - 2u_j^n}{(\Delta x)^2} + (u_j^n)^3$

can't just substitute $s = \Delta t/(\Delta x)^2$ here due to the nonlinear $u_j^n$ term which is why the book said it's ok to use the standard one and just tacking on $(u_j^n)^3$ at the end. 

* Come up with a scheme
* be able to keep track of the calculations

## Chapter 12: Distributions and Transforms

Example: the $\delta$ distribution or $\delta$ measure

### 12.1 Distributions

#### Delta Functions

* infinite at $x = 0$ 
* zero at all $x \neq 0$ 
* Integral overal $(-\infin , \infin)$ should equal 1

#### Definitions and Notation

##### Test Functions

* smooth, in $C^\infin$ 
* $\exists R > 0$, s.t. it vanishes outside of $(-R, R)$, vanishes at $\pm \infin$ 

$\mathscr{D} = \{ \text{test functions}\}$ 

##### Distributions

A map $f: \mathscr{D} \rightarrow \R$, $\phi \mapsto (f, \phi)$, which is 

* **linear:** $\forall a, b \in \R, \forall \phi, \psi \in \mathscr{D}$: $(f, a\phi + b\psi) = a(f, \phi) + b(f, \psi)$ 
* **Continuous**: Whenever $\phi_n \rightarrow \phi$ in $\mathscr{D}$, then $(f, \phi_n) \rightarrow (f, \phi)$ (convergence)

##### Notion of Convergence

Here $\phi_n \rightarrow \phi$ in $\mathscr{D}$ means 

- $\forall \alpha = 0, 1, 2,\ldots$: $\frac{\partial^\alpha}{\partial x^\alpha}\phi_n \rightarrow \frac{\partial^\alpha}{\partial x^\alpha} \phi$ as $n \rightarrow \infin$ **uniformly** 
- $\exists R > 0$ $\forall n = 1, 2, \ldots$: $\phi_n = 0$ outside of $(-R, R)$ 

##### Delta Function

Rule that assigns $\phi(0)$ to the function $\phi(x)$ 

#### Convergence of Distributions 

##### Distributional Convergence// Converge Weakly

A sequence $(f_n)_n$ of distributions is said to **<u>converge (weakly)</u>** to $f$ if
$$
\begin{align} 
\forall \phi \in \mathscr{D} && (f_n, \phi) \rightarrow (f, \phi) && \text{as }n \rightarrow \infin
\end{align}
$$

* This can mean either a function like $S(x, t) \rightarrow \delta(x)$ as $ t\rightarrow 0$ 
* Or $K_N(\theta) \rightarrow 2 \pi \delta$ as $N \rightarrow \infin$ 

#### Derivative of a Distribution

* Always exists

**Definition**: Derivative of a distribution 

Let $f$ be a distribution. Then we define its derivative $f'$ as the distribution
$$
(f', \phi) = -(f, \phi')
$$
If $f_N \rightarrow f$ weakly, then $f'_N \rightarrow f'$ weakly 

$(f_N', \phi)$ $= -(f_N, \phi')$ $ \rightarrow -(f, \phi')$ $ = (f', \phi)$ 

#### Distribution in an open Interval

Same definitions as earlier, but test functions vanish outside a closed interval $\sub I$ 

Continuity: valid for sequence $\{ \phi_n \}$ that vanish outside a common closed interval $\sub I$ 

#### Distributions in Three Dimensions 

* Instead of intervals, we talk about Balls
* $\delta$ here is $\phi \mapsto \phi(\bold 0)$ 
  * Partial Derivative $\partial \delta/\partial z$ is the functional that maps $\phi$ to $-\partial \phi/\partial x(\bold 0)$ 

##### Integrable Function

$f$ ordinary integrable is considered to be the same as

$\phi \mapsto \int_{-\infin}^\infin \int_{-\infin}^\infin \int_{-\infin}^\infin f(\bold x) \phi(\bold x)\, d \bold x$ 

#### Exercises

##### 12.1.1

Verify $\int_{-\infin}^\infin f(x)\phi(x)\, dx$ is a distribution

Because of the $\int_{-\infin}^\infin$ part I was confused: **use that test functions** dissappear outside of some interval: so we let $[-M, M]$ be the interval that $\phi_n$'s disspear outside of: 

$|\int_{-\infin}^\infin f(x)[\phi_n-\phi]\, dx| = |\int_{-M}^M f(x)[\phi_n- \phi]dx| $ $\leq \int_{-M}^M |f(x)|^2\, dx \int_{-M}^M |\phi_n - \phi|^2dx$

---

if $f$ continuous at $x = a$, then $|f|$ is continuous at $a$: 

What is true: for all $\epsilon > 0$, there exists $\delta_\epsilon$ s.t. $x \in (a - \delta_\epsilon, a + \delta_\epsilon)$ satisfies $|f(x) - f(a)| <\epsilon$ 
want to show: for all $\epsilon > 0$, there exists $\delta_\epsilon$ s.t. $x \in (a - \delta_\epsilon, a + \delta_\epsilon)$ satisfies $||f(x)| - |f(a)|| < \epsilon$ 

Triangle Inequality: $|a + b| \leq |a| + |b|$ $\implies$ $|a| \leq |a-b| + |b|$ $\implies$ $|a| - |b| \leq | a - b|$ 
$|b| \leq |b - a| + |a| $ $\implies $ $|b| - |a| \leq |a- b|$ , so $||a| - |b|| = ||b| - |a|| \leq |a-b|$ 

so $||f(x)| - |f(a)|| \leq |f(x) - f(a)| < \epsilon$ 

if $f$ is continuous at $x = a$, then $f^2$ is continuous at $a$: 

$|f(x)^2 - f(a)^2| < \epsilon$?  $|f(x) - f(a)||f(x) + f(a)|$ , since $f$ is continuous at $a$, we have that we can choose an interval around $a$ s.t. $f$ is bounded: on $(a - \delta_1, a + \delta_1)$ 

so let $|f(x) + f(a)| \leq M$, since both $f(a), f(x)$ are both finite over $(a - \delta_1, a + \delta_1)$ 

there's a $\delta_2$ s.t. for $|x - a| <\delta_2$,  $|f(x) - f(a)| < \frac{\epsilon}{M}$  

let $\delta = \min(\delta_1, \delta_2)$ $\implies$ for $x \in (a - \delta, a + \delta)$,  $|f(x)-f(a)||f(x) + f(a)|$ $< \frac{\epsilon}{M}M = \epsilon$ 

so $f$ is continuous at $a$ means $|f|$ is continuous at $a$ which also means $|f|^2$ is continuous at $a$ 

so $|f|^2$ discontinuities are a subset of $f$'s discontinuities

---

so $|f|^2$ is integrable $\implies$ $\int_{-M}^M |f|^2\, dx < M_f$

so it's easy to use $\int_{-M}^M|\phi_n - \phi|^2 \, dx \leq 2M \max|\phi_n - \phi|^2$ , and find $N$ large enough so that $|\phi_n - \phi|^2 < \frac{\epsilon}{2M M_f}$ 

##### 12.1.2

Verify: if $f$ a distribution $\implies$ $f'$, $(f', \phi) = -(f, \phi')$ is a distribution
$(f', a\phi + b \psi) $ $ = -(f, a \phi' + b \psi ')$ $ = -a(f, \phi') - b(f, \psi')$ $= a(f', \phi) + b(f', \psi)$ 
**Note**: Distributions are continuous! so use this

$(f, \phi_n') \rightarrow (f, \phi')$ 

we have that since $\phi_n \rightarrow \phi$, with our notion of convergence, this implies $\phi_n' \rightarrow \phi'$ and since $f$ is continuous, this means $(f, \phi_n') \rightarrow (f, \phi')$ 

##### 12.1.3: A little confusing

Verify that the derivative is a linear operator on the vector space of distributions

So vector space means that if $f, g$ are distributions and $a, b$ are constants: $af + bg$ is a distribution, in the sense that $(f, \phi) + (g, \phi) = (f+g, \phi)$ 

so we want: $((af + bg)', \phi)$ $=a(f', \phi) + b(g', \phi)$ 
$((af + bg)', \phi) = -(af + bg, \phi')$ $ = -a(f, \phi') - b(g, \phi')$ $=a(f', \phi) + b(g', \phi)$ 

##### 12.1.4

Denoting $p(x) = x^+$, show that $p' = H$, and $p'' = \delta$ 

$(p', \phi) = -(p, \phi') = - \int p(x) \phi'(x)\, dx = $ $- \int_0^\infin x \phi'(x) \, dx$ $ = \int_0^\infin \phi(x)\, dx$ (by IBP)
$ = \int_{-\infin}^\infin H(x)\phi(x)\, dx$ $= (H, \phi)$ 

$(p'', \phi) = (H', \phi) = -(H, \phi')$ $= - \int_0^\infin \phi'(x)\, dx$ $= -[\phi|_0^\infin - \int_0^\infin 0\cdot \phi(x)\, dx] = -\phi(\infin) + \phi(0) = \phi(0)$ $ = (\delta, \phi)$ 

##### 12.1.5: Wave Equation Weak Solution, Distributions MultiVariable

for simplicity: $c = 1$ for the Wave Equation

$(u_{tt} - u_{xx}, \phi) = (u, \phi_{tt} - \phi_{xx})$ $ = \int H(x-t)(\phi_{tt} - \phi_{xx})\, dx \, dt $ 

Noting: $(u_{tt}, \phi) - (u_{xx}, \phi)$ $=-(u_t, \phi_t) + (u_x, \phi_x)$ $= (u, \phi_{tt}) - (u, \phi_{xx})$ $=(u, \phi_{tt} - \phi_{xx})$ 
Continuing: $\int_{-\infin}^\infin \int_t^\infin \phi_{tt} - \phi_{xx}\, dx \, dt$  

$\phi_{xx}$ Term: $- \int_{-\infin}^\infin  \int_{t}^\infin \phi_{xx} \, dx \, dt$ $ = - \int_{_\infin}^\infin [\phi_x(x, t) |_t^\infin ] \, dt$ $= \int_{-\infin}^\infin \phi_x(t, t) \, dt$ 
$\phi_{tt}$ Term: $\int_{-\infin}^\infin \int_{-\infin}^x \phi_{tt} \, dt \, dx$ $ = \int_{-\infin}^\infin \phi_t(x, x) \, dx$ 

$ = \int_{-\infin}^\infin \phi_x(s, s) + \phi_t(s, s) \, ds$ 

so $x(s) = s, t(s) = s$, so $dx/ds = 1 = dt/ds$ 

$\frac{d\phi}{d s}$ $ = \phi_x \frac{dx}{ds} + \phi_t \frac{dt}{ds}$ $ = \phi_x + \phi_t$ 

Continuing: $= \int_{-\infin}^\infin \frac{d}{ds}\phi(s, s) \, ds = \phi(\infin, \infin) - \phi(-\infin, -\infin) = 0 - 0 = 0$ 

##### 12.1.6

##### 12.1.7

##### 12.1.8

##### 12.1.9

##### 12.1.11

##### 12.1.12

### 12.2 Green's Functions, Revisited

#### Laplace Operator

Recall: $\frac{1}{r}$ harmonic in **Three** dimensions ($\log r$ in **two** )

so $\Delta_3u = u_{rr} + \frac{2}{r}u_r$ and $\frac{2}{r^3} - \frac{2}{r}\frac{1}{r^2} = 0$ 

$\Delta_2 u = u_{rr} + \frac{1}{r}u_r$ $\implies$ $\Delta_2 (\log r) =  -\frac{1}{r^2} + \frac{1}{r}\frac{1}{r} = 0$ 

---

For $\phi \in \mathscr{D}$ we have seen:

$\phi(\bold 0) = -\int \frac{1}{r}\Delta \phi(\bold x)\frac{d \bold x}{4 \pi }$ 

$(\Delta(-\frac{1}{4\pi r}), \phi(\bold x))$ DOES $ = (-\frac{1}{4 \pi r}, \phi(\bold x))$ because $(u_{xx}, \phi) = (u, \phi_{xx})$

So combining the two: $\Delta(- \frac{1}{4 \pi r}) = \delta(\bold x)$ in **three** dimensions

---

##### Dirichlet Problem

$$
\begin{align}
\Delta u &= f  &\text{in }D
\\ u &= 0 & \text{on }\partial D
\end{align}
$$

Then using 1. the solution of the Dirichlet Problem in terms of Green's function 2. assuming $u(\bold x)$ is a test function
$$
u(\bold x_0) = \int_D G(\bold x, \bold x_0)f(\bold x)\, d \bold x
\\ = \int_D \Delta G(\bold x, \bold x_0)u(\bold x)\, d \bold x
$$
becuase $\Delta u = f$, and $(G, \Delta u) = (\Delta G, u)$ (Where $\Delta G$ is understood in the sense of distributions)

and $u(\bold x_0) = \int \delta (\bold x - \bold x_0) u(\bold x) \, d \bold x$ on the left hand side so:
$$
\Delta G(\bold x, \bold x_0) = \delta(\bold x - \bold x_0) \qquad \text{in }D
$$

#### Diffusion Equation

Source Function $S(\bold x, t)$ satisfies
$$
\begin{align}
S_t - k \Delta S &= 0 & \bold x \in \R^3, 0<t<\infin
\\ S(\bold x, 0) = \delta(\bold x)
\end{align}
$$
Let $R(x, t):= S(x-x_0, t-t_0)$, $t > t_0$ and put
$R(x, t) := 0$ for $t < t_0$ $\implies$
$$
R_t - k \Delta R = \delta(x-x_0)\delta(t-t_0)
$$

---

$(R_t - k \Delta R, \phi)$ $= (R, -\phi_t - k \Delta \phi)$, let $k = 1$ for simplicity and assume $(x_0, t_0) = (0, 0)$ since derivatives commute with translation by $(x_0, t_0)$ 
$ = -\int R(\phi_t + \Delta \phi)\, dt \, dx$ $ = -\lim_{\epsilon \rightarrow 0} \int_{\R_t\setminus[-\epsilon, \epsilon] \times \R_x} R(\phi_t + \Delta \phi)$

$- \lim_{\epsilon \rightarrow 0} [\int_{-\infin}^{-\epsilon} \int_{-\infin}^\infin R(\phi_t + \Delta \phi)\, dx \, dt +  \int_{\epsilon}^\infin  \int_{-\infin}^\infin R(\phi_t + \Delta \phi)\, dx \,dt$ 
$=-\lim_{\epsilon \rightarrow 0}[ \int_{-\infin}^{-\epsilon} $ $\int_{-\infin}^\infin \Delta R \phi \, dx  dt$ $- \int_{-\infin}^\infin \int_{-\infin}^{-\epsilon} R_t \phi \, dt \, dx$ $ + \int_{\epsilon}^\infin \int_{-\infin}^\infin \Delta R \phi \, dx \, dt - $ $\int_{-\infin}^\infin R(\epsilon, x)\phi(\epsilon, x)\, dx$ $- \int_{-\infin}^\infin \int_{\epsilon}^\infin R_t\phi dt\, dx]$

This was accomplished using integration by parts and the fact that $\phi$ dissapears at infinity and $R$ disappears for $t < 0$ , so $t = -\epsilon$ , $R(-\epsilon, x) = 0$ 

Now we have $\lim_{\epsilon \rightarrow 0} \iint_{\R_t \setminus[-\epsilon, \epsilon]\times \R_x} (R_t - \Delta R)\phi \, dt \, dx + \int_{-\infin}^\infin R(\epsilon, x)\phi(\epsilon, x)\, dx$ 

**Important**: this first term disappears, since we are in the domain where $R(x, t) = S(x, t)$ is differentiable, and thus $R_t \Delta R = S_t k \Delta S = 0$ 

now $\lim_{\epsilon \rightarrow 0} \int_{-\infin}^\infin R(\epsilon, x)\phi(\epsilon, x) \, dx$ and since $R(t, x) = S(t, x)$, which for $t = \epsilon$ small is concentrated almost entirely near $x = 0$ with area $1$, so we may treat $\phi(\epsilon, x) \approx \phi(\epsilon, 0)$  as a constant: 

$= \lim_{\epsilon \rightarrow 0} \phi(\epsilon, 0)\int_{-\infin}^\infin R(\epsilon, x)\, dx$ $ = \phi(0, 0)$ $= (\delta_0(t)\delta_0(x), \phi)$ 

Or alternatively: for $\epsilon \rightarrow 0$, we have from in a previous section $\int_{-\infin}^\infin S(\epsilon, x)\phi(\epsilon, x)\, dx \rightarrow \phi(0, 0)$ 

---

#### Wave Equation

Source function for the wave equation satisfies
$$
\begin{align}
S_{tt} - c^2 \Delta S &= 0 && \bold x \in \R^3, -\infin < t < \infin
\\ S(\bold x, 0) &= 0
\\ S_t(\bold x, 0) &= \delta(\bold x)
\end{align}
$$
and is called the **Riemann Function**

Finding a formula: Let $\psi \in \mathscr D$ 
$$
u(\bold x, t) = \int S(\bold x - \bold y, t)\psi(\bold y)\, d \bold y
$$
$u$ satisfies wave eq. w/ $u = 0, u_t = \psi $ at $t = 0$ 

##### One Dimension

From what we know about this problem, we have:
$$
\int_{-\infin}^\infin S(x-y, t)\psi(y)\, dy = \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(y)\, dy \quad t \geq 0
$$
$\implies$ $S(x, t) = \begin{cases} \frac{1}{2c} & |x| < ct \\ 0 & |x| > ct \end{cases}$ 

So
$$
S(x, t) = \frac{1}{2c}H(c^2t^2 - x^2) \text{sgn}(t) \qquad c|t| \neq x
$$
has a jump discontinuity along $c|t| = x$ (exaple of the propogation of singularities)

the $\text{sgn}$ term allows us to permit $-\infin < t <\infin$ : $- \frac{1}{2c}\int_{x + c|t|}^{x - c|t|} \psi(y)\, dy $ when $t \leq 0$ 

##### Three Dimensions: Skipped in Lecture

#### Skipped Boundary And Initial Conditions

#### Exercises

Important you use distributional derivatives!

##### Exercise 4

Find PDE $S_t$ satisfies, where $S(t, x) = 1/(2c)$ for $|x| <ct$ and $0$ otherwise 

$(S_t, \phi) = -(S, \phi_t)$ $= - \iint S \phi_t \, dx \, dt$ $ = - \frac{1}{2c}\int_0^\infin \int_{x-ct}^{x+ct} \phi_t \, dx \, dt$ $\frac{1}{2c} \int_{-\infin}^0 \int_{x-ct}^{x+ct} \phi_t \, dx \, dt$ 

and if we switch integration to go from $t = 0$ to $\infin$ in the second integral we get $\int_{x+ct}^{x-ct}$ $\rightarrow $ $- \int_{x-ct}^{x+ct}$ : so we end up with

$ = - \frac{1}{c}\int_0^\infin \int_{x-ct}^{x+ct} \phi_t(t, x)\, dx\, dt$ 

**Rewrite the domain of Integration** : switch $dt $ and $dx$ 

$x$ can technically range from $-\infin$ to $\infin$ as $t \rightarrow \infin$, and we have $-ct < x < ct$ 

so $|x| < ct$ $\implies$ $t > \frac{|x|}{c}$ , and we need to rememember we are originally integrating from $t = 0$ to $\infin$ so in this new case, we are integrating fr

$-\int_{-\infin}^\infin( \int_{|x|/c}^{\infin} \phi_t(t, x)\, dt )\, dx$  $= \int_{-\infin}^\infin \phi(|x|/c, x)\, dx$ 

##### Exercise 5

##### Exercise 6

### 12. 3 Fourier Transforms

$(-l, l) \rightarrow $ Fourier Series

$(-\infin, \infin)$ $\rightarrow $ Fourier Transform

#### Motivation

$f(x)$ and for all $0 < l < \infin$: $f(x) = \sum_{n = -\infin}^\infin c_n e^{\frac{i n \pi x}{l}}$ , $x \in (-l, l)$ 
$c_n = \frac{1}{2l} \int_{-l}^l f(y)e^{- i n \pi y/l}\, dy$ 

Then let $l \rightarrow \infin$ , so $f(x) = \frac{1}{2 \pi} \sum_{n = -\infin}^\infin (\int_{-l}^l f(y)e^{-i k_n y}\, dy)e^{i k_n y}\frac{ \pi }{l}$, where $k_n = \frac{n \pi}{l}$ $\underset{l \rightarrow \infin}{"\rightarrow"} \frac{1}{2\pi} \int_{-\infin}^\infin (\int_{-\infin}^\infin f(y)e^{-iky}\, dy)e^{iky}dk$, where $dk \sim \Delta k = k_n - k_{n-1} = \frac{\pi}{l}$ 

* Book: $k = n\pi/l$, letting $l \rightarrow \infin$, the poitns $k$ get closer together, and $k$ becomes a continuous variable, and the sum becomes an integral

So we have $f(x) = \frac{1}{2\pi}\int_{_\infin}^\infin [\int_{-\infin}^\infin f(y)e^{-iky}\, dy]e^{ikx}\, dk$ 

Another way to state this:


$$
f(x) = \int_{-\infin}^\infin F(k)e^{ikx}\frac{dk}{2\pi}
$$

##### Fourier Transform

$$
F(k) = \int_{-\infin}^\infin f(y)e^{-ikx}\, dx
$$

$k$: Frequency Variable

Notation in other txtbooks:
$$
F(k) = \hat f(k) = \mathscr F(f)(k)
\\ \hat f(k)= \frac{1}{\sqrt{2 \pi}}\int_{-\infin}^\infin f(x)e^{-ikx}\, dx
$$

### List of Important Transforms

|                           | $f(x)$        | $F(k)$                                                       |
| ------------------------- | ------------- | ------------------------------------------------------------ |
| **$\delta$ Distribution** | $\delta(x)$   | 1                                                            |
| Square Pulse              | $H(a - |x|)$  | $\frac{2}{k}\sin(ak)$                                        |
| Exonential                | $e^{-a |x|}$  | $\frac{2a}{a^2 + k^2}$ (a >0)                                |
| Heaviside Function        | $H(x)$        | $\pi \delta(k) + \frac{1}{ik}$                               |
| Sign function             | $H(x)- H(-x)$ | $\frac{2}{ik}$                                               |
| Constant                  | 1             | $2 \pi \delta(k)$ (this explains the $\pi \delta(k)$ term in the Heaviside fct when $k = 0$) |
| Gaussian                  | $e^{-x^2/2}$  | $\sqrt{2 \pi }e^{-k^2/2}$                                    |

---

Further properties

| Function           | Transform                                  |
| ------------------ | ------------------------------------------ |
| $\frac{d}{dx}f(x)$ | $ik F(k)$                                  |
| $xf(x)$            | $i \frac{dF}{dk}$                          |
| $f(x-a)$           | $e^{-iak}F(k)$                             |
| $e^{iax}f(x)$      | $F(k-a)$                                   |
| $a f(x)+ bg(x)$    | $aF(k) + b G(k)$                           |
| $f(ax)$            | $\frac{1}{|a|}F(\frac{k}{a})$ $(a \neq 0)$ |

---

#### Placherel's Theorem: Analogue of Praseval's Identity

#### 

$$
\int_{-\infin}^\infin |f(x)|^2 \, dx = \int_{-\infin}^\infin |F(k)|^2 \frac{dk}{2 \pi}
$$

and
$$
\int_{-\infin}^\infin f(x)\overline g(x)\, dx = \int_{-\infin}^\infin F(k)\overline{G(k)}\frac{dk}{2\pi}
$$

#### Convolution

Given two functions $f, g$, their convolution:
$$
(f*g)(x) = \int_{-\infin}^\infin f(x-y)g(y)\, dy
$$
Source function is an example of a convolution, i.e., the solution the the one dimensional diffusion eq. is $(S*\phi)(x) = \int_{-\infin}^\infin S(x-y, t)\phi(y)\, dy$ 

##### Fourier Transform of Convolution is the Product

$\widehat{f*g} = \int (f*g)(x) e^{-ikx}\, dx = F(k)G(k)$ Proof involves using formula above, rearranging, change of variables

#### Higher Dimensions

$F(\bold k) = \int_{\R^d}f(\bold x)e^{-i \bold k\cdot \bold x}\, d \bold x$ , e.g. $\iint f(x, y)e^{-i(k_1 x + k_2 y)}\, d x\, dy$ 

and

$f(\bold x) = \int_{\R^d} F(\bold k)e^{i \bold k \cdot \bold x}\frac{d \bold k}{(2 \pi)^d}$ , e.g. $\iint F(x, y)e^{i(k_1x + k_2 y)}\frac{dx}{2\pi}\frac{dy}{2\pi}$ 

### 12. 5 Laplace Transform

Given $f = f(t)$, $t > 0$ Define the Laplace transform as
$$
F(s) := \int_0^\infin f(t)e^{-st}\, dt
$$
Difference: potential is **real** and variable is **time** instead of $k$ 

##### Notation

$F = Lf = \mathscr{L}f$ 

#### Laplace Transforms

| $f(t)$                                  | $F(s)$                              |
| --------------------------------------- | ----------------------------------- |
| 1                                       | $\frac{1}{s}$                       |
| $e^{at}$                                | $\frac{1}{s-a}$                     |
| $\cos (wt)$                             | $\frac{s}{s^2 + w^2}$               |
| $\sin (w t)$                            | $\frac{w}{s^2 + w^2}$               |
| $\cosh(at)$                             | $\frac{s}{s^2 - a^2}$               |
| $\sinh(at)$                             | $\frac{a}{s^2-a^2}$                 |
| $t^k$                                   | $\frac{k!}{s^{k+1}}$                |
| $H(t-b)$                                | $\frac{1}{s}e^{-bs}$                |
| $\delta(t-b)$                           | $e^{-bs}$                           |
| $a(4\pi t^3)^{-1/2}e^{-a^2/4t}$         | $e^{-a\sqrt{s}}$                    |
| $(\pi t)^{-1/2}e^{-a^2/4t}$             | $\frac{1}{\sqrt{s}}e^{-a \sqrt{s}}$ |
| $1 - \mathscr{Erf} \frac{a}{\sqrt{4t}}$ | $\frac{1}{s}e^{-a \sqrt{s}}$        |

#### Properties

|                     | Function                   | Transform                   |
| ------------------- | -------------------------- | --------------------------- |
| 1.  (linear)        | $af(t) + bg(t)$            | $aF(s) + bG(s)$             |
| 2. (1st derivative) | $\frac{df}{dt}$            | $sF(s) - f(0)$              |
| 3. (2nd derivative) | $\frac{d^2 f}{dt^2}$       | $s^2F(s) - sf(0) - f'(0)$   |
| 4.                  | $e^{bt}f(t)$               | $F(s-b)$                    |
| 5. dividing by $t$  | $\frac{f(t)}{t}$           | $\int_s^\infin F(s')\, ds'$ |
| 6. multiplying by t | $tf(t)$                    | $- \frac{dF}{ds}$           |
| 7.                  | $H(t-b)f(t-b)$             | $e^{-bs}F(s)$               |
| 8.                  | $f(ct)$                    | $\frac{1}{c}F(\frac{s}{c})$ |
| 9.                  | $\int_0^t g(t-t')f(t')dt'$ | $F(s)G(s)$                  |

#### Inverse Laplace Transform

$f(t) = \frac{1}{2 \pi i}\lim_{R \rightarrow \infin}$ $\int_{L_R}e^{st}F(s)\, ds$, requires Complex Analysis 

### 12.4 Source Functions

#### Diffusion: Fourier

##### Start With Source Function PDE

Source Function defined as unique solution of $S_t = S_{xx}$, $x \in (-\infin, \infin), t \in (0, \infin)$ w/ $S(x, 0) = \delta(x)$ and $k = 1$ 

##### Assume Source Function has a Fourier Transform

Assume Source FUnction has a Fourier Transform as a distribution in $x$ for each $t$: 

$\hat S(k, t) = \int_{-\infin}^\infin S(x, t)e^{-ikx} \, dx$  , Warning: $k$ is the frequency varaible!

##### Use Properties of Fourier Transforms to ReWrite PDE

Interested in $S_t(x, t)$, which has transform $\hat S_t$ , and from our PDE, $S_t = \hat S_t = \hat S_{xx} = (\hat S_x)_x = (ik \hat S)_x = (ik)^2 \hat S$ $ = -k^2 \hat S$ , $\hat S(k, 0) = \hat \delta(k) = 1$

##### Obtain ODE and Solve

For each $k$ this is an ODE in variable $t$: $\hat S_k'(t) = -k^2 \hat S_ k(t)$ $\implies$ $\hat S(k, t) = e^{-k^2 t}$ 

##### Use Table Above to find Function

The function associated with transform $e^{-k^2/2}$ is $e^{-x^2/2}/\sqrt{2 \pi}$ 

Issue: multiply $k^2$ by $2t$ , need to use $f(ax) = \frac{1}{|a|}F(\frac{k}{a})$ , we try to find $|a|f(ax) = F(\frac{k}{a})$ $ = e^{-k^2 /(2a^2)} = e^{-k^2t}$
so $(k/a)^2/2 = k^2 t$ $\implies$ $a^{-2} = 2t$ $\implies$ $a = (2t)^{-1/2}$ 

so $(1/\sqrt{2\pi})f((2t)^{-1/2}x) = \frac{1}{\sqrt{4 \pi t}} e^{-x^2/4t}$ 

#### Diffusion: LaPlace

#### READ THROUGH THE REST OF THIS ON LIKE SUNDAY OR SOMETHING

## Chapter 11: General Eigenvalues

### 11.1 The Eigenvalues are Minima of the Potential Energy

#### Eigenvalue Problem for LaPlace Operator on $D$, domain in $\R^d$ 

$$
-\Delta = \lambda u \quad \text{in }D
\\ u = 0 \quad \text{ on }\partial D
$$

All eigenvalues are non-negative and real: $0 \leq \lambda_1 \leq \lambda_2\ldots$ 

Recall definitions of **inner product** and **norm** from Chapter 5

#### Minimization Problem

$$
(MP) \qquad m = \min\{ \frac{\|\nabla w\|^2}{\| w \|^2}: w = 0 \text{ on } \partial D, w \neq 0\}
$$

##### Rayleigh Quotient: $\frac{\| \nabla w\|^2}{\| w \|^2}$ 

##### Solution:

$$
u \in C^2, u = 0 \in \partial D, u \neq 0: 
\\ \frac{\|\nabla u\|^2}{\|u\|^2} \leq \frac{\| \nabla w \|^2}{\| w \|^2}
$$

**Remark**: if $u$ is a solution of (MP), so is $Au$, for any number $A \neq 0$ 

#### Variational Principle for the 1st Eigenvalue

$u$ solution to (MP) $\implies$ $\lambda_1 = \frac{\| \nabla u \|^2}{\| u\|^2} = \text{min}\{  \frac{\| \nabla w\|^2}{\| w \|^2}: w = 0 \text{ on }\partial D, w \not \equiv 0 \}$ 

and $-\Delta u = \lambda_1 u$ in $D$, $u = 0$ on $\partial D$ 

* First eigenvalue $\lambda_1$ is the minimum of the energy
* $u$ is the "ground state"

##### Proof

1. Assume $u$ is the solution to (MP)

2. Show that $m$, the minimum in (MP) is an eigenvalue

   1. Define an admissible function $w_{\epsilon} := u + \epsilon v$ , $v = 0$ on $\partial D$ 
   2. $f(\epsilon) = \frac{\| \nabla w_\epsilon\|^2}{\| w_\epsilon \|^2}$ , use that this is at a minimum when $\epsilon = 0$ and calculate $f'(\epsilon)$ 
   3. use Green's First Identity and boundary conditions
   4. substitue in $m$ somewhere

3. Show $m$ is the smallest eigenvalue: easy

   1. let $\lambda$ be any eigenvalue for some function $v$ $\implies$ $v \in C^2$, $v = 0$ on $\partial D$ 
   2. $m \overset{\text{(MP)}}{\leq} \frac{\| \nabla v\|^2}{\| v \|^2} = \frac{\int |\nabla v|^2}{\int v^2} = \frac{\int (-\Delta v)v}{\int v^2} = \frac{\int \lambda v^2}{\int v^2} = \lambda$ 

   #### Vartiational Principle for the $n$th Eigenvalue

   Given the first (n-1) eigenvalues $\lambda_i$ and their eigenfunctions $v_i$, $i = 1, \ldots, n-1$ we have
   $$
   (MP)_n \quad \lambda_n = \min\{ \frac{\|\nabla w\|^2}{\| w\|^2}: w \in C^2, w = 0 \text{ on }\partial D \not \equiv 0, 0 = (w, v_1) = \cdots = (w, v_{n-1}\}
   $$

   * $w$ is continuous at least up to 2nd derivatives
   * homogeneous Dirichlet BC
   * orthogonal to first $n-1$ eigenfunctions

##### Proof

1. Can show $\int (\Delta u + m^* u)v = 0$ , where $v$ satisfies the same constraints as above
2. because $(u, v_j) = 0$, $\int (\Delta u + m^* u)v_j = 0$ 
   1. use (G2) and boundary conditions
3. $m^*$ is an eigenvalue
   1. let $h$ be a $C^2$ function with homogeneous BC, not 0
   2. $v(\bold x) := h(\bold x) - \sum_{k = 1}^{n-1}c_k v_k(\bold x)$, $c_k = (h, v_k)/(v_k v_k)$ 
   3. $(v, v_j) = 0$ for  $j = 1, 2, \ldots, n-1$ 
   4. Use 1. and 2. to show for any $h$: $h = v +\sum_{k= 1}^{n-1}c_k v_k$, $\int (\Delta u + m^* u)h = \int (\Delta u + m^* u)v - \sum_{k = 1}^{n-1} \int (\Delta u + m^* u)v_k = 0+ 0 = 0 $
   5. $m^*$ $= \lambda_N$ : See Homework for proof
      1. Induction: Base Case is Theorem 1
      2. IH: $\lambda_i$ for $i < n$ may be expressed as solution to MP$_i$, evident $m^* \geq \lambda_i$ 
         1. smaller family of functions we are minimizing over
      3. Show any eigenvalue $\lambda$ other than the first $n-1$ are larger than $m^*$ 
         1. Need to show the corresponding eigenfunction $v$ is a valid input for the (MP)$_n$ 
            1. obviously $v \in C^2$, $v = 0$ on $\partial D$ since itis an eigenfunction
            2. need to show $(v, v_i) = 0$: $\lambda(v, v_i) $ $= (\lambda v, v_i)$ $= (-\Delta v, v_i)$ $=(v, -\Delta v_i) $ $= \lambda_i (v, v_i)$ so either $(\lambda - \lambda_i) = 0$, which is a contradiction, or $(v, v_i)=0$ 
         2. after using 1., use $m \leq \frac{\| \nabla v\|^2}{\|v\|^2}$ $ = \frac{- \int v\Delta v }{\int v^2}$ $ = \frac{\lambda \int v^2}{\int v^2}$ $= \lambda$ 

### 11.2 Computation of Eigenvalues

trial function: nonzero $w$ vanishing on the boundary

#### Rayleigh-Ritz Approximation

$w_1, \ldots, w_n$ be $n$ arbitrary trial functions $C^2$ functions that **vanish on **$\partial D$. Let 
$$
a_{jk} = (\nabla w_j, \nabla w_k) = \iiint_{D}\nabla w_j \cdot \nabla w_k \, d \bold x
$$
and
$$
b_{jk} = (w_j, w_k) = \iiint_D w_j \cdot w_k \, d \bold x
$$
Let $A$ be the $n \times n$ symmetric matrix $(a_{jk})$ and $B$ the $n \times n$ symmetric matrix $(b_{jk})$. Then *the roots of the polynomial equation* 
$$
\det(A - \lambda B) = 0
$$
are approximations to the first $n$ eigenvalues $\lambda_1, \ldots \lambda_n$ 

##### See Bessel Functions to find Roots

#### Minimax Principle

$$
(9) \qquad \lambda_n^* = \max \{ \frac{\| \nabla w\|^2}{\|w\|^2}, \text{ taken over all functions } w \text{ that are linear combinations of } w_1, \ldots, w_n\}
$$

Formula $(9)$ leads to the minimax principle, which states *the smallest possible value of* $\lambda_n^*$ *is the $n\text{th}$ eigenvalue* $\lambda_n$. Thus $\lambda_n$ is a minimum of a maximum 

##### Theorem

If $w_1, w_2, \ldots w_n$ is an arbitrary set of trial functions, define $\lambda_n^*$ by formula (9). Then the $nth$ eigenvalue is
$$
\lambda_n = \min \lambda_n^*
$$
where the minimum is taken over all possible choice of $n$ trial functions $w_1, \ldots, w_n$ 

###### Proof

$\leq $ : using that $\lambda_n$ will be the smallest value by $(MP)_n$ and that in general $\lambda_n^*$ is large

1. Fix any Choice of $n$ trial functions $w_1, \ldots, w_n$ 
2. choose $n$ constants, $c_1, \ldots, c_n$ (not all zero) so that the linear combination $w(\bold x) = \sum_{k=1}^n c_k w_k(\bold x)$ is orthogonal to the first (n-1) eigenfcts. $v_i$ $i = 1, \ldots, n-1$ 
3. Obtain: $\lambda_n \leq \frac{\| \nabla w\|^2}{\| w\|^2} \leq \lambda_n^*$ 
   1. $\lambda_n \leq \cdot$ because $w$ is admissible for $(MP)_n$ 
   2. $\cdot \leq \lambda_n^*$ by definition
4. This is the case for any choice of $w_1, \ldots, w_n$, so $\lambda_n \leq \min_{w_1, \ldots, w_n} \lambda_n^*$ 

$\lambda_n \leq \min_{w_1, \ldots, w_n} (\max_{(c_1, \ldots, c_n)} \frac{\| \nabla(\sum_k c_k w_k)\|^2}{\| \sum_k c_k w_k\|^2})$ 

$\geq$: using that $\lambda_j \leq \lambda_n$ for $j = 1, 2, \ldots n$ 

1. $w_j = v_j, j = 1, \ldots, n$ , WLOG choose $\| v_j \|^2 = 1$ Then: 
2. $\lambda_n^* = \max \frac{\| \nabla (\sum c_j v_j)\|^2}{\| \sum c_j v_j\|^2}$ $\underset{(G1)}{=}$ $\frac{(-\Delta(\sum c_j v_j), \sum c_j v_j)}{\sum c_j^2}$ $ = \frac{(\sum c_j \lambda_jv_j, \sum c_j v_j)}{\sum c_j^2}$ $ = \frac{\sum c_j^2 \lambda_j}{\sum c_j^2}$ $ \leq \frac{\sum c_j^2 \lambda_n}{\sum c_j^2} = \lambda_n$ 

### 11.3 Completeness

#### Theorem: Neumann BC 

* trial function: any $C^2$ function $w(\bold x)$ s.t. $w \not \equiv 0$ 
* With this def, the preceding characterization of eigenvalues still holds
  * MP, MP$_n$ , Rayleigh-Ritz Approximation, and minimax principle

#### Theorem: Eigenfunctions are Complete in $L^2$ sense 

This means the following: let $0 \leq \lambda_1 \leq \lambda_2 \leq \cdots$ so the eigenvalues of $-\Delta$ on $D$ with the homogeneous Dirichlet BCs and  $v_1, v_2, \ldots$ be associated eigenfunctions. Assume orthogonal: $(v_m, v_n) = 0$ if $m \neq n$ 

Let $f$ be $L^2 $ function: $\| f\| < \infin$ and let $c_n := \frac{(f, v_n)}{(v_n, v_n)}$ $\implies$ 
$$
\| f - \sum_{n=1}^N c_n v_n \|^2 = \int_D (f(\bold x) - \sum_{n=1}^N c_n v_n (\bold x))^2 \, d\bold x \underset{N \rightarrow \infin}\rightarrow 0
$$

##### Proof: 

Restrict to simpler case of $f$ admissible $f \in C^2, f = 0$ on $\partial D$ 

1. Orthogonality of the remainder $r_N(\bold x) := f(\bold x) - \sum_{n=1}^N c_n v_n(\bold x)$ so $(r_N, v_j) = 0$ 
   1. just plug in, use linearity of inner product and remember what $c_n$ is
2. Rayleigh Quotient
   1. $r_N$ is admissible for $(MP)_{N+1}$: $\lambda_{N+1} = \min\{ \frac{\| \nabla w\|^2}{\| w \|^2}\}: w \text{ admissible for (MP)}_n \} \leq \frac{\| \nabla r_N \|^2}{\| r_N \|^2}$ $\equiv$ $\| r_N\|^2 \leq \frac{\| \nabla r_N\|^2}{\lambda_{N+1}}$ 
3. A uniform bound on $\| \nabla r_N\|^2 \leq \| \nabla f\|^2$ 
   1. $\| \nabla r_N\|^2 = \|\nabla  f\|^2 - 2(\nabla f(\bold x), \sum_{n \leq N}c_n\nabla v_n) + \sum_{m, n}c_m c_n (\nabla v_n, \nabla v_m)$ 
   2. Using (G1) and orthognality: $ = \|\nabla f\|^2 - 2 \sum_{n}c_n(f, -\Delta v_n) +\sum_{n} c_n^2 \lambda_n (v_n, v_n)$ 
   3. $ = \| \nabla f\|^2 - 2 \sum_{n}c_n \lambda_n (f, v_n) + \sum_{n}c_n^2 \lambda_n(v_n, v_n)$ 
   4. $ = \| \nabla f\|^2 - 2\sum_n c^n \lambda_n (f, v_n) + \sum_n c_n^2 \lambda_n(v_n, v_n)$ $= \|\nabla f\|^2 - \sum_n\lambda_n c_n^2 \| v_n\|^2$ 
      1. $\lambda_n \geq 0$ 
   5. $\leq \| \nabla f\|^2$ 
4. Combine steps 2 and 3: $\| r_N \|^2 \leq \frac{\| \nabla r_N\|^2}{\lambda_{N+1}} \leq \frac{\| \nabla f\|^2}{\lambda_{N+1}}$ whcih as $N \rightarrow \infin$ tends to $0$ as $\|\nabla f\|^2$ is bounded. 

## Chapter 14: Nonlinear PDEs

### 14.1 Shock Waves

### 14.2 Solitons

### 14.3 Calculus of Variations
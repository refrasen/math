# Math 126 Lecture 1

Substitute: Craig Evans, 1033 Evans Hall, evans@math.berkeley.edu

## Notation

$x \in \R^d$, $x = (x_1, . . ., x_d)$ 

$f: \R^d \rightarrow \R$, $f = f(x) = f(x_1, . . ., x_d)$ 

$f_{x_i} = \frac{\partial f(x)}{\partial x_i}$ $ = \lim_{h \rightarrow 0}\frac{f(x_1, ..., x_i+h, ..., x_d) - f(x_1, ..., x_i, ..., x_d)}{h}$ 

* provided the limit exists

**Example:** $G(x, y, u, u_x, u_y)$ $= 0$, find unknown $u(x, y)$ 

##### <u>Def</u> Order of a PDE

is the order of the highest partial derivative that appears in it

#### Examples

1. $u_t + bu_x = 0​$
2. $u_t + uu_x = 0​$
3. $u_{xx} + u_{yy} = 0​$
4. $-(u_xx + u_yy) = \lambda u​$ 
5. $u_{tt} - u_{xx} + u^3 = 0​$ 
6. $u_t - u_{xx} = f(x, t)​$ 
7. $u_t - i u _{xx} = 0​$ 

| Example                          | Order | Linear | Name                    |
| -------------------------------- | ----- | ------ | ----------------------- |
| $u_t + bu_x = 0$                 | 1     | Yes    |                         |
| $u_t + uu_x = 0$                 | 1     | no     | Burgers' Equation       |
| $u_{xx} + u_{yy} = 0$            | 2     | yes    | LaPlace's Equation      |
| $-(u_{xx} + u_{yy}) = \lambda u$ | 2     | yes    |                         |
| $u_{tt} - u_{xx} + u^3 = 0$      | 2     | no     | Heat Equation           |
| $u_t - u_{xx} = f(x, t)$         | 2     | yes    | Diffusion/Heat equation |
| $u_t - i u _{xx} = 0$            | 2     | Yes    | Schrodinger's Equation  |

##### <u>Def</u> Operator, $L$ (normally, $\mathscr{L}$)

An operator $L$ (acting on functions) is called linear if:
$$
L(u+v) = L(u) + L(v)\\
L(cu) = cL(u)
$$

##### <u>Def</u> Linear PDE

1. A PDE is called linear if it has the form $Lu = 0$ 
2. If it has the form $Lu = g$, the PDE is linear, nonhomogeneous 
   1. $g \neq 0$ is a given function of the independent variables

**Advantages of linearity** 

1. if we have an equation $Lu = 0​$, and $u​$ and $v​$ are both solutions, so is $(u+v)​$ 

   if $u_1, . . ., u_n$ are all solutions, so is any linear combination

   $c_1 u_1 (x) + \cdots + c_n u_n(x) = \sum_{j=1}^n c_j u_j (x)$, $c_j = \text{ constants}$ 

   This is sometimes called the superposition principle

2. if you add a homogeneous solution to an inhomogeneous solution you get an inhomogeneous solution (Why?)

<u>From Class</u>

If we have $L(u) = f$, and we want to solve: $L(u_k) = f_k$, $L(u) = L(\sum_{k=1}^n c_k u_k) = \sum_{k=1}^n L(c_k u_k) = \sum_{k=1}^n c_k L(u_k) = \sum_{k=1}^n c_k f_k = f$ 

<u>Book Examples</u>

Note: Might be useful to quickly review Linear ODE's

1. $u_{xx} = 0$ 

   $u_x = f(y)$ 

   $u = f(y)x + g(y)$ 

2. $u_{xx} + u =0$ 

   $x^2 + 1 = 0$ $r_1 = i$, $r_2 = -i$ 

   $y = C_1 \cos (x) + C_2 \sin (x)$ 

   From Math 123:

   $x'' + x =0$ 

   Set $y = x'$ and we have a new system

   $x' = y \\ y' = -x$   $\implies$ $\begin{pmatrix} 0 & 1 \\ -1 & 0 \end{pmatrix}$

Only difference: $C_i$'s are functions of $y$ (this is only the case because we don't have any partial derivatives in terms of $y$, only partial derivatives w.r.t. to one independent variable: $x​$)

3. $u_{xy} = 0$

   "integrate" with respect to $y$, and we should get a function of only $x$

   $u_x = f(x)$ 

   "integrate" with re

   $u = F(x) + G(y)$ 

   where $F'(x) = f(x)$ 


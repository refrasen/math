# Math 126 Lecture 5

## Chapter 2: Waves and Diffusions

First, let's revist the method of characteristics
$$
(1,c)\cdot \nabla u = u_t + u_x = 0
$$
What does it tell us?

When we have $\vec v \cdot \nabla u = 0$ $\implies$ $\vec v \perp \nabla u$ 

![Sketch003](Math 126 Lecture 5.assets/Sketch003.jpg)

$\nabla u(t,x)$ is the normal vector to level lines $\{ (t,x): u(t,x) = u(t_0, x_0) \}$ 

$\vec v$ is the tangent of the level line. 

Want to reconstruct $u$ by finding level line. 

### The Wave Equation

"Forget about boundary conditions for now" "forget about initial condtions"
$$
(*)\qquad u_{tt} = c^2u_{xx} \qquad \text{for }-\infin < x < +\infin
$$
(imagine a very long string)

**simplest 2nd Order equation**: Why? We can factorize
$$
u_{tt} - c^2u_{xx} = (\frac{\partial }{\partial t}-c\frac{\partial}{\partial x})(\frac{\partial}{\partial t}+c\frac{\partial}{\partial x})u = 0
$$
Using: $a^2 - b^2 = (a-b)(a+b)$, where $a = \frac{\partial }{\partial t}$ and $b = c\frac{\partial }{\partial x}$ 

We let
$$
(\frac{\partial}{\partial t}+c\frac{\partial}{\partial x})u = v(x, t)
$$
then obtain
$$
u_{tt} - c^2u_{xx} = (\frac{\partial}{\partial t} -c\frac{\partial}{\partial x})v = v_t - cv_x = 0
$$
so Now, we obtain a system of first order PDEs
$$
(1) \qquad u_t + cu_x = v
\\(2) \qquad  v_t - cv_x = 0
$$
From Lecture 3, the general solution of $(2)$ is
$$
v(x,t) = h(x+ct)
$$
for any function $h: \R \rightarrow \R$ 

We now have
$$
u_t + cu_x = h(x+ct)
$$
Ansatz: $u(x,t) = f(x+ct)$ 
$$
u_t + cu_x = \frac{\partial f}{\partial (x + ct)}\frac{\partial (x+ct)}{\partial t} + c\frac{\partial f}{\partial (x+ct)}\frac{\partial (x+ct)}{x}
\\ = f'(x+ct)(c) + cf'(x+ct) = h(x+ct)
\\ \implies
\\ f'(x+ct) = \frac{h(x+ct)}{2c}
$$
letting $s = x+ct$, we have $f'(s) = h(s)/2c$ 

**By Superposition Principle** 

The solution to the homogeneous equation $u_t + cu_x = 0$, $\vec v = (c, 1)$:
$$
g(x - ct)
$$
Since the equation is linear, we can add $g(x-ct)$ to $f(x+ct)$ to get another solution

small proof:

$\mathscr L u = u_t + cu_x$, and $\mathscr L f = h$, $\mathscr L g = 0$, so $\mathscr L (f + g) = \mathscr L f + \mathscr L g = h + 0$ 

**Claim**: (proof in homework) The most general solution of $u_t + cu_x = h(x+ct)$ is
$$
u(x,t) = f(x+ct) + g(x-ct)
$$
Any solution to the homogeneous equation is of the form $g(x-ct)$ 

---

**Alternative Proof**: Characteristic Coordinates
$$
\xi = x+ct \qquad\eta = x-ct
$$
Chain Rule:

$\partial x = $$\frac{\partial }{\partial x}$ $= \frac{\partial }{\partial \xi}\frac{\partial \xi}{\partial x} + \frac{\partial }{\partial \eta}\frac{\partial \eta}{\partial x}$ $ = \partial \xi + \partial \eta$ 

$\partial t = \frac{\partial }{\partial t} = \frac{\partial }{\partial \xi}\frac{\partial \xi}{\partial t} + \frac{\partial}{\partial \eta}\frac{\partial \eta}{\partial t}$ $ =c \partial \xi -c \partial \eta$ 

$\partial t - c\partial x = -2c \partial \eta$ $\qquad$ $\partial t + c\partial x = 2c\partial \xi$ 

So $(*)$ is of the form:
$$
0 = (\partial t - c\partial x)(\partial_t + c\partial_x) = -4c^2\partial \eta \partial \xi = -4c^2 u_{\eta \xi}
$$
but $-4c^2 \neq 0$ $\implies$ $u_{\eta \xi } = 0$ 
$$
u_{\eta} = g'(\eta) 
\\ u = \int g'(\eta)d\eta
\\ u= g(\eta)+ f(\xi)
$$
Same as before.

---

![Sketch004](Math 126 Lecture 5.assets/Sketch004.jpg)

There are *two* families of characteristic lines, $x \pm ct = \text{ constant}$ for the wave equation.

The most general solution is the sum of two functions, One, $g(x-ct)$ is a wave of arbitrary shape traveling to the right at speed $c$, the other $f(x+ct)$ is a shape traveling to the left at speec $c$. 

### Initial Value Problem (wave equation)

$$
u_{tt} = c^2u_{xx} \qquad \text{for }-\infin < x < +\infin
$$

With initial conditions
$$
u(x,0) = \phi(x) \qquad u_t(x,0) = \psi(x)
$$
$\phi(x), \psi(x)$ are arbitrary functions. 

Example: $\phi(x) = \sin x$, $\psi (x) = 0$ $\implies u(x,t) = \sin(x)\cos(ct)$ 

Start from $u(x,t) = f(x+ct) + g(x-ct)$ $(**)$  

put $t = 0$ 
$$
u(0, t) = \phi(x) = f(x) + g(x)
$$
and differentiate $(**)$ by $t$ then put $t = 0$ 
$$
\psi(x) = cf'(x) - cg'(x)
$$
We have two equation, two unknowns $\implies$ goodness, use variable $s$ instead of $x$ 

$\implies$ $\phi' = f' + g'$ 

$\frac{1}{c}\psi = f' - g'$ 
$$
(+) \qquad \phi' + \frac{1}c\psi = 2f' \implies f' = \frac{1}{2}(\phi' + \frac{1}c\psi)
\\(-) \qquad \phi' - \frac{1}c\psi = 2g' \implies g' = \frac{1}{2}(\phi' - \frac{1}{c}\psi)
$$

$$
f(s) = \frac{1}{2}\phi + \frac{1}{2c}\int_0^s \psi ds + A
\\ g(s) = \frac{1}{2} \phi -\frac{1}{2c}\int_0^s \psi ds + B
$$

since we know $f + g = \phi$, $A + B = 0$. Hence the general solution of (IVP) is

(after doing $g(s) = \frac{1}{2}\phi - [-\frac{1}{2c}\int_{-s}^0 \psi ds ]+ B$ ) then adding $f + g$ and plugging in $s = x + ct $ for $f$ and $s = x-ct$ for $g$: 
$$
u(x,t) = \frac{1}{2}(\phi(x+ct)+\phi(x-ct)) + \frac{1}{2c} \int_{x-ct}^{x+ct} \psi(s)ds
$$
**Exercise** $\phi = 0$, $\psi(x) = \cos x$ 

$u(x,t) = 0 + \frac{1}{2c} \int_{x-ct}^{x+ct} \cos(s)ds$ 

$ = \frac{1}{2c} \sin(s)|_{x-ct}^{x+ct}$ $= \frac{1}{2c}[\sin(x+ct)- \sin(x-ct)]$ $ = \frac{1}{2c}[\cos(x)\sin(ct)+\cos(ct)\sin(x)-\cos(x)\sin(-ct) - \cos(-ct)\sin(x)]$ 

$= \frac{1}{c} \cos(x)\sin(ct)$ 

---

The formula is even easier if $\psi = 0$: 

$u(x,t) = \frac{1}{2}(\phi(x+ct) + \phi(x-ct))$ 

![Sketch005](Math 126 Lecture 5.assets/Sketch005.jpg)

Note about the formulas relating to the above (from book):

* given $\phi (x)$, $\psi = 0$ 
* Then you want to look at $x \pm ct$ at different times $t$ 
* For a given time $t_n$, note what $u(x,t)$ looks like in relation to $x \pm ct_n$ for different intervals of $x$ 

## Causality And Energy

### Causality

* The effect of an initial position $\phi(x)$: pair of waves traveling in either direction at speed $c$ and at half the original amplitude (?)
* The effect of an initial velocity $\psi$ is a wave spreading out at speed $\leq c$ 
  * waves lag behind if there is an initial velocity $\psi \neq 0​$ 

* We've seen if $\psi = 0$, everything moves at speed c. 
* for general $\psi​$ **Nothing moves faster than c**: *Principle of Causality* 



![image-20190202220026673](Math 126 Lecture 5.assets/image-20190202220026673.png)

$\triangle$ shaded part: the *domain of dependence* or the *past history* of the point $(x,t)$ 

![Sketch006](Math 126 Lecture 5.assets/Sketch006.jpg)

* An initial condition at the point ($x_0, 0$) for $t > 0$ only in the shaded sector, which is called the domain of influence (this is because the wave travels no faster than $c$)
  * $\implies$ if $\phi$ and $\psi$ vanish for $|x| > R$, then $u(x,t) = 0$ for $|x| > R + ct$ 
* The domain of influence of an interval $|x| \leq R$ is a sector $|x| \leq R + ct$ 

Fix a point $(x,t)$ for $t > 0$. Look at the expressin of $u(x,t)$ in terms of $\phi, \psi$: it depends only on values of $\phi$ at $x \pm ct$ and $\psi$ in the interval $[x-ct, x+ct]$ (from the integral). 

​	$\implies$ The interval $(x-ct, x+ct)$ is the interval of dependence of the point $(x,t)$ on $t = 0$ 

**Domain of dependence is bounded by the pair of characteristic lines that pass through $(x,t)$**  

What is the domain of influence of $(x_0, 0)$?: My guess: it is the part of the $xt$ plane where the point $(x_0, 0)$ has "influence" or where the part of waves of $\phi$ originating at that point have gone through. Because apparently we are dealing with a pair of waves traveling in either direction at speed $\leq c$ (due to $\phi, \psi$) and they will effectively "pass" through the shaded sector of "the domain of influence" 

What is the domain of dependence?: it is an inverse way of thinking of the above: we take a point $(x, t)$, $t > 0$ and it tells us where the waves that come to this point have been through over time. 

### Energy

$$
\text{String} \qquad \rho u_{tt} = Tu_{xx} \qquad - \infin < x <\infin
$$

$$
\text{Kinetic Energy} \qquad K = \frac{1}{2}\rho \int_{-\infin}^\infin u_t^2 dx
$$

Note: $K = \frac{1}{2}mv^2$ 

$\implies K < \infin$, as for the integral to converge, we suppose $\phi, \psi:$ ![Sketch007](Math 126 Lecture 5.assets/Sketch007.jpg)
$$
\frac{\partial K}{\partial t} = \frac{\rho}{2}\frac{\partial }{\partial t}\int_{-\infin}^\infin u_t^2 dx
\\ = \frac{\rho}{2}\int_{-\infin}^\infin (u_{tt}u_t + u_tu_{tt})dx
\\ = \rho \int_{-\infin}^\infin u_{tt}u_t dx
\\ \text{from the String equation above: } u_t = \frac{T}{\rho}u_{xx}
\\ = T \int_{-\infin}^\infin u_tu_{xx}dx
\\ \int f'g  = fg - \int fg'
\\ f = u_t, g = u_x, f' = u_{tx}, g' = u_{xx}
\\ T\int u_tu_{xx} = Tu_t u_{x} - T\int u_{tx} u_{x}
\\ \text{ the term }Tu_tu_x \text{ vanishes at }x = \pm \infin
\\ = -T \int_{-\infin}^\infin u_{tx}u_xdx
\\ (u_{tx}u_x = (\frac{1}{2} u_x^2)_t) \implies
\\ = -\frac{d}{dt}\int \frac{1}2 Tu_x^2  dx 
\\ = \text{Potential Energy } P
$$


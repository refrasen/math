# Math 126 Lecture 6

Office Hours:

| **Evans 895 - Professor Tim Laux**      | Evans 745 - Albert Ai                  |
| --------------------------------------- | -------------------------------------- |
| Monday 10-11 am <br />Wednesday 10-noon | Monday 1-2:30 pm <br />Thursday 6-8 pm |

## The Diffusion Equation

$$
(1)\qquad u_t = ku_{xx}\qquad(k>0)
$$

Properties of diffusions:

1. Maximum Principle
2. Uniqueness
3. Stability
4. Solution formula for (1) in $-\infin < x < \infin​$ 

---

### Maximum Principle

If $u(x,t)$ satisfies (1) in the rectangle $R = \{ (x,t): 0 \leq x \leq l, 0 \leq t \leq T \}$, in space-time, then the maximum value of $u(x,t)$ is attained either initially $(t=0)$ or on the lateral boundary $(x = 0$ or $x = l)$. 

![image-20190205233040287](Math 126 Lecture 6.assets/image-20190205233040287.png)

##### Remarks: 

1. This is usually called the <u>weak</u> Maximum Principle (still allows for the inner region to have $u$ attain its max). The <u>strong</u> Maximum Principle says that the maximum value can **only** occur on the bottom $(t=0)$ or lateral boundary $(x = 0$ or $x=l)$ 
2. Replace $u$ by $-u$ $\implies$ the same holds true for the minimum
3. Makes sense if you think of Temperature in a room, "heat would spread from hot spot in room"

#### Idea of Proof

By contradiction, suppose $n$ attains its max value at an interior point $(x_0, t_0)$. 

By calculus at this interior point:
$$
u_t = 0\\
u_x = 0 \\
u_{xx}\leq 0 \\ 
$$
$u_x = 0, u_{xx} \leq 0$ comes from being a local max, concave down

If we know $u_{xx}(x_0, t_0) \neq 0$ at the maximum (which we don't), then $u_{xx} < 0$ as well as $u_t = 0$, contradicting $u_t = ku_{xx}$ 

---

#### Proof of the Weak Maximum Principle

let $M:= \{ \text{ max of }u \text{ on the three sides } t= 0, x = 0, x = l \}$ 

**Want To Show**: $u(x,t) \leq M$ for all $(x,t) \sub R$ 

let $\epsilon > 0$, and define $v(x,t) := u(x,t) + \epsilon x^2$ 

Then $v$ solves a diffusion inequality:
$$
(3)\qquad v_t - kv_{xx}= u_t - k(u + \epsilon x^2)_{xx} \\ 
v_t - kv_{xx}= u_t - ku_{xx} - 2k\epsilon 
\\ v_t - kv_{xx} = -2k\epsilon < 0
$$
remembering that $\epsilon, k > 0$. This strict inequality will help us. 

$(*)$ Now suppose that $v(x,t)$ attains its max value at an interior point $(x_0, t_0)$ of $R$: $0 < x_0 < l$, $0<t_0 < T$ 

By calculus at $(x_0, t_0)$: $v_t = 0, v_x = 0, v_{xx} \leq 0$ 

$\implies$ $v_t - kv_{xx} \geq 0$ , contradiciton to $(3)$ 

$(*)$ Now suppose that $v(x,t)$ has a maximum at a point on the **top edge**: $t_0 = T$ and $0 < x < l​$ 

Then $v_x(x_0, t_0) = 0$ and $v_{xx} (x_0, t_0) \leq 0$ as before

Furthermore, because $v(x_0, t_0)$ is bigger than $v(x_0, t_0 - \delta)$, we have

$v_t(x_0, t_0) = \lim_{\delta \rightarrow 0^+} \frac{v(x_0, t_0) - v(x_0, t_0 - \delta)}{\delta} \geq 0$ as $\delta \rightarrow 0$ through positive values (this is not an equality because the maximum is only "one-sided" in the variable $t$)

so we have $v_t - kv_{xx} \geq 0$, contradiction to $(3)$ 

**therefore**: the max of $v$ on $R$ cannot occur on the interior or the top, but $v$ is continuous and $R$ is compact, so the maximum has to occur somewhere: the lateral boundar or the bottom $(x = 0 \or x = l \or t. = 0)$ 

There we know $v(x,t) \leq M + \epsilon l^2$ on this set, so $v(x,t) \leq M + \epsilon l^2$ in $R$. This is true for any $\epsilon > 0$ $\implies$ $u(x,t) = v(x, t) - \epsilon x^2 \leq M + \epsilon (l^2 - x^2)$ in $R$

This is true for any $\epsilon > 0$, so $u(x,t) \leq M$ in $R$ ($\epsilon \rightarrow 0)$ 

### Uniqueness

Given function s$f, g, h, \phi$ there exists at most one solution of
$$
(4)\\
u_t - ku_{xx} = f(x,t) \qquad \text{for }0 < x < l, t > 0 \\
u(x,0) = \phi(x) \qquad (\text{initial})
\\ u(0, t) = g(t) \quad (\text{boundary})
\\ u(l,t) = h(t) \quad (\text{boundary})
$$

---

How does Maximum Principle help us prove this?

#### Proof

Let $u_1, u_2$ be solutions of $(4)$ 

**Want To Show**: $u_1 = u_2$ 

Define $w = u_1 - u_2$. Then $w$ solves:
$$
(5) \\ 
w_t - kw_{xx} = 0 \\
w(x,0) = 0 \\
w(0, t) = 0 \\
w(l, t) = 0\\
$$
Let $T > 0$. Then by the maximum principle, $w$ has to attain its max on the Rectangle $0 \leq x \leq l, 0 \leq t \leq T$ on the bottom $(t =0)$ or the sides $(x = 0 \text{ or } x = l)$, where $w = 0$ from $(5)$ 

$\implies$ $w(x,t) \leq 0$ on $R$ $(6a)$ 

The same holds for the minimum (MP on $-w$: 

$\implies$ $w(x,t) \geq 0$ on $R$ $(6b)$ 

$6a$ and $6b$ $\implies$ $w(x,t) = 0$ on $R$ 

and since $T> 0 $ was arbitrary, $w = 0$ everywhere $t > 0$ 

---

#### Alternative Proof via energy method

$$
0 = w_t - kw_{xx} \\
w\cdot0 = w\cdot(w_t - kw_{xx}) \\
= \frac{\partial }{\partial t}(\frac{1}{2}w^2) - (kww_x)_x + kw_{x}^2 \\
\int_0^l w\cdot 0 \;dx = \int_0^l \frac{\partial }{\partial t}(\frac{1}2 w^2)\; dx - \int_0^l (kww_x)_x \; dx + \int_0^l kw_x^2 \; dx\\
$$

And 
$$
\int_0^l(kww_x)_x \; dx = k[w(l, t)w_x(l, t) - w(0,t)w_x(l,t)] = k[0-0] = 0
$$
and we can switch order of integration and differentation
$$
0 = \frac{d}{dt}\int_0^l \frac{1}{2}w^2 \; dx + \int_0^l kw_x^2 \; dx
\\
\frac{d}{dt}\int_0^l \frac{1}{2}w^2\;dx = -k \int_0^l(w_x)^2\; dx \leq 0
$$
since $(-k) < 0​$ and $(w_x)^2 \geq 0​$. 

So the time derivative of $\int_0^l \frac{1}{2}w^2 dx$ is negative $\implies$  $\int_0^l w^2 dx$ is decreasing over time, so
$$
(7)\qquad \int_0^l w^2(x, t) \; dx \leq \int_0^l w^2(x,0)\; dx \qquad \text{ for } t\geq 0
$$
In our case, $w^2(x,0) = 0$ (from $(5)$)$\implies$ 
$$
0 \leq \int_0^l w^2(x,t) \; dx \leq \int_0^l w^2(x,0)\; dx = 0
 \\ \implies \int_0^l w^2(x,t)\; dx = 0
 \\ \implies w(x,t) = 0 \qquad \forall\;  t \geq 0, \forall\;  0 \leq x \leq l\qquad(\text{by vanishing theorem})
$$

### Stability

Given $f, g, h, \phi_1, \phi_2$ and suppose $u_1, u_2$ solve $(4)$ with $\phi = \phi_1$ or $\phi = \phi_2$ respectively. (we are essentialy dealing with $(5)$, but the initial conditoin is different: instead of $0$ it is $\phi_1(x) - \phi_2(x)$). Then
$$
(8)\qquad\int_0^l [u_1(x, t) - u_2(x,t)]^2\; dx \leq \int_0^l [\phi_1(x)-\phi_2(x)]^2 \; dx \qquad \forall \; t \geq 0
\\
(9) \qquad \max_{0 \leq x \leq l} |u_1(x,t)- u_2(x,t)| \leq \max_{0\leq x \leq l}|\phi_1(x) - \phi_2(x)|
$$

#### Proof

$(8)$ follows from $(7)$, which we came to without using initial conditions

Let's turn to $(9)$.

Apply the Maximum Principle to $w = u_1 - u_2$ 

on the lateral sides:

$u_1(x,t) - u_2(x,t) = w(x,t) \leq \max w = \max|\phi_1(x)-\phi_2(x)|​$ 

Apply the Minimum Principle to $w = u_1 - u_2$ 

on the lateral sides:

$u_1(x,t) - u_2(x,t) = w(x,t)\geq \min w \geq - \max|\phi_1(x ) - \phi_2(x)|$ 

**Therefore**, because $-b \leq a \leq b \equiv |a| \leq b$ 
$$
\max_{0 \leq x\leq l} |u_1(x,t) - u_2(x,t)| \leq \max_{0 \leq x \leq l}|\phi_1(x) - \phi_2(x)|
$$
valid for all $t > 0$ 

---

$(9)$: Stability in the "uniform" sense... different method of measuring the nearness of functions than $(8)$ 

$(8)$: Stability in the "square integral" sense... If we start nearby at $t = 0$, we stay nearby. 

* Left side: measure nearness of the solutions at any later time
* Right side: measure nearness of intial data for two solutions
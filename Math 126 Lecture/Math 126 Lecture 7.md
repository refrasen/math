# Math 126 Lecture 7

See (2.4 Diffusion on the Whole Line)

## 4. Construction of Solutions

$$
(1) \qquad\begin{cases} u_t = ku_{xx} \\ u(x,0) = \phi(x) \end{cases} \qquad t> 0, -\infin<x<\infin
$$

### Some Invariance Properties

#### a. Translation

$$
u(x,t) \text{ solution} \implies u(x-y, t) \text{ solution}
$$

#### b. Derivatives 

$$
u(x,t) \text{ solution} \implies u_x, u_{xx}, u_t, \cdots \text{ are solutions}
$$

#### c. Linear Combinations

$$
u_k(x,t) \text{ solutions}, c_k \in \R \implies \sum c_ku_k(x,t) \text{ is a solution}
$$

#### d. Integrals

$$
S(x,t) \text{ solution} \implies S(x-y, t) \text{ is a solution}
\\ \implies v(x, t) = \int_{-\infin}^\infin S(x-y, t)g(y)dy \text{ is a solution}
$$

#### e. Dilations

$u(x,t)$ Is a solution, $a > 0$ $\implies$ $v(x,t) = u(\sqrt{a}x, at)$ is a solution
$$
\partial_t(v(x,t)) = \partial _t(u(\sqrt{a}x, at)) = au_t
\\ \partial x(v(x,t)) = \partial_x(u(\sqrt{a}x, at)) = \sqrt{a}u_x
\\ \partial_{x}\partial_x(v(x,t)) = au_{xx}
$$
so we do indeed have $au_t = aku_{xx} \equiv v_t = kv_{xx}$ 

---

### **Goal**: Find one particular solution $Q(x,t)$. Then construct all solutions from $\partial_x Q(x,t)$ using (d) 

**Initial Conditions** For $Q(x,t)$:
$$
(3) \qquad Q(x,0) = 1 \text{ for } x>0\qquad Q(x,0) = 0 \text{ for }x<0 
$$
The reason for this choice: $Q(x,0)$ does not change under dilations

### We'll find $Q(x,t)$ in 3 steps:

1. An Ansatz (i.e., a guess)
2. Turn the PDE into an ODE
3. Find a fomrula for $Q(x,t)$ 
4. Find a general solution formula

#### Step 1: An Ansatz

$$
(4) \qquad Q(x,t) = g(p) \qquad \text{where }p = \frac{x}{\sqrt{4kt}}
$$

* $g$ is a function of only one variable (to be determined)
* The $\sqrt{4k}$ factor is included to simplify a later formula
* property $(e)$ says equation $(1)$ doesn't "see" the dilation $x \rightarrow \sqrt{a}x, t \rightarrow at$
* (3) doesn't change at all under dialation, obviously
* $Q(x,t)$, defined by conditions (1) and (2), ought not to see the dilation either... How could this happen?
  * In only one way: if $Q$ depends on $x$ and $t$ solely through the combination $x/\sqrt{t}$ 

The dilation takes $x/\sqrt{t}$ into $\sqrt{a}x/\sqrt{at}$ $ = x/\sqrt{t}$. Thus, $p = x/\sqrt{4kt}$...

### Step 2: An ODE for $g$ 

By Chain rule: Using $p_t = -\frac{1}{2t}\frac{4kx}{(4kt)^{3/2}} = -\frac{1}{2t}\frac{x}{\sqrt{4kt}} = -\frac{1}{2t}p$  and $p_x = \frac{1}{\sqrt{4kt}}$ 
$$
Q_t = \frac{dg}{dp}\frac{\partial p}{\partial t} = -\frac{1}{2t}pg'(p) \\

Q_x = \frac{dg}{dp}\frac{\partial p}{\partial x} = \frac{1}{\sqrt{4kt}}g'(p) \\

Q_{xx} = \frac{dQ_x}{dp}\frac{\partial p}{\partial x} = \frac{1}{4kt}g''(p)
$$
$0 = Q_t - kQ_{xx} = \frac{1}{t}[-\frac{1}{2}pg'(p) - \frac{1}{4}g''(p)]$ 

$\implies$ $g$ must solve
$$
2pg' + g'' = 0
$$
This ODE is easily solved using $\exp \int 2p\; dp = \exp(p^2)​$ 

so we get $g'(p) = c_1\exp(-p^2)$ and 
$$
Q(x,t) = g(p) = c_1\int e^{-p^2}dp + c_2
$$

### Step 3: Explicit Formula for $Q(x,t)$ 

From right above we have
$$
Q(x,t) = c_1 \int_0^{(p = ) x/\sqrt{4kt} } e^{-p^2}dp + c_2
$$
This formula is valid only for $t > 0$. We now use $(3)$, expressed as a limit as follows
$$
\text{If }x>0, 1 = \lim_{t \rightarrow 0}Q = c_1 \int_0^{+\infin}e^{-p^2}dp + c_2 = c_1 \frac{\sqrt{\pi}}{2} + c_2
\\ \text{If }x< 0, 0 = \lim_{t\rightarrow 0} = c_1 \int_{0}^{-\infin}e^{-p^2}dp + c_2 = -c_1 \frac{\sqrt{\pi}}{2}+c_2
$$
(See Exercise 6, or Recognize it is from [probability theory, a gaussian][https://en.wikipedia.org/wiki/Normal_distribution])

Adding: $2c_2 = 1$, Subtracting $c_1 = \frac{1}{\sqrt{\pi}}$ 
$$
(5) \qquad Q(x,t) = \frac{1}{2}+ \frac{1}{\sqrt \pi} \int_0^{x/\sqrt{4kt}}e^{-p^2}dp
$$

### Step 4: General Solution

Set $S = \frac{\partial Q}{\partial x}$ and note: by $(b)$ $S$ solves $S_t = kS_{xx}$ 

Given any function $\phi$, we also define
$$
(6)\qquad u(x,t) = \int_{-\infin}^\infin S(x-y, t)\phi(y)dy \quad \text{for }t> 0
$$
By $(d)$, $u$ solves $u_t = ku_{xx}$ 

**Claim**: $u$ is the unique solution of $(1)$ 

**Argument**: 
$$
u(x,t) = \int_{-\infin}^\infin \frac{\partial Q}{\partial x}(x-y, t)\phi(y)dy 
\\= -\int_{-\infin}^{\infin}\frac{\partial }{\partial y}(Q(x-y, t))\phi(y)dy
\\ = + \int_{-\infin}^\infin Q(x-y, t)\phi'(y)dy - Q(x-y,t)\phi(y)|_{y=-\infin}^{y= +\infin} \qquad (\text{integration by parts})
$$
We **ASSUME** these limits vanish, In particular: temporarily assume $\phi(y) = 0$ for $|y|$ large. 

Therefore:
$$
u(x,0) = \int_{-\infin}^\infin Q(x-y, 0)\phi'(y)\; dy
\\ = \int_{-\infin}^xQ(x-y, 0)\phi'(y)\; dy + \int_x^\infin Q(x-y, 0)\phi'(y)\; dy
$$
**and for $y < x$, we have $x-y > 0$, so $Q(x-y, 0) = 1$ and for $y> x$, we have $x-y < 0$, so $Q(x-y, 0)$ in the second integral = 0**
$$
u(x,0) = \int_{-\infin}^x \phi'(y)\; dy
\\ = \phi(x)|_{-\infin}^x 
\\ = \phi(x)
$$
 because assumption that $\phi(-\infin) = 0$ 

This validates the initial condition from (1)

We conclude that (6) is the solution formula, where
$$
(7) \qquad S = \frac{\partial Q}{\partial x} = \frac{1}{\sqrt{4\pi kt}}e^{-x^2/4kt} \quad \text{for }t>0
$$
So the general solution formula for $(1)​$: 
$$
(8)\qquad u(x,t) = \frac{1}{\sqrt{4\pi kt}}\int_{-\infin}^\infin e^{-\frac{(x-y)^2}{4kt}}\phi(y)\; dy , \qquad t>0, -\infin<x<\infin
$$

### Names For $S$ 

Source function

Green's function

fundamental solution

gaussian of variance 2kt

diffusion kernel

propagator of the diffusion equation

![image-20190207005330923](Math 126 Lecture 7.assets/image-20190207005330923.png)

#### The source Function

The source function $S(x,t)$ is defined for all $ x\in \R$ and all $t > 0$. 

* $S(x,t)$ is positive and is even in $x$ $[S(-x, t) = S(x,t)]$ 

* It looks like Figure 1 for various values of $t$. 

* For small $t$, we get a tall thin spike (**delta function**) of height $\frac{1}{4\pi kt}$ 

**The Area Under the Graph**

$\int_{-\infin}^\infin S(x,t)\; dx = \frac{1}{\sqrt{\pi}} \int_{-\infin}^\infin e^{-q^2}\; dq$

substitute $q = \frac{x}{\sqrt{4\pi kt}}$ and $dq = (dx)/ \sqrt{4kt}$ 

* Coming back to small $t$, if we cut out the tall spike, the rest of $S(x,t)$ is very very small, so

$$
\max_{|x| > \delta} S(x,t) \rightarrow 0 \qquad \text{as }t \rightarrow 0
$$

The value of the solution $u(x,t)$ given by $(6)$ is a kind of **weighted average** of the initial values around the point $x$
$$
u(x,t) = \int_{-\infin}^\infin S(x-y, t)\phi(y)\; dy \simeq \sum_{t}S(x-y_i, t)\phi(y_i)\Delta y_i
$$
This is the average of the solutions $S(x-y_i, t)$ with weights $\phi(y_i)$ 

* For $t$ small, the source function is a spike, exaggerating the values of $\phi$ near $x$ 
* For any $t> 0$ the solution is a spread-out version of the initial values at $t = 0$ 

#### Physical Interpretation

Remember that $u(x,t)$ $= \text{mass}/\text{unit length}$ 

* Diffusion: $S(x-y, t)$ represents the result of a unit mass (say, 1 gram) of substance lcoated at time zero exactly at the position $y$ which is diffusing (spreading out) as time advances
  * so for example, if we look at $x = y$, it is as if we are checking out $S(0, t)$, i.e., the start point
  * So $S(x-y, t)$ tells us at time $t$, this is how much of the original unit mass from position $y$ has spread to $x-y$ units away from where it started at position $y$ 
    * Notice: small $t$, only for $x$ near $y$ do we have values that are well larger than $0$
    * As time progresses, $S(x-y, t_{\text{large}})$ will smooth out, so $S_x(x-y, t_{\text{large}})$ is small(?)
  * For any initial distribution of concentration, the amount of substance initially in the interval $\Delta y$ spreads out in time and contributes approximately the term $S(x-y_i, t)\phi(y_i)\Delta y_i$, i.e., at position $x$ and time $t$, this is how much of the mass at $y_i$ is at $x$ 
* So I suppose we have to consider all $y \in (-\infin, \infin)$ since there may be mass at any position, but that's where $\phi(y)$ comes into play: it represents the "weight" (I suppose) of a mass that would be at position $y$, which is probably why we assume for large $y$ $\phi$ disappears... So when we think about the concentration at position $x$ at time $t$, we have to consider all possible masses spreading out and reaching the point $x$. 

---

* Heat flow: hot spot in rod cooling off and spreading heat along the rod
  * $S(x-y, t)$ is the result of a hot spot at $y$ at time $0$. 
* Brownian motion, particles moving randomly in space
* etc.

#### Error Function

Usually impossible to evaluate (8) completely in terms of elementary functions

So answer to particular problems are somtimes expressible in terms of the error function of statistics
$$
(10) \qquad \mathscr{E}\text{rf}(x) = \frac{2}{\sqrt{\pi}} \int_0^x e^{-p^2}dp
$$
Note: $\mathscr{E}\text{rf}(0) = 0$...

##### Example 1: 

$$
Q(x,t) = \frac{1}{2} + \frac{1}{2}\mathscr{E}\text{rf}(\frac{x}{\sqrt{4kt}})
$$

##### Example 2:

$$
\phi(x) = e^{-x} \rightarrow \text{ solution for }u \\ \text{ by completing the square in the exponent}
$$

From the book:
$$
u(x,t) = \frac{1}{\sqrt{4\pi kt}}\int_{-\infin}^\infin e^{-(x-y)^2/4kt}e^{-y}dy
$$
take the exponent and complete the square:
$$
-\frac{x^2 - 2xy + y^2 + 4kty}{4kt} = -\frac{(y+2kt - x)^2}{4kt} + kt - x
$$
Let $p = (y+2kt - x)/\sqrt{4kt}$ so that $dp = dy/\sqrt{4kt}$ 
$$
u(x,t) = e^{kt-x}\int_{-\infin}^\infin e^{-p^2}\frac{dp}{\sqrt{\pi}} = e^{kt-x}
$$

* This solution grows in time, because $u(x,0) \rightarrow \infin$ as $x \rightarrow - \infin$ so at the very left side of the rod, it is very hot, and the heat gradually diffuses throughout the rod.

**This proves that the max principle doesn't hold for $-\infin < x < \infin$ instead othe bounded interval $0 \leq x \leq l$ ** 

* as $x \rightarrow - \infin$, $u(x, 0) \rightarrow + \infin$, so the left side is very hot initially and this heat gradually diffuses throughout the rod
* and as $t \rightarrow \infin​$, $u​$ grows 


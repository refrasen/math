# Math 126 Lecture 30 Notes

# Chapter 11: General Eigenvalues

Fourier Series Analysis: eigenvalues and functions of an operator on a domain are crucial, e.g., for finding complete sets of orthogonal functions or to find solution formulas for boundary value problems and initial value problems

## 11.1 The Eigenvalues are minima of the potential energy

Let $D$ be an arbitrary domain in $\R^d$ (i.e. an open set with a piecewise smooth boundary). Consider the equation
$$
(\text{IVP}) \qquad 
\begin{cases} 
-\Delta u = \lambda u & \text{in }D
\\ u = 0 & \text{on }\partial D
\end{cases}
$$
This is the **eigenvalue problem** for the Laplace operator on $D$ 

(with homogeneous Dirichlet conditions)

As in the chapter on Fourier series, we can show that all eigenvalues are non-negative & real
$$
0 \leq \lambda_1 \leq \lambda_2 \leq \ldots \lambda _n \leq \ldots
$$
We consider again the **inner product** and **norm** 
$$
(f,g) := \int_{D} f(\bold x) \overline{g(\bold x)} \, d \bold x, \quad , \| f \| := \sqrt{(f, f)} = \sqrt{\int_D|f(\bold x|^2\, d \bold x)}
$$
Let's consider the minimization problem 
$$
(\text{MP}) \qquad m = \min \{ \frac{\| \nabla w\|^2}{\| w \|^2}: w = 0 \text{ on }\partial D, w \neq 0 \}
$$
"Rayleigh quotient": $\frac{\| \nabla w \|^2}{\| w \|^2}$ , need to find the smallest possible value of this.

A solution of (MP) is a $C^2$ function $u$ such that $u = 0$ on $\partial D$, $u \neq 0$ such that
$$
\frac{\| \nabla u\|^2}{\| u \|^2} \leq \frac{\| \nabla w \|^2}{\|w\|^2} \quad \text{for all }w \text{ with }w = 0 \text{ on }\partial D \text{ and }w \not \equiv 0
$$
**Remarks**: If $u$ is a solution of (MP), then so is $Cu$ for any number $C \neq 0$ 

#### Theorem 1: Variational Principle for the 1st Eigenvalue

Let $u$ be a solution to (MP). Then 
$$
\lambda_1 = \frac{\| \nabla u \|^2}{\| u\|^2} = \text{min}\{  \frac{\| \nabla w\|^2}{\| w \|^2}: w = 0 \text{ on }\partial D, w \not \equiv 0 \}
$$
and
$$
\begin{cases}
- \Delta u = \lambda_1 u & \text{in }D
\\ u = 0  & \text{on }\partial D
\end{cases}
$$
"The first eigenvalue is the minimum of the energy" 

$u$: "ground state"

**Proof**: 

$w$ **admissible** : $\iff$ $w$ is $C^2$ in $D$, $w = 0$ on $\partial D$, $w \not \equiv 0$ 

Let $m$ be defined as in (MP), and $u$ a solution to (MP) 

**<u>1: $m$ is an eigenvalue:</u>** 

Let $v$ be $C^2$, $v = 0$ on $\partial D$, $v \neq 0$, then for each $\epsilon > 0$ 
$$
w_{\epsilon} := u + \epsilon v \qquad \text{is admissible!}
$$
Then:
$$
f(\epsilon) := \frac{\| \nabla w_{\epsilon}\|^2}{\| w _{\epsilon}\|^2}  = \frac{\int |\nabla (u + \epsilon v)|^2}{\int (u+\epsilon v)^2} = \frac{\int(|\nabla u|^2 + 2\epsilon \nabla u \cdot \nabla v +|\nabla v|^2)}{\int(u^2 + 2\epsilon u v + \epsilon^2 v^2)}
$$
has a minimum (w/ value $f(0) = m$) at $\epsilon = 0$ 

Hence
$$
0 = f'(0) = \frac{(2 \int \nabla u \cdot \nabla v)(\int u^2)- (2 \int u v)(\int |\nabla u|^2)}{(\int u^2)^2}
$$
$\implies$ 
$$
\underset{(G1) \, \underset{v=0 \\ \text{on }\partial D}{=} \int - \Delta u \, v }{\int \nabla u \cdot \nabla v} = \underset{= m}{\frac{\int |\nabla u|^2}{\int u^2}}\int uv = m \int uv
$$
$\implies$ 
$$
\int (\Delta u + mu)v = 0 \qquad \text{for all }v, v = 0 \text{ on }\partial D, v \not \equiv 0
$$
Hence
$$
\Delta u + mu = 0
$$
Otherwise, if $\Delta u + mu \neq 0$ at some point, e.g., $(\Delta u + mu)(\bold x_0) > 0$, Take $v$, reach contradiction. Since by assumption $u \not \equiv $, $m$ is eigenvalue of $- \Delta$ in $D$, and $u$ is its eigenfunction (the same way we did for other similar proofs with energy, see Section 7.1)

<u>**2: $m$ is the smallest eigenvalue:**</u>  $m = \lambda_1$ 

Suppose $\lambda$ is any eigenvalue, i.e. $\exists$ $v \neq 0$
$$
\begin{align}
-\Delta v &= \lambda v && \text{in }D
\\ v &= 0 && \text{on }\partial D
\end{align}
$$
But that means $v$ is admissible, hence
$$
m \overset{\text{(MP)}}{\leq} \frac{\| \nabla v\|^2}{\| v \|^2} = \frac{\int |\nabla v|^2}{\int v^2} = \frac{\int (-\Delta v)v}{\int v^2} = \frac{\int \lambda v^2}{\int v^2} = \lambda
$$
**Remark**: Another equivalent variational principle for $\lambda_1$
$$
\lambda_1 = \min \{  \int|\nabla w|^2 : w \text{ is }C^2, w = 0 \text{ on }\partial D \text{ and } \int w^2 = 1  \}
$$

### Example (homework)

The minimum of $\int_0^1 (w'(x))^2\, dx$ among all functions $w$ such that $\int_0^1 w^2 \, dx = 1$ and $w(0) = w(1) = 0$ **is** the **first eigenvalue** of $-\frac{d^2}{dx^2}$, i.e. $\pi^2$ (Homework: Direct Derivation)

#### Theorem 2 Variational Principle for the $n$th eigenvalue

Given the first (n-1) eigenvalues $\lambda_1, \lambda_2, \ldots , \lambda_{n-1}$, and their eigenfunctions, $v_1, \ldots, v_{n-1}$, we have
$$
(MP)_n \qquad \lambda_n = \min\{ \frac{\| \nabla w\|^2}{\|w\|^2}: \begin{align} w \in C^2, w = 0 \text{ on }\partial D w \not \equiv 0 \\ \text{ and } 0 = (w, v_1) = \ldots = (w, v_{n-1}) \end{align} \}
$$
**assuming** that the minimum exists. 

**Remark**: Since we are putting more constraints on the admissible functions $w$, the minimal value has to be larger: $\lambda_n \geq \lambda _{n-1} \geq \ldots \lambda_1 $ 

##### Proof

Let $u$ denote the minimum function in $(MP)_n$ (which exists by assumption)

let $m^* = \frac{\int |\nabla u|^2}{\int u^2}$ be the minimal value in $(MP)_n$, so $u =0$ on $\partial D$, $u \not \equiv 0$, and $u$ is orthogonal to $v_1, \ldots, v_{n-1}$ 

**1: As before we can show** 
$$
\int (\Delta u + m^* u)v = 0 \qquad \text{ for all fcts }v \text{ satisfy the same constraints}
$$
(Simplify use $f(\epsilon) = \frac{\int |\nabla (u + \epsilon v)|^2}{\int (u + \epsilon v)^2}$ is minimal for $\epsilon = 0$, so $f'(0)=0$) 

**2**: For $j = 1, \ldots, n-1$, since $(u, v_j) = 0$ we have
$$
\int (\Delta u + m^* u)v_j = 0
$$
Indeed, 
$$
\underset{\substack{(G2) \\v = u = 0 \text{ on }\partial D}}{=} \int (u \underset{=-\lambda_j v_j}{\Delta v_j} + m^* u v_j) = (m^* - \lambda_j) \underset{= (u, v_j)= 0}{\int u v_j \, dx}
$$
**3**: **$m^*$ is an eigenvalue** , i.e., $-\Delta u = m^* u$ 

Let $h$ be any $C^2$ function with $h = 0$ on $\partial D$ and $h \not \equiv 0$ 

Set
$$
v(\bold x) := h(\bold x) - \sum_{k=1}^{n-1}c_kv_k(\bold x) \qquad \text{where }c_k = \frac{(h, v_k)}{(v_k, v_k)}
$$
then $(v, v_j) = 0$ for $j = 1, \ldots, n-1$ 

Because: $(v, v_j) = (h, v_j) - \sum_{k=1}^{n-1}c_k(v_k, v_j)$ $= (h, v_j) - c_j(v_j, v_j) = (h, v_j) - (h, v_j) = 0$ 

**Hence** by 1 and 2
$$
\int(\Delta u + m^* u)h = \int (\Delta u + m^* u)v - \sum_{k = 1}^{n-1}\int (\Delta u  + m^* u)v_k = 0 + 0 = 0
$$
Since $h$ is an arbitrary $C^2$ function with $h = 0$ on $\partial D$ with $h \not \equiv 0$ 

We obtain 
$$
\Delta u + m^* u = 0 \qquad \text{ in }D
$$
By assumption $u \not \equiv 0$, $u = 0$ on $\partial D$ so $m^*$ is indeed an eigenvalue

**4**: $m^* = \lambda_N$: Homework

**Remark**: The evidence of the minima $u$ in (MP) & (MP)$_n$ is subtle and took about 50 years. Idea: direct method

a. If $\exists$ an admissible function, take sequence $u_n$ s.t. 
$$
\frac{|\ \nabla u_n\|^2}{\| u_n \|^2} \rightarrow \min \{ \frac{\| \nabla w \|^2}{\|w\|^2}: w \text{ is admissible} \}
$$
b. Compactness: $\exists $ subsequence $(u_k)$ such that $u_{n_k} \rightarrow u$ for some $u$

![image-20190413211747006](Math 126 Lecture 30 Notes.assets/image-20190413211747006.png)

c. Continuity $\| u \| = \lim_{ k \rightarrow \infin}\| u_{n_k}\|, \| \nabla u\| \leq \lim_{k \rightarrow \infin} (?) f\| \nabla u_{n_k}\|$

d. Regularity /: $u \in \C^2$, $u = 0$ on $\partial D$, $u \not \equiv 0$ admissibility

The curcial parts are a. and almost equivalently d. 
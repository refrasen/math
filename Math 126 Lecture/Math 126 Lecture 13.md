# Math 126 Lecture 13

## Orthogonality

**Definition**: Let $f, g​$ be real (complex) function on $(a,b)​$. Then, the **inner product** of $f, g​$ is
$$
(1) \qquad (f,g) := \int_a^b f(x)\overline {g(x)}\; dx
$$
(conjugation for complex valued functions)

and
$$
(f,f) = \int_{a}^b |f(x)|^2 dx
$$
**Definition**: $f$ and $g$ are **orthogonal** if $(f,g) = 0$ 

### Eigenfunctions and Orthogonality

Let $X_1(x)$, $X_2(x)$ be two different eigenfunctions with the same Boundary Conditions
$$
(2)\begin{cases} 
-X_1'' = \lambda_1 X_1 \\
- X_2'' = \lambda_2 X_2 
\end{cases} + BCs
$$

#### Case 1 $\lambda_1 \neq \lambda_2$: 

Compute $-X_1 '' X_2 + X_1 X_2 '' = (-X_1' X_2 + X_1 X_2')'$ 

Verifying: $-X_1'' X_2 - X_1'X_2' + X_1' X_2' + X_1X_2''$ 

**Green's second identity** 
$$
(3) \qquad (\lambda_1 - \lambda_2)\int_a^b X_1X_2 \; dx  = \int_a^b (-X_1'' X_2 + X_1X_2'')\; dx 
\\ = (-X_1'X_2 + X_1 X_2')|_a^b 
$$
Proving $(3)$ = 0

##### Verifying

$-X_1'(b)X_2(b) + X_1(b)X_2'(b) + X_1'(a)X_2(a) - X_1(a)X_2'(a)$ 

for $(D)$: we should have $X_1(b) = X_2(b) = 0 = X_1(a) = X_2(a)$ 

for $(N$): First derivatives vanish at both ends. Once again, $0$

for Periodic: $X_j(a) = X_j(b), X_j'(a) = X_j' (b)$ for $j = 1, 2$ 

for $(R)$: ??? see exercise $8$

**NOTE:** the right side of $(3)$ isn't always zero… the boundary conditions havr to be right, so in the common homogeneous examples… yes.

### Symmetric Boundary Conditions

**Definition**: (any) <u>pair of BC's</u> 
$$
(4) \qquad \begin{cases} 
\alpha_1X(a) + \beta_1X(b) + \gamma_1 X'(a)  + \delta_1 X'(b) = 0
\\ \alpha_2  X(a) + \beta_2 X(b) + \gamma_2 X'(a)+\delta_2X'(b) = 0
\end{cases}
$$
is called **symmetric** (hermitian) if $\forall f, g$ satisfying $(4)$ we have $(-f'g + fg')|_a^b = 0$   (or $-f'\overline{g} + f\overline{g'}|_a^b = 0$) 

where $f$ and $g$ satisfy the pair of boundary conditions $(4)$ 

**Note about the BC's**: 

* when we say pair of BC's, do we mean 

$$
X(a) = A_1, X(b) = B_1, X'(a) = C_1, X'(b) = D_1
\\ X(a) = A_2, X(b) = B_2, X'(a) = C_2, X'(b) = D_2
$$

* A robin condition would involve $\frac{\partial u}{\partial x} + au$ or in our case: $X' + aX$ 
* The standard BC's are symmetric, but the example: $X(a) = X(b)$, $X'(a) = 2X'(b)$ isn't because as we showed earlier, the right side of $3$ becomes $X_1'(b)X_2(b) - X_1(b)X_2'(b) \neq 0$ 

#### Theorem 1. 

Any two eigenfunctions with **symmetric Boundary Conditions** that correspond to different Eigenvalues are orthogonal

##### Corollary

The coefficients of a function in these eigenfunctions are uniquely determined. 

If $\phi(x) = \sum_{n=1}^\infin A_n X_n $ where each $X_m$ corresponds to a different $\lambda_m$, then 
$$
A_m = \frac{(\phi_, X_m)}{(X_m, X_m)}
$$
**Caution**: 

* avoided all questions of convergence
* If there are two eigenfunctions, say $X_1(x)$ and $X_2(x)$ with the same eigenvalues, then they don't have to be orthogonal, but if they aren't orthogonal they can be made so by the **Gramd_Schmidt** orthogonalization procedure

$$
v_1 = x\\
v_2 = y - \frac{(v_1,y)}{(v_1,v_1)}v_1
$$

Note: $\text{proj}_u(v) = \frac{(u,v)}{(u,u)}u$ 

### Complex Eigenvalues

#### Theorem 2.

Under the **same conditions as Theorem 1**, *all the eigenvalues* are **real** numbers. Furthermore, all the eigenfunctions can be chosen to be real valued 

##### Proof:

$\lambda = a + ib$, $a, b \in \R$. $X(x) = Y + i Z$ with $-X'' = \lambda X$ plus BCs

Taking conjugate of the equation: $-\overline X'' = \overline \lambda \overline X$ plus BCs. 

​	$\implies$ $\overline \lambda $ is an eigenvalue

Green's second identity with $X$, $\overline X$ and BC's are symmetric:
$$
\int_a^b (-X'' \overline X + X \overline X'') dx = (-X'\overline X + X\overline X')|_a^b = 0
$$
so $(\lambda- \overline \lambda) \int_a^b X\overline X \; dx = 0$ 

but $X \overline X = |X|^2 \geq 0$, and equality only occurs when $X \equiv 0$, which cannot be an eigenfunction. So the integral *cannot* vanish

therefore: $\lambda - \overline \lambda = 0$ $\implies$ $\lambda$ is **real** 

$-Y'' - iZ'' = \lambda Y + i\lambda Z$ equating the real and imaginary:

$-Y'' = \lambda Y$, $-Z'' = \lambda Z$

and the BCs still hold for both $Y$ and $Z$ bevcause the eight constants in $(4)$ are real

### Negative Eigenvalues

#### Theorem 3.

Assume same conditions as in Theorem 1. if
$$
(10) \qquad f(x)f'(x)|_a^b \leq 0
$$
for all real valued functions $f(x)​$ satisfying the BC's, then there is no negative eigenvalue

**proof: exercise 13** 

## Completeness

Consider the eigenvalue problem 
$$
(1) \qquad X'' + \lambda X = 0 \qquad x \in (a,b)
$$
with **symmetric** BC's

We know that the eigenvalues are **real** and (nonnegative?)

#### Theorem 1

There are an infinite number of eigenvalues. They form a sequence $\lambda_n \rightarrow + \infin$  as $n\rightarrow \infin$ 

write $\lambda_1\leq \lambda_2 \leq … \leq $  

​	if $\lambda_i$ appears multiple times, just repeat back to back

with corresponding eigenfunctions $X_1, X_2, …$ 

​	WLOG: we may assume that all $X_n$'s are pairwise orthogonal (Gram Schmidt)

**Definition**: $f$ a function $\implies$ $A_n = \frac{(f, X_n)}{(X_n, X_n)}$ is the $n$th Fourier coefficient of $f$, $f = \sum A_n X_n(x)$ is the Fourier Series of $f$ 

### Three Notions of Convergence

The series $\sum_{n=1 }^\infin f_n $ converges to $f(x)$...

##### pointwise in $(a,b)$

if $\forall x \in (a,b)$, 
$$
|f(x) - \sum_{n=1}^N f_n| \rightarrow \text{ as } N \rightarrow \infin​
$$

#####  uniformly in $[a,b]$ 

if
$$
\max_{a\leq x \leq b}|f(x) - \sum_{n=1}^N f_n(x)| \rightarrow 0 \text{ as }N \rightarrow \infin
$$
Take the biggest difference over all the $x$'s and *then* take the limit

##### in the mean-square sense ($L^2$) 

if
$$
\int_a^b |f(x) - \sum_{n=1}^N f_n(x)|^2 \; dx \rightarrow 0 \text{ as }N \rightarrow \infin
$$
$L^2$ refers to the square inside the integral

#### Example 1

$f_n(x) = (1-x)x^{n-1} = x^{n-1}- x^n$ $0 < x < 1$ then
$$
\sum_{n=1}^N f_n(x) = \sum_{n=1}^N x^{n-1}-x^n 
\\ = 1 - x + x - x^2 +x^2 - x^3 ... + x^{n-2}-x^{n-1}+x^{n-1}-x^n
\\ = 1 -x^n \rightarrow 1 \text{ as }N \rightarrow \infin
$$
because $x < 1$, so $x^n \rightarrow 0$ as $n \rightarrow \infin$ 

So $\sum f_n$ converges pointwise in $(0,1)$, but **not** uniformly:
$$
\max_{0 \leq x \leq 1} |1 - \sum _{n=1}^N f_n(x)| 
\\= \max_{0\leq x \leq 1} |x^N|
\\ = 1 \not \rightarrow 0
$$
Does converge in $L^2$: 
$$
\int_0^1 |x^N|^2 \; dx = \frac{x^{2N+1}}{2N+1}|_0^1 
\\ = \frac{1}{2N+1} \rightarrow 0 \text{ as }N \rightarrow \infin 
$$
![image-20190225194124390](Math 126 Lecture 13.assets/image-20190225194124390.png)

Red: $1 - x$, blue: $1- x^2$, green: $1-x^3$, purple: $1-x^4$, black: $1-x^{10}$ 

##### **More examples in book** 

### Convergence Theorems 

#### Theorem 2. Uniform Convergence

The Fourier series $\sum A_n X_n$ of $f(x)$ **converges uniformly** to $f(x)$ in $[a,b]$ if

1. $f(x), f'(x), f''(x)$ exist and are continuous in $[a,b]$ 
2. $f(x)$ satisfies the given boundary conditions

* For the classical Fourier series (full, sine, and cosine), we don't need the assumption on $f''(x)$ 

#### Theorem 3. $L^2$ Convergence

The Fourier series $\sum A_n X_n$ of $f(x)$ **converges to $f(x)$ in the mean-square sense**  in $(a,b)$ provided only that $f(x)$ is any function for which 
$$
\int_a^b |f_n(x)|^2\; dx < \infin\text{ (is finite)}
$$
**Definition**: $f(x)$ is called piecewise continuous in $[a,b]$ if $\exists$ $p_1, p_2, …, p_n$: $f$ is continuous in $[a,b] \setminus  \{ p_1, …, p_n\}$

and $f(p_k +), f(p_k -)$ exist for all $k$ 

![image-20190225195013411](Math 126 Lecture 13.assets/image-20190225195013411.png)

#### Theorem 4. $\infin$ (Pointwise Convergence)

If $f$ is perioidic of period $2l$ on the line for which $f(x), f'(x)$ are pointwise continuous, then the classical full Fourier Series converges to $\frac{1}{2} (f(x+) + f(x-))$ for all $-\infin <x<\infin$ 

### **Pointwise Convergence** 

1. The classical Fourier series (full or sine or cosine) converges to $f(x)$ pointwise on $(a,b)$ provided that $f(x)$ is a conintuous function on $a \leq x \leq b$ and $f'(x)$ is piecewise continuous on $a \leq x \leq b$ 

2. More generally, if $f(x)$ itself is only piecewise continuous on $a \leq x \leq b$ and $f'(x)$ is also piecewise continuous on $a \leq x \leq b$, then the classical Fourier series converges at every point $x$ $\in (-\infin < x < \infin)$. 

   The sum is
   $$
   \sum_n A_n X_n (x) = \frac{1}2[f(x+)+f(x-)] \quad \text{for all }a < x < b
   $$
   The sum is $\frac{1}{2}[f_{ext}(x+) + f_{ext}(x-)]​$ for all $-\infin <x<\infin​$ where $f_{ext}(x)​$ is the extended function (periodic, odd periodic, or even periodic)

At a jump discontinuiuty, the series converges to the *average* of the limits from the right and from the left. 

### The $L^2$ Theory

$(f,g) = \int_a^b f(x) \overline{ g(x)} \; dx$ 

**Definition**: $L^2$ norm of $f$ 

$||f|| = \sqrt{(f,f)} = (\int_a^b |f(x)|^2 \;dx )^{1/2}$ 

Then $||f-g|| = (\int_a^b |f(x) - g(x)|^2 \; dx)^{1/2}$ 

measures the distance of $f$ and $g$ 

**Theorem 3 Says**: If $X_n$ are eigenfunctions w/ symmetric BCs and if $|| f || < \infin$, then $|| f - \sum_{n \leq N} A_n X_n || \rightarrow 0$ as $N \rightarrow \infin$ 

$\sum_{n\leq N} A_n X_n$ is the projection of $f(x)$ onto the span $\{ X_1, X_2, X_3, …, X_n\}$ 


# Math 126 Lecture 29

### Fourier

$$
\begin{align}
S_t &= S_{xx}&& -\infin <x <\infin, t>0
\\ S(x,0) &= \delta(x)
\end{align}
$$

If we assume: $\hat S(k, t) = \int_{-\infin}^\infin S(x, t)e^{-ikx}\, dx$

Then:
$$
\begin{align}
\frac{\partial \hat S}{\partial t}&= (ik)^2 \hat S = - k^2 \hat S
\\ \hat S(k,0) &= 1
\end{align}
$$
This is a sipmle **1 -parameter family** of ODEs, which we can solve indepently for each $k$: 
$$
\hat S(k,t)= e^{-k^2t}
$$
Comparing to the table:
$$
S(x,t) = \frac{1}{\sqrt{4 \pi t}}e^{-x^2/(4t)}
$$

### Laplace

$$
\begin{align}
u_t - u_{xx} &= 0 && t > 0, x > 0
\\ u(0,t) &= f(t) && t> 0
\\ u(x,0)&= 0 && x> 0
\\ u(x,t) &\rightarrow 0 && \text{as }x \rightarrow \infin
\end{align}
$$

$\mathscr{L}u = \int_0^\infin e^{-st}u(x, t)dt$ 

$U = \mathscr{L}u$ satisfies
$$
\begin{align}
sU = U_{xx}
\\ U(0, s) = F(s)
\\ U(+\infin) =0
\end{align}
$$
The solution is
$$
U(x,s) = F(s)e^{-x\sqrt{s}} = F(s)
$$
and $e^{- x\sqrt{s}}$ is the Laplace transform of $E(x, t) = [x/(2 \sqrt{k \pi}t^{3/2})]e^{-x^2/4kt}$ 
$$
\begin{align}
u(x,t) &= \int_{0}^t E(t-t')f(t')dt'
\\ &=  \frac{x}{2 \sqrt{ \pi}}\int_0^t \frac{1}{(t-t')^{3/2}}e^{-x^2/4(t-t')}f(t')dt'
\end{align}
$$

### Laplace's equation in a half-place

Consider
$$
\begin{align}
u_{xx} + u_{yy} &= 0 && -\infin <x <\infin, y>0
\\ u(x,0) &= \delta(x)
\end{align}
$$

#### Fourier transform in x:

Define 
$$
U(k,y) := \int_{-\infin}^\infin e^{-ikx}u(x,y)\, dx
$$
Then $U$ satisfies the ODE:
$$
\begin{align}
-k^2U + U_{yy} &= 0 & y>0
\\ U(k,0)&= 1
\end{align}
$$
The solutions of the ODEs are $e^{yk}$ and $e^{-yk}$ and only the one with negative exponent can be used as we need to apply the fourier transform to it. 

So if $y > 0$: $e^{-ky}$; if $y < 0$, $e^{ky}$ 

So
$$
U(k,y) = e^{-y|k|}
$$
Therefore
$$
\begin{align}
u(x,y) &= \int_{-\infin}^\infin e^{ikx}- e^{y|k|}\frac{dk}{2 \pi} && \text{this implies integral converges}
\\&= \int_{-\infin}^0 e^{k(y+ix)}\frac{dk}{2\pi} + \int_0^\infin e^{k(-y + ix)}\frac{dk}{2\pi}
\\ &= \frac{1}{2 \pi (y +ix)}e^{k(y+ix)}|_{k = -\infin}^{k = 0} + \frac{1}{2\pi (-y + ix)}e^{k(-y + ix)}|_{k=0}^{k = \infin}
\\ &= \frac{1}{2\pi}[\frac{1}{y +ix}-\frac{1}{-y + ix}] = \frac{y}{\pi (x^2 +y^2)}
\end{align}
$$

### Bessel Potential

Let's consider
$$
-\Delta u + u = f \quad \text{in }\R^d
$$
where $f \in \mathscr{D}$ (?) (in fact we'll only need $\int f^2 (\bold x)\, d \bold x< \infin$)

The Fourier Transform $U(\bold k) = \int_{\R^d}u(\bold x)e^{-i \bold k \cdot \bold x}\,d\bold x$ satisfies
$$
(1 + |\bold k|^2)U = F
$$
where $F$ is the Fourier Transform of $f$

Now this is trivial to solve for $U$: 
$$
U(\bold k) = \frac{F(\bold k)}{1 + |\bold k|^2}
$$
Hence:
$$
u(\bold x) = \mathscr{F}^{-1}(\frac{F(\bold k)}{1 +|\bold k|^2}) = f * B
$$
Where $B$ satisfies
$$
F(B) = \frac{1}{1 + |\bold k|^2}
$$
Solve formally for $B$: Since
$$
\frac{1}{a} = \int_0^\infin e^{-ta}\, dt \text{  for  }a>0
$$
we have 
$$
\frac{1}{1+|\bold k|} = \int_0^\infin e^{-t(1 + |\bold k|^2)}d t
$$
Thus:
$$
B = \mathscr{F}^{-1}(\frac{1}{1 + |\bold k|^2})= \frac{1}{(2\pi)^d}\int_0^\infin e^{-t}(\int_{\R^d}e^{i\bold x\cdot \bold k - t |\bold k|^2}d\bold k)\, dt
$$
and  (confused about this, need to find in book)
$$
\begin{align}
\int_{\R^d}e^{i\bold x \cdot \bold k - t |\bold k|^2}d \bold k
&= \prod_{j =1}^d \int_{-\infin}^\infin e^{ix_j k_j - t k_j^2}dk_j
\\&= \prod_{j=1}^d \int_{-\infin}^\infin e^{-tk_j^2}dk_j 
\\ & = \prod_{j=1}^d\sqrt{\frac{\pi}{t}}e^{-\frac{x_j^2}{4t}}
\\ &= (\frac{\pi}{t})^{d/2} e^{-|\bold x|^2/4t}
\\ &= \frac{1}{(4 \pi)^{d/2}}\int_0^\infin \frac{1}{t^{d/2}}e^{-t - |\bold x|^2/4t}\, dt
\end{align}
$$
and
$$
u(\bold x) = \frac{1}{(4\pi)^{d/2}}\int_0^\infin \int_{\R^d}\frac{1}{t^{d/2}} e^{-t - \frac{|x-y|^2}{4t}}f(y)\, dy \, dt
$$

### Wave Equation

Consider
$$
\begin{align}
u_{tt} - \Delta u = 0 && \text{in }\R^d \times (0, \infin)
\\ u = \phi \qquad  u_t = \psi && \text{on }\R^d
\times \{t = 0\}\end{align}
$$
$U =$ Fourier transform of $u$ in $\bold x$ - variable. Then
$$
\begin{align}
U_{tt} +|\bold k|^2 U =0 && t>0
\\ U = \Phi, U_t = \Psi && t =0
\end{align}
$$
This is an ODE for each fixed $\bold k$ and has the solution
$$
U(\bold k, t) = \Phi \cos(t|\bold k|)+ \frac{\Psi}{|\bold k|}\sin (t |\bold k|)
$$
Inverting, we find:
$$
u(\bold x, t) = \mathscr{F}^{-1}(\Phi \cos(t|\bold k|) + \frac{\Psi}{|\bold k|}\sin (t |\bold k|)) 
$$
If $\Psi \equiv 0$, then
$$
u(\bold x, t) = \frac{1}{(2\pi)^d}\int_{\R^d} \frac{\Phi(\bold k)}{2}(e^{i (\bold x\cdot \bold k + t |\bold k|) }+ e^{i(\bold x \cdot \bold k - t |\bold k|)})d\bold k
$$
Recall: The energy
$$
E(t) = \frac{1}{2}\int_{\R^d}(u_t^2 + |\nabla u|^2)\, d \bold x
$$
is constant in time. 
$$
E(t) = E(0) = \frac{1}{2}\int_{\R^d}\psi^2 + |\nabla \phi|^2\, d\bold x
$$
We'll show that as $t \rightarrow \infin$, the energy splits equally into PE and KE
$$
\lim_{t \rightarrow \infin} \int_{\R^d}|\nabla u|^2 d\bold x = \lim{t \rightarrow \infin}\int_{\R^d}u_t^2 d \bold x = E(0)
$$
Using the formula for $U$: 
$$
\int_{\R^d}|\nabla u|^2 \, d\bold x = \frac{1}{(2 \pi)^d}\int_{\R^d}|\bold k|^2 |U(\bold k)|^2 d\bold k
\\ = \frac{1}{(2\pi)^d}(\int|\bold k|^2 |\Phi|^2 \cos^2(t|\bold k|) + |\Psi|^2 \sin^2(t|\bold k|)d \bold k + \int \cos (t |\bold k|)\sin(t|\bold k|)|\bold k|(\Phi \overline{\Psi}+ \overline{\Phi}\Psi)d\bold k)
$$

* One can show taht the last r.h.s. integral goes to 0 as $t \rightarrow \infin$ Using $\cos(t|\bold k|)\sin (t |\bold k|) =\frac{1}{2}\sin (2 t |\bold k|)$ 
* Using $\cos^2(t|\bold k|) = \frac{1}{2}(\cos(2t|\bold k|)+1)$ likewise

$$
\frac{1}{(2\pi)^d}\int |\bold k|^2 |\Phi|^2 \cos^2(t|\bold k|)d \bold k \underset{t \rightarrow \infin}{\rightarrow } \frac{1}{(2\pi)^d}\frac{1}{2}\int |\bold k|^2 |\Phi|^2 d\bold k = \frac{1}{2}\int |\nabla \phi|^2
$$

and
$$
\frac{1}{(2\pi)^d}\int|\Psi|^2 \sin^2 (t|\bold k|)d \bold k \underset{t \rightarrow \infin}{\rightarrow}\frac{1}{(2\pi)^d}\frac{1}{2}\int |\Psi|^2 d\bold k = \frac{1}{2}\int \Psi^2
$$
**ad(?)***: Suppose $f \in \mathscr{D}$, then
$$
\begin{align}
\int \cos(t|\bold k|)\sin(t|\bold k|)f(\bold k)d \bold k &= \frac{1}{2}\int \sin (2 t |\bold k|)f(\bold k)d \bold k
\\ &= \frac{1}{2}\int_0^\infin \sin(2tr)(\int_{\partial B_r(0)}f\, dS)\, dr 
\\ &= - \frac{1}{4t}\int _0^\infin \frac{d}{dr}(\cos (2tr))(\quad)dr
\\&\underset{\text{IBP}}{=} \frac{1}{4t}\underbrace{\int_0^\infin \cos(2tr)\frac{d}{dr}\int_{\partial B_r(0)}f\, dS \, dr}_{= \text{const}}
\\ &= O(\frac{1}{t})\rightarrow 0 \text{ as }t \rightarrow \infin
\end{align}
$$

# Math 126 Lecture 36

## Calculus of Variations (14.3)

Recall: $F = F(x, z, p)$, $E[u] = \int_a^b F(x, u(x), u'(x))\, dx$, $x \in \mathscr{A}$, meaning: $u(a) = \alpha, u(b) = \beta$ 

If $u \in \mathscr A$ satisfies $E[u] = \min_{v \in \mathscr A} E[v]$, then
$$
(E-L) \qquad - \frac{d}{dx}[\frac{\partial F}{\partial p}(x, u(x), u'(x))] + \frac{\partial F}{\partial z}(x, u(x), u'(x)) = 0
$$
Caution: Bad Notation: $- \frac{d}{dx}\frac{\partial F}{\partial u'}. + \frac{\partial F}{\partial u} = 0$ 

##### **Example**: The Brachistochrone

Find the slope of the curve down which a marble/ball sliding from rest (and only accelerated by gravity) will slip as quick as possible from our point $(a, \alpha)$ to another $(b, \beta)$ - of course with $\beta < \alpha$; and we ignore friction here

![image-20190428100853173](Math 126 Lecture 36.assets/image-20190428100853173.png)

$T[u] = $ time to travel from $(a, \alpha)$ to $(b, \beta)$ along graph of $u$ 
$$
= \int_{(a, \alpha)}^{(b, \beta)}\frac{ds}{V}\qquad \begin{pmatrix} s = \text{arclength} \\ v = \text{velocity} \end{pmatrix}
$$

* By conservation of energy: KE = $\frac{1}{2}m V^2 = mgu$ = PE, so $V = \sqrt{2gu}$ 
* By Pythagoras: $ds = \sqrt{(dx)^2 + (du)^2} = \sqrt{1 + (\frac{du}{dx})^2}dx$ 

So we want to minimize:
$$
T[u] = \int_a^b (\frac{1 + (u'(x))^2}{2gu(x)})^{\frac{1}{2}}\, dx, \qquad u \in \mathscr{A}
$$
The integrand is therefore: $F(x, z, p) = (\frac{1+p^2}{2gz})^{1/2}$, and our optimal curve - The brachistochrone - satisfies 
$$
(E-L) \qquad 0 = -\frac{d}{dx}(\frac{\partial F}{\partial p}(x, u, u'))+ \frac{\partial F}{\partial z}(x, u, u')
$$
with $\frac{\partial F}{\partial p} = \frac{p}{\sqrt{2gz(1+p^2)}}$ 

---

Instead of pluging in and solving, let's use the following fact:

#### Theorem: Beltrami identity

If $F = F(z, p) $ does not depend on $x$ explicitly, then (E-L) implies
$$
(B-I) \qquad F(u, u') - u' \frac{\partial F}{\partial p}(u, u') = C\qquad \text{for some }C \in \R
$$
Proof: https://en.wikipedia.org/wiki/Beltrami_identity

* multiply both sides of E-L equation by $p​$ 
* Use chain rule on $\frac{dF}{dx}$ ($dL/dx \neq \partial L/\partial x$) , rearrange to substitute in for $p \frac{\partial L}{\partial p}$ 
* Use product rule  on $\frac{d}{dx}(\frac{\partial L}{\partial p}p) $ and rearrange 
* Now substitute back in and use the fact that $\partial L/ \partial x = 0$ 

Hence:
$$
(B-I) \qquad \sqrt{\frac{1 + (u')^2}{2gu}} - u' \frac{u'}{\sqrt{2gu(1+(u')^2)}} = C \qquad \text{for some }C \in \R
$$

$$
\text{LHS} = \frac{1}{\sqrt{2gu}}(\frac{1+(u')^2}{\sqrt{1 + (u')^2}}- \frac{(u')^2}{\sqrt{1 + (u')^2}}) 
$$

So: 
$$
\frac{1}{\sqrt{2gu}}\frac{1}{\sqrt{1 + (u')^2}} = C
$$
and squaring both sides and rearranging:
$$
(1+(u')^2)u = \frac{1}{2gC^2} = : k^2
$$
Solution: 
$$
x = \frac{1}{2}k^2(\theta - \sin \theta)
\\ u = y = \frac{1}{2}k^2(1 - \cos \theta)
$$
the equations of a cycloid (?)

$(1 + (du/dx)^2)u = k^2$ 

$\frac{du}{dx} = \sqrt{\frac{k^2}{u}- 1}$ 

$\frac{1}{\sqrt{k^2/u - 1}} du= dx$ 

$\frac{\sqrt{u}}{\sqrt{k^2 - u}}du = dx​$ 

let $w = \sqrt{u}$ , $\frac{dw}{du} = \frac{1}{2\sqrt{u}}$ $du = 2w \, dw$

$\int \frac{2w^2}{\sqrt{k^2 - w^2 }}\, dw$ 

so let $w = k \sin \theta$ , and $dw = k \cos \theta \, d\theta$ 

$\int \frac{2k^2 \sin^2\theta}{k \cos \theta} (k \cos \theta) d\theta $ $= \int 2k^2 \sin^2 \theta \, d\theta$, 

$\sin^2(x) = \frac{1}{2}(1 - \cos(2x))$  

$k^2 \int 1 - \cos 2 \theta \, d\theta$ … $\phi = 2\theta$, $d\phi = 2d\theta$ $d\theta = \frac{1}{2}d\phi$ 

$\frac{1}{2}k^2\int 1 - \cos \phi$ $= \frac{1}{2}k^2(\phi - \sin \phi) = x$ 

And $u = w^2 = k^2 \sin^2 \frac{\phi}{2}$ 

$\sin^2 \frac{\phi}{2} = \frac{1}{2}(1 - \cos \phi)$, which is what we want. 

---

### Calculus of Variations in higher dimensions

Given a domain $D \sub \R^d$, boundary data $h: \partial D \rightarrow \R$ and an integrand $F = F(x, z, p)$, $F: \R^d \times \R \times \R^d \rightarrow \R$, we define:
$$
E[u] := \int_D F(x, u(x), \nabla u(x))\, dx
$$
for
$$
u \in \mathscr{A} = \{ v: D \rightarrow \R | v = h \text{ on }\partial D\}
$$
Problem: **Find $u \in \mathscr{A}$ s.t. $E[u] = \min_{v \in \mathscr A}E[v]$** $(*)$ 

##### Example

$F = \sqrt{1 + |p|^2}$, then:

$E[u] = \int-D \sqrt{1 + |\nabla u|^2}\, dx $= surface area of graph of $u$ 

![image-20190428111409917](Math 126 Lecture 36.assets/image-20190428111409917.png)

#### Theorem (Euler-Lagrange eq)

If $u  \in \mathscr A$ solves $(*)$, then
$$
(E-L) \qquad - \sum_{i=1}^d \frac{\partial}{\partial x_i}[\frac{\partial F}{\partial p_i}(x, u, \nabla u)] + \frac{\partial F}{\partial z}(x, u, \nabla u) = 0
$$
Note: This is a PDE!

##### Proof: 

Fix w.r.t. $w = 0$ on $\partial D$, then $\underset{ = e(0)}{E[u]} \leq \underset{= : e(s)} E[u+sw]$ for all $s \in \R$ $\implies$ $e'(0) = 0$ ...

so you do the whole integral thing for $e'(0)$ and then do IBP, etc. 

#### Examples

##### 1. Minimal Surface Equation

If $E[u] = \int_D \sqrt{1 + |\nabla u|^2}\, dx$, then the (E-L) equation reads
$$
\sum_{i=1}^d \frac{\partial }{\partial x_i}( \frac{u_{x_i}}{\sqrt{1 + |\nabla u|^2}} ) = 0
$$
In gemoetric terms: $H = K_1 + K_2 = 0$ 

$H$ is the mean curvature???

##### 2. Poisson's Equation

$$
F(x, z, p) = \frac{|p|^2}{z} + f(x)z
$$

then E-L equation reads (but first:)
$$
\frac{\partial F}{\partial p_i} = p_i \implies \frac{\partial}{\partial x_i}(\frac{\partial F}{\partial p_i}(x, u, \nabla u)) = \frac{\partial}{\partial x_i}(u_{x_i}) = u_{x_i x_i}
\\ \frac{\partial F}{\partial z} = f(x)
$$

$$
\nabla u = f
$$

##### 3. Saddle Points

Action = Kinetic Energy - Potential energy = K - P

so e.g. , in the case of the vibrating string
$$
A[u] = \int_{t_1}^{t_2}\int_0^l [\rho u_t^2 - T u_x^2]\, dx \, dt
$$
Then one then looks for stationary points of the action:
$$
\frac{d}{ds}|_{s = 0}A[u + sv] = 0
$$
for all $v$ s.t. $v = 0$ along $x = 0, x =l$ and $t = t_1, t= t_2$ 

Compute: 
$$
A[u+sv] = \int_{t_1}^{t_2}\int_0^l[\rho(u_t^2+ 2su_tv_t + s^2v_t^2 ) - T(u_x^2+ 2su_tv_x + s^2v_x^2 )\, dx\, dt
\\ A'[u] = \int \int 2\rho u_t v_t - 2T u_x v_x \, dx \, dt
\\ \underset{IBP, v(0, t) = v(l, t) = 0, etc}{=} \int_0^l[-2\rho\int_{t_1}^{t_2}u_{tt}v \, dt]\, dx -2T\int_{t_1}^{t_2}[\int_0^lu_{xx}v \, dx]dt
$$

$$
0 = \int_{t_1}^{t_2}\int_0^l(2\rho u_t v_t - 2T u_x v_x)\, dx \,dt = -2 \int_{t_1}^{t_2}\int_0^l (\rho u_{tt}- Tu_{xx})v\, dxdt
$$
Since $v$ is arbitrary we get: 
$$
\rho u_{tt} - Tu_{xx}
$$

##### 4. Semilinear Poisson Equation

If $F(x, z, p) = \frac{|p|^2}{2} + \Phi(z) $ then the E-L equation reads
$$
\Delta u = \phi(u), \qquad \text{where }\phi = \Phi'
$$
Indeed:
$$
\frac{\partial F}{\partial p_i} = p_i \implies \frac{\partial}{\partial x_i}(\frac{\partial F}{\partial p_i}(x, u, \nabla u)) = \frac{\partial }{\partial x_i}(u_{x_i})= u_{x_i x_i}
\\ \frac{\partial F}{\partial z} = \Phi'(z) = \phi(z) \implies \frac{\partial F}{\partial z}(x, u, \nabla u) = \phi(u)
$$

### Further Topics

* constraints $\rightarrow$ lagrange multipliers in E-L equation

* some constraints are toughter: general rule: 

  * integral constraints, e.g., $\int u\, dx =1$ are easier than pointwise contraints,
  *  e.g., $u \geq \phi(x)$ for some given function $\phi$ 

* The existence of a solution to $(*)$, i.e., a minimizer is highly non-trivial. For the math-experts: one would like to immitate the proof of the **Bolzano-Weierstrß Theorem** :

  * $u: K \rightarrow \R$ Cont., $K$ compact $\implies$ there exists max, min, in $K$ 

* Good intuition: Think of the space $\mathscr{A}$ being equipped with the "height function" $E: \mathscr{A} \rightarrow \R$, so we may think of an "energy landscape"

* How to find **saddle points**? Mountain pass lemma:

  ![image-20190428114434028](Math 126 Lecture 36.assets/image-20190428114434028.png)

$\min_{u_s}\max_{s \in [0, 1]}E[u_s]$ 
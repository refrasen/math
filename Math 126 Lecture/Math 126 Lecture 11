# Math 126 Book Notes Section 4.1-2

## 4.1 Separation of Variables, The Dirichlet Condition

**Homogeneous Dirichlet conditions** for the **wave equation** w/ some initial conditions
$$
(1)\qquad u_{tt} = c^2u_{xx} \quad \text{for }0<x<l
\\ (2)\qquad u(0,t) = 0 = u(l,t)
\\ (3) \qquad u(x,0) = \phi(x) \quad u_t(x,0) = \psi(x)
$$

##### Separated Solution

A solution of $(1)$ and $(2)$ of the form:
$$
(4) \qquad u(x,t) = X(x)T(t)
$$
**remark**: important to distinguish between the independent variable written as a lower case letter and the function written as a capital letter

**Goal 1**: Look for as many separated solutions as possible

* Plug $(4)$ into the wave equation $(1)$ 
  $$
  X(x)T''(t) = c^2X''(x)T(t)
  $$

* Divide by $-c^2XT$ 
  $$
  -\frac{T''}{c^2T} = -\frac{X''}{X} = \lambda
  $$

We get a quantity $\lambda$, a constant

**Proof that $\lambda$ is a constant**: 
$$
\partial \lambda/ \partial x= \frac{\partial}{\partial x}(-\frac{T''}{c^2T}) = 0
\\ \partial \lambda / \partial t = \frac{\partial}{\partial t}(-\frac{X''}{X}) = 0
$$
Alternative Proof:

$\lambda$ doesn't depend on $x$ because of the first expression

$\lambda$ doesn't depend on $t$ because of the second expression

$\implies$ $\lambda$ doesn't depend on any variable

**Assume**: $\lambda > 0$ (proof at end of section)

Let $\lambda = \beta^2$, where $\beta > 0$ 

1. Equations above are a pair of *separate* (!) **ordinary differential equations** for $X(x)$ and $T(t)$:

$$
(5) \qquad X'' + \beta^2 X = 0 \quad \text{and} \quad T'' + c^2 \beta^2 T = 0
$$

These ODEs are easy to solve:

using $r^2 + \beta^2 = 0$, $s^2 + c^2\beta^2$, we get $e^{\beta i}$ and $e^{c\beta i}$ 
$$
(6) \qquad X(x)  = C\cos \beta x + D \sin \beta x
\\ (7) \qquad T(t) = A\cos\beta c t + B \sin \beta c t
$$
where $A, B, C$ and $D$ are constants

* We can use that the real and imaginary parts of the solution are solutions themselves...?

$X = x +iy$ is a solution to $AX''  +BX'  + CX = 0$

$(Ax'' + Bx' + Cx) + i(Ay'' + By' + Cy) = 0$ 

2. Impose the boundary conditions (2) on the separated solution

$X(0) = 0 = X(l)$: 

$0 = X(0) = C$ and $0 = X(l) = D \sin \beta l$ 

Not interested in $C = D = 0$: We must have $\beta l = n\pi$ $\implies$ $\sin \beta l = 0$ 

so that $D$ $\neq 0$ 
$$
(8) \qquad \lambda_n = (\frac{n\pi}{l})^2, \quad X_n(x) = \sin \frac{n\pi x}{l} \quad (n = 1, 2, 3, ...)
$$
**What happened to D?**: it is an arbitrary constant: each $\sin$ above can be multiplied by an arbitrary constant.

Therefore, **there are an infinite (!)** number of separated solutions of $(1)$ and $(2)$, one for each $n$ 
$$
u_n(x,t) = (A_n \cos \frac{n\pi c t}{l} + B_n \sin \frac{n\pi c t}{l})\sin \frac{n\pi x }{l}
$$
($n = 1, 2, 3, ...$), where $A_n$ and $B_n$ are arbitrary constants

**The sum** of solutions is again a solution, so **any finite sum** is a solution of $(1)$ and (2)
$$
(9) \qquad u(x,t) = \sum_{n}(A_n \cos \frac{n\pi c t}{l}+B_n\sin \frac{n\pi c t}{l})\sin \frac{n\pi x}{l}
$$
Formula $(9)$ solves $(3)$ as well $(1)$ and $(2)$, provided that it satisfies the initial conditions, i.e.:
$$
(10) \qquad \phi(x) = \sum_{n} A_n \sin \frac{n\pi x}{l}
\\ (11) \qquad \psi(x) = \sum_{n} \frac{n\pi c}{l}B_n \sin \frac{n \pi x}{l}
$$

### Infinite sums

What kind of data pairs $\phi(x), \psi(x)$ can be exapnded as in $(10), (11)$ for some choice of coefficients $A_n, B_n$?

**Practically any (!) function $\phi(x)$ on the interval $(0,l)$ can be expanded in an infinite series $(10)$** (proven in chapter 5)

* Need to know convergence and differentiability of infinite series like (9)

**Fourier sine series on $(0,l)$: Formula $(10)$ above**

#### Implications

* $(11)$ is the same kind of series for $\psi(x)$ as $(10)$ is for $\phi(x)$ 
* if $(10)$ and $(11)$ are true, then the infinite series $(9)$ is a solution of the **whole problem** $(1), (2), (3)$ 

##### Sketch of first few sine functions $n = 1, 2, 3, 4$ 

The more squished, the higher the $n$ , $\sin(\frac{n\pi x}l)$ 

![Screen Shot 2019-02-16 at 11.34.16 PM](Math 126 Book Notes Section 4.1.assets/Screen Shot 2019-02-16 at 11.34.16 PM.png)

The functions $\cos(n\pi ct/l)$ and $\sin (n\pi ct /l)$ which describe the behavior in *time* have a similar form **(What form?)**

* Infinite sums? infinite series?
* The sketch?

The coefficients of $t$ inside the sines and cosines, $n\pi c/l$ are the **frequencies** (note: sometimes it is defined as $nc/2l$)

### Violin String 

Frequences, as $c = \sqrt{T/\rho}$  
$$
(12) \qquad \frac{n\pi \sqrt{T}}{l\sqrt{\rho}} \quad \text{for } n = 1, 2, 3...
$$
The **"fundamental"** note of the string is the smallest of these: $\pi \sqrt{T}/(l\sqrt{\rho})$ (n = 1)

The **"overtones"** are *exactly* the double, the triple, and so on, of the fundamental!

### Diffusion

$$
\begin{cases} (13)  \qquad \text{DE: } u_t = ku_{xx} \quad (0<x<l, 0<t<\infin)
\\ (14) \qquad \text{BC: }u(0,t) = u(l,t) = 0
\\ (15) \qquad \text{IC: }(x,0) = \phi(x)\end{cases}
$$

**Solving**: 

Separate the variables $u = T(t)X(x)$ as before
$$
\frac{T'}{kT} = \frac{X''}{X} = -\lambda = \text{constant}
$$
$\implies$ 

$T(t)$ satisfies $T' = -\lambda k T$, whose solution is $T(t) = Ae^{-\lambda kt}$ 

and
$$
(16) \qquad -X'' = \lambda X \quad \text{ in } 0<x<l \quad \text{ with }X(0) = X(l) = 0
$$
**This is the same problem for $X(x)$ as before** and thus has the **same solutions** 

so $X(x) = \sin \frac{n\pi x}{l}$, and we know the sums of solutions are solutions so:
$$
(17) \qquad u(x,t) = \sum_{n=1}^{\infin} A_n e^{-(n\pi/l)^2kt} \sin (\frac{n\pi x}{l})
$$
is the solution of $(13) -(15)$ provided that (plugging $t = 0$ in to the above)
$$
(18) \qquad \phi(x) = \sum_{n=1}^\infin A_n \sin \frac{n\pi x}{l}
$$
**Our solution is expressible for each $t$ as a Fourier sine series in $x$ provided that the initial data are** 

#### Example: Tube of length $l$ 

* Diffusion of a substance in a tube of length $l$ 
* Each end of the tube opens up into a very large empty vessel
* The concentration $u(x,t)$ at each end is *essentially* zero
* Initial concentration $\phi(x)$ in the tube
* Concentration at all later times is given by $(17)$ 
* As $ t\rightarrow \infin$ each term in $(17)$ goes to $0$ 
  * The substance gradually empties out into the two vessels and less and less remains in the tube

---

### Eigenvalues, Eigenfunctions

##### Eigenvalues

$\lambda_n = (n\pi /l)^2$ are called eigenvalues

##### Eigenfunctions

$X_n(x) = \sin(n\pi x/ l)^2$ are called eigenfunctions

##### Reason for Terminology

They satisfy the conditions
$$
(19) \qquad -\frac{d^2}{dx^2}X = \lambda X, \quad X(0) = X(l) = 0
$$
This is an ODE with conditions at two points

Let $A$ be the operator $-\frac{d^2}{dx^2}$, which acts on the functions satisfying the Dirichlet boundary conditions

The diff eq. has the form $AX = \lambda X$

An eigenfunction is a solution $X \not \equiv 0$ of this equation and an eigenvalue is a number $\lambda$ for which there exists a solution $X \not \equiv 0$ 

**Analogy**: $N \times N$ matrix $A$ with a vector $X$ satisfying $AX = \lambda X$

* eigenvalues and eigenvectors (nonzero vectors only)

* at most $N$ eigenvalues for $N\times N$ matrix

But for the differential operator we are interested in, there are an **infinite number of eigenvalues $\pi^2/l^2, 4\pi^2/l^2, 9\pi^2/l^2, ...$ **

You could say that we are dealing with "infinite-dimensional linear algebra"

##### normal modes

eigenfunctions are called normal modes because they are the **natural shapes** of solutions that persist for **all time** 

##### Why are all the eigenvalues of this problem positive?

Proof of the claim from earlier.

1. Could $\lambda = 0$ be an eigenvalue?

   This implies that $X'' = 0$ so that $X(x) = C + Dx$, but $X(0) = X(l) = 0$ implies that $C = D = 0$ so we get $X(x)\equiv 0$, which cannot be an eigenfunction, so $\lambda$ is not an eigenvalue

2. Could $\lambda<0$? 

   Let's write it as $\lambda = -\gamma^2$. Then $X'' = \gamma^2X$ so that

   $X(x) = C \cosh \gamma x + D \sinh \gamma x$ 

   Why?:

   $r^2 = \gamma^2$ so $r_1 = \gamma$, $r_2 = -\gamma$ 

   $X(x) = C_1e^{\gamma x} + C_2 e^{-\gamma x}$ 

   $C_1 = \frac{C +D}{2}$ 

   $C_2 = \frac{C-D}{2}$ 

   **so we do a "sort  of " change of variables for the constants**

   **$X(x) = \frac{Ce^{\gamma x} + Ce^{-\gamma x}}{2} + \frac{De^{\gamma x}-De^{-\gamma x}}{2}$ $ = C \cosh (\gamma x) + D \sinh (\gamma x)$ ** 

   **back to analysis** 

   $X(0) = C = 0$ and $X(l) = D\sinh \gamma l $ and since $\sinh \gamma \neq 0$, $D = 0$ 

   so we end up with $X \equiv 0$, which can't be an eigenfunction

3. could $\lambda$ be complex?

   Let $\gamma$ be either one of the two square roots of $-\lambda$, the other is $-\gamma$ 

   Then:

   $X(x) = Ce^{\gamma x} + De^{-\gamma x}$ 

   complex exponential function... (5.2)

   we obtain: $0 = X(0) = C + D$ and $0 = Ce^{\gamma l }+ De^{-\gamma l}$ 

   multiply by $e^{\gamma l}$: $Ce^{\gamma l}e^{\gamma l} + D =0$ $\implies$ $Ce^{2\gamma l} +D =0$ 

   if $e^{2\gamma l} \neq 1$, we would have a contradiction with $C +D =0$ 

   so $e^{2\gamma l} = 1$, which means, if $\gamma= a + ib$ 

   $e^{2al + 2ibl} = e^{2l*a}(\cos 2bl + i \sin 2bl)$ 

   $2la = 0$ $\implies$ $a = 0$ 

   so $2bl = 2\pi n​$, for some integer $n​$ $\implies​$ $b = \frac{\pi n}{l}​$ 

   so $\gamma = i \frac{n \pi }{l}$ and $-\lambda = -\gamma^2 = -n^2\pi^2/l^2$ which is real and positive, contradiction to $-\lambda$ being negative which would result in complex roots

**Thus: the only eigenvalues $\lambda$ of our problem $(16)$ are positive numbers**: $(\pi/l)^2, (2\pi/l)^2, ..., (n\pi/l)^2$ 

since from $3$, we got what eigenvalues are for the problem

## 4.2 The Neumann Condition

Take $(4.1.2)$ and replace it with $u_x(0,t) = u_x(l,t) = 0$ 

Then the eigenfunctions are the solutions $X(x)$ of
$$
(1) \qquad -X'' = \lambda X, \quad X'(0) = X'(l) = 0
$$
other than the trivial solution $X(x) \equiv 0$ 

1. Search for positive eigenvalues $\lambda = \beta^2 > 0$ 

As in $(4.1.6)$: $X(x) = C\cos \beta x + D \sin \beta x$, so that

$X'(x) = -C\beta \sin \beta x + D\beta \cos \beta x$ 

Using the boundary conditions:

$0 = X'(0) = D\beta$ $\implies D = 0$

$0 = X'(l) = -C\beta \sin \beta l$ $\implies$ $\beta l = \pi n$ $\implies$ $\beta = \pi n/l$ 

and along with $\lambda = \beta^2$ means the following
$$
\begin{cases} (2) \qquad \text{Eigenvalues: }(\frac{\pi}{l})^2, (\frac{2\pi}{l})^2, ...
\\ (3) \qquad \text{Eigenfunctions: }X_n(x) = \cos \frac{n\pi x}{l} \quad (n = 1, 2, ...) \end{cases}
$$

2. Check whether zero is an eigenvalue

Set $\lambda = 0$ in the ODE

$X'' = 0$ $\implies$ $X(x) = C + Dx$ and $X'(x) \equiv D$

The Neumann boundary conditions are satisfied if

$D = 0$ and $C$ can be any number, so $X(x) = C$ $\not \equiv 0$ 

Therefore: $\lambda = 0$ *is an eigenvalue* w/ any constant function as is its eigenfunction

3. Check for other cases of eigenvalues: negative, complex

It can be shown directly as in the Dirichlet case that there is no eigenfunction (also see section 5.3)

**Therefore** The list of all eigenvalues is
$$
(4) \qquad \lambda_n = (\frac{n\pi}{l})^2 \quad \text{for } n = 0, 1, 2, 3, ... .
$$
Note that $n = 0$ is included this time

---

### Diffusion Equation

The solution with Neumann BC's has the solution
$$
(5)\qquad u(x,t) = \frac{1}{2} A_0 + \sum_{n=1}^\infin A_n e^{-(n\pi/l)^2kt} \cos \frac{n\pi x}{l}
$$
This solution requires the initial data to have the "Fourier cosine expansion"
$$
(6) \qquad \phi(x) = \frac{1}{2}A_0 + \sum_{n=1}^\infin A_n \cos \frac{n\pi x}{l}
$$

* All coefficients $A_0, A_1, A_2$ are just constants
* first term in $(5)$ and $(6)$ ($\frac{1}{2}A_0$) comes from the eigenvalue $\lambda = 0$ 
  * So we aren't expected to know why this is written this way until Section 5.1 when its convenience will become apparent

#### Behavior of $u(x,t)$ as $t \rightarrow +\infin$ 

All but the first term contains an exponentially decaying factor, so $u(x,t)$ decays quite fast to $\frac{1}{2}A_0$, a constant

These boundary conditions correspond to insulation at both ends, so the fact that $u(x,t)$ eventually remains at a constant tempreature makes sense and agrees with the solution "spreading out"

**they omit the verification of convergence…** 

### Wave Equation

The eigenvalue $\lambda = 0$ then leads to $X(x) = \text{constant}$ (as seen before) and to the differential equation $T''(t) = \lambda c^2 T(t) = 0$, which has the solution $T(t) = A+Bt$ 

Therefore, the wave equation with Neumann BCs has the solutions
$$
(7)  \qquad\qquad u(x,t) = \frac{1}{2}A_0 + \frac{1}{2}B_0 t \\ + \sum_{n=1}^\infin (A_n \cos \frac{n\pi c t}{l}+ B_n \sin \frac{n\pi c t}{l})\cos \frac{n\pi x}{l}
$$
**The factor $\frac{1}{2}$ will be justfied later** 

Initial conditions:
$$
(8) \qquad \phi(x) = \frac{1}{2}A_0 + \sum_{n=1}^\infin A_n \cos \frac{n\pi x}{l}
$$
and
$$
(9) \qquad \psi(x) = \frac{1}{2}B_0 + \sum_{n=1}^\infin \frac{n\pi c}{l}B_n \cos \frac{n\pi x}{l}
$$
(All I did was the obvious: plug in and differentiate then plug in)

---

### Mixed Boundary Condition

Dirichlet at one end, Neumann at the other

Example: $u(0,t) = u_x(l,t) = 0$ 
$$
(10) \qquad -X'' = \lambda X \quad X(0) = X'(l) = 0
$$
Eigenvalues turn out to be: $(n+\frac{1}{2})^2 \pi^2/l^2$ 

Eigenfunctions: $\sin[(n+\frac{1}{2})\pi x/l]$ for $n = 0, 1, 2, …$ (this is shown in Exercise 1 and 2) 

#### Example

**Schrödinger** equation $u_t = iu_{xx}$ in $(0,l)$ with the Nuemann BCs $u_x(0,t) = u_x(l,t) =0 $ and initial condition $u(x,0) = \phi(x)$ 

**Separation of variables** leads to the equation
$$
\frac{T'}{iT} = \frac{X''}{X} = -\lambda = \text{constant}
$$
$u(x,t) = T(t)X(x)$ 

$u_t(x,t) = T'(t)X(x)$, $u_{xx} = T(t)X''(x)$ 

so $T'(t)X(x) = iT(t)X''(x)$ 

Then dividing by $iTX$ gives the above.

so that $T(t) = e^{-i\lambda t}$ and $X(x)$ satisfies the usual ODE $(1)$ 

so the solution is
$$
u(x,t) = \frac{1}{2}A_0 + \sum_{n=1}^\infin A_n e^{-i(n\pi/l)^2t}\cos \frac{n\pi x}{l}
$$
The initial condition requires the cosine expansion $(6)$ 

(this is just the diffusion equation but with $k =i$?) 
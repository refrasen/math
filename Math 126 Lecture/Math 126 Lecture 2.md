# Math 126 Lecture 2

# "Review" of Multivariable Calculus

## A. Subsets of $\R^d$ 

### Notation

$x = (x_1, . . ., x_d) \in \R^d, y = (y_1, . . ., y_d) \in \R^d$ 

$|x| = (\sum_{i=1}^d x_i^2)^{1/2}$ 

$x\cdot y = \sum_{i=1}^d x_i y_i$ (**inner product**)

##### Def $B_r(x)$ 

Open ball centered on $x$ with radius $r > 0$, defined by $\{ y \in \R^d | |x-y| < r\}$ 

##### Def open subset

A subset $A \sub \R^d$ is called <u>open</u> if for each $x \in A$, there exists $r > 0$ such that $B_r(x) \sub A$ 

If $D \sub \R^d$ is <u>open</u>, we write $\partial D$ for its boundary

## B. Differentiation

If $f: D \rightarrow \R$, $D \sube \R$ (D is open)

$\frac{\partial f}{\partial x_i}(x) = f_{x_i}(x) = \lim_{h\rightarrow 0} \frac{x_1, ..., x_i + h, ..., x_d) - f(x_1, . . ., x_d)}{h}$ 

$\frac{\partial ^2 f}{\partial x_i \partial x_j} = \frac{\partial }{\partial x_i}(\frac{\partial f}{\partial x_j}) = f_{x_i x_j}$ 
$$
f_{x_i x_j} = f_{x_j x_i} \text{ for all } 1 \leq i, j \leq d
$$

##### gradient of $f$ 

$\nabla f = (f_{x_1}, . . ., f_{x_d})$ , (is a vector field)

##### Def Laplacian

If $f: D \rightarrow \R$, we write $\triangle f = \sum_{i=1}^d f_{x_i x_i}$, $\triangle f$ is called the "Laplacian"

##### Def Vector Field

Let $\underline{F}: \R^d \rightarrow \R^d$ be a vector field

$\underline{F}(x) = (F^1(x), F^2(x),. . . , F^d(x))$ 

**divergence**: 
$$
\text{div }\underline{F} = \sum_{i = 1}^d \frac{\partial F^i}{\partial x_i}
$$
also $\text{div }\underline{F} = \nabla \cdot \underline{F}$ 

**Remark**: $\triangle f = \text{div }\nabla f = \nabla \cdot \nabla f$ 

Can think of $\nabla f$ as $\underline F$ and proceed normally.

## C. Integration

Let $D \sub \R^2$. the graph $S$ of $f: D \rightarrow \R$ is going to be a surface in $\R^3$ 

![Image result for multivariable calculus D and graph of f](http://tutorial.math.lamar.edu/Classes/CalcIII/SurfaceIntegrals_Files/image001.png)

### **d - dim "volume integral"** 

volume is $\int_{D} f(x)dx$ 

**Remarks**:

$x = (x_1, . . ., x_d)$ 

$dx = dx_1dx_2\cdots dx_d$ 

alternative notation: $\int \int \int _D f dx_1 dx_2 dx_3$ (d = 3)

#### Another Interpretation

$f(x) = $ density (of some quantity) at point $x$

$\int_D f(x)dx = $ total amount (of the quantity) within $D$ 

### Surface Integral $\R^d$ 

let $\Sigma$ be a (d-1) dimensional surface lying in $\R^d$ ($\sub \R^d)$ 

Let $f: \R^d \rightarrow \R$ 

then $\int_{\Sigma}f dS$ is the integral of $f$ over the surface $\Sigma$

#### How to define $\int_{\Sigma}f dS$ 

let $E$ be a set in $\R^{d-1}$, so $E \sub \R^{d-1}$ 

and let $g$ be a function from $\R^{d-1}$ to $\R$ 

let $\Sigma$ be the graph of $g$ over the set $E \sub \R^{d-1}$ 

then 
$$
\int_{\Sigma}f d\Sigma = \int _E f(x_1, ..., x_{d-1}, g(x_1, ..., x_{d-1})) \sqrt{1 + |\nabla g|^2}dx
$$
where $dx = dx_1dx_2 \cdots dx_{d-1}$ 

and $\sqrt{1 + |\nabla g |^2}dx = dS$ 

### Divergence Theorem - Fundamental Theorem of Multivariable Calculus

Let $D \sub \R^d$ and $\underline F: D \rightarrow \R^d$ be a vector field

$\underline F(x) = (F^1(x), ..., F^d(x))$ 
$$
\int_D \text{div }\underline F dx = \int_{\partial D}\underline F \cdot \underline n \;dS
$$
Here, $\underline n$ is the <u>outward</u> <u>pointing</u> <u>unit</u> <u>normal</u> vector field to $\partial D$ 

**unit**: length is equal to 1, so $|n| = 1$

**normal** $\implies$ $\underline n$ is perpendicular to $\partial D​$ 
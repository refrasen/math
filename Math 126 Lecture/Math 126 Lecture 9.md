# Math 126 Lecture 9

## (3.3) Diffusion With A Source

$$
(1)\begin{cases} u_t - ku_{xx} = f(x,t) & (-\infin<x<\infin, 0<t<\infin)\\ u(x,0) = \phi(x) \end{cases}
$$

Where $f(x,t), \phi(x)$ are given functions

**For instance**: 

if $u(x,t)$ = temperature of a rod $\implies$ $\phi(x)$ is the initial temperature distribution and $f(x,t)$ is a source (or sink) of heat provided to the rod at **later** times

#### Claim: Solution to This Problem

$$
(2) \qquad u(x,t) = \int_{-\infin}^\infin S(x-y, t)\phi(y)dy + \int_0^t\int_{-\infin}^\infin S(x-y, t-s)f(y,s)dyds
$$

* Both terms involve the source function $S$
* One term involves $\phi$ 
* Other term involves $f$ 

#### Analogy: ODE

$$
(3)\qquad \frac{du}{dt} + Au(t) = f(t), \qquad u(0) = \phi
$$

This is similar to:
$$
X'(t) = -AX(t)+f(t), \qquad X(0) = \phi
$$
where $\phi \in \R^{(N)}, A \in \R^{(N\times N)}$, let $N = 1$ 

The solution of $(3)$ is
$$
u(t) = e^{-tA}\phi+\int_0^t e^{-(t-s)A}f(s)ds
$$
where $e^{-tA} = \sum_{k=0}^\infin \frac{(-tA)^k}{k!}$ (if $N > 1$, this is a matrix)

Suppose $f \neq 0$: Multiply $(3)$ by $S(-t) = e^{tA}$ 
$$
\frac{d}{dt}[S(-t)u(t)] = S(-t)\frac{du}{dt} + S(-t)Au(t) =_{(3)} S(-t)f(t)
$$

---

note the first equality actually is the product rule, as $S'(t) = -Ae^{-tA}$ so $S'(-t) = --Ae^{tA} = Ae^{tA}$ $= AS(-t)$ 

---

Integrating from $0$ to $t$: 
$$
S(-t)u(t) -\phi = \int_0^tS(-s)f(s)ds
$$
Multiplying by $S(t) = [S(-t)]^{-1}$
$$
u(t) = S(t)\phi + \int_0^t S(t-s)f(s)ds
$$

---

Note: $S(t)S(-s) = e^{-tA}e^{sA} = e^{-(t-s)A}$ 

---

$S(t)\phi$: Represents homogeneous equation 

$\int_0^t S(t-s)f(s)ds$: Represents the effect of the source $f(t)$ 

##### Compare to our problem, (1)

The solution to $(1)$ will have two terms

1. The solution of the homogeneous problem: $\int_{-\infin}^\infin S(x-y,t)\phi(y)dy$ $ = (\mathscr{S}(t)\phi)(x)$ 

   $\mathscr{S}(t)$: **source operator**, transforming any function $\phi$ to the new function given by the integral

   Takes $\phi$ and adds another variable $y$ as an argument to $\phi$, multiplies by $S(x-y, t)$, where $t$ is an argument of the operator, then integrates w.r.t. $y$ from $-\infin$ to $\infin$ 

   Operators transform functions to functions

2. ??? particular solution???

**Guess**: 

In analogy to formula $(5)$ our guess:
$$
u(t) = \mathscr{S}(t)\phi + \int_0^t \mathscr{S}(t-s)f(s)ds
$$
This is exactly the same as (2)
$$
u(x,t) = \int_{-\infin}^\infin S(x-y, t)\phi(y)dy + \int_0^t \int_{-\infin}^\infin S(x-y, t-s)f(y, s)dy ds
$$
**This was the operator method** 

#### Proof of Claim

WTS: $u$ defined by $(2)$ solves $(1)$ and since the solution of $(1)$ is unique, we would then know that $u(x,t)$ is that unique solution

Without Loss of Generality, we can let $\phi \equiv 0$, as we understand this term already and can use linearity (as the first integral is just the solution to the homogeneous equation)

**First**: verify PDE
$$
\frac{\partial u}{\partial t} = \frac{\partial}{\partial t}\int_0^t \int_{-\infin}^\infin S(x-y, t-s)f(y,s)dyds
\\ = \int_0^t\int_{-\infin}^\infin \frac{\partial S}{\partial t}(x-y, t-s)f(y, s)dyds + \lim_{s \uparrow t} \int_{-\infin}^\infin S(x-y, t-s)f(y, s)\;dy 
$$
![image-20190213002324271](Math 126 Lecture 9.assets/image-20190213002324271.png)

So $I(t) = \int_{a(t)}^{b(t)}f(s,t)ds​$ where $f(s,t) = \int_{-\infin}^\infin S(x-y, t-s)\phi(y)dy​$ 

Now using that **(1)** $S(x-y, t-s)$ satisfies the diffusion equation... <u>(did we prove this at some point?) Answer: yes, because derivatives of solutions are solutions, and we proved that $Q$ is a solution</u>, **(2)** the fact that we can pull the spatial derivative out of the integral, and **(3)** the initial condition satisfed by $S$, so 

$S(x-y, t) = \frac{1}{\sqrt{4 \pi k t}} e^{-(x-y)^2/4kt}$ 

$\lim_{\epsilon \rightarrow 0} \int_{-\infin}^\infin S(x-y, \epsilon)f(y,t)\, dy$ $ =\lim_{\epsilon \rightarrow 0} \int_{-\infin}^\infin Q_x(x-y, \epsilon) f(y, t) \, dy$  $ =\lim_{\epsilon \rightarrow 0} \int_{-\infin}^\infin -\frac{\partial}{\partial y}Q(x-y, \epsilon)f(y, t) \, dy$  $ = \lim_{\epsilon \rightarrow 0} [\int_{-\infin}^\infin Q(x-y, \epsilon) f'(y, t) \, dy -Q(x-y, \epsilon)f(y,t)|_{-\infin}^\infin]$ assume $f(y,t)$ vanishes at $y = \pm \infin$ and plug $\epsilon = 0$ into $Q(x-y, \epsilon)$ 

$ = \int_{-\infin}^x f'(y, t) \, dy = f(x, t)$ 

Or easier: $\int_{-\infin}^\infin S(x-y, 0)f(y, t) dy = f(x, t)$ See discussion in 2.4

Therefore:
$$
\frac{\partial u}{\partial t} = k\frac{ \partial^2}{\partial x^2}\int_0^t\int_{-\infin}^\infin S(x-y, t-s)f(y, s)dyds + f(x,t)
$$
and since $u(x,t) = \int_0^t \int_{-\infin}^\infin S(x-y, t-s)f(y,s)dyds​$, we are finished. 

**Second**: verify initial condition

1st term in $(2)$ as $t \downarrow 0$ $\rightarrow\phi(x)$ (because of the intial condition of $S$)

2nd term in $(2)$ as $t \downarrow 0$ $\rightarrow$ 0 (because an integral from $0$ to $0$ is 0)

### Sources on a half-line

Use method of reflection~ (see Exercise 1) the only difference is the $f(x,t)$ factor...

**question**: would we extend$f(x,t)$ to be odd function on the whole line here as well?

**More complicated Problem**: **boundary source $h(t)$** on the half-line
$$
(9) \qquad \begin{cases} v_t - kv_{xx} = f(x,t) \qquad \text{for }0<x<\infin, 0 <t<\infin
\\ v(0,t) = h(t)
\\ v(x,0) = \phi(x)
\end{cases}
$$
Trick: "subtraction": Define $V(x,t) = v(x,t) - h(t)$ Then:
$$
(10) \qquad \begin{cases} 
V_t - kV_{xx} = f(x,t) - h'(t)
\\ V(0,t) = 0
\\ V(x,0) = \phi(x) - h(0)
\end{cases}
$$
This has a homogeneous boundary condition to which we can aply the method of reflection (**odd**)

Then set $v(x,t) = V(x,t) + h(t)$ & $V$ solves $(9)$ 

The domain of independent variables $(x,t)$ is a quarter-plane with specified conditions on both its half-lines, if they do not agree at the corner, $\phi(0) \neq h(0)$, then it is discontinuous there, but continuous everywhere else

Similarly for Neumann boundary conditions: $w_x(0, t) = h(t)$ subtract $xh(t)$, $W(x,t) = w(x,t) - xh(t)$, differentiation implies $W_x(0,t) = 0$ use **even reflection**

## (3.4) Waves with a Source

Want to solve
$$
(1) \qquad u_{tt} -c^2u_{xx} = f(x,t)
$$
on the whole line, together with the usual initial conditions
$$
(2) \qquad \begin{cases} u(x,0) = \phi(x) \\ u_t(x,0) =\psi(x) \end{cases}
$$
$f(x,t)$ can be interpreted as an external force acting on an infinitely long vibrating string

Due to $L = \partial_t^2 - c^2\partial_x^2$ being a linear operator, the solution will be the **sum of 3 terms**: one for $\phi$, one for $\psi$, and one for $f$ 

First two terms are already given in section 2.1 (homogeneous), so we must find the third term (particular)

### Theorem 1.

Unique solution of (1)(2) is given by
$$
(3) \qquad u(x,t) = \frac{1}{2}[\phi(x+ct)+\phi(x-ct)] + \frac{1}{2c}\int_{x-ct}^{x+ct}\psi + \frac{1}{2c}\int\int_{\Delta} f
$$
where $\Delta$ is the characteristc triangle of $(x,t)$![Screen Shot 2019-02-13 at 1.01.24 AM](Math 126 Lecture 9.assets/Screen Shot 2019-02-13 at 1.01.24 AM.png)

The double integral in $(3)$ is equal to the iterated integral
$$
\int_0^t\int_{(x-ct)+cs}^{(x+ct)-cs}f(y,s)dyds
$$
way to think about it:

* fix $s \in (0,t)$, then the left boundary is $(x-ct) + cs$ up to $(x+ct)-cs$ 
* if we fix $(x_0, t_0)$, then the left line: $x_l = (x_0 - ct_0) + tc$ and the right line: $x_r = (x_0 + ct_0) - ct$, where we think of $s$ as the usual $y$ axis

**The effect of a force $f$ on $u(x,t)$ is obtained by simply integrating $f$ over the past history of the point $(x,t)$ back to the inital time $t = 0$** (Causality Principle)

### Corollary: Well-Posedness

The problem $(1)(2)$ is well-posed

Proof of the corollary (taking theorem 1 for granted)

**Existence**: formula $(3)$

If $\phi$ has a continuous 2nd derivative, $\psi$ has a continuous first derivative, and $f$ is continuous, then $(3)$ yields a function $u$ with continuous second partials that satisfies the equation. 

**Uniqueness**: there are no other solutions of (1),(2): This will follow from any one of the derivations given <u>below</u>

So Theorem 1 proves the above 2. 

**Stability** 

If $(\phi, \psi, f)$ change a little, then $u$ also changes only a little

We need a way to measure the "nearness" of functions

**metric** or **norm** on function spaces

**uniform norms**: 
$$
||w|| := \max_{-\infin<x<\infin} |w(x)|
$$
and
$$
||w||_{T} = \max_{-\infin<x<\infin, 0 \leq t \leq T}|w(x,t)|
$$
Here, $T$ is fixed. $0 <T<\infin$ 

Suppose Let $u_i(x,t)$ be solution with $(\phi_i(x), \psi_2(x), f_i(x,t))$ ($i = 1, 2$, six functions)

We have the same formula $(3)$ satisfed by $u_1$ and by $u_2$ except for the different data

We let $u = u_1 - u_2$. Since $\Delta = ct^2$, we have from $(3)$ the inequality (Theorem 1)
$$
|u(x,t)| \leq \frac{1}{2}|\phi(x+ct)-\phi(x+ct)| + \frac{1}{2c}|\int_{x-ct}^{x+ct}\psi(s)ds| + \frac{1}{2c}|\int\int_{\Delta}f|
\\ = \max|\phi| + \frac{1}{2c}\cdot \max|\psi|\cdot(2ct) + \frac{1}{2c}\cdot \max|f| \cdot ct^2
\\ = \max|\phi| + t \cdot \max|\psi| + \frac{t^2}{2}\cdot \max|f|
\\ \text{ and with }0 \leq t \leq T
\\ \leq ||\phi|| + T ||\psi|| + \frac{T^2}{2}||f||_T
$$
So here, we used the triangle inequality and $ML$ estimate principle for integrals

Therefore, when we let $0 \leq t \leq T$ and use the definitions above for uniform norms
$$
||u_1 - u_2||_{T} \leq ||\phi_1 - \phi_2|| + T||\psi_1 - \psi_2|| + \frac{T^2}{2}||f_1 - f_2||_T
$$
so for any $\epsilon > 0$, let $\delta \leq \epsilon/(1+T+T^2)$ and let $||\phi_1-\phi_2||, ||\psi_1 - \psi_2||, ||f_1 - f_2||_T$ each be $<\delta$ 
$$
||u_1-u_2||_T < \delta(1+T+T^2)\leq \epsilon
$$

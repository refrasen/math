# Math 126 Lecture 28

## LaPlace Operator

$\frac{1}{r}$: harmonic in $\R^2 \{\bold 0 \}$ (where $r = |\bold x|)$ 

For $\phi \in $ $\mathscr{D}$ we have seen that 
$$
\phi(\bold 0) = - \int \frac{1}{r} \Delta \phi( \bold x)\frac{d \bold x}{4 \pi }
$$
which means that
$$
 \Delta(- \frac{1}{4 \pi r}) = \delta
$$
Dirichlet problem
$$
\begin{cases} \Delta u = f & \text{in } D \\ u = 0 & \text{on } \partial D\end{cases}
$$
Then
$$
\begin{align}
u(\bold x_0) &= \int_DG(\bold x, \bold x_0)f(\bold x)\, d\bold x 
\\ &= \int_D \Delta G(\bold x, \bold x_0)u(\bold x)\, d \bold x
\end{align}
$$
and $u(\bold x_0) = \int \delta(\bold x - \bold x_0)u(\bold x)\, d \bold x$ on the left hand side so
$$
\Delta G(\bold x, \bold x_0) = \delta(\bold x - \bold x_0)\qquad \text{in }D
$$

## Diffusion Equation

The source function satisfies
$$
\begin{align}
S_t - k \Delta S &= 0  && \bold x \in \R^3, - \infin < t< \infin
\\ S(\bold x, 0) &= \delta(\bold x)
\end{align}
$$
Let $R(x, t) := S(x-x_0, t-t_0)$, $t > t_0$ and put 

$R(x, t) := 0$ for $t < t_0$ 

Then $R$ satisfies
$$
R_t - k \Delta R = \delta(x-x_0)\delta(t-t_0) \qquad \text{for }-\infin <x<\infin, - \infin < t< \infin
$$

## Wave Equation

Source function satisfies
$$
\begin{align}
S_{tt} - c^2 \Delta S &= 0 && \bold x \in \R^3, -\infin < t < \infin
\\ S(\bold x, 0) &= 0
\\ S_t(\bold x, 0) &= \delta(\bold x)
\end{align}
$$
and is called the **Riemann Function**

Finding a formula: Let $\psi \in \mathscr D$ 
$$
u(\bold x, t) = \int S(\bold x - \bold y, t)\psi(\bold y)\, d \bold y
$$
$u$ satisfies wave eq. w/ $u = 0, u_t = \psi $ at $t = 0$ 

**one dimension**: 
$$
\int_{-\infin}^\infin S(x-y, t)\psi(y)\, dy = \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(y)\, dy \quad t \geq 0
$$
Hence: 
$$
S(x,t) = \begin{cases}\frac{1}{2c}& |x| <ct \\ 0 & |x| >ct \end{cases}
$$

## Fourier Transforms

$(-l, l) \rightarrow $ Fourier series

$(-\infin, \infin) \rightarrow $ Fourier transform

### Motivation

Given function $f = f(x)$ then for all $0 < l < \infin$: 
$$
f(x) = \sum_{n = - \infin}^\infin c_n e^{\frac{i n \pi x}{l}}\, dy
$$
where
$$
c_n = \frac{1}{2l}\int_{-l}^l f(y)e^{\frac{-i n \pi y}{l}}dy
$$
Then one can attain the Fourier Integral by letting $l \rightarrow \infin$. **idea**: 
$$
f(x) = \frac{1}{2 \pi }\sum_{n = -\infin}^\infin (\int_{-l}^l f(y)e^{-i k_n y}dy)e^{ik_n y}\frac{\pi}{l} \qquad (k_n = \frac{n \pi }{l})
\\ \underset{l \rightarrow \infin}{\rightarrow} \frac{1}{2 \pi}\int_{-\infin}^\infin (\int_{-\infin}^\infin f(y) e^{-iky}dy)e^{iky}dk
$$
This can be made rigorous. Another way to state this:
$$
f(x) = \int_{-\infin}^\infin F(k)e^{ikx}\frac{dk}{2\pi}
$$
where
$$
F(k) = \int_{-\infin}^\infin f(y)e^{-ikx}\, dx
$$
is called the **Fourier Transform** of $f$ 

Notation in other txtbooks:
$$
F(k) = \hat f(k) = \mathscr F(f)(k)
\\ \hat f(k)= \frac{1}{\sqrt{2 \pi}}\int_{-\infin}^\infin f(x)e^{-ikx}\, dx
$$

### List of Important Transforms

|                       | $f(x)$        | $F(k)$                                                       |
| --------------------- | ------------- | ------------------------------------------------------------ |
| $\delta$ Distribution | $\delta(x)$   | 1                                                            |
| Square Pulse          | $H(a - |x|)$  | $\frac{2}{k}\sin(ak)$                                        |
| Exonential            | $e^{-a |x|}$  | $\frac{2a}{a^2 + k^2}$ (a >0)                                |
| Heaviside Function    | $H(x)$        | $\pi \delta(k) + \frac{1}{ik}$                               |
| Sign function         | $H(x)- H(-x)$ | $\frac{2}{ik}$                                               |
| Constant              | 1             | $2 \pi \delta(k)$ (this explains the $\pi \delta(k)$ term in the Heaviside fct when $k = 0$) |
| Gaussian              | $e^{-x^2/2}$  | $\sqrt{2 \pi }e^{-k^2/2}$                                    |

Further properties

| Function           | Transform                                  |
| ------------------ | ------------------------------------------ |
| $\frac{d}{dx}f(x)$ | $ik F(k)$                                  |
| $xf(x)$            | $i \frac{dF}{dk}$                          |
| $f(x-a)$           | $e^{-iak}F(k)$                             |
| $e^{iax}f(x)$      | $F(k-a)$                                   |
| $a f(x)+ bg(x)$    | $aF(k) + b G(k)$                           |
| $f(ax)$            | $\frac{1}{|a|}F(\frac{k}{a})$ $(a \neq 0)$ |

#### Plancherel's Theorem: Analogue of Parseval's Identity

$$
\int_{-\infin}^\infin |f(x)|^2 \, dx = \int_{-\infin}^\infin |F(k)|^2 \frac{dk}{2 \pi}
$$

and
$$
\int_{-\infin}^\infin f(x)\overline g(x)\, dx = \int_{-\infin}^\infin F(k)\overline{G(k)}\frac{dk}{2\pi}
$$

#### Important operation in convolution:

GIven two function $f, g$, define their convolution 
$$
(f*g)(x)= \int_{-\infin}^\infin f(x-y)g(y)\, dy
$$
Source function for example is involved in convolutions we've done b4. 

---

We claim:
$$
\widehat{f*g} = F(k)G(k)
$$
"Fourier transform of convlution is the product"

**Proof**: 
$$
\begin{align}
\int (f * g)(x)e^{-ikx} \, dx &= \iint f(x-y)g(y)\, dy e^{-ikx}\, dx 
\\
&= \iint f(x-y)e^{-ikx}dx \, g(y)\, dy
\\ &=  \int g(y)\int f(z)e^{-ik(y+z)}dz\, dy
\\ &= F(k)G(k)
\end{align}
$$

---

### Higher Dimensions

$F(\bold k) = \int_{\R^d}f(\bold x)e^{-i \bold k\cdot \bold x}\, d \bold x$ 

and

$f(\bold x) = \int_{\R^d} F(\bold k)e^{i \bold k \cdot \bold x}\frac{d \bold k}{(2 \pi)^d}$ 

## Laplace Transform

Given $f = f(t)$, $t > 0$

Define
$$
F(s) = \int_0^\infin f(t)e^{-st}\, dt
$$
**Notation**: 

$F = Lf = \mathscr{L}f$ 

| $f(t)$                           | $F(s)$                              |
| -------------------------------- | ----------------------------------- |
| 1                                | $\frac{1}{s}$                       |
| $e^{at}$                         | $\frac{1}{s-a}$                     |
| $\cos (wt)$                      | $\frac{s}{s^2 + w^2}$               |
| $\sin (w t)$                     | $\frac{w}{s^2 + w^2}$               |
| $\cosh(at)$                      | $\frac{s}{s^2 - a^2}$               |
| $\sinh(at)$                      | $\frac{a}{s^2-a^2}$                 |
| $t^k$                            | $\frac{k!}{s^{k+1}}$                |
| $H(t-b)$                         | $\frac{1}{s}e^{-bs}$                |
| $\delta(t-b)$                    | $e^{-bs}$                           |
| $a(4\pi t^3)^{-1/2}e^{-a^2/4t}$  | $e^{-a\sqrt{s}}$                    |
| $(\pi t)^{-1/2}e^{-a^2/4t}$      | $\frac{1}{\sqrt{s}}e^{-a \sqrt{s}}$ |
| $1 - \mathscr{Erf} \frac{a}{4t}$ | $\frac{1}{s}e^{-a\sqrt{s}}$         |

**Some Properties** 

![image-20190412032502093](Math 126 Lecture 28.assets/image-20190412032502093.png)

Last property: transform of the "convolution" is the product of the transform

**inverse Laplace Transform**: not as simple as the inverse Fourier transform! This requires complex analysis


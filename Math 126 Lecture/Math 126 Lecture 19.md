# Math 126 Lecture 19

#### Recall: Green's First Identity

$$
\begin{align}
(\text G1) && \iint_{\partial D} u \frac{\partial v}{\partial n} dS &= \iiint_D \nabla u \cdot \nabla v \, d\bold x + \iiint_D u \Delta v \; d\bold x
\\
(\tilde{\text G}1) && \iint_{\partial D} v \frac{\partial u} {\partial n} dS &= \iiint_D \nabla u \cdot \nabla v \, d\bold x + \iiint_D v \Delta u \, d\bold x
\end{align}
$$

## Green's Second Identity

$$
(\text G2) = (\text G1 - \tilde{\text G}1) = \iiint_D (u\Delta v - v\Delta u)\, d\bold x = \iint_{\partial D} (u\frac{\partial v}{\partial n} - v \frac{\partial u}{\partial n})dS
$$

Let $\mathscr{L} = \Delta$. Think about matrices but with $(u, \mathscr L v)$ and $(v, \mathscr L u) $. if $(u, \mathscr L v) = (\mathscr L u, v)$, we have symmetry

#### Definition: Symmetric BCs

A BC is called symmetric for $\Delta$ in $D$ if
$$
\int_{\partial D}u \frac{\partial v}{\partial n} - v \frac{\partial u}{\partial n} \, dS = 0
$$
for all functions $u, v$ that satisfy the BC

##### Remark:

Homogeneous Dirichlet, Neumann, Robin BCs are all symmetric

### Representation Formula

If $\Delta u = 0$ in $D \sub \R^3$, then:
$$
(1) \qquad u(\bold x_0) = \int_{\partial D}[-u(\bold x)\frac{\partial}{\partial n}(\frac{1}{|\bold x - \bold x_0}|)+ \frac{1}{|\bold x - \bold x_0|} \frac{\partial u}{\partial n}]\frac{dS}{4\pi}
$$

#### Proof:

Apply $(\text G 2)$ to $u$ and $v = -\frac{1}{4\pi |\bold x - \bold x_0|}$

But: $v$ is not continuous at $\bold x_0$ in $D$, so we define:
$$
D_{\epsilon} = D\setminus \overline{B_{\epsilon}(\bold x_0)} = \{ \bold x \in D: |\bold x - \bold x_0 | \geq \epsilon \} 
$$
Without loss of generality: $\bold x_0 = 0$. Then we have $v(\bold x ) = -\frac{1}{4\pi r}, r = |\bold x |$ 

Now, recall: $\Delta v = 0 = \Delta u$ in $D_{\epsilon}$ (see spherical/polar coordinate formula for $\Delta$). So
$$
0 = -\frac{1}{4\pi}\int_{\partial D_{\epsilon}} [u \frac{\partial}{\partial n}(\frac{1}{r})- \frac{\partial u}{\partial n}(\frac{1}{r})]dS 
$$
Two parts of $\partial D_{\epsilon}$ $ = \partial D \cup \{ |\bold x| = \epsilon \}$ 

Since on $\{ |\bold x | = \epsilon \}$, $\bold n = -\frac{\bold x}{\epsilon}$, $\frac{\partial}{\partial n} - \frac{\partial}{\partial r}$ 
$$
- \frac{1}{4\pi} \int_{\partial D}[u \frac{\partial}{\partial n}(\frac{1}{r}) - \frac{\partial u}{\partial n}(\frac{1}{r})]\, dS = \frac{1}{4\pi}\int_{|\bold x| = \epsilon} [-u \underbrace{\frac{\partial}{\partial r}(\frac{1}{r})}_{= -\frac{1}{r^2} \\ = \frac{1}{\epsilon^2}} + \frac{\partial u}{\partial r}\frac{1}{r}]\, dS  
\\ (2) \qquad = \frac{1}{4\pi \epsilon^2} \int_{|\bold x| = \epsilon} u \, dS + \frac{1}{4\pi \epsilon}\int_{|\bold x| = \epsilon} \frac{\partial u}{\partial r}, dS
\\ \underset{\text{taking average}}= <u>_\epsilon + \epsilon<\frac{\partial u}{\partial r}>_{\epsilon}
$$
Because $u$ is continuous @ $\bold 0$: $< u> _\epsilon = u(\bold 0) $ as $\epsilon \rightarrow 0$ 

$|<\frac{\partial u}{\partial r}>_{\epsilon}| \leq \max | \nabla u| $ $\implies$ $\epsilon \cdot \text{bounded}$ $\underset{\epsilon \rightarrow 0}{\rightarrow 0}$ 

Taking $\epsilon \rightarrow 0$ in $(2)$ yields 1

#### In 2D

$$
u(\bold x_0) = \int_{\partial D}[u(\bold x)\frac{\partial}{\partial n}(\log |\bold x - \bold x_0) - \frac{\partial u}{\partial n}(\log|\bold x - \bold x_0|)] \, \frac{dS}{2\pi}
$$

## Green's Functions

We've used 2 properties of $v$ 

1. $\Delta v = 0$ in $\R^3 \setminus \{\bold x_0\}$ 
2. The precise shape of $v$ close to $\bold x_0$ 

#### Definition

The Green's Function $G(\bold x) = G(\bold x, \bold x_0)$ for the oeprator $- \Delta$ in the domain $D \sub \R^3$ at the point $\bold x_0 \in D$ is a function such that

1. $G(\bold x)$ is smooth in $\overline D \setminus \{ \bold x_0 \}$ with $\Delta G = 0$ in $\overline{D} \setminus \{ \bold x_0 \}$ 
   1. $\Delta G = 0$ in $\overline{D}\setminus\{ \bold x_0 \}$ 
2. $G(\bold x) = 0$ on $\partial D$ 
3. $H(\bold x) = G(\bold x) + \frac{1}{4 \pi |\bold x - \bold x_0|}$ is smooth and $\Delta H = 0$ everywhere in $\overline{D}$   
   1. Shape of $G$ close to $\bold x_0$ 

#### Rule

It can be shown that $G$ with these properties **exists** and is **unique**

$G = G(\bold x, \bold x_0)$ 

#### Theorem 1

If $G(\bold x, \bold x_0)$ is the Green function then the solution to 
$$
\begin{cases} \Delta u = 0 && \text{in }D
\\ u = g && \text{on }\partial D
\end{cases}
$$
is given by
$$
(3) \qquad u(\bold x_0) = \int_{\partial D}g(\bold x) \frac{\partial G}{\partial n}(\bold x, \bold x_0)\, dS
$$

##### Proof

From $(1)$: $v(\bold x) = -\frac{1}{4\pi |\bold x - \bold x_0|}$: 
$$
(4)\qquad u(\bold x_0) = \int_{\partial D}(u \frac{\partial v}{\partial n} - v \frac{\partial u}{\partial n}) \, dS
$$
Write $H(\bold x) = G(\bold x, \bold x_0) - v(\bold x)$, By 3., $H$ is smooth & harmonic in $\overline D$: 

By $(\text G2)$ for $u, H$: 
$$
(5) \qquad 0 = \int_{\partial D} (u \frac{\partial H}{\partial n} - H \frac{\partial u}{\partial n})\, dS 
$$
remembering that $\Delta H = \Delta u = 0$ which is why the left side is 0

then $(5) + (4)$ since $G = v+H$ yields $(3)$ 
$$
\begin{align} u(\bold x_0) &= \int_{\partial D}[ u (\frac{\partial H}{\partial n} + \frac{\partial v}{\partial n} ) - \frac{\partial u}{\partial n}(H+v)]\, dS
\\ &= \int_{\partial D} [u\frac{\partial G}{\partial n} - \frac{\partial u}{\partial n}G]\, dS
\end{align}
$$
and $u = g$ on $\partial D$  and $G$ vanishes on $\partial D$, so we end up with $(3)$ 

### Symmetry of Green's Function G

#### Theorem

G is symmetric: $G(\bold x, \bold x_0) = G(\bold x_0, \bold x)$, $(\bold x \neq \bold x_0)$ 

Proof (should just see the book):

* apply $(\text G2)$ to $u(\bold x) = G(\bold x, \bold a)$ and $v(\bold x) = G(\bold x, \bold b)$ and to the domain $D_{\epsilon}$, where iti s $D$ with two little spheres of radii $\epsilon$ cut out
* Boundary of $D_{\epsilon}$ consists of 3 parts: D and the two spheres
* Use the fact that $u$ and $v$ are essentially $G$, which is harmonic to make left side vanish
* Use the fact that $G$ disappears on the boundary to get rid of the integral over $\partial D$ with integrand $u \frac{\partial v}{\partial n} - v \frac{\partial u}{\partial n}$ 
* so we have that the sum of the two integrals with the integrand above over the spheres is equal to 0 for any radius of the spheres $\epsilon > 0$ (s.t. the spheres are still in $D$) 
* Take the limits as $\epsilon \rightarrow 0$ 
* expand the integrals, using definitions of $u$ and $v$ in their respective integral 

#### Theorem 2

The Solution to $\begin{cases} \Delta u = f && \text{in } D \\ u = g && \text{on } \partial D \end{cases}$ 

is given by 
$$
u(\bold x_0) = \iint_{\partial D} h(\bold x) \frac{\partial G(\bold x, \bold x_0)}{\partial n}\, dS + \iiint_{D} f(\bold x)G(\bold x, \bold x_0)\, d\bold x
$$
Proof left as exercise

#### 7.4 Half-Space and Sphere

#### Example: Half-Space

$D = \{(x, y, z): z > 0 \}$ 

**Notation**: If $\bold x = (x,y, z) \in D$, $\bold x^* = (x, y, -z)$ 

**Def**: The Green Function for the Half Space $D$ is
$$
G(\bold x, \bold x_0) = \frac{1}{4\pi}(-\frac{1}{|\bold x - \bold x_0|} + \frac{1}{|\bold x - \bold x_0^*|})
$$
"Odd reflection"
$$
H(\bold x) = \frac{1}{4\pi} \frac{1}{|\bold x - \bold x_0^*|}
$$
smooth and harmonic in $\R^3 \setminus \{ \bold x_0 ^* \}$ $\notin \overline{D}$ 

Going back to our Definition of a Green Function

1. $G$ is smooth everywhere in $\overline D$ except at $\bold x_0$  and $\Delta G = 0$ since we've seen this type of function to be harmonic before and the sum of two harmonic functions is harmonic
2. on $\partial D$: $z = 0$ $\bold x \in \partial D: |\bold x - \bold x_0| = |\bold x - \bold x_0^*| \implies G(\bold x, \bold x_0) = 0$ 
3. $H$ indeed is equal to $G - v$ and is smooth everywhere in $\overline{D}$ and $\Delta H = 0$ as we have seen before with $v$ (it is just a translation of $v$ I believe) 

##### Now: Solve a PDE

$$
\begin{cases}
\Delta u = 0 && \text{in } \{z > 0 \}
\\ u(x,y, 0) = h(x,y) && \text{Boundary Condition}
\end{cases}
$$

By the previous theorem: 
$$
\begin{align}
u( x_0,  y_0,  z_0) &=\iint_{\R^2} h(\bold x)\frac{\partial G(\bold x, \bold x_0)}{\partial n}\, dx\, dy
\\ &=\iint_{\R^2}h(\bold x) (- \frac{\partial G}{\partial z}) \, dx\, dy
\end{align}
$$
because $n$ is just a vector $(0, 0, -1)$ and dotting this with $\nabla G$ gives - $G_z​$ as above
$$
u(x_0, y_0, z_0) = \iint_{\R^2}h(\bold x)(\frac{1}{2\pi}\frac{z_0}{|\bold x - \bold x_0|^3}) dx dy
$$
$G(\bold x, \bold x_0) = \frac{1}{4\pi}(-\frac{1}{|\bold x - \bold x_0|} + \frac{1}{|\bold x - \bold x_0^*|})$ 

$\frac{\partial G}{\partial z} = \frac{1}{4\pi} ( \frac{z-z_0}{|\bold x - \bold x_0|^3} - \frac{z+z_0}{|\bold x - \bold x_0^*|^3})  $

since $|\bold x - \bold x_0| = \sqrt{(x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2}$ and 

$|\bold x - \bold x_0^*|$is the same as above, but with $(z+z_0)^2$ instead

and on $(x, y, 0)$, the two denominators are the same, and $z - z_0 - z - z_0 = -2z_0$ so

$\frac{\partial G}{\partial z}(x, y, 0) = \frac{1}{4\pi}(-\frac{2z_0}{|\bold x - \bold x_0|^3})$ which equals the above when we simplify and multiply by negative 1
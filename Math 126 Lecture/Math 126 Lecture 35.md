# Math 126 Lecture 35

## Solitons

A soliton is a localized traveling wave solution of a nonlinear PDE, which is stable

One such PDE is the **Korteweg- de Vries** Equation:
$$
(KdV)\qquad u_t + u_{xxx} + 6uu_x = 0, \qquad x \in \R, t > 0
$$
It desdribes water waves along a canal, first observed by Russell in 1834

It also appears in the Theory of plasmas, etc. 

---

There is an infinite hierarchy of preserved qualities, the most basic ones are
$$
\begin{align}
\text{mass} &= \int_\R u(x, t)\, dx
\\ \text{momentume}&= \int_{\R}u^2(x, t)\, dx
\\ \text{energy}&= \int_{\R}(\frac{1}{2}u_x^2(x, t)-u^3(x, t))\, dx
\end{align}
$$
Homework: show $d/dt(…) = 0$ 

---

##### Let's find a traveling wave solution, i.e., a solution of the form

$$
u(x, t) = f(x-ct)
$$

Then $f$ has to solve
$$
-cf' + f^{(3)}+ 6ff' = 0
$$
where $ff' = \frac{1}{2}(f^2)'$, so we have
$$
-cf + f'' + 3f^2 = a
$$
for some constant $a$ 

Multiply by $2f'$, we get
$$
-c(f^2)' + ((f')^2)' + 2(f^3)' = 2af'
$$
hence:
$$
-cf^2 + (f')^2 + 2f^3 = 2af + b 
$$
for some $a, b $ $\in \R$ 

We expect $f(s) \rightarrow 0$ as $s \rightarrow \pm \infin$, as well as $f'(s), f''(s)$ 

![image-20190427035800443](/Users/r/Library/Application Support/typora-user-images/image-20190427035800443.png)

So $a = b = 0$ and
$$
-cf^2 + (f')^2 + 2f^3 = 0
$$
The solution: 
$$
f(x) = \frac{c}{2}\text{sech}^2(\frac{1}{2}\sqrt{c}(x-x_0))
$$
where $x_0$ is a constant of integration and $\text{sech}(x) = \frac{2}{e^x + e^{-x}}$ = hyperbolic secant

###### Note:

The soliton $u(x,t) = f(x-ct)$ 

* travels to the right at speed c, 

* has amplitude $c/2$ 

  * | $c$ Large | $c$ Small |
    | --------- | --------- |
    | tall      | short     |
    | thin      | Fat       |
    | fast      | slow      |

---

**Many other PDEs have Soliton Solution** 

* cubic Schrödinger equation: $iu_t + u_{xx} + |u|^2 u = 0$ 
* Sine-Gordon equation: $u_{tt} - u_{xx} + \sin u = 0$ 
  * sine is a pun on Klein-Gordon equation $u_{tt} - u_{xx} + u =0 $ since $u$ is replaced by the sine function
* Kadomster-Petviashvili equation: $(\underset{KdV}{u_t + u_{xxx}+6uu_x})_x + 3u_{yy} = 0$ 

---

## Calculus of Variations

Recall: Dirichlet's Principle (7.1). Harmonic function $\mapsto$ least potential energy
$$
\begin{align}
\Delta u &= 0 \text{ in D} & & &u \text{ minimizes}
\\ u&= g \text{ in }\partial D & \iff & &E[w] = \frac{1}{2}\int |\nabla w|^2\, d\bold x 
\\ & & & & \text{along all }w \text{ with }w = g \text{ on }\partial D
\end{align}
$$
**Today**: 1-dimensional problems (ODE Theory for Calc of Var)

Given a function $F = F(x, z, p): \R \times \R \times \R \rightarrow \R$ (and $a < b$)

**define**
$$
E[u] := \int_a^b F(x, u(x), u'(x))\, dx \leftarrow \text{"action" functional}
$$
and
$$
\mathscr{A} = \{ \text{admisslbe functions} \} = \{ v: [a, b] \rightarrow \R | v(a) = \alpha, v(b) = \beta\} 
$$
![image-20190427041026325](Math 126 Lecture 35.assets/image-20190427041026325.png)

---

### Problem: Least Action Principle

$$
(*) \qquad \text{Find } u \in \mathscr{A}: E[u] = \min_{v \in \mathscr{A}} E[v]
$$

This is called the **Least Action Principle** 

#### Examples

##### 1: Length Functional and Geodesics 

If $F(x, z, p) = \sqrt{1 + p^2}$, then

$E[v] = \int_a^b \sqrt{1 + (v')^2}\, dx $ = length of graph

**Homework next week:** shortest path is a straight line

##### 2: Brachistochrone 

Design curve of quickest descent

![image-20190427044417634](Math 126 Lecture 35.assets/image-20190427044417634.png)

##### 3: Catenary: Hanging chain

Potential Energy: 
$$
\int_a^b pgu\sqrt{1 + (u')^2}\, dx
$$
![image-20190427044453078](Math 126 Lecture 35.assets/image-20190427044453078.png)

---

#### Theorem: Euler-Lagrange Equation

If $u$ solves $(*)$, then 
$$
(E-L)\qquad - \frac{d}{dx}[\frac{\partial F}{\partial p}(x, u, u')] + \frac{\partial F}{\partial z}(x, u, u')
$$
Here $\frac{\partial F}{\partial p}$ = derivative of $F$ with respect to last variable

​	$\frac{\partial F}{\partial z} = $ derivative of $F$ with respect to second variable 

##### Proof:

Suppose $u$ solves $(*)$ and suppose $w: [a, b] \rightarrow \R$ such that
$$
(1) \qquad w(a) = w(b) = 0
$$
Define for $s \in \R$ 

$e(s) := E[u + sw]$. 

Note that because $u$ solves $(*)$ and (1), $u(x) + sw(x) \in \mathscr{A}$  so
$$
e(0) = E[u] \leq E[u+sw] e(s) \qquad \forall s \in \R
$$
So by calculus, 
$$
0 = e'(0) = \frac{d}{ds}|_{s = 0} \int_a^b F(x, u(x)+ sw(x), u'(x)+sw'(x))\, dx
\\ = \int_a^b \frac{\partial F}{\partial z}w(x) + \frac{\partial F}{\partial p}w'(x)\, dx
\\ \overset{IBP, w(a)=w(b)= 0}{=} \int_a^b \frac{\partial F}{\partial z}w(x)\, dx - \int_a^b \frac{d}{dx}(\frac{\partial F}{\partial p}(x, u(x), u'(x))w(x)\, dx
\\ \int_a^b[\frac{\partial F}{\partial z}(x, u(x), u'(x))- \frac{d}{dx}(\frac{\partial F}{\partial p}(x, u(x), u'(x)))]w(x)\, dx = 0
$$
This is true for **any** $w$ satisfying $1$ so:
$$
\frac{\partial F}{\partial z}(x, u(x), u'(x)) - \frac{d}{dx}(\frac{\partial F}{\partial p}(x, u(x), u'(x))) = 0
$$
for all $x \in (a, b)$ 

---

#### Back to Examples

1: is homework

2: left blank

##### Example 3

$F(x, z, p) = z \sqrt{1 + p^2}$ 

say $p = g = 1$?
$$
\frac{\partial F}{\partial z} = \sqrt{1 + p^2}
\\ \frac{\partial F}{\partial p}  = z \frac{p}{\sqrt{1 + p^2}} \implies \frac{\partial F}{\partial p}(x, u(x), u'(x)) = u(x) \frac{u'(x)}{\sqrt{1 + (u')^2}}
$$
E-L equation is
$$
\sqrt{1 + (u')^2} - \frac{d}{dx}(u(x) \frac{u'(x)}{\sqrt{1 + (u')^2}}) = 0
$$
… compute …, 

$u(x ) \sim \cosh(x)$ How do we get this?

$\sqrt{1 + (u')^2} - \frac{u'(x)^2}{\sqrt{1 + (u')^2}} - u(x)\frac{u''(x)\sqrt{1 + (u')^2} - \frac{u'}{\sqrt{1+ (u')^2}}(u'(x))}{1+(u')^2} = 0​$

$\sqrt{1 + (u')^2} - \frac{u'(x)^2}{\sqrt{1 + (u')^2}} - u(x)\frac{u''(x)(1 + (u')^2) - (u')^2}{(1+(u')^2)^{3/2}} = 0$

$1 + (\not u')^2 - (\not u')^2 - u \frac{u''(x)(1+(u')^2) - (u')^2}{1+(u')^2} = 0$ 

$= 1 + (u')^2 - uu'' - uu''(u')^2 + u(u')^2 = 0$ 


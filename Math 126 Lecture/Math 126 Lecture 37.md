# Math 126 Lecture 37

## Gradient Flow

##### Definition

Let $X$ be a complete[^1] vector space with inner product $(\cdot, \cdot): X \times X \rightarrow \R$, 

[^1]: every Cauchy Sequence of points in $M$ has a limit thatis also in $M$

and let $E: X \rightarrow \R$ be differentiable. 

 (i.e.) $\forall u \in X$  $\exists$ linear map $dE(u): X\rightarrow \R$ such that $\lim_{v \rightarrow u} \frac{|E[u] - (E[u]+dE(u)[v-u])|}{\| v - u\|} = 0$ , 

$\| w \| = \sqrt{(w, w)}$, with $w = v - u$?

---

The **gradient** of $E$ is then the unique vector $\nabla E(u) \in X$ such that $dE(u)[w] = (\nabla E(u), w) \forall w \in X$ 

so $dE(u)$ maps $w \in X$ to the inner prodct of the vector $\nabla E(u)$ and $w$ for all $w \in X$ 

this way we have a proper mapping, i.e. only one value in $\R$ assigned to each $w$ 

The **gradient flow** of $E$ in $(X, (\cdot, \cdot))$ is 
$$
(GF)\qquad \begin{cases} \frac{d}{dt} u = - \nabla E(u) & t > 0 \\ u(0) =\phi & t = 0 \end{cases}
$$
in other words:
$$
(\frac{d}{dt}u, w) = - dE(u)[w] \qquad \nabla w \in X
$$

#### Need to know: 

Fix $E: X \rightarrow \R$, a differentiable map from the vector space to $\R$. For each $u \in X$: there exists

* $dE(u)[w] = (\nabla E(u), w)$, for all $w \in X$ , is a real number mapped to $w \in X$ 
  * Issue, how do we find $dE(u)[w]​$?
* $u_t = - \nabla E(u)$
  * take note of initial condition for $u$ 
* Combine the first two bullet points to get $(u_t, w) = -dE(u)[w]$ 

Steps are normally:

1. Take $E$, Compute $dE(u)[w]$ (seemingly by doing $\frac{d}{ds}|_{s = 0} E[u+sw]$ 

2. Then use $\bullet$: $dE(u)[w] = (\nabla E(u), w)$ and the definition of the inner product to calculate $\nabla E(u)$ 
3. Then use $u_t = - \nabla E(u)$ to get our equation, $(GF)$ 

#### Example

##### 1. Finite Dimensions

$X \in \R^N$, $E: \R^N \rightarrow \R$ convex, then 
$$
\frac{d}{dt}u = -\nabla E(u)
$$
is an ODE. E.g., $E = \frac{|u|^2}{2}$, then
$$
\frac{d}{dt}u = - u \implies u(t) = \phi e^{-t}
$$
so I believe $u = (u_1(t), … , u_n(t))$ 

so $\frac{d}{dt}u = (u_1'(t), …, u_n'(t))$ $\in X$ 

Side note in our example

$-\frac{dE}{d x_i} = -\frac{d}{dx_i}(\frac{x_1^2 + \cdots + x_n^2}{2}) =- x_i = -u_i(t) = u_i'(t)$ so we do have $u'(t) = - u(t)$ 

##### 2. Diffusion Equation

$X = \{ u: [a, b] \rightarrow \R| u(a)= u(b)= \phi \}, (u, v) := \int_a^b uv \, dx$

$X$ is the vector space of all functions $u$ that map $(a, b)$ to $\R$ and $u(a) = u(b) = \phi$  

and we define the inner product to be what you see above. 

**Recall**: This is indeed an inner product
$$
E[u] = \frac{1}{2}\int_a^b u_x^2 \, dx
$$
Q: What is $\nabla E(u)$? 

1. Compute $dE(u): $ Let $w \in X$, then 

   (Since when did $dE(u)[w]$ get computed this way?)
   $$
   dE(u)[w] = \frac{d}{ds}|_{s = 0} E[u+sw] = \frac{d}{ds}|_{s=0}\int_a^b (u_x + s w_x)^2 \, dx =   \int_a^b u_x w_x \, dx
   $$

2. Compute $\nabla E(u)$ 

$$
\int_a^b u_x w_x \, dx = dE(u)[w] = (\nabla E(u), w) = \int_a^b \nabla E(u)w \, dx
$$

Since $\int_a^b u_x w_x \, dx = \int_a^b - u_{xx}w\, dx$ 

We see that
$$
\nabla E(u) = - u_{xx}
$$
$\nabla$ w.r.t. $(\cdot, \cdot)$ 

Then the gradient flow is
$$
u_t = - \nabla E(u) = u_{xx}
$$
which is the diffusion equation 

##### 3. Diffusion equation again

$E[u] := \frac{1}{2}\int u^2 \, dx$ 
$(u, v) : = \int_a^b UV\, dx$, where 
$$
\begin{align}
U(x) &= \int_a^x u(y)\, dy
\\ V(x) &= \int_a^x v(y)\, dy
\end{align}
$$
**Check**: this is an inner product

* $(x, y) = \overline{(y, x)}$ : Conjugate Symmetry
  * Since it appears to be real numbers in this case, we don't need to worry too much about this since $\int_a^x u(y)\, dy \int_a^x v(y) \, dy$ is symmetric 
* $(ax + by, z) = a(x, z) + b(y, z)$: Linearity in the first argument
  * Integrals are linear
* $(x, x) >0$: positive definite 

**Question**: What is $\nabla E(u)$? 

1. Compute $dE(u)$: Let $w \in X$, then (same as before)

$$
dE(u)[w] = \frac{d}{ds}|_{x = 0}E[u + sw] = \int_a^ b uw \, dx
$$

2. Compute $\nabla E(u)$ 
   $$
   \int_a^b uw\, dx = dE(u)[w] = (\nabla E(u), w)
   \\ \int_a^b uw =  \int_a^b U_x W_x \, dx \underset{IBP}{=} \int_a^b-U_{xx}W \, dx = (-u_{xx}, w)
   $$

so again $\nabla E(u) = - u_{xx}$ and $u_t = - \nabla E(u) = u_{xx}$ 

##### 4. Diffusion equation once again 

Good flow of entropy $\int_a^b u \log u \, dx $ w.r.t. Wasrestein (?) distance (weiserstein?)

#### Theorem

A function $u$ *solves* $(GF)$ for $t \in (0, T)$ 

$\iff$ 
$$
(ED)\qquad E(u(T)) + \int_0^T (\frac{1}{2}\| \nabla E(u)\|^2 + \frac{1}{2}\| u_t \|^2)dt\leq E(u(0))
$$
**Remark**: This characterizes a gradient flow in terms of an inequliaty!

##### Proof

$(GF) \implies (ED)$: Chain Rule

it seems to me that $dE(u(t))$ is a sorta derivative of $E$? 
$$
\begin{align}
\frac{d}{dt}E(u(t)) &= dE(u(t))[u_t] = (\nabla E(u), u_t )
\\ & = \frac{1}{2}(\nabla E(u), u_t) + \frac{1}{2}(\nabla E(u), u_t)
\\ & \underset{GF}{=} -\frac{1}{2}\|\nabla E(u)\|^2 - \frac{1}{2}\|u_t\|^2
\end{align}
$$
$\implies​$ 
$$
E(u(T)) - E(u(0)) = \int_0^T - \frac{1}{2}\| \nabla E(u)\|^2 - \frac{1}{2}\|u_t\|^2 \, dt
$$
rearrange to get $(ED)$ with equality!

so it seems with (GF) we get equality

---

$(ED) \implies (GF)$: 

By (ED) $0 \geq E(u(T)) - E(u(0)) + \int_0^T \frac{1}{2}\|\nabla E(u)\|^2 + \frac{1}{2}\|u_t \|^2 \, dt$ 

$ = \int_0^T \frac{d}{dt}E(u(t))\, dt$ $+$ second term

Chain rule on first integrand, then use property of $E$ 

$ = \int_0^T dE(u(t))[u_t] \, dt= \int_0^T(\nabla E(u), u_t)\, dt$ 

Now adding and factoring out $1/2$ 

$ = \frac{1}{2} \int_0^T \|\nabla E(u) \|^2 + 2(\nabla E(u), u_t) + \| u_t \|^2 \, dt $ 

Integrand $ = \|\nabla E(u) + u_t)\|^2 \geq 0$ 

$\implies \|\nabla E(u) + u_t\|^2 = 0$ for all $t \in (0, T)$ $\implies$ $u_t = - \nabla E(u)$ for all $t \in (0, T)$ 

---

#### Examples

##### Example 1. $\R^N$ 

The ODE: $\frac{d}{dt}u = - u$ is characterized by
$$
\frac{1}{2}u^2(T) + \int_0^T \frac{1}{2}u^2 + \frac{1}{2}u_t^2 \, dt \leq \frac{1}{2}u^2(0)
$$

##### Example 2. Diffusion Eq. 

$$
\begin{cases} u_t = u_{xx} \\ u(a) = u(s) = 0 \\ u = \phi, t = 0 \end{cases} \iff \frac{1}{2}\int_a^b u_x^2 (x, t) \, dx + \int_0^T \int_a^b \frac{1}{2}u_{xx}^2 + \frac{1}{2}u_t^2 \, dx \, dt \leq \frac{1}{2}\int_a^b \phi_x^2(x, 0)\, dx
$$

In particular, the theorem says
$$
\frac{d}{dt}E[u(t)] \leq 0
$$

---

### Interesting Questions

a. Long time Behavior $u(x, t) \underset{t \uparrow \infin}{\rightarrow }U(x)$? 

1. Does $U$ solve $\nabla E(U) = 0$? 



b. Approximation/ Convergence of gradient flow?

1. How to construct solutions to $u_t = - \nabla E(u)$ in general?

a) Let's first consider finite dimensions: $u \in \R^N$, $E: \R^N \rightarrow \R$ 

Convex: $D^2 E(u) \geq \lambda 1d_{\R^n} = \begin{pmatrix} \lambda & & \\ & \lambda & \\ & & \lambda \end{pmatrix}​$ , so a symmetric matrix, positive definite

### ???  $\uparrow$ what does this mean?

Oh so it's the double gradient thing, so that would in fact be a matrix and this is a condition we need for convex. (?)

![image-20190430142433224](Math 126 Lecture 37.assets/image-20190430142433224.png)

---

And $E[u] \rightarrow \infin$ as $|u| \rightarrow \infin$ 

By (ED), $\int_0^\infin \frac{1}{2}\| \nabla E(u)\|^2 + \frac{1}{2}\| u_t \|^2 \, dt$  $\leq E(u(0))  = E(\phi)$

So there exists a sequence $t_n : \| \nabla E(u (t_n))\|^2 + \| u_t(t_n)\|^2 \rightarrow 0$ as $n \rightarrow \infin$ since the interval converges

The next part helps us find what $u$ may/can converge to as the above tends towards 0

For this sequence: $(t_n)_n$ $t_n \rightarrow \infin$ We have furthermore
$$
E(u(t_n)) \leq E(u(0)) = E(\phi)
$$
So $u(t_n)$ is bounded, so there exists a subsequence $t_{n_k} \rightarrow \infin$ and point $U \in \R^n$ such that

$u(t_{n_k}) \rightarrow U$ as $k \rightarrow \infin$ so $u(t_{n_k})$ converges to a solution that doesn't depend on $t$? is that what this implies? 

we also know that from ED, $\frac{d}{dt}E(u(t)) \leq 0$ ??

##### Is $U$ the unique minimizer of $E$? 

Yes, we Know that 

$0 = \lim_{k \rightarrow \infin} \nabla E(u(t_{n_k})) = \nabla E (U)$ 

so $\nabla E (U) = 0$ and hence $U$ is the unique minimizer

What happens in general if $E$ is not uniformly convex?

* One can get stuck in local minima 
* and rarely in saddle points
* ![image-20190430142326073](Math 126 Lecture 37.assets/image-20190430142326073.png)
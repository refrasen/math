# Math 126 Lecture 14

## Recall

#### Theorem 5. Least-Square Approximation

$\{ X_n \}$ **orthogonal se**t of functions, $||f|| < \infin$, **then** for all $N \in \N$:
$$
\sum_{n=1}^N A_n X_n, \qquad A_n := \frac{(f, X_n)}{(X_n, X_n)}
$$
Minimizes $|| f - \sum_{n=1}^N c_n X_n||$ among all choices of $c_1, .., c_N \in \C$ 

##### Proof: Linear Algebra

WLOG: $c_n, X_n \in \R​$ $E_n = ||f - \sum_{n=1}^N c_n X_n ||^2 ​$ $ = \int_a^b |f - \sum_{n \leq N} c_n X_n(x)|^2 \; dx​$ 

$ = \int_a^b f^2 - 2\sum_{n \leq N} c_n  f X_n + \sum_{n} \sum _{m}c_mc_nX_n (x) X_m(x)\; dx$

then due to orthogonality: 

$ = ||f||^2 - 2\sum c_n (f, X_n) + \sum c_n^2 ||X_n||^2$ 
then "completing the square" in terms of the last two sums:

$E_N= ||f^2|| + \sum_{n \leq N} ||X_n||^2(c_n - \frac{(f, X_n)}{||X_n||^2})^2$ $ - \sum_{n \leq N} \frac{(f, X_n)^2}{||X_n||^2}$ 

Now the coefficients $c_n$ appear in only one place.  ($||f^2||$ and $\sum_{n \leq N} \frac{(f, X_n)}{||X_n||}$ are independent of choice of coefficients and remain constant w.r.t. to choosing coefficients). And we have that the expression is the smallest when
$c_n = \frac{(f, X_n)}{||X_n||^2} = A_n$, proving Theorem 5.
Since $E_N \geq 0$, setting $c_n = A_n$, 
$$
(16) \qquad 0 \leq E_n = ||f||^2 - \sum_{n \leq N} \frac{(f, X_n)^2}{||X_n||^2} = ||f||^2 - \sum_{n\leq N}A_n^2||X_n||^2
$$


##### Corollary: Bessel's inequality

Set $c_n = A_n = \frac{(f, X_n)}{||X_n||^2}$, so we have the **Fourier Coefficients** now:

For all $N \in \N$: 


$$
(17) \sum_{1 \leq n \leq N} A_n^2 ||X_n||^2 \leq ||f||^2
\\ (18) \qquad \sum_{n=1}^\infin A_n^2 ||X_n||^2 \leq ||f||^2
$$
(so as $N \rightarrow \infin$, what happens? this is a now a question of convergence in the $L^2$ sense)

**valid** as long as $\int_a^b |f|^2 \; dx < \infin$ 

#### Theorem 6. 

The Fourier Series of $f(x)$ $\sum A_n X_n $ converges to $f$ in $L^2$ $\iff$ $(19) \qquad \sum_{n=1}^\infin A_n^2 ||X_n||^2 = ||f||^2​$ "Parseval equality"

(i.e., if and only if you have equality) in the book, we have $N = \infin$

Proof: Mean-square convergence means $E_N \rightarrow 0$ as $N \rightarrow \infin$, but from (16) this means $\sum_{n\leq N} |A_n|^2 ||X_n||^2 \rightarrow ||f||^2$  

**Definition**: The infinite orthogonal set of functions $\{ X_1(x), X_2(X), … \}$ is called **complete** if Parseval's equality is true for all $f$ with $\int_a^b |f|^2 \; dx < \infin$ 

$\sum_{n=1}^\infin A_n^2 ||X_n||^2 = ||f||^2$  is Parseval's equality, and this is true when $(f,f)$ is finite 

From theorem 3, our set of eigenfunctions from $-X'' = \lambda X$ with symmetric BC is complete: $\int_a^b |f|^2\; dx$ is finite $\implies$ convergence to $f$ in $L^2$ sense $\implies$ Parseval Equality is true

**Remember**: Parseval equality only occurs when we have fourier coefficients

##### Corollary 7. 

If $\int_a^b|f(x)|^2 \; dx$ is finite, then the Parseval equality $(19)$ is true

## Pointwise & Uniform Convergence

We want to prove Theorem 4$\infin$ from last time:
$$
f, \text{ a function of period }2l, \text{ and }f, f' \text{ are pointwise continuous} \implies 
\\ \text{classical full Fourier series converges to }\frac{1}{2}(f(x+)- f(x-)) \quad \forall x
$$

##### Proof of Theorem 4 $\infin$ for $f \in C^1$ 

WLOG (Scaling) $l = \pi$ 

1. Setup $A_n = \int_{-\pi}^\pi f(y) \cos (ny) \frac{dy}{\pi}, n = 0, 1, 2, …$ and $B_n = \int_{-\pi}^\pi f(y) \sin (ny) \frac{dy}{\pi}$, $n =1, 2, …$ 

$$
S_{N^*} = \frac{1}{2}A_0 + \sum_{n=1}^N (A_n \cos(n x) + B_n \sin (nx))
$$

We want to show: $\forall x:$ $S_{N^*} \rightarrow f(x)$ as $N \rightarrow \infin$ 
$$
(1) \qquad S_{N^*}(x) = \int_{-\pi}^\pi [1 + 2 \sum_{n=1}^N \underbrace{\cos(ny)\cos(nx) +\sin(ny)\sin(nx)}_{= \cos(ny-nx)}]f(y) \frac{dy}{2\pi}
\\ = \int_{-\pi}^\pi K_N (x-y) f(y)\frac{d y}{2\pi}
$$

$$
\text{Dirichlet Kernel: } K_N(\theta) = 1 + 2\sum_{n=1}^N \cos n\theta
$$

2. The Kernel

* $K_N$ is $2\pi$ periodic
* $\int_{-\pi}^\pi K_N(\theta) \frac{d\theta}{2\pi} = 1 + 0+ 0 + \cdots = 1$  $(2)$ 
* $K_N(\theta) = \frac{\sin ((N+ \frac{1}{2}\theta)}{\sin (\frac{\theta}{2})}​$ $(3)​$ (proof by complexification) $K_N(\theta) = 1 + \sum_{n=1}^N (e^{in\theta} + e^{-in\theta}) = \sum_{n = -N}^N e^{in\theta}​$, this is a finite geometric series with first term $e^{-iN\theta}​$, ratio $e^{i\theta}​$, lat term $e^{iN\theta}​$, so it equals $\frac{e^{-iN\theta} - e^{i(N+1)\theta}}{1 - e^{i\theta}}​$ , multiply top and bottom by $e^{-i\theta/2}​$, we get $\frac{e^{-i(N+1/2)\theta} - e^{i(N+1/2)\theta}}{-e^{i\theta/2} + e^{-i\theta/2}}​$ $= \frac{\sin [(N+1/2)\theta]}{\sin( \theta/2)}​$ 

$\implies$ letting $\theta = y-x$, knowing $K_N$ is even:

$S_{N^*}(x) - f(x) = \int_{-\pi}^\pi K_N(\theta)f(\theta + x)\; \frac{d\theta}{2\pi} - f(x)$ 

and $f(x)*1 = \int_{-\pi}^\pi K_N(\theta)f(x) \frac{d\theta}{2\pi}$, so
$$
(4)\qquad S_{N^*}(x) - f(x) = \int_{-\pi}^\pi K_N(\theta)[f(x+\theta)-f(x)]\frac{d\theta}{2\pi}
\\ = \int_{-\pi}^\pi \frac{f(x+\theta) - f(x)}{\sin \frac{\theta}{2}}\sin ((N+\frac{1}{2})\theta) \frac{d\theta}{2\pi}
\\ = \frac{1}{2\pi}(g, \phi_N)
$$
and let $g(\theta) = \frac{f(x+\theta) - f(x)}{\sin \frac{\theta}{2}}$, noting that $x$ is fixed, and let $\phi_N = \sin (N + \frac{1}{2})\theta)$ 

so $S_{N^*} - f(x) = \frac{1}{2\pi}(g(\theta), \phi_N(\theta))$

3. Orthogonality, Note: $g$ is continuous

* clear for $\theta \neq 0$
* at $\theta = 0$: $\lim_{\theta \rightarrow 0} g(\theta) = \lim_{\theta \rightarrow 0} \frac{f(x+\theta) - f(x)}{\theta}\cdot \frac{\theta}{\sin \frac{\theta}{2}} = 2f'(x)$, so $||g|| < \infin$ 

Observe: $\{\phi_N\}_N$ is an orthogonal set of functions (**Exercise 5.3.5**), so by Bessel's inequality:

$\sum_{n=1}^\infin \frac{|(g, \phi_N)|^2}{(\phi_N, \phi_N)} \leq ||g||^2$ , and by direct calculation: $||\phi_N||^2 = \pi$ 

Question: Is the set $\{\phi_N\}_N$ a set of eigenfunctions, and is that why we are allowed so say the following:

and since $||g|| < \infin$, the series converges  $\implies$ the terms $(g, \phi_N) \rightarrow 0$ as $N \rightarrow \infin$

Question (cont): or is it because the theorem we used above can be used for any series of orthogonal functions with fourier coefficients?

(otherwise we'd keep adding $|(g, \theta_n)|^2 > 0$ terms, which implies nonconvergence 

**So we have proved the <u>pointwise convergence</u> of the Fourier Series of any $C^1​$ function** 

### Proof for Discontinuous Functions

2. The Kernel, equation (4) becomes

   $S_{N^*} - \frac{1}{2}( f(x+) - f(x-)) = S_{N^*} - \int_0^\pi K_N(\theta)f(x+)\frac{d\theta}{2\pi} - \int_{-\pi}^0 K_N(\theta) f(x-) \frac{d\theta}{2\pi}​$ 

$ = \int_{-\pi}^\pi K_N(\theta) f(x + \theta) \frac{d\theta}{2\pi} - \int_0^\pi K_N(\theta)f(x+)\frac{d\theta}{2\pi} - \int_{-\pi}^0 K_N(\theta) f(x-) \frac{d\theta}{2\pi}$  

$ \int_0^\pi K_N(\theta) [f(x + \theta) - f(x +)) \frac{d\theta}{2\pi} + \int_{-\pi}^0 K_N(\theta) ( f(x+\theta) - f(x-)) \frac{d\theta}{2\pi} \\= \int_0^\pi g_+(\theta) \phi_N(\theta) \frac{d\theta}{2\pi} + \int_{-\pi}^0 g_-(\theta)\phi_N(\theta) \frac{d\theta}{2\pi}$ 

Question: Why did the integrals go from $\int_{-\pi}^\pi$ to $\int_0^\pi$ and $\int_{-\pi}^0$: Because $K_N(\theta)/2\pi$ is an even function and $\int_{-\pi}^ \pi K_N(\theta) \frac{d\theta}{2\pi} = 2\int_{-\pi}^0 K_N(\theta) \frac{d\theta}{2\pi}= 2\int_0^\pi K_N(\theta) \frac{d\theta}{2\pi} = 2$  

where $g_{\pm}(\theta) = \frac{f(x+\theta) - f(x \pm)}{\sin \frac{\theta}{2}}$ , the functions $\{\phi_N\}$ are <u>even</u> orthogonal on $(0, \pi)$ or $(-\pi, 0)$, Hence we may employ Bessel's inequality separately for each (???) 

So we deduce (exercise 8) that both of the integrals above tend to zero as $N \rightarrow \infin$, **provided** that $\int_0^\pi |g_+(\theta)|^2 d\theta$ and $\int_{-\pi}^0 |g_{-}(\theta)|^2 d\theta$ are finite: Need to show one-sided limits, only prove boundedness of $g^2$ 

##### Where would divergence come from?

vanshing of $\sin \frac{1}{2} \theta$ at $\theta = 0$. 
$$
\lim_{\theta \downarrow 0}g_{+}(\theta) = \lim_{\theta \downarrow 0} \frac{f(x+\theta) - f(x+)}{\theta}\cdot \frac{\theta}{\sin (\frac{1}2\theta)} = 2f'(x+)
$$
if $x$ is a point where the one-sided derivatve $f'(x+)$ exists. If $f'(x+)$ doesn't exist ($f$ might have a jump at the point $x$), then $f$ is still differentiable at nearby points

By mean value theorem:

Let us consider $[x+\epsilon, x+\theta]$, where $\epsilon$ is very small, so that $f$ is continuous and differentiable (possible, since there are only finite number of discontinuous points for both $f$ and $f'$):

 $[f(x+\theta) - f(x+)]/\theta = f'(\theta^*)$ for some point $\theta^*$ between $x$ and $x + \theta$ . Since the derivative is bounded $\implies$ $[f(x+\theta) - f(x)]/\theta$ is bounded as well for $\theta$ small and postive $\implies$ $g_+(\theta)$ is bounded and $\int_0^\pi |g_+(\theta)|^2 d\theta$ is finite

the same way works for $g_-(\theta)$ (would we be doing $x +\theta$ to $x$, where $\theta < 0$? 

### Proof of Uniform Convergence 

**Theorem 5.4.2**: If $(i) f \in C^1 ([-\pi, \pi]) \\ (ii) f \text{ periodic}$ then $S_{N^*} = \frac{1}{2} A_0 + \sum_{n=1}^N A_n \cos (n x) + B_n \sin (nx))$ converges uniformly to $f$ in $[-\pi, \pi]$ 

**Proof**: Define $A_n, B_n$: Fourier coefficients of $f$ 
$A_n', B_n'$: Fourier coefficients of $f'$ 

Integrate by parts:
$$
A_n = \int_{-\pi}^\pi f(x)\cos nx \frac{dx}{\pi} = \underbrace{\frac{1}{n \pi}f(x)\sin nx|_{-\pi}^\pi}_{=0} - \int_{-\pi}^\pi f'(x) \sin n x\frac{dx}{n\pi}
\\
B_n = \int_{-\pi}^\pi f(x)\sin nx \frac{dx}{\pi} = \underbrace{-\frac{1}{n\pi}f(x)\cos nx|_{-\pi}^\pi}_{= 0} + \int_{-\pi}^\pi f'(x)\cos nx \frac{dx}{n\pi}
$$
So:
$$
A_n = -\frac{1}{n}B_n' \text{ for } n \neq0\qquad \text{and}\qquad B_n = \frac{1}{n}A_n'
$$
By Bessel's inequality for $f'$: 
$$
\sum_{n=1}^\infin (|A_n'|^2 + |B_n'|^2) < \infin
$$
Therefore: $\sum_{n=1}^\infin (|A_n \cos nx| + |B_n \sin nx|) \leq \sum_{n=1}^\infin (|A_n| + |B_n|) = \sum_{n=1}^\infin \frac{1}{n}(|B_n'| +|A_n'|) $

$\leq (\sum_{n=1}^\infin (\frac{1}{n})^2)^{1/2}( \sum_{n=1}^\infin (\underbrace{|B_n'| + |A_n'|}_{ \leq 2 (|A_n[|^2 +|B_n'|^2)})^2 )^{1/2} < \infin$ 

so the Fourier series is finite $\implies$ converges

$|a|^2 - 2|a||b| + |b|^2 = (|a|-|b|)^2 \geq 0$ so$|a|^2 + |b|^2 \geq 2|a||b|$ 

$\implies$ $|a|^2 + 2|a||b| + |b|^2 \leq 2|a|^2 + 2|b|^2$ 

We know from Theorem 5.4.4$\infin$ that the sum of the Fourier series is $f(x)$:

**Conclusion** 
$\max_{x} |f(x) - S_{N^*}(x)| = \max_x |\sum_{n = N+1}^\infin A_n \cos nx + B_n \sin nx| \\ \leq \sum_{n=N+1}^\infin (|A_n| + |B_n|) \rightarrow 0 \text{ as } N \rightarrow \infin$
because the [tail of a **convergent** series tends towards $0$][https://proofwiki.org/wiki/Tail_of_Convergent_Series_tends_to_Zero] 

and thus this fulfills the definition of uniform convergence  

### The Gibbs Phenomenon

At jump discontinuities, $S_N$ "overshoots". $S_N(x)$ always differs from $f(x)$ near the jump by an "overshoot" of about 9 percent. The width of the overshoot goes to zero as $N \rightarrow \infin$ while the extra height remains at $9$ percent (top and bottom)
$$
\lim_{N\rightarrow \infin} \max|S_N(x)-f(x)|\neq 0
$$
**although**, $S_N(x) - f(x)$ does tend to zero for each $x$ where $f(x)$ doesn't jump

##### Simple example

$$
f(x) = \begin{cases} \frac{1}2 & 0 <x<\pi \\ -\frac{1}2& -\pi <x<0 \end{cases}
$$

Fourier series: $\sum_{n \text{ odd}= 1}^\infin \frac{2}{n\pi} \sin n x$  

$f(x) = \frac{1}{2}A_0 + \sum_{n=1}^\infin A_n \cos nx + B_n \sin nx​$ 

$\frac{1}{\pi} \int_{-\pi}^\pi f(x)\cos nx \; dx​$ $= \frac{1}{2\pi} [\int_0^\pi \cos nx \; dx -\int_{-\pi}^0 \cos nx \; dx] ​$ $ = 0​$, since $\int_0^\pi (even) dx = \int_{-\pi}^0 (even) dx​$ 

$\frac{1}{2\pi} [\int_0^\pi \sin nx \; dx - \int_{-\pi}^0 \sin nx \; dx]$ $ =  [-\frac{\cos nx}{2n\pi} ]_0^\pi + [\frac{\cos nx}{2n\pi}]_{-\pi}^0 = \frac{1}{2n\pi}[(-1)^{n+1} +1 + 1+ (-1)^{n+1}] = \frac{1}{n\pi}[(-1)^{n+1} + 1]$ 

![image-20190227042152915](Math 126 Lecture 14.assets/image-20190227042152915.png)

**By our previous computations**: 
$$
S_N(x) = (\int_0^\pi - \int_{-\pi}^0)\frac{1}{2}K_n(x-y)\frac{dy}{2\pi}
\\ = (\int_0^\pi - \int_{-\pi}^0) \frac{\sin[(N+\frac{1}{2})(x-y)]}{\sin [\frac{1}{2}(x-y)]}\frac{dy}{4\pi}
$$
Set $M:= N + \frac{1}{2}$ and change variables
$$
\theta = M(x-y) \text{ in the first integral}
\\ \theta = M(y-x) \text{ in the second integral}
$$

$$
S_N(x) = (\int_{M(x-\pi)}^{Mx} - \int_{-M(x + \pi)}^{-Mx})\frac{\sin \theta}{2M \sin \frac{\theta}{2M}}\frac{d\theta}{2\pi}
$$

$\int_{M(x-\pi)}^{Mx} - \int_{-M(x+\pi)}^{-Mx}$ $ = \int_{Mx - M\pi}^{Mx} - \int_{-Mx - M\pi}^{-Mx}$ $ \int_{Mx - M\pi}^c + \int_c^{Mx} + \int_{-Mx}^c + \int_c^{-Mx - M\pi} = \int_{-Mx}^{Mx}  - \int_{-Mx - M\pi}^{Mx - M\pi}$ 

so we get the following, and then proceed to change $\theta$ to $-\theta$: 
$$
S_N(x) = (\int_{-Mx}^{Mx} - \int_{-M\pi - Mx}^{-M\pi + Mx}) \frac{\sin \theta}{2M \sin \frac{\theta}{2M}} \frac{d\theta}{2\pi}
\\ = (-\int_{Mx}^{-Mx}+ \int_{M\pi+Mx}^{M\pi  - Mx})\frac{\sin \theta}{2M \sin \frac{\theta}{2M}}\frac{\theta}{2\pi}
\\ = (\int_{-Mx}^{Mx}- \int_{M\pi - Mx}^{M\pi + Mx})\frac{\sin \theta}{2M \sin \frac{\theta}{2M}}\frac{d\theta}{2\pi}
$$
What happens close to $x = 0$? i.e., where the jump is. Recall $M >> 1$: 

Choose $x = \frac{\pi }{M}$ : this is because this is where the first integral is maximized:

using $FTC$: $\sin Mx = 0$ when $x = \pi/M$ 
$$
S_N(\frac{\pi}{M}) = (\int_{-\pi}^{\pi} - \int_{M\pi - \pi}^{M\pi + \pi}) \frac{\sin \theta}{2M \sin \frac{\theta}{2M}}\frac{d\theta}{2\pi}
$$
**Second integral**: (proving it disappears as $M \rightarrow \infin$ 

$\theta/2M$: $(M\pi \pm \pi)/2M$ $= \frac{\pi}{2}[1 \pm \frac{1}{M}]$  

$\frac{\pi}{4} < [1 - \frac{1}{M}]\frac{\pi}{2} \leq \theta/2M \leq [1 + \frac{1}{M}] \frac{\pi}{2} < \frac{3 \pi}{4}$ for $M > 2$ 

$\implies$ $\sin (\theta/2M) > 1/\sqrt{2}$ $\implies$ $\int_{M\pi - pi}^{M\pi + \pi} \frac{\sin \theta}{2M \sin \frac{\theta}{2M}} \frac{d\theta}{2\pi}$  $< \int_{M\pi - \pi}^{M\pi + \pi} \frac{1}{2M/\sqrt{2}} \frac{d\theta}{2\pi} = \frac{\sqrt{2}}{4\pi M}[M\pi + \pi - M\pi + \pi] $ $ = \frac{1}{2\sqrt{2} \pi M} [2\pi]$ $ = \frac{1}{\sqrt 2 M}$ 
which tends to $0$ as $M \rightarrow \infin$

**First Integral**: 

$|\theta| \leq \pi$ and $2M \sin \frac{\theta}{2M} \rightarrow \theta $ uniformly in $-\pi \leq \theta \leq \pi$ as $M \rightarrow \infin$ (Why?)

​	is this due to the derivative: $\frac{2M}{2M} \cos \frac{\theta}{2M}$ $\rightarrow 1$ as $M \rightarrow \infin$ for any $\theta \in \R$ ?

​	see how this works by graphing $c \sin \frac{x}{c}$ for large $c$ i.e., we get larger periodicity, and the area around $x = 0$ where $\sin x/c$ looks like a line with slope 1 grows larger. 

Hence:

$S_N(\frac{\pi}{M}) \rightarrow \int_{-\pi}^{\pi} \frac{\sin \theta}{\theta} \frac{d\theta}{2\pi} = \frac{1}{2\pi} = 0.59...$  as $M \rightarrow \infin$ 

this is Gibb's 9 percent overshoot of the unit jump value

![image-20190227051231231](Math 126 Lecture 14.assets/image-20190227051231231.png)
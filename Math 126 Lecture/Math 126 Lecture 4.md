# Math 126 Lecture 4

## Recall

$$
u_t + cu_x = 0 \; (\text{convection/transport)}
\\ u_{tt} = c^2 u_{xx}\; (\text{wave equation \ if stationary, then }\triangle u = 0)
\\ u_{tt} = c^2 \triangle u \; (\text{see above})
\\ u_t = k \triangle u  \; (\text{diffusion equation \ if stationary, then } \triangle u = 0)
\\ c \rho u_t = k \triangle u \; (\text{see above})
$$

### Note: 

**In 1D**: $\triangle u = 0$ means $u_{xx} = 0$, i.e., $\exists a, b \in \R$ such that $u = ax + b$ 

**In 2D**: If $f: \C \rightarrow \C$ holomorphic. Then, $u = \Re f $ satisfies $\triangle u = u_{xx} + u_{yy}$ 

---

## Initial Conditions and Boundary Conditions

### **Initial Conditions**

specify the data at a particular time $t_0$ 
$$
u(\vec x, t_0) = \phi(\vec x)
$$
**Examples**:

* <u>diffusion</u>: $\phi(\vec x) = $ initial concentration @ $\vec x$
* <u> heat</u>: $\phi(\vec x) =$ temperature @ $\vec x$ 

* <u>wave</u>: $u(\vec x, t_0) = \phi(\vec x), u_t(x, t_0) = \psi(\vec x)$ 
  * $\phi(\vec x)$: initial position
  * $\psi(\vec x)$: initial velocity

​				

### Boundary Conditions

$D$ domain e.g. $D = \{ x: 0 < x < l  \}$ for length $l$ of a vibrating string

There are several kinds of boundary conditions
$$
(D) \; \text{Dirichlet: } u = f \text{ on } \partial D\text{ (u is specified)}
\\(N) \; \text{Neuman: } \frac{\partial u}{\partial n} = \vec n\cdot \nabla u = f \text{ on } \partial D 
\\ (R) \; \text{Robin: } u + a \frac{\partial u}{\partial n} = f \text{ on }\partial D, a \text{ a function of }x, y, z, t
$$
holds for all $t$, and for $\vec x \in \partial D$ 

Any of these boundary conditions is called **homogeneous** if the specified function $f(\vec x, t) \equiv 0$ 

**Example** Vibrating String, describes **<u>one-dimensional problems</u>** where $D$ is just an interval $0 < x < l$, the boundary is the two endpoints:
$$
(D) \; u(0, t) = g(t), \; u(l, t) = h(t) \text{ conditions at each end of string}
\\ \text{ }
\\(N) \; u_x(0, t) = g(t),\; u_x(l, t) = h(t)
\\ \text{ }
\\(R) \text{ my guess } u(0, t) + au_x(0, t) = g(t),\; u(l, t) + au_x(l, t) = h(t)
$$

#### How Do Bounary Conditions arise in physical experiments?

##### The Vibrating String

* If the string is held *fixed* at both ends (violin string), we have the homogeneous Dirichlet conditions
  $$
  (D)\; u(0, t) = u(l, t) = 0
  $$

* String on one end is *free* to move transversally without any resistance (vertically, along a frictionless track). No tension $T$ (as in vector, not tension $T$ magnitude, we do have I believe a "cosntant" magnitude $T$ $\implies$ $u_x = 0$) at that end, so $u_x = 0$, this is a Neumann condition.
  $$
  (N) \; u_x = 0
  $$
  Explanation from [here][https://physics.stackexchange.com/questions/219282/vibrating-string-free-end-boundary-condition], video demonstration [here][https://www.youtube.com/watch?v=1GyiHMj67JE], the video kind of shows that in a small interval at the free end, the string basically looks flat, which means $u_x = 0$ or $\frac{dy}{dx}$ at that part of the string is $0$ 
  $$
  T = \text{tension magnitude}
  \\ Tu_x = \int_{x_0}^{x_1}\rho u_{tt}dx
  \\ \text{let }x = 0 \text{ be the end moving freely}
  \\ Tu_x = \int_{0}^{x_1}\rho u_{tt}dx
  \\ x_1 \rightarrow 0
  \\ Tu_x(0) = 0
  $$
  Net force along $y$-axis at $x = 0​$ is "null"

* An end of the string free to move, but attached to it is a coiled spring or a rubber band, which tended to pull it back to equilibrium position. **Robin condition** 
* If end of string moved in a specified way, **Dirichlet condition, inhomogeneous** 

##### Diffusion

container $D$ with boundary $\partial D$

* none of the substance can exit or enter through $\partial D$, then the concentration gradient must vanish (no flux) by Fick's law $\implies$ 

$$
(N) \;\frac{\partial u}{\partial n}= \vec n \cdot \nabla u = 0 \text{ on S }= \partial D
$$

* If the container is permeable and if any substance that escapes gets washed away immediately, we have (on the boundary, we have no substance as things get washed away as they exit $D$...)
  $$
  (D) \; u = 0 \text{ on S = } \partial D
  $$

##### Heat

$u_{tt} = \kappa \triangle u$ in $D$ 

* If object $D$ through which heat is flowing is perfectly *insulated*, then no heat flows across the boundary

$$
(N) \; \partial u/\partial n = 0
$$

* If $D$ is immersed into a large reservoir (think of you in the bay) of temperature $g(t)$ and we have perfect, thermal conduction across $S = \partial D$, then 
  $$
  (D)\; u= g(t) \text{ on }S = \partial D
  $$
  ![Sketch002](/Users/r/Documents/Sketch002.jpg)

$$
\frac{\partial u}{\partial n} = 0 \text{ on } \partial D \times (0, \infin)
\\ u = \phi \text{ on } D \times \{t =0\}
$$

In 1.4: missed a few examples, and jump conditions, conditions at infinity

## Well-Posed Problems

Informal definition: A problem of solving a PDE w/ certain initial/boundary conditions is well-posed if

1. Existence: THere exists at least one solution $u(x,t) $ satisfying all these conditions
2. Uniqueness: There is at most one solution
3. Stabilitiy: The unique solution $u(x,t)​$ depends in a stable manner on the data of the problem. This means that if the data are changed a little, the corresponding solution changes only a little. 

**Example** From last time
$$
au_x + bu_y = 0
\\ u(0,y) = f(y)
$$
has the unique solution $u(x,y) = f(e^{-x}y)$ 

(1) and (2) are satisfied

(3) Given two function $f, \tilde f$ then the solution, $u, \tilde u$ of the above equation with $f$ and $\tilde f$ respectively
$$
\sup_{x,y} |u(x,y) - \tilde u(x,y)| = \sup_{x,y} |f(e^{-x}y) - \tilde f(e^{-x}y)| \leq \sup_x|f(x)-\tilde f(x)|
$$
A famous example (there is a huge difference between $u_{xx} + u_{yy} = 0$ and $u_{xx}-u{yy} = 0)$ 

d = 2

$D = \{ (x,y): y > 0 \}$ 
$$
u = \phi
\\\frac{\partial u}{\partial n} = -u_y = \psi
$$
**NOT WELL POSED**  (to specify both $u$ and $u_y$ on the boundary of $D$ )

For $K \in \N$, we have solutions 
$$
u_k(x,y) = \frac{1}{k}e^{\sqrt{k}} \sin kx \sinh ky
$$
Notice: they have boundary data 
$$
u_k(x, 0) = 0
\\ \frac{\partial u_k}{\partial y}(x, 0) = e^{-\sqrt{k}}\sin kx  \rightarrow 0 \text{ as }k \rightarrow \infin
$$
BUT for *any* $y \neq 0$: $u_k(x, y) $ $\not\rightarrow 0$ as $k \rightarrow \infin$ 

on the $x-$axis, $u_k(x,0)$ is $0$ 

$\frac{\partial u_k}{\partial y}(x,0)$ converges to $0$ as $k \rightarrow \infin$ 

so as $k \rightarrow 0$, we should have a function that is equal to $0$ on the $x$ axis and as $y$ changes, shouldn't change, so this points to a function that $= 0$, at least in some open set (?) containing to the $x$-axis, but we don't actually obtain that when we take the limit of $u_k(x,y) \rightarrow \infin​$ 

His explanation as for why it violates (3):

$u_k \not\rightarrow u$... what is $u$ supposed to be? 0? is this due to the combination of boundary data?

**continuous dependence**: no continuous dependence of the solution on initial data

we can have $y = \epsilon$ (small) and $ \max_x |u_k(x, \epsilon)|$ $\rightarrow \infin$ 
# Math 126 Lecture 3

January 28, 2019

---

## 1st Order Constant Coefficients

### Geometric

Given $a, b \in \R$, $(a,b) \neq (0,0)$, find $u$ such that 
$$
(a,b)\cdot \nabla u = au_x + bu_y = 0
$$
For any vector going in the direction $\vec v = (a,b)​$, we have that $u​$ is constant

and any point $(x,y)$ will lie in some line going in the direction of $\vec v$

we can parameterize by $\vec w = (b, -a)$, the vector perpendicular to $\vec v$, and along the line $c(b, -a)$, for any $c \in \R$, we will intersect with any line going in the direction $(a,b)$ exactly once. and since any point in $\R^2$ lies on some line going in the direction $(a,b)$, we can say: 

$\exists$ a function, $f: \R \rightarrow \R$ such that $u(x,y) = f(\vec w \cdot (x,y)) = f(bx - ay)$ 

Noting: any line going in the direction $a, b$ can be expressed as $(y - y_1) = \frac{b}{a}(x-x_1)$

$ay - ay_1 = bx - bx_1 $ $\equiv$ $bx_1 - ay_1 = bx - ay$ and we let $\xi = bx_1 - ay_1$ 

each line is uniquely determined by $\xi$, and therefore, $u$ is a function of $\xi$ 

### Change of Variables

1. define new variables $x', y'$

$$
x' = ax + by = \vec v \cdot \vec x \\ y' = bx - ay = \vec w \cdot \vec x
$$

2. Take note of the partial derivatives of $x', y'$ w.r.t. to $x, y$ 

$$
\frac{\partial x'}{\partial x} = a \; \; \; \; \frac{\partial x'}{\partial y} = b \\ \frac{\partial y'}{\partial x} = b \; \; \; \; \frac{\partial y'}{\partial y} = -a
$$

3. Change thinking of $u$ from in terms of $(x,y)$ to in terms of $(x', y')$ and find $\frac{\partial u}{\partial x}(x', y')$ and $\frac{\partial u}{\partial y}(x', y')$ using chain rule

$$
u_x = \frac{\partial u}{\partial x} = \frac{\partial u}{\partial x'}\frac{\partial x'}{\partial x} + \frac{\partial u}{\partial y'}\frac{\partial y'}{\partial y} = au_{x'} + bu_{y'}
\\
u_y = \frac{\partial u}{\partial x'}\frac{\partial x'}{\partial y} + \frac{\partial u}{\partial y'}\frac{\partial y'}{\partial y} = bu_{x'} - a u_{y'}
$$

4. rewrite the original PDE in terms of $u_{x'}$ and $u_{y'}$ using subsitution

$$
au_x + bu_y = a(au_{x'}+ bu_{y'}) + b(bu_{x'}-au_{y'}) = (a^2 + b^2)u_{x'} = 0
$$

Since $a^2 + b^2$ $\neq 0$, we have $u_{x'} = 0$, so $u$ is constant with respect to changes in $x'$ and so solving the simple PDE $u_{x'} = 0$: $u = f(y')$ $ = f(bx -ay)$

## Variable Coefficients

Example:
$$
(1) \; \; \; (1,y)\cdot \nabla u = u_x + yu_y = 0
$$
Find curves $y = y(x)$ with tangent $(1,y)$: 
$$
(2) \; \; \; \frac{\partial y}{\partial x} = \frac{y}{1} = y
$$
(2) has solution $y = Ce^x$, which we call the **characteristic curves** of (1)

These curves: 

1. Don't intersect
2. Fill out the whole xy-plane

Along each characteristic curve, $(x, Ce^x)$, $u$ is constant: 
$$
\frac{d}{dx} (u(x, Ce^x))= \frac{d}{dx}(u(x, y)) = \frac{\partial u}{\partial x}\frac{\partial x}{\partial x} + \frac{\partial u}{\partial y}\frac{\partial y}{\partial x} = u_x + u_y(Ce^x) = u_x + yu_y = 0
$$

1. because $y = Ce^x$ 
2. Chain rule
3. $\frac{\partial y}{\partial x} = Ce^x$ because $y = Ce^x$ 
4. replaced $Ce^x$ with $y$ because $y = Ce^x$ 
5. because (1)

Due to being constant along each characteristic curve, we can associate with any point $(x,y)$ a unique characteristic curve (since characteristic curves fill out (x,y) and don't intersect)

Hence: 
$$
u(x,y) = u(x, Ce^x) = u(0, Ce^{0}) = u(0, e^{-x}y)
$$

1. Because $u(x,y)$ is constant along $(x, Ce^x)$ so we just associate with a curve that $(x,y)$ is on
2. Because $u(x,y)$ is constant along $(x, Ce^x)$ so we can choose any point, $(0, Ce^0)$  in this case
3. because $y = Ce^x$ $\equiv$ $C = ye^{-x}$ 

Hence: $u(x,y) = f(e^{-x}y)$ is the general solution to $(1)$ 

#### Exercise

Find the solution of (1) with $u(0,y) = y^5$ 

Use: $u(0,y) = u(0, e^{-x}y) = y^5$ $ = (e^{-x}y)^5$ 

1. from the math block from earlier
2. from what's given in the exercise
3. since with $(0,y)$, we have the $y$ component equal to $e^{-x}y$ 

## Examples of PDEs

### Ex. 1 Convection (Simple Transport)

fluid/water flowing at a constant rate $c$ along a horizontal pipe of fixed cross section in the positive $x$ direction. A substance is suspended in the water:

$u(x,t)​$: concentration of particles:

Then: $u_t + cu_x = 0$ 

we have $(a, b) = (c, 1)$ so $f(x -c t)$ 

this means the substance is transported to the right at a fixed speed $c$ 

because note: let's say we follow an individual particle at $x_0$ at time $t = 0$ 

then after $t_n$ units of time, $x_0$ will be at $x_0 + ct_n$ $\implies$ $f(x_0 + ct_n - ct_n) = f(x_0)$  

### Ex. 2 Vibrating String

$u(x,y)$ is the displacement of the string from the equilibrium position, $(x,y)$ 

Because we know the ends are fixed, the $x$ coordinate of $u(x,y)$ stays constant

The slope of the string $= \frac{b}{a} = u_x$ 

so we have the vector tangent to the string: $(a,b) = (1, u_x)$ 

$\vec T$ = tension || tangent = $T(x) \frac{1}{\sqrt{1 + u_x^2}}(1, u_x)$ 

---

So TIP: if we have $y = mx + B$, and we want the vector for the tangent (slope), we can simply use $(1, m)$ 

to say the tangent vector is $(a,b)$ is to say the slope is $\frac{b}{a}$ 

---

Newton's Law: $\vec F = m\vec a$ decompose into $x$ and $u$ components
$$
(1) \qquad \frac{T}{\sqrt{1 + u_x^2}}|_{x_0}^{x_1} = 0
\\
(2) \qquad \frac{Tu_x}{\sqrt{1 + u_x^21}}|_{x_0}^{x_1} = \int_{x_0}^{x_1}\rho u_{tt} dx
$$
right side: mass $\times$ acceleration integrated over the piece of string

$\rho​$ Is mass density, which is constant in this case... but may well vary with $x​$ 

$u_{tt}​$ is the acceleration (?) (in terms of how the string is displaced)

Further simplifications: We assume motion, $|u_x|$ is quite small $|u_x| << 1$, $|u_{xx}| < 1$ $\cdots​$ 

$\sqrt{1 + u_x^2} = 1 + \frac{1}{2}u_x^2 + ... \approx 1$ (using Taylor expansion w.r.t. to $u_x$)

then $(1), (2)$ simplifies to:
$$
(1') \; \; \; \;T = \text{constant}
\\
(2') \;\;\;\; Tu_x|_{x_0}^{x_1} = \int_{x_0}^{x_1}\rho u_{tt}dx
$$
taking (2) and deriving w.r.t. to $x​$ 
$$
(Tu_x)_x = \rho u_{tt} \implies
\\ u_{tt} = c^2 u_{xx}, \; \; c = \sqrt{T/\rho}
$$

which is the *wave equation* 

**from book**: can see other variations and applications

### Ex. 3 Vibrating drumhead (same in d =2) 

SKIP **from book**: 

two dimensional version of a string: elastic, flexible, homogeneous drumhead lyingin $xy$ plane

$u(x,y,t)$ Is the vertical displacement (so on $z$-axis), and there is no horizontal motion

so the tension vector is in the direction of $\frac{\partial u}{\partial n} = \bold n \cdot \nabla u$, the directional derivative in the outward normal direction
$$
F = \int_{\partial D} T\frac{\partial u}{\partial n}ds = \int \int_D\rho u_{tt}dx dy = ma
$$
so the left side: sum of forces. right side: sum of $ma$ components in the domain $D$ 

Green's theorem: (try to use this one) $\int\int_D \nabla \cdot \bold f \; dxdy = \int_C \bold f \cdot \bold n \; ds$ 
$$
\int \int _D \nabla \cdot(T\nabla u) \; dx \, dy = \int \int_D \rho u_{tt}\; dx\, dy
$$
$D$ is arbitrary: we can use second vanishing theorem on $\int \int_D \nabla \cdot (T \nabla u) - \rho u_{tt} \; dx\, dy = 0$

to get $\nabla \cdot (T \nabla u) = \rho u_{tt}$ $\implies$
$$
u_{tt} = c^2 \nabla \cdot(\nabla u) \equiv c^2 (u_{xx} + u_{yy})
$$
  Three dimensions:]
$$
u_{tt} = c^2(u_{xx}+u_{yy}+u_{zz})
$$


### Ex. 4 Diffusion

concentration $u(x,t)​$, dealing with dye in a tube with flowing water? see youtube for visuals

##### Fick's Law:

Flux = amount of dye that flows through a unit area in 1 unit of time ~proportional~ concentration gradient

$M(t) = \int_{x_0}^{x_1} u(x,t)dx$ 

conservation of mass:
$$
\int_{x_0}^{x_1}u_t dx = \frac{dM}{dt} = \text{inflow - outflow} = \kappa u_x(x_1,t) - \kappa u_x(x_0, t)
$$
$\kappa$: diffusion coefficent 

1. I think since $M(t) = \int_{x_0}^{x_1}u dx​$ and we can switch order of integration and differentiation in this case
2. inflow of mass - outflow of mass (I think these are rates?) (at the ends)
3. = $\kappa$(change of concentration w.r.t. to position)

$\frac{d}{dx_1} \rightarrow$ (differentiating w.r.t. to $x_1$), (similar to FTC)
$$
u_t = (\kappa u_x)_x = \kappa u_{xx}
$$
Similalrly, in any dimension d:
$$
\int_D u_t d\vec x = \int_{\partial D}\kappa (\vec n \cdot \nabla u)dS = \int_D \nabla \cdot (\kappa \nabla u) d\vec x 
$$

1. Fick's law​: rate of motion is proportional to concentration gradient $\nabla u$, proportion coefficient(?) $\kappa$, IDK where $\vec n$ comes from. It is the normal vector to $\partial D$ 
   1. gradient is in the direction of the normal to the surface because gradients are perpendicular to tangent vector of the surface/boundary
   2. this depends on $u(\partial D) = c$, constant $c$ 
2. Divergence Theorem

Constancy principle: 
$$
u_t = \kappa \nabla \cdot \nabla u = \kappa \triangle u
$$

### Ex. 5 Heat Flow

$u(\vec x, t) = $ temperature field

$H(t) = $ heat contained in region $D$ = $\int_D c \rho u \; d\vec x$

$c$ is the specific heat, $\rho$ is the mass density

##### Fourier's law: 

heat flux from hot to cold ~proportional~ temperature gradient
$$
\int_D c\rho  u_t \; d\vec x = \frac{dH}{dt} = \int_{\partial D}\kappa (\vec n \cdot \nabla u) \; dS = \int_D \nabla \cdot (\kappa \nabla u)\; d\vec x
$$

1. from $H(t) = \int_{D}c\rho u \; d\vec x$ 
2. from Fourier's law
3. divergence theorem

##### Heat Equation:

$$
c\rho u_t = \nabla \cdot (\kappa \nabla u)
$$

If we have $c = 0$? $u_{tt} = 0$: 

$\nabla \cdot (\kappa \nabla u) = 0$ $ = \kappa \triangle u$ $\equiv$ Laplace's Equation


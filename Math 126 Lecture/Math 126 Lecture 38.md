# Math 126 Lecture 38

# Approximation and Convergence of Gradient Flows

### Goal

Given $X$, $(\cdot, \cdot)$, $E: X \rightarrow \R$, $ \phi \in X$, construct solution to
$$
\begin{cases}
u_t = - \nabla E(u) & t > 0
\\ u = \phi & t = 0
\end{cases}
$$
I **think** the notation means:

$\nabla E(\bold x) = (E_{x_1}(\bold x), \ldots, E_{x_d} (\bold x))$ where $d$ is the dimension of $X$ 

and $E_{x_i}$ $ = \frac{\partial E}{\partial x_i}$ 

$u_t(\bold x, t) = - \nabla E(u) = -(E_{x_1}(u), \ldots, E_{x_d}(u))$ 

Question:

1. Does $u$ depend **only** on $t$? i.e.: $u: \R \rightarrow X$ 
2. Or: is $u: X \times \R \rightarrow X$? 

### Idea: De Giorgi "Minimizing Movements"

Let $h > 0$ - time step size, set $u^0 := \phi$ 

so each $u^n$ means at time step $n$, maybe corr. to some time $t_n$ 

For $n = 1, 2, \ldots$, solve the minimization problem
$$
(MM) \qquad \min_{u \in X} \{\frac{1}{2h}\| u - u^{n-1}\| + E[u] \} \sim \rightarrow u^n
$$

---

Renee Note:

So we define $u_n$ recursively to be the minimizer for $(MM)$

and from the paper I read: $u(t) = u^{\lfloor t/h\rfloor}$

---

This mimics the gradient flow $u_t = - \nabla E(u)$ in the sense that one wants to **decrease** $E[u]$, but does not want to move too far due to the **penalization** $\frac{1}{2h}\| u - u^{n-1}\| ^2$ 



**Question**:  What is the Euler-Lagrange Equation? 

For $v \in X$, 
$$
\begin{align}
0 &= \frac{d}{ds}_{|s = 0} (\frac{1}{2h}\| (u^n + sv)- u^{n-1})\|^2 + E[u^n+sv])
\\ &= \frac{d}{ds}_{|s= 0}(\frac{1}{2h}[(u^n + sv, u^n + sv) - 2(u^n + sv, u^{n-1}) + (u^{n-1}, u^{n-1})] + dE(u^n)[v]  
\\ & = \frac{d}{ds}_{|s = 0}(\frac{1}{2h}[\|u^n\|^2 + \|u^{n-1}\|^2 + s^2\|v\|^2 - 2(u^n, u^{n-1}) + 2s(u^n - u^{n-1}, v)]) + dE(u^n)[v]
\\ &= \frac{1}{h}(u^n - u^{n-1}, v) + dE(u^n)[v] 
\\ &= \frac{1}{h}(u^n - u^{n-1})
\\ &= (\frac{u^n - u^{n-1}}{h}+\nabla E(u^n), v)
\end{align}
$$
Note: Use the linearity property of inner products to get from the second expression to the third expression, we'll find that the only term that's left after differentiating and setting $s = 0$ are the terms of the form:

$(v, u^{n-1}) - (v, u^{n-1}) + (u^n, v) - (u^{n-1}, v)$ and I think since we are working in $\R$? we can do $(a, b) = (b, a)$ (normally: $(a, b) = \overline{(b, a)}$)

and this implies
$$
(EL) \qquad \frac{u^n - u^{n-1}}{h} = - \nabla E(u^n)
$$
This is the **implicit** Euler scheme for $u_t = -\nabla E(u)$ 

**Weak Formulation**: The easiest way to show that the limit of these functions as $h \downarrow 0$ solves $u_t = -\nabla E(u)$ is the "weak formulation"
$$
E(u(T)) + \int_0^T (\frac{1}{2}\| \nabla E(u)\|^2 + \frac{1}{2}\| u_t \|^2)\, dt \leq E(\phi)
$$

#### 1. In General: Problem with exact prefactor!

By minimality (MM)
$$
\frac{1}{2}\| u^n - u^{n-1}\|^2 + E[u^n] \leq 0 +  E[u^{n-1}]
$$
$\implies$
$$
\frac{E[u^n] - E[u^{n-1}]}{h} \leq - \frac{1}{2}\| u^n - u^{n-1}\|^2
$$
This inequality is **not sharp**! 

#### 2. E Convex

That means

![image-20190504123157720](Math 126 Lecture 38.assets/image-20190504123157720.png)
$$
\begin{align}
E[v] &\geq E[u] + dE[u](v-u)
\\  &= E[u] + (\nabla E(u), v-u)
\\ &\text{or}
\\ &E[u] - E[v] \leq (\nabla E(u), u-v)
\end{align}
$$
where I believe $dE[u](v-u) = \frac{d}{ds}|_{x = 0}E(u + s(v-u))$? 

Then with: $u = u^n, v = u^{n-1}$ and divide by $h > 0$ 
$$
\begin{align}
\frac{E[u^n] - E[u^{n-1}]}{h} \leq (\underset{= - \frac{u^n - u^{n-1}}{h}}{\nabla E(u^n)}, \frac{u^n - u^{n-1}}{h}) &=  - \| \frac{u^n - u^{n-1}}{h}\|^2 
\\ &= - \frac{1}{2}\| \frac{u^n - u^{n-1}}{h}\|^2 - \frac{1}{2}\| \nabla E(u^n)\|^2
\end{align}
$$
because of (E-L): $\nabla E(u^n) = - \frac{u^n - u^{n-1}}{h}$ 

#### 3. How to Cure? case 1 in the general case?

![image-20190504130225812](Math 126 Lecture 38.assets/image-20190504130225812.png)

De Giorgi's Variational interpolation: 

For simplicity, let's only consider the 1st iteration starting from $u^0$. 

Then we define, for $t \in (0, h]$ 

$v(t)$ to minimize $\frac{1}{2t}\|v - u^0\|^2 + E[v]$. So for $t = h, v(h)  = u^1$, Hence "interpolation "

---

Renee Note:

for $t = h$: We have the MM problem: because we are only considering $n = 1$, we have that the minimizer would be $u^1$ 



Also, I believe, we are fixing $t$ and then we choose which value $v(t)$ would minimize

$\frac{1}{2t}\| v - u^0\|^2 + E[v]$ which allows us to minimize

---

and set 
$$
e(t) := \frac{1}{2t}\|v(t)- u^0\|^2 + E[v(t)]
$$

#### Claim: 

$$
(*) \qquad \frac{d}{dt}e(t) = - \frac{\| v^l(t)- u^0\|^2}{2t^2}\quad t \in (0,h)
$$

Indeed, by definition, for $0 < s < t < h$ 
$$
\begin{align} e(t) &= \frac{1}{2t}\|v(t) - u^0\| ^2 + E[v(t)] 
\\ &\underset{v(t) \min}{\leq} \frac{1}{2t}\|v(s) - u^0\|^2 + E[v(s)]
\\ &= e(s) + (\frac{1}{2t} - \frac{1}{2s})\| v(s) - u^0\|^2
\end{align}
$$


Hence:
$$
\frac{e(t) - e(s)}{t-s} \leq - \frac{1}{2st}\| v(s) - u^0\|^2 \overset{s \uparrow t}{\rightarrow } - \frac{1}{2t^2}\|v(t) - u^0\|^2
$$
The reverse inequality is similar with $t < s$ 

**End of Proof of Claim** 

---

#### Now Integrate $(*)$ 

$$
e(h) - e(0) = \int_0^h \frac{d}{dt}e(t)\, dt = - \frac{1}{2}\int_0^h \| \frac{v(t) - u^0}{t}\|^2 \, dt
$$

and
$$
\frac{1}{2h}\| \underset{ = u^1}{v(h) } - u^0\| + E[\underset{= u^1}{v(h)}] - (0 + E[u^0]) = e(h) - e(0)
$$

---

Renee Note: so apparently $v(0) = u^0$ so $u^0$ 

I think this is due to $\frac{1}{2t}$ blowing up as $t \rightarrow 0$, so if $v(0) - u^0 \neq 0$, it would always $\rightarrow \infin$ 

---

Hence:
$$
\frac{E[u^1]- E[u^0]}{h} = - \frac{1}{2}\int_0^h \| \frac{v(t)- u^0}{t}\|^2 \, dt - \frac{1}{2}\|\frac{u^1 - u^0}{h}\|^2
$$
Question: Where's the $h$ in the first term on the RHS? Didn't we divide by $h​$? 

This looks almost like $(ED)$ 

Here's the missing bit:

**Define** 
$$
\begin{align} | \partial E|(u) &:= \underset{w \rightarrow u}{\lim \sup} \frac{(E(u) - E(w))_+}{|u - w|}
\\ &= \text{maximal downward slope at }u
\end{align}
$$

#### Claim:

$$
|\partial E|(v(t)) \leq \frac{\| v(t) - u^0 \| }{t}
$$

This would imply that
$$
\frac{E[u^1] - E[u^0]}{h} \leq - \frac{1}{2}\int_0^h |\partial E|^2(v(t))\, dt - \frac{1}{2}\| \frac{u^1 - u^0}{h}\|^2
$$
So after ____ over all time intervals and telescoping the LHS
$$
\sum_{n = 1}^N \frac{E[u^n] - E[u^{n-1}]}{h} \leq \sum_{n = 1}^N - \frac{1}{2}\int_{(n-1)h}^{nh} |\partial E|^2(v(t)) \, dt - \frac{1}{2}\| \frac{u^n - u^{n-1}}{h}\|^2
$$
Or equivalently
$$
E[u^h(Nh)] + \int_0^{Nh}(\frac{1}{2}|\partial E|^2(v(t)) + \frac{1}{2}\| \frac{u^n - u^{n-1}}{h}\|^2)\, dt \leq E[u^0]
$$
which is almost $(ED)$ 

##### Proof of Claim

$$
\begin{align}
E[v(t)] - E[w] \underset{v(t)\min}{\leq} \frac{1}{2t}(\|w - u^0\|^2 - \|v(t) - u^0\|^2)
\end{align}
$$

Where
$$
\|w - u^0\|^2 - \|v(t) - u^0\|^2 = \|w\|^2 - \| v(t)\|^2 + 2(u^0, v(t)-w)
$$

---

Renee note:

$(w-u^0, w - u^0) - (v(t)-u^0, v(t) -u^0)$ $ = \|w\|^2 - (u^0, w) - (w, u^0) + \|u^0\|^2 - \|v(t)\|^2$ 

$+ (u^0, v(t)) + (v(t), u^0) - \| u^0\|^2$ 

$ = \|w\|^2 - \|v(t)\|^2  - 2(u^0, w) + 2(u^0, v(t))​$ 

---

Then because:

$\|w\|^2 - \| v(t)\|^2 = (w, w)  - (v(t), v(t)) + (w, v(t)) - (w, v(t))$ 

$ = (w, w+ v(t)) - (v(t), v(t) + w(t))$ $= (w - v(t), w + v(t))$ $ = - (v(t) - w, w + v(t))$ 

Then plugging back into the original equation:
$$
E[v(t)]- E[w] \leq \frac{1}{2t}(v(t) - w, 2u^0 - w - v(t))
$$
Therefore:
$$
\begin{align} \frac{(E[v(t)] - E[w])_+}{\| v(t)- w\|} \leq \frac{\| 2u^0 - v(t) -w\|}{2t} &\overset{w \rightarrow v(t)}{\rightarrow} \frac{2\| u^0 - v(t)\|}{2t}
\\ &=  \frac{\| v(t) - u^0\|}{t}
\end{align}
$$

---

**OMG**: Cauchy Inequality: is how we get

$\frac{1}{2t}\frac{(v(t)-w, 2u^0 - w - v(t))}{\| v(t)-w\|} \leq \frac{\| 2u^0 - v(t)-w\|}{2t}$

because:

$(v(t) - w, 2u^0 - w - v(t)) \leq \| v(t)-w\| \| 2u^0 - w - v(t)\|$ 

---

#### Applications

##### 1. Definition of generalized gradient flows:

note that $\min_{u \in X}\{ \frac{1}{2h}\| u - u^0 \|^2 + E[u] \}$ would make sense in an arbitrary metric space $(X, d)$: 
$$
\min \{ \frac{1}{2h} d^2(u, u^0) + E[u] \}
$$
In fact this allows to give sense to the equation 
$$
u_t = - \nabla E(u)
$$
in metric spaces!

For more details, see [Ambrosio- Gigli? - Savare, Birklia??ter 2008]

##### Converge of numerical schemes for PDEs
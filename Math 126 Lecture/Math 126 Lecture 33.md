# Math 126 Lecture Notes 33

# 14 Nonlinear PDEs

## 14.1 Shock Waves

Shcok waves occur in many applications (e.g. traffic flow, fluid dynamics, etc.) The simplest such equation is the first-order equation. 
$$
(1) \qquad u_t + a(u)u_x = 0
$$
Before exploring the new phenomena due to the nonlinearity $a(u)u_x$, let's recall <u>linear</u> first-order equations from week 2:

Consider
$$
u_t + a(x, t)u_x = 0
$$
Characteristic curves are defined by
$$
\frac{dx}{dt} = a(x,t)
$$
**<u>Note:</u>** Every point $(x_0, t_0)$ has a unique characteristic curve passing through it - because the ODE
$$
\begin{align}
\frac{dx}{dt} &= a(x, t)
\\ x(t_0) &= x_0
\end{align}
$$
has a unique solution, say, $x = x(t; x_0, t_0)$ 

Now **differentiate u** along such a curve:
$$
\frac{du}{dt} = \frac{d}{dt}(u(x(t), t)) = u_x \frac{dx}{dt}+ u_t = u_x a + u_t = 0
$$
Hence:
$$
u \bold{\text{ is constant along characteristics}}
$$


#### Examples

##### Example 1

Consider $u_t + e^{x+t}u_x = 0​$ 

Characteristic equation: $\frac{dx}{dt} = e^{x+t}​$ $\equiv​$ $e^{-x}dx = e^t dt​$ 

The solutions are given by $-e^{-x} = e^t - C​$ or $e^{-x} = C - e^t​$, 
$$
x = - \log(C-e^t)
$$
The general solution is therefore
$$
u(x,t) = f(e^{-x}+e^t)
$$
for an arbitrary function $f​$. 

##### Example 2

Let's solve **Example 1** with initial conditions
$$
u(x,0) = \phi(x)
$$
By **Example 1**: $\phi(x) = f(\underset{=: s}{e^{-x}+1})$, $x = -\log(s - 1)$, so $f(s) = \phi(-\log(s-1))$, 
$$
u(x,t) = \phi(-\log(e^{-x}+e^t - 1))
$$
I suppose $s = e^{-x}+e^t$ and $u(x,t) = f(e^{-x}+e^t) = f(s) = \phi(-\log(s-1)) = \phi(-\log(e^{-x}+e^t - 1))$ 

##### Example 3

Burgers' Equation
$$
(2) \qquad u_t + u u_x = 0
$$
is the simples form of **equation 1**. But ***be careful***, it's not as easy as it seems!

**Characteristic Curves**
$$
(3)\qquad \frac{dx}{dt} = u(x,t)
$$
Because we are solving a nonlinear PDE, the characteristic equation (3) depends on the solution $u$ of $(2)$: Each solution $u$ will give different characteristics

Nevertheless, we still have:
$$
(4)\qquad \frac{d}{dt}(u(x(t), t)) = u_t + u_x \frac{dx}{dt} = u_t + u_x u \overset{(2)}{=} 0
$$
So:
$$
u \bold{\text{ is constant along each characteristic curve}}
$$
Furthermore, since
$$
(5)\qquad \frac{dx}{dt} = u(x(t), t) = u(x_0, t_0) = \text{constant}
$$
(So we have that since $u$ is constant along each curve, $dx/dt = u(x(t), t)$ is constant) 

$u$ is constant on each curve $x(t)$ (which we don't know), so above we see that for each curve $x(t)$, $u(x(t), t) = u(x_0, t_0)$, where $x_0 = x(t_0)$ for that given curve.

so the slope of each $x(t)$ is a constant $(u(x_0, t_0))$ (depends on its initial conditions)

We can already infer:

($\alpha$) **Each characteristic curve is a straight line**. So each solution $u(x,t)$ has a family of straight lines (of various slopes) as its characteristics

($\beta$) **The solution is constant on each line** 

($\gamma$) **The slope of each line is equal to the value of $u(x,t)$ on it** 



Suppose now that we ask for a solution of the PDE that satisfies the initial condition
$$
(9) \qquad u(x,0) = \phi(x)
$$
By $(\gamma)$, the characteristic passing through $(x_0,0)$ has to have a constant slope $\phi(x_0)$ 

![image-20190421220741030](Math 126 Lecture 33.assets/image-20190421220741030.png)

Similalrly, the characteristic line through $(x_1, 0)$ must have slope $\phi(x_1)$ 

If the two lines intersect: we're in trouble

**Note**: If we are lucky and $\phi$ is increasing, then the characteristics don't cross for positive times. 

Because note, we have $t = \frac{1}{\phi(x_n)}(x - x_n)$ so $1/\phi$ decreases as $\phi$ increases

But in general, shocks may form - We need to sxtend our concept of solutions

![image-20190421225249581](Math 126 Lecture 33.assets/image-20190421225249581.png)

Where two solutions exists, we can give a precise formula because the slope of the characteristic through $(x,t)$ is
$$
\frac{x-x_0}{t-0} = \frac{dx}{dt}= u(x,t) = u(x_0, 0) = \phi(x_0)
$$
so
$$
(6)\qquad x-x_0 = t \phi(x_0)
$$
Which is the (implicit!) formula for $x_0 = x_0(x,t)$ in terms of $(x,t)$ and 
$$
(7) \qquad u(x,t) = \phi(x_0(x,t))
$$

##### Example 4

Consider Example 3 with initial condition
$$
(8) \qquad \phi(x)= x^2
$$
Then (6) becomes
$$
x-x_0 = tx_0^2 \iff tx_0^2 + x_0 - x = 0
\\ \iff x_0 = \frac{-t \pm \sqrt{1 + 4tx}}{2t}, t \neq 0
$$
So by (7), the solution to Burgers' equation (2) with data (8) is
$$
u(x,t) = \phi(x_0) = (\frac{-1 \pm \sqrt{1 + 4tx}}{2t})^2 = \frac{1 \mp 2 \sqrt{1 + 4tx} + (1 + 4tx)}{4t^2}
\\ = \frac{1 + 2tx \mp \sqrt{1+4tx}}{2t^2}, t \neq 0
$$
But $u$ is not even defined for $t = 0$. We want
$$
x^2 = u(x,0) = \lim_{t \rightarrow 0}\frac{1 + 2tx \mp \sqrt{1 + 4tx}}{2t^2}
$$
With the "+"-sign: has limit $(\frac{1}{0}) = \infin$ 

With the "-"-sign: $\frac{0}{0}$ is the limit, so we want to use L'hopitals rule:

$\lim_{t\rightarrow 0} \frac{2x - \frac{2x}{\sqrt{1 + 4tx}}}{4t} = \frac{0}{0}$, and again L'hopitals rule:
$$
\lim_{t\rightarrow 0} \frac{4x^2(1 + 4tx)^{-3/2}}{4} = x^2
$$
So our solution is
$$
u(x,t) = \begin{cases} \frac{1 + 2tx - \sqrt{1 + 4tx}}{2t^2}& t \neq 0 \\ x^2 & t = 0 \end{cases}
$$
This is the unique continuous solution, but it's only defined if $1 + 4tx \geq 0$, so $tx \geq - \frac{1}{4}$ 

---

Back to the general equation:
$$
(1) \qquad u_t + a(u)u_x = 0
$$
Characteristics:
$$
\frac{dx}{dt} = a(u(x,t))
$$
Along Characteristics:
$$
\frac{du}{dt} = \frac{d}{dt}(u(x(t), t)) = u_t + u_x \frac{dx}{dt} = u_t + u_x a(u) \overset{(1)}{=} 0
$$
So even in the general case:
$$
\text{The characteristics are straight lines, and the solution is constant along them}
$$
So if the characteristics don't intersect we can easily solve (1) with initial conditions $u(x,0) = \phi(x)$ 

![image-20190421225204404](Math 126 Lecture 33.assets/image-20190421225204404.png)

The slope of the characteristic line is:
$$
\frac{x-z}{t-0}= \frac{dx}{dt} = a(u(x,t), t) = a(u(z, 0)) = a(\phi(z))
$$
Hence $x-z = t\, a(\phi(z))$ and $u(x,t) = u(z,0) = \phi(z) = \phi(z(x,t))$ 

only if the slope is increasing as a function of $z$, i.e., 
$$
a(\phi(z))\leq a(\phi(w))\qquad \text{whenever }z\leq w
$$
 

Solutions exist throughout the entire half plane $t \geq 0$. 

Such a solution is called an **expansive wave** or a **rarefaction wave** 

In the general case, characteristics may cross. What happens close to such a shock?

Wave speed: $a(u)$ 

As it depends on the value of $u$, some parts of the wave may move faster than others

E.g.: each hump is $t = 0$, $t = t_1$, $t = t_2$ 

![image-20190421225304623](Math 126 Lecture 33.assets/image-20190421225304623.png)

have $a(u)$ larger as $u$ increases, so $dx/dt$ at larger $u$ is larger $\implies$ at the top of these humps, the wave moves faster 

What happens exactly and how to use it? **Next time!** 
# Math 126 Lecture 34

# Shocks & Discontinuous Solutions

(14.1 Continued)

**Recall**: We considered the general PDE
$$
(1)\qquad u_t + a(u)u_x = 0
$$
We may think of the special case of Burger's Equation

 
$$
(2)\qquad u_t + uu_x = 0
$$
Define:
$$
A(z):= \int_0^z a(s)\, ds
$$
then $A'(z)= a(z)$ and (1) because
$$
(3)\qquad u_t + (A(u))_x = 0 
$$
Note about (3): Divergence form "conservative law" since $\frac{d}{dt}\int u = \int u_t = - \int A(u)_x \, dx = 0$ if $A \rightarrow 0$ as $x \rightarrow \pm \infin$ 

Today we want to understand what happens when a shock forms - we want to describe $u$ at the shock.

**Idea**: Consider generalized solutions, often called "weak" solutions. We want to use our knowledge of distributions (?) to generalize solutions

#### Method:

##### Step 1

Let $\psi = \psi(x, t)$ be any smooth function which has bounded support and equals zero for $ t\leq 0$: 

![image-20190423142741553](Math 126 Lecture 34.assets/image-20190423142741553.png)

##### Step 2

Multiply equation (3) by $\psi$, integrate in $x$ and $t$, and integrate by parts:

**Definition**: Let $u = u(x,t)$ be a bounded function. We say $u$ is a weak solution to (3) $u_t + (A(u))_x = 0$ if 
$$
\forall \psi: \int_0^\infin \int_{\R}(u \psi_t + A(u)\psi_x)dx\, dt = 0
$$
This simply means that (3) holds in the sense of distributions 

#### Example: Burgers' Equation

$$
\begin{align}
u_t + uu_x &= 0 &t>0
\\ u &= \phi & t = 0
\end{align}
$$

Characteristic equation: $\dot x = a(u) = u = \phi(x_0)$ 
$$
\phi(x) = \begin{cases} 
1 & x<0 &\rightarrow \dot x = 1
\\ 1-x & 0 \leq x \leq 1 & \rightarrow \dot x = 1 - x_0 \rightarrow x(t) = (1 - x_0)t + x_0
\\ 0 & x >1 & \rightarrow \dot x = 0 
\end{cases}
$$
![image-20190423143541030](Math 126 Lecture 34.assets/image-20190423143541030.png)

Then

![image-20190423151508897](Math 126 Lecture 34.assets/image-20190423151508897.png)

Shock Wave

$x = \xi(t) = \frac{1+t}{2}$ 

$\dot \xi = $ speed of shock wave $= \frac{1}{2}​$ 

The solution $u$ for $t<1$ is
$$
u(x,t) = \begin{cases}
1 & x \leq t
\\ \frac{1-x}{1-t}& t \leq x \leq 1
\\ 0 & x \geq 1
\end{cases}
$$
How can we build shock waves?

![image-20190423151920999](Math 126 Lecture 34.assets/image-20190423151920999.png)

Notation: $c(t) = \dot \xi(t) = $ speed of shock wave

so $\Sigma$ seems to denote the shock wave

$n$ is the normal on the shock wave

#### Theorem (Routine - Hugouiot Formula)

$$
c(t) = \dot \xi(t) = \frac{A(u^+) - A(u^-)}{u^+ - u^-}
$$

**Proof**: For any $\psi$, by definition of weak solution
$$
0 = \int_0^\infin \int_0^{\xi(t)}u^- \psi_t + A(u^-)\psi_x\, dx \, dt + \int_0^\infin \int_{\xi(t)}^\infin u^+ \psi_t + A(u^+)\psi_x\, dx\, dt
\\ \overset{\text{IBP}}{=} \int_0^\infin \int_{-\infin}^{\xi(t)} -\psi(u^-_t + (A(u))_x)\, dx\, dt
\\ + \int_{\Sigma} - u^- \psi n_2 - A(u^-) \psi n_1\, ds
\\ + \int_0^\infin \int_{\xi(t)}^\infin -\psi(u_t^+ - (A(u^+))_x)\, dx \, dt
\\ + \int_{\Sigma} u^+ \psi n_2 + A(u^+)\psi n_1\, ds
$$

---

So next to $u_t^+ + (A(u^+))_x = 0 \\ u_t^- + (A(u^-))_x = 0$   away from $\Sigma$ 

we obtain
$$
(u^+ - u^-)n_2 + (A(u^+) - A(u^-))n_1 = 0
$$
so 
$$
\frac{A(u^+)- A(u^-)}{u^+ - u^-} = - \frac{n_2}{n_1}
$$
![image-20190423153434701](Math 126 Lecture 34.assets/image-20190423153434701.png)

Tangent Vector to $\Sigma$? 
$$
\bold t = \frac{1}{\sqrt{1 + \dot \xi^2}}\begin{pmatrix} \dot \xi(t) \\ 1 \end{pmatrix}
\\ \bold n = \frac{1}{\sqrt{1 + \dot \xi^2}}\begin{pmatrix} -1 \\ \dot \xi(t) \end{pmatrix}
$$
So:
$$
n_1 = \frac{-1}{\sqrt{1 + \dot \xi^2}}\qquad n_2 = \frac{\dot \xi}{\sqrt{1 + \dot \xi^2}}
$$

$$
- \frac{n_2}{n_1} = \dot \xi
$$

##### Back to the example:

$a(u) = u \qquad A(u) = \frac{1}{2}u^2$ 
$$
\dot \xi = \frac{A(u^+) - A(u^-)}{u^+ - u^-} = \frac{\frac{1}{2}0^2 - \frac{1}{2}1^2}{0-1} = \frac{1}{2} 
$$
![image-20190423154834459](Math 126 Lecture 34.assets/image-20190423154834459.png)

So indeed, $\xi = \frac{1 + t}{2}$ 

##### Another Example

$$
\begin{align}
u_t + uu_x &= 0 & t > 0
\\ u &= \phi & t = 0
\end{align}
$$

$$
\phi(x) = \begin{cases} 0 & x < 0 \\ 1 & x >0 \end{cases}
$$

![image-20190423155150569](Math 126 Lecture 34.assets/image-20190423155150569.png)

![image-20190423155215802](Math 126 Lecture 34.assets/image-20190423155215802.png)

**Two ways to Fill the Gap**: 
$$
u_1(x,t) = \begin{cases} 0 & x < t/2 \\ 1 & x > t/2 \end{cases}
$$

$$
u_2(x, t) = \begin{cases} 0 & x < 0  \\ x/t & 0 < x < t \\ 1 & x>t  \end{cases}
$$

Rarefaction Wave:

![image-20190423155359969](Math 126 Lecture 34.assets/image-20190423155359969.png)

To rule out this non-uniqueness issue we need to gaurantee that the characteristics go into the shocks:

**Entropy condition**
$$
a(u^-) > c(t) > a(u^+)
$$

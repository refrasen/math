# Math 126 Lecture 8

## Comparison of Waves and Diffusions

**waves**: information gets transported in both directions at a finite speed

**diffusions**: initial disturbance gets spread out in a smooth fashion and gradually disappears

| Property                               | Waves $u_{tt}-c^2u_{xx} = 0$                                 | Diffusion $u_t - ku_{xx} = 0$                                |
| -------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| (i) Speed of propogation?              | Finite ($\leq c$) [^1]                                       | Infinite($+\infin$)[^2]                                      |
| (ii) Singularities for $t > 0$?        | Transported along characteristic curves, (speed = $c$)       | Lost immediately e.g. $(Q(x,t))$ ("smooths out")             |
| (iii) Well-posed for $t> 0$?           | Yes                                                          | Yes (for bounded solution)                                   |
| (iv) Well-posed for $t< 0$?            | Yes                                                          | No                                                           |
| (v) Maximum principle                  | No                                                           | Yes                                                          |
| (vi) Behavior $t \rightarrow +\infin$? | Energy is constant/ preserved, so does not decay... Best we can hope for is that $u$ looks like a single wave moving w/ constant speed as $t \rightarrow +\infin$ | Decays to zero (if $\phi$ integrable) or convergence to steady state on boundary integral with boundary conditions. expect convergence to steady state |
| (vii) Information                      | Transported                                                  | Lost Gradually                                               |

[^1]: See End of 2.1, First part 2.2
[^2]: page 55, 2.5

Solution Equations:

**wave**: 
$$
u(x,t) = \frac{1}{2}[\phi(x+ct)+\phi(x-ct)]+ \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(s)\; ds
$$
$u(x_0, 0) = \frac{1}{2}[\phi(x_0)+\phi(x_0 )] = \phi(x_0)$ 



**Diffusion**: 
$$
Q(x,t) = \frac{1}{2} + \frac{1}{\sqrt\pi}\int_0^{x/\sqrt{4kt}}e^{-p^2}dp
$$

$$
S = \frac{\partial Q}{\partial x} = \frac{1}{2\sqrt{\pi kt}} e^{-x^2/4kt}
$$


$$
u(x,t) = \int_{-\infin}^\infin S(x-y, t)\phi(y)\; dy \quad\text{ for }t>0
$$

$$
u(x,t) = \frac{1}{\sqrt{4\pi kt}}\int_{-\infin}^\infin e^{-(x-y)^2/4kt}\phi(y)\; dy
$$



Where $\phi(y)​$ is assumed to disappear for large $|y|​$ 

##### (i) Speed of Propogation

**Waves**: 

$\phi(x)$: pair of waves traveling in either direction at speed $c$ and at half the original amplitude (realtively simple to undersatnd with no initialy velocity)

$\psi(x)$: a wave spreading out at speed $\leq c$ in both directions

(I suppose the idea is that because it may be "pulled up" from the initial velocity and so will lag behind)

**Diffusion**: 

$\phi(y)$ represents weights...

$u(x,t)$ depends on $\phi(y)$ for all $y$, $-\infin < y < \infin$ **This is Important** 

**Question**: What is $y$? Why does it matter that $u(x,t)$ depends on $\phi (y)$ for all $y$ 

*Answer*: $y$ is the position of where a mass of weight/magnitude(?) $\phi(y)$ is. 

SO BECAUSE at any positive $t> 0$: $u(x,t)$ depends on $\phi(y)$ for all $y$, even $y$ far away, (hence the integral from $-\infin$ to $\infin$), $\infin$, the speed is infinite. 

$\phi(x_0)$ ($\phi$ at a point $x_0$ has an **immediate effect everywhere** for positive $t > 0$, even though most of its effect is only for a short time near $x_0$ 

**COME BACK TO THIS AND LOOK AT OLD NOTES** 

Also, Exercise 2(b) shows that solutions of the diffusion equation can travel at any speed.

---

##### (ii) Singularities for $t > 0$ 

**Waves**: Obvious from the solution equation, and just knowing that waves travel at a speed $\leq c$ 

**Diffusion**: Section 3.5, but Professor mentions $Q(x,t)$ smoothing out. 

---

##### (iii) Well-posed for positive time $t > 0$ 

**Waves**: See page 26, 

We already know existence, uniqueness (page 35)

Well-Posedness... comes from being differentiable??? if $\phi$ is continuously differentiable up to second derivative and $\psi$ has a continuous first derivative, we have that $u$ has continuous second partial derivatives... and so for small changes in $\phi$ and $\psi$, we should get small changes in $u$ 

Also, consider the information being transported along characteristic lines...

so we know for a small change in the initial conditions, it'll result in a relatively small change in how information gets transported?

Or for a small change in $x$ or $t$... see again the way it gets transported...

**Diffusion**: See 2.3, since the maximum principle $\implies$ uniqueness and stability, Existence of solution was shown in 2.4

---

##### (iv) Well-Posed for negative time $t< 0$ 

**waves**: again, due to being transported along the characteristic lines...

$\phi(x+ct)$ and $\phi(x-ct)$ 

we have for $t < 0$

$\phi(x-ct) = \phi(x + c|t|)$ ...

also easy to see how it acts "backwards" in time, since energy is preserved?

Like, imagine a wave going back and forth...

**Diffusion**: see below

---

##### (v) Maximum Principle

**waves**: I think it's due to information propogating/being transported, so at some point in the "interior" of $xt$ -space we have any boundary information here as well. 

**diffusion**: Shown explicitly in chapter 2.3

---

##### (vi) Behavior as $t \rightarrow + \infin$

**waves**: Already shown in 2.2 that Energy is preserved

**Diffusion**: Already shown by the graphs of $S(x, t)$ that it decays to $0$ "spreads out" or reaches a steady-state (like heat equation, especially if there's a "source")

See the equation: $e^{-(x-y)^2/4kt}$ will decay to $0$ as $ t\rightarrow \infin$ as will $\frac{1}{\sqrt{4\pi kt}}$. 

But, it decays if $\phi$ is integrable...

But I'm confused, we had an integrable function $\phi(x)= e^{-x}$ and we had $u(x,t) \rightarrow \infin$ as $ t \rightarrow \infin$ since $u(x,t) = e^{kt - x}$ (due to "left side of the rod" being "initially very hot")

---

##### (vii) Information

**waves**: Clear from earlier discussions that information gets transported 

* See example graph ('movies')
* See the solution equation itself

**Diffusion**: Clear from the graph of a typical solution from $S(x,t)$ 

---

#### Coming back to the Diffusion equation not being well-posed for $t< 0$: 

$u_t - ku_{xx} = 0$ is not well posed for $t < 0$: 

1. ##### Let $u_n(x,t) = \frac{1}{n}\sin (nx) e^{-n^2kt}$ 

Then $\frac{\partial u_n}{\partial t} - k \frac{\partial^2 u_n}{\partial x^2} = 0$ for all $t, x$ 

*checking* 
$$
\frac{\partial u_n}{\partial t} = -nk \sin(nx) e^{-n^2 kt}\\\frac{\partial u_n}{\partial x} = \cos(nx)e^{-n^2kt}\implies\frac{\partial ^2 u_n}{\partial x^2} = -n \sin(nx)e^{-n^2 kt}
$$
**and** $u_n(x,0) = \frac{1}{n}\sin(nx)$ $\rightarrow 0$ **uniformly** as $n\rightarrow \infin$, so $\max_x|u_n(x,0)| \rightarrow 0 $ as $n \rightarrow \infin$  

* no matter what value $x​$, $u_n(x,0)​$ will approach $0​$ 
* Since the max of $|u_n(x,0)|​$ is when $x = \frac{l\pi}{2n}​$ i.e., $\sin(nx) = \pm1​$, so the max ends up being $\frac{1}{n}​$, which does approach 0 as $n \rightarrow \infin​$ 

<u>But</u> $u_n(x,t)$ for $t< 0$, e.g., $t = -1$: 

$u_n(x, -1) = \frac{1}{n}\sin(nx)e^{n^2 k} \rightarrow \pm \infin$ uniformly, at least away from the points $x = l \pi$, so uniformly at points where $\sin(nx) \neq 0$ 

So $u_n(x,0)$ is close to 0 as $n \rightarrow \infin​$ 

but $u_n(x, -1)$ isn't close to $0$ as $n \rightarrow \infin$ 

[For a visualization][https://www.desmos.com/calculator/smiyplv9ei]

**How this violates Stability**: 

$u_n \approx 0$ at $t = 0$, but is nowhere close to $0$ whenever $t < 0$ (even small $t < 0$)

2. ##### Let $u(x,t) = S(x, t+1)$; 

$$
S(x,t+1) = \frac{1}{\sqrt{4\pi k(t+1)}}e^{-\frac{x^2}{4k(t+1)}}
$$

at $t = 0$, $u(x,0) = S(x,1)$ 

**NOte**: 
$$
S_t(x,t+1) = -\frac{4\pi k}{2(4\pi k(t+1))^{3/2}}e^{-x^2/4k(t+1)}+ \frac{1}{\sqrt{4\pi k(t+1)}}(\frac{4kx^2}{(4k(t+1))^2})e^{-x^2/4k(t+1)}
\\ = e^{-x^2/4k(t+1)}(\frac{-2\pi k }{(4\pi k (t+1))^{3/2}} + \frac{4kx^2}{\sqrt{4\pi k(t+1)}(4k(t+1))^2})
\\ = ke^{-x^2/4k(t+1)}[\frac{-2}{(4k(t+1))(\sqrt{4\pi k (t+1)})} + \frac{4x^2}{\sqrt{4\pi k(t+1)(4k(t+1))^2}}]
$$

$$
S_x(x, t+1) = \frac{1}{\sqrt{4\pi k(t+1)}}(-\frac{2x}{4k(t+1)})e^{-x^2/4k(t+1)}
\\ S_{xx}(x, t+1) = e^{-x^2/4k(t+1)}[- \frac{2}{\sqrt{4\pi k(t+1)}(4k(t+1))} + \frac{4x^2}{\sqrt{4\pi k(t+1)}(4k(t+1))^2}]
$$

So from the above, we have $u_t - ku_{xx} = 0$ for $t > -1$, but $u(x,t) = S(0, t+1)$ $\rightarrow +\infin$ as $t \rightarrow -1$ (from the right?). 

$\implies$ we cannot solve the equation for $t \leq 1$ "blows up for $t = -1$"

[For a visualization, I hope this works][https://www.desmos.com/calculator/d8lkvgskwl

---

## Chapter 3 Reflections & Sources

### Diffusion on the Half-Line

Simplest case $D = (0, \infin)$, i.e., $0 < x < \infin$ 

Take the *Dirichlet boundary condition* at the single endpoint $x = 0$ 
$$
(1) \begin{cases} v_t - kv_{xx} = 0 & \text{in}\{0<x<\infin, 0 <t<\infin \}
\\ v(x, 0) = \phi(x) & \text{for }t = 0
\\ v(0, t) = 0 & \text{for }x = 0
\end{cases}
$$

* The PDE is supposed to be satisfied in the open region $\{0 <x<\infin, 0 < t<\infin \}$ 
* If the solution exists, it is unique due to the discussion in 2.3 (talks about uniqueness of diffusion equation solution)
* Interpet as the temp in a very long rod with one end immersed in a reservoir of temperature = 0 and with insulated sides

I suppose the issue here is that all our solutions were for $-\infin < x <\infin$, so we are dealing with a half line/ semi-infinite line

#### Idea: Reduce new problem to old one using *odd function* 

![Screen Shot 2019-02-09 at 9.52.22 PM](Math 126 Lecture 8.assets/Screen Shot 2019-02-09 at 9.52.22 PM.png)

**Odd Function Definition**:

$\phi_{\text{odd}}(-x) = -\phi_{\text{odd}}(x)$ for all $x > 0$ 

The graph of an odd function, $y = \phi_{\text{odd}}(x)$ is symmetric with respect to the origin (see Figure 1 above)

Automatically, by putting $x =0$ in the definition, $\phi_{\text{odd}}(0) = 0$ 

Note: $\phi(x)$, the intitial datum of the problem, is defined only for $x \geq 0$ 

##### So let's define our odd extenstion of $\phi$ to the whole line:

$$
\phi_{\text{odd}}(x):= \begin{cases}\phi(x) & \text{for }x>0 
\\ - \phi(-x) & \text{for }x<0 
\\ 0 & \text{for }x = 0
\end{cases}
$$

##### Now we can solve the initial value problem

$$
(2) \begin{cases}
u_t - ku_{xx} = 0 & - \infin < x < \infin, t > 0
\\ u(x,0) = \phi_{\text{odd}}(x) & t = 0 
\end{cases}
$$

**Difference**: this is defined for the **whole line** 

From 2.3, we know a solution for this problem is:
$$
u(x,t) = \int_{-\infin}^\infin S(x-y, t)\phi_{\text{odd}}(y)dy
$$

##### **Claim**: the restriction of $u(x,t)$ is a unique solution of (1)

$$
v(x,t) = u(x,t), \qquad x>0
$$

Is the solution of (1)

Note: no difference between $u$ and $v$ except that the negative values of $x$ are not considered when discussing $v$

**Argument**: 

First of all, because $v = u​$ for $x > 0​$ and $u​$ satisfies the same PDE for all $x​$ and the same initial condition for $x > 0​$ 

* $v_t - kv_{xx} = u_t - ku_{xx} = 0$ for $x > 0$, $t > 0$ (solves PDE)
* $v(x,0) = u(x, 0) = \phi_{\text{odd}}(x) = \phi(x)​$ for $x > 0​$ (solves initial condition)

Secondly, Since $\phi_{\text{odd}}$ is odd, $u$ is odd: $u(-x, t) = -u(x, t)$ 

In particular: $u(0, t) = u(-0, t) = -u(0, t)$ which only occurs when $u(0,t) = 0$,

so we have $v(0, t) = 0$, the boundary condition, *automatically* satisfied

**Confusion**: $v = u$ only for $x > 0$, so how does $v(0,t) = u(0,t)$? 

I suppose since as $x \rightarrow 0^+$, $u(x, t) \rightarrow 0$, since $u(0, t) = 0$, and thus it makes sense that $v(0,t) =0$ as well, since for very small $x > 0$, $v(0,t) \approx 0$ 

**Explicit Formula for $v(x,t)$**: 

using the formula for $u(x,t)$ with $\phi_{\text{odd}}$ and the fact that $\phi_{\text{odd}}(y) = -\phi(-y)$ for $y < 0$ 
$$
u(x,t) = \int_0^\infin S(x-y, t)\phi(y)dy + \int_{-\infin}^0 S(x-y, t)[-\phi(-y)] dy
\\ = \int_0^\infin S(x-y, t)\phi(y)\; dy - \int_{-\infin}^0 S(x-y, t)\phi(-y)\; dy
$$
Then changing the variable $-y$ to $+y$ in the second integral:
$$
u(x,t) = \int_0^\infin S(x-y, t)\phi(y)dy - \int_{\infin}^0 S(x+y, t)\phi(y)(-dy)
\\= \int_0^\infin [S(x-y, t) - S(x+y, t)]\phi(y)dy
$$

$$
v(x,t) = \frac{1}{\sqrt{4\pi k t}}\int_0^\infin[e^{-(x-y)^2/4kt}- e^{-(x+y)^2/4kt}]\phi(y)]; dy
$$

What we just did is called the **Method of odd extensions** or **reflection method** . 

In book: Two examples that could be helpful. 

First example:

**Homework**: 
$$
w_t - kw_{xx} = 0 \qquad\text{for }0 < x <\infin, 0<t<\infin
\\
w(x,0) = \phi(x) \\
w_{x}(0, t) = 0
$$
Solve the Neumann problem by **even** reflection

* Derivative of an even function is an odd function
  * $\implies$ Slope at the origin is $0$, as odd functions must be $0$ at origin

$\phi_{\text{even}}(x) = \begin{cases} \phi(x) & \text{for }x\geq 0 \\ + \phi(-x) & \text{for }x \leq 0 \end{cases}$

* then solve a new initial value problem in the form of one we know, call it $u$, i.e., over the whole $x$ line,  but with initial datum $\phi_{\text{even}}(x)$ 
* Restrict the solution you found for the new problem to the positive $x$-axis and justify why it is a solution (solves Neumann condition, solves initial value problem, solves PDE)
* Use the explicit formula of $u(x,t)$ and the definition of $\phi_{\text{even}}$ to obtain an explicit formula for $w(x,t)$ 

So the formula for this:

$w(x,t) = \frac{1}{\sqrt{4\pi kt}} \int_0^\infin [e^{-(x-y)^2/4kt}+e^{-(x+y)^2/4kt}]\phi(y)\; dy$

Second Example: Solve Example 1 neumann problem with $\phi(x) = 1$ 

Note: $\mathscr{E}\text{rf}(x) = \frac{2}{\sqrt{\pi}} \int_0^x e^{-p^2}dp$

so $w(x,t) = \frac{1}{\sqrt{4\pi k t}}\int_0^\infin [e^{-(x-y)^2/4kt} + e^{-(x+y)^2/4kt}]dy$ 

so $p = (x- y)/\sqrt{4kt}$ , $dp =- dy/\sqrt{4kt}$ || $p = (x+y)/\sqrt{4kt}$, $dp = dy/\sqrt{4kt}$ 

$w(x,t) = - \frac{1}{\sqrt\pi}\int_{x/\sqrt{4kt}}^{-\infin} e^{-p^2} dp + \frac{1}{\sqrt{\pi}}\int_{x/\sqrt{4kt}}^{\infin} e^{-p^2}dp $

$ = \frac{1}{\sqrt{\pi}}[\int_{-\infin}^0e^{-p^2}dp + \int_0^{x/\sqrt{4kt}}e^{-p^2}dp - \int_0^{x/\sqrt{4kt}}e^{-p^2}dp + \int_0^\infin e^{-p^2}dp]$

and since $\int_0^\infin e^{-p^2}dp = \frac{\sqrt{\pi}}{2}$ and $\int_{-\infin}^\infin e^{-p^2}dp = \frac{\sqrt\pi}{2}$ and from definition of $\mathscr E \text{rf}$ 
$$
w(x,t) = [\frac{1}{2} + \mathscr E\text{rf}(\frac{x}{\sqrt{4kt}})] + [\frac{1}{2}- \mathscr{E}\text{rf}(\frac{x}{\sqrt{4kt}})]
$$
$Q(x,t) = \frac{1}{2} + \frac{1}{2}\mathscr{E}\text{rf}(x/\sqrt{4kt})​$ 

and $w(x,t) = \int_0^\infin [S(x-y, t)+ S(x+y, t)]dy$ 

### Reflections of Waves

Again, dealing with half-line $D = (0, \infin)$ 

Begin with *Dirichlet problem* on the half line:
$$
\begin{cases}
\text{DE: }v_{tt}-c^2v_{xx} = 0 & \text{for }0<x<\infin \text{ and }-\infin<t<\infin \\
\text{IC: }v(x,0) = \phi(x) \qquad v_t(x,0) = \psi(x) & \text{for }t=0 \text{ and }0<x<\infin \\
\text{BC: }v(0,t) = 0 & \text{for }x = 0 \text{ and }-\infin < t < \infin
\end{cases}
$$
Let's do odd reflection once more on the initial functions:
$$
\phi_{\text{odd}} \text{ and } \psi_{\text{odd}}
$$
 Let $u(x,t)$ be the solution of the Initial Value Problem on $(-\infin, \infin)$ with the initial data $\phi_{\text{odd}}$ and $\psi_{\text{odd}}$ 

$\implies$ $u(x,t) = \frac{1}{2}(\phi_{\text{odd}}(x+ct) + \phi_{\text{odd}}(x-ct))+ \frac{1}{2c}\int_{x-ct}^{x+ct}\psi_{\text{odd}}(s)\; ds$ 

* $u(x,t)$ is once again an odd function of $x$ $\implies$ $u(0,t) = 0$, so the boundary condition is satisfied automatically
* Define $v(x,t) = u(x,t)$ for $0 <x<\infin$ (restriction of $u$ to the half-line), and it is the solution we are looking for. 

![Screen Shot 2019-02-09 at 11.31.25 PM](Math 126 Lecture 8.assets/Screen Shot 2019-02-09 at 11.31.25 PM-9784592.png)

Notice: for $x > c|t| > 0$: only positive arguments occur in the formula, so that $v(x,t)$ is given by the usual formula
$$
(2) \qquad v(x,t) = \frac{1}{2}[\phi(x+ct) + \phi(x-ct)]+ \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(s)\; ds
$$
Other region: $0 < x < c|t|$ so $0 < x < ct$ and $0 < x < -ct$ 

we have $\phi_{\text{odd}}(x-ct) = - \phi(ct - x) $ and so on, so 
$$
v(x,t) = \frac{1}{2}(\phi(ct+x) - \phi(ct-x)) + \frac{1}{2c}[\int_{x-ct}^0 -\psi(-s)\; ds + \int_0^{x+ct}\psi(s)\; ds]
$$
where
$$
\int_{x-ct}^0-\psi(-s)\; ds = \int_{ct-x}^0 \psi(s)ds 
$$
so we end up with
$$
(3) \qquad v(x,t) = \frac{1}{2}[\phi(ct+x)-\phi(ct-x)] + \frac{1}{2c}\int_{ct-x}^{ct+x = x + ct} \psi(s)\; ds
$$

---

The complete solution is given by the pair of formulas $(2)$ and $(3)$ 

**Graphically**: the result can be interpreted as follows. 

1. Draw the <u>backward</u> characteristics from the point $(x,t)$ (assuming we start in $x > 0$ )
   1. $(x,t)$ is in region $x <ct$: it will hit the $t$ axis before it hits the $x$ axis
      1. From formula $(3)$: **reflection induces a change of sign** from $x-ct$ to $ct - x$ 
      2. $v(x,t)$ depends on values of 
         1. $\phi$ at $ct \pm x$ instead of $x \pm ct$ 
         2. $\psi$ in the short interval between $ct - x$ and $ct + x$ 
      3. Note: other values of $\psi$ have been canceled out
      4. **The shaded area $D$ in Figure 2 below is called the DOMAIN OF DEPENDENCE OF THE POINT (x,t)** 
         1. ~~I think it's because "negative" $x$ values cannot be factored into what influences the positive points~~ 
         2. we follow the positive $ct-x$ point from $t = 0$ to positive $t$ until $ct - x = 0$ (left will hit the $t$ axis at $(0, t-x/c)$)
         3. Note: characteristic line from $(x-ct, 0)$ and $(ct- x, 0)$ intersect at $(0, t-\frac{x}{c})$            
         4. ~~Honestly, to analyze this, look at a point $(x', t')$ in this region, then use $t - t' = \frac{x - x'}{c}$~~ 
            1. ~~since $x = ct \equiv t = x/c$~~ 
         5. Note: for certain values of $t$, we depend on $(ct-x, 0)$ when $x-ct$ is still negative
            1. BUT once $ct - x = x - ct = 0$, we start depending on $x-ct$ again

![Screen Shot 2019-02-09 at 11.58.56 PM](Math 126 Lecture 8.assets/Screen Shot 2019-02-09 at 11.58.56 PM.png)

**Homework**: Neumann problem "even reflection"
$$
\begin{cases}
w_{tt}- c^2 w_{xx} = 0 & \text{for }x > 0, -\infin <t < \infin 
\\ w(x,0) = \phi(x) & x > 0
\\ w_t(x,0) = \psi(x) & x > 0
\\ w_x(0, t) = 0 & \text{for } x = 0 \text{ and } -\infin < t < \infin
\end{cases}
$$
My guess:

1. Define $\phi_{\text{even}}$, $\psi_{\text{even}}$ and solve New Initial value problem with those new initial conditions over $-\infin < x < \infin​$ 
2. Use the solution we know for this case 
3. Show that the solution restricted to the half plane $x > 0$ satisfies 
   1. the PDE
   2. The boundary condition *note: use the fact that the derivative of an even function, in this case, $u(x,t)$, is an odd function* 
   3. The initial conditions
4. Write out the explicit formula in terms of $\phi_{\text{even}}$ and $\psi_{\text{even}}$ then for the two cases (regions), use the definition of the even functions to come up with a pair of formulas

Note: 

### [Addition and subtraction][https://en.wikipedia.org/wiki/Even_and_odd_functions#Basic_properties]

- The [sum](https://en.wikipedia.org/wiki/Addition) of two even functions is even.
- The sum of two odd functions is odd.
- The [difference](https://en.wikipedia.org/wiki/Subtraction) between two odd functions is odd.
- The difference between two even functions is even.
- The sum of an even and odd function is neither even nor odd, unless one of the functions is equal to zero over the given [domain](https://en.wikipedia.org/wiki/Domain_of_a_function).

#### The Finite Interval

**The book says you can skip the rest of the section**, and the professor didn't go over this in detail... what he went over instead was this: ![Screen Shot 2019-02-10 at 12.21.54 AM](Math 126 Lecture 8.assets/Screen Shot 2019-02-10 at 12.21.54 AM.png)

"reflected, like billiards"

---

Writing strictly from the book:

* Initial data $\phi(x)$ and $\psi(x)$ are now given only for $0 < x < l$ 
* Extend them to the whole line to be "*odd*" with respect to both $x = 0$ and $x = l$ 

$$
\phi_{\text{ext}}(-x) = -\phi_{\text{ext}}(x) \qquad \text{ and }\qquad \phi_{\text{ext}}(2l-x) = -\phi_{\text{ext}}(x)
$$

so define:
$$
\phi_{\text{ext}}(x)= \begin{cases} 
\phi(x) & \text{for }0 < x <l
\\ -\phi(-x) & \text{for }-l<x<0
\\ \text{extended to be of period 2l}
\end{cases}
$$
Period 2l: $\phi_{\text{ext}}(x + 2l) = \phi_{\text{ext}}(x)$ for all $x$ 

so for example: if $x + 2l < 0$ $\implies$ $\phi_{\text{ext}}(x+2l) = - \phi_{\text{ext}}(-x - 2l) = - \phi_{\text{ext}}(-x-2l +2l) = -\phi_{\text{ext}}(-x) $

$\phi_{\text{ext}}(2l - x) = \phi_{\text{ext}}(-x) = - \phi_{\text{ext}}(x)$ (if $x > 0$)

We do exactly the same thing for $\psi(x)$ (defined for $x \in (0, l)$)to get $\psi_{\text{ext}}(x)$ defined for the whole $x$-axis

So same sort of reasoning:

* We have an infinite line problem with the extended data, which we know the solution to, which we'll call $u(x,t)$ 
* We let $v$ be the restriction of $u$ to $(0,l)$

$$
v(x,t) = \frac{1}{2}[\phi_{\text{ext}}(x+ct) + \frac{1}{2}\phi_{\text{ext}}(x-ct)] + \frac{1}{2c}\int_{x-ct}^{x+ct}\psi_{\text{ext}}(s)\; ds
$$

for $0 \leq x \leq l$ 

**To see it explicitly**: must unwind the definitions of $\phi_{\text{ext}}$ and $\psi_{\text{ext}}$ 

**complicated**: includes a precise description of **all** reflections of the wave at both the boundary points $x = 0$ and $x= l$ ![Screen Shot 2019-02-10 at 12.34.41 AM](Math 126 Lecture 8.assets/Screen Shot 2019-02-10 at 12.34.41 AM.png)

Trying to understand *Figure 5*:

If $x \in (ml, (m+1)l)$ and $m$ is even, then $\phi_{\text{ext}}(x)= \phi(x - ml)$ 

if $m$ is odd, then $\phi_{\text{ext}}(x) = -\phi((m+1)l - x)$ 

When trying to understand *Figure 4* in terms of what intervals meet in reflections:

if the line has $x$ intercept in $(ml, (m+1)l)$

* cross $x = 0$: $(-(m+1)l, -ml)$ 
* cross $x = l$: $(l - ml, 2l- ml)$ 

**Tackling the specific example in the book**: 

From figure 4: 

$\phi_{\text{ext}}(x+ct) = -\phi(4l - x- ct)$ (3 reflections, odd)

$\phi_{\text{ext}}(x-ct) = \phi(x-ct+2l)$ (2 reflections, even)

so: 
$$
v(x,t) = \frac{1}{2}\phi(x-ct + 2l) - \frac{1}{2}\phi(4l-x-ct) + \frac{1}{2c}[\int_{x-ct}^{-l}\psi(y+2l)dy + \int_{-l}^0 -\psi(-y)dy 
\\ + \int_0^l \psi(y)dy + \int_l^{2l}-\psi(2l - y)dy]
\\ + \int_{2l}^{3l}\psi(y - 2l)dy + \int_{3l}^{x+ct}-\psi(4l - y)\;dy
$$
Notice:
$$
\int_{-l}^0-\psi(-y)dy = \int_{l}^0\psi(y)dy

 \\ \int_{l}^{2l}-\psi(2l-y)dy = \int_{3l}^{2l}\psi(y-2l)dy
 \\ \int_{2l}^{3l}\psi(y-2l)dy = -\int_{2l}^{l}\psi(-y+2l)dy
$$
so it has to be that $\int_a^b f(g_1(x))dx$ $= $ $\int_c^d f(g_2(x))dx$ 

$g_2(c) = g_1(a), g_2(d) = g_1(b)$ I believe. 

so we end up with
$$
v(x,t) = \frac{1}{2}[\phi(x-ct+2l)-\phi(4l-x-ct)]+ \frac{1}{2c}[\int_{x-ct}^{-l}\psi(y+2l)dy + \int_{3l}^{x+ct}-\psi(4l-y)dy]
$$
change variables: $s = y + 2l$ and $s = 4l - y$ 
$$
v(x,t) = \frac{1}{2}[\phi(x-ct+2l)-\phi(4l-x-ct)]+ \frac{1}{2c}[\int_{x-ct+2l}^{l}\psi(s)ds + \int_{l}^{4l-x-ct}\psi(s)ds]
$$
And we end up with
$$
v(x,t) = \frac{1}{2}[\phi(x-ct+2l)-\phi(4l-x-ct)]+ \frac{1}{2c}\int_{x-ct+2l}^{4l-x-ct}\psi(s)\; ds
$$
This formula is only valid for point $(x,t)$ s.t. it has three reflections on one end and two on the other!

The solution formula at any other point $(x,t)​$ is characterized by the number of reflections at each end $(x = 0, l)​$ ![Screen Shot 2019-02-10 at 3.14.04 AM](Math 126 Lecture 8.assets/Screen Shot 2019-02-10 at 3.14.04 AM.png)
# Math 126 Lecture 10

**Based on Book, 3.4-3.5** 
**Theorem 1**: Unique solution to inhomogeneous wave eq. with initial conditions
$$
(3) \qquad u(x,t) = \frac{1}2[\phi(x+ct)+\phi(x-ct)]+\frac{1}{2c}\int_{x-ct}^{x+ct}\psi + \frac{1}{2c}\int\int_{\Delta}f
$$

## Proof of Theorem 1

### Method of Characteristic Coordinates

$\xi = x + ct$, $\eta= x-ct$ 
From Section 2.1
$$
Lu \equiv u_{tt} -c^2u_{xx} = -4c^2u_{\xi\eta} = f(\frac{\xi + \eta}2, \frac{\xi - \eta}{2c})
$$
Notice: we expressed $x, t$ in terms of $\xi$ and $\eta$ in $f(x,t)$ 
![Screen Shot 2019-02-14 at 4.24.47 PM 1](Math 126 Lecture 10.assets/Screen Shot 2019-02-14 at 4.24.47 PM 1.png)
Note about the **Figure 2**:

1. Think of it in terms of $\xi​$ axis and $\eta​$ axis... $\xi = 0​$ on the $\eta​$ axis, which is when $0 = x+ct​$ $\equiv​$ $x = -ct​$, thus the $\eta​$ axis, or the line along which "track" how $\eta​$ increases/decreases,  is $x = -ct​$ 
   1. this means $\xi$ $ = \xi_0$, a constant, $\implies$ $x = -ct + \xi_0$, so $\xi$ is constant along lines of this form
2. Similarly, $\eta = 0$ on the $\xi$ axis, which is when $0 = x-ct$ $\equiv$ $x = ct$, thus the $\xi$ axis, or the line along which we "track" how $\xi$ increases/decreases, is $x = ct$ 
   1. When $\eta = \eta_0$ is constant, it is along some line $x = ct + \eta_0$ 
3. $\xi = \eta$ $\implies$ $x + ct = x-ct$ $\implies$ $ct = -ct$, which only occurs when $t = 0$ i.e., the $x$-axis

Integrate this equation with respect to $\eta​$, leaving $\xi​$ as a constant
Thus: $-4c^2 u_{\xi} = \int^{\eta} f d\eta​$ $\implies​$ $u_{\xi} = -\frac{1}{4c^2} \int^{\eta}f d\eta​$, Then integrate w.r.t. to $\xi​$ 
$$
(5) \qquad u = -\frac{1}{4c^2}\int^\xi \int^\eta f \; d\eta\, d\xi
$$
**Lower limits of integration are arbitrary**: They correspond to constants of integration
**For the sake of understanding**: fix a point $P_0$ w/ coordinates $x_0, t_0$ and
$$
\xi_0 = x_0 +ct_0 \qquad \eta_0 = x_0 - ct_0
$$
We then evaluate $(5)$ at $P_0$ and *make a particular choice of the lower limits* 
$$
(6)\qquad u(P_0) = -\frac{1}{4c^2}\int_{\eta_0}^{\xi_0}\int_{\xi}^{\eta_0} f(\frac{\xi + \eta}{2}, \frac{\xi-\eta}{2c})d\eta d\xi
\\= +\frac{1}{4c^2}\int_{\eta_0}^{\xi_0}\int_{\eta_0}^\xi f(\frac{\xi+\eta}{2}, \frac{\xi - \eta}{2c})d\eta d\xi
$$
Note about $(6)$: 

1. The first integral represented increasing along the $\xi$ axis from $\eta_0$ to $\xi_0$, but this means that $\xi \geq \eta_0$, so in the inner integral, going from $\xi$ to $\eta_0$ represents integrating backwards along the $\eta$ axis
2. The second integral fixes this issue and we end up with integrating as shown in Figure 3
   ![Screen Shot 2019-02-14 at 5.30.39 PM](Math 126 Lecture 10.assets/Screen Shot 2019-02-14 at 5.30.39 PM.png)

"$\eta$ is a variable going along a line segment $\rightarrow$ the base $\eta = \xi$ of the triangle $\Delta$ from the left hand edge of $\eta = \eta_0$"
**while**: $\xi$ runs from the left hand corner to the right-hand edge, or it is running from $\xi = \eta_0$, which starts at the intersection of $\eta = \eta_0$ and $\eta = \xi$ and ends at $\xi = \xi_0$ 

The iterated integral **is not exactly** the double integral over $\Delta$ due to the coordinate axes not being orthogonal, so we want to **switch back to original variables**:
$$
(7) \qquad x = \frac{\xi + \eta}{2}\qquad t = \frac{\xi - \eta}{2c}
$$
A little square in FIgure 4 (showing the $\eta$, $\xi$ space) goes into a parallelogram, and the change in its area is measured by the jacobian determinant $J$ 
![image-20190214174916637](Math 126 Lecture 10.assets/image-20190214174916637.png)
$$
J = |\det \begin{pmatrix}\frac{\partial \xi}{\partial x} & \frac{\partial \xi}{\partial t} \\ \frac{\partial \eta}{\partial x} & \frac{\partial \eta}{\partial t} \end{pmatrix}| = |\det \begin{pmatrix} 1 & c \\ 1 & -c\end{pmatrix}| = |-2c| = 2c
$$
Thus $\frac{\partial \eta \partial \xi}{\partial x \partial t} = J = 2c$ $\implies$ $d\eta d \xi = J dx dt = 2c \; dx \; dt$ 
$$
u(P_0) = \frac{1}{(2c)^2}\int\int_{\Delta} f(x,t) J dx \; dt
$$
**Note the J!**. This is precisely **Theorem 1**. This formula can also be written as
$$
u(x_0, t_0) = \frac{1}{2c} \int_0^t \int_{x-c(t_0 -t)}^{x+c(t_0 - t)}f(x,t)dxdt
$$

### Method Using Green's Theorem

Integrate $f$ over past history triangle $\Delta$
$$
(10) \qquad \int\int_{\Delta} f \; dx\; dt = \int\int_{\Delta}(u_{tt}-c^2u_{xx})dx\; dt
$$
**Green's theorem**: using $P_x = -c^2u_{xx}, Q_t = -u_{tt}$ so $f = u_{tt} -c^2u_{xx}$ $= P_x - Q_t$
$$
\int\int_{\Delta} f dx dt =\int\int_{\Delta}P_x - Q_t dxdt = \int_{\partial \Delta} P dt + Q dx = \int_{\partial \Delta = L_0 + L_1 + L2} -c^2u_x dt - u_t dx
$$
where $L_0, L_1, L_2$ are pictured in Figure 6

![Screen Shot 2019-02-14 at 6.15.50 PM](Math 126 Lecture 10.assets/Screen Shot 2019-02-14 at 6.15.50 PM.png)

We can evaluate each line separately, since the line integral of a sum of curves is the sum of the integrals over each curve:

#### On $L_0$ 

$dt = 0$, and $u_t(x,0) = \psi(x)$ so that
$$
\int_{L_0} = -\int_{x_0-ct_0}^{x+ct_0} u_t(x,0) dx = -\int_{x_0 - ct_0}^{x+ct_0}\psi(x)\; dx
$$

#### On $L_1$ 

I suppose we consider the function $x+ct$, which in this case is $x_0 + ct_0$, so **I think**:
$d(x+ct) = dx  + (cdt) = dx + cdt = \frac{d (x_0)}{dx}dx + c\frac{d(t_0)}{dt}dt = 0dx+c0dt = 0​$ 

i.e., $d(x+ct)$ is $0$ on $x_0 + ct_0$ since $x+ct$ is constant

since $x + ct = x_0 + ct_0$ (so we have the line $x = -c(t-t_0) + x_0$ )

so on $x + ct = x_0 + ct_0$, $d(x + ct) = dx + cdt = 0$, (i.e., the change of $x + ct$) 

Therefore, $dx= - cdt$ $\implies$ $-c^2 u_x dt - u_t dx = cu_x dx+ cu_t dt = c\; du$
$$
\int_{L_1} = \int_{L_1} c\; du \underbrace{=}_{\substack{\text{FTC}} \\ \text{line integrals}} cu(x_0, t_0) - cu(x_0+ct_0, 0) \underbrace{=}_{\text{initial condition}} cu(x_0, t_0) - c\phi(x_0 + ct_0)
$$

on $L_1$, $u$ starts at $u(x_0 + ct_0, 0)$ and ends at $u(x_0, t_0)$ 

#### On $L_2$

$dx - cdt = 0$ $\implies$ $dx = cdt$ so $-c^2u_x dt - u_t dx$ = $-cu_x dx -cu_tdt$ $= -cdu$ 
$$
\int_{L_2} = -c \int_{L_2} du = -c[u(x_0-ct_0, 0) - u(x_0,t_0)] = -c\phi(x_0-ct_0) + cu(x_0, t_0)
$$
Adding the three:
$$
\int\int_{\Delta}f\; dx \, dt = 2cu(x_0, t_0)  - c[\phi(x_0+ct_0)+\phi(x_0-ct_0)] - \int_{x_0ct_0}^{x+ct_0}\psi (x)dx
$$
Thus:
$$
(12)\qquad u(x_0,t_0) = \frac{1}{2c}\int\int_{\Delta}f dx dt + \frac{1}{2}[\phi(x_0+ct_0)+\phi(x_0 - ct_0) ] + \frac{1}{2c}\int_{x-ct_0}^{x+ct_0}\psi(x)dx
$$
Finishing proof.

### Operator Method

Similar to solving diffusion equation with a source

ODE Analog: 
$$
(13) \qquad \frac{d^2 u}{dt^2} + A^2 u(t) = f(t), \quad u(0) = \phi, \quad \frac{du}{dt}(0) = \psi
$$
Think of $A^2$ as a positive constant/positive square matrix

$\psi A^{-1}\sin tA$ 

Solution of (13) is:
$$
(14) \qquad u(t) = S'(t)\phi + S(t)\psi + \int_0^t S(t-s)f(s)ds
$$
where
$$
(15) \qquad S(t) = A^{-1}\sin tA\qquad \text{and}\qquad S'(t) = \cos tA
$$
**To understand formula 14**: $S(t)\psi$ is the solution of problem $(13)$ in the case that $\phi = 0$ and $f = 0$ 

$r^2 + A^2 = 0$, gives ​$r = \pm Ai$, so we are dealing with general solution of the form:

$c_1 \cos(At) + c_2\sin (At)$. Then with initial condition $\phi = 0$:

$c_1\cdot1 + c_2\cdot0 = 0$ $\implies$ $c_1 = 0$, and $Ac_2 \cos(A\cdot 0) = \psi$ $\implies$ $Ac_2 = \psi$, so $c_2 = \psi/A$ 

solution to problem when $f = 0, \phi = 0$ is 

---

Returning to the PDE
$$
(16) \qquad u_{tt} -c^2u_{xx} = f(x,t) \quad u(x,0) = \phi(x) \quad u_t(x,0) = \psi(x)
$$
Basic operator "ought to be given" by the $\psi$ term
$$
(17) \qquad\mathscr{S}(t)\psi = \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(y)dy = v(x,t)
$$
where $v(x,t)$ solves $v_{tt}-c^2v_{xx} = 0$, $v(x,0) = 0$, $x_t(x,0) = \psi(x)$ 

$\mathscr{S}(t)$ is the **source operator** and from $(14)$ we should expect the $\phi$ term to be $(\partial/\partial t)\mathscr{S}(t)\phi$ 
$$
\frac{\partial}{\partial t}\mathscr{S}(t)\phi = \frac{\partial}{\partial t}\frac{1}{2c}\int_{x-ct}^{x+ct}\phi(y)dy = \frac{1}{2c}[c\phi(x+ct)-(-c)\phi(x-ct)]
$$
which is in agreement with our old formula.

We now take the $f​$ term; that is $\phi = \psi = 0​$ By analogy with the last term in $(14)​$ it would be

$\int_0^t \mathscr{S}(t-s)f(s)ds = \int_0^t \frac{1}{2c}\int_{x-c(t-s)}^{x+c(t-s)}f(y, s)dyds = \frac{1}{2c}\int_0^t\int_{x-c(t-s)}^{x+c(t-s)}f(y,s)dyds$, which is the same result

#### Takeaway of the Operator Method

If you can solve the homogeneous equation, you can also solve the inhomogeneous equation... **Duhamel's principle** 

## Source On A Half-Line

$$
(18) \qquad \begin{cases}\text{DE:} \quad v_{tt}-c^2v_{xx} = f(x,t) \quad \text{in }0<x<\infin
\\ \text{IC:}\quad v(x,0) = \phi(x) \quad v_t(x,0) = \psi(x)
\\ \text{BC:}\quad v(0,t) = h(t) \end{cases}
$$

The solution to $(18)$ is the sum of four terms, one for each data function $\phi, \psi, f$ and $h$ 

For $x > ct> 0$, the solution has prcisely the same form as in $(3)$ with backward triangle $\Delta$ as the domain of dependence

For $0<x<ct$ the solution is
$$
v(x,t) = \phi \text{ term} + \psi \text{ term} + h(t-\frac{x}{c})+ \frac{1}{2c}\int\int _D f
$$
where $t - x/c$ is the reflection point and $D$ is the shaded region in Figure 3.2.2

**Caveat**: the given conditions had better coincide at the origin: $\phi(0) = h(0) = \psi(0) = h'(0)$ 

If note assumed, there would be a singularity on the characteristic line emanating from the corner

Derive the boundary term $h(t-x/c)$ for $ x<ct$. Assume $\phi = \psi = f = 0$ 

Starting from scratch: $v(x,t)$ must take the form $j(x+ct) + g(x-ct)$. 

From the initial conditions $\phi = \psi = 0$, we have $j(s) = g(s) = 0$ for $s> 0$. **(why?)**

From boundary condition we have $h(t) = v(0,t) = g(-ct)$ for $t> 0$ 

So $g(s) = h(-s/c)$ for $ s< 0​$ 

$\therefore$, if $x<ct$, $t> 0$, we have $v(x,t) = 0 + h(-[x-ct]/c) = h(t-x/c)$ 

### Finite Interval![Screen Shot 2019-02-14 at 7.25.41 PM](Math 126 Lecture 10.assets/Screen Shot 2019-02-14 at 7.25.41 PM.png)

## Diffusion Revisited

(3.5)

The solution formula for the diffusion equation is an example of a **convolution**, the convolution of $\phi$ with $S$ 
$$
(1)\qquad u(x,t) = \int_{-\infin}^\infin S(x-y, t)\phi(y)dy = \int_{-\infin}^\infin S(z,t)\phi(x-z)dz
$$
Where $S(z,t) = 1/\sqrt{4\pi k t}e^{-z^2/4kt}$, introduce $p = z/\sqrt{kt}$ to obtain:
$$
(2) \qquad u(x,t) = \frac{1}{\sqrt{4\pi}} \int_{-\infin}^\infin e^{-p^2/4}\phi(x-p\sqrt{kt})dp
$$

### Theorem 1

Let $\phi(x)$ be a bounded continuous function for $-\infin<x<\infin$. Then the formula (2) defines an infinitely differentiable function $u(x,t)$ for $-\infin<x<\infin$, $0<t<\infin$, which satisfies the equatino $u_t = ku_{xx}$ and $\lim_{t \downarrow 0} u(x,t) = \phi(x)$ for each $x$ 

#### Proof:

The integral converges easily because
$$
|u(x,t)| \leq \frac{1}{\sqrt{4\pi}}(\max |\phi|) \int_{-\infin}^\infin e^{-p^2/4}dp = \max|\phi|
$$
(This inequality is related to the **maximum principle**) 

Thus the integral converges uniformly and absolutely

**show $\frac{\partial u}{\partial x}$ exists**: 

it equals: $\int (\partial S/\partial x)(x-y, t)\phi(y)\; dy$ provided that this new integral also converges absolutely. Now:
$$
\int_{-\infin}^\infin \frac{\partial S}{\partial x}(x-y, t)\phi(y) = -\frac{1}{\sqrt{4\pi kt}}\int_{-\infin}^\infin \frac{x-y}{2kt} e^{-(x-y)^2/4kt}\phi(y)dy
\\ = \frac{c}{\sqrt{t}}\int_{-\infin}^\infin pe^{-p^2/4} \phi(x-p\sqrt{kt})dp 
\\ \leq \frac{c}{\sqrt{t}}(\max |\phi|)\int_{-\infin}^\infin |p| e^{-p^2/4}dp
$$
$c$ is simply a constant, and $p = x - y/\sqrt{kt}$. $The last integral is finite $\implies$ this integral converges uniformly and absolutely

Therefore: $u_x = \frac{\partial u}{\partial x}$ exists and is given by this formula

All derivatives of all orders work the same way, because each diffrentiation brings down a power of $p$ so we end up with convergent integrals like $\int p^n e^{-p^2/4}$ $\implies$ $u(x,t)$ is differentiable to all orders

Since $S(x,t)$ satisfies the diffusion equation for $t> 0$, so does $u(x,t)$ 

**Prove initial condition**: 

Must use limits, since the formula has meaning only for $t > 0$

Because $\int S = 1$ we have $\phi(x) \int_{-\infin}^\infin S(x-y, t)dy = \phi(x)$ 
$$
u(x,t) - \phi(x) = \int_{-\infin}^\infin S(x-y, t)[\phi(y)-\phi(x)]\; dy 
\\ = \frac{1}{\sqrt{4\pi}}\int_{-\infin}^\infin e^{-p^2/4}[\phi(x-p\sqrt{kt}) -\phi(x))]dp
$$
For fixed $x​$, we must show that this tends to zero as $t \rightarrow 0​$ 

**Idea**: For $p\sqrt{t}​$ small, the **continuity of $\phi​$** makes the integral small, while for $p \sqrt{t}​$ not small, $p​$ is large and the **exponential factor** is small

Let $\epsilon > 0​$, Let $\delta > 0​$ be so small that

$\max_{|y-x|\leq \delta}|\phi(y)-\phi(x)| <\frac{\epsilon}{2}$ (possible due to continuity of $\phi$)

**Break up the integral into the part where $|p| < \delta/\sqrt{kt}$ and the part where $|p| \geq \delta/\sqrt{kt}$ ** 

The first part is, since we have $y = x-p\sqrt{kt} < x \pm \delta$
$$
|\int_{|p| < \delta/\sqrt{kt}}| \leq (\frac{1}{\sqrt{4\pi}}\int e^{-p^2/4} dp) \cdot \max_{|y-x|\leq \delta}|\phi(y)-\phi(x)| < 1 \cdot \frac{\epsilon}{2}= \frac{\epsilon}2
$$
Second part is (using exponential factor)
$$
|\int_{|p| \geq \delta/\sqrt{kt}}|\leq \frac{1}{\sqrt{4\pi}}\cdot 2(\max|\phi|)\cdot \int_{|p| \geq \delta/\sqrt{kt}}e^{-p^2/4}dp< \frac{\epsilon}2
$$
By choosing $t$ sufficently small, since the integral $\int_{-\infin}^\infin e^{-p^2/4}dp$ converges and $\delta$ is fixed (i.e., if $t$ is small, $N = \delta/\sqrt{kt}$ is large enough)

Therefore:
$$
|u(x,t)-\phi(x)| <\frac{1}{2}\epsilon + \frac{1}{2}\epsilon = \epsilon
$$
provided $t$ is small enough. 

$\implies$ $u(x,t) \rightarrow \phi(x)$ as $t \rightarrow 0$ 

#### Corollary. 

The solution has all derivatives of all orders for $t> 0$, **even if $\phi$ is not differentiable**. We can say therefore that all solutions become smooth as soon as diffusion takes effect. There are no singularities, in sharp contrast to the wave equation. 

**Proof**: Use formula $(1)$ 

$u(x,t) = \int_{-\infin}^\infin S(x-y, t)\phi(y)dy$ together with rule for differentiation under an integral sign, Theorem 2 in Section A.3.

**Check this~!** 

I believe from the proof of **Theorem 1**, we showed that the partial derivative exists without needing $\phi$ to be differentiable

### Piecewise Continuous Initial Data

The continuity of $\phi(x)$ was used in only one part of the proof. WIth an appropriate change, we can allow $\phi(x)$ to have a jump discontinuity

(Consider: the initial data for $Q(x,t)$)

**A Function has a jump** at $x_0$: if both the limit of $\phi(x)$ as $x \rightarrow x_0$ from the right exists and the limit from the left exists, **but** these two limits are not equal. 

**A function is piecewise continuous** if in each finite interval it has only a finite number of jumps and is continuous at all other points (See Section 5.2)

### Theorem 2

Let $\phi(x)$ be a bounded function that is piecewise continuous. Then (1) is an infinitely differentiable solution for $t> 0$ and
$$
\lim_{t \downarrow 0} u(x,t) = \frac{1}{2}[\phi(x+) + \phi(x-)]
$$
for all $x$. At every point of continuity, this limit equals $\phi(x)$ 

#### Proof.

The idea is the same as before. The only difference is to split the integrals into $p>0$ and $p<0$. We need to show that
$$
\frac{1}{\sqrt{4\pi}}\int_0^{\pm \infin}e^{-p^2/4}\phi(x+\sqrt{kt}p)dp \rightarrow \pm \frac{1}{2}\phi(x\pm)
$$
**Details are left as an exercise** 

$\sqrt{kt}p$ and $p = +0$ or $p = -0$ 
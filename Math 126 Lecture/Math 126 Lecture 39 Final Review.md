# Math 126 Lecture 39: Final Review

Modern Math Workshop 2019. Deadline July 8

Next week's office hours:

* Usual hours + during lecture times 

## Types of PDEs and useful methods

### First Order

##### Linear Transport Equation, e.g. $a(x, y)u_x + b(x, y)u_y = 0$ 

or more generally: 
$$
\sum_{i = 1}^ d a^i(\bold x) u_{x_i} = 0
$$

###### Method of Study: Characteristics 

* drawing pictures
* show is constant along characteristics

##### Conservation laws

e.g. $u_t + uu_x = 0​$ $\leftarrow​$ Burger's equation

$uu_x = \frac{1}{2}(u^2)_x$ 

More generally: 
$$
u_t + (A(u))_x = 0
$$
in arbitrary dimensions 
$$
\nabla \cdot (\bold A(u)) = \sum_{i=1}^d (A^i(u))_{x_i} = 0
$$


###### Method of Study: 

* Characteristics

* Shocks
* Weak Solutions

Much more difficult: Systems of conservation laws. 

Why are these so difficult?… E.g.: 

* Euler's equation… viscosity… viscous fluids 

##### Hamiltonian - Jacobi Equation 

$$
u_t + H(\nabla u) = 0
$$

$H$: Some nonlinear function of the gradient of $u$ 

###### Method:

Maximum Principle

### Second Order

##### LaPlace's Equation

$$
\Delta u = 0
$$

more generally:

**elliptic**: $\sum_{i, j = 1}^d a^{ij}(\bold x) u_{x_i x_j} = 0$ 

where $A = (a^{ij}) $ is a symmetric positive definite matrix

###### Method

* Fourier

* Dirichlet's Principle

* Energy Method
* max principle
* Separation of variables
* ...
* Mean Value Principle is related

##### Diffusion equation

$$
u_t = \Delta u
$$

In general: parabolic equation $u_t = \sum_{i, j}^d a^{ij}(\bold x) u_{x_i x_j}$ 

###### Method of study: 

* Fourier 
* Laplace Transform
* max principle 
* energy method
* separation of variables

##### Wave Equation

$$
u_{tt} = \Delta u
$$

###### Methods

* energy method
  * preserved in this case
* Fourier
* Separation of variables

In general: hyperbolic equation $u_{tt} = \sum_{i, j = 1}^d a^{ij}(\bold x)u_{x_i x_j}$ 

* note to self: look at section 1.6

##### Schrödinger's equation (9.4)

$$
i h u_t + \frac{h^2}{2m}\Delta u + V(\bold x)u = 0
$$

###### Method

* Fourier
* Energy Method

##### Nonlinear elliptic 

$$
\Delta u = \phi(u)
$$

Semilinear poisson's eq. 

###### Methods

* calculus of variations
* energy method

$$
\text{Divergence form } \quad \nabla \cdot (\bold F(\bold x, u, \nabla u)) = 0
$$

###### Methods

* Weak solutions
* Calculus of variations

e.g. Minimal surface equation 
$$
\nabla \cdot ( \frac{\nabla u}{\sqrt{1 + |\nabla u|^2}}) = 0
$$
Mounge- Aupise equation (?)
$$
\det \begin{pmatrix}u_{x_1 x_1} &\cdots& u_{x_1 x_d}\\ u_{x_2, x_1} & \cdots &\\ u_{x_d, x_1} & \ddots & u_{x_d x_d} \end{pmatrix} = f
$$

###### Methods

* Optimal Transport
* Measure (?) theory
* probability theory
* Hard analytic ???

##### Nonlinear Parabolic equations

Semilinear
$$
u_t = \Delta u = \phi(u)
$$

###### Methods

* Energy Method
* Gradient flows

Poron's modium equation
$$
u_t = \Delta (u^m)
$$
Mean Curvature Flow
$$
V = H
$$
Geometric heat equation in some sense

$\partial_t g = \text{Ric }g$???

##### Nonlinear Wave Eq.

$$
u_{tt} - \Delta u = \phi(u)
$$

###### Methods

* Energy Method
* Fourier

### Higher Order Equations

##### KdV

$$
u_t + 6uu_x + u_{xxx} = 0
$$

3rd order equation, traveling wave solutions: Solitons

##### Caln-Hilliard Equation

$$
u_t + (u_{xx}- W'(u))_{xx} = 0
$$

$$
\int (\frac{1}{2}u + W(u))\, dx
$$

4th order

###### Methods

* Gradient Flow
* pattern (?) function

##### Mullus(?) - Seba?? // Mullins-Sekerka

No clue how to read this one "very famous equation"

Ice melting…?

$\Delta u = 0$ inside, $\Delta u = 0$ outside but on boundary:
$$
V = [\frac{\partial u}{\partial n ?}]
\\ H = u
$$
NO MAX PRINCIPLE

Gradient Flows

### Lessons From This Course

1. Two Broad Approaches to PDEs
   1. Exact formulas
   2. deducing properties of solutions
      1. Qualitative statements
         1. existence, uniqueness, convergence 
      2. Quantitative
         1. looks like a function
2. The most powerful Method are relatively simple
   1. Energy Method
   2. Maximum Principle
   3. Calculation of Variations
      1. How do we do this in more general senses, not just from examples or given integrals...
      2. How to take a PDE then use calculus of variations
      3. https://en.wikipedia.org/wiki/Functional_(mathematics)
      4. https://en.wikipedia.org/wiki/Calculus_of_variations
   4. Separation of variables
   5. Special solutions (e.g., traveling ways, symmetric solutions) $\leftarrow $ Ansatz
3. Often we should look for weak solutions
   1. what are weak solutions
4. Numerical Methods are powerful
5. PDEs are extremely useful for modeling
   1. ocean waves to coffee mug

Further courses:

* Modern Math workshop
* Math 203: Asymptotic methods, applied PDEs
* Math 222 B - Theory of PDEs
* Math 224 - applied PDEs
* Math 228 - Numerical solutions of PDEs


# Math 124 Lecture 6

Plotting: "pick up as you go"

## Uniformly distributed random numbers

`rand(n)`: generates $n$ *pseudo-random* numbers from a uniform distribution on $[0, 1]$. (creates an $n$ element array of Float64s)

**sometimes** it is useful to be able to generate the same sequence of random numbers (e.g. for debugging code)

This can be done using the Random.seed! Command from the Random package, and an arbitrary so-called *seed* number

## Other intervals

to generate uniform random numbers from a different interval $[a,b]$, you can simply scale a random number $x \in [0,1]$ by $a + x(b-a)$ for example:

```julia
y = 2rand(5).- 1	#5 uniform random numbers on [-1, 1]
```



```julia
z = 7rand(3).+ 4 #3 uniform random numbers on [4, 11]
```


$$
0<x<1
\\ 0<(b-a)x<b-a
\\ a<a+(b-a)x<b
$$

## Random Integers

The rand function can also be used with a range or a vector, from which it chooses sampels at random. 

```julia
rand(1:10, 3)
rand([-pi, exp(1), -1.0], 5) #picks 5 random objects from the vector
```

## Visualization using histograms

*histogram*: 

```julia
function count_histogram(nbr_outcomes, sequence)
    count = zeros(nbr_outcomes)
    for x in sequence
        count[x]+=1
    end
    count
end
```

We can now e.g. visualize the probability of getting 1, 2, ..., 6 when rolling a fair die

The function below simulate the rolling of a die $n$ times, by repeatedly...

```julia
using PyPlot

function simulate_die(ntrials)
    outcomes = collect(1:6)
    x = rand(outcomes, ntrials);
    bar(outcomes, count_histogram(6,x)/ntrials)
    xlabel("Die outcome")
    ylabel("Probability")
end

simulate_die(1000)
```

"In Monopoly you roll two dies, then you add them"

As a generalization, we can simulate rolling a die $n$ times and adding all the outcomes:

```julia
function simulate_sum_of_n_dice(ntrials, ndice)
    outcomes = collect(1:6)
    x = zeros(Int64, ntrials)
    for i = 1:ndice
        x .+= rand(outcomes, ntrials)
    end
    outcomesn = collect(1:6ndice)
    bar(outcomesn, count_histogram(6ndice, x)/ntrials)
    xlabel("sum of n dice outcome")
    ylabel("probability")
end
simulate_sum_of_n_dice(1000, 2)
```

The famous central limit theorem states that this distribution approaches a normal distribution as the number of dice rolls increases

## General histogram into bins

The following code histograms 10000 random numbers from the normal distribution by counting the frequencies inside each of 50 equally size bins

```julia
x = randn(10000)
nbins = 50

plt[:hist](x, nbins)
xlabel("Random variable x")
ylabel("Count")

```

## Estiamte pi by throwing darts

Imagine the unit circle inside the square [-1, 1]$\times$[-1,1]

Imagine throwing random "darts" (so random points)

we can check if the points lie inside the circle using $\sqrt{x^2 + y^2}$ 

So specifically:

imagine throwing $n$ darts on the square with equal probability everywhere

Then the fraction of darts that hit inside the circle should approximate the ration between the areas
$$
\frac{\text{hits}}{n} \approx \frac{\pi r^2}{(2r)^2}
$$
where the radius $r = 1$
$$
\pi \approx 4 \frac{\text{hits}}{n}
$$

```julia
n = 10000               # Number of darts
x = 2rand(n) .- 1       # Coordinates
y = 2rand(n) .- 1

plot([-1,1,1,-1,-1], [-1,-1,1,1,-1])  # Draw square
theta = 2π*(0:100)./100               # Draw circle
plot(cos.(theta), sin.(theta))   
plot(x, y, linestyle="None", marker=".", markersize=1)   # Plot dart points
axis("equal")

# Determine if points are inside the circle (a "hit")
hits = 0
for i = 1:n
    if x[i]^2 + y[i]^2 ≤ 1
        hits += 1
    end
end

approx_pi = 4hits / n
```

This is extremeley bad: 3.134 with (10,000 tries!)

"Integrals can be tricky"

Suppose we're given some random function $f(x)$ 

Assume right now $f(x) \geq 0$ 

draw a rectangle of height $\max_x f(x)$ and base (the length of the interval we are integrating over)

do the same as above: random points/darts at the rectangle

We count the hits... can we still use pythagorean theorem? 

Or do we just check if at a point $(x,y)$, $y \leq f(x)$

## Example: Random walk

A *random walk* can be described by the following algorithm:

* Consider a 2d array of squares
* Start at the center square
* At each turn, randomly choose a direction (up/down/left/right)
* Continue until reaching an edge

**When to use for vs while?**

`for`: now the amount of iterations

`while`: I suppose if we know a "goal" we want to achieve, but don't know how long it'll take us to get there

```julia
function random_walk(n)
    x = [0]
    y = [0]
    while abs(x[end]) < n && abs(y[end]) < n
        if rand() < 0.5 #sort of simulating a 1/2 chance of going vertical or horizontal
            if rand() < 0.5  # Up
                push!(x, x[end])
                push!(y, y[end] + 1)
            else             # Down
                push!(x, x[end])
                push!(y, y[end] - 1)
            end
        else
            if rand() < 0.5  # Right
                push!(x, x[end] + 1)
                push!(y, y[end])
            else             # Left    
                push!(x, x[end] - 1)
                push!(y, y[end])
            end
        end
    end
    x,y
end
```

## Monte Carlo simulations

A common technique to estimate probabilities and statistical quantities is *Monte Carlo simulation*.  The computer runs many simulations that depend on random numbers and  record the outcomes. The desired quantity can then be estimated by  simple fractions.

As an example, the code below simulates rolling a die repeatedly  until it rolls a 6, and recording the number of rolls that it took. The  average number of rolls required can then be estimated.

```julia
ntrials = 10000            # Number of experiments
x = Int64[]                # Array of outcomes
sumx = 0
for i = 1:ntrials
    nrolls = 0
    found_a_six = false
    while !found_a_six #has to be a while loop, for we don't know how many times it'll take to get a six
        nrolls += 1
        roll = rand(1:6)
        if roll == 6 #we've found a six
            found_a_six = true #we can quit the while loop
        end
    end
    push!(x, nrolls)
    sumx += nrolls
end

plt[:hist](x, 50);
xlabel("Number of rolls")
ylabel("Count")
average = sumx / ntrials
```

## Card games, permutations

To simulate card games, we need a way to represent the cards. There are 13 *ranks* and 4 *suits*,
so we could simply use an integer between 1 and 52 and let 1 to 13 
represent Ace, 2, 3, ..., King of clubs, 14 to 26 all the diamonds, etc.
We can use integer division and remainder to find the rank and suit:

`randperm(n)`: generates a random permutation of the integers 1 up to `n` 

### Example: Probability of flush

*flush*: all cards have the same suit


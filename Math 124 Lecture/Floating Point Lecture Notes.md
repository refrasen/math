$(0, 1)$: a lot

$2^{23}-1$ numbers in between $2^{e}$ and $2^{e+1}$ , when $0 \leq e \leq e_{max}$

i.e., between 1 and 2, 2 and 4, 4 and 8, ...



$\text{fl}(x) \leq |x| + \epsilon_{\text{machine}}|x|​$  
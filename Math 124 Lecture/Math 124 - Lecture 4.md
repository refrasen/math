# Math 124 - Lecture 4

## Announcements

* Quiz on Friday: pen and paper, if you did the homework, it should be fine. 

  * based on HW1 and HW2

* Problem 4(b): Newton's Method for solving $f(x) = x - \cos x = 0$ is the following iteration
  $$
  x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)}
  $$
  

Note: may be based on idea that we can approximate where the points on the curve are using the tangent line. (Linear Approximation)
$$
x_{n+1} = x_n - \frac{x_n^2 - a}{2x_n}
\\ = \frac{x_n^2+a}{2x_n}
\\ = \frac{1}{2}(x_n + \frac{a}{x_n})
$$
Note: kosher to do comment: 

```
#Need to initialize to get started
```

## Introduction to 1-d arrays

list of number:
$$
[3, 4, -1, 6]
$$
"What's the first index?" For Julia, 1-based indexing. 

"0 based indexers don't even talk to 1 base indexers"

"Big conflict, people have a lot of opinions"

**Why do we even want this?**

Otherwise, you'd have to do this

```
x1 = 3
x2 = 4
x3 = -1
x4 = 6
```

When does the above fail?

* when you want to iterate through the list/array
* very large numbers/ want small code with a lot of data
* not sustainable...

An array is not necessarily a vector... certainly looks a lot like a vector. 

---

Arrays can be created using built-in-functions: zeros(T, n), ones(T, n), etc. (see slides)

Push! function cannot add a float to an array of Int64s. 

### Accessing Elemnts of an array

```julia
y[3] #Acess the 3rd element in the array y
```

```julia
z[1] = z[end]*z[end-1] # Set the 1st element in z to the product of the last two
```

### Traversing an array

Use the `for`-loop **or** use the `in` keyword

#### Example: Sieve of Eratosthenes

$n = 20$ ...

uses the term "mark"...?

Refinement: can start from $p^2$ because we've already seen: $2p, 3p,  . . ., (p-1)p$ when marking 1 through $p-1$ 

... Code in Slides/Julia file

### The dot-syntax: Vectorized operators and functions

Any function `f` can be applied elementwise to an array by using the defined function`f.`

Examples: ?length(x) = length(y)?:  `x.*y` , `sqrt.(x)`, `x + sqrt.(y.^x)` 

also: `@.` syntax makes all oeprators in the expression following it be applied elementwise:

```julia
@. x + sqrt(x^y)
```

**When using the dot-operator on binary operations, the two arrays must have the same number of elements** 

However: if one of the arguments to the oeprator is a scalar $\implies$ it is interpreted as a constant vector of the correct size...

```julia
x.*-3
```

(will multilply each element of `x` by -3)

Also the following works:

```julia
-3x
```

**Dot-syntax is automatically defined even for new functions** 

if the function takes $n$ arguments and we place in $n$ vectors of equal size $k$ 

the function will return a vector of size $k$ 

### Array slices

Slicing an array means extracting a subset of the original array

`x[starting_index:stepsize:stopping_index]`

### Multi-dimensional array

```julia
A = [1 2 3; -4 -5 -6] #this is a 2x3 Array{Int64, 2}
```

Kind of stopped here.
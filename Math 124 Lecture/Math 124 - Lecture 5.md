# Math 124 - Lecture 5

**Note**: Project 1 is due Fri 2/22

## Arrays (cont.)

### Review

###  Array Slices

```julia
println(y)               # Original vector
println(y[1:3])          # First 3 elements
println(y[1:2:4])        # All odd-numbered elements
println(y[end:-1:2])     # From end back to second element in reverse
println(y[4:3])          # Empty subset
println(y[:])            # All elements (same as original vector)
println(y[[4,2,4,3,3]])  # Index by vector - pick elements 4,2,4,3,3

```

#### Ranges

Ranges can be used to define the array itself. For example:

`x=1:8`

will define an object that can be used as an array. 

**The matrix can be explicitly created using the** `collect` function

```julia
x1 = collect(1:3) #creates a 3-element Array{Int64, 1} [1, 2, 3]
```

## Arrays are passed by sharing

```julia
x = [1, -2, 3]
y = x
y[2] = 123
println("y = ", y)
println("x = ", x)
```

this will return 

```
y = [1, 123, 3]
x = [1, 123, 3]
```

* y is like an alias to x
* *shared reference* 

#### If you really want a copy, must use `copy` function

#### The situation is the same when passing arrays to function

```julia
function modify_scalar(x)
    x = 111
    return nothing
end

function modify_vector!(x)
    x[:] .= 111
    return nothing
end

x = 0
modify_scalar(x)
println("x = ", x)    # Still 0 - function does not modify a scalar

x = zeros(1,5)
modify_vector!(x)
println("x = ", x)    # Function modifies the original vector
```

### Multi-Dimensional Arrays

* Contructed by using 
  * spaces between the number in each row
  * Semi-colons between the rows

```julia
A = [1 2 3; -4 -5 -6]
```

This returns a 2 x 3 Array of integers... dimension 2

### Multi-dimensional arrays can be operated on using similar syntax as for 1-d arrays

`length` function on a multi-dim array returns the **total** number of elements in array

so if array is $m\times n$, its length is $mn$ 

`size(A, i)` gives the dimension, where A is the array, and i notes the dimension 

so if we have $A$, a $m_1 \times m_2 \times ... \times m_n$ array (if this is possible?)

`size(A, i)` will return $m_i$ 

the `size` function is helpful in traversing the elements in an array using a for loop. 

```julia
sumA = 0
for i = 1:size(A,1)
    for j = 1:size(A,2)
        sumA += A[i,j]
    end
end

println("Sum of the elements in A = ", sumA)
```

^ the above is an example of a **nested for-loop** 

For example, if the size of `A` is 2-by-3, the nested loops will produce the values (i,j) = (1,1), (1,2), (1,3), (2,1), (2,2), (2,3)

`in` syntax also works on multi-dimensional arrays, if indices are not need (so we don't care too much about the order of elements?)

**Note**: it will go down each column from left to right so

(i,j) = (1, 1), (2, 1), (1, 2), (2, 2), (1, 3), (2, 3)

### Multi-dimensional array creation

| Function             | Description                                       |
| -------------------- | ------------------------------------------------- |
| `zeros(T, dims...)`  | an array of size dims of all zeros                |
| `ones(T, dims...)`   | an array of size dims of all ones                 |
| `trues(n, dims...)`  | a `BitArray` of size dims with all values `true`  |
| `falses(n, dims...)` | a `BitArray` of size dims with all values `false` |

`dims...` means a comma-seperated list 

**Note**: Julia distinguishes between 1-d arrays, 2-d column vectors, 2-d row vectors

**row vector**: 1 row, multiple columns `(1, n)`

**column vector**: 1 column, multiple rows `(n, 1)`

### Concatenation, using square bracket [] syntax

```julia
D = [A; C] #vertical, places A on top of C? so must have same number of columns
E = [zeros(5) C] #horizontal, places C to the right of zeros(5), must have same number of rows
F = [A ones(2,2); zeros(1,5)] #places ones(2,2) to the right of A,  then places zeros(1,5) underneath
```

let A have $m\times n$ dimension, $B$ has $m \times p$ , $C$ has $p \times n$ 

Vertical: `[A; C]` will produce a $(m+p) \times n$ matrix

Horizontal: `[A B] ` will produce a $m \times (n+p)$ matrix

### Dot-syntax and slices for multi-dimensional arrays

The dot-syntax still works element wise on multi-dim arrays just like with one-dim arrays

Slices work as before, but now you can provide a subset of indices for each array dimension

## Plotting

### PyPlot Package

`using` command

### Basic Plotting

`plot(x,y)`: by default draws a straight line between the $x, y$- coordinates in the vectors `x` and `y` 

`axis("equal")`: will equally scale...

`grid(true)`: includes grid-lines in the plot

`xlabel, ylabel`, and `title`: include strings of text in the plot, enclosed by `""`

### Plotting Functions

plot functions of the form $y = f(x)$. 

Example: $y = \frac{1}x$, $y = \sin(\frac{1}{x})$ 

#### New Features

* `linewidth`,  `linestyle`, and `color`: can be used to control the appearance of the lines
* The `legend` functino creates a box with identifying text for each line

```julia
f(x) = sin(3x) + 0.5sin(7x) - 0.1x
x = range(0, stop=10, length=200)      # Create x-vector, 200 points should make a good plot

plot(x, f.(x))
plot(x, f.(x .- 3), linewidth=3)
plot(x, (@. -0.7f(100/(10 + x^2))), color="m", linestyle="--")
legend(("f(x)", "f(x-3)", "-0.7f(x^2)"))
grid(true)
```

![image-20190207152600083](Math 124 - Lecture 5.assets/image-20190207152600083.png)

### Plotting Parametric Curves

Example: 
$$
\begin{cases}x = r\cos(10\pi s) \\ y = r\sin(10\pi s)\end{cases} 
$$
with $r = 0.1 + s + 0.1\sin(50\pi s)$, for $s \in [0,1]$ 
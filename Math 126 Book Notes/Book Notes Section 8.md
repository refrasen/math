# Book Notes Section 8

## 8.1 Opportunities and Dangers

**finite differences**: method that consists of replacing each derivative by a difference quotient. 

### Functions of One variable

Let $u(x)$ be a function of one variable

**mesh size**: $\Delta x$ 

approximate value $u(j\Delta x)$ for $x = j \Delta x$ by a number $u_j$ indexed by an integer $j$: 
$$
u_j \sim u(j \Delta x)
$$
**Three standard Approximations** for the **first derivative** $\frac{\partial u}{\partial x}(j \Delta x)$ 
$$
\begin{align} 
(1) & \quad  \text{The backward difference: } &\frac{u_j - u_{j-1}}{\Delta x}
\\(2)& \quad \text{The forward difference: }&\frac{u_{j+1}-u_j}{\Delta x}
\\ (3)& \quad\text{The centered difference: }&\frac{u_{j+1}-u_{j-1}}{2\Delta x}
\end{align}
$$
Each is correct approximation because of **Taylor expansion**: 

$u(x + \Delta x) = u(x) + u'(x)\Delta x + \frac{1}{2}u''(x)(\Delta x)^2 + \frac{1}{6}u'''(x)(\Delta x)^3 + O(\Delta x)^4$ $u(x - \Delta x) = u(x) - u'(x)\Delta x + \frac{1}{2}u''(x)(\Delta x)^2 - \frac{1}{6}u'''(x)(\Delta x)^3 + O(\Delta x)^4$  

**Deduce:** 
$$
\begin{align} u'(x) &= \frac{u(x) - u(x-\Delta x)}{\Delta x} + O(\Delta x)
\\ &= \frac{u(x+\Delta x) - u(x)}{\Delta x} + O(\Delta x)
\\ &= \frac{u(x + \Delta x)- u(x - \Delta x)}{2\Delta x} + O(\Delta x)^2
\end{align}
$$
$O(\Delta x)$ means any expression that is bounded by a constant times $\Delta x$ and so on

if we replace $x$ with $j \Delta x$, we have $(1)$ and $(2)$ are correct approximations to the order $O(\Delta x)$ and $(3)$ correct to the order $O(\Delta x)^2$ 

---

**Second Derivative** approximation:
$$
(4) \qquad \text{centered second difference: }u''(j\Delta x)~ \frac{u_{j+1} - 2u_j + u_{j-1}}{(\Delta x)^2}
$$
justified by the same two taylor expansions given above (added to get rid of $u'(x)$ term): easy to see that $(4)$ is valid with an error of $O(\Delta x)^2$ 

---

### Functions of Two variables

$$
u(j\Delta x, n \Delta t) \sim u_j^n
$$

where $n$ is a **superscript** *not* a power
$$
(5) \qquad \frac{\partial u}{\partial t}(j \Delta x, n \Delta t) \sim \frac{u_j^{n+1}- u_j^n}{\Delta t}
$$
(5) is the forward difference for $\partial u/\partial t$  and similalry for $\partial u/\partial x$: 
$$
(6) \qquad \frac{\partial u}{\partial x}(j \Delta x, n \Delta t)\sim \frac{u_{j+1}^n - u_j^n}{\Delta x}
$$
we can write similar expressions for $(1)-(4)$ in either  $t$ or $x$ 

---

### Two Kinds of Errors

**Truncation error**: error introduced in the solutions by the approximations themselves, that is, the $O(\Delta x)$ terms

This error is a complicated combination of many small errors

We want the truncation error to tend to zero as the mesh size tends to zero. 

**local truncation errors**: The errors writtin in $(1)-(4)$. they occur in the approximation of the individual terms in a differential equation

**Global truncation error**: error introduced in the actual solutions of the equatino by the *cumulative effects* of the *local truncation errors* 

**Roundoff error**: occur in a real computation because only a certain number of digits are retained by the cmoputer at each step of the computation 

#### Example 1

$$
u_{t} = u_{xx} \qquad u(x, 0) = \phi(x)
$$

Solve this using finite differences. Using forward difference for $u_t​$ and a centered difference for $u_{xx}​$ 

**Difference equation**: 
$$
(7) \qquad \frac{u_j^{n+1} - u_j^n}{\Delta t} = \frac{u_{j+1}^n - 2u_j^n + u_{j-1}^n}{(\Delta x)^2}
$$
local truncation error of $O(\Delta t)$ left side, $O(\Delta x)^2$ right side

**Suppose**: choose small value for $\Delta x$ and choose $\Delta t = (\Delta x)^2$ 

Then $(7)$ simplifies to: 
$$
(8) \qquad u_j^{n+1} = u_{j+1}^n  - u_j^n + u_{j-1}^n
$$
Let's take $\phi(x)$ to be the very **simple step function** which is to be approximated by the values $\phi_j$: 

$0 \quad 0 \quad 0 \quad 0 \quad 1 \quad 0 \quad 0 \quad 0 \quad 0 \quad 0 \rightarrow x$

**Marching in Time**: 

$\phi(x)$ provides $u_j^0$ then $(8)$ gives $u_j^1$ then $(8)$ gives $u_j^2$ and so on.. 

We can summarize $(8)$ schematically using the diagram (**template**)
$$
\bullet +1\quad \bullet - 1 \quad \bullet +1
$$
This means that in order to get $u_j^{n+1}$ you add or subtract its three lower neighbors as indicated

## 8.2 Approximations of Diffusions

In the previous example, the problem was that the little errors accumulated. What was wrong was the choice of the mesh $\Delta t$ relative to the mesh $\Delta x$ 

Let
$$
(1) \qquad s = \frac{\Delta t}{(\Delta x)^2}
$$
As before we can solve the scheme $(8.1.7)$ for $u_j^{n+1}$ 
$$
(2) \qquad u_j^{n+1} = s(u_{j+1}^n + u_{j-1}^n) + (1- 2s)u_j^n
$$
This scheme is **explicit** because the values at $(n+1)$st time step are given explicitly in terms of the values at the earlier times

#### Example 1

Standard Problem: **Diffusion Equation** 
$$
\begin{align}
u_t &= u_{xx} &&\text{for }0<x<\pi, t>0
\\ u &= 0 &&\text{at }x = 0, \pi
\end{align}
\\ u(x, 0) = \phi(x) = \begin{cases} x &\text{in } (0, \frac{\pi}{2}) \\ \pi - x & \text{ in }( \frac{\pi}{2}, \pi) \end{cases}
$$
From Section 5.1, the exact solution is:
$$
u(x, t) = \sum_{k=1}^\infin b_k \sin kx \; e^{-k^2 t}
$$
where $b_k = 4(-1)^{(k+1)/2}/\pi k^2$ for odd $k$ and $b_k = 0$ for even $k$ 

(Look at figure 1 to see what this looks like)

Approximate this problem by the sceme $(2)$ for $j = 0, 1, …, J-1$ and $n = 0, 1, 2, \ldots$ together with **discrete boundary** and **initial conditions**
$$
u_0^n = u_J^n = 0 \quad \text{ and } \quad u_j^0 = \phi(j \Delta x)
$$
 $J = 20$, $\Delta x = \pi/20$ and $s = 5/11$ 

$s = 5/11$ is stable, $s = 5/9$ is not stable

---

### Stability Criterion

In the previous problem, the primary distinction is whether $s$ is bigger or smaller than $1/2$ 

To actually demonstrate that this is the stability condition...

**Separate the Variables in the DIfference Equation** 
$$
(4) \qquad u_j^n = X_j T_n
$$
$X_j T_{n+1} = s(X_{j+1}T_n + X_{j-1}T_n) + (1-2s)X_jT_n$ 
$$
(5) \qquad \frac{T_{n+1}}{T_n} = 1 - 2s + s \frac{X_{j+1} +X_{j-1}}{X_j}
$$
Both sides of $(4)$ (do they mean (5)) must be a constant $\xi$ independent of $j$ and $n$ Therefore:

$T_1 = \xi T_0 \implies T_2 = \xi T_1 = \xi ^2 T_0$ and so on: 
$$
(6) \qquad T_n = \xi^n T_0
$$
and
$$
(7) \qquad s \frac{X_{j+1} + X_{j-1}}{X_j}+ 1 - 2s = \xi
$$
**Solving (7)**: argue it is a discrete version of a second order ODE which has sine and cosine solutions

**Guess** solutions of (7) of the form
$$
X_j = A\cos j\theta + B \sin j\theta
$$
for some $\theta$, where $A$, $B$ are arbitrary

**Boundary condition** $X_0 = 0$ at $j = 0$ $\implies$ $A = 0$ so we freely set $B = 1$ $\implies$ $X_j = \sin j\theta$ 

then $X_J = 0 = \sin J\theta$ $\implies$ $J\theta = \pi k$ for some integer $k$ 

and the **discretization** into $J$ equal intervals of length $\Delta x$ means that $J = \pi /\Delta x$ $\implies$ $\theta = k \Delta x$ and:
$$
(8) \qquad X_j = \sin (jk \Delta x)
$$
Now $(7)$ takes the form:
$$
s \frac{\sin((j+1)k \Delta x) + \sin((j-1)k \Delta x)}{\sin (j k \Delta x)} + 1 - 2s = \xi
$$
or because: $\sin( (j \pm 1)k\Delta x) = \sin(j k \Delta x)\cos(k \Delta x) \pm \sin(k \Delta x)\cos(j k \Delta x)$ 
$$
(9)\qquad \xi = \xi(k) = 1 - 2s[1 - \cos(k \Delta x)]
$$
**From equation (6)**: The growth in time $t = n \Delta t$ at the **wave number $k$** is governed by the powers $\xi(k)^n$, Therefore:

**unless $|\xi(k)| \leq 1$ for all $k$, the scheme is unstable** 

and could not possibly approximate the true (exact) solution

**the true solution tends to zero as $t \rightarrow \infin$** 

**analyze (9)** to determine whether $|\xi(k)| \leq 1$ or not: 

$0 \leq 1 - \cos(k \Delta x) \leq 2$, so this means $1 - 4s \leq \xi \leq 1$ and stability requires that $1 - 4s \geq -1$ $\implies$ 
$$
(10) \qquad \frac{\Delta t}{(\Delta x)^2} = s \leq \frac{1}{2}
$$
**Further analysis**: 

The wave number $k$ for which $\xi(k) = -1$ is the most dangerous for stability. I.e., $k = \pi/\Delta x$, which in practice is a fairly high wave number.

**The complete solution of the difference scheme (2)** together with discrete boundary conditions, is the "Fourier Series"
$$
(11) \qquad u_j^n = \sum_{k = -\infin}^\infin b_k \sin(j k \Delta x)[\xi(k)]^n
$$
Taylor series of $(9)$ (centered around $k = 0$):

$\xi'(k) = -2s \Delta x \sin(k \Delta x)$, $\xi''(k) = -2s(\Delta x)^2 \cos(k \Delta x)$ and each term is $\frac{\xi^{(n)}(k)}{n!}(k-0)^n$ 

$\xi(k) = 1 - 2sk^2(\Delta x)^2/2! + \cdots \simeq 1 - k^2\Delta t​$

if $k\Delta x$ is *small* 

**Taking the nth power** and letting $j\Delta x = x$ and $n \Delta t = t$ we have:
$$
\xi(k)^n \simeq (1 - k^2 \Delta t)^{t/\Delta t} \simeq e^{-k^2t}
$$
since $\lim_{n \rightarrow \infin}(1 + \frac{x}{n})^n = e^x$ and we are replacing $x = -k^2 t$ 

Therefore, the series $(11)$ (from our approximation) looks like it tends to the series $(3)$ *as it should* (**this isn't a proof of convergence**) 

---

**General Procedure to Determine Stability in a diffusion or wave problem**: Separate the variables in the difference equation

For the time factor, we obtain a simple equation like (6) which as an **amplification factor** $\xi(k)$ 

**Correct Condition necessary for stability**: aka **von Neumann stability condition** 
$$
(12) \qquad |\xi(k)| \leq 1 + O(\Delta t) \qquad \text{for all }k
$$
and for small $\Delta t$ 

Quckly go from $(7)$ to $(9$)by assuming:
$$
(13) \qquad X_j = (e^{i k \Delta x})^j
$$
plugging $13$ into $7$ we recover equation $(9)$ for the amplificaiton factor $\xi$ 

### Neumann Boundary Conditions

Interval: $0 \leq x \leq l$ and BCs:
$$
u_x(0, t) = g(t) \qquad \text{and}\qquad u_x(l, t) = h(t)
$$
Simplest approximations:
$$
\frac{u_1^n - u_0^n}{\Delta x} = g^n \qquad \text{ and }\qquad \frac{u_J^n - u_{J-1}^n}{\Delta x} = h^n
$$
**however**: they introduce local truncation errors of order $O(\Delta x)$ 

**Instead**: Use cetnered differences for $O(\Delta x)^2$ errors only

Introduce: "ghost points" $u_{-1}^n$ and $u_{J+1}^n$ in addition to $u_0^n, \ldots, u_J^n$ 

Then:
$$
\frac{u_1^n - u_{-1}^n}{2\Delta x} = g^n \qquad \text{and}\qquad \frac{u_{J+1}^n - u_{J-1}^n}{2\Delta x} = h^n
$$
at $n$th time step: calculate $u_0^n, \ldots, u_J^n$ using the scheme for the PDE
Calculate the values at the ghost points using $(14)$ 

### Crank-Nicolson Scheme

**There is a class of schemes that is stable no matter what the value of $s$** 

Denote the **centered second difference**: 
$$
\frac{u^n_{j+1} - 2u_j^n + u_{j-1}^n}{(\Delta x)^2} = (\Delta^2 u)_j^n
$$
*Pick a number $\theta$ between 0 and 1*. Consider the scheme:
$$
(15) \qquad \frac{u_j^{n+1}-u_j^n}{\Delta t} = (1-\theta)(\delta^2 u)_j^n + \theta(\delta^2 u)_j^{n+1}
$$
We'll call it **the $\theta$ scheme**. If $\theta = 0$, it reduces to the previous scheme. If $\theta > 0$, the scheme is **implicit** since $u^{n+1}$ appears on both sides.  

Therefore: $(15)$ $\implies$ we solve a set $(n = 1)$ of algebraic linear equations to get $u_j^1$, another set $(n = 2)$ to get $u_j^2$, and so on

**Stability of this scheme**: analyze by plugging in:
$$
u_j^n = (e^{ik \Delta x})^j (\xi(k))^n
$$
Then:
$$
\xi(k) = \frac{1 - 2(1-\theta)s(1- \cos k \Delta x)}{1 + 2\theta s(1- \cos k \Delta x)}
$$
where $s = \Delta t/(\Delta x)^2$ 

Look for the stability condition $|\xi(k)| \leq 1$ for all $k$ 

Always true that $\xi(k) \leq 1$, but the condition $\xi(k) \geq -1$ requires:
$$
s(1- 2\theta)(1 - \cos k \Delta x)\leq 1
$$
**How do we know it is always true that $\xi(k) \leq 1$ ** 

$s(1 - 2\theta)(1 - \cos k \Delta x) = s(1 -2\theta - \cos k \Delta x + 2 \theta \cos k \Delta x )$

$\xi(k) = \frac{1 - (2 - 2\theta)s(1 - \cos k \Delta x)}{1 + 2\theta s (1 - \cos k \Delta x)}$ 

$\xi(k) = \frac{1 - 2s + 2s \cos k \Delta x + 2\theta s - 2\theta s \cos k \Delta x}{1 + 2\theta s(1 - \cos k \Delta x)}$ 

$\xi(k) = 1 - \frac{2s(1 - \cos k \Delta x)}{1 + 2\theta s(1 - \cos k \Delta x)} \geq -1$ 

$-2s(1 - \cos k \Delta x) \geq -2(1 + 2\theta s(1- \cos k \Delta x))$ $\implies$ 

$s(1 - \cos k \Delta x) \leq1 + 2\theta s ( 1 - \cos k \Delta x)$ $\implies$ 

$s(1 - 2\theta s)(1 - \cos k \Delta x) \leq 1$ 

---

$\xi(k) \leq 1$ if $1 - \frac{2s(1 - \cos k \Delta x)}{1 + 2\theta s(1 - \cos k \Delta x)} \leq 1$ $\implies$ $- \frac{2s(1 - \cos k \Delta x)}{1 + 2\theta s(1 - \cos k \Delta x)} \leq 0$ 

$-4s \leq 2s(1 - \cos k \Delta x) \leq 2s$ 
$1-4s \leq -4\theta s\leq 1 + 2\theta s(1 - \cos k \Delta x) \leq 1 + 2\theta s \leq 1+2s$  

I believe $-\frac{2s}{1 + 2s} \leq 0$ so we have$\xi(k) \leq 1$ (this is assuming $-\frac{4s}{4s - 1}\leq - \frac{2s}{1 + 2s})$ 

---

So if $1 - 2\theta \leq 0$, we have that $s(1 - 2\theta )(1 - \cos k \Delta ) \leq 1$ is always true:
$$
(16) \qquad \text{if }\frac{1}{2} \leq \theta \leq 1, \text{ there is no restriction on the size of }s
$$
for stability to hold. 

Such a scheme is called **Unconditionally stable** 

The special case: $\theta = \frac{1}{2}$ is called the Crank- Nicolson scheme

Its template:

![image-20190402074741763](Book Notes Section 8.assets/image-20190402074741763.png)

On the other hand: in case $\theta < \frac{1}{2}$ a necessary condition for stability is $s \leq (2 - 4 \theta)^{-1}$ . So $(15)$ is stable if: 
$$
\frac{\Delta t}{(\Delta x)^2}= s < \frac{1}{2 - 4\theta}
$$
$\theta = \frac{1}{2}$ plugged into 15:
$$
\frac{u_j^{n+1}-u_j^n}{\Delta t} = \frac{1}{2}(\delta^2u)_j^n + \frac{1}{2}(\delta^2 u)_j^{n+1}
$$
$\frac{s}{1 + s} = \frac{\Delta t/(\Delta x)^2}{\frac{(\Delta x)^2 + \Delta t}{(\Delta x)^2}}$ $ = \frac{\Delta t}{(\Delta x)^2 + \Delta t}$ (idk if this is helpful or not)

so we have altogether:
$$
\frac{u_j^{n+1} -u_j^n}{\Delta t} = \frac{1}{2}\frac{u_{j+1}^n - 2u_j^n + u_{j-1}^n}{(\Delta x)^2} + \frac{1}{2} \frac{u_{j+1}^{n+1} - 2u_j^{n+1} + u_{j-1}^{n+1}}{(\Delta x)^2}
$$

$$
u_j^{n+1} = \frac{1}{2}s(u_{j+1}^n - 2u_j^n + u_{j-1}^n + u_{j+1}^{n+1}- 2u_j^{n+1}+u_{j-1}^{n+1}) + u_j^n
$$

$$
u_j^{n+1} = \frac{1}{2} s u_{j+1}^n - su_j^n + \frac{1}{2}s u_{j-1}^n + \frac{1}{2}s u_{j+1}^{n+1} - s u_j^{n+1} + \frac{1}{2}s u_{j-1}^{n+1} + u_j^n
$$

$$
u_j^{n+1} = \frac{1}{2}s(u_{j+1}^n +u_{j-1}^n + u_{j+1}^{n+1} -2u_j^{n+1} + u_{j-1}^{n+1}) + (1 - s)u_j^n
$$

$$
u_j^{n+1}+ su_j^{n+1} = \frac{1}{2}s(u_{j+1}^n + u_{j-1}^n + u_{j+1}^{n+1} + u_{j-1}^{n+1}) + (1-s)u_j^n
$$

and we get the template as above by dividing both sides by $1 +s$ 
# Math 126 Book Notes Chapter 1

Unlike last semester, this will just be me explaining some things to myself mostly.

## 1.2 First-Order Linear Equations

### Working with: **$au_x + bu_y$**

#### Geometric Method

In regards to lines parallel to $V$:

$V$ = $(a, b)$ lines parallel to $V$ will be: $(c, d) + t(a, b)$ 

$(c+at, d + bt)\cdot(b, -a)$ $ = bc + bat -ad - abt$ $= bc - ad + bat - bat$ = $bc - ad$ 

and $bc - ad$ is constant, depending on $(c,d)$ 

---

In regards to the solution to $au_x + bu_y = 0$:

Reasoning: This may require really understanding directional derivative

* $au_x + bu_y$ is the directional derivative of $u$ in the direction $(a,b)$ 
* since the directional derivative in the direction $(a,b)$ is $0$, according to the equation, $u(x,y)$ is constant in the direction of $(a,b)$
* So on any line of the form $(c, d) + t(a,b)$, $u(c + at,d + bt)$ will be constant for any $t$ 
* and for any $t$, $b(c + at) - a(d + bt)$ is constant
* so the solution depends only on $bx - ay$, as any point in $\R^2$ lies on some line going in the direction $(a,b)$, and this line is uniquely identified with the value $bx - ay$ 
  * $(x_1, y_1), (x_2, y_2)$ 
  * $bx_1 - ay_1 = bx_2 - ay_2$ 
  * $b(x_1 - x_2) = a(y_1 - y_2)$ 
  * $\frac{b}{a}(x_1 - x_2) = (y_1 - y_2)$ 
  * these points lie on a line going in the direction $(a,b)$ 

This is why $u(x,y) = f(bx - ay)$, $f$ is any function of one variable

---

#### Coordinate Method - Change variables

so we want to use new variables, $x'$ and $y'$ 

we let $x' = ax + by$, $y' = bx - ay$ 

this seems kind of arbitrary, but it lets us have a nicer equation at the end.

note, we know first partial derivatives of $x', y'$ w.r.t. $x, y$ 

* use chain rule to get $u_x$ and $u_y$ in terms of $u_{x'}$ and $u_{y'}$ so we can have the equation in terms of $u_{x'}$ and $u_{y'}$ 

we substitute $u_x$ and $u_y$ and get $au_x + bu_y = (a^2 + b^2)u_{x'} = 0$ 

since $a^2 + b^2 \neq 0$ $0/(a^2 + b^2) = 0$ so we have $u_{x'} = 0$ 

and $u = f(y')$ $= f(bx - ay)$ 

So change of variables:

1. define new variables in terms of old variables
2. use chain rule to express old partial derivatives in terms of new partial derivatives
   1. example: $u_x = g(u_{x'}, u_{y'})$, $u_y = h(u_{x'}, u_{y'})$ 
3. Substitute in $u_x, u_y$ with the above
4. we should have a solution in terms of $x', y'$, and we replace $x', y'$ with their expressions in terms of $x, y$ 

---

### Example 1: $4u_x - 3u_y$ 

So they derived the general answer quite easily: $f(-3x - 4y)$

having $u(0,y) = y^3$ as a condition allows us to get a more specific answer

so we plug in $(0,y)$: $u(0,y) = f(-3(0) - 4y)$ $ = f(-4y)$ $= y^3$ 

*Trick*: Change of variables: $w = -3x-4y$, and in this case, $w = -4y$  

$w^3 = -64y^3$ so $y^3 = -w^3/64$ 

so $f(w) = -w^3/64$ 

$\implies$ $f(-3x - 4y) = (3x + 4y)^3/64$ 

---

### The Variable Coefficient Equation

$$
u_x + yu_y = 0
$$

Linear and homogeneous:

Checking linear:

$L(u+v) = (u+v)_x + y(u+v)_y$ $ = u_x + v_x + yu_y + yv_y = u_x + yu_y + v_x + yv_y$ 

$ = L(u) + L(v)$ 

---

Confusion "the curves in the xy plane with $(1,y)$ as tangent vectors have slopes $y$"

i guess this makes sense cause if $(1,y)$ is the tangent vector, this means the slope is $\frac{y}{1}$ duh, definition of tangent

---

there are curves with points $(x,y)$ s.t. $(1,y)$ is the tangent vector

in these situations, their equations are $\frac{dy}{dx} = \frac{y}{1}$ 

and we obtain $y = Ce^{x}$, the characteristic curves of the above PDE

---

"As C is changed, the curves fill out the xy plane perfectly without intersecting"

fill out part:

for any $(x,y)$: we can fix $e^x$ and solve for $C = \frac{y}{e^x}$ as $e^x \neq 0$ for all $x \in \R$ 

without intersecting:

Fix $x$ (WLOG, we can also do this for $y$)

$C_1e^x \neq C_2 e^x$ 

---

Showing $u(x,y)$ is constant on any curve $Ce^x$:

$\frac{d}{dx} u(x, y)$ $= \frac{\partial u}{\partial x}\frac{\partial x}{\partial x} + \frac{\partial u}{\partial y}\frac{\partial y}{\partial x}$ $ = u_x + Ce^x u_y$ 

since $y = Ce^x$ and $\frac{\partial y}{\partial x} = Ce^x$ 

---

Since $u(x,y)$ is constant along any characteristic curve, and each characteristic curve can be uniquely identified by its $C$ value, and $C = ye^{-x}$ we have $u(x,y) = f(C) = f(e^{-x} y)$ 

**The general solution**: $f(e^{-x}y)$ 

---

Checking by differentiation:

$\frac{\partial f}{\partial x} = -ye^{-x}$

$\frac{\partial f}{\partial y} = e^{-x}$ 

$-ye^{-x} +yu^{-x}$ $ =0$ as expected

---

Example 2: Find the solution of $u_x + yu_y = 0$ satisfying

$u(0,y) = y^3$ 

$f(y) = y^3$ 

so $f(e^{-x}y) ={e^{-3x}y^3}$

---

Example 3: Solve $u_x + 2xy^2u_y = 0$ 

$(1, 2xy^2)$ 

so slope of characteristic curves

$\frac{dy}{dx} = 2xy^2$ 

Separation of variables:

$dy = 2xy^2 dx$ 

$\frac{dy}{y^2} = 2x dx$ 

$-\frac{1}{y} = x^2 + C$ 

$y = \frac{1}{-x^2 + C}$ 

so $u$ is a function of $C$, the constant that uniquely determines each curve, because $u$ is constant on each curve.

$C = -\frac{1}{y} - x^2$ 

## 1.3 Flows, Vibrations, and Diffusions

### Example 1. Simple Transport

Intersting in regards to Example 1:

Using $F(x) = \int_a^x f(t)dt$ $\implies F'(x) = f(x)$

so since $M$ $= \int_0^b u(x,t) dx = \int_{ch}^{b+ch} u(x, t+h)dx$, differentiating both sides w.r.t. to $b$ 

$u(b,t) = u(b+ch, t+h)$ 

Differentiating w.r.t. to $h$:

$0 = cu_x(b+ch, t+h) + u_t(b+ch, t+h)$ 

letting $h = 0$:

$cu_x(b, t) + u_t(b, t)$ 

---

### Example 2. Vibrating String

homogeneous $\implies$ constant density along string

flexible $\implies$ Tension (force) is directed tangentially along the string

Don't understand:

page 12, the longitudinal and transverse Newton's law

is $T$ a constant? (assume yes?)

so they multiplied the magnitude of tension $T$ by the slope $u_x$ (for transverse that is)

$\frac{1}{\sqrt{1 + u_x^2}}$ and $\frac{u_x}{\sqrt{1 + u_x^2}}$ ??? what are these?

skipping this...

Next confusion: Taylor expansion 

$f(x) = f(a) + \frac{f'(a)}{1!}(x-a) + \frac{f''(a)}{2!}(x-a)^2$ 

in this case, $f(x) = \sqrt{1 + x}$ , $a = 0$ 

$f'(x) = \frac{1}{2(1+x)}$ , $f''(x) = -\frac{1}{2(1+x)^2}$ 

$\sqrt{1 + x} = 1 + \frac{1}{2} x + \frac{1}{2\cdot 2!}x^2 + ...$

plugging in $x = u_x^2$ 

if $u_x$ small, we have $\sqrt{1 + u_x^2} \approx 1$ 

so we end up with $\frac{Tu_x}{\sqrt{1 + u_x^2}}|_{x_0}^{x_1}= \int_{x_0}^{x_1}\rho u_{tt}dx \approx Tu_x|_{x_0}^{x_1}$

### Example 3. Vibrating Drumhead

 $\int Pdx + Qdy = \int \int \frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y}dxdy$ 

What is $\nabla$ when it is dot producted with something?:

$\text{div }F = \nabla \cdot F$ = $\frac{\partial F_1}{\partial x} + \frac{\partial F_2}{\partial y} + \frac{\partial F_3}{\partial z}$ 

$T \frac{\partial u}{\partial n}ds  = T(n \cdot \nabla u)ds$

What is $ds$?

I have a lot of questions...

### Example 4. Diffusion




# Math 126 Book Notes Section 9

# Waves in Space

This is sort of from the lecture 3/11 or 3/13 possibly 

## 9.1 Energy and Causality

**Goal**: Wave equation in 2 or 3 dimensions in the absence of boundaries 
$$
(1) \qquad u_{tt} -c^2\Delta u = 0
\\ u_{tt} = c^2(u_{xx}+u_{yy}+u_{zz})
$$
**This equation is invariant under**: 

1. Translations in space and time
2. rotations in space
3. Lorentz transformations 

### The Characteristic Cone

**The characeristics are now surfaces** 

Characteristic line in 1D: $x - x_0 = c(t-t_0)$, rotate around the $t = t_0$ axis to get a "hypercone"
$$
(2)\qquad |\bold x - \bold x_0| = |(x-x_0)^2 + (y-y_0)^2+(z-z_0)^2|^{1/2} = c|t-t_0|
$$
cone in four dimensional space-time. 
**Characteristic cone** or **light cone** at $(\bold x_0, t_0)$: The set in space-time defined by equation $(2)$ 

* light cone, for $c$ the speed of light
* cone is the union of all light rays emanating from $(\bold x_0, t_0)$ 

![image-20190319171334574](Math 126 Book Notes Section 9.assets/image-20190319171334574.png)

**Solid light cone**: "inside" of the cone, $\{ |\bold x - \bold x_0| < c|t-t_0|\}$ 

* Union of the future and the past half-cones
* **fixed time $t$**: the light cone is just an **ordinary sphere**, and the future is just the ball consisting of the points that can be reach in time $t$ by a particle moving at less than the speed of light from $(\bold x_0, t_0)$ 

#### Geometry

**unit normal vector to the light cone (2)** 

* 3-surface in 4-space given by the equation: (The light cone is given by the equation below I believe): 

$$
\phi(t, x, y, z) \equiv -c^2(t-t_0)^2 +(x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2 = 0
$$

* The above is a level surface of $\phi$, so a normal vector is the gradient vector of $\phi(x, y, z, t)$ (vectors with four components)

$$
\text{grad }\phi = (\phi_x, \phi_y, \phi_z, \phi_t) = 2(x-x_0, y-y_0, z-z_0, -c^2(t-t_0))
$$

* The unit normal vectors are:

* $$
  \bold n = \pm \frac{\text{grad }\phi}{|\text{grad }\phi|}
  $$

or more explicitly: $\bold n =  \pm \frac{(x-x_0, y-y_0, z-z_0, -c^2(t-t_0))}{\sqrt{c^4(t-t_0)^2 + (x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2}}$

Note: we factored out the $2$ from $\text{grad } \phi$ , for simplicifation 

* $r^2 = (x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2$, so with new notation:
  * Light cone equation: $r = \pm c (t-t_0)$ 
  * $\bold n = (\frac{x-x_0}{\sqrt{(c^2 + 1)r^2}}, \cdots, \frac{-c^2(t-t_0)}{\sqrt{(c^4 + c^2)(t-t_0)^2}})$
    * $r^2 = (x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2 = c^2(t-t_0)^2$ 
      so $\sqrt{(c^2 + 1)r^2} = \sqrt{c^4(t-t_0)^2 + (x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2} = \sqrt{(c^4 + c^2)(t-t_0)^2}$   
  * $(3) \qquad \bold n = \pm \frac{c}{\sqrt{c^2 + 1}}( \frac{x-x_0}{cr} , \cdots, - \frac{t-t_0}{|t-t_0|})$ 

we have **two unit normal vectors** to the **light cone** in **4-space**: one pointing **in** and one pointing **out** 

### Conservation of Energy

**Fundamental concept**: Mimicing **Section 2.2** 

* Multipling wave equation by $u_t$ then do algebra:

$$
(4) \qquad 0 = (u_{tt}-c^2\Delta u)u_t = \frac{1}{2}(u_t^2 + c^2|\nabla u|^2)_t - c^2 \nabla \cdot(u_t \nabla u)
$$

**Explanation on the algebra**: 

so we added $0 = \frac{c^2}{2}(|\nabla u|^2)_t - c^2 (\nabla u_t \cdot \nabla u)$

$\frac{1}{2} (|\nabla u|^2)_t = \frac{1}{2}(2(\nabla u_t \cdot \nabla u))= \nabla u_t \cdot \nabla u = u_{x}u_t + u_y u_t + u_z u_t$

* Integrate over 3-space
* Integral of $-c^2 \Delta \cdot (u_t \nabla u)$ should dissapear if the derivatives of $u(\bold x, t)$ $\rightarrow 0$ as $|\bold x| \rightarrow \infin$ 
* Assuming the derivatives disappear: 

$$
(5) \qquad 0 = \iiint_{\R^3} \frac{\partial}{\partial t}(\frac{1}{2}u_t^2 + \frac{1}{2}c^2|\nabla u|^2)\, d \bold x
$$

* Pull out time derivative 

$$
(6) \qquad E = \frac{1}{2} \iiint (u_t^2 + c^2|\nabla u|^2)\, d \bold x
$$

is a **constant** because $E_t = 0$ from $(5)$ 

* First term in (6) is **kinetic energy**, second term is **potential energy**

### Principle of Causality

* Let $u$ solve:

$$
u_{tt} = c^2 \Delta u
\\ u(\bold x, 0) = \phi(\bold x)\quad u_t(\bold x, 0) = \psi(\bold x)
$$

* Fix a point $\bold x_0$ and fix time $t_0 > 0$ 
* **Principle of Causality**: $u(\bold x_0, t_0)$ depends only on the values of $\phi(\bold x)$ and $\psi(\bold x)$ in the ball $\{ |\bold x - \bold x_0| \leq ct_0 \}$ 
  * This is the intersection of the solid light cone: $\{|\bold x - \bold x_0| < c|t-t_0|\}$ with the initial hyperplane $\{ t = 0 \}$ 
  *  ![image-20190319180557602](Math 126 Book Notes Section 9.assets/image-20190319180557602.png)

#### Proof

* Write out explicitly $(4)$ to get 

$$
(7)\qquad \partial_t(\frac{1}{2}u_t^2 + \frac{1}{2}c^2|\nabla u|^2) + \partial _x (-c^2 u_t u_x)+ \partial _y(-c^2u_t u_y) + \partial _z(-c^2u_tu_z) = 0
$$

Note: $\partial_t = \partial/\partial t$ is an abbreviation 

* Integrate $(7)$ over a solid cone "frustum" $F$ in **four**-dimensional space-time: top $T$, bottom $B$, side $K$: it is simply a piece of a solid light cone
* $(7)$: sets up four-dimensional divergence theorem
  * $F$ is four dimensional with $\partial F$ three dimensional
* $(n_x, n_y, n_z, n_t)$ is the outward normal $4$-vector on $\partial F$ 
* $dV$ denotes three dimensional volume integral over $\partial F$ 

$$
(8) \qquad \iiint_{\partial F} [n_t (\frac{1}{2}u_t^2 + \frac{1}{2}c^2|\nabla u|^2) - n_x(c^2u_tu_x) - n_y(c^2u_tu_y)-n_z(c^2u_tu_z)]dV = 0
$$

* $\partial F = T \cup B \cup K$  $\implies$ $(8)$ = $ \iiint_T +\iiint_B + \iiint_K = 0$  

  * on $T$, the normal vector points straight up: $(n_x, n_y, n_z, n_t) = (0, 0, 0, 1)$ 

    $\iiint_T (\frac{1}{2}u_t^2 + \frac{1}{2}c^2|\nabla u|^2 \, d\bold) x$ 

  * on $B$: the normal vector points straight down: $(n_x, n_y, n_z, n_t) = (0, 0, 0, -1)$ 

    $\iiint_B(-1)(\frac{1}{2}u_t^2 + \frac{1}{2}c^2|\nabla u|^2 \, d\bold)\, d\bold x = -\iiint_B (\frac{1}{2} \psi^2 + \frac{1}{2} c^2 |\nabla \phi|^2 ) \, d \bold x​$ 

  * on $K$: complicated, **claim**: it is positive or zero

    * Proof: plug the formula $(3)$ for $\bold n$ into the $K$ integral. 
    * have positive $t$ component on $K$, (see figure 4)
    * note: $t < t_0$ and $r= |\bold x - \bold x_0|$ 

    $(9) \qquad \frac{c}{\sqrt{c^2 + 1}} \iiint_K [\frac{1}{2}u_t^2 + \frac{1}{2}c^2 |\nabla u|^2 + \frac{x-x_0}{cr}(-c^2u_tu_x) + \frac{y-y_0}{cr}(-c^2u_tu_y) + \frac{z-z_0}{cr} (-c^2 u_tu_z)] dV$ Last integrand can be written more concisely:

    $(10) \qquad I = \frac{1}{2}u_t^2 + \frac{1}{2}c^2 |\nabla u|^2 - cu_t u_r$

    Where $\nabla u = (u_x, u_y, u_z)$ 

    * $\hat{\bold r} = \frac{\bold x - \bold x_0}{|\bold x - \bold x_0|} = (\frac{x-x_0}{r}, \frac{y-y_0}{r}, \frac{z-z_0}{r})$ 
    * radial derivative: $u_r = u_x \frac{\partial x}{\partial r} + u_y \frac{\partial y}{\partial r} + u_z \frac{\partial z}{\partial r} = \hat{\bold r} \cdot \nabla u$  

  * Complete square in $(10)$ 

  * $(11) \qquad I = \frac{1}{2}(u_t - cu_r)^2 + \frac{1}{2} c^2(|\nabla u|^2 - u_r^2) + \frac{1}{2}c^2(|\nabla u|^2 - u_r^2) = \frac{1}{2}(u_t - cu_r)^2 + \frac{1}{2}c^2|\nabla u - u_r \hat{\bold r}|^2$

    * check this later
    * THis is **positive**

  * Therefore $(9)$ is positive since the integrand $(10) = (11)$ is positive 

* From $(8)$, rearranging terms, and $(9)$ being positive 

$$
(12)\qquad \iiint_T (\frac{1}{2}u_t^2 + \frac{1}{2}c^2 |\nabla u|^2)d\bold x \leq  \iiint_B (\frac{1}{2}\psi^2 + \frac{1}{2}c^2|\nabla \phi|^2)\, d \bold x
$$

* Assume $\psi$ and $\phi$ vanish in $B$ $\implies$ By $(12)$ and virst vanishing theorem, $\frac{1}{2}u_t^2 + \frac{1}{2}c^2|\nabla u|^2$ vanishes in $T$ $\implies$ $u_t$ and $\nabla u$ vanish in $T$ 
* BUT we can vary the heigh of $F$ at will
* $\implies$ $u_t$ and $\nabla u$ vanish in the whole piece of solid cone $C$ that lies above $B$ 
* $\implies$ $u$ is a cosntant in the solid cone $C$ 
* and since $u =  \phi = 0$ on the bottom $B$ $\implies$ $u \equiv 0$ in all of $C$ 
  * in particular $u(\bold x_0, t_0) = 0$ 
* So this means that if we took the difference of two solutions $u$ and $v$, where $u = v$ in $B$, and took the difference, we can see that $u(x_0, y_0, z_0, t_0) = v(x_0, y_0, z_0, t_0)$ 
  * so we have two different solutions (?) but since they are identical at least in $B$, they are the same at $(\bold x_0, t_0)$, so the value of $u$ at $(\bold x_0, t_0)$ depends only on the values $\phi$ and $\psi$ at $B$ indeed...
  * i.e.: We can take any two solutions, and if $u = v$ on $B$, then $u(\bold x_0, t_0) = v(\bold x_0, t_0)$, which means that the value of $u$ or $v$ at $(\bold x_0, t_0)$ only depends on $\phi, \psi$ on $B$ 

##### Domain of Dependence/Past History of $(\bold x_0, t_0)$ 

Is the cone $C$ that was just discussed

#### Corollary

The initial data $\phi$, $\psi$ at a spatial point $\bold x_0$ can influence the solution only in the solid light cone with vertex at $(\bold x_0, 0)$ 

##### Domain of influence of a point $(\bold x_0, 0)$  

is at most the solid light cone emanating from that point

**From the PDE alone**: no signal can travel faster thant he speed of light

## 9.2 The Wave Equation in Space-Time

**Explicit Formula**: For the solution of the **homogeneous** wave equation
$$
\begin{align}
&(1) && u_{tt} = c^2(u_{xx}+u_{yy} +u_{zz})
\\ &(2) && u(\bold x, 0) = \phi(\bold x) && u_t(\bold x, 0) = \psi(\bold x)
\end{align}
$$
The **answer**: 
$$
(3) \qquad u(\bold x, t_0) = \frac{1}{4\pi c^2 t_0}\iint_S \psi(\bold x)\, dS + \frac{\partial}{\partial t_0}[\frac{1}{4\pi c^2 t_0} \iint_S \phi(\bold x)\, dS]
$$
looks a lot like the D'Alembert's Formula 2.1.8

* $S$: sphere of center $\bold x_0$, radius $ct_0$ 

### Compare to Causality Principle

* $u(\bold x_0, t_0)$ Depends, according to $(3)$ just on the values of $\psi(\bold x)$ and $\phi(\bold x)$ for $\bold x$ on the sphereical **surface** $S = \{|\bold x - \bold x_0| = ct_0 \}$ **but not** on the values of $\psi$ and $\phi$ **inside** this sphere
  * Figure 1![image-20190319210338239](Math 126 Book Notes Section 9.assets/image-20190319210338239.png)
* Alternatively, we can say: the values of $\psi$ and $\phi$ at a spatial point $\bold x_1$ influence the solution only on the **surface** $\{|\bold x - \bold x_1| = ct\}$ of the light cone that emenate from $(\bold x_1, 0)$ 
  * Figure 2![image-20190319210347280](Math 126 Book Notes Section 9.assets/image-20190319210347280.png)

##### Huygen's Principle

Any solution of the 3-dim wave equation propagates at **exactly** the speed $c$ of light, no faster, and **no slower** 

* This allows us to see sharp images
* any sound is carried through the air at exactly a fixed speed $c$ without "echoes"
* Thus: at any time $t$ a listener hear exactly what has been played at the time $t - d/c$ where $d$ is the distance to the musical instrument, rather than hearing a mixture of the notes played at various earlier times

#### Proof of Kirchhoff's Formula (3)

##### Method of Spherical means

* Average of $u(\bold x, t)$ on the sphere $\{ |\bold x| = r \}$ of center $\bold 0$ and any radius $r$ be denoted $\overline u(r, t)$ 

$$
\begin{align} 
(4) \qquad \overline u(r, t) &= \frac{1}{4 \pi r^2} \iint_{|\bold x| = r} u(\bold x, t)\, dS
\\ &= \frac{1}{4\pi}\int_0^{2\pi}\int_0^\pi u(\bold x, t) \sin \theta \, d \theta \, d\phi
\end{align}
$$

* **Show** $\overline u$ satisfies the PDE:

$$
(\overline u)_{tt} = c^2(\overline u)_{rr} + 2c^2 \frac{1}{r}(\overline u)_r
$$

##### Proof of (5)

**Simplicity**: assume $c = 1$. 
Use rotational invariance of $\Delta$: $\Delta(\overline u) = (\overline{\Delta u})$ $\equiv$ the laplacian of the mean is the mean of the laplacian. Therefore:
$$
\Delta (\overline u) = (\overline{\Delta u}) = (\overline{u_{tt}}) = (\overline{u})_{tt}
$$
so we know $\overline{u}$ satisfies exactly the same PDE that $u$ does. and in spherical coordinates:
$$
\Delta (\overline u) = \overline u_{rr} + \frac{2}{r}\overline u_r + \text{angular derivative terms}
$$
and $\overline u$ **depends only on $r$** (see $(4)$), so we indeed get $(5)$ since the angular derivative terms vanish

**End of Proof of (5)** 

**Alternative proof of (5)**: Apply Divergence Theorem to $u_{tt} = \Delta u$ over the domain $D = \{|\bold x| \leq r \}$ 
$$
(6) \qquad \iiint_D u_{tt} \, d\bold x = \iiint_D \Delta u \, d \bold x = \iiint_D \nabla\cdot(\nabla u)\, d\bold x 
\\ = \iint_{\partial D}\nabla u \cdot \bold n \, dS = \iint_{\partial D}\frac{\partial u}{\partial n}\, dS
$$
**In spherical coordinates** :
$$
\int_0^r\int_0^{2\pi}\int_0^\pi u_{tt}\rho ^2 \sin \theta \, d\theta\, d\phi\, d\rho = \int_0^{2\pi}\int_0^\pi \frac{\partial u}{\partial r}r^2  \sin \theta d\theta\, d\phi
$$
or
$$
(7) \qquad \int_0^r \rho^2 \overline{u_{tt}}(\rho, t)\, d\rho = r^2 \frac{\partial \overline u}{\partial r}(r, t)
$$
we get $(7)$ from dividing both sides by $4\pi$ then $(4)$ on the integrals above 

**Differentiate (7)** w.r.t. to $r$: 
$$
r^2 \overline{u_{tt}} = 2r \overline u_r + r^2 \overline u_{rr}
$$
and this is equivalent to $(5)$ once we divide by $r^2$ 

**End of Proof of (5)** again

##### Returning to proof of (3)

**substitute**: $v(r,t) = r \overline u(r,t)$ into the PDE (5)
Then: $v_r = r\overline u_r + \overline u$ and $v_{rr} = r \overline u_{rr} + 2 \overline u_r$, so in terms of $v$: $(5)$ becomes:

$r \overline u_{tt} = c^2 r \overline u_{rr} + 2c^2 \overline u_r$ $\equiv$ $v_{tt} = c^2(r\overline u_{rr} + 2\overline u_r)$ $ = c^2v_{rr}$ 
$$
(8)\qquad v_{tt} = c^2 v_{rr}
$$
valid only for $0 \leq r < \infin$, where $v = r\overline u  = 0$ when $r = 0$ 
$$
(9) \qquad v(0,t) = 0 \qquad \text{for } r = 0
$$
and satisfies:
$$
(10) \qquad v(r, 0) = r \overline \phi (r)\qquad v_t(r, 0) = r \overline \psi(r)
$$
**Reduced to a half-line problem in one dimension**: the PDE $(8)$ and the BC $(9)$ and the IC $(10)$, which we know the solution to:
$$
(11) \qquad v(r,t) = \frac{1}{2c}\int_{ct-r}^{ct + r} s \overline \psi(s) \, ds + \frac{\partial }{\partial t}[\frac{1}{2c}\int_{ct-r}^{ct+r}s \overline \phi(s)\, ds]
$$
for $0 \leq r \leq ct$ and by a different one for $r \geq ct$ 

**Next**: **recover $u$ at the origin $r = 0$** 
$$
(12) \qquad u(\bold 0, t) = \overline u(0, t) = \lim_{r \rightarrow 0}\frac{v(r,t)}{r} 
\\ = \lim_{r \rightarrow 0} \frac{v(r,t) - v(0,t)}{r} = \frac{\partial v}{\partial r}(0,t)
$$
Note from $(4)$: $\overline u(0, t) = \frac{1}{4\pi} \int_0^{2\pi} \int_0^{\pi} u(\bold x, t) \sin \theta \, d\theta\, d\phi = u(\bold 0, t)$ 

**Differentiate (11)** 
$$
\frac{\partial v}{\partial r} = \frac{1}{2c}[(ct+r)\overline \psi(ct + r) + (ct-r)\overline \psi(ct - r)] + \cdots \text{similar term depending on }\overline \phi
$$
**Put $r = 0$**, and we obtain: $[\partial v/\partial r](0, t) = (1/2c)[2ct \overline  \psi (ct)] = t \overline \psi (ct)$ which is the right handside of $(12)$ 

**Therefore**: 
$$
(13) \qquad u(\bold 0, t) = t \overline \psi(ct) = \frac{1}{4 \pi c^2 t} \iint_{|\bold x| = ct} \psi(\bold x)\, dS + \cdots
$$
This is precisely the first term in formula $(3)$ (in the case that the spatial point is the origin and time is $t$): $t$ $\cdot$ the average of $\psi$ on the sphere of center $\bold 0$ and radius $ct$ 

**Translate the result (13)** : $w(\bold x, t) = u(\bold x + \bold x_0, t)$ 
this is the solution of the wave equation with initial date $\phi(\bold x + \bold x_0)$ and $\psi(\bold x + \bold x_0)$

**Apply $(13)$ to $w(\bold x, t)$: ** 
$$
(14)\qquad u(\bold x_0, t) = \frac{1}{4 \pi c^2 t} \iint_{|\bold x|= ct }\psi(\bold x + \bold x_0)\, dS + \cdots 
\\ = \frac{1}{4 \pi c^2 t}\iint_{|\bold x-\bold x_0|  = ct} \psi(\bold x)\, dS + \cdots
$$ {\}
This is the first term of $(3)$ 

Just need to show second term, but it works in the same way… all we need to do is do what we did to $\psi$ with $\phi$, but also take the time derivative, which is exactly $(3)$.

### Solution in Two Dimensions

##### Invalidity of Huygen's Principle in 2D!

$$
(15) \qquad u_{tt} = c^2 (u_{xx}+u_{yy})
\\ (16) \qquad u(x,y,0) = \phi(x,y), \quad u_t(x, y, 0) = \psi(x, y)
$$

Regard $u(x, y, t)$ as a solution of the 3D problem, **which just happens not to depend on $z$ ** $\implies$ given by Kirchhoff formula

**Simplicity**: assume $\phi \equiv 0$, $(x_0, y_0) = (0,0)$ so we obtain:
$$
 u(0, 0, t) = \frac{1}{4 \pi c^2 t} \iint_{x^2 +y^2 + z^2 = c^2t^2} \psi(x,y)\, dS
$$
**Simplify Further**: this is twice the integral over the top hemisphere $z = \sqrt{c^2t^2 - x^2 - y^2}$ (since it doesn't depend on $z$) 

and use the usual formula for Surface element dS (multivariable calculus) in terms of $(x,y)$ 
$$
(17) \qquad u(0, 0, t) = \frac{1}{2 \pi c^2 t} \iint_{x^2 + y^2 \leq c^2t^2} \psi(x,y)[1 + (\frac{\partial z}{\partial x})^2 + (\frac{\partial z}{\partial y})^2]^{1/2}\, dx \, dy
$$
Inside Brackets: $1 + (-\frac{x}{z})^2 + (-\frac{y}{z})^2 = \frac{z^2 - x^2 - y^2}{z^2} = \frac{c^2t^2}{z^2} = \frac{c^2t^2}{c^2t^2 - x^2 -y^2}​$ 
$$
(18) \qquad u(0, 0, t) = \frac{1}{2\pi c} \iint_{x^2 + y^2 \leq c^2t^2} \frac{\psi(x,y)}{\sqrt{c^2t^2 - x^2-y^2}}\, dx\, dy
$$

##### **At a general point:**  

$$
(19) \qquad u(x_0, y_0, t_0) = \iint_D \frac{\psi(x,y)}{[c^2t_0^2-(x-x_0)^2 - (y-y_0)^2]^{1/2}}\, \frac{dx \, dy}{2\pi c}
\\ + \frac{\partial}{\partial t_0}(\text{same expression but with }\phi)
$$

$D$ is the disk $\{(x-x_0)^2 + (y-y_0)^2 \leq c^2t_0^2 \}$ (this is just $x^2 + y^2 \leq c^2 t^2$ translated to have center $(x_0, y_0)$ instead of (0, 0)

$(19)$ shows that the value of $u(x_0, y_0, t_0)$ depends on the values of $\phi(x,y)$ and $\psi(x,y)$ inside the cone: $(x-x_0)^2 + (y-y_0)^2 \leq c^2t_0^2$ 

* we projected points on the sphere with radius $ct_0$ onto the domain $D$ in 2D, which is the area bounded by the circle with radius $ct_0$ (see Figure 3)

**Hyugen's principle is false in two dimensions** 

* Calm pond, drop pebble, get surface waves satisfying approximately the wave equation with speed $c$… water bug whose distance from the point of impact $\delta$ experiences a wave first at time $t = \delta/c$ but thereafter continues to feel ripples
  * theoretically the waves could continue forever, but the ripples physically become small enough that the wave equation is not really valid anymore as other physical effects begin to dominate
* Fun fact: 3 dimensions is the smallest dimension in which signals propagate sharply...
* for each odd dimension $n = 2m+1​$ we have Huygen's principle, and then descending to $2m​$ gets a formula that invalidates Huygen's principle

## 9.3 Rays, Singularities, and Sources

### Characteristics

**light ray**: a path of a point in three dimensions moving in a straight line at speed $c$ 

​	$|d \bold x/ dt| = c$ or $\bold x = \bold x_0 + \bold v_0 t$ where $|\bold v_0| = c$ 

* orthogonal to the sphere $|\bold x - \bold x_0| = c|t|​$ 
  * I think in space: $\bold x$ is orthogonal to $|\bold x - \bold x_0| = c|t|$ any straight line vector emanating from point $\bold x_0$ is going to be orthogonal to the sphere. 
  * what this looks like: a vector from the center of the sphere to a point on the sphere. [link to desmos][https://www.desmos.com/calculator] 
  * ![image-20190330003825980](Math 126 Book Notes Section 9.assets/image-20190330003825980.png)
* so varies with $\bold v_0$ (each different $\bold v_0​$ gives a different point on the sphere)

**time slices of surface $S​$**: $S_t = S \cap \{ t = \text{constant}\}​$ 

* $S​$ is three dimensional surface in 4 dimensions
* $S_t​$'s are 2-dimensional surfaces

**Characteristic Surface $S$**:  $S$ is a characteristic surface if it is a <u>union of light rays</u> each of which is <u>orthogonal in three-dimensional spsce to the time slices $S_t$</u> 

***Analytical Description***: 

Suppose $S$ is the level surface of a function of the form $t - \gamma(\bold x)$ or $S = \{ (\bold x, t): t - \gamma(\bold x) = k \}$ for some constant $k$ 

* The time slices are $S_t = \{ \bold x: t - \gamma(\bold x) = k \}$ (for each t constant)

#### Theorem 1

All level surfaces of $t - \gamma(\bold x)​$ are characteristic $\iff​$ $|\nabla \gamma(\bold x)| = 1/c​$ 
All level surfaces of $ t- \gamma(\bold x)​$ are chracteristic $\iff​$ the length of the gradient of $\gamma(\bold x)​$ is $1/c​$ 

##### Example

Take any surface $S_0$ at all in 3-space at $t = 0$. Draw straight lines $(1)$ of slope $c$ with $\bold x_0 \in S_0$ to construct a characteristc surface $S$ 

1. $\bold x_0​$ Satisfies: $a_1x_0 + a_2 y_0 + a_3 z_0 = b​$ ($a_1^2 + a_2^2 + a_3^2 =1 ​$) i.e., $S_0​$ is the surface $\{(x, y, z): a_1x + a_2 y + a_3 z = b\}​$ and we take surfaces $S​$ and $S'​$  to be $\{(\bold x, t): a_1x + a_2y + a_3z \pm ct = b  \}​$ 
   1. $\gamma(\bold x) = \pm\frac{1}{c}(a_1 x + a_2 y + a_3 z )$ I think.
      1. so we do have $|\nabla \gamma(\bold x)| = 1/c$ 
2. **sphere**: $S_0 = \{\bold x: |\bold x - \bold x_0| = R \}$ $\implies$ $S = \{ (\bold x, t): |\bold x - \bold x_0| = R \pm ct \}$ 
   1. $\frac{|\bold x - \bold x_0|}{c} = \frac{R}{c} \pm t$ so we have indeed $|\nabla \gamma(\bold x)| = 1/c$ with $\gamma(\bold x) = \frac{|\bold x - \bold x_0|}{c}$  

Is adding/subtracting $ct$ to any equation kosher?

* Check $|\nabla \gamma(\bold x)| = 1/c$ 
* the surface would be in the form $t - \gamma(\bold x) =​$ constant

### Relativsitic Geometry 

**past**/ **past history** of the point $(\bold 0, 0)$: A four-dimensional vector $(\bold v, v^0)$ is called:

* **Timelike** if $|v^0| > c|\bold v|​$ : points into the future or the past
* **Spacelike** if $|v^0| < c|\bold v|$ 
* **Null** (or **characteristic**) if $|v^0| = c|\bold v|$ 

**ray** or **bicharacteristic**: a straight line in space-time with a null (as described above) tangent vector; it projects onto a light ray

​	i.e. $\bold x =\bold w t$ (describes a straight line that runs through point $0$ in the direction of $\bold v$) and taking $|\bold x| = |\bold w t|$ where $t \equiv v^0$, $\bold x \equiv \bold v$ and this equation describes a light ray with $\bold x_0 = 0$. Also, we have that straight line with a null $\equiv$ $|\bold w| = c$ 

**characteristic surface in space-time**: Its normal vector in 4 dimensions is a null vector

* Theorem 1 states that $S = \{(\bold x, t): t - \gamma(\bold x) = k\}$ ($k$ a constant) is characteristic $\equiv$ $|\nabla \gamma(\bold x)| = 1/c$ 
* Therefore for example: with $k = 0$, we have a normal vector is $ (\nabla \gamma(\bold x), -1)$ (**This confused me**) and this vector being null $\implies$ $1 = |v^0| = c|\bold v| = c|\nabla \gamma(\bold x)|$ $\iff$ $|\nabla \gamma(\bold x)| = 1/c$ so $S$ is characteristic 

**spacelike  surface**: all its normal vectors are timeline: $|\nabla \gamma(\bold x)| < 1/c​$ 

I suppose for any surface $S$: $(\nabla \gamma(\bold x), -1)$ is the normal vector 

**Spacelike surfaces are just the ones that naturally carry initial conditions** 

#### Theorem 2

If $S$ is any spacelike surface, then one can uniquely solve the initial-value problem
$$
(3) \qquad \begin{align} 
u_{tt}&= c^2 \Delta u && \text{in all of space-time}
\\ u &= \phi && \text{and} && \frac{\partial u}{\partial n} = \psi \text{ on S}
\end{align}
$$
where $\frac{\partial}{\partial n}$ indicates the derivative in the direction normal to $S$ 

If $S$ is represented as $\{t = \gamma(\bold x) \}$ the second initial condition in $(3)$ means: 
$$
(4) \qquad u_t - \nabla \gamma \cdot \nabla u = [1 + |\nabla \gamma|^2]^{1/2} \psi \qquad \text{for }t = \gamma(\bold x)
$$


### Singularities

#### Theorem 3

Characteristic surfaces are the **only surfaces** that can carry the singularities of solutions of the wave equation. 

**Idea**: information gets transported along light rays and a singularity is a very specific bit of information

**singularity** of a solution: any point where the solution or a derivative of it of some order is **not continuous** 

##### Example of Singularity: 

$$
(5) \quad \begin{align} u(\bold x, t) &= \frac{1}{2}v(\bold x, t)[t - \gamma(\bold x)]^2 && \text{for }\gamma(\bold x) \leq t
\\ u(\bold x, t) &= 0 && \text{for }\gamma(\bold x) \geq t
\end{align}
$$

where $v(\bold x, t)$ is a $C^2$ function, nonzero on the surface $S = \{t = \gamma(\bold x)\}$ 

and on the surface $S$, $u(\bold x, t)$ has obvious jump discontinuities 

**if $u$ solves the wave equation then the surface must be characteristic** 

i.e., since $u$ has singularities on $S$, acc. to theorem 3, if it solves the wave equation, $S$ must be characteristic. 

* First, assume $u$ solves the wave equation

* calculate $u_t, u_{tt}$ and $\nabla u$ and $\Delta u$ and set $0 = u_{tt} - c^2 \Delta u$ to get:
  $$
  (6)\quad 0 = v(1-c^2|\nabla|^2) + (2v_t + c^2v \Delta \gamma + 2c^2 \nabla v \cdot \nabla \gamma)(t- \gamma) + \frac{1}{2}(v_{tt}-c^2\Delta v)(t- \gamma)^2
  $$

* we focus on $u$ being a solution across the surface which leads to seeing that $(6)$ must be zero on $\{t - \gamma(\bold x) = 0 \}$ which means in $(6)$ we set $t = \gamma(\bold x)$ which means: 

  $v(1 - c^2|\nabla \gamma|^2) = 0$ $\implies$ $1 = c^2|\nabla \gamma|^2$ $\equiv |\nabla \gamma| = 1/c$ 

  $|\nabla \gamma| = 1/c$ is the **eikonal equation**: a nonlinear first order PDE satisfied by $\gamma$ 

  so for all $t$, $v(1 - c^2|\nabla \gamma|^2) = 0$, which means the right side of $(6)$ drops the $v(1- c^2|\nabla \gamma|^2)$ so we may divide the right side of $(6)$ by $(t - \gamma)$ for any $t$ so we obtain:
  $$
  (7) \qquad 0 = 2v_t + c^2  v\Delta \gamma + 2c^2 \nabla v \cdot \nabla \gamma + \frac{1}2(v_{tt}-c^2\Delta v)(t - \gamma)
  $$
  on one side of $S$. Again, matching across $S$, we must have $(7)$ valid when $t = \gamma(\bold x)$ $\implies$ the **transport equation**: 
  $$
  (8) \qquad v_t + c^2 \nabla \gamma \cdot \nabla v = -\frac{1}{2}c^2(\Delta \gamma)v
  $$
  linear first-order PDE satisfied by $v$ (I suppose $\gamma$ is given)

To Understand: $\mathscr{D} = \partial_t + c^2 \nabla \gamma \cdot \nabla$ is a derivative in a direction tangent to $S$ so $\mathscr{D}v = v_t + c^2 \nabla \gamma \cdot \nabla v = (v_t, \nabla v)\cdot (1, c^2\nabla \gamma) $ so this is going in direction of the ray $d\bold x/ dt = c^2 \nabla \gamma$  with $|d \bold x/dt| = c^2|\nabla \gamma|$ and by $(6)$: $= c$  

I think the tanget to $S$ is $(1, \nabla \gamma)$ is this because: $S = \{t - \gamma(\bold x) = k \}$ and focusing on only $U(\bold x, t) = t - \gamma(\bold x) $ gives (I'm not sure about this): $S_t = 1$… and uh $S_\bold x = \nabla \gamma (\bold x)$ i actually do not know. [this is helpful with all the tangents and normals to level surfaces][http://mathonline.wikidot.com/tangent-planes-to-level-surfaces] 

### Wave Equation with a Source

(this seems to be what was covered after 9.2 in lecture 23)

(I am mostly copying from Lecture 23)
$$
(9)\qquad u_{tt}-c^2\Delta u = f(\bold x, t) \qquad \text{ in }\R^3 \times (0, \infin)
\\ u(\bold x, 0) \equiv 0, u_t(\bold x, 0) \equiv 0 \qquad \text{ on }\R^3 \times \{t = 0 \}
$$

#### Duhamel's Principle

to solve
$$
(*) \qquad u_{tt} - Au = f \qquad t>0
\\ u, u_t = 0\qquad t = 0
$$
Let $v = v(t, s)​$ solve
$$
(**) \qquad v_{tt} - Av = 0 \qquad \text{for }t> s
\\ v = 0, v_t = f(s) \qquad \text{for }t = s
$$
Then: $u(t):= \int_0^t v(t, s)\, ds $ solves $(*)$ 

**Proof**: 
$u_t = \underset{= 0}{v(t, t)} + \int_0^t v_t(t, s)\, ds$ 
$u_{tt} = \underset{= f(t)}{v_t(t, t)} + \int_0^t v_{tt}(t, s)\, ds$ 
$Au = \int_0^t Av(t, s)\, ds$, so $u_{tt} - Au = f(t) + \int_0^t v_{tt}(t, s) - Av(t, s) \, ds \underset{(**) \implies }{=} \int_0^t 0 \, ds + f(t) = f(t)$

---

By Kirchhoff's Formula (9.2), the solution $v$ of 
$$
v_{tt} - c^2 \Delta v = 0 \qquad t > s
\\ v = 0, v_t = f(\bold x, s) \qquad t = s
$$
is $v(\bold x, t, s) = \frac{1}{4 \pi c^2(t-s)}\int_{\partial B_{c(t-s)}(\bold x)} f(\bold y, s) \, dS(\bold y)​$ 

in the book, we think of $v(\bold x_0, t_0, 0)$ as $(\mathscr{S}(t_0)\psi)(\bold x_0) = \frac{1}{4\pi c^2 t_0} \iint_S \psi (\bold y) \, d S_{\bold y}$ 
or specifically: $v(\bold x, t, s) = (\mathscr{S}(t - s) f)(\bold x)$)

Where $S = \partial B_{c(t-s)}(\bold x) = \{ \bold y: |\bold y - \bold x| = c(t-s)\}$ 

This is the **Duhamel formula**. 

I think, I am translating this from book page 246 to the notes from lecture 23. 

So Moving on: $u(\bold x, t) = \int_0^t v(\bold x, t, s)\, ds = \int_0^t \frac{1}{4 \pi c^2(t-s)}(\int_{\partial B_{c(t-s)}(\bold x)} f(\bold y, s) \, dS (\bold y))\, ds$ 

$\\ = \frac{1}{4\pi c^2} \int_0^t \int_{\partial B_{c(t-s)}(\bold x)} \frac{f(\bold y, s)}{t-s}\, dS(\bold y)\, ds$ 
change of variables: $r := c(t-s), dr = -c\, ds$, $t-s = r/c$, $s = t - r/c$ 
$ = \frac{1}{4 \pi c^2} \int_0^{ct}\int_{\partial B_{r}(\bold x)} \frac{f(\bold y, t - r/c)}{r/c} \, dS(\bold y)\, \frac{dr}{c}$ 

and this is simply a volum integral:  (note we are integrating $r$ from $r = 0$ to $r = ct$, this is equivalent to integrating over the solid ball of radius ct) and noting that the radius is equivalent to $|\bold y - \bold x|$: 
$$
u(\bold x, t) = \frac{1}{4 \pi c^2} \int_{B_{ct}(\bold x)} \frac{f(\bold y, t - \frac{|\bold y - \bold x|}{c})}{|\bold y - \bold x|}\, d\bold y
$$
this is the integral over the surface of the **backward light cone** 

Book: In order to solve $(9)$ the inhomogeneous wave equation with homogeneous initical conditions, you must multiply $f(\bold y, s)$ by the **potential** $1/(4 \pi c|\bold y - \bold x|)$ and integrate it over the **backward light cone**: 

* consists exactly of the domain of dependence of the given point $(\bold x, t)$ 
* The points that can reach $(\bold x, t)​$ via a light ray from some time $s​$ in the past $(0 \leq s \leq t)​$ 



## 9.4 The Diffusion and Schrodinger Equations

### Three-Dimensional Diffusion Equation

$$
\begin{align}
(1) && \frac{\partial u}{\partial t} = k \Delta u = k(\frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2} + \frac{\partial^2 u}{\partial z^2})
\\(2) && u(\bold x, 0) = \phi(\bold x)
\end{align}
$$

Use knowledge from chapter 2 to solve.

#### Theorem 1

For any bounded continuous function $\phi(\bold x)$, the solution of $(1), (2)$ is
$$
(3) \qquad u(\bold x, t) = \frac{1}{(4\pi kt)^{3/2}}\iiint \exp(-\frac{|\bold x - \bold x'|^2}{4kt})\phi(\bold x')d\bold x'
$$
for all $t > 0$. "dummy variables of integration": $\bold x' = (x', y', z')$ run over all of 3-space (in the one dimensional case, we used $y$ and $dy$, so $\bold x'$ and $d \bold x'$ is the 3-dim analog)

##### Proof 

**Denote**: $S(z, t) = \frac{1}{(4\pi kt)^{1/2}}e^{-z^2/4kt}$ 

* We know this equation: it is the **one**-dimensional source function.

Now we define:
$$
(4) \qquad S_3(x,y, z, t) = S(x,t)S(y,t)S(z,t)
$$
This is the product of three source functions in different variables

**Then**: find $\frac{\partial S_3}{\partial t}$ = $\frac{\partial S}{\partial t}(x, t)\cdot S(y, t)\cdot S(z,t)$ +(two similar terms, this is just product rule) 

$ = k\frac{\partial ^2 S}{\partial x^2}(x, t) \cdot S(y,t) \cdot S(z,t) + S(x, t) \cdot k \frac{\partial ^2 S}{\partial y^2}(y, t)\cdot S(z,t) + S(x, t)\cdot S(y, t) \cdot k \frac{\partial ^2 S}{\partial z^2}(z, t)$ $ = k( \frac{\partial^2}{\partial x^2}+ \frac{\partial ^2}{\partial y^2}+ \frac{\partial ^2}{\partial z^2})(S(x,t)S(y,t)S(z,t))$ 
$ = k \Delta S_3$ 

* This comes from knowing that $S_t(x, t ) = S_{xx}(x, t)$ and similalrly for the other variables

So we know: $S_3(\bold x, t)$ satisfies the three-dimensional diffusion equation. 

**Claim**: $S_3$ is the source function. 

To prove, first note: $(5) \qquad \iiint S_3(\bold x, t)\, d \bold x = (\int S(x, t)\, dx)(\int S(y, t)\, dy)(\int S(z, t)\, dz) = 1^3 = 1$ 

**Special case**: $\phi(x, y, z)​$ depends only on $z​$. Then we have:
$$
\lim_{t \rightarrow 0} \iiint S_3(\bold x - \bold x', t)\phi(z')d \bold x' = [\int S(x - x', t)dx']\cdot[S(y-y', t)dy']\cdot [\lim_{t \rightarrow 0}\int S(z-z', t)\phi(z')dz'] = 1 \cdot 1 \cdot \lim_{t \rightarrow 0}\int S(z-z', t)\phi(z')\, dz' = \phi(z)
$$
by Theorem 3.5.1 (one dimensional case on the integral with $z$ and $z'$ variable)

**Similalrly**: We can show
$$
(6) \qquad \lim_{t \rightarrow 0} \iiint S_3(\bold x - \bold x')\phi(\bold x')\, d \bold x' = \phi(\bold x)
$$
**if** $\phi(\bold x) = \phi(x)\psi(y)\zeta(z)$ 

**Therefore**: $(6)$ is also true of any linear combination of such products (since: $\iiint S_3 \cdot(a_1 \phi_1 + \cdots)) d\bold x' = a_1 \iiint S_3 \cdot \phi_1 \, d \bold x'+ \cdots$ )

**It can be deduced**: the same is true for **any** bounded continuous function $\phi(\bold x)​$ 

so $(5)$ and $(6)$ $\implies$ $S_3(\bold x, t)$ is the source function 

**Consequently**: the unique bounded solution of (1), (2):

$u(\bold x, t) = \iiint S_3(\bold x - \bold x', t)\phi(\bold x')\, d \bold x'$ , and writing this out explicitly gives $(3)$ 

### Schrodinger's Equation

$$
(8) \qquad -ihu_t = \frac{h^2}{2m}\Delta u + \frac{e^2}{r}u
$$

The potential $e^2/r$ is a variable coefficient 

##### Free Schrodinger equation

$$
(9) \qquad -i \frac{\partial u}{\partial t}= \frac{1}{2}\Delta u
$$

in 3 dimensions, with $h = m = 1$ and dropping the potential $e^2/r u$ term

**Remarks**: looks like the diffusion equation: $k = i/2$ (imaginary)

Also: because of the presence of $i$, the temporal factor has the form $T(t) = e^{i \lambda t} = \cos \lambda t + i \sin \lambda t$ (oscillatory)

**Solutions of (9) that tend to zero as $|\bold x| \rightarrow \infin$ ** 

Solution of $(9)$ with initial condition $u(\bold x, 0) = \phi(\bold x)$ 
$$
(10) \qquad u(\bold x, t) = \frac{1}{(2\pi it)^{3/2}}\iiint \exp( - \frac{|\bold x - \bold x'|^2}{2it}\phi(\bold x/)\, d \bold x')
$$

* can for mneumonic purposes think of this as taking $(3)$ the solution to the diffusion equation and pluggin in $k = i/2$ 

**Question**: Which square root for a complex number do we take in the first factor?

- Assume $\phi(\bold x')$ vanishes for large $|\bold x'|$ 

- Solve "nearby equation" $\frac{\partial u_\epsilon}{\partial t} = \frac{+ \epsilon + i}{2}\Delta u_{\epsilon}$ , $u_\epsilon(\bold x, 0) = \phi(\bold x)$ 

  - solution depends on the real number $\epsilon > 0$ 

- Diffusion equation solution with $k = (\epsilon + i)/2$: 
  $$
  (12) \qquad u_\epsilon(\bold x, t) = \frac{1}{(2\pi t)^{3/2}(\epsilon + i)^{3/2}}\iiint \exp[- \frac{|\bold x - \bold x'|^2}{2(\epsilon + i)t}]\phi(\bold x')\, d \bold x'
  $$
  - $(\epsilon + i)^{1/2}$ **denotes** the **unique** square root with the ***positive*** real part
    - Question: as opposed to what? the negative part? Why does this need to be mentioned?
    - Answer: It must be positive otherwise the exponent becomes positive and grows large as $|\bold x - \bold x'| \rightarrow \infin$ and we wouldn't have a bounded solution
  - $\epsilon > 0$ $\implies$ the exponent is negative, so the integral converges

- As $\epsilon \downarrow 0$: we obtain the solution for $(9)$ given by $(10)$ 

  - i.e. as $\epsilon \downarrow 0$, the nearby equation becomes $(9)$ and the solution to the nearby equation becomes $(10)$ where $i^{1/2}$ factor is the unique square root with the **positive real part**: we use $i^{1/2} = \frac{1 + i}{\sqrt{2}}$

**Different Method**: Separation of variables $u(\bold x, t) = T(t)X(\bold x)$ 

* Try one-dimensional case and obtain $(13) \qquad =2i \frac{T'}T = \frac{X''}X = -\lambda$ 
  * no solutions of $X'' + \lambda X = 0$ that satisfy required condition at infinity
* **This fails** (unless we use Fourier transform)

### Harmonic Oscillator

Harmonic oscillator equation in one-dimension
$$
(14) \qquad -iu_t = u_{xx} - x^2u \qquad (-\infin < x< \infin)
$$
**Let us require the condition**: 
$$
u \rightarrow 0 \qquad \text{ as }x \rightarrow \pm \infin
$$
**Separate variables** $u = T(t)v(x)$ 
$$
-i \frac{T'}{T}= \frac{v'' - x^2v}{v}= -\lambda
$$

* $\lambda$: energy of the harmonic oscillator
* $T = e^{-\lambda i t}$  since it must solve $T' = -i\lambda T$ 

**ODE for $v(x)$** 
$$
(15) \qquad v'' + (\lambda - x^2)v = 0
$$
Simples case: $\lambda = 1$ with $e^{-x^2/2}$ as solutions

Checking: $v'(x) = -xe^{-x^2/2} $ then $v''(x) = -e^{-x^2/2} + x^2 e^{-x^2/2} = -v + x^2 v = -(1 - x^2)v$ 

**For any $\lambda$**: attemp substitution

$v(x) = w(x)e^{-x^2/2}$ 

$\implies$ **an equation for $w$** (by plugging in $v(x) = we^{-x^2/2}$ into (15) )

$(x^2 - \lambda)e^{-x^2/2}w = [w''  -2xw + (x^2 - 1)w]e^{-x^2/2}$ $\implies$ 

**Hermite's differential equation**: 
$$
(16) \qquad w'' - 2xw' + (\lambda - 1)w = 0
$$
**Solve by the method of power series**: substitute the following into (16)

* $$
  (17) \qquad w(x) = a_0 + a_1 x + a_2 x^2 + \cdots
  $$

  To obtain $\sum_{k= 0}^\infin k(k-1)a_k x^{k-2} -  \sum_{k = 0}^\infin (2k - \lambda + 1)a_k x^{k} = 0$ 

**match like powers of $x​$** to obtain:

$2a_2 = (1- \lambda)a_0$ and $6a_3 = (3- \lambda)a_1$ 

In general: $(k+2)(k+1)a_{k+2} = (2k + 1 - \lambda)a_k$  for $k = 0, 1, 2, 3…$ 

This is a recurision formula and the first two coefficients may be arbitrary

if $a_0 = 0$ solution is odd. if $a_1 = 0$ solution is even

**Simple Case**: $\lambda = 2k + 1$ for some integer $k$, then $a_{k+2} = 0$, $a_{k + 4} = 0$ and so on…

It is called the Hermite polynomial $H_k(x)$ (with an appropriate normalization… which *I think* means letting either $a_0 = 0$ if $k$ is odd or $a_1 = 0$ if $k$ is even)

**Therefore**: for $\lambda = 2k + 1$, $w(x) = H_k(x)$ and $v_k(x) = H_k(x)e^{-x^2/2}$. These $v_k$ are separated solutions of $(15)$ 

The corresponding solutions of $(14)$: 
$$
u_k(x, t) = e^{-i(2k+1)t}H_k(x)e^{-x^2/2}
$$
for $k = 0, 1, 2, …$ 

**Note**: $u_k(x, t) \rightarrow 0$ as $x \rightarrow \pm \infin$ 

If $\lambda \neq 2k + 1$, no power series solution can satisfy the condition at infinity THEREFORE the only eigenvalues (energy level) ($\lambda$)are the positive odd integers

## 9.5 The Hydrogen Atom

$$
(1) \qquad iu_t = -\frac{1}{2}\Delta u - \frac{1}{r}u
$$

$e = m = h = 1$. $(1)$ is supposed to be satisfied in all of space $\bold x = (x, y, z)$. and $r = |\bold x|$ 

**Required**: (just because) **vanishing at infinity** 
$$
(2) \qquad \iiint |u(\bold x, t)|^2 \, d \bold x<\infin
$$
**We Separate Variables** 

Potential term leads to **eigenvalues** (like harmonic oscillator)

Writing $u(\bold x, t) = T(t)v(\bold x)$:
$$
2i \frac{T'}{T} = \frac{-\Delta v - \frac{2}{r}v}{v} = \lambda
$$
$\implies$ $u = v(\bold x )e^{-i \lambda t/2}$ ($T' = -\frac{i}{2}\lambda T)$ and we are left with:
$$
(3) \qquad - \Delta v - \frac{2}{r} v = \lambda v
$$
$\lambda$ - the energy of the bound state $u(\bold x, t)$ 

energy level of the electron in a hydrogen atom occur only at special values (related to squares of integers)

**Look for solutions of $(3)$ that are spherically symmetric**

$v(\bold x) = R(r)$. Using $\Delta$ in spherical (6.17)
$$
(4) \qquad - R_{rr} - \frac{2}{r}R_r - \frac{2}{r}R = \lambda R
$$
in $0 < r< \infin$ with the **condition at infinity** that
$$
(5) \qquad \int_0^\infin |R(r)|^2 r^2 \, dr < \infin
$$
It is also understood that: (this is what the book says rightafter listing $(5)$)

$(6) \qquad R(0)$ is finite

**This ODE is not easily solved**. 
**Change of variables** $\rightarrow$ $(4)$: **Laguerre's differential equation** 

It **turns out** that all the **eigenvalues $\lambda$ are negative** 

But for now **Assume** that $\lambda < 0$ 

**Change of Variables** 

Must note: if the second and third terms in $(4)$ were absent (this is true at infinity) the equation would be simple: $-R'' = \lambda R$ and this is easy to solve: $R = e^{\pm \beta r}$ where $\beta = \sqrt{-\lambda}$  

Since we are only interested in solutions vansihing at infinity we choose $R = e^{-\beta r}$ as an **approximation** to a solution of equation $(4)$ 

**Regardless**: we try the change of variables
$$
(7) \qquad w(r)= e^{+\beta r}R(r) \qquad \beta = \sqrt{-\lambda}
$$
Then: $R = we^{-\beta r}$, $R_r = w_r e^{-\beta r}-\beta w e^{-\beta r}$ and $R_{rr} = (w_{rr} - 2\beta w_r + \beta^2 w)e^{-\beta r}$ 

**This converts (4) to the equation** (after plugging in the above to $(4)$)
$$
(8) \qquad \frac{1}{2}rw_{rr}- \beta r w_r + w_r + (1- \beta)w = 0
$$
 we observe $r = 0$ is a regular singular point (See A.4)

**Power series method** (provides **some** solutions)

$w(r) = \sum_{k=0}^\infin a_k r^k = a_0 + a_1 r + a_2 r^2 + \cdots$ 

still have yet to determine the coefficients. 

**Substitute the power series into (8)** 

$\frac{1}{2} \sum_{r=0}^\infin k(k-1) a_k r^{k-1} - \beta \sum_{k=0}^\infin k a_k r^k + \sum_{k=0}^\infin ka_k r^{k-1} + (1 - \beta)\sum_{k=0}^\infin a_k r^k = 0$ 

Want things in $r^{k-1}$, so in the second and fourth term we switch $k$ to $k -1$: 

$\sum_{k=0}^\infin [ \frac{1}{2}k(k-1)  +k ]a_k r^{k-1}+ \sum_{k=1}^\infin [-\beta (k-1) + (1-\beta)]a_{k-1} r^{k-1} = 0$ 

**Each coefficient must vanish:** 
$$
(9) \qquad \frac{k(k+1)}{2}a_k = (\beta k - 1)a_{k-1} \qquad (k = 1, 2, ...).
$$
$\implies$ 
$$
\begin{align}
a_1 &= (\beta - 1)a_0 & 3a_2 &= (2\beta - 1)a_1
\\ 6a_3 &= (3\beta - 1)a_2 & 10a_4 &= (4\beta - 1)a_3
\\ 15a_5 &= (5\beta - 1)a_4 & 21a_6 &= (6\beta- 1)a_5
& \text{etc. } \end{align}
$$
$\implies$ if $\beta$ happens to be the **reciprocal of a positive integer**, the sequence of coefficients terminates and we have a **polynomial solution** of $(8)$ 

i.e., $w$ is a polynomial solution in the case that $\beta = 1/n$ where $n \in \N$ (of degree $n-1$?)

$\implies$ $v(\bold x) = R(r) = w(r)e^{-\beta r}$ $= \text{polynomial }\times \text{decaying exponential}$  

$\implies$ $v \underset{r \rightarrow \infin}{\rightarrow }0$ so **the condition at infinitiy (2) is satisfied** 

![image-20190331032759167](Math 126 Book Notes Section 9.assets/image-20190331032759167.png)

The lowest energy state (ground state) $v(\bold x) = e^{-r}$ drops off exponentially with the distance from the proton and vanishes nowhere ($v(\bold x) \neq 0$ for all $\bold x$ )

The second state corresponds to $n = 2$ and vanishes for a single value of $r$ (has one **node**). 

**The nth state has $(n-1)$ nodes**: Its energy is $\lambda = -\beta^2 = -1/n^2$ (since $\beta = 1/n = \sqrt{-\lambda}$) 

$\implies$ lowest possible energies: $-1, -1/4, -1/9$ ...

### Other solutions

For $\beta = 1/n$, there is, of course, another linearly independent solution of the second-order ODE $(8)$ 

**However**: that solution is singular at $r = 0$ 

**What happens when** $\beta \neq 1/n$? Then: the factor $(\beta k - 1)$ never vanishes, so that the recurions relation $(9)$ looks approximately like $(k^2/2)a_k \sim (\beta k)a_{k-1}$ for large $k$ **or** like $a_k \sim (2\beta/k)a_{k-1}$ (by dividing both sides by $k^2/2$)

**These are the coefficients in the taylor expansin of $e^{2\beta r}$ ** ($a_k = \frac{(2\beta)^k}{k!} = \frac{2\beta}{k}a_{k-1}$) 

So $R(r)$ looks **approximately** like: $e^{-\beta r} e^{+2\beta r} = e^{+\beta r}$ 

Such a solution would **not** satisfy the condition at inifinity $(2)$ since $e^{+\beta r} \rightarrow \infin$ as $r = |\bold x|\rightarrow \infin$ 

The **only** eigenvalues are $\lambda = 1/n$ for $n \in \N$ 

**Are the eigenfunctions complete?**: No:

1. There are plenty of eigenfunctions that possess angular dependence (spin)

2. There is plenty of **continuous spectrum** as a consequence of $D = $ all of space, rather than a bounded part of it

   1. continuous spectrem corresponds to "unbound states" which are scattered by the potential

   


# Math 126 Book Notes Section 14.2

## Solitons

**Definition**: a soliton is a localized traveling wave solution of a nonlinear PDE that is remarkably stable

One PDE that has such a solution: **Korteweg - DeVries** (KdV) eauation
$$
(1) \qquad u_t + u_{xxx} + 6u u_x = 0 \qquad(-\infin < x<\infin)
$$
The "six" is of no special significance

It has been known for a century that this equation describes water waves along a canal. 

##### Three fundamental Quantities

$$
\text{mass} = \int_{-\infin}^\infin u\, dx
\\ \text{momentum} = \int_{-\infin}^\infin u^2\, dx
\\ \text{energy} = \int_{-\infin}^\infin (\frac{1}{2}u_x^2 - u^3)\, dx
$$

each of which is a constant of motion (an invariant). There are an **infinite** number of other invariants involving higher derivatives 

Let's look for a **traveling wave solution** of (1)
$$
u(x,t) = f(x-ct)
$$
We get the ODE: $-cf' + f''' + 6ff' = 0$ 

Integrating it once: $-cf + f'' + 3f^2 = a$, where $a$ is a constant

Multiplying it by $2f'$ and integrating again leads to:
$$
-2cff' + 2f'f'' + 6f'f^2 = 2af'
$$

$$
(2) \qquad -cf^2 + (f')^2 + 2f^3 = 2af + b
$$

Where $b$ is another constant 

We are looking for a solution that is solitary like Russell's, meaning that away from the heap of water, there is no elevation

$f(x), f'(x)$, and $f''(x)$ should tend to zero as $x \rightarrow \pm \infin$ 

$\implies$$ a = b = 0$ so that $-cf^2 + (f')^2 + 2f^3 = 0$. The solution of this first-order ODE is
$$
(3) \qquad f(x) = \frac{1}{2}c \text{ sech}^2 = [\frac{1}{2}\sqrt{c}(x-x_0)]
$$
where $x_0$ is the constant of integration and sech is the hyperbolic secant: $\text{sech }x = 2/(e^x + e^{-x})$ (see Exercise 3). It decays to zero exponentially as $x \rightarrow \pm \infin$ 

With this function $f$, $u(x,t) = f(x-ct)$ is the soliton 

It travels to the right at speed $c$. Its amplitude is $c/2$. There is a soliton for every $c> 0$. it is tall, thin, and fast if $c$ is large, and is short, fat, and slow if $c$ is small

##### Stability of the Soliton

If we start with two solitons, the faster one will overtake the slower one and, after a complciated nonlinear interaction, the two solitons will emerge uncathed as they move to the right, except for a slight delay. 

every solution of (1), with any initial function $u(x,0) = \phi(x)$ seems to decmopose as $t \rightarrow + \infin$ into a finite number of solitons (of various speeds $c$) plus a **dispersive tail** which gradually disappears

### Inverse Scattering

Explanation of soliton stability: **inverse scattering** analysis

Associated linear equation with $(1)$: 
$$
(4) \qquad - \psi_{xx} - u \psi = \lambda \psi
$$
with the parameter $\lambda$ and the "potential function" $u = u(x,t)$ 

In $(4)$ the time value is regarded as another parameter, so we are dealing with a family of potentials that are functions of $x$ depending on the parameter $t$ 

If $\lambda(t)$ is an eigenvalue and $\psi(x, t)$ its eigenfunction that satifies $(4)$, let's substittute 
$$
(5) \qquad u = -\lambda - \frac{\psi_{xx}}{\psi}
$$
into (1) to get
$$
(6) \qquad \lambda_t \psi^2 + (\psi h_x - \psi_x h)_x = 0
$$
where
$$
h = \psi_t - 2(-u + 2\lambda)\psi_x - u_x \psi
$$
Normalizing: $\int_{-\infin}^\infin |\psi|^2 \, dx = 1$ 

Integrating (6), **Confused:** we then get (?)
$$
\lambda_t \int \psi^2\, dx + \psi h_x - \psi_x h= 0
$$

---

Because $(6)$ implies:
$$
\lambda_t  = \frac{(\psi h_x- \psi_x h)_x}{\psi^2}
$$
so since the left side… only depends on $t$, the right side should only depend on $t$ ???

or...
$$
\lambda_t \psi^2 = (\psi h_x - \psi_x h)_x
\\ \lambda_t \int \psi^2 = \psi h_x - \psi_x h
\\ \lambda_t = [\psi h_x - \psi_x h]_{x = -\infin}^\infin = 0?
$$
$h_x = \psi_{tx} +2u_x - 2(-u + 2\lambda)\psi_{xx} - u_{xx}\psi - u_x \psi_x$ 

$\psi h_x = \psi \psi_{tx} + 2 \psi u_x - 2(-u + 2\lambda)\psi_{xx}\psi - u_{xx}\psi^2 - u_x \psi_x \psi$ 

$\psi_x h = \psi \psi_t -2(-u + 2\lambda)\psi_x^2 - u_x \psi \psi_x$ 
$\psi h_x - \psi_x h$ $ = \psi \psi_{tx} - \psi \psi_t + 2 \psi u_x - 2(-u + 2\lambda)\psi_{xx}\psi - u_{xx}\psi^2 - \psi \psi_t + 2(-u + 2\lambda)\psi_x^2 $ 

I guess I'll continue this later..

---

I have no clue how it leads to:
$$
\lambda_t = 0
$$
so that $\lambda​$ is a constant 

**If $u​$ solves (1) and $\lambda​$ is an eigenvalue of (4), then $\lambda​$ doesn't depend on $t​$** 

Thus each eigenvalue provides another constant of the motion. 

Denote all the discrete spectrum of (4) by:
$$
\lambda_n \leq \lambda_{N-1} \leq \cdots \leq \lambda_1 < 0
$$
where $\lambda_n = \kappa_n^2$ has the eigenfunction $\psi_n(x,t)$. The limiting behavior
$$
\psi_n(x,t)\sim c_n(t)e^{-\kappa_n x}\qquad \text{as }x \rightarrow +\infin
$$
can be proven from (4) (see [AS] or [Ne]). The $c_n(t)$ are called the **normalizing constants** 

As we know, a complete analysis of (4) involves the continuous spectrum as well 

So let $\lambda = k^2 > 0$ be a fixed positive number. From Section 13.4 we know that there is a solution of (4) with the asymptotic behavior
$$
(7) \qquad \begin{align}\psi(x)&\sim e^{-ikx}+ Re^{+ikx} & \text{as }x \rightarrow + \infin 
\\ \psi(x)&\sim Te^{-ikx} &\text{as }x \rightarrow - \infin
\end{align}
$$
Here the reflection coefficient $R$ and the transmission coefiicient $T$ may dependon both $k$ and the "parameter" $t$ 

#### Theorem 1.

$\partial T/\partial t \equiv 0$, $\partial R/\partial t = 8ik^3 R$, and $dc_n/dt = 4\kappa_n^3 c_n$ 

$\implies$ 
$$
T(k,t) = T(k,0)\qquad R(k, t) = R(k, 0)e^{8ik^3t}\qquad c_n(t)= c_n(0)e^{4\kappa_n^3 t}
$$

##### Proof. 

**First**, $\psi h_x - \psi_x \equiv 0$ 

Identity (6) is also true for any $\lambda > 0$ 

But $\lambda$ is a fixed constant, so $(6)$ says that $\psi h_x - \psi_x h$ depends only on $t$ 

because: $0 = (\psi h_x - \psi_x h)/\psi^2$ 

so $\psi h_x - \psi _x h = $ its asymptotic value as $x \rightarrow - \infin$, which is the sum of several terms. 

From the formula for $h$, the fact that $u(x,t) \rightarrow 0$, and from the asymptotic expression (7) for $\psi(x,t)$, all of these terms cancel

Hence, $\psi h_x - \psi_x h \equiv 0$ 

**Second**, 

dividing the last result by $\psi^2$, it follows that the quotient $h/\psi$ is a function of $t$ only

because: 

$(h/\psi)_x = \frac{h_x\psi - h\psi_x}{\psi^2} = \frac{-0}{\psi^2} = 0$

So $h/\psi$ also equals its asymptotic value as $x \rightarrow +\infin$ 

That is:
$$
\begin{align}
\frac{h}{\psi} &= \frac{\psi_t - 2(-u + 2\lambda)\psi_x - u_x \psi}{\psi}
\\ &\sim \frac{R_t e^{ikx}- 4\lambda(-ike^{-ikx}+ Rike^{ikx})-0}{e^{-ikx}+Re^{ikx}}
\\ &= \frac{[R_t - 4ik\lambda R]e^{ikx}+[4ik\lambda]e^{-ikx}}{Re^{ikx}+e^{-ikx}}
\end{align}
$$
For this expression to be independent of $x$, the numerator and denominator must be linearly dependent as functions of $x$ 

$[c_1(R_t - 4ik\lambda R) + c_2 R]e^{ikx} + [c_1 4ik\lambda + c_2]e^{-ikx} = 0$ 

$c_1(R_t - 4ik\lambda R) = -c_2 R$ $\implies$ $-\frac{c_2}{c_1} = \frac{R_t - 4ik\lambda R}{R}$ 
$c_1 4ik\lambda = - c_2 $ $\implies$ $-\frac{c_2}{c_1} = 4ik\lambda$ 

Therefore:
$$
\frac{R_t - 4ik\lambda R}{R} = \frac{4ik \lambda }{1}
$$
$\implies$ $R_t = 8ik\lambda R = 8ik^3R$, proving the second part of the theorem. The first and third parts are left for Exercise 8. 

### Solution of the KdV

The scattering theory of the associated Schrödinger equation leads to the resolution of the KdV equation with an initial condition $u(x,0) = \phi(x)$ 

Schematically, the method:
$$
(8) \qquad \begin{align}
&\rightarrow \text{ scattering data at time }0
\\ &\rightarrow \text{ scattering data at time }t
\\ &\rightarrow u(x,t)
\end{align}
$$
By **scattering data** we mean the refelction and transmission coefficients, the eigenvalues, and the normalizing constants

the first arrow: the **direct scattering problem**, finding the scattering data of a given potential

second arrow: trivial due to theorem above

third arrow: **inverse scattering problem**, finding the potential with given scattering data.: difficult

#### 2 Examples

##### Example 1

Suppose: $u(x,0)$ to be the initial data of a single soliton:

$u(x,0) = 2 \text{sech}^2 x$ 

unique solition: $u(x,t) = 2 \text{sech}^2(x - 4t)$ which is (3) with $c = 4$ 

It turns out that in this case: $R(k,t) \equiv 0$ and there is exactly one negative eigenvalue of the Schrödinger operator (4)

---

All of the cases with vanishing reflection coefficient can be explicitly computed 
If there are $N$ negative eigenvalues of (4), one obtains a complicated but explicit solution (1) which as $t \rightarrow \pm \infin$ resolves into $N$ distinct solitons

This solution is called an **N-soliton** 

##### Example 2

Let $\phi(x) = 6 \text{sech}^2 x$. Then it turns out that $R(k,t) \equiv 0$, $N = 2$ 

$\lambda_1 = -1$, and $\lambda_2 = -4$. The solution of (1) is the 2-soliton given by the formula
$$
u(x,t) = \frac{12[3 + 4\cosh(2x-8t)+\cosh(4x-64t)]}{[3\cosh(x-28t)+\cosh(3x-36t)]^2}
$$
see figure 3:

![image-20190423193209946](Math 126 Book Notes Section 14.2.assets/image-20190423193209946.png)

It looks asymptotically like a pair of single solitons of amplitudes 8 and 2 which are interacting. There is a **phase shift**, which means they are slightly shifted from what they would have been if they were not interacting 
# Math 126 Book Notes Section 7

**Three Dimensions**: 

Recall: Divergence Theorem: $\int_{D} \nabla \cdot \vec F \; d \vec x = \int_{\partial D} \vec F \cdot \vec n \; dS$ 

## Green's First Identity

### Quick Review of Multivariable Calc

Recalling: $\text{grad }f = \nabla f = (f_x, f_y, f_z)$ 
Consider $\vec F = (F_1, F_2, F_3)$:
$\text{div } \vec F = \nabla \cdot \vec F = \frac{\partial F_1}{\partial x} + \frac{\partial F_2}{\partial y} + \frac{\partial F_3}{\partial z}$ 

$\Delta u = \text{div grad } u = \nabla \cdot \nabla u = u_{xx} + u_{yy} + u_{zz}$  
$|\nabla u|^2= |\text{grad } u|^2 = u_x^2 + u_y^2 + u_z^2$ 

**everything (almost) will be written for three-dimensional case** 
$$
\iiint_D \cdots \; d\bold x = \iiint_D \cdots dx\, dy\, dz
$$
Surface Integral indicated by $dS$: 
$$
\iint_{\partial D} \cdots dS = \iint_S \cdots dS
$$
where $S = \partial D$, the bounding surface for the solid region $D$. 
$$
(1)\qquad \iiint_D \text{div }\bold F \, d\bold x = \iint_{\partial D} \bold F \cdot \bold n \; dS
$$
$\bold F$: any vector function, $D$: bounded solid region, $\bold n$: unit outer normal on $\partial D$. 

### Green's First Identity

$(vu_x)_x = v_x u_x + vu_{xx}$ (sim for $y, z$ derivatives) and $(vu_x)_x + (vu_y)_y + (vu_z)_z = \nabla\cdot(v \nabla u) $$\implies$
$$
\nabla \cdot (v \nabla u) = \nabla v \cdot\nabla u+v\Delta u
$$
**Integrate** and use **Divergence Theorem** on the left side:
$$
(\text G1) \qquad \iint_{\partial D} v \frac{\partial u}{\partial n}\, dS = \iiint_{D}\nabla v \cdot \nabla u \; d\bold x + \iiint_D v\Delta u \; d\bold x
$$
remembering: $\partial u/\partial n = \bold n \cdot \nabla u$ 

$(\text G 1)$ is valid for any solid region $D$ and any pair of functions $u$ and $v$. 

---

**Take $v \equiv 1$** to get
$$
(2)\qquad \iint_{\partial D}\frac{\partial u}{\partial n} = \iiint_D \Delta u \; d\bold x
$$

#### Neumann Problem in any domain $D$ 

$$
(3) \qquad \begin{cases}
\Delta u = f(x) & \text{in }D
\\ \frac{\partial u}{\partial n} = h(x) & \text{ on }\partial D
\end{cases}
$$

Using $(2)$: 
$$
(4) \qquad \iint_{\partial D}h \; dS = \iiint_D f\; d\bold x
$$
($f$ and $h$) are *not* arbitrary, but are required to satisfy condition $(4)$, Otherwise, **there is no solution** $\implies$ not well-posed

If there is a solution, could add any constant to any solution of $(3)$ and still get a solution $\implies$ **no uniqueness** 

Problem $(3)$: Lacks uniqueness as well as existence (guarantee of existence)

### Mean Value Property

The average value of any harmonic function over any sphere equals its value at the center

#### Proof

$D$: ball, $\{|\bold x| < a\}$ $\equiv$ $\{ x^2 + y^2 + z^2 < a^2 \}$ 
$\implies$ $\partial D$ is the sphere/surface $\{|\bold x | = a \}$. 

Assume: $\Delta u = 0$ in any region containing $D$ and $\partial D$ 

for a sphere, $\bold n$ points directly away from origin:
$$
\frac{\partial u}{\partial n} = \bold n \cdot \nabla u = \frac{\bold x}{r} \cdot \nabla u = \frac{x}{r}u_x + \frac{y}{r}u_y + \frac{z}{r}u_z = \frac{\partial u}{\partial r}
$$
$\frac{\partial r}{\partial x} = \frac{x}{r}$ , $(\frac{\partial r}{\partial x}, \frac{\partial r}{\partial  y}, \frac{\partial r}{\partial z}) \cdot \nabla u$ $ = \nabla r \cdot \nabla u$ 

$r_x = (\sqrt{x^2 + y^2 +z^2})_x = \frac{1}{2}\frac{2x}{(x^2 + y^2 + z^2)^{1/2}}$  

$\frac{\partial u}{\partial n} = \nabla u \cdot \frac{\bold x}{r} = \frac{x}{r} u_x + \frac{y}{r} u_y + \frac{z}{r} u_z$ 

$u(x,y,z) = u(r \cos \phi \cos \theta, r \cos \phi \sin \theta, r \sin \phi)$ 

$u_r = u_x \cos \phi \cos \theta + u_y \cos \phi \sin \theta + u_z \sin \phi$ 

$(2)$ now becomes:
$$
\iint _{\partial D}\frac{\partial u}{\partial r}\; dS = 0
$$
Since $\Delta u = 0$ 

**Write in Spherical Coordinates**: $(r, \theta, \phi)$ 
$$
\int_0^{2\pi}\int_0^\pi u_r(a, \theta, \phi)a^2 \sin \theta\, d\theta d\phi = 0
$$
since $r = a$ on $\partial D$ 

**Divide by $4 \pi a^2$, the area of $\partial D$** (think of $a$ as a variable and call it $r$, where $a > 0$). Pull out $\frac{\partial }{\partial r}$ 
$$
\frac{\partial}{\partial r}[\frac{1}{4\pi}\int_0^{2\pi}\int_0^{\pi}u(r, \theta, \phi) \sin \theta, d\theta\, d\phi] = 0
$$
Thus:
$$
\frac{1}{4\pi}\int_0^{2\pi}\int_0^{\pi}u(r, \theta, \phi)\sin\theta\, d\theta\, d\phi
$$
is independent of $r$. And the above expression it the average value of $u$ on the sphere $\{|\bold x | = r\}$ 

Letting $r \rightarrow 0$: 
$$
\frac{1}{4\pi}\int_0^{2\pi}\int_0^\pi u(\bold 0) \sin \theta \, d\theta \, d\phi = u(\bold 0)
$$
So:
$$
\frac{1}{\text{area of }S}\iint_S u\, dS = u(\bold 0)
$$
in 2 dimensions: $u$ is harmonic $\implies$ $u_{xx} + u_{yy} = 0$ and if we use green's theorem with $P = -u_y$ and $Q = u_x$… etc. 

### Maximum Principle

*if $D$ is any solid region, a nonconstant harmonic function in $D$ cannot take its maximum value inside $D$, but only on $\partial D$* 

Proof: From mean value property just like in 6.3, i.e., prove that if the max value is inside $D$, the function is constant: draw small spheres, starting with one centered on some point in $D$ $x_{M}$ 

Also: $\partial u/\partial n$ is strictly positive at a maximum point (Hopf maximum principle)

### Uniqueness of Dirichlet's Problem

Other proof: Section 6.1, using maximum principle, ah simply use $w = v - u$, so $w$ satisfies $\Delta w = 0$, and homogeneous BCs, so that since the max value of $w$ must take place on boundary, which is $0$, and similarly the min vlue of $w$ must take place on the boundary, which is $0$, we have $w \equiv 0$ 

#### Proof by Energy Method

Take two harmonic functions $u_1, u_2$ with same boundary data. $u = u_1 - u_2$ is harmonic and has zero boundary data

**Use $(\text G 1)$** and substitute $v = u$ and note $\Delta u =0$ due to harmonicity
$$
(7) \qquad \iint _{\partial D}u \frac{\partial u}{\partial n}\, dS = \iiint_D |\nabla u|^2 \; d\bold x
$$
since $u = 0$ on the boundary of $D$, the left side of $(7)$ vanishes. $\implies$ $\iiint_D |\nabla u|^2 d\bold x =0$ 

**First Vanishing Theorem**: $|\nabla u|^2 \equiv 0$ in $D$ $\implies \nabla u = 0$ 

and if $D$ is connected $\implies$ $u(\bold x) \equiv C$ throughout $D$, $C$ a constant

**but**: $u$ vanishes somehwere on $\partial D$ (we must have continuity I suppose?) $\implies$ $C = 0$ so $u \equiv 0$ 

$\implies$ $u_1 = u_2$ 

### Dirchlet's Principle

Among all the functions $w(\bold x)$ in $D$ that satisfy the Dirichlet boundary condition $(8): w = h(\bold x)$ on $\partial D$, the **lowest energy** occurs for the **harmonic** function satisfying $(8)$. 

##### Energy Definition

$$
(9) \qquad E[w] = \frac{1}2 \iiint_D |\nabla w|^2 d\bold x
$$

Pure Potential Energy, no kinetic energy b/c there is no motion

Physics: Any system prefers to go to the state of lowest energy: **ground state**, thus: the harmonic function is the preferred physical stationary state

#### Mathematic Statement of Dirichlet's Principle

$u(\bold x)$ is the unique harmonic function in $D$ satisfying $(8)$ $\iff$ 
$$
(10) \qquad E[w] \geq E[u], \text{ for all }w: w \text{ satisfies }(8)
$$

##### Proof: $(\implies)$ 

$v = u - w​$, then **expand the square in the integral** 
$$
(11) \qquad E[w] = \frac{1}{2}\iiint_D |\nabla(u-v)|^2 d\bold x = E[u] - \iiint_D \nabla u \cdot \nabla v d \bold x + E[v]
$$
$\nabla(u - v) = (u_x - v_x, u_y - v_y, u_z - v_z)​$ 

$|\nabla (u - v)|^2 = (u_x^2 - 2u_x v_x + v_x^2, u_y^2- 2 u_yv_y +v_y^2, u_z^2 - 2u_z v_z + v_z^2)​$ $ = |\nabla u|^2 - 2\nabla u \cdot \nabla v + |\nabla v|^2​$ 

**Apply $(\text G 1)​$** to $u​$ and $v​$, noting $v = 0​$ on $\partial D​$ and $\Delta u = 0​$ $\implies​$ the middle term in $(11)​$ is zero

**Therefore**: 
$$
E[w]= E[u] + E[v]
$$
And obviously: $E[v] \geq 0​$ $\implies​$ $E[w] \geq E[u]​$, so energy is the smallest when $w = u​$. 

---

##### Alt Proof $(\impliedby)$ 

Let $u(\bold x)​$ be any fct. satisfies $(8)​$ and minimizes energy $(9)​$. Let $v(\bold x)​$ be any fct. that vanishes on $\partial D​$ $\implies​$ $u + \epsilon v​$ satisfies $(8)​$ 
$$
(12) \qquad E[u] \leq E[u+\epsilon v] = E[u] - \epsilon \iiint_D \Delta u\,  v \, d\bold x + \epsilon^2 E[v]
$$
where we used $(\text G 1)$ and the vact that $v$ vanishes on $\partial D$ to have $-\iiint_D v \Delta u \, d \bold x = \iiint \nabla v \cdot \nabla u \, d \bold x$ 

And $(12)$ is true for any constant $\epsilon$. When $\epsilon = 0$, we must have the minimum energy

$e(\epsilon) := $ rightside of $(12)$ $\implies$ critical point at $\epsilon = 0$: $e '(0) = -\iiint_D \Delta u \, v d \bold x  = 0$ so $\iiint_D \Delta u \, v d \bold x = 0$ 

This is valid for practically *all* functions $v$ in $D$, since the expression on the rightside of $(12)$ for $E[u + \epsilon v]$ and with that the function $e(\epsilon)$ do not depend on $v$ vanishing on $\partial D$ 

Let $D'$ be any strinct subdomain of $D$: $\overline{D'} \sub D$. Let $v(\bold x) \equiv 1$ for $\bold x \in D'$ and $v(\bold x) \equiv 0$ elsewhere in $D$ …(approximation argument is omitted here)

$(13)$ becomes $\iiint_{D'} \Delta u \, d \bold x = 0$ for all $D'$ in $D$ 

**Second vanishing theorem**: $\Delta u =0$, so $u$ is harmonic. 

By uniqueness, it is the only function satisfying $(8)$ that can minimize energy

## Green's Second Identity

higher-dimensional version of $5.3.3$ 

basic representation formula for harmonic functions. 

Take $(\text G 1)$ and apply to $u$ and $v$ and then $v$ and $u$. The middle term that dots the gradient of $v$ and the gradient of $u$ stays the same in either one, so if we were to subtract:
$$
(\text G 2) \qquad \iiint_D (u \Delta v - v\Delta u)d\bold x = \iint_{\partial D}(u \frac{\partial v}{\partial n} - v \frac{\partial u}{\partial n})\; dS
$$
**valid for any functions** $u, v$ 

remembering $(\text G 1)$ is based on $\nabla \cdot (v \nabla u) = \nabla v \nabla u + v\Delta u$ 

### Representation Formula

For any harmonic function $u$, so if $\Delta u =0 $ in $D$ then:
$$
(1) \qquad u(\bold x_0) = \iint_{\partial D}[-u(\bold x)\frac{\partial}{\partial n}(\frac{1}{|\bold x - \bold x_0|}) + \frac{1}{|\bold x - \bold x_0|}\frac{\partial u}{\partial n}]\frac{dS}{4\pi}
$$

##### **Proof** - refer to book for details

1. Right side of $(1)$ 
   1. Apply $(\text G 2)$ with $v(\bold x) = (-4\pi |\bold x - \bold x_0|)^{-1}$ 
   2. Easy to see right sides agree:
      1. $\Delta u = 0$ and $\Delta v = 0$ (Poisson kernel, but multiplied by a constant?)
2. Left side: $v(\bold x)$ is infinite at $\bold x_0$ 
   1. small ball around $\bold x_0$: $D_\epsilon = D\setminus B(\bold x_0, \epsilon)$ 
   2. Simplicity: $\bold x_0$ is $\bold 0$ 
   3. apply $(\text G2)$ with this choice of $v$ in $D_\epsilon$ 
      1. Break into the two parts of $D_\epsilon$: $D$ and $\{ r = \epsilon \}$ 
   4. Need to show surface integral on $\{r = \epsilon \}$ tends to $4\pi u(\bold 0)$ as $\epsilon \rightarrow 0$  
   5. Use $(\frac{1}{r})_r = -\frac{1}{r^2}$ and remember that $r = \epsilon$ on the surface and plug this into the surface integral on $\{r = \epsilon \}$ to obtain two integrals: one over $u$ and one over $u_r$ 
   6. Use the fact that these are $area$$\cdot$ average values to express in terms of the average value of $u$ on the sphere and the average value of $u_r​$ on the sphere
   7. Take limit as $\epsilon \rightarrow 0$ and note that $u$ is continuous and $u_r$ is bounded

#### Two Dimensions

$$
u(\bold x_0) = \frac{1}{2\pi}\int_{\partial D}[u(\bold x)\frac{\partial }{\partial n}(\log|\bold x - \bold x_0|) - \frac{\partial u}{\partial n}\log|\bold x - \bold x_0|]\, ds
$$

whenever $\Delta u = 0$ in a plane domain $D$ and $\bold x_0$ is a point within $D$. The right side is a line integral over the boundary curve w.r.t. to arc length. and $\log = \ln$ in this notation and $ds$ is the arc length on the bounding curve

**7.3** and some of **7.4** will be done in Lecture 19
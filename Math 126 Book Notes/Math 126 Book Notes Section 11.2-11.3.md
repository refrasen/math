# Math 126 Book Notes Section 11.2-11.3

## 11.2

Beginning with the first eigenvalue, we know that
$$
\lambda_1 \leq \frac{\| \nabla w\|^2}{\| w \|^2}
$$
For all $w$ vanishing on the boundary

A good choice of trial function $w = v_1$ should give equality, but since we don't know $v_1$ in advance, we should be satisfied with a moderately clever choice that might provide a moderately good approximation

#### Example 1

Finding the first eigenvalue of $-u'' = \lambda u$ on the interval $0 \leq x \leq l$ with the Dirichlet BC

$u(0) = u(l) = 0$, assuming that we didn't already know the correct answer

$\lambda_1 = \frac{\pi^2}{l^2}$ 

**Simple Trial Function**: $w(x) = x(l-x)$. This satisfies the boundary conditions

The Rayleight quotient:
$$
(2) \qquad \frac{\|w'\|^2}{\| w \|^2} = \frac{\int_0^l (l-2x)^2 \, dx}{\int_0^l x^2(l-x)^2 dx} = \frac{10}{l^2}
$$
This is an amazingly good approximation to the exact value $\lambda_1$ 

It is actuallly difficult to guess a good trial function.

### Rayleigh-Ritz Approximation

Let $w_1, \ldots, w_n$ be $n$ arbitrary trial functions ($C^2$ functions that vanish on $\partial D$). Let
$$
a_{jk} = (\nabla w_j, \nabla w_k) = \iiint_{D}\nabla w_j \cdot \nabla w_k \, d \bold x
$$
and
$$
b_{jk} = (w_j, w_k) = \iiint_D w_j \cdot w_k \, d \bold x
$$
Let $A$ be the $n \times n$ symmetric matrix $(a_{jk})$ and $B$ the $n \times n$ symmetric matrix $(b_{jk})$. Then *the roots of the polynomial equation* 
$$
\det(A - \lambda B) = 0
$$
are approximations to the first $n$ eigenvalues $\lambda_1, \ldots \lambda_n$ 

#### Example 2

Radial vibrations of a circular membrane (disk) of radius 1:
$$
(4) \qquad -\Delta u = -u_{rr} - \frac{1}{r}u_r = \lambda u \quad (0 < r< 1), u = 0 \text{ at }r = 1
$$
Then $-(ru_r)_r = \lambda r u$ and the Rayliegh quotient is
$$
(5) \qquad Q = \frac{\iint |\nabla u|^2\, d\bold x}{\iint u^2 \, d \bold x} = \frac{\int_0^1 r u_r^2 \, dr}{\int_0^1 ru^2 dr}
$$
Trial functions are required to satisfy the  Boundary conditions $u_r(0) = 0 = u(1)$ 

$1-r^2$, and $(1-r^2)^2$ for trial functions. Then

$-2r$, $-4r(1-r^2)$ 
$$
\frac{A}{2\pi} = \begin{pmatrix}\int_0^1 4 r^2r\, dr & \int_0^1 8r^2(1-r^2)r\, dr \\ \int_0^18r^2(1-r^2)r\, dr & \int_0^1 16r^2(1-r^2)^2 r \, dr \end{pmatrix} = \begin{pmatrix} 1 & \frac{2}{3} \\ \frac{2}{3} & \frac{2}{3} \end{pmatrix}
$$

$$
\frac{B}{2\pi} = \begin{pmatrix}\int_0^1 (1-r^2)^2 dr & \int_0^1 (1-r^2)^3 r\, dr \\ \int_0^1(1-r^2)^3 r\, dr  & \int_0^1 (1-r^2)^4 r \, dr \end{pmatrix} = \begin{pmatrix} \frac{1}6 & \frac{1}{8} \\ \frac{1}{8} & \frac{1}{10} \end{pmatrix}
$$

$$
\frac{1}{(2\pi)^2} \det (A - \lambda B) = (1 - \frac{\lambda}{6})(\frac{2}{3}- \frac{\lambda}{10}) - (\frac{2}{3} - \frac{\lambda}{8})^2
$$

$\lambda^2/960 - 2\lambda/45 + 2/9 = 0$ 

$\lambda_1 \sim 5.784$, $\lambda_2 \sim 36.9$ 

The **true** eigenvalues are the square of the roots of the Bessel function $J_o$ 

$\lambda_1 \sim 5.783$ (CLOSE), $\lambda_2 \sim 30.5$ (poor), to do better with the second, either use a different pair of trial functions or else use three trial functions

### Informal Derivation of the RRA

Let $w_1, \ldots, w_n$ be arbitrary linearly independt trial functions. As an *approximation* to the true minimum problem (MP)$_n$, let's **impose the additional condition** that $w(\bold x)$ is a linear combination of $w_1(\bold x), \ldots, w_n(\bold x)$. We can then write
$$
(6) \qquad w(\bold x) = \sum_{k=1}^n c_k w_k (\bold x)
$$
If we were extremely clever, then $w(\bold x)$ would be an eigenfunction $\implies$ $-\Delta w = \lambda w$, where $\lambda$ is both the eigenvalue and the value of the Rayleigh quotient

Then we would have $(\nabla w, \nabla w_j) = \lambda(w, w_j)$ because: (G1)

$\int_{\partial D} w_j \partial w/\partial n= 0 = \int_D \nabla w \cdot \nabla w_j + \int_D w_j \Delta w$ 

so $-\int_D w_j \Delta w = \int_D w_j (\lambda w) = \int_D \nabla w \cdot \nabla w_j$ 

and substituting $(6)$ into $(\nabla w, \nabla w_j) = \lambda(w, w_j)$ 
$$
(7) \qquad \sum_{k} a_{jk}c_k = \lambda \sum_{k}b_{jk}c_k
$$
In matrix notation, we can write $(7)$ as
$$
A\bold c = \lambda B \bold c, \text{ where }\bold c \in \R^n
$$
Since $w \not \equiv 0$, we have $\bold c \neq \bold 0$. This means that $A - \lambda B$ would be a singular matrix/ determinant of $(A - \lambda B) = 0$  because $(A - \lambda B)\bold c = 0$ for $\bold c\neq \bold 0$ 

### Minimax Principle

What we really want is an *exact* formula rather than just an approximation

To motivate it, denote the roots of the polynomial equation (3) by 
$$
\lambda_1^* \leq \cdots \leq \lambda_n^*
$$
Let's consider the biggest one, $\lambda_n^*$ , then using linear algebra: (Exercise 9)
$$
(8) \qquad \lambda_n^* = \max_{\bold c \neq 0} \frac{A \bold c \cdot \bold c}{B \bold c \cdot \bold c}
$$
$\lambda_n^* = \max \frac{\sum a_{jk}c_j c_k}{\sum b_{jk}c_j c_k}$ $ = \max \frac{(\nabla (\sum c_j w_j), \nabla(\sum c_k w_k))}{(\sum c_j w_j, \sum c_k w_k)}$ 

where the maxima are taken over all (nonzero) $n$-tuples $c_1, \ldots, c_n$ 

Therefore:
$$
(9) \qquad \lambda_n^* = \max \{ \frac{\| \nabla w\|^2}{\|w\|^2}, \text{ taken over all functions } w \text{ that are linear combinations of } w_1, \ldots, w_n\}
$$
Formula $(9)$ leads to the minimax principle, which states *the smallest possible value of* $\lambda_n^*$ *is the $n\text{th}$ eigenvalue* $\lambda_n$. Thus $\lambda_n$ is a minimum of a maximum 

#### Theorem 1. Minimax Principle

If $w_1$, $w_2$ $\ldots$ $w_n$ is an arbitrary set of trial functions, define $\lambda_n^*$ by formula (9). Then the $n$th eigenvalue is
$$
(10) \qquad \lambda_n = \min \lambda_n^*
$$
where the minimum is taken over all possible choices of $n$ trial functions $w_1, \ldots, w_n$ 

##### Proof. 

First: fix any choice of $n$ trial functions $w_1, \ldots, w_n$ 

second: choose $n$ constants $c_1, \ldots c_n$ (not all zero), so that the linear combination

$w(\bold x) \equiv \sum_{j=1}^n c_j(w_j, v_k) = 0$ is orthogonal to the first $(n-1)$ eigenfunctions $v_1(\bold x), \ldots, v_{n-1}(\bold x)$ 
$$
(11) \qquad (w, v_k) = \sum_{j=1}^n c_j(w_j, v_k) = 0, k = 1, \ldots, n-1
$$
This is possible, because the constants $c_1, \ldots, c_n$ are $n$ unknowns that need only satisfy the $(n-1)$ linear equations $(11)$ , Because there are fewer linear equations than unkowns, so there is always a nonzero solution. 

So are we doing all $w$ s.t. $w$ is orthgonal to the first $n-1$ eigenfunctions? and that's what $\lambda_n$ is? 

Using the minimum principle for the $n$th eigenvalue 

**Remark/Question**: this one requires each $w$ being orthogonal to the first $n-1$ eigenfunctions, so it would make sense that we are looking at all such linear combinations of trial functions that are orthogonal to the first $n-1$ eigenfunctions 

So it still makes sense: $\lambda_n$ is the minimum of any linear combination of trial functions (with the linear combination being orthogonal to $v_j$ $j = 1, …, n-1$)

so for $w_1, … , w_n$ arbitrary trial functions there's a linear combination of them that is orthgonal to the first $n-1$ eigenfunctions

and the Rayleigh quotient of this linear combination is at least $\lambda_n$ 

There's another linear combination of these aribtrary trial functions that has a rayleigh quotient at most $\lambda_n^*$ (for this arbitrary set) therefore (12) makes sense. 

we have 

$\lambda_n \leq \|\nabla w\|^2/\|w\|^2$, on the other hand, because the maximum in (9) is taken over all possible linear combinations, we have $\|\nabla w\|^2/\|w\|^2 \leq \lambda_n^*$ 

Thus:
$$
(12) \qquad \lambda_n \leq \frac{\| \nabla w\|^2}{\| w\|^2} \leq \lambda_n^*
$$
Inequality $(12)$ is true for **all** choices of $n$ trial functions $w_1, \dots, w_n$ ($\lambda_n^*$ chagnes with each $n$ trial functions)

Therefore: $\lambda_n \leq \min \lambda_n^*$, where the minimum is taken over all such choices (like the minimum of all possible maximums)

To show that the two numbers in (10) are equal, need a special choice of trial functions $w_1, \ldots, w_n$. Choose them to be the first $N$ eigenfunctions: $w_i = v_i$ for $i = 1, \ldots, n$ 

We may assume they are normalized: $\| v_j \| = 1$. With *this* choice of the $w's$ we have
$$
(13)\qquad \lambda_n^* = \max_{c_1, \ldots c_n}\frac{\| \nabla \sum c_j v_j\|^2}{\| \sum c_j v_j\|^2}
$$
Using Green's First Formula: Because BC, $0 = \int_D \nabla(\sum c_j v_j)\cdot \nabla(\sum c_j v_j) + \int_D (\sum c_j v_j)\Delta(\sum c_j v_j)$ $\implies$ $\int_D |\nabla(\sum c_j v_j)|^2 = \int_D c_j^2 v_j (\lambda_j v_j)$ 

the last quotient equals $\sum c_j^2 \lambda_j/\sum c_j^2$ which is at most $\sum c_j^2 \lambda_n/\sum c_j^2 = \lambda_n$ (since $\lambda_1 \leq \ldots \leq \lambda_n$)  

Thus $(13)$ implies that $\lambda_n^* \leq \lambda_n$ for **this particular choice** of $n$ trial functions. But we must have that $\lambda_n \leq \lambda_n^*$ for **all** choices of $n$ trial functions.
$$
(14) \qquad \lambda_n = \lambda_n^* \qquad \text{for our particular choice}
$$
 Therefore $\lambda_n$ equals the minimum of $\lambda_n^*$ over all possible choices of $n$ trial functions

## 11.3 Completeness

Neumann boundary condition. 

Prove the completeness of the eigenfunctions *for general domains* 

### The Neumann Boundary Condition

$\partial u/ \partial n = 0$ on $\partial D$. We denote the eigenvalues by $\tilde{\lambda_j}$ and the eigenfunctions by $\tilde{v_j}(\bold x)$. Thus
$$
(1) \qquad - \Delta \tilde{v_j}(\bold x) = \tilde{\lambda_j}\tilde{v_j}(\bold x) \qquad \text{in }D
\\ \frac{\partial \tilde{v_j}}{\partial n}= 0 \qquad \text{on }\partial D
$$
Number the eigenvalues: 
$$
0 = \tilde{\lambda_1} < \tilde{\lambda_2} \leq \tilde{\lambda_3} \leq \cdots \leq \tilde{\lambda_3} \leq \cdots
$$
(this is not the same numbering as in Section 4.2.) The first eigenfunction $\tilde{v_1}(\bold x)$ is a constant. 

#### Theorem 1. 

For the Neumann condition, we define a 

**"trial function"**: as *any* $C^2$ function $w(\bold x)$ such that $w \not \equiv 0$.

With this new definition: **any** of the preceding characterizitaions of the eigenvalues

* Minimum Principle (11.1.5)
* (MP)$_n$ 
* Rayleigh-Ritz approximation
* minimax prinicple (11.2.10)

There is **no condition** on the trial functions at the boundary. The trial functions are *not* supposed to satisfy the Neumann or any other boundary condition. 

The Nuemann condition is sometimes called the *free condition* 

whilst the Dirichlet condition is "fixed"

##### proof. 

Repeat the various steps of the preceding proofs. However, there are differences only where the boundary comes into play

**Theorem 11.1.1** ends up with (11.1.7):
$$
\iiint_D (- \nabla u \cdot \nabla v + muv)\, d \bold x = 0
$$
which now is valid for *all* $C^2$ functions $v(\bold x)$ with no restriction on $\partial D$ 

This can be rewritten by Green's first formula ($G1$) as 
$$
(2) \qquad \iiint_D (\Delta u + mu) v\, d \bold x = \iint_{\partial D} \frac{\partial u}{\partial n}v \, dS
$$
In $(2)$ we first choose $v(\bold x)$ arbitrarily *inside* $D$ and $v = 0$ on $\partial D$. As before, we still get $\Delta u + mu = 0$ *inside* $D$ 

* $\Delta u + mu$ is independent of $v$, so it must stay the same for all $v$ 

* And because $(2)$ is valid for **all** trial functions
* When we see that for a certain choice $v$ that the left side vanishes, this still must mean that $\Delta u + mu = 0$ inside $D$ $\implies$ no matter what: 

Thus the left side of $(2)$ vanishes for every trial function $v(\bold x)$. So
$$
(3) \qquad \iint_{\partial D} \frac{\partial u}{\partial n} v \, dS = 0
$$
for *all trial functions* $v$. But the new kind of trial function can be chosen to be an arbitrary function on $\partial D$ 

Choose $v = \frac{\partial u}{\partial n}$ on $\partial D$ . Then $(3)$ $\implies$ the integral of $(\partial u/\partial n)^2$ vanishes. By the first vanishing theorem, we deduce that $\partial u/\partial n = 0$ on $\partial D$ which is the Neumann condition. 

We basically proved that having no condition on the trial function $\implies$ the neumann condition has to be satisfied

### Completeness 

#### Theorem 2. 

For the Dirichlet Boundary Condition, the eigenfunctions are complete in the $L^2$ sense. The same is true for the Neumann condition. 

The actual theorem:

Let $\lambda_n$ be all the eigenvalues and $v_n(\bold x)$ the corresponding eigenfunctions. The eigenfunctions may be assumed to be mutually orthogonal. 

Let $f(\bold x)$ be any $L^2$ function in $D$, that is, $\| f\| < \infin$. 

Let $c_n = (f, v_n)/(v_n, v_n)$. Then:
$$
(4) \qquad \| f - \sum_{n=1}^N c_n v_n \|^2 = \iiint_D|f(\bold x) - \sum_{n = 1}^N c_n v_n (\bold x)|^2 \, d \bold x \rightarrow 0 \text{ as }N \rightarrow \infin
$$
The proof depends on 

1. the existence of the minima discussed in Section 11.1
2. the fact that the sequence of eigenvalues $\lambda_n$ $\rightarrow +\infin$ as $n \rightarrow \infin$ 

##### proof.

Prove (4) only in the special case that $f(\bold x)$ is a trial function. 

**<u>Dirichlet Case</u>** 

$f(\bold x)$: $C^2$ function that vanishes on $\partial D$ 

Denote the remainder after the subtraction of $N$ terms of the fourier expansion of $f$ by:
$$
(5) \qquad r_N(\bold x) = f(\bold x) - \sum_{n=1}^N c_n v_n(\bold x)
$$
By the orthogonality:
$$
\begin{align}
(r_N, v_j) &= (f - \sum_{n=1}^N c_n v_n, v_j)
\\ &= (f, v_j) - \sum_{n=1}^N c_n(v_n, v_j)
\\ &= (f, v_j) - c_j(v_j, v_j) =0
\end{align}
$$
because $c_j = \frac{(f, v_j)}{(v_j, v_j)}$ and this is true for $j = 1, 2, \cdots, N$ 

Therefore, the remainder is a **trial function** that satisfies the contraints of $(MP)$ Hence
$$
(6) \qquad \lambda_{N+1} = \min_w \frac{\| \nabla w\|^2}{\| w\|^2} \leq \frac{\| \nabla r_N\|^2}{\| r_N\|^2}
$$
by Theorem 11.1.2

Next, we expand
$$
(7) \qquad \begin{align}
\| \nabla r_N\|^2 &= \int |\nabla [f - \sum_{n=1}^N c_n v_n]|^2 
\\ &= \int (|\nabla f|^2 - 2 \sum_{n}c_n \nabla f \cdot \nabla v_n + \sum_{n, m} c_n c_m \nabla v_n \cdot \nabla v_m)
\end{align}
$$
using shorthand notation for the integrals. 

By G1 and the boundary condition $(f = v_n = 0)$ on $\partial D$, the middle term in $(7)$ contains the integral
$$
(8) \qquad \int \nabla f \cdot \nabla v_n = - \int f \Delta v_N = \lambda_n \int f v_n
$$
While the last term in $(7)$ contains the integral
$$
(9) \qquad \int \nabla v_n \cdot \nabla v_m = - \int v_n \Delta v_ m =  \int \lambda_m  v_n v_m = \delta _{mn}\lambda_n \int v_n^2
$$
where $\delta_{mn} = 0$ for $m \neq n$ and equals $1$ for $m = n$, remembering that $\int v_n v_m = 0$ for $n \neq m$ 

So the expression $(7)$ simplifies:
$$
(10) \qquad \| \nabla r_N\|^2 = \int |\nabla f|^2 - 2 \sum_n c_n \lambda_n (f, v_n) + \sum_{n, m}\delta_{mn}c_n^2 \lambda_n (v_n, v_n)
$$
Noting: $\sum_{n, m}\delta_{mn}c_n^2 \lambda_n (v_n, v_n) = \sum_{n}c_n^2 \lambda_n (v_n, v_n)$ 

and $c_n \cdot (v_n, v_n) = (f, v_n)$ 

Recalling the definition of $c_n$ the last two sums combine to produce the identity
$$
\| \nabla r_N\|^2 = \int |\nabla f|^2 - \sum_{n}c_n^2 \lambda_n (v_n, v_n)
$$
All the sums run from $0$ to $N$. If we throw away the last sum, we get the inequality:
$$
(11) \qquad \| \nabla r_N\|^2 \leq \int |\nabla f|^2 = \| \nabla f\|^2
$$
If we combine $(11)$ and $(6)$, we get
$$
(12) \qquad \| r_N\|^2 \leq \frac{\| \nabla r_N\|^2}{\lambda _N } \leq \frac{\| \nabla f \|^2}{\lambda_N}
$$
Recalling that $\lambda_N > 0$ and $\lambda_N \leq \lambda_{N+1}$ 

Note: $\lambda_N \rightarrow \infin$ (shown in 11.6), so the right side of (12) tends to zero since the numerator is fixed. 

Therefore $\| r_N\| \rightarrow 0$ as $N \rightarrow \infin$ 

This proves $(4)$ in case $f(\bold x)$ is a trial function and the boundary condition is Dirichlet. 

**<u>Neumann Case</u>**

Assume $f(\bold x)$ is a trial function, use the notation $\tilde{\lambda_j}$ and $\tilde{v_j}(\bold x)$ for the eigenvalues and eigenfunctions. 

Then the proof is the same as above (with tildes). Notice that $(8)$ nad $(9)$ are still valid because the eigenfunctions $\tilde{v_j}(\bold x)$ satiasfy the Neumann BCs. 

The only other difference from the preceding proof occurs as a consequence of the fact that the first eigenvalue vanishes: $\tilde \lambda_1 = 0$. However $\tilde \lambda_2 > 0$ (Exercise 1)

So when writing $(12)$, we simply assume $N \geq 2$ 
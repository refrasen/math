# Math 126 Book Notes 5.1-5.2

## 5.1 The Coefficients

##### Fourier Sine Series

$$
(1) \qquad \phi(x) = \sum_{n=1}^\infin A_n \sin \frac{n\pi x}{l}
$$

in the interval $(0,l)$ 

### Problem: Find Coefficients $A_n$ if $\phi(x)$ is given

Key observation:
$$
(2) \qquad \int_0^l \sin \frac{n\pi x}{l}\sin\frac{m \pi x}{l} dx = 0 \quad \text{if }m\neq n
$$
where $m, n \in \N$. 

**Proof of (2)** 

Use $\sin a \sin b = \frac{1}{2}[\cos(a-b) - \cos(a+b)]$ 

Therefore, the integral equals: 

$\frac{l}{2(m-n)} \sin \frac{(m - n)\pi x}{l} - \frac{l}{2(m+n)} \sin \frac{(m+n)\pi x }{l} |_0^l$ 
if $m\neq n$, this is a linear combination of $\sin(m\pm n) \pi$ and $\sin 0$, so it vanishes 

---

$$
(3)\quad\int_0^l \phi(x)\sin \frac{m\pi x}{l}dx = \int_0^l \sum_{n=l}^\infin A_n \sin \frac{n\pi x}l \sin \frac{m\pi x}l dx 
\\= \sum_{n=1}^\infin A_n\int_0^l \sin \frac{n\pi x}{l}\sin \frac{m\pi x}{l}dx
$$

All but one term vanishes, i.e., the term where $m = n$ 
we are thus left with $A_m \int_0^l \sin^2 \frac{m \pi x}{l}dx$ 
and $\sin^2 \frac{m\pi x}{l} = \frac{1}{2}[1 - \cos(\frac{2 m \pi}{l})]$ 
so ultimately
$$
(4) \qquad A_m = \frac{2}{l}\int_0^l \phi(x)\sin \frac{m\pi x}{l}dx
$$
this is the **formula for the Fourier coefficients** in series (1)

**if** $\phi(x)$ has expansion $(1)$, **then** the coefficients must be given by $(4)$ 

### Application to Diffusions and Waves

##### Diffusion Equation w/ Dirichlet Boundary Conditions

Formula $(4)$ is all we need.

##### Wave Equation w/ Dirichlet Boundary Conditions

Coefficients $A_m$ in $(4.1.9)$ are given by $(4)$ 
Coefficients $B_m$:
$$
(5)\qquad \frac{m\pi c}{l} B_m = \frac{2}{l}\int_0^l \psi(x)\sin \frac{m\pi x}{l}dx
$$

### Fourier Cosine Series

Corresponds to the Neumann Boundary Conditions on $(0,l)$ 
$$
(6) \qquad \phi(x) = \frac{1}{2}A_0 + \sum_{n=1}^\infin A_n \cos \frac{n \pi x}{l}
$$
Again: $\cos a \cos b = \frac{1}{2}[\cos(a-b) + \cos(a+b)]$ 
$$
\int_0^l \cos \frac{n\pi x}{l}\cos \frac{m\pi x}{l}dx = 0 \quad \text{if }m\neq n
$$

$$
\int_0^l \phi(x)\cos \frac{m\pi x}{l}dx = A_m \int_0^l \cos^2 \frac{m\pi x}{l} dx = \frac{l}{2}A_m
$$

if $m \neq 0$. For $m = 0$: 
$$
\int_0^l \phi(x)\cdot 1 dx = \frac{1}{2}A_0 \int_0^l 1^2 dx = \frac{l}{2}A_0
$$
**For all nonnegative integers $m$** 
$$
(7) \qquad A_m = \frac{2}{l}\int_0^l \phi(x)\cos \frac{m \pi x}{l}dx
$$
**this is why we put $\frac{1}{2}$ in front of the constant term in (6)** 

(to have one uniform formula i suppose)

### Full Fourier Series

Full Fourier Series of $\phi(x)$ on the interval $-l <x<l$ 
$$
(8) \qquad \phi(x) = \frac{1}2A_0 + \sum_{n=1}^\infin (A_n \cos \frac{n\pi x}{l} + B_n \sin \frac{n\pi x}{l})
$$
**Watch out**: The interval is **twice** as long!

##### Eigenfunctions:

$\{ 1, \cos(n\pi x/l), \sin(n\pi x/l) \}$, where $n = 1, 2, 3, …$ 

**Wonderful coincidence** 

$\sin a \sin b = \frac{1}{2}[\cos(a-b) - \cos(a+b)]$ 
$\cos a \cos b = \frac{1}{2}[\cos(a-b) + \cos(a+b)]$ 
$\sin a \cos b = \frac{1}{2}[\sin (a+b) + \sin(a-b)]$ 

$\frac{1}{2}\int_{-l}^{l} \sin [\frac{(m+n)\pi x}{l}] + \sin [\frac{(m-n)\pi x}{l}] dx$ $ = -\frac{1}{2} [ \frac{l}{(m+n)\pi} \cos \frac{(m+n)\pi x}{l} + \frac{l}{(m-n)\pi} \cos \frac{(m-n)\pi x}{l}]$ $_{-l}^l$ 
$ = -\frac{1}{2}[\frac{l}{(m+n)\pi}[ \cos((m+n)\pi) - \cos(-(m+n)\pi)  ] \\+ \frac{l}{(m-n)\pi}[\cos((m-n)\pi) - \cos(-(m-n)\pi)] ]$  
and using $\cos(x) = \cos(-x)$, we see that this is equal to $0$ for any $m, n$ 

So altogether:
$$
\int_{-l}^l \cos \frac{n\pi x}{l}\sin \frac{m\pi x}{l}dx = 0 \quad \text{for all }n,m
\\ \int_{-l}^l \cos \frac{n\pi x}{l}\cos \frac{m \pi x}{l}dx = 0 \quad \text{for }n \neq m
\\ \int_{-l}^l \sin \frac{n\pi x}{l} \sin \frac{m\pi x}{l}dx = 0 \quad \text{for }n \neq m
\\ \int_{-l}^l \cos \frac{n\pi x}{l}dx = 0 = \int_{-l}^l \sin \frac{m\pi x }{l}dx
$$
for the "single" cosine and sine functions:

for cosine: due to the integral equaling a linear combination of $\sin(\pm n\pi) = 0$
for sine: due to the integral equaling a constant times $\cos(m \pi ) - \cos(-m\pi) = \cos(m\pi) - \cos(m\pi) = 0$

Calculating the integrals of the squares** 
$$
\int_{-l}^l \cos^2 \frac{n\pi x}{l}dx = l = \int_{-l}^l \sin ^2 \frac{n \pi x}{l}dx \\
\int_{-l}^l 1 dx = 2l
$$
**Verification**:
$\int_{-l}^l\cos^2 \frac{n\pi x}{l}dx = \int_{-l}^l\frac{1}2[1 + \cos\frac{2 n\pi x}{l}]dx$

and the integral is then $\frac{1}{2} [x + \frac{l}{2n\pi}\sin \frac{2n\pi x}{l}]_{-l}^{l} = \frac{l - (-l)}{2} = l$

$\int_{-l}^l\sin^2 \frac{n\pi x}{l} dx = \int_{-l}^l\frac{1}{2}[1 - \cos \frac{2n\pi x}{l}]dx$

and the integral is then $\frac{1}{2}[x - \frac{l}{2n\pi} \sin \frac{2n\pi x}{l}]_{-l}^{l} = l$ 

##### Formulas for Coefficients

$$
(9) \qquad A_n = \frac{1}{l}\int_{-l}^l \phi(x)\cos \frac{n \pi x}{l}dx \quad (n = 0, 1, 2, ...)
\\ (10) \qquad B_n = \frac{1}{l}\int_{-l}^l \phi(x)\sin \frac{n\pi x}{l}dx \quad (n = 1, 2, 3, ...)
$$

### Examples

#### Example: $\phi(x) \equiv 1$, $x\in[0,l]$ 

##### Fourier Sine Series

$A_m = \frac{2}{l}\int_0^l \sin \frac{m\pi x}{l} dx = -\frac{2}{m\pi} \cos \frac{m \pi x}{l} |_0^l = \frac{2}{m\pi}[1 - \cos (m\pi )]$ $ = \frac{2}{m\pi}[1 - (-1)^m]$

$\implies $$A_m = 4/m\pi$ if $m$ odd, and $A_m = 0$ if $m$ even
$$
(11) \qquad 1 = \frac{4}{\pi}(\sin \frac{\pi x}{l}+ \frac{1}{3}\sin \frac{3\pi x}{l}+ \frac{1}{5}\sin \frac{5\pi x}{l}+ \cdots)
$$
in $(0,l)$ 

##### Fourier Cosine Series

$A_m = \frac{2}{l} \int_0^l \cos \frac{m \pi x}{l} dx = \frac{2}{m \pi} \sin \frac{m \pi x}{l} |_0^l = 0$ (Since no matter what $\sin (m \pi) = \sin 0 = 0$ for $m \neq0$ 
For $A_0 = \frac{2}{l} \int_0^l 1 dx = 2$ so $\frac{1}{2} A_0 = 1$ 

**It is thus TRIVIAL** i.e., $1 = 1$

the Fourier cosine expansion is *unique* and $1 = 1$ is obvious

unlike with $\sum_{n=1}^\infin A_n \sin \frac{n\pi x}{l}$ 

#### Example: $\phi(x) \equiv x$ in $(0,l)$ 

**I used Integration by Parts with $u(x) = x, v'(x) = \text{trig}$, etc.** 

##### Fourier Sine Series 

$$
A_m = \frac{2}{l}\int_0^l x \sin \frac{m \pi x}{l}dx
\\ = \frac{2}{m\pi}[-x \cos \frac{m \pi x}{l}] + \frac{2}{m\pi}\int_0^l\cos\frac{m\pi x}{l}dx
\\ = \frac{2}{m\pi}[ -x \cos \frac{m\pi x}{l} + \frac{l}{m \pi}\sin \frac{m\pi x}{l}]_0^l
\\ = \frac{2}{m\pi}[ -l\cos (m\pi) ] 
\\ = - \frac{2l}{m\pi}(-1)^m = (-1)^{m+1}\frac{2l}{m\pi}
$$

So the Series is then:
$$
(12) \qquad x = \frac{2l}{\pi}[\sin \frac{\pi x}{l} - \frac{1}{2}\sin \frac{2\pi x}{l} + \frac{1}{3}\sin \frac{3 \pi x}{l}+ \cdots]
$$


##### Fourier Cosine Series in $[0,l]$ 

$$
A_0 = \frac{2}{l}\int_0^l x dx = \frac{2}{l}\frac{l^2}{2} = l
\\ A_m = \frac{2}{l}\int_0^l x \cos \frac{m \pi x}{l}dx
\\ = \frac{2}{\pi m}[x \sin \frac{m\pi x}{l} + \frac{l}{m\pi} \cos \frac{m \pi x}{l}]_0^l
\\ = \frac{2}{\pi m}[\frac{l}{\pi m}(-1)^m - \frac{l}{\pi m}]
\\ = \begin{cases} -\frac{4l}{\pi^2m^2} & m \text{ odd} \\ 0 & m \text{ even}\end{cases}
$$

Thus in $(0,l)$ 
$$
(13) \qquad x = \frac{l}{2} - \frac{4l}{\pi^2}[\cos \frac{\pi x}{l} + \frac{1}{3^2}\cos\frac{3\pi x}{l} + \frac{1}{5^2}\cos \frac{5 \pi x }l]
$$

##### Full Fourier Series

$$
A_0 = \frac{1}{l}\int_{-l}^l x dx = 0
\\ A_m = \frac{1}{l}\int_{-l}^l x \cos \frac{m \pi x}{l}dx = \frac{x}{m \pi }\sin \frac{m \pi x}{l} + \frac{l}{m^2 pi^2}\cos \frac{m \pi x}{l}|_{-l}^l
\\ = \frac{l}{m^2\pi^2}(\cos m\pi - \cos(-m\pi)) = 0
$$

$$
B_m = \frac{1}{l}\int_{-l}^l x \sin \frac{m \pi x}{l}dx
\\ = -\frac{x}{m\pi}\cos \frac{m\pi x}{l}+ \frac{l}{m^2\pi^2}\sin \frac{m \pi x}{l}|_{-l}^l
\\ = \frac{-l}{m\pi}\cos m \pi + \frac{-l}{m \pi }\cos(-m\pi) = (-1)^{m+1}\frac{2l}{m\pi}
$$

This gives us the exactly the same series as $(12)$, except that it is valid in $(-l, l)$, which is not surprising because both sides of $(12)$ are odd.

#### Example: Solving Wave Equation

$$
u_{tt} = c^2u_{xx}
\\ u(0,t) = u(l,t) = 0
\\ u(x,0) = x, u_t(x,0) = 0
$$

From Section 4.1:
$$
u(x,t) = \sum_{n=1}^\infin (A_n \cos \frac{n\pi c t}{l}  + B_n \sin \frac{n \pi c t}{l})\sin \frac{n \pi x}{l}
$$
Diff. w.r.t. to time yields
$$
u_t(x,t) = \sum_{n=1}^\infin \frac{n \pi c}{l}(B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}l)\sin \frac{n \pi x}l
$$
setting $t = 0$: 
$$
0 = \sum_{n=1}^\infin \frac{n \pi c}{l}B_n \sin \frac{n \pi x}{l}
$$
so all $B_n = 0$ 

setting $t = 0$ in $u(x,t)$:
$$
x = \sum_{n=1}^\infin A_n \sin \frac{n \pi x}{l}
$$
which is the series $(12)$ 
$$
u(x,t) = \frac{2l}{\pi}\sum_{n=1}^\infin \frac{(-1)^{n+1}}{n}\sin \frac{n \pi x}{l}\cos \frac{n \pi c t}{l}
$$

## 5.2 Even, Odd, Periodic, and Complex Functions

* Almost any function $\phi(x)$ defined on the interval $(0,l)$ is the sum of its Fourier sine series and is also the sum of its Fourier cosine series
* Almost any function defined on $(-l, l)$ is the sum of its full Fourier series
* Each of these series converges inside the interval, **but not necessarily at the endpoints** 

##### Periodic Function

A function $\phi(x)$ defined for $-\infin <x<\infin$ is *periodic* if there is a number $p > 0$ such that
$$
(1)\qquad \phi(x+p) = \phi(x) \quad \text{for all }x
$$
The **period** of this function is then $p$ 

* the graph of the function repeats forever horizontally

* **Note**: if $\phi(x)$ has period $p$, then $\phi(x+np) = \phi(x)$ for all $x$ and all integers $n$

  ​	Induction:

  ​	Case $n = 1$: $\phi(x + p) = \phi(x)$ is true, (trivial)
  ​	Assume $k \geq 1$ to be true… we shall show $k + 1$ is true:
  ​	$\phi(x + (k+1)p) = \phi((x+kp) +p)$ due to periodicity 

  ​	this equals $\phi(x+kp)$ which is $= \phi(x)$ by inductive hypothesis

---

* the sum of two functions of period $p$ has period $p$ 

* If $\phi(x)$ has period $p$, then $\int_a^{a+p}\phi(x) dx$ doesn't depend on $a$

  ​	let $H(a) = \int_a^{a+p} \phi(x) dx$ 
  ​	$\frac{dH}{da} = \int_a^{a+p} \frac{d \phi}{da} dx + \phi(a+p) - \phi(a) = 0 + \phi(a+p) - \phi(a)$ 	and since $\phi$ is periodic: $\phi(a+p) - \phi(a) = 0$ 

* The sum of $\cos(mx) + \sin 2mx$ is the sum of functions of periods $2\pi/m$ and $\pi/m$ and therefore itself has period $2\pi/m$, the "larger" of the two

* If a function is defined only on an interval of length $p$ it can be **extended** in only one way to a function of period $p$ 

  * a function defined on $-l<x<l$ has a **periodic extension** 
    $$
    (2) \qquad \phi_{per}(x) = \phi(x-2lm) \quad \text{for}\quad-l + 2lm<x<l+2lm
    \\ \text{for all integers }m
    $$

  * The periodic extension has jumps at the endpoints **unless** the one-sided limits are equal: $\phi(-l) = \phi(-l+)$ 

Each term in the full Fourier series has period $2l$, so its sum, provided it converges, also has to have priod $2l$ 

**Therefore**: the full Fourier series can be regarded as either

* an expansion of an arbitrary function on the interval $(-l, l)$ 
* Or as an expansion of a periodic function of *period 2l* defined on the whole line $-\infin <x< \infin$ 

##### Even function

Satisfies 
$$
(3) \qquad \phi(-x) = \phi(x)
$$

* its graph is symmetric with respect to the $y$ axis

We require $\phi(x)$ to be defined on some interval $(-l, +l)$ which is symmetric around $x = 0$ 

##### Odd function

Satisfies
$$
(4) \qquad \phi(-x) = -\phi(x)
$$

* its graph is symmetric with respect to origin

We require $\phi(x)$ to be defined on some interavl $(-l, +l)$ which is symmetric around $x = 0$ 

##### Examples of Even and Odd functions

| Function                                   | Even/Odd                                                     |
| ------------------------------------------ | ------------------------------------------------------------ |
| $x^n$                                      | $\begin{cases} \text{even} & n \text{ even} \\ \text{odd} & n \text{ odd} \end{cases}$ |
| $\cos x, \cosh x, $ and functions of $x^2$ | even                                                         |
| $\sin x, \tan x, \sinh x$                  | odd                                                          |
| $\text{even} \times \text{even}$           | even                                                         |
| $\text{even} \times \text{odd}$            | odd                                                          |
| $\text{odd} \times \text{odd}$             | even                                                         |
| $\text{odd} + \text{odd}$                  | odd                                                          |
| $\text{even} + \text{even}$                | even                                                         |
| $\text{even} + \text{odd}$                 | inconclusive                                                 |

**How to break a function down into the sum of an even function and an odd function**

Let $f$ be any function at all defined on $(-l, l)$ 

Let $\phi(x) = \frac{1}{2}[f(x) + f(-x)]$ and $\psi(x) = \frac{1}{2}[f(x) - f(-x)]$ 
$f(x) = \phi(x) + \psi(x)$ (obvious)
$\phi(-x) = \frac{1}{2}[f(-x) + f(-(-x))] = \frac{1}2[f(-x) + f(x)] = \phi(x)$ so $\phi$ is even

$\psi(-x) = \frac{1}{2}[f(-x) - f(x)] = -\frac{1}{2}[f(x) - f(-x)] = -\psi(x)$, so $\psi$ is odd

**example**: $e^x = \cosh x + \sinh x$ 

If $p(x)$ is a polynomial its even part is sum of its even terms, odd part is sum of its odd terms

##### Integration and Differentiation of Even/Odd functions

if $\phi(x)$ is even $\implies$ $d\phi/dx$ and $\int_0^x \phi(s) ds$ are odd

if $\phi(x)​$ is odd $\implies​$ $d\phi/dx​$ and $\int_0^x \phi(s) ds​$ are even

Note that the lower limit of integration is at origin

Graph of an odd function must pass through the origin since $\phi(0) = -\phi(-0) = -\phi(0)$ 

Graph of an even function must cross the $y$ axis horizontally, $\phi'(x) = 0$ since the derivative is odd (if the derivative exists)

### Example: $\tan(x)$ 

* $\tan(x)$ is product of even and odd function, both have period $2\pi$ 
  * so $\tan x$ is odd and periodic
  * BUT its period is $\pi$ 
* $d/dx(\tan (x))$ is $\sec^2 x$, an even and periodic function with period $\pi$ 
* $\tan (ax)$ is also odd and periodic and has period $\pi/a$ for any $a>0$ 

##### Definite integrals around symmetric intervals

$$
(5) \qquad \int_{-l}^l (\text{odd})dx = 0 \qquad \int_{-l}^l(\text{even})dx = 2\int_0^l (\text{even})dx
$$

##### even extension of $\phi(x)$ when defined over $(0,l)$ 

$$
\phi_{\text{even}} = \begin{cases} \phi(x) & 0<x<l \\ \phi(-x) & -l<x<0\end{cases}
$$

**not necessarily defined at origin** 

##### odd extension of $\phi(x)$ when defined over $(0,l)$ 

$$
\phi_{\text{odd}}= \begin{cases} \phi(x) & 0<x<l \\ -\phi(-x) & -l<x<0 \\ 0 & x= 0\end{cases}
$$

image **must** be through origin

### Fourier Series and Boundary Conditions

The fourier sine series, if it converges, can be regarded as an expansion of an arbitrary function that is odd and has period $2l$ defined on the whole line $-\infin <x< +\infin$ 

* it is the sum of terms $\sin(n\pi x/l)$, each an odd function with period $2l$ 

The fourier cosine series, if it converges, can be regarded as an expansion of an arbitrary function which is even and has period $2l$ defined on the whole line $-\infin <x<\infin$ 

* it is the sum of terms $\cos(n\pi x/l)$, each an even function with period $2l$ 

##### Relationship between BCs and even/odd/periodic

* $u(0,t) = u(l,t) = 0$: Dirichlet BCs correspond to the **odd** extension
* $u_x(0,t) = u_x(l,t) = 0$: Neumann BCs correspond to the **even** extension
* $u(l,t) = u(-l,t), u_x(l,t) = u_x(-l,t)​$: Periodic BCs correspond to the **periodic** extension

### Complex Form of the Full Fourier Series

#### DeMoivre Formulas

$$
\sin \theta = \frac{e^{i\theta} - e^{-i\theta}}{2i} \qquad \cos \theta = \frac{e^{i\theta} + e^{-i\theta}}{2}
$$

We can use $e^{+in\pi x/l}$ and $e^{-in\pi x/l}$ as an alternative pair to cosine and sine.

**Careful!**: These functions are complex

We get $\{ 1, e^{+i\pi x/l}, e^{-i\pi x/l}, e^{+2 \pi x/l}, e^{-2 \pi x/l}, …  \}$ 

so $ \{ e^{in\pi x/l}  \} $ is what we use with $n$ is **any** integer, positive or negative

So we obtain a new form for the full Fourier series
$$
(12) \qquad \phi(x) = \sum_{n= -\infin}^\infin c_n e^{in \pi x/l }
$$
$(12)$ is the sum of two infinite series: one from 0 to $\infin$ and one from $-1$ to $-\infin$ 

##### Magical fact

$$
\int_{-l}^l e^{in\pi x/l}e^{-im\pi x/l} dx = \int_{-l}^l e^{i(n-m)\pi x/l}dx
\\ = \frac{l}{i (n-m)\pi}[e^{i(n-m)\pi }-e^{i(m-n)\pi}]
\\ = \frac{l}{i \pi (n-m)} [(-1)^{n-m} - (-1)^{m-n}] = 0
$$

provided $n \neq m$ 

When $n = m$ we obtain 
$$
\int_{-l}^l e^{i(n-n)\pi x/l}dx = \int_{-l}^l 1 dx = 2l
$$
So we have by the method of section 5.1
$$
c_n = \frac{1}{2l} \int_{-l}^l \phi(x)e^{-in\pi x/l}dx
$$
**This is the same series just written in a different form** 


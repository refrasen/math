# Math 126 Book Notes Appendices

## A.1 Continuous and Differentiable Functions

### Intervals

$[b, c] = \{b \leq x \leq c\}$ 

$(b,c) = \{b < x < c\}$ 

$[b, c), (b, c]$ 

### Limits

Let $f(x)$ be a (real) function of one variable

The function $f(x)$ has the limit $L$ as $x$ approaches $a$ if for any number $\epsilon > 0$ there exists a number $\delta > 0$ such that $0 < |x-a| < \delta$ $\implies$ $|f(x) - L| < \epsilon$ 

$\equiv$ For any sequence $x_n \rightarrow a$ $\implies$ $f(x_n) \rightarrow L$ 

Function must be defined on both sides of $a$, but not necessarily at $a$ 

#### One-sided limits

$f(x)$ has the limit $L$ from the right as $x$ approaches $a$ if for any number $\epsilon > 0$ there exists a number $\delta > 0$ such that $0 < x-a < \delta$ $\implies$ $|f(x) - L| < \epsilon$ (so $x > a$)

$L$ denoted by $f(x+)$ 

Limit from the left is the same except that $0 < a - x < \delta$ 

$L$ denoted by $f(x-$ )


$$
f(x+) = f(x-) \implies \lim_{x \rightarrow a} f(x) = f(x+) = f(x-)
$$

### Continuous

$f(x)$ is continuous at a point $a$ if 
$$
\lim_{x \rightarrow a}f(x) = f(a)
$$
In particular, the function has to be defined at $a$ 

A function is continuous in an interval $b \leq x \leq c$ if it is continuous at each point of that interval (at endpoints only one-sided continuity is defined)

### Intermediate Value Theorem

If $f(x)$ is continuous in a finite closed interval $[a,b]$ and $f(a) < p < f(b)$ then there exists at least one point $c \in [a,b]$ such that $f(c) = p$ 

### Theorem on the Maximum

If $f(x)$ is continuous in a finite closed interval $[a,b]$ then it has a maximum in that interval:

 $\exists$ a point $m$ $\in [a,b]$ s.t. $f(x) \leq f(m)$ for all $x \in [a,b]$ 

by applying the theorem to $-f(x)$ we have that $f(x)$ also has a minimum

### Vanishing Theorem

Let $f(x)$ be a **continuous** function in a **finite closed interval** $[a,b]$. **Assume** that $f(x) \geq 0$ in the interval and that $\int_a^b f(x)dx = 0$ $\implies$ $f(x)​$ is identically zero

### Jump Discontinuity 

If $f(x-)$ and $f(x+)$ exists, but $f(x-) \neq f(x+)​$ 

### Peicewise Continuous function on a finite closed interval

$f(x)$ is a piecewise continuous function on $[a,b]$ if there are a finite number of points $a = a_0 \leq a_1 \leq \cdots \leq a_n = b$ such that $f(x)$ is continuous on each open subinterval $(a_{j-1}, a_j)$ and all one-sided limits $f(a_j^-)$ for $1 \leq j \leq n$ and $f(a_j ^+)$ for $0 \leq j \leq n -1$ exist

**has jumps** at a finite number of points, but is otherwise continuous.

**Every piecewise continuous function is integrable** 

### Differentiable at a point $a$ 

$$
\lim_{x \rightarrow a} \frac{f(x) - f(a)}{x-a} = f'(a) = \frac{df}{dx}(a)
$$

### Functions of Two or More Variables

$\vec x = (x, y, z)$ in three dimensions, $\vec x = (x, y)$ in two.

**domain** or region: open set $D$ 

**boundary point** of anys et $D$ (in 3-D space): a point $\vec x$ for which every ball centered at $\vec x$ intersects both $D$ and the complement of $D$ 

**open set**: a set that contains none of its boundary points

**closed set**: a set that contains all of its boundary points

**closure** $\overline D$: the union of the domain and its boundary, $\partial D \cup D$ 

#### First Vanishing Theorem 

$f(\vec x)$: continuous function in $\overline D$ where $D$ is a bounded domain

Assume: $f(\vec x) \geq 0$ in $\overline D$ and that $\int \int \int _D f(\vec x) d\vec x = 0$ 

$\implies$ $f(\vec x) \equiv 0$ 

#### Second Vanshing Theorem

$f(\vec x)$: continuous function in $D_0$ 

such that $\int \int \int _D f(\vec x)d\vec x = 0$ FOR ALL subdomains $D \sub D_0$ 

$\implies$ $f(\vec x) \equiv 0$ for all $\vec x\in D_0​$ 

A function is said to be **class $C^1$** in a domain $D$ if each of its partial derivatives of first order exists and is continuous in $D$ 

**Mixed Derivatives are Equal**: If a function $f(x,y) \in C^2$ $\implies$ $f_{xy} = f_{yx}$

The same is true for derivatives of any order

**Chain Rule**: consider $(s, t) \mapsto (x,y) \mapsto u$ If $u$ is a function of $x$ and $y$ and of class $C^1$, while $x$ and $y$ are diff. functions of $s$ and $t$ $\implies$
$$
\frac{\partial u}{\partial s} = \frac{\partial u}{\partial x}\frac{\partial x}{\partial s} + \frac{\partial u}{\partial y}\frac{\partial y}{\partial s}
\\ \text{and}
\frac{\partial u}{\partial t} = \frac{\partial u}{\partial x}\frac{\partial x}{\partial t} + \frac{\partial u}{\partial y}\frac{\partial y}{\partial t}
$$
**gradient** of a function: $\nabla f = (f_{x_1}, . . ., f_{x_n})$ 

**directional derivative** of $f(\vec x)$ at a point $\vec a$ in the direction of the vector $\vec v$ 
$$
\lim_{t \rightarrow 0} \frac{f(\vec a + t \vec v)- f(\vec a)}{t} = \vec v \cdot \nabla f(\vec a)
$$
*example*: rate of change of quantity $f(\vec x)$ seen by a moving particle $\vec x = \vec x (t)$ 

is $\frac{d}{dt}f(\vec x) = \nabla f \cdot \frac{d\vec x}{dt}​$ 

### Vector Fields

a vector field assigns a vector to each point. 

$x = g(x', y'), y = h(x', y')$ in two variables

"Coordinate change" from primed to unprimed coordinates

**Jacobian Matrix**: 
$$
\mathscr J = \begin{pmatrix} \frac{\partial x}{\partial x'} & \frac{\partial y}{\partial x'} \\ \frac{\partial x}{\partial y'} & \frac{\partial y}{\partial y'} \end{pmatrix}
$$
$J = \det \mathscr J$

If $g$ and $h$ are linear, then $\mathscr J$ is the matrix of the linear transformation (w.r.t. to the coordinate systems) and $J$ is its determinant 
$$
\begin{pmatrix} \frac{\partial u}{\partial x'} \\ \frac{\partial u}{\partial y'} \end{pmatrix} = \mathscr J \begin{pmatrix} \frac{\partial u}{\partial x} \\ \frac{\partial u}{\partial y} \end{pmatrix}
$$
If the transformation $x = g(x', y')$ and $y = h(x', y')$ carries the domain $D'$ onto the domain $D$ in a one-to-one manner and is of class $C^1$, and if $f(x,y)$ is a continuous function on $\overline D$ then
$$
\int \int_D f(x,y)dxdy = \int \int_{D'} f(g(x', y'), h(x', y')) \cdot |J(x', y')|dx' dy'
$$
The size of the jacobian factor $|J|$ expresses the amount that areas are stretched/shrunk by the transformation

If $G$ is a vector field in a simply connected domain in three dimensions:

$\nabla \times G = 0$ if and only if $G = \nabla f$ for some scalar function $f$ (letting $G = [G^1, G^2, G^3])$ 

$\nabla \times G = \det \begin{pmatrix}  \vec i & \vec j & \vec k \\ \frac{\partial }{\partial x} & \frac{\partial}{\partial y} & \frac{\partial }{\partial z} \\ G^1 & G^2 & G^3 \end{pmatrix} = (G^3_y - G^2_z, G^1_z - G^3_x, G^2_x - G^1_y)​$

$G​$ is curl free if and only if it is a gradient. 

In two dimensions the same is true if we define $\text{curl }G = \frac{\partial g_2}{\partial x} - \frac{\partial g_1}{\partial y}$ , $G = [g_1, g_2]​$ 

## A.3 Differentiation and Integration

### Derivatives of Integrals

$$
(1)\;\;\;  I(t) = \int_{a(t)}^{b(t)} f(x,t)dx
$$



#### Theorem 1

$f(x,t)$, $\frac{\partial f}{\partial t}$ are continuous in rectangle $[a, b]\times[c, d]$, then
$$
(2)\;\;\;\;(\frac{d}{dt})\int_a^b f(x,t)dx = \int_a^b\frac{\partial f}{\partial t}(x, t)dx, \text{for }t \in [c, d] 
$$

#### Theorem 2

$f(x, t), \partial f/\partial t$ are continuous in $(-\infin, \infin)\times (c, d)$ 

Assume $\int_{-\infin}^\infin |f(x,t)|dx $ and $\int_{-\infin}^{\infin} |\partial f/\partial t| dx$ converge uniformly (as improper integrals) for $t \in (c,d)$ , then
$$
(\frac{d}{dt})\int_{-\infin}^{\infin} f(x,t)dx = \int_{-\infin}^\infin\frac{\partial f}{\partial t}(x, t)dx, \text{for }t \in [c, d]
$$

#### Theorem 3

If $I(t)$ is defined by (1), where $f(x,t)$ and $\partial f/\partial t$ are continuous on the rectangle $[A, B]\times[c,d]$ 

where $[A, B]$ contains the union of all the intervals $[a(t), b(t)]$, 

and if $a(t)$ and $b(t)$ are differentiable functions on $[c,d]$, then
$$
(3) \;\;\;\; \frac{dI}{dt} = \int_{a(t)}^{b(t)}\frac{\partial f}{\partial t}(x,t)dx + f(b(t), t)b'(t) - f(a(t), t)a'(t)
$$
Note: if we have $g(t, a(t), b(t)) = \int_{a(t)}^{b(t)}f(x, t)dx$

$g_a = -f(a, t)$, $g_b = f(b, t)$ due to FTC with single variable

$g(a) = \int_a^b f(x, t)dx = -\int_b^a f(x, t)dx$, $g(b) = \int_a^b f(x, t)dx$ 

#### Theorem 4

$D$: fixed three-dimensional bounded domain

$f(\vec x, t)$, $(\partial f/\partial t)(\vec x, t)$ are continuous for $\vec x \in \overline D$ and $t \in [c, d]$. then
$$
(4) \;\;\;\; (\frac{d}{dt})\int\int\int_D f(\vec x, t)d\vec x = \int\int\int_D \frac{\partial f}{\partial t}(\vec x, t)d \vec x \;\;\; \text{for }t \in [c,d]
$$

### Curves and Surfaces

**curve**: continuous function from an interval $[a, b]$ into space

given by triple of continuous functions, also called the parameterization of the curve
$$
(5) \;\;\; x = f(t), \;\;\; y = g(t),  \;\;\; z = h(t)\;\;\; \text{for }a \leq t \leq b
$$
The triple of functions is really the **image** of the function in $\vec x$ space

**Natural arc length**:
$$
ds/dt = |d\vec x/dt|
$$
if the three coordinate functions are of class $C^1$, the curve is of class $C^1$ 

The curve is **piecewise** $C^1$ if there is a partition $a = t_0 < t_1 < \cdots < t_n = b$ such that the coordinate functions are $C^1$ on each closed subinterval $[t_{j-1}, t_j]$; at each endpoint $t_j$ the functions and their first derivatives are computed as one-sided limits

**surface**: a continuous function from a closed set $\overline D$ in a plane into three-dimensional space
$$
(6) \;\;\;\; x = f(x,t), \;\;\;\; y = g(s,t), \;\;\;\; z = h(s,t) \;\;\;\; \text{for }(s,t) \in \overline{D}
$$

* parameterization of $S$
* $s$ and $t$ are the **parameters** 
* $\overline D$: **parameter domain** 

Surface may also be given as the **graph** of a function $z = h(x,y)$ where the point $(x,y)$ runs over a plane domain $\overline D$ 

* parameteriation in this case: $x =s, y = t, z = h(s,t)$ for $(s,t) \in \overline D$ 

A surface is of class $C^1$ if all three coordinate functions are of class $C^1$ 

Union of finite number of overlapping surface is also considered a surface. 

surface is piecewise $C^1$ if it is the union of a finite number of $C^1$ surfaces that meet along boundary curves

*example*: cube is piecewise $C^1$ 

**Implicit Function Theorem [F1]**: 

#### Theorem 5 

$F(x,y, z)$ is $C^1$ function of three variables such that $\nabla F \neq 0$ 

then the level set $\{ F(x,y,z)=0 \}$ is a $C^1$ surface. That is, local $C^1$ parameterizations like (6) can be found for it

### Integrals of Derivatives

#### Green's Theorem 6

$D$ be bounded plane domain with a piecewise $C^1$ boundary curve $C = \partial D$, C is traversed once with $D$ on the left

Let $p(x,y), q(x,y)$ be any $C^1$ functions defined on $\overline{D} = D \cup C$ Then
$$
(7) \;\;\;\; \int\int_D (q_x - p_y)\;dxdy = \int_C p\; dx + q \; dy = \int_C (p,q)\cdot \vec t \; ds
$$

* $\vec t$ is the unit tangent vector along the curve and $ds$ is the element of arc length

Ewuivalent formulation:

$p = -g$, $q = +f$. $\vec f = (f, g)$ is any $C^1$ vector filed in $\overline D$ 

$\int\int _D (f_x + g_y) dx dy = \int_C (-g dx + f dy)$ 

$\vec n = (+dy/ds, -dx/ds)$ 
$$
(8)\;\;\;\; \int\int_D \nabla \cdot \vec f \; dxdy = \int_C \vec f \cdot \vec n \; ds
$$
where $\nabla \cdot \vec f = f_x + g_y$ denotes the divergence of $\vec f$ 

#### Divergence Theorem 7

$D$: bounded spatial domain with a piecewise $C^1$ boundary surface $S$

$\vec n$: unit outward normal vector on $S$ 

$\vec f (\vec x)$: any $C^1$ vector field on $\overline{D} = D \cup S$, then
$$
(9) \;\;\; \int\int\int_D \nabla \cdot \vec f d \vec x= \int\int_S \vec f \cdot \vec n dS
$$
$\nabla \cdot \vec f$: three dimensional divergence of $\vec f$ 

$dS$ = surface area on $S$
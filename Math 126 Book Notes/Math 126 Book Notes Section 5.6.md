# Math 126 Book Notes 5.6

## Inhomogeneous Boundary Conditions

* Separation of variables will not work

##### Diffusion equation with sources at both endpoints

$$
u_t = ku_{xx} \qquad 0 <x < l, t>0
\\ u(0,t) = h(t) \quad u(l,t) = j(t)
\\ u(x,0) \equiv 0
$$

### Expansion Method

#### Diffusion Equation 

1. For the corresponding homogeneous problem, (cause Dirichlet) the correct expansion is the Fourier sine series. For each $t$, we can expand

$$
(2) \qquad u(x,t) = \sum_{n=1}^\infin u_n(t)\sin \frac{n\pi x}{l}
$$

* completeness theorems guarantee that any function in $(0,l)$ can be so expanded

2. The coefficients are given by: (just use the usual coefficient formula)

$$
(3) \qquad u_n(t) = \frac{2}{l}\int_0^lu(x,t) \sin \frac{n \pi x}{l}\; dx
$$

* Note: The series converges inside the interval, not necessarily at the endpoints
* Convergence theorems 3 and 4, but **not** 2 of section 5.4

3. Differentiating the series $(2)$ term by term:

$$
0 = u_t - ku_{xx} = \sum [\frac{du_n}{dt} + ku_n(t)(\frac{n\pi}{l})^2]\sin \frac{n \pi x}l
$$

**(????):** since $\sin \frac{n \pi x}{l} \not \equiv 0$ on $x \in (0, l)$, we must have $du_n/dt + k\lambda_n u_n = 0$, so that $u_n(t) = A_n e^{k \lambda_n t}$ or… $u_n(t) = A_n e^{-k\lambda_n t}$, but this **doesn't fit** the boundary condition **(Why?)** i suppose if $x \in (0,l)$, $\sin \frac{n \pi x}{l} \neq 0$, and $A_n e^{k \lambda _n t}$ can be taken to $\infin$ with $t \rightarrow \infin$, which would throw off $u(0,t) = h(t)$ or $u(l, t) = j(t)$ 

**Moral**: Can't differentiate term by term. See example 3 in Section 5.4

---

**Start Over**

* Assume $u(x,t)$ continuous so we know that $(2)$ and $(3)$ are valid by the completeness theorem $5.4.3$ 
* we need $u_n(0) = 0$ by initial condition
* If the derivative of $u(x,t)$ are continuous, let's expand them.

$$
(4) \qquad\frac{\partial u}{\partial t} = \sum_{n=1}^\infin v_n(t)\sin \frac{n \pi x}{l}
$$

with coefficients
$$
(5)\qquad v_n(t) = \frac{2}l\int_0^l \frac{\partial u}{\partial t}\sin \frac{ n \pi x}{l}\; dx \underbrace{=}_{u \text{ is continuous}} \frac{2}{l}\frac{\partial }{\partial t}\int_0^l u(x,t)\sin \frac{n \pi x}{l}\; dx = \frac{\partial u_n}{\partial t}
$$
Now we do the same with $u_{xx}$: 
$$
(6)\qquad \frac{\partial^2 u}{\partial x^2} = \sum_{n=1}^\infin w_n(t)s\in \frac{n \pi x}{l}
$$
with coefficients
$$
(7) \qquad w_n(t) = \frac{2}{l}\int_0^l \frac{\partial^2 u}{\partial x^2}\sin \frac{n \pi x}{l}\; dx
$$
Using Green's second identity: $\int_0^l -X_1'' X_2 + X_1X_2'' \; dx = (-X_1'X_2 + X_1X_2')|_0^l$ 

with $X_1 = -u$, $X_2 = \sin \frac{n \pi x}{l}$, then we have $\frac{2}{l} \int_0^l \frac{\partial ^2 u}{\partial x^2} \sin \frac{n \pi x}{l} + (\frac{n\pi}{l})^2 u(x,t) \sin \frac{ n \pi x}{l} \; dx$, but we "add"  $-\frac{2}{l} \int_0^l (\frac{n\pi}{l})^2 u(x,t) \sin \frac{ n \pi x}{l} \; dx$ on both sides to get $w_n(t)$ expression:
$$
-\frac{2}{l} \int_0^l (\frac{n\pi}{l})^2 u(x,t) \sin \frac{ n \pi x}{l} \; dx + \frac{2}{l}(u_x \sin \frac{n \pi x}{l} - \frac{n \pi }{l}u \cos \frac{n \pi x}{l})|_0^l
$$
"Here come the boundary conditions", and remembering $(3)$ and $\lambda_n = (\frac{n \pi}{l})^2$: 
$$
(8) \qquad -\lambda_n u_n - 2n\pi l^{-2}(-1)^n j(t) + 2n\pi l^{-2}h(t)
$$
By $(5)$ and $(7)$ and the PDE
$$
v_n - kw_n(t) = \frac{2}{l}\int_0^l(u_t - ku_{xx})\sin \frac{n \pi x}{l}\; dx = \frac{2}{l}\int_0^l 0 \; dx = 0
$$
and by $(5)$ and $(8)$ and the above
$$
(9)\qquad \frac{du_n}{dt} = v_n(t) = kw_n(t) = k[-\lambda_nu_n(t) - 2n\pi l^{-2}[[(-1)^nj(t)- h(t)]]
$$
$(9)$ is just an ordinary differential equation, to be solved with initial condition $u_n(0) = 0$ from $(1)$ 

The solution of $(9)$: 
$$
(10) \qquad u_n(t) = Ce^{-\lambda_n k t} - 2n \pi l^{-2}k \int_0^t e^{-\lambda_n k(t-s)}[(-1)^n j(s)-h(s)]\; ds
$$
Using that the solution to $du/dt = -Au(t) + f(t)$ is $u(t) = e^{-tA}\phi + \int_0^t e^{(s-t)A} f(s) \; dx$ 
where $\phi$ is the inital condition $u(0) =\phi$ 

**(????):** we did $A = k \lambda_n$, $\phi = 0…$ **(?)**., $f(s) = -2n \pi k l^{-2} [(-1)^n j(t) - h(t)]$ 

#### Wave Equation

$$
(11) \begin{cases} u_{tt} -c^2u_{xx} = f(x,t)
\\ u(0,t)= h(t) \qquad u(l,t) = k(t)
\\ u(x,0) = \phi(x) \qquad u_t(x,0) = \psi(x) \end{cases}
$$

$$
u(x,t) = \sum_{n=1}^\infin u_n(t)\sin \frac{n \pi x}{l}
$$

Again. then expand

*  $u_{tt}(x,t)$ with coefficients $v_n(t)$ 
*   $u_{xx}(x, t)$ with coefficients $w_n(t)$ 
*  $f(x,t)​$ with coefficients $f_n(t)​$
* $\phi(x)​$ with coefficients $\phi_n​$ 
* $\psi(x)$ with coefficients $\psi_n$ 

Then we get:
$$
v_n(t) = \frac{2}{l}\int_0^l \frac{\partial ^2 u}{\partial t^2}\sin \frac{n \pi x}{l} \; dx = \frac{d^2 u_n}{dt^2}
$$
and
$$
w_n(t) = \frac{2}{l}\int_0^l u_{xx}\sin \frac{n \pi x}{l}\; dx \\
= -\lambda_n u_n(t) + 2n \pi l^{-2}[h(t)-(-1)^n k(t)]
$$
From the PDE:
$$
v_n(t) - c^2w_n(t) = \frac{2}{l}\int_0^l (u_{tt} - cu_{xx})\sin \frac{n \pi x}{l}\; dx \underbrace{=}_{\text{because of the PDE and expansion of }f(x,t)} f_n(t)
$$
Therefore:
$$
(12) \qquad \frac{d^2u_n}{dt^2} + c^2\lambda_n u_n(t) = -2n \pi l^{-2}[(-1)^nk(t)- h(t)]+f_n(t)
$$
with initial conditions $u_n(0) = \phi_n, \quad u_n'(0) = \psi_n$  

### Method of Shifting the Data

**Subtraction** Method, the data can be shifted from the boundary to another spot in the problem. 

**Boundary conditions can be made homogeneous by subtracting any known function that satisfies them**

For problem $(11)$: the function $\mathscr{U}(x,t) = (1- \frac{x}{l})h(t) + \frac{x}{l}k(t)$ satisfies the BCs. 

so let $v(x,t) = u(x,t) - \mathscr{U}(x,t)$, then $v(x,t)$ satisfies $(11)$ but with homogeneous boundary conditions, with initial data $\phi(x) - \mathscr{U}(x,0)$, $\psi(x) - \mathscr(U)_t(x,0)$ and with righthand side $f$ replaced by $f - \mathscr{U}_{tt}$ 

**The boundary condition and the differential equation can simultaneously be made homogeneous by subtracting any known function that satisfies them** 

One case: "stationary data": $h, k,$ and $f(x)$ are independent of time

Then, we find a solution of
$$
-c^2\mathscr{U}_{xx} = f(x) \quad \mathscr{U}(0) = h \qquad \mathscr{U}(l) = k
$$
Then $v(x,t) = u(x,t) - \mathscr{U}(x)$ solves the problem with zero boundary data, zero right-hand side, and initial data $\phi(x) - \mathscr{U}(x)$ and $\psi(x)$ 

$v_{tt} = u_{tt}$, $v_{xx} = u_{xx} - \mathscr{U}_{xx}$, so $v_{tt} - c^2v_{tt} = u_{tt} - c^2u_{xx} - f(x) = 0$ 

##### Example: Periodic Case

$$
f(x,t) = F(x)\cos \omega t \qquad h(t) = H \cos \omega t \qquad k(t) = K \cos \omega t
$$

same "time behavior" in all the data
$$
\mathscr{U}_{tt}-c^2 \mathscr{U}_{xx} = F(x)\cos \omega t
\\ \mathscr{U}(0,t) = H \cos \omega t \qquad \mathscr{U}(l, t) = K \cos \omega t
$$
A Good Guess: $\mathscr{U}$ should have the form $\mathscr U(x,t) = \mathscr U_0 (x) \cos \omega t$ 

This will happen if $\mathscr U_0(x)$ satisfies:
$$
-\omega^2 \mathscr U_0 - c^2 \mathscr U_0'' = F(x) \qquad \mathscr U_0 (0) = H \qquad \mathscr U_0(l) = K
$$

# Math 126 Book Notes 3.2 Finite Interval

#### The Finite Interval

**The book says you can skip the rest of the section**, and the professor didn't go over this in detail... what he went over instead was this: ![Screen Shot 2019-02-10 at 12.21.54 AM](/Users/r/Documents/Git/math/Math%20126%20Lecture/Math%20126%20Lecture%208.assets/Screen%20Shot%202019-02-10%20at%2012.21.54%20AM.png)

"reflected, like billiards"

------

Writing strictly from the book:

- Initial data $\phi(x)$ and $\psi(x)$ are now given only for $0 < x < l$ 
- Extend them to the whole line to be "*odd*" with respect to both $x = 0$ and $x = l$ 

$$
\phi_{\text{ext}}(-x) = -\phi_{\text{ext}}(x) \qquad \text{ and }\qquad \phi_{\text{ext}}(2l-x) = -\phi_{\text{ext}}(x)
$$

so define:
$$
\phi_{\text{ext}}(x)= \begin{cases} 
\phi(x) & \text{for }0 < x <l
\\ -\phi(-x) & \text{for }-l<x<0
\\ \text{extended to be of period 2l}
\end{cases}
$$
Period 2l: $\phi_{\text{ext}}(x + 2l) = \phi_{\text{ext}}(x)$ for all $x$ 

so for example: if $x + 2l < 0$ $\implies$ $\phi_{\text{ext}}(x+2l) = - \phi_{\text{ext}}(-x - 2l) = - \phi_{\text{ext}}(-x-2l +2l) = -\phi_{\text{ext}}(-x) $

$\phi_{\text{ext}}(2l - x) = \phi_{\text{ext}}(-x) = - \phi_{\text{ext}}(x)$ (if $x > 0$)

We do exactly the same thing for $\psi(x)$ (defined for $x \in (0, l)$)to get $\psi_{\text{ext}}(x)$ defined for the whole $x$-axis

So same sort of reasoning:

- We have an infinite line problem with the extended data, which we know the solution to, which we'll call $u(x,t)$ 
- We let $v$ be the restriction of $u$ to $(0,l)$

$$
v(x,t) = \frac{1}{2}[\phi_{\text{ext}}(x+ct) + \frac{1}{2}\phi_{\text{ext}}(x-ct)] + \frac{1}{2c}\int_{x-ct}^{x+ct}\psi_{\text{ext}}(s)\; ds
$$

for $0 \leq x \leq l$ 

**To see it explicitly**: must unwind the definitions of $\phi_{\text{ext}}$ and $\psi_{\text{ext}}$ 

**complicated**: includes a precise description of **all** reflections of the wave at both the boundary points $x = 0$ and $x= l$ ![Screen Shot 2019-02-10 at 12.34.41 AM](/Users/r/Documents/Git/math/Math%20126%20Lecture/Math%20126%20Lecture%208.assets/Screen%20Shot%202019-02-10%20at%2012.34.41%20AM.png)

Trying to understand *Figure 5*:

- $-\phi(-x)$, we are in $-l<x<0$, so multiplying $x$ by $-1$ will give us $-x \in (0,l)$, but since it must be odd, we then multiply $\phi(-x)$ by $-1$ as well
- $-\phi(-x+2l)$, because we are treating $x > l$ similalrly to how we treat $x < 0$ 
  - Because for values in $(l, 2l)$: we want to do this:
  - let $d = x - l$ we need $\phi(x) = -\phi(l-d)$
  - and $2l -x = 2l - (l + d) = l -d$ as desired
  - and naturally, since it is odd, we multiply $\phi(l-d)$ by $-1$ 
- $\phi(x+2l)$, for $x \in (-2l, -l)$ 
  - Periodical sort of thinking:
    - $\phi(x+2l)$ should be associated with $\phi(x)$ done. 
    - also it is $x + 2l$ because $-2l < x < -l$ $\equiv$ $0 < x + 2l < l$ 
  - We can also think of every $(ml, 0) $ $m \in \Z$ as a reflection point
    - Tricky to remember when it should be negative $\phi$ or positive $\phi$ 
    - Trick though: if we have interval $(ml, (m+1)l)$ with $m$ even, then we are using positive $\phi$ 
- $\phi(x-2l)$, for $x \in (2l, 3l)$ 
  - obviously: $2l < x < 3l$ $\equiv$ $0 < x-2l < l$ 
  - I suppose since our periodicity is $2l$, we always want to express things in terms of $2l m$ 
  - It is also positive due to the periodicity: $\phi([0,l]) = \phi([2l m, 2l(m+1)])$ for all $m \in \Z$ 

So note: for any interval $(lm, l(m+1))$ that our point $x$ lies in:

if $m$ is odd: express it as $-\phi(l(m+1)- x)$ 

if $m$ is even: express is as $\phi(x - lm)$ (note this works for $m< 0$, i.e., $m = -2$ gives $\phi(x + 2l)$)

------

So focusing on the example we have in the book from figures 4 and 5: 

$\phi_{\text{ext}}(x+ct) = -\phi(4l - x- ct)$: Minus sign is due to $(x+ct)$ lying in $(3l, 4l)$, also "odd" number of reflections

$\phi_{\text{ext}}(x+ct) = \phi(x-ct+2l)$ : plus sign due to $(x-ct)$ lying in $(-2l, -l)$, also "even" reflections

------

**Question**: Why do the number of reflections tell us this information?

I Let $(x_0, 0)$, where $x_0 \in (0, l)$. The point that the line, $t = \frac{x - x_0}{c}$ will intersect with $x = l$ is

$(l, \frac{l - x_0}{c})$ and the line that crosses this point with slop $-1/c$ is $(t - \frac{l-x_0}{c}) = -\frac{x - l}{c}$ which will cross the $x$-axis as $t = 0$: $l-x_0 = x_1 - l$ $\equiv$ $x_1 = 2l - x_0$, which means $l < x_1 < 2l$, the interval where $\phi_{\text{ext}}(x)$ is negative $\phi$. 

Ah, simpler explantion without so much algebra, but very informal:

each reflection brings us to a line shifted over an interval

e.g., if we start at $(0,l)$, we then are either in $(-l, 0)$ or $(l, 2l)$

then we go to $(2l, 3l)$ or $(-2l, -l)$ etc. 

- $(0,l)$ crossing $x= l$ will be matched with $(l, 2l)$ 
  - $(0,l)$ crossing $x = 0$ will be matched with $(-l, 0)$ 
- $(l, 2l)$ crossing $x = 0$ will be matched with $(-2l, -l)$ 
  - $(-l, 0)$ crossing $x = l$ must be matched with $(2l, 3l)$ (as $l - (-l)$ = $2l$)
- $(-2l, -l)$ crossing $x = l$ will be matched with $(3l, 4l)$ 
  - $(2l, 3l)$ crossing $x = 0$ must be matched with $(-3l, -2l)$ 

So in general: $(ml, (m+1)l)$ 

- crossing $x=0$ gets paired with $(-(m+1)l, -ml)$ 

- crossing $x = l$ gets paired with $((1-m)l,(2-m)l )$ 

  - idea is similar to $0 \pm d$, where $d$ is distance
  - but we are dealing with $l \pm d$ 

- We want a point $x_0$ in $(ml, (m+1)l)$ to be reflected across $x= l$: 

  Start with the point in the interval: $ml < x_0 < ml + l$ 

  $\implies$ $-ml < l - x_0 < l - ml$ (distance measure from $l$)

  $l  + l - x_0$ is the new point:

  $\implies$ $l - ml < 2l - x_0 < 2l - ml$ 

  So note: 

  if $x_0 < l$: $0 < l -x_0$ $\implies$ $l < 2l - x_0$ 

  if $x_0 > l$: $0 > l -x_0$ $\implies$ $l > 2l - x_0$ 

------


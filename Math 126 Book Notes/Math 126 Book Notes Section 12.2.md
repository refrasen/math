# Math 126 Book Notes Section 12.2

# Green's Functions, Revisted

## Laplace operator

Note: $\frac{1}{r}$ is a harmonic function in three dimensions except at origin

$\phi(\bold x)$: let this be a test function

By exercise 7.2.2: 
$$
\phi(\bold 0) = -\iiint \frac{1}{r} \Delta \phi(\bold x)\frac{d \bold x}{4 \pi}
$$
**just take their word for it I guess** 

Idk how $- \iiint \frac{1}{r}\Delta \phi(\bold x) \frac{d \bold x}{4 \pi } = \iiint \phi(\bold x)\Delta(-\frac{1}{4 \pi r})d\bold x$ ($\Delta$$(-\frac{1}{4 \pi r}$)  vanishes everywhere but at $\bold x =0$

$\implies​$ 
$$
(1) \qquad \Delta(-\frac{1}{4 \pi r}) = \delta(\bold x)
$$
in three dimension: How?

$\delta(\bold x)$ vanishes except at origin and formula (1) explains why $1/r$ is a harmonic function away from the origin and explains how it differs from being harmonic at the origin.

**Dirichlet problem for Poisson Eqn**: 
$$
\Delta u = f \quad \text{in }D \qquad u = 0 \quad \text{on }\partial D
$$
Then by 7.3.2, where $G(\bold x, \bold x_0)$ is the Green's Function, the solution:
$$
(2) \qquad u(\bold x_0) =\iiint_D G(\bold x, \bold x_0)f(\bold x) \, d \bold x
$$
Fix point $\bold x_0 \in D$ the left side of $(2)$ can be written as: (from earlier as $u$ is a test function)
$$
u(\bold x_0) = \iiint_D \delta(\bold x - \bold x_0) u(\bold x)\, d\bold x
$$
assume $u(\bold x)$ is an arbitrary test function whose support (closure of the set where $u \neq 0$) is a bounded subset of $D$ 
$$
u(\bold x_0) = \iiint_D G(\bold x, \bold x_0)\Delta u(\bold x)\, d \bold x = \iiint_D \Delta G(\bold x, \bold x_0)u(\bold x)\, d \bold x
$$
 Noting:

$\iiint_D G(\bold x, \bold x_0)\Delta u - u \Delta g d \bold x = \iint_{\partial D} G \frac{\partial u}{\partial n} - u \frac{\partial G}{\partial n}\, dS = 0$ (and using Green's 2nd Identity)

Because: $G = 0$ on $\partial D$ and $u = 0$ on $\partial D$ 

$\Delta G​$ understood in the sense of distributions...

**Deduce:**
$$
(3) \qquad \Delta G (\bold x, \bold x_0) = \delta(\bold x - \bold x_0) \quad \text{ in }D
$$
Note: The function $G(\bold x, \bold x_0) + (4\pi|\bold x - \bold x_0|)^{-1}​$ is harmonic in the whole of the domain, including at $\bold x_0​$. Thus: $\Delta G + \Delta[(4\pi|\bold x - \bold x_0|)^{-1}] = 0​$ 
$$
\Delta G = - \Delta \frac{1}{4 \pi |\bold x - \bold x_0|} = \delta(\bold x - \bold x_0) \quad \text{in }D
$$
which is the same result as $(3)$. 

$G(\bold x, \bold x_0)$ is the unique distribution that satisfies the PDE $(3)$ and the boundary condition
$$
(4) \qquad G = 0 \quad \text{for }\bold x \in \partial D
$$
$G(\bold x, \bold x_0)$ may be interpreted as the steady-state temperature an object $D$ that is held at zero temperature on the boundary due to a unit source of heat at the point $\bold x_0$ 

## Diffusion Equation

One-Deimensional diffusion equatin on the whole line. 

The source function solves: 
$$
(5) \qquad S_t = k \Delta S \quad (-\infin < x <\infin, 0 < t < \infin) \qquad S(x,0) = \delta(x)
$$
Let: $R(x, t) = S(x - x_0, t -t_0)$ $t > t_0$ $R(x, t) \equiv $ $t < t_0$ 
$$
(6) \qquad R_t - k \Delta R = \delta(x -x_0)\delta(t -t_0) \quad \text{for }- \infin <x < \infin, -\infin <t<\infin
$$
(see Exercise 7)

The same interpretations are true in any dimension by Section 9.4

## Wave Equation

$$
(7) \qquad
\begin{align}
S_{tt} = c^2 \Delta S && (-\infin < x , y, z < \infin, -\infin < t < \infin)
\\ S(\bold x, 0) = 0 && S_t(\bold x, 0) = \delta(\bold x)
\end{align}
$$

**Riemann function**. Where $S$ is the source function for the wave equation 

Finding a formula for the Riemann Function: Let $\psi(\bold x)$ be any test function and let
$$
(8) \qquad u(\bold x, t) = \int S(\bold x - \bold y, t)\psi(\bold y)\, d \bold y
$$
Then $u(\bold x, t)$ satisfies the wave equation with initial data $u(\bold x, 0) \equiv 0$ and $u_t(\bold x, 0) = \psi(\bold x)$ 

Now in **one** dimension by section 2.1 (8) becomes: 
$$
\int_{-\infin}^\infin S(x-y, t)\psi(y)\, dy = u(x,t) = \frac{1}{2c}\int_{x-ct}^{x+ct} \psi(y)\, dy
$$
for $t \geq 0$. Therefore:
$$
S(x,t)= \begin{cases} \frac{1}{2c} & \text{for }|x| < ct \\ 0 & \text{for }|x| > ct \end{cases}
$$
Using the Heaviside Function:
$$
(9) \qquad S(x,t) = \frac{1}{2c}H(c^2t^2 - x^2)\cdot \frac{t}{|t|} \qquad \text{for }c^2t^2 \neq x^2
$$
where we now permit $-\infin < t < \infin$ 

Note: Riemann function has a jump discontinuity along the characteristics (jumps from 0 to 1/2c and vice versa), so we have propagation of singularities

---

In **three** dimensions
$$
\iiint S(\bold x - \bold y, t) \psi(\bold y) = u(\bold x, t) = \frac{1}{4 \pi c^2 t}\iint_{|\bold x - \bold y| = ct} \psi(\bold y)\, dS
\\ = \frac{1}{4 \pi c^2 t} \iiint \delta(ct - |\bold x - \bold y| ) \psi(\bold y) \, d \bold y
$$
for $t \geq 0$, with a minor shift of notation from Section 9.2

Therefore: $S(\bold x, t) = 1/(4 \pi c^2 t)\delta(ct - |\bold x|)$ for $t \geq 0$ 
$$
(10) \qquad S(\bold x, t) = \frac{1}{2 \pi c}\delta(c^2 t^2 - |\bold x|^2)\cdot \frac{t}{|t|} 
$$
Noting: Huygen's Principle

---

Explanation for expressions:

If $f$ is a distribution and $w = \psi(r)$ is a function with $\psi'(r) \neq 0$ $\implies$ the meaning of the distribution $g(r) = f[\psi(r) ]$ 
$$
(11) \qquad (g, \phi) = (f \quad , \quad \frac{\phi[\psi^{-1}(w)]}{|\psi'[\psi^{-1}(w)]|})
$$
for all test functions $\phi$. Motivated by change of variables $w = \psi(r)$ for ordinary integrals
$$
\int f[\psi(r)]\phi(r)\, dr = \int f(w) \phi (r) \frac{dw}{|\psi'(r)|}
$$
For instance: (10): $f = \delta$ and $w = c^2t^2 - r^2$ with $r > 0$ 
$$
(\delta(c^2t^2 - r^2), \phi(r)) = (\delta(w), \frac{\phi(\sqrt{c^2t^2 - w})}{2(\sqrt{c^2t^2 -w})}) = \frac{1}{2c|t|}\phi(c|t|)
$$
$\psi(r) = c^2t^2 - r^2$, $\psi'(r) = -2r$ $|\psi'(r)| = 2r = 2\sqrt{c^2t^2 - w}$  

In *two* dimensions:
$$
(12) \qquad S(\bold x, t) = 
\begin{cases} 
\frac{1}{2 \pi c}(c^2 t^2 - |\bold x|^2)^{-1/2} &  \text{for }|\bold x| < ct
\\ 0 & \text{for }|\bold x| > ct
\end{cases}
$$
Riemann function smooth function inside light cone in 2 dimensions (depending only on the relativistic quantity $c^2t^2 - |\bold x|^2$) with a singularity on the cone

## Boundary and Initial Conditions

Source function is defined as the solution to: 
$$
(13) \qquad 
\begin{align}
S_t &= k \Delta S && \text{for }\bold x \in D
\\ S &= 0 && \text{for }\bold x \in \partial D
\\ S &= \delta(\bold x - \bold x_0) && \text{for }t = 0
\end{align}
$$
We denote it by $S(\bold x, \bold x_0, t)$. Let $u(\bold x, t)$ denote the solution of the same problem
$$
\begin{align} 
u(\bold x, t) &= \sum_{n=1}^\infin c_n e^{-\lambda_n k t}X_n (\bold x)
\\ &= \sum_{n=1}^\infin [\iiint_D \phi(\bold y)X_n (\bold x) d\bold y]e^{-\lambda_n k t}X_n(\bold x)
\\ &= \iiint_D [\sum_{n=1}^\infin e^{-\lambda_n kt}X_n(\bold x)X_n(\bold y)]\phi(\bold y)\, d\bold y
\end{align}
$$
assuming that the switch of summation and integration is justisfied. 
$$
(14) \qquad S(\bold x, \bold x_0, t) = \sum_{n=1}^\infin e^{-\lambda_n k t}X_n (\bold x)X_n(\bold x_0)
$$

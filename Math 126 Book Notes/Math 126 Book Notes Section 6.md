# Math 126 Book Notes Section 6

## 6.1 

Highlights in Book

##### Laplace's Equation

##### Poisson's Equation

##### Instances of Laplace's and Poisson's Equation

* Electrostatics
* Steady Fluid Flow
* Analytic Functions of a complex variable
* Brownian Motion

##### Maximum Principle

##### Uniqueness of the Dirichlet Problem

### Invariance in 2 and 3 dimensions

#### 2 Dimensions

$\Delta_2 u = u_{xx} + u_{yy} = u_{rr} + \frac{1}{r} u_r + \frac{1}{r^2}u_{\theta \theta}$ 

Rotationally invariant harmonic functions: $u = u(r)$

$u_{rr} + \frac{1}{r}u_{r} = 0 $ $ = ru_{rr} + u_r\implies (ru_r)_r = 0$ $\implies$ $ru_r = c_1$

$\implies u = c+1 \ln r + c_2$ 

#### 3 Dimensions

$\Delta u = \sum_{i = 1}^d u_{x_i x_i}$ is the trace of the Hessian matrix $\{ u_{x_i x_j} \}_{i, j = 1, …, d}$ 

3D Laplacian in spherical coordinates:

transformations: 
$$
\begin{align} 
(x, y, z) &\rightarrow (r, \phi, \theta)
\\ \searrow &(s, \phi, z) \nearrow
\end{align}
$$

$$
\begin{align} 
x &= s \cos \phi && \text{where } s = r \sin \theta && r = \sqrt{x^2 + y^2 + z^2} = \sqrt{s^2 + z^2}
\\y &= s\sin \phi && && s = \sqrt{x^2 + y^2}
\\ z &= r \cos \theta
\end{align}
$$

![image-20190306141436113](Math 126 Book Notes Section 6.assets/image-20190306141436113.png)![image-20190306141459651](Math 126 Book Notes Section 6.assets/image-20190306141459651.png)

Using Last time's Calculuation with $(z,s)$ rather than $(x,y)$ 
$$
u_{zz}+u_{ss} = u_{rr}+\frac{1}{r}u_r + \frac{1}{r^2}u_{\theta \theta}
\\ u_{xx}+u_{yy}= u_{ss}+ \frac{1}{s}u_s + \frac{1}{s^2}u_{\phi \phi}
$$

$$
\Delta_3 u = u_{xx}+u_{yy}+u_{zz} = u_{rr} + \frac{1}{r}u_r + \frac{1}{r^2}u_{\theta \theta} + \frac{1}{s} u_s + \frac{1}{s^2}u_{\phi \phi}
$$

Need to replace the $u_s$ term:
$$
\begin{align}
u_s &= u_r r_s + u_\theta \theta_s + u_\phi \phi_s
\\ r_s &= \frac{1}{2r}\cdot 2s = \frac{s}{r}
\\ s &= r \sin \theta \implies 1 = r_s\sin\theta + r\cos\theta(\theta_s) = \sin^2\theta + r\cos\theta\cdot \theta_s \implies \theta_s 
\\ &= \frac{\cos \theta}{r}
\\ \phi_s &= 0
\end{align}
$$

$$
\Delta_3 u = u_{rr} + \frac{2}{r}u_r + \frac{1}{r^2}[u_{\theta \theta} + \cot \theta u_{\theta} + \frac{1}{\sin^2\theta}u_{\phi \phi}]
$$

**Rotationally Invariant/symmetric Solutions**:  $u = u(r)$ 
$$
\begin{align}
0 &= \Delta_3 u = u_{rr} + \frac{2}{r}u_r |\cdot r^2
\\ &= r^2u_{rr} + 2ru_r 
\\ &= (r^2u_r)_r
\\ r^2u_r &= c_1 \implies
\\ u &= \frac{c_1}{r}+c_2
\end{align}
$$

## 6.2 Rectangles and Cubes

1. Look for separated solutions of the PDE
2. Put in the homogeneous boundary conditions to get the eigenvalues. (special geometry) 
3. Sum the Series
4. Put in the inhomogeneous initial/BC conditions

### Rectangle

![image-20190306143432526](Math 126 Book Notes Section 6.assets/image-20190306143432526.png)
$$
\Delta u = u_{xx}+u_{yy} = 0 \qquad \text{in }D = \{(x,y): 0 <x<a, 0 <y<b \}
$$
**First Step**: Decompose $u = u_1 + u_2 + u_3 +u_4$ 
$$
\begin{align}
u_1  && &&(g(x), 0, 0, 0)
\\ u_2  && \text{has data: } &&(0, h(x), 0, 0)
\\ u_3  && &&(0, 0, j(y), 0)
\\ u_4 && &&(0, 0, 0, k(y))
\end{align}
$$
**Second step**: Focus on $u_1$ 

**1. Separation of variables**: $u(x,y) = X(x)Y(y) $ $\implies$ 
$$
\Delta u = X'' Y + XY''
\\ \implies
\\ \frac{X''}{X}= - \frac{Y''}{Y} = -\lambda
$$
Then:
$$
\begin{cases} 
X'' + \lambda X = 0 & 0 <x<a & X(0) = X'(a) = 0
\\ Y'' - \lambda Y = 0 & 0 < y < b & Y'(0)+Y(0) = 0 & Y(b) = g(x)
\end{cases}
$$
We know the General form of $X$: 

Recall for (D) and (N) homogeneous BC's: 

$0 < \beta_n^2 - \lambda_n = (n + \frac{1}{2})^2 \frac{ \pi^2}{a^2})$, $u = 0, 1, 2, …$ 
$$
X_n(x) = \sin \beta_n x = \sin \frac{(n+ \frac{1}{2})\pi x}{a}
$$
Checking:

$A \cos \beta x + B \sin \beta x$ $\implies$ $A = 0$, $B \beta\cos \beta a = 0$ so $\beta a = (2n +1)\pi/2 = (n + 1/2) \pi$ 

and since $\lambda _n > 0$: 
$$
Y(y) = A \cosh \beta_n y + B \sinh \beta_n y
$$
**2. Apply Homogeneous BCs** 
$$
\begin{align}
Y(0) = A && Y'(0) = B\beta_n 
\\ &Y'(0)+Y(0)= A + B \beta_n = 0
\end{align}
$$
Then, WLOG, $B = -1$ so $A = \beta_n$ 
$$
Y(y) = \beta_n \cosh \beta_ny - \sinh \beta_n y
$$
**3. Sum the series** 
$$
u_1(x,y) = \sum_{n=0}^\infin A_n \sin \beta_n x ( \beta_n \cosh \beta_n y - \sinh \beta_n y)
$$
The above sum is a harmonic function in $D$ that satisfies the lateral and southern BCs for $u_1(x,y)$. 

**4. Apply Inhomogeneous BCs**  (similar to initial condition)
$$
g(x) = \sum_{n= 0}^\infin A_n \sin \beta_n x(\beta_n \cosh \beta_n b - \sinh \beta_n b)
$$
we have:
$$
A_n (\beta_n \cosh b - \sinh \beta_n b) = \frac{2}{a}\int_0^ag(x)\sin (\beta_n x)\; dx
\\ \implies A_n = \frac{2}{a}\frac{1}{\beta_n \cosh \beta_n b - \sinh \beta_n b}\int_0^a g(x)\sin (\beta_n x)\; dx
$$


### Cube or Cube

![image-20190306150441861](Math 126 Book Notes Section 6.assets/image-20190306150441861.png)
$$
\text{BC's: } u(\pi, y, z) = g(y, z)
\\ \text{else: } u(0, y, z) = u(x, \pi , z) = u(x, 0, z) = u(x, y, 0) = u(x, y, \pi) = 0
$$
**1. Separation of Variables** 

$u(x,y,z) = X(x)Y(y)Z(z)$ 
$$
\frac{X''}{X} + \frac{Y''}{Y} + \frac{Z''}{Z} = 0
\\ X(0)= Y(0)= Z(0) = Y(\pi) = Z(\pi) = 0
\\ X(\pi) = g(y, z)
$$

$$
\frac{X''}{X}= -\frac{Y''}{Y}- \frac{Z''}{Z}
\\ \frac{Y''}{Y} = - \frac{X''}{X}- \frac{Z''}{Z}
\\ \frac{Z''}{Z}= - \frac{X''}{X} - \frac{Y''}{Y}
$$

Since the left side and right hand side of each of the above equations depend on different variables:

*Each quotient* $\frac{X''}{X}, \frac{Y''}{Y}, \frac{Z''}{Z}$ must be a constant
$$
\begin{align}
X'' &= -\xi X
\\ Y'' &= -\eta Y
\\ Z'' &= -\zeta Z
\end{align}
$$
**2. Find Solutions and Apply Homogeneous Bounday Conditions** 

since $Y$ and $Z$ have Homogeneous Dirichlet BCs, they have a solution we know of:
$$
\begin{align}
Y(y) &= \sin(my) && m = 1, 2, 3, ...
\\ Z(z) &= \sin(nz) && n = 1, 2, 3, ...
\end{align}
$$

$$
\begin{align} X'' &= -(\frac{Y''}{Y}+\frac{Z''}Z)X
\\&= -(-m^2 \frac{\sin my}{\sin my} - n^2 \frac{\sin nz}{\sin n})X
\\&= (m^2+n^2)X, && X(0)= 0
\end{align}
$$

and we get as a solution to this:
$$
X(x) = A \sinh \sqrt{m^2 + n^2}x + B \cosh \sqrt{m^2 +n^2}x
\\ \text{apply BC: }X(0)= 0
\\ X(0) = B = 0
\\\implies
X(x) = A \sinh (\sqrt{m^2 + n^2}x)
$$
**3. Sum the Series**: 
$$
(*)\qquad u(x,y, z) = \sum_{n=1}^\infin \sum_{m=1}^\infin A_{m, n}\sinh(\sqrt{m^2 + n^2}x)\sin(my)\sin(nz)
$$
**4. Apply Inhomogeneous BC**: 

$g(y, z) = \sum_{n=1}^\infin \sum_{m=1}^\infin A_{m,n} \sinh(\sqrt{m^2 + n^2}\pi) \sin(my)\sin(nz)$ 

**Double Fourier Sine Series** in $(0, \pi) \times (0, \pi)$ 

Note: $\{ \sin (my) \sin(nz) | n, m \in \N \}$ is an orthogonal set of functions and
$$
\begin{align} \int_0^\pi \int_0^\pi (\sin (my) \sin (nz))^2 \; dy \; dz &= \int_0^\pi \sin^2 (my)\; dy \int_0^\pi \sin^2(nz)\; dz \\ &= (\frac{\pi}2)(\frac{\pi}2) = \frac{\pi}{4}
\end{align} 
$$

$$
(**)\qquad A_{m, n} = \frac{4}{\pi^2 \sinh \sqrt{m^2 + n^2 \pi}}\int_0^\pi \int_0^\pi g(y, z) \sin(my)\sin(nz)\; dy \; dz
$$



## 6.3 Poisson's Formula

### Dirichlet Problem for a Circle

Rotational invariance of $\Delta$ $\implies$ circle is a "natural shape" for harmonic functions
$$
\begin{align} 
(1) && u_{xx} + u_{yy} &= 0 && \text{for }x^2 + y^2 < a^2
\\ (2) && u&= h(\theta) && \text{for }x^2 + y^2 = a^2
\end{align}
$$

#### Seperate Variables in Polar Coordinates

$u = R(r) \Theta (\theta)$ 

so using equation 6.1.5
$$
\begin{align} 0 &= u_{xx} + u_{yy} = u_{rr} + \frac{1}r u_r + \frac{1}{r^2}u_{\theta \theta}
\\ &= R'' \Theta + \frac{1}r R' \Theta + \frac{1}{r^2}R\Theta''
\end{align}
$$
**Divide by** $R\Theta$ (like in chapter 4) and multiply by $r^2$, then following the same thing with chapter 4: i.e., taking $R$ parts = $\Theta$ parts = $\lambda$ 
$$
\begin{align}
(3) &&\Theta'' + \lambda \Theta = 0
\\(4) && r^2 R'' + rR' - \lambda R = 0
\end{align}
$$
These are each ODE's, easily solved. **What are the BC's?** 
$$
(5) \qquad \Theta(\theta + 2\pi) = \Theta(\theta) \qquad \text{for }-\infin < \theta < \infin
$$
Solving $(3)$: $\alpha^2 + \lambda = 0$ $\implies$ $\alpha = \pm\sqrt{-\lambda}$ $\implies$ $Ae^{\alpha \theta} + Be^{-\alpha \theta}$ $\implies$ 

$A e^{\alpha \theta + \alpha 2\pi} + B e^{-\alpha\theta-\alpha 2\pi  }$ $ = Ae^{\alpha \theta} e^{\alpha 2 \pi} + B e^{-\alpha \theta}e^{-\alpha 2\pi} = Ae^{\alpha \theta} + B e^{- \alpha \theta}$

need $e^{\alpha 2 \pi} = 1​$ $\implies​$ $\Re (\alpha) = 0​$ $\implies​$ $\alpha = bi​$ for some $b \in \R​$ 

then: $e^{\alpha 2\pi} = e^{i b 2\pi } = \cos (b 2 \pi) +i \sin (b 2 \pi)$, and since $e^{\alpha 2 \pi} =1 $, we must have $\cos(b) = 1, i \sin (b) = 0$, so $b 2\pi = n 2\pi$, where $n$ is a nonnegative integer

so $\alpha = in$ $\implies$ $-\lambda = -n^2$ $\implies$ $\lambda = n^2$ 

so we end up with
$$
(6) \qquad \lambda = n^2 \qquad \text{and} \qquad \Theta(\theta) = A \cos n \theta + B \sin n\theta \qquad (n = 1, 2, \ldots)
$$
**there is a solution with ** $n = 0$: $\Theta(\theta) = A$ 

Solving $(4)$: [Euler type][https://en.wikipedia.org/wiki/Cauchy%E2%80%93Euler_equation] with solutions of the form $R(r) = r^{\alpha}$ 
$$
(7) \qquad (\alpha)(\alpha -1)r^{\alpha} + \alpha r^{\alpha} - n^2 r^{\alpha} = 0
$$
whence $\alpha = \pm n$ which are Distinct roots, so solution is $R(r) = Cr^n + Dr^{-n}$ 
$$
(8)\qquad u = (Cr^n + \frac{D}{r^n})(A \cos n \theta + B \sin n\theta)
$$
for $n = 1, 2, 3, …$. In case $n = 0$, we have a repeated real root, so 
$$
R(r) = C  + D\ln (r)
$$
**Applying Boundary conditions to $r$ variable** 

The requirement that the solutions are finite is the **"boundary condition at $r = 0$ "**

#### Solution

$$
(10) \qquad u = \frac{1}{2}A_0 + \sum_{n=1}^\infin r^n (A_n \cos n\theta + B_n \sin n\theta)
$$

Finally: inhomogeneous BCs at $r = a$ 

$h(\theta) = \frac{1}{2}A_0 + \sum_{n=1}^\infin a^n (A_n \cos n\theta + B_n \sin n\theta)$ 

This is the Full Fourier Series for $h(\theta)$ 

Using what we know about Fourer Series:

$l = \pi$, so 
$$
\begin{align}
(11) &&A_n &= \frac{1}{\pi a^n}\int_0^{2\pi}h(\phi)\cos n \phi \; d\phi
\\(12)&& B_n &= \frac{1}{\pi a^n}\int_0^{2\pi}h(\phi)\sin n \phi \; d\phi
\end{align}
$$
**(10) through (12)** is the solution to the Laplacian equation with a boundary condition on a circle

* Plug (11) and (12) into (10) then use the fact that $\cos(n(\theta - \phi)) = \cos n\theta \cos n \phi - \sin n\theta \sin n\phi$ 
* we obtain an integral with a geometric series inside it
* then use that $e^{in(\theta - \phi)} + e^{-in(\theta- \phi)} = 2\cos n(\theta- \phi)$ 
* This:

$$
\sum_{n=0}^\infin (\frac{r}{a})^n e^{in(\theta- \phi)} - 1= \frac{1}{1 - (r e^{in(\theta-\phi)})/a}-1 = \frac{a - (a-re^{in(\theta-\phi)})}{a - re^{in(\theta-\phi)}}
$$

##### Poisson's Formula

$$
(13) \qquad u(r,\theta) = (a^2-r^2)\int_0^{2\pi}\frac{h(\phi)}{a^2 - 2ar \cos (\theta - \phi)+r^2}\frac{d \phi}{2\pi}
$$

This can replace $(10)- (12)$, and it expresses any harmonic function inside a circle in terms of its boundary values

$\bold x = (x,y)$ is a point with polar coordinates $(r, \theta)$. $\bold x$ is the vector from the origin $\bold 0$ to the point $(x,y)$. and $\bold x'$ is a point on the boundary, so has coordinates $(a, \phi)$  

$|\bold x- \bold x'|^2 =a^2 + r^2 - 2ar \cos(\theta - \phi)$ by law of cosines:

if $A, B, C$ are  the sides of a triangle, and $\alpha$ is the angle between $A$ and $B$, then $|C| = |A|^2 + |B|^2 - 2|A||B| \cos \alpha$ 

The (change in?) arc length: $ds' = a d \phi$ Note:  arc length = (radius)(radian)
$$
(14) \qquad u(\bold x) = \frac{a^2 - |\bold x |^2}{2\pi a}\int_{|\bold x'| = a}\frac{u(\bold x')}{|\bold x - \bold x'|^2}\; ds'
$$
for $\bold x \in D$ we write $u(\bold x') = h(\phi)$ 

#### Theorem 1. 

Let $h(\phi) = u(\bold x')$ be any continuous function on the circle $C = \partial D$. Then the Poisson formula $(13)$ or $(14)$ provides the only harmonic function in $D$ for which
$$
(15)\qquad \lim_{\bold x \rightarrow \bold x_0} u(\bold x) = h(\bold x_0) \qquad \text{for all }\bold x_0 \in C
$$

* $u(\bold x)$ is a continuous function on $\overline D = D \cup C$ 
* $u(\bold x)$ Is differentiable to all orders inside $D$ 

### Mean Value Property

Let $u$ be a **harmonic** function in a disk $D$, continuous in closure $\overline D$. Then: *the value of $u$ at the center of $D$ equals the average of $u$ on its circumference*

Proof: just put $\bold x = \bold 0$ in $(14)$ or $r = 0$ in $(13)$ 

#### Maximum Principle

**complete proof of its strong form** 

Let $u(\bold x)$ be harmonic in $D$. The maximum is attained somwhere (by continuity of $u$ on $\overline D$) at $\bold x_{M} \in \overline D$. Have to show $\bold x_M \not \in D$ **unless** $u \equiv \text{constant}$. 

* By definition of $M$: $u(\bold x) \leq u(\bold x_M) = M$ for all $\bold x \in D$ 
* Draw a circle around $\bold x_M$ contained in $D$ 
* Use Mean Value Property to say $u(\bold x_M) = $ the average of $u$ around the circumference of the circle around $\bold x_M$ 
* Since the average is no greater than the maximum, we have:

$$
M = u(\bold x_M) = \text{average on circle} \leq M
$$

Therefore, all $\bold x$ on the circumference: $u(\bold x) = M$ 

* And this is true for any such circle around $x_M$, so $u(\bold x) = M$ for all $\bold x$ in the largest circle contained in $D$ centered on $\bold x_M$ 
* We repeat the process with a different center
* And we obtain two types of open sets: those that have $u(\bold x) = M$ and those that have $u(\bold x) < M$ 
* Then by the connectedness of $D$, i.e., $D$ cannot be a partition of two open sets, $u(\bold x) \equiv M$ throughout $D$, so $u \equiv$ constant

### Differentiability

Let $u$ be a harmonic function in any open set $D$ of the plane. Then $u(\bold x) = u(x,y)$ possesses all the partial derivatives of all orders in $D$ 

1. First case: disk with its center at origin, Use Poisson's formula in its second form
   1. integrand is differentiable to all orders for $\bold x \in D$ (why? OH! because it is just w/ respect to $\bold x$) while $u(\bold x')$ is a different story.
      1. I suppose the Riemann sum…?
      2. $\frac{\partial}{\partial \bold x}\sum_{\bold x_i' \in \partial D} \frac{u(\bold x_i')}{|\bold x - \bold x'_i|^2}​$ 
   2. by Section A.3, we can differentiate under the integral sign, so $u(\bold x)$ is differentiable to any order in $D$ 
2. Second case: any domain
   1. let $\bold x_0 \in D$ with $B$ a disk contained in $D$ with center $\bold x_0$, We showed already that $u(\bold x)$ is differentiable inside $B$, and hence at $\bold x_0$. But $\bold x_0$ is an arbitrary point in $D$, so $u$ is differentiable at all points of $D$ 

### Proof of The Limit (15)

$$
(16)\qquad u(r, \theta) = \int_0^{2\pi} P(r, \theta - \phi) h(\phi)\frac{d \phi}{2\pi}
$$

for $r < a$
$$
(17)\qquad P(r, \theta) = \frac{a^2 - r^2}{a^2 - 2ar \cos \theta + r^2} = 1 + 2 \sum_{n=1}^\infin (\frac{r}{a})^2 \cos n\theta
$$
Above is called the **Poisson Kernel** 

* $P(r, \theta) > 0$ for $ r < a$. 
  * $a^2 - 2ar \cos \theta + r^2 \geq a^2 - 2ar + r^2 = (a-r)^2 > 0$ 
* $\int_0^{2\pi} P(r, \theta) \frac{d \theta}{2\pi} = 1$ 
  * $\int_0^{2\pi} \cos n\theta \; d\theta = 0$ for $n = 1, 2, 3, …$ 
* $P(r, \theta)$ is a **harmonic function** inside the circle. 
  * each term $(r/a)^n \cos n\theta$ is harmonic, and the sum of harmonic functions is harmonic

$$
u_{rr}+ \frac{1}{r}u_r + \frac{1}{r^2}u_{\theta \theta} = \int_0^ {2\pi}(P_{rr}+ \frac{1}{r}P_r + \frac{1}{r^2}P_{\theta \theta})(r, \theta - \phi)h(\phi)\frac{d \phi}{2\pi}
$$

$ = \int_0^{2\pi} 0 \cdot h(\phi) d\phi = 0$ 

So $u$ is harmonic in $D$

FIx $\theta_0$, consider radius $r$ near $a$
$$
(18)\qquad u(r, \theta_0) - h(\theta_0) = \int_0^{2\pi}P(r, \theta_0 - \phi)[h(\phi)- h(\theta_0)]\frac{d \phi}{2\pi}
$$
using proeprty $(ii)$ of $P$. 

The value of $P$ gets larger when $a^2 - 2ar\cos\theta + r^2$ is smaller, which is when $\cos \theta = 1$ at $\theta = 0$ 

More precisely: $\delta \leq \theta \leq 2\pi - \delta$ 
$$
(19)\qquad |P(r, \theta)| = \frac{a^2 - r^2}{a^2 - 2ar \cos \theta + r^2} = \frac{a^2 - r^2}{(a-r)^2 + 4ar \sin^2 (\theta/2)} < \epsilon
$$
using that $-2ar \cos \theta = -2ar(\cos^2 (\theta/2) - \sin^2(\theta/2)$ 

$= -2ar(1 -1 + \cos^2(\theta/2) - \sin^2(\theta/2)) = -2ar(1 - 2\sin^2(\theta/2)) = -2ar + 4 ar \sin^2(\theta/2))$

And $(19)$ is true for $r$ sufficiently close to $a$ 

More More Precisely, for each small $\delta > 0$ and each small $\epsilon > 0$, $(19)$ is true for $r$ sufficiently close to $a$ 

Using Property $(i)$, (18), and (19):
$$
(20) \qquad |u(r, \theta_0) - h(\theta_0)|\leq \int_{\theta_0 - \delta}P(r, \theta_0 - \phi)\epsilon \frac{d \phi}{2\pi} + \epsilon \int_{|\phi- \theta_0| > \delta}|h(\phi)- h(\theta_0)| \frac{d \phi}{2\pi}
$$

* $(18)$ gives the integral we work off of

* $(19)$ gives the epsilon in the second integral

* $(i)$ lets us take the absolute value of the first integral

* $\phi < \theta_0 - \delta$ $\implies$ $\theta_0 - \phi > \delta$ 

* $2\pi \geq \phi > \theta_0 + \delta$ $\implies$ $\theta_0 - \phi < 2\pi - \delta$ 

Now: since $|h| \leq H$ for some constant $h$ and using property $(ii)$

From $(20)$: 
$$
|u(r, \theta_0)- h(\theta_0)| \leq \epsilon + 2H\epsilon
$$
for sufficiently small $|r - a|$ , the above is true for any $\epsilon, \delta $ 

**Confusing**: 

Why the $\delta$? To get an inequality  in (20)?

So $(19)$ is true for any $\delta, \epsilon > 0$ provided that $r$ is close enough to $a$ 

and then this leads to us haveing the inequality in $(20)$

cause we know we can use $|h(\phi) - h(\theta_0) |\leq \epsilon$ for a certain window around $\theta_0$, so for $\phi$ near $\theta_0​$  

But then we still have $\int P(r, \theta_0, \phi) |h(\phi) - h(\theta_0)| \epsilon \frac{d\phi}{2\pi}$, which is why we need inequality $(19)$ for the integral over $\phi$ s.t. $|\theta_0 - \phi|> \delta$ 

Also **note**: $P(r, \theta)$ is at its max around $\theta = 0, 2\pi$ 
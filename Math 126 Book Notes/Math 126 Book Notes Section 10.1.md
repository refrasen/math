# Math 126 Book Notes Section 10.1

## 10.1 Fourier's Method, Revisited

solve the **wave** and **diffusion** equations
$$
u_{tt} = c^2 \Delta u \qquad \text{and}\qquad u_t = k \Delta u
$$
in *any* bounded domain $D$ with one of the classical homogeneous conditions on $\partial D$ and with the standard initial condition. We denote
$$
\Delta = \frac{\partial^2}{\partial x^2}+ \frac{\partial ^2}{\partial y^2} \quad \text{or}\quad \frac{\partial^2}{\partial x^2}+ \frac{\partial ^2}{\partial y^2}+ \frac{\partial ^2}{\partial z^2}
$$
in two or three dimensions. 

**First step**: Separate Time variable only:
$$
(1)\qquad u(x, y, z, t)= T(t)v(x,y,z)
$$
**Second Step**: ODEs for Time part
$$
(2)\qquad - \lambda = \frac{T''}{c^2 T}= \frac{\Delta v}{v}\quad \text{or}\quad -\lambda = \frac{T'}{kT} = \frac{\Delta v}{v}
$$
depending on which equation.

In either case:

**Third step**: Eigenvalue Problem (for both equations)
$$
(3)\begin{align}
-\Delta v = \lambda v \quad \text{in }D
\\ v \text{ satisfies (D), (N), (R) }\quad \text{on }\partial D
\end{align}
$$
Therefore, if this problem has eigenvalues $\lambda_n$ (all positive, say) and eigenfunctions $v_n(x, y, z)= v_n(\bold x)$, then the solutions of the 

**wave equation** are
$$
(4) \qquad u(\bold x, t) = \sum_n [A_n \cos (\sqrt{\lambda_n}ct)+B_n \sin(\sqrt{\lambda_n}ct)]v_n(\bold x)
$$
**diffusion equation** are
$$
(5) \qquad u(\bold x, t)= \sum_n A_n e^{-\lambda_n k t}v_n (\bold x)
$$
coefficients will be determined by the initial conditions

In three dimensions the index $n$ in the sums $(4)$ and $(5)$ will be a *triple index* [$(l, m, n)$]

and the various series will be *triple series*, one sum for each coordinate

### Orthogonality

**inner product**: 
$$
(f, g)= \iiint_D f(\bold x)\overline{g(\bold x)}\, d \bold x
$$
as a triple integral (in 2 dim, a double integral)

if $\nabla \cdot $ denotes the divergence, the indentity:
$$
(6)\qquad u(\Delta v) - (\Delta u)v 
\\ = \nabla \cdot(u \nabla v)- \nabla u \cdot \nabla v - [\nabla \cdot(v \nabla u) - \nabla u \cdot \nabla v] 
\\ = \nabla \cdot [u(\nabla v) - (\nabla u)v]
$$
It is integrated over $D$. Using the divergence theorem:
$$
(G2)\qquad \iiint_D [u \Delta v - v\Delta u]\, d \bold x = \iint_{\partial D} (u \frac{\partial v}{\partial n} - \frac{\partial u}{\partial n}v)\, dS
$$
noting that $\partial u/\partial n = n \cdot \nabla u$ 

if $u$ and $v$ both satisfy homogeneous Dirichlet conditions ($u = v = 0$ on $\partial D$), the surface integral must vanish. (the same is true for Neumann or Robin boundary conditions)

For instance: if
$$
\frac{\partial u}{\partial n} + au = 0 = \frac{\partial v}{\partial n} + av \qquad \text{on }\partial D
$$
then $u(\partial v/\partial n)- (\partial u/\partial n)v = - uav + auv = 0$ 

We therefore say that **each of the three classical BCs is symmetric** since in each case:
$$
(u, \Delta v)= (\Delta u, v) \text{ for all functions that satisfy the BCs}
$$
(because $\int_D u \Delta v - v \Delta u \,  d \bold x =0$ with these BCs)

Now suppose that both $u$ and $v$ are real eigenfunctions: 
$$
(7) \qquad -\Delta u = \lambda_1 u \quad \text{and}\quad -\Delta v = \lambda_2 v
$$
where $u$ and $v$ both satisfy (D) or (N) or (R) on $\partial D$. By (G2):
$$
(8)\qquad (\lambda_1 - \lambda_2)(u, v) = (u, \Delta v)- (\Delta u, v) = 0
$$
Therefore, $u$ and $v$ are orthogonal provided that $\lambda_1 \neq \lambda_2$ 

As in Section 5.3, a similar argument shows that all the eigenvalues are necessarily real. We summarize these observations in the following theorem:

#### Theorem 1.

Consider any one of the problems (3). Then all the eigenvalues are real. The eigenvalues can be chosen to be real valued. The eigenfunctions that correspond to distinct eigenvalues are necessarily orthogonal. All the eigenfunctions may be chosen to be orthogonal. 

### Multiplicity

An eigenvalue $\lambda$ is *double* (*triple,* $\ldots$)if there are two (three, $\ldots$) linearly independent eigenfunctions for it. It has *multiplicity* $m$ if it has $m$ linearly-independent eigenfunctions. In other words, the "eigenspace" for $\lambda$ has dimension $m$ 

So for such an eigenvalue, let $w_1, \ldots, w_m$ be linearly independent eigenfunctions. They are not necessarily orthogonal. 

However, we can always choose a new set of eigenfunctions that *is* orthogonal. 

**Gram-Schmidt Orthogonalization Method**: 

Let $w_1, \ldots, w_m$ be any (finite or infinite) set of linearly independent vectors in any vector space $V$ that has an inner product. 

First: we normalize:
$$
u_1 = \frac{w_1}{\|w_1\|}
$$
Second: we subtract from $w_2$ the component parallel to $u_1$ and then normalize

That is: 
$$
(9) \qquad v_2 = w_2 - (w_2, u_1)u_1\quad \text{and}\quad u_2 = \frac{v_2}{\|v_2\|}
$$
That $u_2$ and $u_1$ are orthogonal is easy to see either from a sketch or a calculation.  

Third: subtract off from $w_3$ the component in $u_1u_2$ plane and then normalize:
$$
(10) \qquad v_3 = w_3 - (w_3, u_2)u_2 - (w_3, u_1)u_1 \quad \text{and}\quad u_3 = \frac{v_3}{\|v_3\|}
$$
and so one. At each stage we subtract off the components in all the previous directions

Then $\{u_1, u_2, u_3, \ldots \}$ is an orthogonal set of vectors. 

![image-20190421030456023](Math 126 Book Notes Section 10.1.assets/image-20190421030456023.png)

### General Fourier Series

Because of Theorem 1, we can talk about general Fourier series which are made up of the eigenfunctions in $D$ 
$$
(11) \qquad \phi(\bold x) = \sum_n A_n v_n(\bold x)
$$
where $v_n(\bold x)$ denote orthogonal eigenfunctions of $(3)$, then 
$$
(12) \qquad A_n = \frac{(\phi, v_n)}{(v_n, v_n)} = \frac{\iiint_D \phi(\bold x)\overline{v_n(\bold x)}\, d \bold x}{\iiint_D |v_n(\bold x)|^2\, d \bold x}
$$
The question of the positivity of the eigenvalues is addressed in this next theorem:

#### Theorem 2.

All the eigenvalues are positive in the Dirichlet case. All the eigenvalues are positive or zero in the Neumann case, as well as in the Robin case $\partial u/\partial n + au = 0$ provided that $a \geq 0$. 

##### Proof. 

We use **Green's First Identity (G1)** with $u \equiv v$, namely:
$$
\iiint_D (-\Delta v)\overline{v}\, d \bold x = \iiint_D |\nabla v|^2\, d \bold x - \iint_{\partial D}\overline{v}\frac{\partial v}{\partial n}\, dS
$$
In the Dirichlet case, with $v$ an **eigenfunction** of $(3)$ we obtain:
$$
\lambda \iiint_D |v|^2\, d\bold x = \iiint_D |\nabla v|^2\, d \bold x \geq 0
$$
The lat integral cannot be zero:

If it were $\implies$ $\nabla v(\bold x)$ would be identically zero and $v(\bold x) \equiv C$ would be a constant function and because of the boundary condition, $v(\bold x) \equiv C = 0$ (which cannot be an eigenfunction)

Therefore, $\lambda > 0$ in the Dirichlet case. 

---

##### Completeness (Chapter 11)

Completeness is always true as long as the boundary $\partial D$ of the domain is not too wild

Complteness in the mean-square sense for (1) means
$$
(13)\qquad \| \phi - \sum_{n \leq N}A_n v_n \|^2 = \iiint_D |\phi(\bold x) - \sum_{n\leq N} A_n v_n (\bold x)|^2 \, d \bold x \rightarrow 0
$$
as $N \rightarrow \infin$ 

So wave/diffusion problem with boundary and initial conditions is reducible to the eigenvalue problem (3). 

**But** we are still left with finding solution of (3)

### Example: Cube

$Q = \{ 0 < x < \pi, 0 < y < \pi, 0 <z< \pi \}$ and solve the problem:
$$
(14)\qquad \begin{align}
&\text{DE: }u_t = k \Delta u \quad &\text{in }Q
\\ &\text{BC: }u = 0 \quad &\text{ on }\partial Q
\\ &\text{IC: }u = \phi(\bold x) \quad &\text{when }t = 0
\end{align}
$$
Separating the time variable as in the general discussion above, we are led to the eigenvalue problem
$$
(15) \qquad -\Delta v = \lambda v \quad \text{in }Q, \qquad v = 0 \quad \text{on }\partial Q
$$
Because the sides of $Q$ are parallel to the axes, we can successfully separate the $x$, $y$, and $z$ variables: $v = X(x)Y(y)Z(z)$ 
$$
\frac{X''}{X} + \frac{Y''}{Y} + \frac{Z''}{Z} = -\lambda
$$
The separated BCs are
$$
X(0) = X(\pi)= Y(0) = Y(\pi)= Z(0) = Z(\pi) = 0
$$
Clearly, the solutions are
$$
(16) \qquad v(x,y,z) = \sin lx \sin my \sin nz = v_{lmn}(\bold x)
$$
Where 
$$
(17) \qquad \lambda = l^2 + m^2 + n^2 = \lambda_{lmn}\qquad (l \leq m, l, n < \infin)
$$
Note the triple index! Therefore:
$$
(18) \qquad u(\bold x, t) = \sum_n \sum_m \sum_l A_{lmn}e^{-(l^2 + m^2 + n^2)kt}\sin lx \sin my \sin nz
$$
Orthogonality in Example 1 implies that:
$$
(19) \qquad A_{lmn}= (\frac{2}{\pi})^3 \int_0^\pi \int_0^\pi \int_0^\pi \phi(x,y,z)\sin lx \sin my \sin nz \, dx \, dy\, dz
$$
Orthogonality of the vunctions $v_{lmn}(x,y,z)$ is, in this case, a direct consequence of the *separate* orthognalities of the separated eigenfunctions, $\sin lx, \sin my, \sin nz$ 

Namely:
$$
\iiint_Q v_{lmn}(\bold x)v_{l'm'n'}(\bold x)\, d \bold x = (\int_0^\pi \sin lx \sin l'x\, dx)(\int_0^\pi \sin my \sin m'y \, dy)(\int_0^\pi \sin nz \sin n'z\, dz) = 0
$$
unless all three indices exactly match 

 
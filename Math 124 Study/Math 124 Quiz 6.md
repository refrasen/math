# Math 124 Quiz 6

## Homework 11: MyPoly

Notes:

* Remember that for Structs, they should handle all types of coefficient vectors 
* `eltype`: useful to ensure the right type

Remember our operators: `+`, `-`, `*`, `/` 

Interesting Note: **Dividing Polynomials**. Long division

### Problem 1

#### Problem 1(a)

Overloaded `*` operator using 

```julia
function Base.:*(p1:: Mypoly, p2::MyPoly)
  code
end
```

* used function `degree(p1 :: MyPoly)` 
* used function `eltype(array)` 

#### Problem 1(b)

This one wasy easy, just use the new overloaded operator from problem 1(a) to multiply polynomials of the form $(x - r_k)$, where $r_k$ is the root

the function was an overloaded `MyPoly` function

```julia
function MyPoly(; roots)
  code
end
```

#### Problem 1(c)

`differentiate(p::MyPoly)` 

* used `eltype` here to iniate a new coefficient array for a new MyPoly
* Used `degree` function for same purpose as above

#### Problem 1(d)

`integrate(p::MyPoly)` 

* `eltype` again as well as `degree` 

### Problem 2: Lagrange Polynomials

#### Problem 2(a)

`LagrangePolynomials(s)`, where `s` is a vector of $n$ numbers...

#### Problem 2(b)

This was straight forward, just required

* `integrate` 
* `differentiate` 

## Homework 12: Graph

Note, SparseArrays utilized here

Notes:

The PyPlot `arrow(x[1], y[1], x[2]-x[1], y[2]-y[1])` will draw an arrow from `(x[1], y[1])`, `(x[2], y[2])` 

at some point: look through the preliminary functions and such

### Problem 1 - Graph to adjacency Matrix

`convert2adjmatrix(g::Graph)`: creating a sparse adjacency matrix for graph `g` 

what does `spy` mean?

### Problem 2 - Spanning Tree

```julia 
function spanning_tree(g::Graph, start)
  #code
end
```

* used the DFS method…which is recursive 

```julia
visited = falses(length(g.vertices))
    function visit(ivertex)
        visited[ivertex] = true
        for nb in g.vertices[ivertex].neighbors
            if !visited[nb]
                push!(gtree.vertices[ivertex].neighbors, nb)
                visit(nb)
            end
        end
    end
```

### Problem 3 - Word Ladder

* used the `shortest_path_bfs` function

First part of code:

creating list of words from the word file that are the same length as the first and last word of our word ladder

* this required file processing

Second Part of Code

Initializing a graph with as many vertices as there are words sort of associating each word with the integer vertex matching their index in words list

* had a hard time with how to define graph so be careful
* You initialize Graph with an array of vertices, where each vertex is associated with 
  * an array of neighbors
  * an (optional) array for coordinates

Third part of Code

Go through and give each word edges to words it only differs by one letter with

* used `findall` to find all indices of the word[i] that do not equal word[j]
  * only works cause we do consider word order, not just unique letters and they are the same length
* had to be careful to use `collect` on the string in order to be able to compare
* needed to add to the neighbors of each word/vertex whenever there was such a word that only differed by one letter

Fourth part of code

used `shortest_path_bfs(g, start, end)`

careful: need to remember that each word is associated with an index

* used `findfirst(words.== first_word)` to find the vertex for the first_word and similalrly for the last_word

Last Part of Code

Printed out the words returned by shortest_path, which returns an array of indices that go in order from start to end

* be careful that you had to print out `words[element_in_path]`

## Project 5 - Neural Network

Just try to understand a little bit of the gradient descent method

# Hopefully: Lecture Notebooks 9, E, F, G

`Base.show(io::Io, struct? )`

Test out how much you can fuck with a struct, in particular arrays

functions on types are a thing

## 9 - Types, Structs , Objects

Keyword `struct` used to create a composite type/user-defined type:

Beware, might have to define a new object of that type when wishing to alter it. 

**type delcaration**`::` operator

`argument::type` or `parameter::type` 

**Customized printing** with `Base` package function `show` 

```julia
function show(io::IO, x::mytype)
  #printstuff?
end
```

**Callable objects** 

so:

```julia
function (name::type)(args)
end
```

Overloading operators, we know

```Julia
function Base.:(operator)(args)
  code
end
```

## E - Optimization

Idea: maximize/minimize the value of a function subject to constraints

### Gradient-Based Optimizers

without constraints

Package: `Optim` 

#### First-Order Method

##### First: Function $f: X^d \rightarrow \R$ 

$f(\bold x)$: function of a vector $\bold x$ 

##### Second: Gradient $\nabla f$ 

Straightforward, the derivatives

although might be hard to define at times

##### Third: Gradient Descent

for any given value of $\bold x$, the negative gradient $- \nabla f(x)$ gives the direction of the fastest decrease of $f(x)$ 

cause, $|u| = 1$,  $D_u f$ $ = \nabla f \cdot u$ $= |\nabla f| |u| \cos \theta$, which will be the greatest when $\cos \theta = 1$, i.e., when $\theta = 0$, so $u$ is in the direction of $\nabla f$ 

###### Assume:

$\exists \alpha $ Scalar, s.t. $f(\bold x - \alpha \nabla f(\bold x)) < f(\bold x)$ i.e, going in the - direction of $\nabla f(\bold x)$ will eventually lead us to being smaller than $f(\bold x)$ 

###### Initial guess $x_0$ and iterate based on assupmtion

$$
x_{k+1} = x_k - \alpha _k \nabla f(x_k)
$$

**First Iteration**: set $\alpha_k$ to be constant and have $\| \nabla f(x)\|_2$ for the termination criterion

* Initialize $x = x_0$ 
* Iterate until some max iteration just incase or until termination criterion 
  * so calculate gradient and check if it meets the criteria
    * terminate then and return $x$ 
    * or set $x = x - \alpha * \nabla f(x)$ and repeat the loop

**Second Iteration**: Line Searches to determine $\alpha_k$ 

Try to minimize the one-dimensional function: 

$f(\alpha) = f(x_k - \alpha \nabla f(x_k))$ 

Our method: increase $\alpha$ by factors of 2 as long as $f$ decreases

* First: check if $\alpha$ goes beyond how big we want it to be (to prevent infinite loop)
* second: define $f_{k+1} = f(x_k - \alpha \nabla x_k)$ 
* check if $f$ is decreasing or increasing: $f_k > f_{k+1}$ or $f_{k+1} \leq f_k$ 
* multiply $\alpha *= 2$ accordingly

So the gradient descent method looks identical to the first iteration

except here, we now change $\alpha$ with line_search and our current $x_k$ before definine $x_{k+1}$ 

## F - Graphs

### Defining the Type

Graphs are defined in terms of 

* `neighbors`: that is whether the two vertices have a directed edge between them
  * for undirected, have that if $a$ is a neighbor of $b$, $b$ is a neighbor of $a$ 
* optional: coordinates

### Graph Algorithms

#### Depth First Search

* starting from a vertex `start` and keep track of visited vertices
* recursively visit each of its neighbors 
  * so it will "finish each path" before looking at other paths going from `start` 
* we keep track of visited vertices, because we don't need to go down that subpath again if we've already been there, i.e., if two paths join at a subpath, we don't need to go through that subpath more than once since we know where it leads

### Breadth First search

visits all of the neighbor vertices at the present depth before moving on to the vertices at the next depth level: non-recursive, use a **queue**: an array where new elements are added at the end, but elements are extraced from the beginning

`popfirst!(array)` function returns the first element of an array while also removing that element from the array

## Sparse Arrays

| Sparse            | Dense           | Description                                    |
| :---------------- | :-------------- | :--------------------------------------------- |
| `spzeros(m,n)`    | `zeros(m,n)`    | m-by-n matrix of zeros                         |
| `sparse(I, n, n)` | `Matrix(I,n,n)` | n-by-n identity matrix                         |
| `Array(S)`        | `sparse(A)`     | Interconverts between dense and sparse formats |
| `sprand(m,n,d)`   | `rand(m,n)`     | m-by-n random matrix (uniform) of density d    |
| `sprandn(m,n,d)`  | `randn(m,n)`    | m-by-n random matrix (normal) of density d     |

`A = sparse(rows, cols, vals)` 

* rows are row Indices
* cols are column indices
* vals are the stored values
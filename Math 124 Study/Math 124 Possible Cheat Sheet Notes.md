

# Math 124 Possible Cheat Sheet Notes

## Arithmetic

Order of operations: 

* parentheses
* power (right to left)
* mult/div (left to right)
* add/sub (left to right)

## Variables and Assignments

variable refers to a value, and variable names cannot begin with a number, are case sensitive 

Assignment statement: `variable_name = expression`, creates new variable, gives it the value of the **evaluated** expression

## Updating Operators

`+= -= *= /= \= \div = %= ^= ` 

## Numerical Literal Coefficients

variables can be immediately preceded by a numeric literal, implying multiplication. 

Works as expected but note: `2^2x = 2^(2x)` and `360/2pi = 360/(2pi)` 

## Functions

**function**: named sequence of statements that performs a computation

**call** a function by name, *optionally* passing an expression in parentheses as an **argument** 

`exp(x)`: $e^x$, `sign(x)` gives sign, `log(b,x)` base `b` logarithm of `x`

**Trig**: e.g.: sin, sinh, asin, asinh, sinc(?)

**multiple return values:** seperate by commas

## For-Loops

```julia
for i = start:step:stop
#this will be repeated until start+ k*step reaches end
end
```

or alternatively

```julia
for i in array
    #will repeat for length(array) times for each element in the array
end
```

## Conditionals

### Boolean Expressions

`true` and `false` are of `type` Bool

`==`, `!=`, `<`, `<=`, `>`, `>=` (comparison between float and integers works as expected)

**Warning**: `=` is an assignment operator, `==` is a comparison operator

### Logical Operators

`&& (and)`, `|| (or)`, `! (not)`, `&&` has higher precedence

`true||false&&false == trule||(false&&false)` and `true&&false||true == (true&&false)||true`  and

`true&&false&&false||true == ((true&&false)&&false)||true` 

### If-statement

```julia
if x > y
    #code
elseif x < y
    #code
else
    #code
end
```

* whatever is true will be run and we skip the rest of the conditional block. Example: if x > y, we don't compare if x < y 

### Short-Circuit

`a && b`, b is only looked at if `a` is true

`a || b`, b is only looked at if `a` is false

* Helpful for error handling… i.e., we want to run a piece of code *only if* it can take that argument. like we'll do `sqrt(x)` only if `x >= 0` 

## While-Loops

Notes: 

* condition might be false the first time: never evaluated
* condition might always be true: infinite loop
* might need some variable to determine the condition which has to be initialized before the loop and updated inside the loop

### Break-Statement

* can check the condition in the loop and then break rather than wait for the loop to end to see if we can stop

### Continue Statement

* when `continue` is encountered the code will immediately go to the next iteration and start at the beginning
* convenient to quickly skip an iteration without terminating the repetitions

## Arrays

### 1-d arrays

##### **First way to create an array**

`[element1, element2, element3, …, elementn]` 

#### Types

Can force a vector to be a certain type using for exapmle, `Float64[element1, …, elementn]` 

#### Functions

`range(start, stop = stop, length = n)`: range of n linearly spaced elements from `start` to `stop` 

`push!(array, element1, element2, ..., elementn)`: adds element1, element2, …, elementn to the end of array 

`append!(z,y)`: adds the elements of second array to the end of the first

#### Accessing elements

`array[index]= expression`: set the index-th element of array to the evaluated expression

#### Traversing an array

`floor(Int, a)`, the largest integer that is less than or equal to `a` 

#### Dot-syntax 

`function.(array1, array2, …, arrayn)`, will perform `function` `length(array1)= ... = length(arrayn)` times elementwise i.e., will return an array with elements `function(array1[i], array2[i], … , arrayn[i])` for `i` = 1, 2, 3, …, length of the arrays… 

#### Array Slices

`array[start_index:step:stop_index]`, or more generally `array[int_vector]` will return the elements of array corresponding to the indices in `int_vector`

`array[:]`: all elements

`collect` can make a range an explicit array

##### Weird thing:

If we have an array of integers: 
$$
A = \begin{pmatrix} a_{11} & a_{12} & \cdots & a_{1n} \\ \vdots& \vdots &\cdots & \vdots \\ a_{m1} & a_{m2} &\cdots &a_{mn} \end{pmatrix}
$$
then taking a **1d** array `x`: 

```julia
A = [a11 a12 ... a1n; ... ; am1 am2 ... amn]
x[A] = [x[a11] x[a12] ... x[a1n]; ... ; x[am1] x[am2] ... x[amn]] 
```

or equivalently 
$$
x[A] = \begin{pmatrix} x[a_{11}] & x[a_{12}] & \cdots & x[a_{1n}] \\ \vdots& \vdots &\cdots & \vdots \\ x[a_{m1}] & x[a_{m2}] &\cdots &x[a_{mn}]\end{pmatrix}
$$

```julia
x[[1 2 3; 4 5 6]] == [x[1] x[2] x[3]; x[4] x[5] x[6]]
```

will return true

#### Arrays are passed by sharing

when assigning arrays to **new** variables or passing them to a function, they still refer to the same array, therefore, any changes that are made to a variable referencing an array will be made to all other variables referencing that same array.

`copy` is when you want to make a true copy.

### Multi-dimensional Arrays

##### First way to construct

`A = [element1 element2 element3; element4 element5 element6]`: makes a 2d array of size 2 by 3

#### Accessing elements

**seperate by commas** 

`length(A)`: total elements in array 

`size(A, dim)` tells you the size of the array along a given dimension

#### Concatenation

`horizontal`: spaces, and number of rows must match

`vertical`: colons, and number of columns must match

## Plotting

have to specify `using PyPlot` 

`plot(x,y)`: draws straight line between x, y-coordinates 

`axis("equal")`: equally scaled so a square looks like a square...

`grid(true)` includes grid lines

`axis("off")`: takes off axis completely 

`xlabel("label of x axis"`, `ylabel("label of y axis)`, `title("titel of graph")` 

`legend(("name of first plot", "name of second plot", "name of third plot"))`

## Random Numbers

`rand(n)` generates `n` pseudo-random numbers from a uniform distribution on `[0, 1]` 

* `rand()` will give a scalar
* `rand(1)` will be a 1-element array

`using Random`: allows us to use `Random.seed!(seed_number)`, and anytime we use `rand()` after wards, we are using it from a predetermined list...

i.e., we'll have a list of numbers [r1, r2, ….]

and rand(n) will give us [r1, …, rn], and using rand() right after will give r_(n+1)

### Other intervals

Using $a + x(b-a)$, so:

`a .+ (b-a)rand(n)` will generate n uniform random numbers in `[a, b]` 

### Normal Distributions

`randn` draws random numbers from a normal distribution with mean `0` and standard deviation `1`

so with a mean `mu` and standard deviation `sigma`

`mu .+ sigma*randn(n)` will generate `n` numbers… which are more likely to lie within sigma of mu

larger sigma will lead more varied numbers… so sigma tending towards infinity makes us tend to a uniform distribution, and sigma tending toward 0 will mean we just be generate mu n times...

### Visualization with Histograms

bar graph… `y` axis: counts/frequencies, `x` axis is the outcomes

can turn `y` axis into probability by dividing counts/frequncies by total number of trials

### General Histogram into bins

`plt[:hist](outcomes, number_of_bins)` 

we'll have number_of_bins equally sized bins, and then count how many values occur in each bin in `outcomes` 

so sort of going through `outcomes` and seeing what "bin" they go into, counting how many times an element of `outcomes` is in a `bin` 

### Estimate an area

$\frac{\mathrm{hits}}{n} \approx \frac{\pi r^2}{(2r)^2}$ to get the ration between areas

for a positive function lying inside a rectangle or whatever, 

we'll throw darts at the rectangle to get a point $(x,y)$ and if $y < f(x)$, we count that as successful

### Random Walk

Consider 50/50 chance we go horizontal or vertical

then within horizontal or vertical a 50/50 chance we go positive or negative direction

### Monte Carlo Simulations

* ntrials of finding how many rolls it took to get a six
  * a for loop through trials, a while loop until a six is found
  * outcomes will be of how many rolls it took to get a six throughout the trials
  * average will be taking the sum of how many rolls for each trial then dividing by the total number of trials

#### Simulating cards

**Warning**: be careful when using $\%$ and $\div$ because for a positive integer $n$, $\%$ will return from the set $\{0, 1, …, n-1\}$ and 

if we have $m$ groups of $n$, so $1, 2,\ldots, m, m+1,\ldots, 2m, \ldots, mn$ 

dividing these numbers with $\div$ will be from the set $\{0, 1, 2, …, m\}$ 

where $m$ is only for $mn$, 

so to make it start at $1$: `(number - 1) % n + 1` or `(number - 1) \div n + 1` 

##### `randperm(n)` 

is a good function which will give us a random permutation of integers from 1 to n

* always think of first shuffling (randperm(52)) then drawing the first 5 cards. 

##### `binomial(n, m)` 

this should equal $\frac{n!}{m!(n-m)!}$ 

## Comprehension

`A = [ F(x,y, …) for x=rx, y=ry, …]`

this will create an `length(rx)` by `length(ry)` array

#### Conditional

`A = [F(x,y, … ) for x=rx, y=ry if conditional]` 

* can force a data type on the array by putting the name of the data type before the brackets i.e., `Float64` `Int64`

* **Interesting Note**: if `true` or `false` is in an int or float array, it will become 1 or 0

## Generator Expressions

They're like conditionals, except instead of using square brackets, you use parentheses

* a rule that produces the requested values on demand without allocating an array to store them
* **Warning**: can't use indices in square brackets for them
* **can**: use a for loop to traverse them by using for example `x in gen` 

## More on Creating Arrays

`reshape(array, dim1, dim2, ... , dimn)`: takes data from one array and rearranges it into new dimensions

Elements are assigned column-wise: so it will go down columns of A and assign these to the columns of the new dimensions of A

if you want to assign by rows, do the transpose dimensions first then transpose that matrix

example: want a 3 by 5 matrix: do 5 by 3 first then transpose it

`permutedims` basically takes the transpose:

`permuteddims(reshape(array, dim2, dim1))` 

Note:

```julia
A = zeros(Int64, 2, 2, 3)
n = 1
for k = 1:size(A, 3) #fills in by 2d array
    for i = 1:size(A, 1) #fills in by row
        for j =  1:size(A, 2) #then goes by column
            A[i, j, k] = n
            n+=1
        end
    end
end
A
```

will give:

```julia
2×2×3 Array{Int64,3}:
[:, :, 1] =
 1  2
 3  4

[:, :, 2] =
 5  6
 7  8

[:, :, 3] =
  9  10
 11  12
```

##### `repeat` function

`repeat(array, n_1, n_2, … )` 

* First Note: if the array is of dim $m$, i.e., has dimension $d_1\times d_2 \times \ldots\times d_m $, then $n_1, n_2, \ldots n_m$ in repeat can be greater than 1. for $n_k$ with $k \geq m+1$, we must have $n_k = 1$ and that will add more dimensions...
  * except for one dimensional arrays… you can do whatever with that… it will return a $d_1n_1 \times n_2 \times \ldots$ array
* second note: $n_i$ tells us how many times we repeat along that dimension. if $i =1$, it tell us to vertically concatenate the array with itself $n_1$times
  * $i = 2$: horixontal concatenation $n_2$ times
  * so the new dimensions after repeat: $d_1n_1 \times d_2n_2$ (for 2d array case)

## Array Functions

##### `sum` function

will add all the elements in an array

`sum(array, dims = n)` says to add along the nth dimension

so if the array is of dimension $d_1 \times d_2 \times \ldots d_k$ 

and we want to add along the $i$-th dimension:

we'll end up with a $d_1 \times \ldots \times d_{i-1} \times 1 \times d_{i+1} \times \ldots \times d_k$ array

where the addition being done is sort of the form:

$A[index_1, … , index_{(i-1)}, 1, index_{(i+1)}, … , index_k] =  \sum_{n = 1}^{d_i} A[index_1, … index_{i-1}, n, index_{i+1}, …, index_k] $ simplest form:

$A$ is an $m$ by $n$ matrix:

```julia
sum(A, dims = 1) = [sum(A[:, 1]) sum(A[:, 2]) ...  sum(A[:, n])] #still a 2d array with 1 X n dimension

sum(A, dims = 2) = [sum(A[1, :]); sum(A[2, :]); ... ; sum(A[m, :])] #a 2d array with m X 1 dimension
```

##### `[:]` syntax

reduces any 2D array to a 1D array

##### `prod`

Same as sum, but with multiplication instead of addition

##### `maximimum` and `minimimum` 

`maximum(A)`/`minimum(A)` will simply find the largest/smallest element of A

`maximum(A, dims = n)`/`minimum(A, dims = n)`: will find the maximum/element element along the nth dimension

so along dims = 1 will go through each column and find the largest/smallest element among the row entries

so think of it as squishing the array along that dimension

and only keeping the max/min element instead of the whole 1d array along that dimension

so think of it as we have an array $A$ with dimension $d_1 \times d_2 \times \ldots \times d_k$ 

and we want to do it along the $i$-th dimension

then we do this: look at the elements

$A[j_1, j_2, \ldots, j_{i-1}, n, j_{i+1}, \ldots, j_k]$ for $n = 1, 2, \ldots, d_i$ 

with `sum` with add these elements, with `prod` we multiply these elements, with `maximum`/`minimum` we find the largest/smallest number and that becomes element $A[j_1, j_2, \ldots, j_{i-1}, 1, j_{i+1}, \ldots, j_k]$ 

##### `cumsum`, `cumprod`: keeps the array the same size

for 1d array, easy to see how it works

for 2d array, it's like doing it for the 1d array but multiple times and depending on the dimension

`cumsum(A, dims = n` or `cumprod(A, dims = n)` 

so think in terms of the previous section, but instead of squishing it into one scalar, we are doing a sort of

for $n = 1, 2, …, d_i$ 
$$
A[j_1, j_2, \ldots j_{i-1}, n, j_{i+1}, \ldots, j_k] = \sum_{m = 1}^n A[j_1, j_2, \ldots, j_{i-1}, m, j_{i+1}, ..., j_k] (or \prod_{m = 1}^n A[j_1, j_2, \ldots, j_{i-1}, m, j_{i+1}, ..., j_k])
$$

## Array functions for indices and booleans 

##### `in` function

loop through an array and return `true` if a given number appears anywhere in the array:

`element in array`: returns true or false depending on if the element is in the array or not

`in` is the same as $\in$ 

### dot syntax with logical operators

Simplest examples:

`array .== element`, `array .< element`, `array. > element`, `array.!= element`, etc. 

More Complex:

`func.(array) .== element` for example, where the `function` could be `function func(x) = x%2` or `function func(x) = x + 3` etc. 

checks this elementwise in array and will create a similar size array to `array` of booleans 

**The above will be used as arguments for the following Functions**: 

##### `all` and `any` 

**STILL NEED DOT SYNTAX** 

`all(func.(array).(logical operator) element/expression)` will return either true or false if **all** elements satisfy the conditional

`any(func.(array).(logical operator) element/expression)` will return either true or false if **any** element satisfies the conditional

**Examples**

```julia
display(all(A.%2 .==0)) #are all elements in A even
display(any(A.%2 .==0)) #are any elements in A even
```

##### `findfirst`

takes in same type of argument as `all` and `any` 

will return the index of the **first** element for which the conditional argument is true

##### `findall`

like `findfirst`, but will return all such indices

**Examples**: 

```julia
idx = findfirst(x .== 503) #returns a scalar natural number giving the index of the first element in x equal to 503
ind = findall(@. x % 97 == 0) #returns a 1d array of natural numbers giving indices of elements in x that are multiples of 97
```

## Logical Indexing

instead of using `x[idx]`, we can do `x[choose]` where `choose` is an array of booleans. The size of  `choose` must be the length of the array (or more generally, the dimension it indexes into)

`x[choose]` tells us to only keep the elements of `x` where the corresponding element of `choose` is true

```julia
x = -11:11
y = sin.(x)
x = x[y.>0] #only keep values of x for which values of y are positive
```

## Arbitrary Precision Integers

##### `BigInt` Data Type

Note: number going into `BigInt(number)` will first be interpreted as an `Int64` and then converted to a `BigInt`. 

`parse(BigInt, "large integer string")` 

You can also take an array of numbers, `array`

and do `BigInt.(array)` before doing operations on the array that would lead to overflow

## Arbitrary precision floating-point numbers

##### `BigFloat()`

Input should be a number that can be expressed as a Float64 exactly before doing operations on it 

`parse(BigFloat, "number")` will have greater than or equal to precision than just `BigFloat("number")`

##### `setprecision()`

Sets precision for all later operations **unless** used in a `do` block, then it will only apply to that block of code

Example:

```julia
setprecision(512) do
    BigFloat(1)/BigFLoat(3)
end
```

**Examples**

* Being able to see cubic convergence when something converges really quickly (i.e., gets close to 0 really fast, so floating point will default to 0 for very small numbers)
  * Define function like usual, but wrap input in `BigFloat()` 

## Complex Numbers

##### `im`

Global constant for $\sqrt{-1}$ 

##### ways to create

```julia
a = 5 - 3im
b = complex(5, -3)
```

**Standard Operations work As Expected**

`*`, `/`, `^`, etc. 

### Functions For Complex Numbers

##### `real(complex)`

returns real part

##### `imag(complex)`

returns imaginary part

##### `conj(complex)`

gives the complex conjugate

##### `abs(complex)`

gives the absolute value as normally defined for complex numbers

##### `abs2(complex)`

gives $|\text{complex}|^2$ 

##### `angle(complex)`

phase angle in radians 

##### And most elementary functions

`sqrt()`, `cos()`, `exp()`, `sinh()` 

**WARNING**: to take the sqrt of a negative numer, it needs to be in complex form first

```julia
sqrt(-1.0 + 0im) #is good
sqrt(complex(-1)) #is good
sqrt(-1) #is not good
```

## Rational Numbers

**ratios of integers** 

Uses `//` operator

* rational number can be converted to floating point number if needed by wrapping it in `Float64()`

When it's not necessary to convert:

* using inequalities on only rational types
* operators with only rational types
* note: it will always simplify to least common denominator after adding, multiplying, etc. 

Examples:

```julia
2//4 + 1//6 #returns 2//3
6//9 #returns 2//3
```

### Combining with other numeric types

Float64 > Rational > Int > Bool

## Arrays of Complex and Rational Numbers

Works exactly as expected, just always need to make sure wrapping properly in `complex()` or whatever.

## Floating Point 

$$
(-1)^s(d_0 + d_1 \beta^{-1}+ \ldots + d_{p-1}\beta^{-(p-1)})\beta^e, \qquad d_i \in \Z: 0 \leq d_i < \beta
$$

Base: $\beta$

Precision $p$ 

exponent range $[e_{min}, e_{max}]$ 

##### Normalized

$d_0 \neq 0$, then we use $e = e_{min} - 1$ to represent $0$ 

##### Gaps between adjacent numbers

scale with size of the numbers, i.e. as we get larger numbers, the gaps between floating pointing numbers grow

for example, with the usual floating point, there will always be $2^{52}-1$ numbers in between any power of $2$  

##### Machine Epsilon

$\epsilon_{machine} = \frac{1}{2}\beta^{1-p}$ 

##### $e_{\max}$ (From discussion):

$\beta^{\text{# bits for exponent}-1} - 1$ 

example: $8$ bits for single precision, $e_{\max} = 127$ and $2^{8-1} - 1 = 127$ 

##### How Accurate

$|x - \text{fl}(x)| \leq \epsilon_{machine}\cdot |x|$  

* always consider the distance between any two floating point $\in [\beta^e, \beta^{e+1}]$ numbers: $(\beta^{1- \text{precision}})\cdot \beta^e$ 

##### Special Quantities

* $\pm \infin$ is returned when an operation overflows
* $x/ \pm \infin = \begin{cases} \pm 0 & x \geq 0 \\ \mp 0 & x < 0\end{cases}$ for any number $x$ 
* $x/0 = \pm \infin$ for any nonzero number $x$ 
* Operations with infinity are defined as limits i.e, replace where infinity happens with $x$ and take limit as $x \rightarrow \infin$ 
* NaN is returned when an opeartion has no well-defined finite or infinite result
* Example: $\infin - \infin$, $\infin/\infin$, $0/0$, $\sqrt{-1}$, $NaN$ operation $x$ 

##### Denormalized Numbers

When we use normalized significand, there is a "gap" between $0$ and $\beta^{e_{min}}$ 

This can result in $x - y = 0$ even though $x \neq y$ and code fragments like `if x != y then z = 1/(x-y)` might break

Solution: Allow non-normalized significand when the exponent is $e_{min}$ 

This is called **gradual underflow**  and guarantees that $x = y \iff x - y= 0$ 

##### IEEE Single Precision

* 1 sign bit, 8 exponent bits, 23 significand bits $p = 24$ 

$$
(-1)^s \times \underbrace{1}_{\text{not stored}}.M \times 2^{E - 127}
$$

|            | E = 0        | 0 < E<255        | E = 255      |
| ---------- | ------------ | ---------------- | ------------ |
| M = 0      | $\pm 0$      | Powers of 2      | $\pm \infin$ |
| M $\neq$ 0 | Denormalized | Ordinary numbers | NaN          |

**in Other words**: 

$c$: positive integer, $f$: fraction $0 \leq f \leq 1$ 
$$
x = \begin{cases}(-1)^s 2^{c - 127}(1+f) & 1 \leq c \leq 254 
\\ (-1)^s 2^{-126}(0+f) & c = 0
\\ \pm \infin &c=255, f = 0
\\ \text{NaN} & c = 255, f \neq 0
\end{cases}
$$
Denormalize:
$$
(-1)^s \cdot 2^{-126}\cdot 0.M
$$
Why?
$$
2^{23}+ ... +2 + 1 = 2^{23} - 1
$$

$$
(1 + \frac{2^{23} - 1}{2^{23}})(2^{-127}) = 2^{-126} - \frac{2^{-127}}{2^{23}} = 2^{-126}(1 - \frac{1}{2^{24}})
$$


##### Important table

![image-20190311032128835](Math 124 Possible Cheat Sheet Notes.assets/image-20190311032128835.png)

In Double Precision: $2^{11} - 1 = 2047$ 
$$
x = \begin{cases} 

(-1)^s 2^{c - 1023}(1+f) & 1 \leq c \leq 2046
\\ (-1)^s 2^{-1022}(0+f) & c = 0
\\ \pm \infin &c=2047, f = 0
\\ \text{NaN} & c = 2047, f \neq 0
\end{cases}
$$

##### Arithmetic and Rounding

When a number is dead in between two floating point numbers, it "rounds to even": last bit of $f$ is $0$ 

## Recursion...

Idk what notes I could place here
$$
\underset{\text{bottom}}{\text{top}}
$$

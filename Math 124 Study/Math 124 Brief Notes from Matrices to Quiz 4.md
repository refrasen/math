# Math 124 Brief Notes from Matrices to Quiz 4

## 7 - Matrix Operations, linear algebra

### Matrix Operations

##### `LinearAlgebra` Package

```julia
using LinearAlgebra	
```

##### addition, subtraction, scalar multiplication

```julia
y = randn(3)
x - 3y
```

this works as expected

Same as **elementwise operation**: `x.-3y` 

#### multiplying two vectors

`z = x*y` gives an error 

##### dot products: `dot` or `\cdot`/ $\cdot$ syntax 

`dot(x,y)` $\equiv$ `x ⋅ y` 

##### cross product: `cross` function for length 3

`cross(x,y)` 

#### Norm(s)

##### 2-norm

```julia
a = norm(x) #2-norm of x
b = sqrt(x' * x) #should be the same
a - b
```

```julia
#returns 0.0
```

##### General norm

$$
(\sum_{i=1}^n |x_i|^p)^{1/p}
$$

for a vector of length $n$, this is the $p$ norm

`norm(vector, p)` in julia will do the p norm of the vector, and this includes the **max-norm**: `p = inf` 

#### Matrix Multiplication 

$C = AB$ where $A$ is $m\times k$ and $B$ is $ k \times n$ 
$$
C_{ij} = \sum_{l = 1}^k A_{il}B_{lj}\qquad \text{for }i = 1, \ldots , m \text{ and } j = 1, \ldots, n
$$
`AA = A*A` calculates the square of matrix

`A2 = A.*A` squares each entry in the matrix

and `A2` $\neq$ `AA` 

##### `^` operator: matrix powers

`A^2` or `A^3.5` even. 

##### `transpose` and `adjoint`

```julia
BT = transpose(B)     # B is 4-by-3, so BT is 3-by-4
BT2 = B'              # Same thing (since B is real so conjugate does not matter)

A * B'                # Well-defined, since A is 3-by-3 and B' is 3-by-4
```

symmetric matrices are hermitian (I believe?)

Hermitian is when $a_{ij} = \overline{a_{ji}}$ which means $a_{ii} = \overline{a_{ii}}$ (this is true for real numbers only)

Note: `'` is equivalent to `adjoint`: it conjugates then transposes

##### can use matrix multiplication for dot product:

$x \cdot y = x^*y$  conjugate transpose of $x$ 

### Special Matrices

| Type                          | Description                                                                                   |
|:----------------------------- |:--------------------------------------------------------------------------------------------- |
| [`Symmetric`](@ref)           | [Symmetric matrix](https://en.wikipedia.org/wiki/Symmetric_matrix)                            |
| [`Hermitian`](@ref)           | [Hermitian matrix](https://en.wikipedia.org/wiki/Hermitian_matrix)                            |
| [`UpperTriangular`](@ref)     | Upper [triangular matrix](https://en.wikipedia.org/wiki/Triangular_matrix)                    |
| [`UnitUpperTriangular`](@ref) | Upper [triangular matrix](https://en.wikipedia.org/wiki/Triangular_matrix) with unit diagonal |
| [`LowerTriangular`](@ref)     | Lower [triangular matrix](https://en.wikipedia.org/wiki/Triangular_matrix)                    |
| [`UnitLowerTriangular`](@ref) | Lower [triangular matrix](https://en.wikipedia.org/wiki/Triangular_matrix) with unit diagonal |
| [`Tridiagonal`](@ref)         | [Tridiagonal matrix](https://en.wikipedia.org/wiki/Tridiagonal_matrix)                        |
| [`SymTridiagonal`](@ref)      | Symmetric tridiagonal matrix                                                                  |
| [`Bidiagonal`](@ref)          | Upper/lower [bidiagonal matrix](https://en.wikipedia.org/wiki/Bidiagonal_matrix)              |
| [`Diagonal`](@ref)            | [Diagonal matrix](https://en.wikipedia.org/wiki/Diagonal_matrix)                              |
| [`UniformScaling`](@ref)      | [Uniform scaling operator](https://en.wikipedia.org/wiki/Uniform_scaling)                     |

---

`T = SymTridiagonal(diagonal, subdiagonals)` 

`Tridiagonal(lowerdiagonal, diagonal, upperdiagonal)` 

`BiDiagonal(diagonal, subdiagonal, L/U)` where L says the subdiagonal goes under, U goes over the diagonal

```julia
Diagonal(rand(3))
Tridiagonal(rand(2), rand(3), rand(2))
Bidiagonal(rand(3), rand(2), :U)
SymTridigaonal(ones(5), ones(4))
```

#### Identity Matrix

`I`: operator and will behave as expected

##### To create an identity matrix

`myI = Matrix{Type}(I, dim1, dim2)`

(What it seems to do: will make an identity matrix of Type of size max(dim1, dim2) then takes the first dim1 rows and the first dim2 columns)

### Linear Systems

##### `\` : backslash operator

```julia
A = [1 2; 3 4]
b = [5,1]
x = A\b         # Solve Ax = b for x
A*x == b        # Confirm solution is correct
```

#### Linear Regression

$$
\begin{pmatrix}
1 & x_1 \\
1 & x_2 \\
\vdots & \vdots \\
1 & x_n
\end{pmatrix}
\begin{pmatrix}
a \\ b
\end{pmatrix}
=
\begin{pmatrix}
y_1 \\ y_2 \\ \vdots \\ y_n
\end{pmatrix}
$$


tries to find the line: $y = bx + a$ 

### Other Matrix Operations

#### Determinant, trace, inverse

```julia
A = randn(3,3)
println("det(A) = ", det(A))
println("tr(A) = ", tr(A))
println("inv(A) = ", inv(A))
```

#### Eigenvalues and eigenvectors

```julia
A = randn(3,3)
lambda = eigvals(A)     # Eigenvalues
X = eigvecs(A)          # Eigenvectors
```

## Strings and Files

### Strings

#### Characters

ASCII: 7-bit character set, contains 128 characters

type: `Char` 

```julia
c = 'Q' #note the single quotes, this means we are assinging to variable c the ascii character for Q

c_and = Char(38) #assigns to c_and the 38th character, "&"

c_at = Int('@') #finds the number of a char by converting to Int
```

##### Arithmetic and Comparisons with Characters

```julia
julia> 'A' < 'a'
true

julia> 'A' <= 'a' <= 'Z'
false

julia> 'A' <= 'X' <= 'Z'
true

julia> 'x' - 'a'
23

julia> 'A' + 1
'B': ASCII/Unicode U+0042 (category Lu: Letter, uppercase)
```

#### String Creation

##### Creating a string with double quotes

```julia
str1 = "Hellow world!\nJulia is fun\n"
```

when returned like this, it will show the backslash syntax for control characters

**but**: when we do `print(str1)` :

```julia
Hello world!
Julia is fun
```

##### Showing special characters as strings

```julia
str2 = "I \"have\" \$50, A\\b\n"
```

so we have to put backslashes in front of: `$`, `"`, and `\` to have them shown when printed. 

##### triple quotes: 

```julia
str3 = """
    This is some multi-line text. 
    Carriage return is inserted for new lines.
    Double quotes can be used as-is: "
    But backslash and dollar still need extra backslashes: \\, \$
    The indentation of the lines is determined by the position of the final triple-quote.
"""
```

#### String Concatenation

`string` function  to concatenate multiple strings:

`string(str1, str2, str3, str4) = str1*str2*str3*str4` 

##### interpolation into string literals `#####  

`"$str1 $str2"` 

or

```julia
vec = rand(3)
str = "Random vector: $vec"
```



##### copies of strings with `^` 

`str ^ n`, where n is how many times you want str to be displayed back to back to back

#### String Comparison

There's a thing where you do:

```julia
julia> "abracadabra" < "xylophone"
true

julia> "abracadabra" == "xylophone"
false

julia> "Hello, world." != "Goodbye, world."
true

julia> "1 + 2 = 3" == "1 + 2 = $(1 + 2)"
true
```

and I don't understand the first two.

#### String Indexing

works as expected, beware unicode characters

##### a view into a string with `SubString` 

`SubString(str, start_index, stop_index)` 

##### length of a string with `length` function

`length(str)` works as expected

##### Strings are immutable

cannot change the content of a string after it is created:

Example: 

```julia
str[4] = 'A' #error 
```

#### String Functions

##### lowercase and uppercase

`lowercase(str)` and `uppercase(str)` work as expected

##### titlecase

captilazes the first character of each word in a string

`titlecase(str)` 

#### Searching for characters and substrings

##### $\in$ Operator to check if a character appears in a string

must only be a single character it seems to use `''` and for multiple charcters we have to use ""

```julia
'a' \in "abc"
```

must use `occursin(x,y)` to see if string x occurs in string y

##### `findfirst(pattern, str)` 

returns the indices of the first occurance of `pattern` in `str` 

```julia
str = "Hello, World! These are my words."
pattern = "wor"
idx1 = findfirst(pattern, lowercase(str))
```

##### `findnext(pattern, str, start)` 

returns the indices of the next occurance of `pattern` in `str` after the one at position `start` 

##### `findlast` , `findprev`

starts from the end of the string

#### Replacing substrings

`replace(str, pattern => repl)` searches the `str` for all occurances of the substring `pattern` and replace them with the string `repl` 

### File Processing

#### Reading Files

##### `open(filename)` 

`f = open(filename)` returns a so-called "stream"

`eof(f)` (end-of-file) returns `true` if the stream `f` has reached the end of the file

`readline(f)` returns a string containing the next file in the stream `f` 

Example:

```julia
f = open(filename)
readline(f)#displays first line
readline(f)#displays second line
readline(f)
```

and each time you run `readline(f)` it will continue reading each line.

`close(f)` closes the stream

```julia
f = open("test_file.txt")
while !eof(f)
    str = readline(f)
    display(str)
end
close(f)
```

and this will print line by line each line in the file

---

`eachline(filename)` allows you to access each line in a file more easily:

```julia
for line in eachline(filename)
  display(line)
end
```

---

##### `read` function

this will read the entire file into a julia string:

`read(filename, String)` 

```julia
str = read("test_file.txt", String)
```

outputs:

```
"This is a test file\n===================\n\nThis is line #4\nHere are some comma-separated numbers:\n\n1,2,3,4,5\n5,-4,3e3,2.0,1"
```

---

##### read an entire file into an array using `readlines` 

each line in the file is an element in this array

`readlines(filename)` creates an array of size how every many lines there are in the file

you can access these strings using the usual array syntax, or loop over all of them

```julia
lines = readlines("test_file.txt")
println("Line #2 says: ", lines[2])
println()
println("Here are all the lines which have between 1 and 18 characters:\n")
for line in lines
    if 1 ≤ length(line) ≤ 18
        println(line)
    end
end
```

#### Writing Files

when creating a new file:

`f = open(new_filename, "w")` 

use `write(f, str)` to write string to stream `f` 

it seems like you can write things to the stream, but it only gets onto the file if you print it?

Example:

```julia
f = open("created_data.txt", "w")
for i = 1:5
     # Create random strings of letters
    str = String(rand('a':'z', 50))
    write(f, str * "\n")  # Write string to stream f
end
println(f) # println can be used with streams too

# Print Fibonacci numbers to file
x = y = 1
print(f, "$x $y")
for i = 1:50
    z = x + y
    x = y
    y = z
    print(f, " $z")
end
println(f)
close(f)
```

#### Delimited Files (package)

#### `using DelimitedFiles` 

Two convenient function for reading and writing arrays of data:

1. `writedlm(filename, A, delim)`: writes the array `A` to file `filename` using the character or string `delim` between each element in a row
   1. so writes each element in to the file, separting each element with delim

2.  `readdlm(filename, delim, T)` reads an array from a file in a similar way, with the optional element type `T` 

#### Extras:

you can use `collect` on a string~

#### big ass note: Julia 1.1.0 makes it so you can't do arithmetic with characters

## Various Julia Language

### Scope of Variables

**scope**: region of code within which a variable is visible

Example:

```julia
x = 10
y = 10

for i = 1:5
    z = i        # Local scope, only visible inside for-loop
    x = z        # Using x from parent scope
    local y = z  # Local scope, only visible inside for-loop (not using y from parent scope)
end

println(x)    #  = 5, since for loop modifies parent variable x
println(y)    #  = 10, since for loop uses local variable y
println(z)    # Error: z only defined in the local scope of the for-loop
```

## Computational Geometry

Two Main Branches

1. Combinatorial computation geometry
2. Numerical computational geometry


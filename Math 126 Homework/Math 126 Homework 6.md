Renee Senining - Math 126 

### **Homework 6** 

#### Section 6.1: Exercises 1, 2, 4, 5, 6, 9, 12

##### Exercise 1

$f(z) = \sum_{n = 0}^\infin a_nz^n = \sum_{n=0}^\infin a_n(x + iy)^n = u(x+iy) + iv(x+iy)$ 

so $u(x,y) + iv(x,y) = \sum_{n=0}^\infin a_n (x+iy)^n$ 
$u_x(x,y) + iv_x(x,y) = \sum_{n=0}^\infin a_n \cdot n (x+iy)^{n-1}$ 
$u_y(x,y) + iv_y(x,y) = i\sum_{n=0}^\infin a_n \cdot n (x+iy)^{n-1}$  $= i(u_x + iv_x) = -v_x + iu_x$ 

equating real and imaginary parts: $u_y(x,y) = -v_x(x,y)$ and $u_x(x,y) = v_y(x,y)$. So the CR-equations are satisfied. Differentiating further: $u_{xx} = v_{yx} = v_{xy} = -u_{yy}$ so $u_{xx} + u_{yy} = 0$ and $v_{yy} = u_{xy} = u_{yx} = -v_{xx}$ so $v_{xx} + v_{yy} = 0$ as well. 

##### Exercise 2

We have: $\Delta_3 u = k^2 u = u_{rr} + \frac{2}{r}u_r + \frac{1}{r^2} [u_{\theta \theta} + (\cot \theta )u_\theta + \frac{1}{\sin^2 \theta} u_{\phi \phi}]$ in spherical coordinates. And since we are looking for solutions that depend only on $r$: 
$u_{rr} + \frac{2}{r} u_r = k^2 u$ $\equiv$ $u''(r) + \frac{2}{r} u'(r) = k^2 u(r)$ 
Now using the hint: $u = v/r$: $u' = v'/r - v/r^2$, $u'' = v''/r - 2v'/r^2 + 2v/r^3$ 
$\frac{v''}{r} - 2 \frac{v'}{r^2} + 2\frac{v}{r^3} + \frac{2}{r}[\frac{v'}{r} - \frac{v}{r^2}] = \frac{v''}{r} = k^2\frac{v}{r}$

 so $v'' = k^2 v$ $\implies$ $v = C_1e^{k r} +C_2 e^{-kr}$ so $u = \frac{C_1e^{kr}+C_2e^{-kr}}{r}$, where $C_1, C_2$ are constants

##### Exercise 4

Using the hint, we have only to find a solution that depends on $r$, so the equation becomes $u_{rr} + \frac{2}{r}u_r = 0$ (from spherical coordinates then using $\frac{\partial }{\partial \theta} u = \frac{\partial }{\partial \phi} u = 0$ )

$u''(r) = -\frac{2}{r} u'(r)$, letting $v = u'(r)$, we have $v'(r) = u''(r) = -\frac{2}{r} v$ 

$\frac{dv}{dr} = -\frac{2}{r} v​$ $\implies​$ $\int \frac{1}{v} dv = -2 \int \frac{1}{r} dr​$ $\implies​$ $\ln(v) = -2 \ln(r) + C​$ $\implies​$ $v = e^{-2 \cdot \ln(r) + C}​$ $ = e^C\frac{1}{r^2}​$ $ = C \frac{1}{r^2}​$ (since $e^C​$ is just an arbitrary constant)

so $u'(r) = C \frac{1}{r^2}$ $\implies$ $u = C \int \frac{1}{r^2} + D$ $\implies$ $u = -\frac{C}{r} + D$  

$\begin{align} u(a) &= - \frac{C}{a} + D = A \\ u(b) &= -\frac{C}{b} + D = B \end{align}$ $\implies$ $\begin{align} -C + Da &= Aa \\ C - Db &= -Bb \end{align}$  $\implies$ $D(a-b) = Aa - Bb$ so $D = \frac{Aa - Bb}{a-b}$ and $C = -Bb + b \frac{Aa - Bb}{a-b}$ $ = \frac{-Bab + Bbb}{a-b} + \frac{Aab - Bbb}{a-b}$ $ = \frac{ab(A - B)}{a-b}$ 

so $u(r) = -\frac{1}{r} \frac{ab(A-B)}{a-b} + \frac{Aa - Bb}{a-b}$ 

##### Exercise 5

$\Delta_2 u = u_{rr} + \frac{1}{r} u_r + \frac{1}{r^2} u_{\theta \theta}$ 

**Going to try to find solutions that only depend on $r$**, since on the boundary, $u(x,y)$ is not a function of $\theta$ 

$u_{rr} + \frac{1}{r} u_r = 1$ $\equiv$ $ru_{rr} + u_r = r$ and $(ru_r)' = u_r + ru_{rr} = r$ 

so $\frac{d}{dr}(r u_r) = r$ $\implies$ $ru_r = \int r \; dr + C = \frac{r^2}{2} + C$ $\implies$ $u_r = \frac{r}{2} + \frac{C}{r}$ 

$u(r) = \frac{r^2}{4} + C \ln r + D$, and $\ln |r| = r$, since $r = \sqrt{x^2 + y^2}$, however when $r = 0$, $\ln r$ is undefined...

Applying BCs: $u(a) = \frac{a^2}{4} + C \ln a + D = 0$ and since the derivatives of $u$ must exist for all $r < a$, and $r = 0 < a$, $u$ must be differentiable at $r = 0$ which means we can't have $\ln 0$ $\implies$ $C = 0$. So now we have $u(a) = \frac{a^2}{4} + D $ $= 0$ $\implies D = -\frac{a^2}{4}$ 
so $u(r) = \frac{r^2}{4} - \frac{a^2}{4}\equiv u(x,y) = \frac{x^2 + y^2 - a^2}{4}$ 

##### Exercise 6

General Solution From Ex. 5: $u(r) = \frac{r^2}{4} + C \ln r + D$
Checking: $u_r = \frac{r}{2} + \frac{C}{r}$ $u_{rr} = \frac{1}{2} - \frac{C}{r^2}$ and $\frac{1}{2} - \frac{C}{r^2} + \frac{1}{r}[\frac{r}{2} + \frac{C}{r}] = \frac{1}{2} - \frac{C}{r^2} + \frac{1}{2} + \frac{C}{r^2} = 1$ 
$u(a) = \frac{a^2}{4} + C \ln a + D = 0 = u(b) = \frac{b^2}{4} + C \ln b + D$ so:
$\frac{a^2}{4} + C \ln a = \frac{b^2}{4} + C \ln b$ $\implies$ $C(\ln b - \ln a) = \frac{a^2 - b^2}{4}$ $\implies$ $C = \frac{a^2 - b^2}{4(\ln b - \ln a)}$ $ = \frac{a^2 - b^2}{4 \ln(\frac{b}{a})}$ 

$D = -(\frac{a^2}{4} + \frac{\ln a (a^2 - b^2)}{4\ln\frac{b}{a}}) = -\frac{a^2 \ln a -b^2 \ln a +  a^2(\ln b - \ln a) }{4\ln \frac{b}{a}} = \frac{b^2 \ln a - a^2 \ln b}{4 \ln \frac{b}{a}}$

$u(r) = \frac{r^2}{4} + \frac{a^2- b^2}{4 \ln \frac{ b}{a}} \ln r + \frac{b^2 \ln a - a^2 \ln b}{4 \ln \frac{b}{a}}$ $ = \frac{r^2}{4} + \frac{a^2 \ln r - b^2 \ln r + b^2 \ln a - a^2 \ln b}{4 \ln \frac{b}{a}}$ $ = \frac{r^2}{4} + \frac{a^2(\ln r - \ln b) + b^2 (\ln a - \ln r)}{4 \ln \frac{b}{a}}$ $ = \frac{1}{4}[r^2 + \frac{a^2 \ln \frac{r}{b} + b^2 \ln \frac{a}{r}}{4 \ln \frac{b}{a}}]$  

$\equiv$ $u(x,y) = \frac{1}{4} [ x^2 + y^2 + \frac{a^2 \ln \frac{\sqrt{x^2 + y^2}}{b} + b^2 \ln \frac{a}{\sqrt{x^2 + y^2}}}{\ln \frac{b}{a}}$ 

Note: $ - a^2 \ln \frac{b}{r} = a^2(- \ln \frac{b}{r}) = a^2 \ln \frac{r}{b}$ so the above could be expressed differently.

##### Exercise 9

**(a)** the Heat equation: $c \rho u_t = \kappa \Delta u $, and since we know it has a steady-state temperature dist $\implies$ our equation: $\Delta_3 u = u_{xx} + u_{yy} + u_{zz} = 0$. And since the temperature depends only on the radius: Our equation becomes: $u_{rr} + \frac{2}{r}u_r  = 0$ $\implies$ $u(r) = - \frac{C}{r} + D$ 
(from exercise 4). 

Applying BCs: $u(1) = -C + D = 100$, $u_r(2) = \frac{C}{4} = -\gamma$ , so $C = -4 \gamma$ $\implies$ $D = 100 - 4 \gamma$ 

so $u(r) = \frac{4 \gamma}{r} + 100 - 4 \gamma$ , on $1 < r < 2$ 

**(b)** Since $u$ only depends on $r$, we can try to find critical points: $u'(r) = - \frac{4 \gamma}{r^2} \neq 0$ (unless $\gamma = 0$), so only on the endpoints do we have max and min:
$u(1) = 100$, $u(2) = 2 \gamma + 100 - 4 \gamma = 100 - 2\gamma$ 

**(c)** $\gamma = 40^{\circ}\text C$ (just from solving $100 - 2\gamma = 20$ for $\gamma$)

##### Exercise 12

$u(x,y) = \frac{1 - x^2 - y^2}{1 - 2x + x^2 + y^2}$ in the disk $\overline D = \{ r^2 \leq 1\}$ 

$u(x,y) = \frac{1 - x^2 - y^2}{(x-1)^2 + y^2}$ . $u$ must be continuous on $\overline D$, but when $r = 1$, at the point $(1, 0)$, we have $\lim_{(x,y) \rightarrow (1,0)} \frac{1 - x^2 - y^2}{(x-1)^2 + y^2}$… approaching from $\lim_{(x,0) \rightarrow (1,0)} \frac{1 - x^2}{(x-1)^2} = \frac{0}{0}$, using L'hospital's, since this is now a single-variable limit depending only on $x$: the limit will be equal to $\lim_{x \rightarrow 1} \frac{1 - 2x}{2x - 2} $ $ = \infin$ 

The limit doesn't exist, so it isn't continuous, which means we can't use the Maximum Principle

#### Section 6.2: Exercises 1, 2, 3, 6

##### Exercise 1

Guess: solution is a quadratic polynomial in $x$ and $y$ 
$$
u(x,y) = c_{2, 0}x^2 + c_{1, 1}xy + + c_{0, 2}y^2 + c_{1, 0}x + c_{0, 1}y + c_{0,0}
$$
Check if it satisfies $\Delta u = 0$: 
$u_{xx} = 2c_{2,0}$, $u_{yy} = 2c_{0, 2}$ $\implies$ $c_{2, 0} = -c_{0, 2}$ 
$u_x(x,y) = 2c_{2,0} x + c_{1, 1} y + c_{1, 0}$ and $u_y(x,y) = c_{1, 1}x + 2c_{0, 2}y + c_{0, 1}$ 
$u_x(0, y) = c_{1, 1}y + c_{1, 0} = -a \\ u_x(a, y) = c_{1, 1}y + (2c_{2, 0}a + c_{1, 0}) = 0$ $u_y(x, 0) = c_{1, 1}x + c_{0, 1} = b \\ u_y(x, b) = c_{1, 1}x + (2c_{0, 2} b + c_{0, 1}) = 0$ 

$u_x(0, y) - u_x(a,y) = -2c_{2, 0} a = -a$ $\implies c_{2,0} = \frac{1}{2}$ 
and since $c_{1, 1}, c_{1,0}$ are constants: $c_{1, 1} = 0$, (else it is a function that depends on $y$ or depends on $x$), $c_{1,0} = -a$ 
$\implies$ (from $u_y(x,0)$) $c_{0,1} = b$ and we know that $c_{2,0} = -c_{0,2}$, so $c_{0, 2} = -\frac{1}{2}$: 
$$
u(x,y) = \frac{x^2}{2} -\frac{y^2}{2}-ax + by + c_{0,0}
$$
where $c_{0,0}$ is any arbitrary constant

##### Exercise 2

Consider $f (y,z) = \sin m_1 y \sin n_1 z​$, $g(y, z) = \sin m_2 y \sin n_2 z​$ where $m_1 \neq m_2​$, $n_1 \neq n_2​$: 

$(f, g) = \int_0^\pi \int_0^\pi \sin m_1 y \sin n_1 z \sin m_2 y \sin n_2 z \; dy \; dz$ $ = \int_0^\pi \sin m_1 y \sin m_2 y \; dy \int_0^\pi \sin n_1 z \sin n_2 z \; dz$ 
and we know from section 5.1 that

$\int_0^l \sin \frac{n \pi x}{l} \sin \frac{m \pi x}{l} \; dx = 0​$ if $m \neq n​$, and in this case, we have $l = \pi​$ 

$\implies$ $\int_0^ \pi \sin m_1 y \sin m_2 y \; dy = 0 =  \int_0^\pi \sin n_1 z \sin n_2 z \; dz$ 

so $(f, g) = 0$ $\implies$ the eigenfunctions are orthogonal in the square.

##### Exercise 3

$u(x,y) = X(x)Y(y)$. $\Delta_2 u = X''Y + XY'' = 0$ $\implies$ 
$\frac{X''}{X} + \frac{Y''}{Y} =0$ $\implies$ $\frac{X''}{X} = - \frac{Y''}Y = \lambda$ 
$$
\begin{align} 
X'' = \lambda X && X(0) = 0, X(\pi) = \frac{1}{2}[1 + \cos 2y]
\\ Y'' = -\lambda Y && Y'(0) = Y'(\pi) = 0
\end{align}
$$
and we know from 4.2 that $Y_n(y) = A\cos ny$ for $n = 0, 1, 2, 3…$ 
and $X'' = n^2 X$ $\implies$ $X = C \cosh nx + D \sinh nx$ 
$X(0) = C = 0$, so $X = D \sinh nx$ for $n = 1, 2, …$ 

and for $n = 0​$: $X'' = 0​$ $\implies​$ $X = C_0x + D_0​$ $\implies​$ $X(0) = D_0 = 0​$ 
so $X = C_0 x​$ when $n = 0​$ 

$u(x,y) = \frac{1}{2}A_0x +  \sum_{n=1}^\infin (A_n \cos ny)(\sinh nx)$
Applying inhomogeneous BC: 
$u(\pi, y)= \frac{1}{2} + \frac{1}{2} \cos 2y = \frac{1}{2} A_0 \pi + \sum_{n=1}^\infin A_n \cos ny \sinh n\pi$ 

matching the coefficients we obtain: $A_0 = \frac{1}{\pi}$, $A_2 = \frac{1}{2 \sinh 2\pi}$, and $A_n = 0$ elsewhere. So our final answer: $u(x,y) = \frac{1}{2\pi} x + \frac{1}{2 \sinh 2\pi} \sinh 2x \cos 2y$ 

##### Exercise 6

Solve the following Neumann problem in the cube: $\{0 <x<1, 0 <y <1, 0 <z<1\}​$ :
$$
\Delta u = 0 
\\ u_z(x,y, 1) = g(x,y)
$$
homogeneous Neumann Conditions on the other five faces

$\int_0^1 \int_0^1 g(x,y) \; dx dy = 0$? 

---

Separation of Variables: $u(x,y, z) = X(x)Y(y)Z(z)$, where $X''YZ + XY''Z + XYZ'' = 0$, divide by $1/XYZ$, $\implies$ $\frac{X''}{X} + \frac{Y''}Y + \frac{Z''}{Z} = 0$, so we end up with:
$X'' = -\lambda X, \quad Y'' = -\lambda Y$, each with homogeneous (N) BCs $\implies$ $X(x) = \cos m\pi x, Y(y) = \cos n\pi y$, $m, n = 0, 1, 2, …$ , where when $n =0$, $m = 0$: $X = C_1$, $Y= C_2$, where $C_1, C_2$ are constants
$Z'' = (m^2 + n^2)\pi^2Z$, $Z'(0) = 0$, $Z'(1) = g(x,y)$ $\implies$ $Z(z) = A \cosh (\pi \sqrt{m^2 + n^2} z) + B \sinh( \pi\sqrt{m^2 + n^2} z)$ and $Z'(0) = \pi\sqrt{m^2 + n^2} B = 0 $ $\implies B = 0$ so $Z(z) = A \cosh(\pi \sqrt{m^2 + n^2}z)$, and when $m = n = 0$: $Z(z) = \text{constant}$ 

$u(x,y,z) =A_0 +  \sum_{n=1}^\infin \sum_{m=1}^\infin A_{m, n} \cosh(\pi\sqrt{m^2 + n^2} z) \cos m\pi x \cos n\pi y$
Then applying the inhomogeneous condition at $z = 1$: 
$u_z(x,y,1) = g(x,y) = \sum_{n=1}^\infin \sum_{m=1}^\infin A_{m, n}\pi \sqrt{m^2 + n^2} \sinh (\pi \sqrt{m^2 + n^2}) \cos m \pi x \cos n\pi y​$ 

$\int_0^1 \int_0^1 g(x,y) \cos k\pi x \cos l\pi y \; dx \; dy = \sum_{n=1}^\infin \sum_{m=1}^\infin A_{m,n} \pi  \sqrt{m^2 + n^2} \sinh (\pi \sqrt{m^2 + n^2}) \int_0^1 \int_0^1 \cos m \pi x \cos k\pi x \cos n\pi y \cos l\pi y \; dx \;dy​$, where $k, l = 1, 2, …​$ and since $\int_0^1 \int_0^1 \cos m\pi x \cos k\pi x \cos n\pi y \cos l\pi y \; dx \;dy = \int_0^1 \cos m\pi x \cos k\pi x \; dx \int_0^1 \cos n\pi y \cos l\pi y \; dy​$$ = \frac{1}{2}[\sin((m+k)\pi x)/[\pi(m+k)] + \sin((m-k)\pi x)/[\pi(m-k)]]_0^1\frac{1}{2}[\sin((n+l)\pi y)/[\pi(n+l)] + \sin((n-l)\pi y)/[\pi(n-l)]]_0^1 = 0​$ when $n \neq l​$ or $m \neq k​$. Therefore: $\int_0^1 \int_0^1 g(x,y)  \cos m\pi x \cos n \pi y \; dx \, dy = A_{m,n}\pi \sqrt{m^2 + n^2} \sinh(\sqrt{m^2 + n^2}) \int_0^1 \int_0^1 \cos^2 m\pi x \cos^2 n \pi y \; dx dy ​$ $ = A_{m,n} \pi \sqrt{m^2 + n^2} \sinh(\pi \sqrt{m^2 + n^2}) \cdot \frac{1}{4}​$, since $\int_0^1 \cos^2 m\pi x \; dx = \frac{1}{2}​$ 

so $A_{m, n} = \frac{4}{\pi \sqrt{m^2 + n^2} \sinh(\sqrt{\pi m^2 + n^2})}$ 

#### Section 6.3: Exercises 1, 2, 4

##### Exercise 1

**(a)**: The maximum value will be the max value of $3 \sin 2\theta + 1 $ on $r = 2$, i.e., the boundary of $D$, due to the Maximum Value Principle. $u'(\theta) = 6 \cos 2 \theta $, which is equal to zero at $ \frac{\pi}{4}$, $\frac{3\pi}{4}$, $\frac{5\pi}{4}$, $\frac{7\pi}{4}$. and we have that $u(\pi/4) = u(5\pi/4) = 4$, $u(3\pi/4) = u(7\pi/4) = -2$ 
so the max value is $4$ 

**(b)**: By the mean value principle: 

$u(\bold 0) = u(0, 0) =4\int_0^{2\pi} \frac{3\sin 2\phi + 1}{4} \; \frac{d\phi}{2\pi} = \frac{1}{2\pi} \int_0^{2\pi} 3 \sin 2\phi + 1 d \phi = \frac{1}{2\pi}[-\frac{3}{2}\cos 2\phi + \phi]_0^{2\pi} = \frac{1}{2\pi}[-\frac{3}{2} + 2\pi + \frac{3}{2}] = 1  $ 

##### Exercise 2

$h(\phi) = 1 + 3 \sin \theta$, $u = \frac{1}{2}A_0 + \sum_{n=1}^\infin r^n (A_n \cos n\theta + B_n \sin n\theta)$ and at $r = a$: 
$u(a, \theta) = \frac{1}{2}A_0 + \sum_{n=1}^\infin a^n (A_n \cos n\theta + B_n \sin n\theta)$ $ = 1 + 3 \sin \theta $ 
Matching Coefficients we obtain: $B_1 =3/a$, $A_n = 0$ for all $n$, $A_0 = 2$, so 
$u(r, \theta) = 1 + \frac{3r}{a}\sin \theta$  

##### Exercise 4

6.1.5: $\Delta_2 = \frac{\partial ^2 }{\partial x^2} + \frac{\partial ^2}{\partial y^2} = \frac{\partial ^2}{\partial r^2} + \frac{1}{r} \frac{\partial }{\partial r} + \frac{1}{r^2} \frac{\partial ^2}{\partial \theta^2}​$, $P(r, \theta) = \frac{a^2 - r^2}{a^2 - 2ar \cos \theta + r^2}​$ 
$P_r(r, \theta) = \frac{-2r(a^2 - 2ar \cos \theta + r^2) - (a^2 - r^2)(-2a \cos \theta + 2r)}{(a^2 - 2ar \cos\theta + r^2)^2}​$ $ = \frac{-2a^2 r + 4ar^2 \cos \theta - 2r^3 + 2a^3\cos \theta - 2a^2r -2ar^2\cos \theta+  2r^3}{(a^2 - 2ar \cos\theta + r^2)^2}​$ $ = \frac{-4a^2r + (2ar^2+2a^3) \cos \theta}{(a^2 - 2ar \cos\theta + r^2)^2} \cdot \frac{(a^2 - 2ar \cos\theta + r^2)}{(a^2 - 2ar \cos\theta + r^2)} ​$ $ = \frac{-4a^4r +8a^3r^2\cos \theta -4a^2r^3 + 2a^3r^2 \cos \theta - 4a^2r^3 \cos^2 \theta +2ar^4 \cos \theta + 2a^5 \cos\theta - 4a^4 r \cos^2 \theta + 2a^3r^2 \cos \theta }{(a^2 - 2ar \cos\theta + r^2)^3}​$ $= \frac{-4a^4r -4a^2r^3 + 2a^5 \cos \theta + 12 a^3r^2 \cos \theta -4a^2r^3 \cos^2\theta + 2ar^4 \cos \theta -4a^4 r \cos^2 \theta}{(a^2 - 2ar \cos \theta + r^2)^3}​$ 
$P_{rr}(r, \theta) = \frac{(-4a^2 + (8ar -4ar) \cos \theta)(a^2 - 2ar \cos \theta + r^2)^2 - 2(-4a^2r + (4ar^2 + 2a^3 - 2ar^2) \cos \theta)(a^2 - 2ar\cos \theta + r^2)(-2a \cos \theta + 2r)}{(a^2 - 2ar \cos \theta + r^2)^4}​$ $ = \frac{(-4a^2 + 4ar \cos \theta)(a^2 - 2ar\cos \theta + r^2) - (-4a^2r + (2ar^2 + 2a^3 )\cos \theta)(-2a \cos \theta + 2r)}{(a^2 - 2ar \cos \theta + r^2)^3}​$ 

$ = \frac{-4a^4 + 8a^3 r \cos \theta - 4a^2r^2 + 4a^3 r \cos \theta -8a^2r^2\cos^2\theta + 4ar^3 \cos \theta-16a^3r\cos\theta +16a^2r^2 +8a^2r^2\cos^2\theta -8ar^3 \cos \theta +8a^4 \cos^2\theta  -8a^3r \cos \theta  }{(a^2 - 2ar \cos \theta +r^2)^3}$ $ = \frac{-4a^4 +12a^2r^2 -4ar^3 \cos \theta  -12a^3 r \cos \theta +8a^4 \cos^2\theta}{(a^2 - 2ar \cos \theta + r^2)^3}$

$P_\theta = -\frac{(a^2 - r^2)(2ar \sin \theta)}{(a^2 - 2ar \cos \theta + r^2)^2}$ , $P_{\theta \theta} = -\frac{(a^2-r^2)[(2ar \cos \theta)(a^2 - 2ar \cos \theta + r^2)^2 - 2(2ar\sin\theta)(a^2 - 2ar \cos \theta + r^2)(2ar \sin \theta)]}{(a^2 - 2ar \cos \theta + r^2)^4}$ $= (a^2 - r^2) \frac{2ar \cos \theta (a^2 - 2ar \cos \theta + r^2) - 4ar \sin \theta}{(a^2 - 2ar \cos \theta + r^2)^3}$$  = -(a^2 - r^2) \frac{2a^3 r \cos \theta - 4a^2 r^2 \cos^2\theta + 2ar^3 \cos \theta - 8a^2r^2 \sin^2 \theta}{(a^2 - 2ar \cos \theta + r^2)^3}$ $ = -\frac{2a^5r \cos \theta - 4a^4 r^2 \cos^2 \theta + 2a^3 r^3 \cos \theta - 8a^4 r^2 \sin^2 \theta - 2a^3r^3 \cos \theta + 4a^2r^4 \cos^2 \theta - 2ar^5 \cos \theta +8a^2r^4 \sin^2 \theta}{(a^2 - 2ar \cos \theta + r^2)^3}$ $ = -\frac{2a^5 r \cos \theta - 4a^4 r^2 \cos^2 \theta - 8a^4 r^2 \sin^2 \theta + 8a^2r^4 \sin^2 \theta + 4a^2 r^4 \cos^2 \theta - 2ar^5 \cos \theta }{(a^2 - 2ar \cos \theta + r^2)^3}$ 

$\Delta_2 P = \frac{-4a^4 + 12a^2r^2 - 4ar^3 \cos \theta - 12a^3r \cos \theta + 8a^4 \cos ^2 \theta}{(a^2 - 2ar \cos\theta + r^2)^3} + \frac{-4a^4 - 4a^2r^2 + (2a^5 \cos \theta)r^{-1} + 12a^3r \cos \theta - 4a^2r^2 \cos^2 \theta + 2ar^3 \cos \theta  - 4a^4 \cos^2 \theta }{(a^2 - 2ar \cos\theta + r^2)^3} + \frac{(-2a^5\cos\theta)r^{-1} + 4a^4\cos^2\theta + 8a^4\sin^2\theta -8a^2r^2 \sin^2\theta - 4a^2r^2 \cos^2\theta +2ar^3 \cos \theta}{(a^2 - 2ar \cos\theta + r^2)^3}​$ $a^4: -4a^4 - 4a^4 + 8a^4(\cos^2\theta + \sin^2\theta)= 0​$, $a^2r^2: 12a^2r^2 -4a^2r^2   -8a^2r^2(\cos^2\theta + \sin^2\theta)  = 0​$, $ar^3\cos\theta: -4ar^3 \cos \theta +2ar^3\cos\theta  +2ar^3 \cos \theta= 0 ​$, $a^3 r \cos \theta: -12a^3r\cos \theta +12a^3r \cos \theta = 0​$, $(a^5\cos\theta)r^{-1}: 2(a^5 \cos\theta)r^{-1} - 2(a^5\cos\theta)r^{-1} = 0​$,  $a^4\cos^2 \theta: -4a^4\cos^2\theta  +4a^4 \cos^2 \theta = 0​$, 

A total of 18 terms above to be added/subtracted: 5 in the first fraction, 7 in the second, 6 in the third. $T_{i, j}​$ means jth term of the ith fraction.
$T_{1, 1} + T_{2, 1} + T_{1, 5} + T_{3, 3} = 0​$ $\equiv​$ $-4a^4 - 4a^4 + 8a^4(\cos^2\theta + \sin^2\theta)= 0​$

$T_{1, 2} + T_{2, 2} +T_{2, 5} + T_{3, 4} + T_{3, 5} = 0$ $\equiv$ $12a^2r^2 -4a^2r^2   -8a^2r^2(\cos^2\theta + \sin^2\theta)  = 0$ 

 $T_{1, 3} + T_{2, 6} + T_{3, 6}= 0$ $\equiv$  $-4ar^3 \cos \theta +2ar^3\cos\theta  +2ar^3 \cos \theta= 0 $

 $T_{1, 4} + T_{2, 4} = 0$ $\equiv$ $ -12a^3r\cos \theta +12a^3r \cos \theta = 0$ 

 $T_{2, 3} + T_{3, 1} = 0$ $\equiv$ $2(a^5 \cos\theta)r^{-1} - 2(a^5\cos\theta)r^{-1} = 0$

 $T_{2, 7} + T_{3, 2} = 0$ $\equiv$ $ -4a^4\cos^2\theta  +4a^4 \cos^2 \theta = 0$ 

so we have $\Delta_2 P = 0$, so $P$ is harmonic in $D$ 
Renee Senining
Math 126

### Homework 12

#### Section 14.3: Exercises 1—7, 10, 11

##### Exercise 1

(1): $T[u] = \int_a^b \sqrt{1 + (\frac{du}{dx})^2}\, dx$ $\implies$ 

$F(x, u, u') = \sqrt{1 + (\frac{du}{dx})^2}$ or $F(x, z, p) = \sqrt{1 + p^2}$ , so then we use the E-L equation:

$\frac{\partial F}{\partial z} = 0$ and $\frac{\partial F}{\partial p} = \frac{p}{\sqrt{1 + p^2}}$ 
so $0 = \frac{d}{dx}\frac{\partial F}{\partial p}(x, u, u') = \frac{d}{dx}(\frac{u'}{\sqrt{1 + (u')^2}})$ or:

Beltram Identity, since $F$ doesn't depend on $x$ explicitly:

$\sqrt{1 + (u')^2} - \frac{u'^2}{\sqrt{1 + (u')^2}} = C$, for some $C \in \R$ $\implies$ 

$\frac{1}{\sqrt{1 + (u')^2}} = C$ $\implies$ $C\sqrt{1 + (u')^2} = 1$ $\implies$ $C^2[1 + (u')^2] = 1$ 
$\implies$ $(u')^2 = \frac{1 - C^2}{C^2}$ $\implies$ $\frac{du}{dx} = \pm \sqrt{\frac{1 - C^2}{C^2}}$ , which is a constant, let $m = $the right hand side
so ​$u =  m x + b$, where $b $ is a constant , and whether $m$ is positive or negative depends on if $u(a) < u(b)$ or vice versa. Also, $C = \frac{1}{\sqrt{1 + (u')^2}}$, which is always less than or equal to 1 since the denominator $\sqrt{1 + (u')^2} \geq 1$, so $m$ will always be a real number. 

##### Exercise 2

Find the shortest curve in the $xy$ plane that joins the two given points $(0, a)$ and $(1, b)$ and that has a given area $A$ below it (above the $x$ axis and between $x = 0$ and $x = 1$); $a$ and $b$ are positive. 

---

Minimize $\int_0^1 \sqrt{1 + (u')^2}\, dx$ $+ m \int_0^1 u \, dx$, where $m$ is a Lagrange multiplier (because of the constraint on $\int_0^1 u \, dx = A$)[^1] 

[^1]: https://en.wikipedia.org/wiki/Lagrange_multiplier#Interpretation_of_the_Lagrange_multipliers

$\int_0^2 \sqrt{1 + (u')^2} + mu \, dx$ so $F(x, z, p) = \sqrt{1 + p^2} + mz$ 

Beltram: $\sqrt{1 + (\frac{du}{dx})^2} + mu - \frac{du}{dx}(\frac{\frac{du}{dx}}{\sqrt{1 + (\frac{du}{dx})^2}}) = C$

$\equiv$ $\frac{1}{\sqrt{1 + (\frac{du}{dx})^2}} + mu = C$ 
$\frac{1}{\sqrt{1 + (u')^2}} = C - mu$ 
$\frac{1}{C - mu} = \sqrt{1 + (u')^2}$ 
$(\frac{1}{C - mu})^2 -1= (\frac{du}{dx})^2$ 
$\frac{du}{dx} = \pm \sqrt{(\frac{1}{C-mu})^2 - 1}$ 
$\pm \int \frac{C - mu}{\sqrt{1 - (C-mu)^2}} du = x + c_1$ 

$w = C - mu$, $dw = -m du$ 
$\mp \frac{1}{m} \int \frac{w}{\sqrt{1 - w^2}}dw = x + c_1$, now let $w = \sin \phi$, so $dw = \cos \phi d\phi$ 
$\mp \frac{1}{m} \int \frac{\sin \phi}{\cos \phi}\cos \phi d\phi = x+c_1$ $\equiv$ $\mp \frac{1}{m}\cos \phi = x + c_1$  

and $u = \frac{C-w}{m}$, where $w = \sin \phi$, so $u = \frac{C - \sin \phi}{m}$ $ = \frac{C}{m} - \frac{1}{m}\sin \phi$ 
so this means that $x = c_1 - \frac{1}{m} \cos \phi $ 
so so thinking of $u = y$, we have that the shortest curve is some arc of a circle centered at $(c_1, C/m)$ with radius $-1/m$ 

##### Exercise 3

Because $P$ and $Q$ are on one side of the plane, the line segment from $P$ to $Q$ $= : A$ is on this same side, so we may project it onto the plane. 

The point $\bold x$ on $\Pi$ s.t. the broken line path from $P$ to $\bold x$ to $Q$ is the shortest lies on the projection of $A$ onto $\Pi$, so we only consider lines on $\Pi'$, the plane containing $P, Q$ and the closest point on $\Pi$ to $Q$ (contains the projection of $A$ on $\Pi$ and the normal to $\Pi$ with initial point $Q$ and all linear combinations of the two. [^2] 

So our problem goes from being 3D to 2D: finding the shortest broken line path consiting of lines only in $\Pi'$. Since the projection of $A$ is perpendicular to $\bold b$, we may think of the line going through the projection of $A$ as our $x$ axis and the line through $\bold b$ as the $y$ axis: 

$P = (x_1, y_1)$ and $Q = (x_2, y_2)$. We want to find the point $(x, 0)$ s.t.

$f(x) = \sqrt{(x-x_1)^2 + y_1^2} + \sqrt{(x-x_2)^2 + y_2^2}$ is minimized
$f'(x) = \frac{x - x_1}{\sqrt{(x-x_1)^2 + y_1^2}} + \frac{x-x_2}{\sqrt{(x-x_2)^2 + y_2^2}}$ $= 0$ 

$(x-x_1)\sqrt{(x-x_2)^2 + y_2^2} + (x-x_2)\sqrt{(x-x_1)^2 + y_1^2} = 0$ 

$(x-x_1)\sqrt{(x-x_2)^2 + y_2^2} = (x_2 - x)\sqrt{(x-x_1)^2 +y_1^2}$ 
$(x-x_1)^2[(x-x_2)^2 + y_2^2] = (x_2 - x)^2[(x-x_1)^2 + y_1^2]$ 

$(x- x_1)^2(x- x_2)^2 - (x-x_1)^2(x-x_2)^2 + y_2^2(x-x_1)^2 - y_1^2(x-x_2)^2 = 0$ 
$(y_2^2 - y_1^2)x^2 - 2(x_1y_2^2 - x_2y_1^2 )x + y_2^2x_1^2 - y_1^2 x_2^2 = 0$  

and because $(y_2 + y_1)(y_2 - y_1) = y_2^2 - y_1^2$ and 
$(y_2 x_1 + y_1 x_2)(y_2x_1 - y_1 x_2 ) = y_2^2 x_1^2 - y_1^2 x_2^2$, 

we have

$((y_2 + y_1)x - (y_2x_1 + y_1x_2)  )((y_2 - y_1)x - (y_2 x_1 - y_1 x_2)) = 0$  

so $x = \frac{y_2 x_1 + y_1 x_2}{y_2 + y_1}$, or $x = \frac{y_2 x_1 - y_1 x_2}{y_2 -y_1}$ 

Since $P$ and $Q$ are on the same side and thus have the same sign on $y_1, y_2$, and can have $y_1 = y_2$, 

$x = \frac{y_2 x_1 + y_1 x_2}{y_2 + y_1}$ 

so we use the dot product to find the angles, using normal $(0, 1)$ 
$x_1 - x = \frac{y_1(x_1 - x_2)}{y_1 + y_2}$  $x_2 - x = \frac{y_2(x_2 - x_1)}{y_1 + y_2}$ 

vector from $x$ to $P$: $\cos \theta_1 = \frac{(0, 1)\cdot (x_1 - x, y_1)}{1\sqrt{y_1^2(x_1-x_2)^2/(y_1 + y_2)^2 + y_1^2}}$ $ = \frac{y_1}{y_1 \sqrt{(x_1 - x_2)^2/(y_1 + y_2)^2 + 1}}$ $= \frac{1}{\sqrt{(x_1 - x_2)^2/(y_1 + y_2)^2 + 1}}$ 

vector from $x$ to $Q$:$\cos \theta_2 = $ $\frac{1}{\sqrt{(x_2 - x_1)^2/(y_1 + y_2)^2 + 1}}$  (from doing similar calculations as above)

and since $(x_2 - x_1)^2 = (x_1 - x_2)^2$, we have $\cos \theta_1 = \cos \theta_2$, and since we are only dealing with $0 \leq \theta \leq \pi$, we have $\theta_1 = \theta_2$ (especially since $P$ and $Q$ are on the same side of the $x$-axis)

[^2]: **Longer Explanation**: Let $\Pi'$ be the plane containing $A: =$ the line segment from $Q$ to $P$ and $B: =$ the line segment starting from $Q$ going in the direction of the normal vector to $\Pi$ to some point in $\Pi$, let $\bold a$, $\bold b$ be the vectors corresponding to these line segments. Since this plane is a vector space, it also contains the vector $\bold a + \bold b$, which goes from $\text{proj}_\Pi Q$ towards $\text{proj}_\Pi P$ , which is the vector corresponding to the projection of $A$ onto $\Pi$, so $\Pi'$ intersects $\Pi$ here.  Let $\bold c$ be the vector orthogonal to $\bold a$ and $\bold b$, i.e, the normal to $\Pi'$  if $\bold x$ lies on the projection of $A$ onto $\Pi$, then our path does not need to include a component outside of the plane, i.e., the corresponding vectors  will only consist of linear combinations of $\bold a$ and $\bold b$ ,(and our path, being the shortest path, will hit $\Pi$ on the projection of $A$, because that is the line of intersection of $\Pi$ and $\Pi'$) but if $\bold x$ lies anywhere else, our path will be a linear combination of $\bold a, \bold b, \bold c$ (since 1. these are linearly independent vectors and 2. we'd need $\bold c$ for the parts of the vector(s) lying outside of $\Pi'$

##### Exercise 4

$y = u(x)$ s.t. $E[u] = \int_0^1 (u'^2 + xu)\, dx$ is stationary with constraints $u(0) = 0, u(1) = 1$ 

$F(x, z, p) = p^2 + xz$ 

$F_z = x$, $F_p = 2p$, so we need: 
$-\frac{d}{dx}(2 u') + x = 0$ $\implies$ $2u'' =  x$ $u''= \frac{x}{2}$, $u' = \frac{x^2}{4} + C$, $u = \frac{x^3}{12} + Cx + D$ 

$u(0) = D = 0$, $u(1) = 1/12 + C = 1$, $C = 11/12$ 
$u(x) = \frac{x^3}{12} + \frac{11}{12}x$ 

##### Exercise 5

###### (a) Carry out the derivatives in (8) to write it as a second-order equation

$\frac{u_{xx}\sqrt{1 + u_x^2 + u_y^2} - u_x(u_xu_{xx} + u_yu_{yx})(\sqrt{1 + u_x^2 + u_y^2})^{-1} }{1 + u_x^2 + u_y^2} $ $+ \frac{u_{yy}\sqrt{1 + u_x^2 + u_y^2} - u_y(u_xu_{yx} + u_y u_{yy})(\sqrt{1 + u_x^2 + u_y^2})^{-1}}{1 + u_x^2 + u_y^2}$ $= 0$ 

$\frac{\sqrt{1 + u_x^2 + u_y^2}(u_{xx}+u_{yy})+ [- 2 u_x u_y u_{yx} - u_x^2 u_{xx} - u_y^2u_{yy}](\sqrt{1 + u_x^2 + u_y^2})^{-1}}{1 + u_x^2 + u_y^2} = 0$ 

multiplying by $(1 + u_x^2 + u_y^2)^{3/2}$ 
$(1 + u_x^2 + u_y^2)(u_{xx}+u_{yy}) - 2u_x u_y u_{yx} - \frac{1}{3}(u_x^3)_x - \frac{1}{3}(u_y^3)_y = 0$ 

we have $(u_x^3)_x$ and $(u_y^3)_y$ terms cancel out then:

$u_{xx} + u_{yy} + u_{xx}u_y^2  + u_x^2 u_{yy}- 2u_x u_y u_{yx} = 0$ 

###### (b) 

Elliptic: We have a PDE of the form: $a_{11}u_{xx} +  2a_{12}u_{xy} + a_{22}u_{yy} + a_1 u_x + a_2 u_y + a_0 u = 0$ 

$a_{11} = 1 + u_y^2, a_{22} = 1 + u_x^2$, $a_{12} = -u_x u_y$ 

$u_x^2u_y^2 < (1+u_x^2)(1 + u_y^2) = u_x^2 u_y^2 + u_x^2 + u_y^2 + 1$ 

so we do have that the PDE is elliptic. since $u_x^2 + u_y^2 + 1 > 0$ 

##### Exercise 6

minimal surfaces of revolution: $\int_a^b 2 \pi u(x)\sqrt{1 + u'(x)^2} \, dx$ for revolution around the $x$ axis provided $a \geq 0$ 

$F(x, z, p) = 2 \pi z \sqrt{1 + p^2}$ 
$F_z = 2 \pi \sqrt{1 + p^2}$, $F_p = 2 \pi z \frac{p}{\sqrt{1 + p^2}}$ 

Doesn't depend on $x$ explicitly so we may use: 

$2 \pi u \sqrt{1 + u'^2} - 2 \pi \frac{u u'^2}{\sqrt{1 + u'^2}} = C​$, for some constant $C​$ 
$2 \pi u(1 + u'^2) - 2 \pi uu'^2 = C \sqrt{1 + u'^2}​$ 
$2 \pi u = C \sqrt{1 + u'^2}​$ 
$4 \pi ^2 u^2= C^2(1 + u'^2)​$ 
$\frac{4 \pi ^2 u^2}{C^2} = 1 + u'^2​$ 

$\frac{du}{dx} = \pm \sqrt{\frac{4 \pi ^2 u^2}{C^2} - 1}$ , WLOG using $+$ (just multiply by $-1$ at the end otherwise)

$\pm \int \frac{du}{\sqrt{ (2 \pi /C)^2 u^2 - 1}} = x + A$, where $A$ is a constant

---

$y = \cosh x = \frac{e^x + e^{-x}}{2}​$ 
$x = \cosh y​$ $\implies​$ $2x = e^y + e^{-y}​$ $\implies​$ $e^y 2 x - e^{2y} - 1 = 0​$ 

let $w = e^y$, we have $w^2 - 2wx + 1 = 0$ 

$w = e^y= \frac{2x \pm 2\sqrt{x^2 - 1}}{2} = {x \pm \sqrt{x^2 - 1}}$ 

$y = \ln (x \pm \sqrt{ x^2 - 1})$ where

$(x - \sqrt{x^2 - 1}) = \frac{x^2 - (x^2 - 1)}{x + \sqrt{x^2 -1}} = \frac{1}{ x + \sqrt{x^2 - 1}}$

so $y = \pm \ln (x + \sqrt{x^2 - 1})​$ $= \cosh^{-1} x​$
and $y'(x) = \pm \frac{1 + x(\sqrt{x^2 - 1})^{-1}}{x + \sqrt{x^2 - 1}} ​$ $ = \pm[\frac{1}{x + \sqrt{x^2 - 1}} + \frac{x}{\sqrt{x^2 - 1}(x +\sqrt{x^2 - 1}}) ]​$ , multiplying the terms by $(x - \sqrt{x^2 - 1})/(x - \sqrt{x^2 - 1})​$ 

$ = \pm [ x - \sqrt{x^2 - 1} + \frac{x^2 - x \sqrt{x^2 - 1}}{\sqrt{x^2 - 1}}$ 

and $x\sqrt{x^2 - 1} /\sqrt{x^2 - 1} = x$, so the $x's​$ cancel out

and so we have: $\pm \frac{-x^2 + 1 + x^2}{\sqrt{x^2 - 1}} = \pm\frac{1}{\sqrt{x^2 - 1}}$

---

so this means our integral is $\frac{C}{2\pi}\cosh^{-1}( \frac{2 \pi u}{C}) = x + A$

so $u = \frac{C}{2\pi}\cosh (2\pi \frac{x+A}{C})$ , which is a catenary

##### Exercise 7

$F(x, u, p) =p^2(1+p)^2  =  p^2(1 + 2p + p^2) = p^2 + 2p^3 + p^4$ 
$F_u = 0$ 
$F_p = 2p + 6p^2 + 4p^3$ , then with $E-L$; 
$0 = \frac{d}{dx}[2u'(x) + 6u'(x)^2 + 4u'(x)^3]$ 
$0 = 2u''(x) + 6[2u'(x)u''(x)] + 4[3u'(x)^2u''(x)]$  
$0 = -[2u''(x) + 12u'(x)u''(x) + 12u'(x)^2u''(x)]$ 
$2u''(x)(-1 - 6u'(x) - 6u'(x)^2) = 0$ 

so first: $u''(x) = 0$ $\implies$ $u'(x) = a$ $\implies$ $u(x) = ax + b$ 

or solving: $-6u'^2 - 6u' - 1 = 0$ 

$u' =  \frac{6 \pm \sqrt{36 - 4(-6)(-1)}}{-12}$ $ = -\frac{1}{2} \pm \frac{1}{\sqrt{12}}$ let $m_1 = - \frac{1}{2} + \frac{1}{\sqrt{12}}$ and $m_2 = -\frac{1}{2} - \frac{1}{\sqrt{12}}$ 
which gives another two solutions: $u = m_1 x + c_1$, $u = m_2 x + c_2$ 

**Note: The Integrand must be $\geq 0$, and is equal to 0 when $h'(x) = 0$ or $h'(x) = -1$,** 
**this rules out the lines with $m_1, m_2​$ as slopes as minimizing solutions.** 

Due to the constraints: we can't have a constant function, nor is there a line with $u(x)= -x + b$ such that $u(0) = 1 \implies b = 1$ and $u(2) = 0 \implies b = 2$ 

so we construct a continuous piecewise function of lines with slopes $0$ and $-1$: 

If we used only $2$ lines to contstruct the PW function, then we'd be limited to:$u(x) = \begin{cases} 1 - x & 0 \leq x \leq 1 \\ 0 & 1 < x \leq 2 \end{cases}$ and $u(x) = \begin{cases} 1 & 0 \leq x \leq 1 \\ 2-x & 1 < x \leq 2\end{cases}$ 

However, if we used three lines we can descend from $1$, stay on some constant, then descend to $0$, each solution is determined by the constant and the intervals for the lines. 

since we know $u(0) = 1$ we have for our first line: $u_1(x) = -x +1$, for some interval $(0, s)$ and similarly, for the last line, $u_3(x) = -x + 2$ for some interval $(t, 2)$ 

so the constant in between, due to continuity, must be $1 - s = -t + 2$ 
so we can solve for $t$ in terms of $s$: $t = 1 +s$. So indeed, we have infinite solutions: 
$$
u_s(x) = \begin{cases} 1-x & 0 \leq x \leq s \\ 1-s & s < x \leq 1+s \\ 2-x & s < x \leq 2\end{cases}
$$
for  all $s \in [0, 1]$, so there are infinitely many points

##### Exercise 10

###### (a) 

let $u$ minimize $A[u]$ and $v$ be any function that vanishes at the boundary of $D$

let $F(x, t, z, p_1, q_1, p_2, q_2 )​$

$g(\epsilon ) = A[u + \epsilon v] = \int \int _D F(x, t, u + \epsilon v, u_x + \epsilon v_x, u_t+\epsilon v_t, u_{xx} + \epsilon v_{xx}, u_{tt} + \epsilon v_{tt})\, dx \, dt$ $g'(\epsilon ) = 0 = \iint_D \frac{\partial F}{\partial z}v + \frac{\partial F}{\partial p_1}v_x + \frac{\partial F}{\partial q_1}v_t + \frac{\partial F}{\partial p_2}v_{xx} + \frac{\partial F}{\partial q_2}v_{tt} \, dx \, dt $ 

Integrating by parts and using that $v​$ vanishes on the boundary:
$\iint_D \frac{\partial F}{\partial z}v - \frac{\partial}{\partial x}\frac{\partial F}{\partial p_1}v - \frac{\partial }{\partial t}\frac{\partial F}{\partial q_1}v - \frac{\partial }{\partial x}\frac{\partial F}{\partial p_2} v_x - \frac{\partial }{\partial t}\frac{\partial F}{\partial q_2} v_t \, dx \, dt​$  

Integrating by parts again:

$\iint_D \frac{\partial F}{\partial z}v - \frac{\partial}{\partial x}\frac{\partial F}{\partial p_1}v - \frac{\partial }{\partial t}\frac{\partial F}{\partial q_1}v +\frac{\partial^2 }{\partial x^2}\frac{\partial F}{\partial p_2} v + \frac{\partial^2 }{\partial t^2}\frac{\partial F}{\partial q_2} v\, dx \, dt$

factoring out $v​$ and making it equal to the first factor in the integral in $D​$ and using vanishing theorem, we have:

$\frac{\partial F}{\partial z} - \frac{\partial}{\partial x}\frac{\partial F}{\partial p_1} - \frac{\partial }{\partial t}\frac{\partial F}{\partial q_1} +\frac{\partial^2 }{\partial x^2}\frac{\partial F}{\partial p_2} + \frac{\partial^2 }{\partial t^2}\frac{\partial F}{\partial q_2}  = 0​$ 

So the Euler Lagrange equation for the action $A[u] = \iint (\frac{1}{2}u_x u_t + u_x^3 - \frac{1}{2}u_{xx}^2)\, dx \, dt​$ is

$F(x, t, z, p_1, q_1, p_2, q_2)​$ $= \frac{1}{2}p_1q_1 + p_1^3 - \frac{1}{2}p_2^2​$ 

$F_{p_1} = \frac{1}{2}q_1 + 3p_1^2$ , 
$F_{q_1} = \frac{1}{2}p_1$ 
$F_{p_2} = p_2$ 

$F_z = F_{q_2} = 0​$ 
$-\frac{\partial}{\partial x}(\frac{1}{2}u_t + 3u_x^2) - \frac{\partial}{\partial t}(\frac{1}{2}u_x) + \frac{\partial^2}{\partial x^2}(u_{xx}) = 0​$

$-\frac{1}{2}u_{tx} - 6u_xu_{xx} - \frac{1}{2}u_{tx}  + u_{xxxx} = 0$ 
$6u_xu_{xx} + u_{tx}+ u_{xxxx} = 0$ 

###### (b)

$v = u_x​$ $KdV​$ is $u_t + u_{xxx} + 6uu_x = 0​$ 

so $v​$ satisfies, from substituting $u_x​$ with $v​$ into the E-L formula above
$6vv_{x} + v_t + v_{xxx} = 0​$ , which is the KdV equation

##### Exercise 11

Using the same general formula as in 10.

$F = p_2^2 - q_1^2$ 
$\frac{\partial F}{\partial p_2} = 2p_2$ $\rightarrow $ $2u_{xx}$ , $\frac{\partial F}{\partial q_1} = -2u_t$ 

then using: (E-L) from exercise 10

$\frac{\partial F}{\partial z} - \frac{\partial}{\partial x}\frac{\partial F}{\partial p_1} - \frac{\partial }{\partial t}\frac{\partial F}{\partial q_1} +\frac{\partial^2 }{\partial x^2}\frac{\partial F}{\partial p_2} + \frac{\partial^2 }{\partial t^2}\frac{\partial F}{\partial q_2}  = 0$ 

$0 -0 - (-2u_{t})_t  + (2u_{xx})_{xx} + 0 = 0$ 

Simplifying gives: $u_{tt} + u_{xxxx} = 0$, which is the beam equation. 
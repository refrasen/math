Renee Senining
Math 126

### Homework 3
#### Section 2.3: Exercises 4, 6, 7
##### Exercise 4
**Consider the diffusion equation $u_t = u_{xx}$ in $\{0<x<1, 0 < t <\infin \}$ with $u(0,t) = u(1,t) = 0$ and $u(x,0) = 4x(1-x)$** 

**(a) Show that $0 < u(x,t) < 1​$ for all $t > 0​$ and $0<x<1​$** 

1. From the **Maximum Principle**, we have that the maximum value of $u(x,t)$ is on the lateral sides or at $t = 0$.  $u(0, t) = u(1, t) = 0$, and at $t = 0$, $u(x,0) = 4x(1-x) = 4x - 4x^2$ 
the max value of $4x(1-x)$ will be at either $x = 0$ or $x = 1$ or at the critical points:

$u_x(x,0) = (4x - 4x^2)_x = 4 - 8x$ and $u_x(x,0) = 4 - 8x = 0$ when $x = \frac{1}{2}$ 
So the max value of $u(x,0)​$ is $u(\frac{1}{2}, 0) = 2(\frac{1}{2}) = 1​$, which is larger than $u(0, t) = u(1, t) = 0​$ 

so $u(x,t) < 1​$ for $t > 0​$ and $0  < x < 1​$ 

2. And from the **Minimum Principle**, the minimum value of $u(x,t)$ is either

* $u(0,t) = u(1, t) = 0$ 
* or the mininum value of $u(x,0) = 4x(1-x)$ 

And since $0 < x < 1$, we have $1-x > 0$ and $4x > 0$, meaning $u(x,0) = (4x)(1-x) > 0$ for all $0 < x < 1$
so the minimum value of $u(x,t)$ is $0$ 
so we have $0 < u(x,t) < 1$ for all $t > 0$ and $0 <x< 1$ 

**(b) Show that $u(x,t) = u(1-x, t)$ for all $t \geq 0$ and $0 \leq x \leq 1$** 

For this, we'll use the **uniqueness** of an initial-boundary problem.

so at $x = 0$: $u(1-x, t) = u(1, t) = 0$ 

at $x = 1​$: $u(1-x, t) = u(0, t) = 0​$ 

and at $t = 0$: $u(1-x, 0) = 4(1-x)(1 - (1-x)) = (4 - 4x)(x) = 4x(1-x)$ 

and letting $w = 1-x$: 
$$
u_x(w, t) = \frac{\partial u}{\partial w}\frac{\partial w}{\partial x} + \frac{\partial u}{\partial t}\frac{\partial t}{\partial x} = -u_w(w,t)
\\ u_{xx}(w, t) = \frac{\partial u_w}{\partial w}\frac{\partial w}{\partial x} + \frac{\partial u}{\partial t}\frac{\partial t}{\partial x} = -(-u_{ww}(w, t)) = u_{ww}(w, t)\\
\text{ and }
\\ u_t(w, t) = \frac{\partial u}{\partial w}\frac{\partial w}{\partial t} + \frac{\partial u}{\partial t}\frac{\partial t}{\partial t} = u_t(w,t)
$$
and since $u_t(x, t) = u_{xx}(x,t)$ we have:

$u_t(w, t) = u_{xx}(w, t) = u_{ww}(w, t)$, so $u(1-x, t)$ solves the PDE with the initial and boundary conditions. And from the **uniqueness** of solutions to initial-boundary problems, $u(1-x, t) = u(x,t)$ 

**(c) Use the energy method to show that $\int_0^1 u^2 dx$ is a strictly decreasing function of $t$** 

We can use the following inequality that was derived in 2.3 right before equation (4) (since our boundary conditions are 0 here too): 

$\frac{d}{dt}\int_0^1 \frac{1}{2}[u(x,t)]^2 dx = - \int_0^1 u_x^2 dx \leq 0$ (since $u_x^2 \geq 0$ )

Multiplying by $2$: 

$\frac{d}{dt}\int_0^1 u^2 dx =-\int_0^1(2u_x^2)dx \leq  0$ 

Want to show $-2\int_0^1 u_x^2 dx < 0​$. 

If $\int_0^1 u_x^2 dx = 0$, due to $u_x^2 \geq 0$ and $u_x^2$ being continuous, we would have $u_x^2 \equiv 0$ 

$\implies$ $u_x = 0$ $\implies u_{xx } = 0 $ $\implies$ from the PDE, $u_t = u_{xx} = 0$ 

$\implies$ $u = \text{constant}$, and from the boundary conditions, this constant must be $0$ which contradicts

$0 < u(x,t) < 1$ in $0 < x < 1$ and $t > 0$ 

so $\int_0^1 u_x^2 dx > 0​$ $\implies​$ $-2\int_0^1 u_x^2 dx < 0​$, so $\int_0^1 u^2 dx​$ is strictly decreasing

##### Exercise 6

**Prove the *comparison principle* for the diffusion equation: If $u$ and $v$ are two solutions, and if $u \leq v$ for $t = 0$ and for $x = 0$ and for $x = l$, then $u\leq v$ for $0 \leq t < \infin$, $0 \leq x \leq l$** 

---

$$
v_t = kv_{xx}
\\ u_t = ku_{xx}
\\ v(x, 0) \geq u(x, 0)
\\ v(0, t) \geq u(0,t)
\\ v(l, t) \geq u(l, t)
$$

so let $w(x,t) = v(x, t) - u(x,t)​$ we have:

$w_t = v_t - u_t = k(v_{xx}-u_{xx}) = kw_{xx}$ 

$w(x,0) \geq 0$ , $w(0, t) \geq 0$, and $w(l, t)\geq 0$ $(*)$ 

From the minimum principle, we must have the minimum of $w(x,t)$ occur on either $x = 0$, $x = l$, or $t =0 $

and from $(*)$ the minimum value of $w(x,t)$ at $x = 0, x = l$, and $t = 0$ is $0$, which means

$w(x,t) = v(x,t) - u(x,t) \geq 0$ For $0 \leq t < \infin$ and $0 \leq x \leq l$ 

so $u(x,t) \leq v(x,t)$ for $0 \leq t < \infin$ and $0 \leq x \leq l$ 

##### Exercise 7

**(a) More generally, if $u_t - ku_{xx} = f, v_t - kv_{xx} = g, f \leq g$, and $u \leq v$ at $x = 0, x= l$, and $t = 0$, prove that $u \leq v$ for $0 \leq x \leq l$,  $0 \leq t < \infin$ **  
$$
u_t - ku_{xx} = f
\\ v_t - kv_{xx} = g
\\ f \leq g
\\ v(0, t) \geq u(0,t)
\\ v(l, t) \geq u(l,t)
\\ v(x, 0) \geq u(x, 0)
$$
So if we define $w = v - u$

$w_t - kw_{xx} = (v - u)_t - k(v-u)_{xx} \\ = v_t - u_t - kv_{xx} + ku_{xx} = v_t - kv_{xx} - (u_t - ku_{xx}) = g - f​$ 
$$
\\ w_t - kw_{xx} = g-f \geq 0
\\ w(0,t) \geq 0
\\ w(l,t) \geq 0
\\ w(x,0) \geq 0
$$
And since $w_t - kw_{xx} \geq 0$, this means there's a source (increases the temp/concentration), so to find the smallest value of $w$ we focus on

$w_t - kw_{xx} = 0$ 

and using the **Minimum Principle** like in Exercise 6, we have that the minimum value of $w(x,t)$ is $0$ (since $w(x,t) \geq 0$ at $x = 0$, $x =l$, and $t = 0$ )

so $v(x,t)- u(x,t) \geq 0$ $\equiv$ $u(x,t) \leq v(x,t)$ for all $0 \leq x \leq l$, $t \geq 0$ 

**(b) If $v_t - v_{xx} \geq \sin x$ for $0 \leq x \leq \pi$, $0 < t < \infin$, and if $v(0,t) \geq 0$, $v(\pi, t) \geq 0$, and $v(x,0) \geq \sin x$, use part $(a)$ to show that $v(x,t) \geq (1-e^{-t})\sin x$ ** 
$$
v_t - v_{xx} \geq \sin(x) \qquad0 \leq x \leq \pi, 0 < t <\infin 

\\ v(0,t) \geq 0
\\ v(\pi, t) \geq 0 
\\ v(x, 0) \geq \sin x
$$
**let $u(x,t) = (1-e^{-t})\sin x$ ** 

$u_t = e^{-t}\sin x$, $u_{xx} = -(1-e^{-t})\sin x = (e^{-t}-1)\sin x$ 

so $u$ satisfies: 

$u_t - u_{xx} = \sin(x)[e^{-t} + 1 - e^{-t}] = \sin(x)$

Let $g = v_t - v_{xx}​$, and $f = \sin(x)​$ 

Also, using what we know about $v(0,t), v(\pi, t)$, and $v(x,0)$ and the values:

$u(\pi k, t) = (1-e^{-t})\sin(\pi k) = 0$ for any integer $k$ and $u(x, 0) = (1-e^{-0})\sin x = (1-1)\sin x= 0$  

and in the interval $[0, \pi]$, $\sin x \geq 0$ $\implies$ $v(x, 0) \geq u(x,0)$ 

So we end up with: 
$$
v_t - v_{xx} = g 
\\ u_t - u_{xx} = f
\\ f \leq g
\\ v(0,t) \geq u(0,t)
\\ v(\pi, t) \geq u(\pi, t)
\\ v(x, 0) \geq u(x,0) 
$$
This is the same situation as in part $(a)$, with $k = 1$, $l = \pi$ , so from the result of part $(a)$, we can conclude that $v(x,t) \geq u(x,t)$ for $0 \leq x \leq \pi, 0 \leq t < \infin$ 

#### Section 2.4: Exercises 1, 6, 7, 9, 17

##### Exercise 1

**Solve the diffusion equation with the initial condition**
$$
\phi(x) = 1 \quad\text{for }|x|<l \quad \text{ and }\quad \phi(x)=0 \quad\text{for }|x| > l
$$
**Write your answer in terms of $\mathscr{E}\text{rf}(x)$** 

---

$u(x,t) = \frac{1}{\sqrt{4\pi kt}} \int_{-\infin}^\infin e^{-(x-y)^2/4kt}\phi(y)dy​$ 

$-l < x < l$: $\phi(x) = 1$, for $x < -l$ and $x > l$: $\phi(x) = 0$ $\implies$ 

$u(x,t) = \frac{1}{\sqrt{4\pi kt}} \int_{-l}^l e^{-(x-y)^2/4kt}dy$

let $p = (x-y)/\sqrt{4kt}​$ and $dp = -\frac{1}{\sqrt{4kt}} dy​$  

$u(x,t) = \frac{1}{\sqrt \pi} \int_{\frac{x-l}{\sqrt{4kt}}}^{\frac{x+l}{\sqrt{4kt}}} e^{-p^2}dp$ $= \frac{1}{\pi}[ \int_{_{(x-l)/\sqrt{4kt}}}^0 e^{-p^2}dp + \int_0^{(x+l)/\sqrt{4kt}}e^{-p^2}dp]$

$ = \frac{1}{2}[\mathscr{E}\text{rf}(\frac{x+l}{\sqrt{4kt}}) - \mathscr{E}\text{rf}(\frac{x-l}{\sqrt{4kt}})]$  

##### Exercise 6

**Compute $\int_0^{\infin}e^{-x^2}dx$** 

---

$\int_0^{\infin}e^{-x^2}dx \cdot \int_0^{\infin}e^{-y^2}dy$ $= \int_0^\infin \int_0^\infin e^{-(x^2 + y^2)}dxdy$ 

converting to polar coordinates: 

$x = r\cos \theta, y = r\sin \theta​$ 

$\frac{\partial(x, y)}{\partial(r, \theta)}$ $= |\begin{pmatrix} \cos \theta & -r\sin\theta \\ \sin \theta & r\cos \theta  \end{pmatrix}|$  $= r\cos^2\theta + r\sin^2\theta = r$ 

and $(x,y)​$ values exist in the 1st quadrant so:

$\int_0^\infin \int_0^{\pi/2} re^{-r^2}drd\theta ​$ $ = \int_0^\infin \frac{\pi}{2} re^{-r^2} = -\frac{\pi e^{-r^2}}{4}|_0^\infin​$

$ = -\frac{\pi}4 [\lim_{r \rightarrow \infin}{e^{-r^2}} - 1] = -\frac{\pi}4[-1] = \frac{\pi}{4}$

so $\int_0^\infin e^{-x^2}dx \cdot \int_0^\infin e^{-y^2}dy = \frac{\pi}{4}$

and since $\int_0^\infin e^{-x^2}dx = \int_0^\infin e^{-y^2}dy$, we can find the square root and obtain:

$\int_0^\infin e^{-x^2}dx = \frac{\sqrt{\pi}}{2}$ 

##### Exercise 7

**Use Exercise 6 to show that $\int_{-\infin}^\infin e^{-p^2}dp = \sqrt{\pi}$. Then substitute $p = x/\sqrt{4kt}$ to show that** 
$$
\int_{-\infin}^\infin S(x,t)dx = 1
$$

---

Since $e^{-(-p)^2} = e^{-p^2}$ (even function), using both $\int_{-a}^a f_{\text{even}}dx = 2\int_0^af_{even}dx$ and exercise 6:

$\int_{-\infin}^\infin e^{-p^2}dp = 2\int_0^\infin e^{-p^2}dp  = 2(\frac{\sqrt{\pi}}2) = \sqrt{\pi}$

$S = \frac{1}{2\sqrt{\pi k t}}e^{-x^2/4kt}$ 

$\int_{-\infin}^\infin S(x,t)dx$ $= \int_{-\infin}^\infin \frac{1}{2\sqrt{\pi k t}}e^{-x^2/4kt}dx$ $=$ 

(by substituting $p = \frac{x}{\sqrt{4 k t}}​$ , $dp = \frac{dx}{\sqrt{4kt}}​$)

$\frac{1}{\sqrt{\pi}} \int_{-\infin}^\infin e^{-p^2}dp = \frac{1}{\sqrt{\pi}}(\sqrt{\pi}) = 1​$ 

##### Exercise 9

**Solve the diffusion equation $u_t = ku_{xx}$ with the initial condition $u(x,0) = x^2$ by the following special method. First show that $u_{xxx}$ satisfies the diffusion equation with zero initial condition. Therefore, by uniqueness, $u_{xxx} \equiv 0$. Integrating this result thrice, obtain $u(x,t) = A(t)x^2 + B(t)x + C(t)$. Finally, it's easy to solve for $A, B, $ and $C$ by plugging into the original problem.** 

---

Differentiating $u_t = ku_{xx}$ w.r.t. $x$ three times:

$(u_{t})_{xxx} = (ku_{xx})_{xxx}$ $\equiv$ $u_{txxx} = ku_{xxxxx}$ 

and using that mixed derivatives are equal:

$(u_{xxx})_t = k(u_{xxx})_{xx}$ 

$u(x,0) = x^2 $ $\implies$ $u_x(x,0) = 2x$ $\implies$ $u_{xx}(x,0) = 2$ $\implies$ $u_{xxx}(x,0) = 0$ 

So $u_{xxx}$ satisfies the diffusion equation with *zero* initial condition, i.e., $u_{xxx}$ satisfies
$$
u_t = ku_{xx}
\\ u(x,0) = 0
$$

---

Due to uniqueness of solutions, since $0$ satisfies the above as well:

$u_{xxx} \equiv 0$ $\implies$ $u_{xx} = A(t)$ $\implies$ $u_{x} = A(t)x + B(t)$ $\implies$ $u = A(t)x^2 + B(t)x + C(t)$ 

so $u_t = A'(t)x^2 + B'(t)x + C'(t)$ 

$u_{xx} = 2A(t)$ 

$A '(t)x^2 + B'(t)x + C'(t) = 0x^2 + 0x +2kA(t)$ 

Matching coefficients:

$A'(t) = 0$ $\implies $ $A(t) = c_1$

$B'(t) = 0$ $\implies$ $B(t) = c_2$ 

$C'(t) = 2kA(t) = 2kc_1$  $\implies$ $C(t) = 2kc_1t$ 

$u(x,t) = c_1x^2 + c_2x + 2kc_1t$ 

$u(x,0) = c_1x^2 + c_2x = x^2​$ $\implies​$ $c_1 = 1, c_2 = 0​$ 

so $u(x,t) = x^2 + 2kt$  

##### Exercise 17

**Solve the diffusion equation with variable dissipation:**

$u_t - ku_{xx} +bt^2 u = 0 \qquad \text{for }-\infin<x<\infin \qquad \text{with }u(x,0) = \phi(x)$ 

**where $b>0$ is a constant** 

Following the hint: 

$u(x,t) = e^{-bt^3/3}v(x,t)$ [ $= w(t)v(x,t)$]

$u_t(x,t) = (-bt^2)e^{-bt^3/3}v(x,t) + e^{-bt^3/3}v_t(x,t)$ 

$u_{xx} = e^{-bt^3/3}v_{xx}(x,t)$

$(-bt^2)e^{-bt^3/3} v+ e^{-bt^3/3}v_t -ke^{-bt^3/3}v_{xx} + bt^2e^{-bt^3/3}v= 0​$

$e^{-bt^3/3}v_t - ke^{-bt^3/3}v_{xx} = e^{-bt^3/3}[v_t - kv_{xx}]=0$ 

and since $e^{-bt^3/3} \neq 0$ for all $t > 0$: 

the PDE becomes (along with initial condition)
$$
v = kv_{xx}
\\ v(x, 0) = e^{b*0^3/3}u(x, 0) = \phi(x)
$$
we know that the solution to this is

$v = \int_{-\infin}^\infin S(x-y, t)\phi(y)dy$ for $t > 0$

so $u(x,t) = e^{-bt^3/3}\int_{-\infin}^\infin S(x-y, t)\phi(y)dy$  

$ = \frac{e^{-bt^3/3}}{\sqrt{4\pi k t}}\int_{-\infin}^\infin e^{-(x-y)^2/4kt}\phi(y)dy$ 

#### Section 3.1: Exercise 2

**Solve** $u_t = ku_{xx}$; $u(x,0) = 0; u(0,t) = 1$ on the half-line $0 <x< \infin$ 
$$
u_t = ku_{xx} \quad \text{in }\{ 0 < x <\infin, 0 <t<\infin \}
\\ u(x,0) = \phi(x) = 0
\\ u(0, t) = 1
$$

---

We must have the boundary condition $= 0$ so define:

$v(x, t) = u(x,t) - 1$ so that

$v_t(x,t) = u_t(x,t)​$, $v_{xx} = u_{xx}(x,t)​$ 
$$
v_t = kv_{xx} \qquad \text{in }\{ 0<x<\infin, 0<t<\infin \}
\\ v(x,0) = -1 = \phi'(x)
\\ v(0, t) = 0
$$
so $v(x,t) = -\frac{1}{\sqrt{4\pi k t}}\int_0^\infin [e^{-(x-y)^2/4kt} - e^{-(x+y)^2/4kt}]dy$ (this is from plugging in the new initial condition to equation (6), since the above is of the form of (1) in the book)

$v(x,t) = -\frac{1}{\sqrt{4\pi k t}}[\int_0^\infin e^{-(x-y)^2/4kt}dy + \int_{\infin}^0 e^{-(x+y)^2/4kt}dy]$ 

for the left: $p = \frac{y-x}{\sqrt{4kt}}$ **because** $(x-y)^2 = (y-x)^2$ 
$$
\sqrt{4kt}\int_{-x/\sqrt{4kt}}^\infin e^{-p^2}dp
$$
for the right: $p = \frac{x+y}{\sqrt{4kt}}​$ 
$$
\sqrt{4kt}\int_{\infin}^{x/\sqrt{4kt}}e^{-p^2}dp
$$
So we end up with: 
$$
-\frac{1}{\sqrt{\pi}}\int_{-x/\sqrt{4kt}}^{x/\sqrt{4kt}}e^{-p^2}dp = -\frac{2}{\sqrt{\pi}}\int_0^{x/\sqrt{4kt}}e^{-p^2}dp
$$
since $e^{-p^2}​$ is an even function

so $v(x,t) = -\mathscr{E}\text{rf}(x/\sqrt{4kt})$ 

and $u(x,t) = 1 + v(x,t)= 1 - \mathscr{E}\text{rf}(x/\sqrt{4kt})$  

##### Exercise 3 (Using even reflection on Neumann)

Derive the solution formula for the half-line Neumann problem
$$
w_t - kw_{xx} = 0 \qquad \text{for }0<x<\infin, 0<t<\infin
\\ w_x(0,t) = 0 
\\ w(x,0) = \phi(x)
$$
So define $\phi_{\text{even}} = \begin{cases} \phi(x) & \text{for }x\geq 0 \\ +\phi(-x) & \text{for }x\leq 0 \end{cases}$

so define a new problem:
$$
u_t - ku_{xx} = 0 \qquad \text{for }-\infin<x<\infin, 0<t<\infin
\\ u(x, 0) = \phi_{\text{even}}(x)
$$
We know that the solution to this is $\frac{1}{\sqrt{4\pi k t}}\int_{-\infin}^\infin e^{-(x-y)^2/4kt}\phi_{\text{even}} (y)dy$ 

we restrict this solution to $0 <x<\infin$ and call it $w(x,t)​$ 

* Since $w(x,0) = \phi_{\text{even}}(x) = \phi(x)$ for positive $x$, we know the restriction solves the initial condition

* and since $w_x - kw_{xx}=  u_x - ku_{xx} = 0$ for $x > 0, t> 0$ (solves the PDE)

* and  $u(x,t)$ is even, proof: 

  $u(-x,t)​$ solves: $u_t(-x, t) - ku_{xx}(-x,t)=0​$, for $-\infin<x<\infin, t>0​$ $u(-x,0) = \phi_{\text{even}}(-x) = \phi_{\text{even}}(x)​$ 

  * Considering: $u(x, t) - u(-x, t)$, it satisfies the following:

  * $$
    u_t - ku_{xx} = 0, u(x,0) = 0
    $$

    *Argument*: 

    * since $u(x,t)$ and $u(-x,t)$ satisfy the diffusion equation, which is linear, the linear combination $u(x,t) -u(-x,t)$ also solves it
    * $u_x(0,t) - u_x(-0,t)  = 0-0 = 0$
    * $u(x,0) - u(-x, 0) = \phi_{\text{even}}(x) - \phi_{\text{even}}(x) = 0$ 

  * And since $0$ is a solution to this equation and solutions are unique, $u(x,t) - u(-x,t) = 0 \equiv u(x,t) = u(-x,t)$ 

which means $u_x(0,t)$, the derivative of an even function, is odd. 

---

Proof:

$g(x) = -x $ so $f(g(x))  = f(x)$ ($f$ is even)

and differentiating on both sides gives: $\frac{d}{dx}f(g(x)) = \frac{d}{dx}f(x)​$

and by chain rule on the left side:

$\frac{d}{dx}f(g(x)) = \frac{df}{dx}g(x)\cdot \frac{d}{dx}g(x) = -\frac{df}{dx}(-x) $ 

so $\frac{d}{dx}f(g(x)) = -f'(-x) = f'(x)$ $\implies$ $f'(-x) = -f'(x)$ $\square$

---

* and since $u_x(-x, t) =- u_x(x, t)​$, we have $u_x(-0, t) = -u_x(0,t)​$ $\implies​$ $u_x(0, t) = 0​$ 

**so $w_x(0, t) = 0$, satisfying the boundary condition.** 

To get an explicit formula for $w(x,t)$ 

$u(x,t) = \int_0^\infin S(x-y, t) \phi(y)dy +\int_{-\infin}^0S(x-y, t)\phi(-y)dy$ 

Then changing $-y $ to $+ y$ in the second integral:

$-\int_{\infin}^0 S(x+y, t)\phi(y)dy$ $= \int_0^\infin S(x+y, t)\phi(y)dy$ $\implies$ 

$u(x,t) = \int_0^\infin[ S(x-y, t) + S(x+y, t)]\phi(y)dy$ 

So for $0 <x<\infin$, $0 <t<\infin$ 
$$
w(x,t) = \frac{1}{\sqrt{4\pi k t}}\int_0^\infin [e^{-(x-y)^2/4kt}+ e^{-(x+y)^2/4kt}]\phi(y)dy
$$

#### Section 3.2: Exercise 1

**Solve the Neumann problem for the wave equatin on the half-line $0<x<\infin$** 

---

$$
w_{tt}-c^2w_{xx} = 0 \qquad \text{ for }x>0, -\infin<t<\infin
\\ w(x,0) = \phi(x) \qquad x>0
\\ w_t(x,0) = \psi(x) \qquad x>0
\\ w_x(0,t) = 0 \qquad \text{for }x = 0 \text{ and }-\infin <t <\infin
$$

Define:

$\phi_{\text{even}} = \begin{cases} \phi(x) & x\geq 0 \\ \phi(-x) & x\leq 0 \end{cases}$ and $\psi_{\text{even}} = \begin{cases} \psi(x) & x\geq 0 \\ \psi(-x) & x \leq 0 \end{cases}$ 

so we solve instead:
$$
u_{tt}-c^2u_{xx} = 0 \qquad -\infin<x<\infin, -\infin<t<\infin
\\ u(x,0) = \phi_{\text{even}} \qquad x>0
\\ u_t(x,0) = \psi_{\text{even}}\qquad x>0
$$
We now the solution for this is:
$$
u(x,t) = \frac{1}{2}[\phi_{\text{even}}(x+ct)+ \phi_{\text{even}}(x-ct)] + \frac{1}{2c}\int_{x-ct}^{x+ct}\psi_{\text{even}}(s)ds
$$
Let $w(x,t) = u(x,t)$ restricted to $x > 0$ 

* $\implies$ $w_{tt}- c^2w_{xx} = u_{tt} -c^2u_{xx} =0$ for $x>0$, solving the differential equation
* $w(x, 0) = \phi_{\text{even}}(x) = \phi(x)$ for $x> 0$ 
* $w_t(x, 0) = \psi_{\text{even}}(x) = \psi(x)$ for $x > 0$ so $w$ solves the initial conditions
* and since $u$ is even, making $u_x(x,t)$ an odd function, which must satisfy $u_x(0,t) = -u_x(-0,t)$ $\implies$ $w_x(0, t) = 0​$ 

Proof that $u$ is even:

$u(-x,t)$ satisfies $u_{tt} -c^2u_{xx} = 0$, $u(-x,0) = \phi_{\text{even}}(-x) = \phi_{\text{even}}(x)$, $u_t(-x, 0) = \psi_{\text{even}}(-x ) = \psi_{\text{even}}(x)$,  (same bounds as above for the extended problem)

so due to the wave equation being linear, $u(x,t) -u(-x,t)$ solves it and:

* $u(x,0) - u(-x, 0) = \phi_{\text{even}} - \phi_{\text{even}}​$ $ = 0​$, and this same argument with $\psi_{\text{even}}​$ applies to $u_t(x, 0) - u_t(-x,0) = 0​$  
* and due to uniqueness of solutions: $u(x,t) - u(-x,t) = 0$ $\implies$ $u(x,t) = u(-x,t)$ 

For an explicit formula, there are three regions to consider:

**Case 1**: $x + ct > 0$ and $x - ct > 0$, we only have positive arguments

$\implies​$ 
$$
w(x,t) = \frac{1}{2}[\phi(x+ct)+\phi(x-ct)] + \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(s)\; ds
$$
When $0 <x<c|t|$ , we use $\phi_{\text{even}}(x-ct) = \phi(ct-x)$  

**Case 2**: $x - ct < 0$ and  $x + ct>0$ 

$w(x,t) = \frac{1}{2}[\phi(x+ct)+ \phi(ct-x)] + \frac{1}{2c}[\int_0^{x+ct} \psi(s)ds + \int_{x-ct}^0 \psi(-s)ds]$

and switching variables from $-s \rightarrow + s$ in the second integral:
$$
w(x,t) = \frac{1}{2}[\phi(x+ct)+\phi(ct-x)] + \frac{1}{2c}[\int_{0}^{ct-x}\psi(s)ds + \int_0^{x+ct}\psi(s)ds]
$$
**Case 3**: $x - ct > 0$ and $x + ct < 0$ 

$w(x,t) = \frac{1}{2}[\phi(-x-ct) + \phi(x-ct)] + \frac{1}{2c}[\int_0^{x+ct}\psi(-s)ds + \int_{x-ct}^0 \psi(s)ds]​$ 

switching variables in the first integral from $-s \rightarrow +s$: 
$$
w(x,t) = \frac{1}{2}[\phi(-x-ct)+ \phi(x-ct)]+ \frac{1}{2c}[\int_{-x-ct}^0\psi(s)ds + \int_{x-ct}^0\psi(s)ds]
$$
or
$$
w(x,t) = \frac{1}{2}[\phi(-x-ct)+ \phi(x-ct)]- \frac{1}{2c}[\int_{0}^{-x-ct}\psi(s)ds + \int^{x-ct}_0\psi(s)ds]
$$
Could easily do:
$$
w(x,t) = \begin{cases}\frac{1}{2}[\phi(x+c|t|)+\phi(-x+c|t|)] +  \frac{t}{2c|t|}[\int_0^{-x+c|t|} \psi(s)ds + \int_0^{x+c|t|}\psi(s)ds & 0<x<c|t|
\\
\frac{1}{2}[\phi(x+ct) + \phi(x-ct)]+ \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(s)ds &x>c|t|
\end{cases}
$$

#### Section 3.3: Exercises 1, 3

##### Exercise 1

**Solve the inhomogeneous diffusion equation on the half-line with Dirichlet boundary condition:**
$$
u_t - ku_{xx} = f(x,t) \qquad (0<x<\infin, 0 <t<\infin)
\\ u(0,t) = 0 \qquad u(x,0) = \phi(x)
$$
**using method of reflection**

---

Using **odd** reflection:

define $f_{\text{odd}}(x,t) = \begin{cases} f(x,t) \qquad x>0, t>0 \\ -f(-x, t) \qquad x<0, t>0 \end{cases}$, and $\phi_{\text{odd}}(x) = \begin{cases} \phi(x) \qquad x> 0 \\ -\phi(-x) \qquad x< 0\end{cases}$ 

So we have a new problem:
$$
U_t - kU_{xx} = f_{\text{odd}}(x,t) \qquad (-\infin<x<\infin, 0<t<\infin)
\\ U(x,0) = \phi_{\text{odd}}(x)
$$
We know the solution to this problem from section 3.3's discussion: $U(x,t) = \int_{-\infin}^\infin S(x-y, t) \phi_{\text{odd}}(y) dy + \int_0^t \int_{-\infin}^\infin S(x-y, t-s)f_{\text{odd}}dyds$ 

We claim the restriction, call it $u(x,t)$,  to the half line $0 <x<\infin$ is the solution to the inhomogeneous diffusion equation on the half-line with the Dirichlet boundary condition:

* Since $u(x,t) = U(x,t)$ on $x> 0$, $u_t - ku_{xx} = U_t - kU_{xx} = f_{\text{odd}}(x,t)$, which from its definition, $ = f(x,t)$ on $0<x<\infin, 0<t<\infin$ 
* And similalry, $u(x,0) = U(x,0) = \phi_{\text{odd}}(x) = \phi(x)$ (from the definition of $\phi_{\text{odd}}$ on $x>0$) 
* And since $\phi_{\text{odd}}$, $f_{\text{odd}}$  is odd, $U$ is odd:

$U(-x,t)$ satisfies

$U_t- kU_{xx} = f_{\text{odd}}(-x,t) =- f_{\text{odd}}(x,t)$ and $U(-x,0) = \phi_{\text{odd}}(-x) = -\phi_{\text{odd}}(x)$ (PDE is defined for​$ - \infin <x <\infin, 0 <t<\infin$)

so $U(x,t) + U(-x,t)​$ (due to linearity) satisfies:

$U_t(x,t) + U_t(-x,t)  - k(U_{xx}(x,t) + U_{xx}(-x,t)) = f_{\text{odd}}(x,t) - f_{\text{odd}}(x,t) = 0$, $U(x, 0) + U(-x, 0) = \phi_{\text{odd}} - \phi_{\text{odd}} = 0$ 

obviously $0$ is a solution for this problem, and by Uniqueness of solutions: $U(x,t) +U(-x,t) = 0$ $\implies $ $U(-x,t) = -U(x,t)$, so **U is odd** 

and since $U$ is odd, $U(0,t) = U(-0,t) = -U(0,t) \implies U(0,t) = 0 $ 

so the restriction of $U$ to the half line, $u$, also satisfies $u(0,t) = 0$

So $u(x,t) = \int_{-\infin}^\infin S(x-y,t)\phi_{\text{odd}}(y)dy + \int_0^t\int_{-\infin}^{\infin} S(x-y, t-s)f_{\text{odd}}(y, s)dyds$ restricted to $0<x<\infin$ 

we know from 3.1 that the first integral is equal to:

$\int_0^\infin [S(x-y, t)-S(x+y,t)]\phi(y)dy​$ 

And the second integral is equal to:
$$
\int_0^t [\int_0^\infin S(x-y, t-s)f(y,s)dy-\int_{-\infin}^0S(x-y,t-s)f(-y,s)dy]ds
\\ \text{switching variables in second integral, }-y \rightarrow +y
\\ = \int_0^t[\int_0^\infin S(x-y, t-s)f(y,s)dy +\int_{\infin}^0S(x+y, t-s)f(y,s)dy]ds
\\ = \int_0^t\int_0^\infin [S(x-y,t-s)-S(x+y, t-s)]f(y,s)dyds
$$
So the solution: is
$$
u(x,t) = \int_0^\infin [S(x-y, t)-S(x+y,t)]\phi(y)dy + \int_0^t\int_0^\infin [S(x-y,t-s)-S(x+y, t-s)]f(y,s)dyds
$$

##### Exercise 3

**Solve the inhomogeneous Neumann diffusion problem on the half-line**
$$
w_t - kw_{xx} = 0 \qquad \text{for }0<x<\infin, 0<t<\infin
\\ w_x(0,t) = h(t) \qquad w(x,0) = \phi(x)
$$
**by the subtraction method indicated in the text**

---

* Subtract off the function $xh(t)​$...

Define $W(x,t) = w(x,t) - xh(t)$, assuming $w(x,t)$ is a solution to the above...

$W(x,t)$ satisfies:
$$
(*)\qquad W_t -kW_{xx} = w_t(x,t) - xh'(t) - k(w_{xx}(x,t)-0) = -xh'(t) \qquad 0<x<\infin, 0<t<\infin
\\ W_x(0,t)= w_x(0,t) -h(t) = h(t) - h(t) = 0
\\ W(x,0) = w(x,0) -xh(0) = \phi(x) -xh(0)
$$

* Use the method of reflection (**even**) to get $W$ 

let $F(x,t) = -xh'(t)$ , $\Phi(x) = \phi(x)-xh(0)$ 

We define $\Phi_{\text{even}} = \begin{cases} \phi(x) -xh(0)& x>0 \\ \phi(-x) +xh(0)& x< 0 \end{cases}$ , and $F_{\text{even}}(x,t) = \begin{cases} -xh'(t) & x>0 \\ xh'(t) & x<0\end{cases}$

Define new problem
$$
(**) \begin{cases}
U_t - kU_{xx} = F_{\text{even}}(x,t) & -\infin<x<\infin, 0 <t<\infin
\\ U(x,0) = \Phi_{\text{even}}
\end{cases}
$$
The solution to this:

$U(x,t) = \int_{-\infin}^\infin S(x-y, t)\Phi_{\text{even}}(y)dy + \int_0^t\int_{-\infin}^\infin S(x-y, t-s)F_{\text{even}}(y,s)dyds$ 

**Claim**: If $W(x,t) = U(x,t)$ for $x > 0$, then $W(x,t)$ is a solution to $(*)$ 

* $W = U$ for $x>0$ $\implies$ $W$ solves the partial differential equation on the half line

* $W(x,0) = U(x,0) = \Phi_{\text{even}}(x)$, and since $x>0$, $\Phi_{\text{even}}(x) = \Phi(x) = \phi(x)-xh(0)$, solving the initial condition

* Since $U(x,t)$ is even, *shown in earlier Neumann problem why*, $U_x$ is odd $\implies$ 

  $U_x(0,t) = U_x(-0,t) = -U_x(0,t)$ $\implies$ $U_x(0,t) = 0$ 

  so $W_x(0,t) = 0$  

We know from the homogeneous equation what $\int_{-\infin}^\infin S(x-y, t)\Phi_{\text{even}}(y)$ will look like and start from there
$$
W(x,t) = \int_{0}^\infin [S(x-y,t)+S(x+y,t)]\phi(y)dy -\int_0^\infin[S(x-y, t)+S(x+y,t)]yh(0)dy
\\ + \int_{0}^t[\int_0^\infin S(x-y, t-s)[-y\frac{d}{ds}h(s)]dy + \int_{-\infin}^0 S(x-y, t-s)[y\frac{d}{ds}h(s)]dy]ds
$$
Focusing on the double integral, then eventually switching order of integration
$$
\int_0^t[\int_0^\infin S(x-y,t-s)[-y\frac{d}{ds}h(s)]dy - \int_{\infin}^0S(x+y, t-s)[-y\frac{d}{ds}h(t)]dy]ds
\\ \int_0^t \int_0^\infin[S(x-y, t-s)+S(x+y, t-s)][-y\frac{d}{ds}h(s)]dyds
\\ = \int_0^\infin\int_0^t [S(x-y,t-s)+S(x+y,t-s)][-y\frac{d}{ds}h(s)]ds dy
$$
then using Integration by Parts
$$
\int_0^\infin[S(x-y,t-s)+S(x+y, t-s)](-yh(s))|_0^t dy
\\ - \int_0^\infin\int_0^t\frac{\partial}{\partial s}[S(x-y, t-s)+S(x+y, t-s)](-yh(s)) dsdy
\\ = \int_0^\infin \lim_{s \uparrow t}[(S(x-y, t-s)+S(x+y, t-s))(-yh(s))]dy 
\\ + \int_0^\infin[S(x-y, t) + S(x+y, t)]yh(0)dy -\int_0^\infin \int_0^t \frac{\partial }{\partial s}[S(x-y, t-s)+S(x+y, t-s)](-yh(s))dsdy
$$
as $s \rightarrow t$, $t-s \rightarrow 0$ $\implies$ $S(x\pm y, t-s) = \frac{1}{\sqrt{4k(t-s)}} e^{-(x\pm y)^2/(4k(t-s))} \rightarrow 0$

so $\int_0^\infin \lim_{s \uparrow t}[(S(x-y, t-s)+S(x+y, t-s))(-yh(s))]dy = 0$ 

and we have so far:
$$
W(x,t) = \int_0^\infin[S(x-y, t)+S(x+y,t)]\phi(y)dy - \int_0^\infin [S(x-y,t)+S(x+y,t)]yh(0)dy 
\\+ \int_0^\infin[S(x-y,t)+S(x+y,t)]yh(0)dy
\\- \int_0^\infin \int_0^t\frac{\partial}{\partial s}[S(x-y, t-s)+ S(x+y, t-s)](-yh(s))dsdy
\\ = \int_0^\infin [S(x-y,t)+S(x+y,t)]\phi(y)dy 
\\- \int_0^\infin\int_0^t\frac{\partial }{\partial s}[S(x-y, t-s)+ S(x+y, t-s)](-yh(s))dsdy
$$
and switching the order of integration in the double integral one last time and pulling the negative out of $(-yh(s))$ 
$$
W(x,t) = \int_0^\infin[S(x-y,t)+S(x+y,t)]\phi(y)dy + \int_0^t\int_0^\infin \frac{\partial}{\partial s}[S(x-y, t-s)+ S(x+y, t-s)](yh(s))dsdy
$$
and so the solution to the original problem is $w(x,t) = W(x,t) +xh(t)$ giving:
$$
w(x,t) = xh(t) + \int_0^\infin[S(x-y,t)+S(x+y,t)]\phi(y)dy 
\\ + \int_0^t\int_0^\infin \frac{\partial}{\partial s}[S(x-y, t-s)+ S(x+y, t-s)](yh(s))dsdy
$$

#### Section 3.4: Exercises 1, 6

##### Exercise 1

**Solve $u_{tt} =c^2u_{xx} +xt$, $u(x,0) = 0$, $u_t(x,0) = 0$** 

---

Using **Theorem 1**, with $f(x,t) = xt$, $\phi(x) = 0$, $\psi(x) = 0$ 

$u(x,t) = \frac{1}{2c} \int \int_{\Delta} ys d\Delta$ $ = \frac{1}{2c}\int_0^t\int_{x-c(t-s)}^{x+c(t-s)}ys \; dyds$ $ = \frac{1}{2c}\int_0^t \frac{s}{2}[x^2 + 2xc(t-s) + c^2(t-s)^2 - x^2 + 2xc(t-s) - c^2(t-s)^2]ds$ 

$= \frac{1}{2c} \int_0^t s [2xc(t-s)]ds$ $= \int_0^t sxt-s^2xds$ $ = [\frac{ts^2x}{2} - \frac{s^3x}{3}]_0^t$ $= x(\frac{t^3}{2}-\frac{t^3}{3})$  $= x \frac{t^3}{6}$ 

##### Exercise 6

**Derive the formula for the inhomogeneous wave equation in yet another way**

---

**a. Write it as the system $u_t + cu_x = v, v_t - cv_x = f$** 

This is possible due to factoring:$(\frac{\partial^2}{\partial t^2}-c^2 \frac{\partial^2}{\partial x^2})u$ $= (\frac{\partial}{\partial t}-c\frac{\partial}{\partial x})(\frac{\partial }{\partial t}+ c\frac{\partial}{\partial x})u = f$ 

so letting $u_t + cu_{x} = v$: $(\frac{\partial}{\partial t}-c\frac{\partial}{\partial x}) v = f$, and we get the following
$$
u_t + cu_x = v
\\ v_t - cv_x = f
$$
**b. Solve the first equation for $u$ in terms of $v$ as**

**$u(x,t) = \int_0^t v(x-ct + cs, s)ds$** 

$(1, c)\cdot (u_t, u_x) = v$ 

So along the curves satisfying $\frac{dx}{dt} = c$, i.e., $x = ct + A$ 

we obtain:

$\frac{d}{dt}u(ct+A, t) = \frac{\partial u}{\partial t} + \frac{\partial u}{\partial x}c = v(ct+A, t)​$ 

so integrating

$u(ct+A, t) = \int_0^t v(cs+A, s)ds + g(A)$, where $g$ is an arbitrary function of $A$ 

And to change back to original variable $x$: since $x = ct +A$, $A = x - ct$ we have:

$u(x, t) = \int_0^t v(x-ct + cs, s)ds + g(x-ct)$, 

and for this problem, if $g(x-ct)$ $= 0$ we have

$u(x,t) = \int_0^t v(x-ct + cs, s)ds$ 

**c. Similalrly, solve the second equation for $v$ in terms of $f$** 

so $(1, -c)\cdot (v_t, v_x) = f$ 

Along the curves satisfying $\frac{dx}{dt} =-c​$, $x = -ct +B ​$, we obtain

$\frac{d}{dt}v(-ct+B, t) = \frac{\partial v}{\partial t} + \frac{\partial v}{\partial x}(-c) = f(-ct + B, t)​$ 

integrating:

$v(-ct+B, t) = \int_0^t f(-cs + B, s) ds + h(B)$, where $h$ is an arbitrary function of $B$ 

Switching back to origianl variable $x$: $B = x+ct$ 

$v(x, t) = \int_0^t f(x + ct - cs, s)ds + h(x+ct)​$ 

and if $h(x+ct) = 0$ we get

$v(x,t) = \int_0^t f(x+ct - cs, s)ds​$

**d. Substitute part (c) into part (b) and write as an iterated integral** 

$u(x,t) = \int_0^t v(x-ct + cs, s)ds$ $+ g(x-ct)$ 

$v(x-ct+cs, s) = \int_0^s f(x-ct+cs +cs - cr, r)dr$ $+ h(x-ct +cs+cs)$ 

$\int_0^t \int_0^s f(x - ct + 2cs - cr, r)dr ds +G(x-ct) + H(x+ct)​$ 

**new arbitrary functions $G$, $H$ to simplify** 

$0\leq s \leq t​$ and $0 \leq r \leq s \leq t​$  

so when we switch integration we have instead:

$0\leq r \leq t$, $r\leq s \leq t$ 

$\int_0^t \int_r^t f(x-ct + 2cs - cr, r)dsdr + G(x-ct) +H(x+ct)$ 

let $y = x-ct + 2cs - cr$ $\implies$ $dy = 2c ds$

$\frac{1}{2c} \int_0^t \int_{x - ct + cr}^{x+ct - cr} f(y, r) dy dr$ $+ G(x-ct) + H(x+ct)$ 

and we can simply do $r = s$ to get

$\frac{1}{2c}\int_0^t \int_{x - ct + cs}^{x +ct - cs} f(y,s)dyds​$. $+G(x-ct) + H(x+ct)​$ 

and note: $x-ct +cs = x-c(t-s)$, $x+ct - cs = x+c(t-s)$ 

This gives a general solution. And initial conditions give:

$u(x,0) = G(x)+ H(x) = \phi(x)​$ 

and to get $u_t(x,t)$: 

$\frac{\partial}{\partial t}[\frac{1}{2c}\int_0^t \int_{x-ct+cs}^{x+ct-cs}f(y,s)dyds] + -cG'(x-ct) +cH'(x+ct)​$

$= \frac{1}{2c}[\int_{0}^{t} \frac{\partial }{\partial t}(\int_{x-ct+cs}^{x+ct-cs} f(y,s)dy)ds +\underbrace{\int_{x-ct+ct}^{x+ct-ct}f(y,t)dy}_{=0}] -cG'(x-ct)+cH'(x+ct)​$ 

$ = \frac{1}{2c}\int_0^t [\int_{x-ct+cs}^{x+ct-cs}\underbrace{\frac{\partial f}{\partial t}(y, s}_{=0, ds/dt = 0})dy + f(x+ct-cs, s)(c) - f(x-ct+cs, s)(-c)]ds \\-cG'(x-ct) +cH'(x+ct)$

$= \frac{1}{2}\int_0^t f(x+ct-cs, s) + f(x-ct + cs, s) ds - cG'(x-ct) +cH'(x+ct) ​$

$u_t(x, 0) = -cG'(x) + cH'(x)​$  $= \psi(x)​$ 

$G'(x) + H'(x) = \phi'(x)​$ $\implies​$ $cG'(x) +cH'(x) = c\phi'(x)​$ 

$\oplus​$: $2cH'(x) = c\phi'(x) + \psi(x)​$ $\ominus​$: $2cG' = c\phi'(x) - \psi​$ 

$H'(x) = \frac{1}{2}\phi'(x) + \frac{1}{2c}\psi(x)​$, $G'(x) = \frac{1}{2}\phi'(x) - \frac{1}{2c}\psi​$ 

and from 2.1, we know this results in

$H(x+ct)+G(x-ct) = \frac{1}{2}[\phi(x+ct)-\phi(x-ct)] + \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(s)ds​$ 

So we have:
$$
u(x,t) = \frac{1}{2c}\int_0^t \int_{x - ct + cs}^{x +ct - cs} f(y,s)dyds +\frac{1}{2}[\phi(x+ct)-\phi(x-ct)] + \frac{1}{2c}\int_{x-ct}^{x+ct}\psi(s)ds
$$
as the unique solution to the initial value problem. (Inhomogeneous)



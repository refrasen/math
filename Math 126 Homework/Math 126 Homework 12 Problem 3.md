Because $P​$ and $Q​$ are on one side of the plane, the line segment from $P​$ to $Q​$ $= : A​$ is on this same side, so we may project it onto the plane. 

The point $\bold x$ on $\Pi$ s.t. the broken line path from $P$ to $\bold x$ to $Q$ is the shortest lies on the projection of $A$ onto $\Pi$, so we only consider lines on $\Pi'$, the plane containing $P, Q$ and the closest point on $\Pi$ to $Q$ (contains the projection of $A$ on $\Pi$ and the normal to $\Pi$ with initial point $Q$ and all linear combinations of the two)

So our problem goes from being 3D to 2D: finding the shortest broken line path consiting of lines only in $\Pi'$. Since the projection of $A$ is perpendicular to $\bold b$, we may think of the line going through the projection of $A$ as our $x$ axis and the line through $\bold b$ as the $y$ axis: 

$P = (x_1, y_1)$ and $Q = (x_2, y_2)$. We want to find the point $(x, 0)$ s.t.

$f(x) = \sqrt{(x-x_1)^2 + y_1^2} + \sqrt{(x-x_2)^2 + y_2^2}$ is minimized
$f'(x) = \frac{x - x_1}{\sqrt{(x-x_1)^2 + y_1^2}} + \frac{x-x_2}{\sqrt{(x-x_2)^2 + y_2^2}}$ $= 0​$ 

$(x-x_1)\sqrt{(x-x_2)^2 + y_2^2} + (x-x_2)\sqrt{(x-x_1)^2 + y_1^2} = 0$ 

$(x-x_1)\sqrt{(x-x_2)^2 + y_2^2} = (x_2 - x)\sqrt{(x-x_1)^2 +y_1^2}​$ 
$(x-x_1)^2[(x-x_2)^2 + y_2^2] = (x_2 - x)^2[(x-x_1)^2 + y_1^2]​$ 

$(x- x_1)^2(x- x_2)^2 - (x-x_1)^2(x-x_2)^2 + y_2^2(x-x_1)^2 - y_1^2(x-x_2)^2 = 0$ 
$(y_2^2 - y_1^2)x^2 - 2(x_1y_2^2 - x_2y_1^2 )x + y_2^2x_1^2 - y_1^2 x_2^2 = 0$  

and because $(y_2 + y_1)(y_2 - y_1) = y_2^2 - y_1^2$ and 
$(y_2 x_1 + y_1 x_2)(y_2x_1 - y_1 x_2 ) = y_2^2 x_1^2 - y_1^2 x_2^2$, 

we have

$((y_2 + y_1)x - (y_2x_1 + y_1x_2)  )((y_2 - y_1)x - (y_2 x_1 - y_1 x_2)) = 0$  

so $x = \frac{y_2 x_1 + y_1 x_2}{y_2 + y_1}$, or $x = \frac{y_2 x_1 - y_1 x_2}{y_2 -y_1}$ 

Since $P$ and $Q$ are on the same side and thus have the same sign on $y_1, y_2$, and can have $y_1 = y_2$, 

$x = \frac{y_2 x_1 + y_1 x_2}{y_2 + y_1}$ 

so we use the dot product to find the angles, using normal $(0, 1)$ 
$x_1 - x = \frac{y_1(x_1 - x_2)}{y_1 + y_2}$  $x_2 - x = \frac{y_2(x_2 - x_1)}{y_1 + y_2}$ 

vector from $x$ to $P$: $\cos \theta_1 = \frac{(0, 1)\cdot (x_1 - x, y_1)}{1\sqrt{y_1^2(x_1-x_2)^2/(y_1 + y_2)^2 + y_1^2}}$ $ = \frac{y_1}{y_1 \sqrt{(x_1 - x_2)^2/(y_1 + y_2)^2 + 1}}$ $= \frac{1}{\sqrt{(x_1 - x_2)^2/(y_1 + y_2)^2 + 1}}$ 

vector from $x$ to $Q$:$\cos \theta_2 = $ $\frac{1}{\sqrt{(x_2 - x_1)^2/(y_1 + y_2)^2 + 1}}$  (from doing similar calculations as above)

and since $(x_2 - x_1)^2 = (x_1 - x_2)^2$, we have $\cos \theta_1 = \cos \theta_2$, and since we are only dealing with $0 \leq \theta \leq \pi$, we have $\theta_1 = \theta_2$ (especially since $P$ and $Q$ are on the same side of the $x$-axis)



**Longer Explanation**: Let $\Pi'$ be the plane containing $A: =$ the line segment from $Q$ to $P$ and $B: =$ the line segment starting from $Q$ going in the direction of some normal vector to $\Pi$ to some point in $\Pi$, let $\bold a$, $\bold b$ be the vectors corresponding to these line segments. 
Since this plane is a vector space, it also contains the vector $\bold a + \bold b$ going from the projection of $Q$ on to $\Pi$ towards the projection of $P$ on to $\Pi$ , which is the projection of $A$  onto $\Pi$, so $\Pi'$ intersects $\Pi$ here. 

Let $\bold c$ be the vector orthogonal to $\bold a$ and $\bold b$, i.e, the normal to $\Pi'$  

if $\bold x$ lies on the projection of $A$ onto $\Pi$, then our path does not need to include a component outside of the plane, i.e., it will only be a linear combination of $\bold a$ and $\bold b$ ,(and will hit $\Pi$ at the projection of $A$) but if $\bold x$ lies anywhere else, our path will be a linear combination of $\bold a, \bold b, \bold c​$ 







 
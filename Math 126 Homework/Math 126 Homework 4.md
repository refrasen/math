Renee Senining
Math 126
**Homework 4**

#### **Section 4.1**: 2, 4, 6

##### **Exercise 2**: 

**Question**: How does being insulated at the sides affect the equation?
Heat Equation: $u_t = ku_{xx}$ $(0<x<l, 0<t<\infin)$ 
Boundary Conditions: $u(0,t) = u(l,t) = 0$, for $t > 0$ 
Initial Conditions: $u(x,0) = 1$ 
Separating variables: $u = T(t)X(x)$, with $T(0)X(x) = 1$, $T(t)X(0) = 0$, $T(t)X(l) = 0$ 
$\frac{T'}{kT} = \frac{X''}{x} = -\lambda = \text{constant}$ (left side doesn't depend on $x$, right side doesnt depend on $t$) 

And from page 88, the eigenvalues of the problem 

$-X'' = \lambda X, X(0) = X(l) = 0$ 

must be positive
$-X'' = \lambda X$ in $0<x< l$ with $X(0) = X(l) = 0$ 
$\implies$ $X(x) = C\cos \sqrt\lambda x + D \sin\sqrt \lambda x$ 

and $X(0) = C = 0​$ and $X(l) = D \sin \sqrt \lambda l​$ $\implies​$ $\sqrt \lambda l = \pi n​$ 

so $X(x) = \sin \frac{\pi n x}{l}​$ and $T(t)= Ae^{-\lambda k t}​$, and $\lambda = (\pi n/l)^2​$  

$\implies$ $T(t) = Ae^{-(\pi n/l)^2kt}$ and $T(0) = A$ $ = 1$ 
so $u(x,t) = \sum_{n=1}^\infin A_n e^{-(n\pi/l)^2 kt} \sin \frac{n \pi x}{l}$ (equation 4.1.17)

and we have $1 = \sum_{n=1}^\infin A_n \sin \frac{n\pi x }{l}​$ $ = \frac{4}{\pi}(\sin \frac{\pi x}{l} + \frac{1}{3} \sin \frac{3\pi x}{l} + \frac{1}{5} \sin \frac{5\pi x}{l} + \cdots)​$ 

so $A_1 = \frac{4}{\pi}$, $A_2 = 0$, $A_3 = \frac{4}{\pi 3}$, $A_4 = 0$, $A_5 = \frac{4}{\pi5}$ 

so $A_n = \begin{cases} 0 & n\text{ is even} \\ \frac{4}{\pi}\frac{1}{n} & n\text{ is odd} \end{cases}$ 

so $u(x,t) = \frac{4}{\pi}\sum_{n=0}^{\infin} \frac{e^{-(\pi (2n+1)/l)^2kt}}{2n+1} \sin \frac{(2n+1) \pi x}{l}$ 

where for $n = 0, 1, 2, 3…$ $2n+1 = 1, 3, 5, 7…$ 

##### Exercise 4:

$$
u_{tt} = c^2u_{xx} - ru_t \quad \text{for }0<x<l
\\ u= 0 \quad \text{ at both ends}
\\ u(x,0) = \phi(x) \quad u_t(x,0) = \psi(x)
$$

$0 < r < 2\pi c/l$ and $r$ is a constant.
Using Separation of Variables: $u(x,t) = T(t)X(x)$ 
$T''(t)X(x) = c^2 T(t)X''(x) - rT'(t)X(x)$ 

$T''(t)X(x) + rT'(t)X(x) = c^2T(t)X''(x)​$, divide by $c^2T(t)X(x)​$ 
$\frac{T''(t) + rT'(t)}{c^2T(t)} = \frac{X''(x)}{X(x)} = -\lambda​$
We have again $-X''(x) = \lambda X​$, $X(0) = X(l) = 0​$ (to satisfy boundary conditions)
So we know that $X(x) = \sin \frac{n\pi x}{l}​$ (from 4.1)
$T'' + rT' +\lambda c^2 T = 0​$ , and since we know $\lambda = (\frac{n \pi}{l})^2​$
the characteristic polynomial: $s^2 + rs + (\frac{n\pi}{l})^2c^2 = 0​$ 
Quadratic equation: $ \frac{-r \pm \sqrt{r^2 - 4(\frac{n\pi}{l})^2c^2}}{2}​$ 

and $r^2 < 4\pi^2c^2/l^2$ $\leq 4 \pi^2n^2c^2/l^2$ 
so $s_1 = \frac{-r + i \sqrt{4(n^2\pi^2/l^2)c^2-r^2}}{2}$ , $s_2 = \frac{-r - i \sqrt{4(n^2\pi^2/l^2)c^2-r^2}}{2}$ 

so $T(t) = e^{-rt/2}[C_1\cos( \frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l}t)  + C_2 \sin (\frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l}t)​$ 
And since the equation above is linear, any linear combination of solutions is a solution… so
$u(x,t) = \sum_{n=1}^{\infin} e^{-rt/2}[A_n \cos(\frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l}t) + B_n \sin (\frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l}t)] \sin \frac{n\pi x}{l}​$ 

To get $A_n$ and $B_n$: We use $\phi(x) = \sum_{n=1}^\infin A_n \sin \frac{n\pi x}{l}$ 

$\sum_{n=1}^\infin A_n \sin \frac{n\pi x}{l}  = \phi(x)$, then multiplying by $\sin \frac{m\pi  x}{l}$ on both sides:

$\sum_{n=1}^\infin A_n \sin \frac{n\pi x}{l} \sin \frac{m\pi x}{l} = \phi(x) \sin \frac{m\pi x}{l}$ 

$\int_0^l\sum_{n=1}^\infin A_n \sin \frac{n\pi x}{l} \sin \frac{m\pi x}{l} dx =\int_0^l \phi(x) \sin \frac{m\pi x}{l}dx$ 

$\sum_{n=1}^\infin\int_0^l A_n \sin \frac{n\pi x}{l} \sin \frac{m\pi x}{l} dx= \int_0^l\phi(x) \sin \frac{m\pi x}{l}dx​$ 
Due to:  $\frac{\cos(A-B) - \cos(A + B)}{2} = \sin A \sin B​$ 

we have $\int_0^l \sin \frac{n\pi x}l \sin \frac{m \pi x}l dx$ $= \frac{1}{2}\int_0^l \cos(\frac{n\pi x - m\pi x}l) - \cos(\frac{n\pi x + m \pi x}{l}) dx$

so if $n = m​$: $\frac{1}{2}\int_0^l 1 -\cos(\frac{2 \pi m x}{l})dx​$ $= \frac{1}{2}[l - \frac{l}{2\pi m} \sin (2\pi m) - (0 - \frac{l}{2\pi m}\sin(0)] = \frac{l}{2} ​$ 

if $n \neq m$: $\frac{1}{2}[\frac{l}{n\pi - m\pi} \sin (\frac{n\pi -m\pi}{l}x) - \frac{l}{n\pi + m\pi}\sin(\frac{n\pi + m\pi}{l}x]_0^l$

since $n$ and $m$ are integers, we'll have only terms of the form $\sin(n\pi\pm m\pi)$ or $\sin(0)$, which both equal $0$, so the integral = 0 in this case. 
So we end up with:
$ A_n \frac{l}{2} = \int_0^l \phi(x) \sin \frac{n\pi x}{l}$  $\implies$ $A_n = \frac{2}{l} \int_0^l \phi(x) \sin \frac{n\pi x}{l} dx$ 
For $B_n$: 

$u_t(x,t) = \sum_{n=1}^\infin [-\frac{r}{2}e^{-rt/2}[A_n \cos(\frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l}t) + B_n \sin (\frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l}t)] \\+ e^{-rt/2}[-A_n\frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l} \sin(\frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l} t) + B_n \frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l}\cos (\frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l} t) ]]\sin \frac{n\pi x}{l}$ 

$u_t(x, 0) = \psi(x) = \sum_{n=1}^\infin [-\frac{r}{2}A_n + B_n \frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l}] \sin \frac{n\pi x}{l}​$

$  -\frac{r}{2} \phi(x) + \sum_{n=1}^\infin B_n \frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l} \sin \frac{n \pi x}{l} = \psi(x)$ 
$\sum_{n=1}^\infin B_n \frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l} \sin \frac{n\pi x}{l} = \frac{r}{2}\phi(x) + \psi(x)$, again we multiply both sides by $\sin \frac{m\pi x}{l}$ and integrate over $0 < x < l$ 

$\int_0^l \sum_{n=1}^\infin B_n \frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l} \sin \frac{n\pi x}{l} \sin \frac{m \pi x}{l} dx = \int_0^l [\frac{r}{2}\phi(x) + \psi(x)] \sin \frac{m \pi x}{l} dx$ and after bringing the integral inside the sum on the left and using trigonmetric identities, we see that the integral vanishes at all $n$ except for $m = n$ and we arrive at:
$B_n \frac{\sqrt{4n^2\pi^2c^2 - r^2l^2}}{2l} *\frac{l}{2} = \int_0^l [\frac{r}{2}\phi(x) + \psi(x)] \sin \frac{n \pi x}{l} dx$ 
so $B_n = \frac{4}{\sqrt{4n^2\pi^2c^2 - r^2l^2}} \int_0^l  [\frac{r}{2}\phi(x) + \psi(x)] \sin \frac{n \pi x}{l} dx$  

##### Exercise 6:

$$
tu_t = u_{xx} + 2u
\\ u(0,t) = u(\pi, t) = 0
$$

$u(x,t) = X(x)T(t)​$ $\implies​$
$tX(x)T'(t) = X''(x)T(t) + 2X(x)T(t)​$ divide by $X(x)T(t)​$:
$\frac{tT'(t)}{T(t)} = \frac{X''(x)}{X(x)} + 2 = -\lambda​$

Starting with: $X'' = (-\lambda - 2)X$, with $X(0) = 0 = X(\pi)$  
if $-\lambda = 2$: we'd obtain $X'' = 0$ $\implies$ $X(x) = C + Dx$ $\implies$ $C = 0$ and $D\pi = 0$ $\implies D = 0$, so the $X(x) \equiv 0$, so $\lambda - 2 = 0$ (not an eigenvalue)
if $-\lambda > 2$: we can write $-\lambda - 2 = \gamma^2$, so $X'' = \gamma^2X$, so $X(x) = C\cosh \gamma x + D \sinh \gamma x$. Then $X(0) = C = 0$ and $X(\pi) = D\sinh \gamma \pi$ but $\sinh \gamma \pi \neq 0$, so $D = 0$ and $X \equiv 0$, so $-\lambda-2 > 0$ is not an eigenvalue
if $-\lambda -2 \in \C$, then let $\gamma = \sqrt{\lambda-2}$ and let $-\gamma = -\sqrt{-\lambda - 2}$ 
$\implies$ $\gamma = a+ib$
$X(x) = Ce^{\gamma x} + De^{-\gamma x}$ 
$X(0) = C + D = 0$ and $X(\pi) = 0 = Ce^{\gamma\pi} + De^{-\gamma\pi}$ 
$\implies$ $Ce^{2\gamma\pi} + D = 0$ $\implies$ $e^{2\gamma \pi} = 1$ 
$\implies$ $e^{2a\pi} = 1 $ $\implies a = 0$ 
and $2b\pi = 2\pi n$ $\implies$ $\gamma = ib = i n$ so $-\lambda -2 = \gamma^2 = -n^2$ $\implies$ $\lambda < 2$ 

$\lambda + 2 = n^2$ so $X(x) = C\cos nx + D \sin nx$
$(X''(x) = -n^2(C\cos nx + D \sin nx) = -(\lambda + 2) X)$ 
$X(0) = C = 0$ and $X(\pi) = D\sin n\pi = 0$, for any $D$ 
and for $tT' = -\lambda T$ we obtain 
$\frac{dT}{dt}t = -\lambda T$
$\frac{1}{T} dT= -\lambda \frac{1}{t} dt$
$\ln(T) = -\lambda \ln(t) +C$ 
$T = e^{-\lambda \ln(t) + C} = e^{C}e^{-\lambda \ln(t)} = e^{C}t^{-\lambda}$ $ = At^{-\lambda}$ 
$tT' = t(A(-\lambda t^{-\lambda - 1})) = -\lambda At^{-\lambda} = -\lambda T$ 

and since $\lambda = n^2 - 2$, $T = At^{2-n^2}$, and using $X(x) = \sin nx$
By the principle of superposition:
$u(x,t) = \sum_{n=1}^{\infin} A_n t^{2 - n^2} \sin nx$ is the general form
With initial condition $0 = u(x,0) = \sum_{n=1}^\infin A_n(0) \sin nx$, where each $A_n$ can be any constant, so there are infinitely many solutions to the initial value problem.

#### Section 4.2: 1, 3

##### Exercise 1:

$$
u_t = ku_{xx} \quad 0<x<l
\\ u(0,t) = u_x(l,t) =0
$$

**Using Separation of Variables**: 
$u(x,t) = X(x)T(t)$, $\implies$ $X(x)T'(t) = kX''(x)T(t)$ 
dividing by $kX(x)T(t)$, we get
$\frac{T'}{kT} = \frac{X''}{X} = -\lambda$ 
First: $-X'' = \lambda X$ 
Can $\lambda = 0$: $\implies$ $X'' = 0$ so $X(x) = C + Dx$
and $X(0) = C = 0$ and $X'(l) = D= 0$ so $\lambda = 0$ is not an eigenvalue
$\lambda < 0$: $\lambda = -\gamma^2$ $\implies$ $X'' = \gamma^2 X$ so $X(x) = C \cosh \gamma x + D \sinh \gamma x$. Then $0 = X(0) = C$ and $X'(l) = D\cosh \gamma l = 0$ and $\cosh \gamma l \neq 0$ so $D = 0$, $\lambda < 0$ is not an eigenvalue
$\lambda \in \C$: let $\gamma = \sqrt{-\lambda}$. Then $X(x) = Ce^{\gamma x} + De^{-\gamma x}$ 
$X(0) = C + D = 0$ and $X'(l) = C\gamma e^{\gamma l} - \gamma D e^{-\gamma l} = 0$ so multiplying both sides by $\gamma^{-1}e^{\gamma l}$: $Ce^{2\gamma l} - D = 0$ $\equiv$ $-Ce^{2\gamma l} + D = 0$ 

$\implies$ $e^{2\gamma l} = -1$  so $e^{2al + 2ibl} = -1$ $\implies$ $e^{2al} = 0$ so $a = 0$ 
and $e^{2ibl} = \cos 2lb + i \sin 2lb$ $\implies$ $b = \frac{\pi}{2l}(2n+1)$, where $n$ is an integer
so $\gamma = i\frac{(2n+1)\pi}{2l}$ $\implies$ $\lambda = -\gamma^2 = \frac{(2n+1)^2\pi^2}{4l^2} = \frac{(n + \frac{1}{2})^2\pi^2}{l^2}$ 

are our eigenvalues, which are positive and real. 
Let $\gamma_n = \frac{(n+1/2)\pi}{l}$: $X_n(x) = C\cos(\gamma_n x) + D \sin (\gamma_n x)$ 

$X_n(0) = C = 0$, $X_n'(l) = \gamma_n D \cos( \gamma_n l) = 0$

and $\gamma_n l = n\pi + \frac{\pi}{2}$, so $\cos(\gamma_n l) = 0$ $\implies$ $D$ is an arbitrary constant
Our eigenfunctions are thus $\sin \frac{(n + 1/2)\pi x}{l}$ for $n = 0, 1, 2, …$ 

Then: $T' = -\lambda k T$ has the obvious solution $A_ne^{-\lambda k t}$ $= A_ne^{-(n+1/2)^2\pi^2 kt/l^2}$ 
$u(x,t) = \sum_{n=0}^\infin A_n \exp[\frac{-(n+1/2)^2\pi^2 kt}{l^2}] \sin \frac{(n+1/2)\pi x}{l}$ 

Where, if $u(x,0) = \phi(x)$, we have
$\sum_{n=0}^\infin A_n \sin \frac{(n+1/2)\pi x}{l} = \phi(x)$ 

##### Exercise 3:

$$
u_t = iku_{xx} \quad k \in \R, 0<x<l
\\ u_x(0,t) = u(l,t) = 0
$$

Using Exercise 4.2.1
$u(x,t) = X(x)T(t)$ 
$X(x)T'(t) = ik X''(x)T(t)$, divide by $ik X(x)T(t)$ 
$\frac{T'}{ikT} = \frac{X''}{X} = -\lambda$ 
solving $X'' = -\lambda X$ with $X'(0) = X(l) = 0$ 
$\lambda = 0$: $X'' = 0$ $\implies$ $X(x) = C + Dx$ $\implies$ $X'(0) = D = 0$ and $X(l) = C = 0$ so $X \equiv 0$, $\lambda = 0$ not an eigenvalue
$\lambda < 0$: $\lambda = -\gamma^2$ $X'' = \gamma^2X$ we get $C\cosh \gamma x+ D \sinh \gamma x$ 
$X'(x) = \gamma [C\sinh \gamma x + D\cosh \gamma x]$  

$X'(0) = \gamma D  = 0$ so $D = 0$ and $X(l) = C\cosh \gamma l = 0$ $\implies$ $C = 0$, so negative values aren't eigenvalues 
$\lambda \in \C$: let $\gamma = \sqrt{-\lambda}$, so $X(x) = Ce^{\gamma x} + De^{-\gamma x}$ 
$X'(x) = \gamma C e^{\gamma x} -\gamma D e^{-\gamma x} $ and $X'(0) = \gamma(C - D) = 0$ $\implies C = D$ 
$X(l) = Ce^{\gamma l} + De^{-\gamma l} = 0$ and since $C = D$,  $C[e^{\gamma l} +e^{-\gamma l} ] = 0$ 

But $e^{\gamma l} + e^{-\gamma l} \neq 0​$, so $C = 0 = D​$, so complex values aren't eigenvalues
so let $\lambda = \beta^2​$ 
$X'' = -\beta^2 X​$ so $X(x) = C \cos \beta x + D \sin \beta x​$ 
and $X'(0) = \beta D  = 0​$ so $D = 0​$, and $X(l) = C\cos \beta l = 0​$

so $\beta = \frac{\pi}{2l} (2n+1)$ or $\beta = \pi(n + 1/2)/l$ so $\lambda = \frac{\pi^2 (2n+1)^2}{4l^2}$ for $n = 0, 1, 2, …$ 

and $C$ is arbitrary
Solving $T' = -\lambda ikT $ we get $T(t) =A e^{-\lambda i k t}$  

so $u(x,t) = \sum_{n = 0}^\infin A_n \exp[-ik \frac{\pi^2(2n+1)^2}{4l^2}t ] \cos (\frac{\pi (2n+1)}{2l}x)$

and if we were given an initial condition $u(x,0) = \phi(x)$: 
$\sum_{n=0}^\infin A_n \cos( \frac{\pi (2n+1)}{2l}x) = \phi(x)$ 

#### Section 5.1: Exercises 2, 3, 4, 8, 10

##### Exercise 2:

**Fourier sine series** $\phi(x) \equiv x^2$, $0 \leq x \leq 1 = l$ 

$A_m = \frac{2}{l}\int_0^l x^2 \sin \frac{m \pi x}{l}dx
\\ = \frac{2}{l}[x^2 (-\frac{l}{m\pi} \cos \frac{m \pi x}{l})|_0^l + \int_0^l 2x (\frac{l}{m\pi} \cos \frac{m \pi x}{l})dx]
\\ = \frac{2}{l}[l^2 (-\frac{l}{m\pi}\cos m\pi) + \frac{2l}{m\pi}\int_0^l x \cos \frac{m \pi x}{l}dx]
\\ = \frac{2}{l}[\frac{l^3}{m\pi}(-1)^{m+1} + \frac{2l}{m\pi}(\frac{l}{m\pi}x \sin \frac{ m\pi x}{l} |_0^l - \int_0^l \frac{l}{m\pi}\sin \frac{m \pi x}{l}dx)]
\\ = \frac{2}{l}[\frac{l^3}{m\pi}(-1)^{m+1} + \frac{2l^2}{m^2\pi^2}(x\sin \frac{m \pi x}{l} + \frac{l}{m\pi}\cos \frac{m \pi x}{l})_0^l]
\\ = \frac{2}{l}[\frac{l^3}{m\pi}(-1)^{m+1} + \frac{2l^2}{m^2\pi^2}(\frac{l}{m\pi}(-1)^m - \frac{l}{m\pi})]$
$$
= \frac{2l^2}{m\pi}(-1)^{m+1} + \frac{4l^2}{m^3\pi^3}((-1)^m -1)
\\ = 2\frac{(-1)^{m+1}(\pi^2m^2) + 2(-1)^{m}-2}{m^3\pi^3}
\\ = 2\frac{(-1)^m(2-\pi^2m^2) -2 }{m^3\pi^3}
$$
so $x^2 = \sum_{m=1}^\infin 2\frac{(-1)^m(2-\pi^2m^2) -2 }{m^3\pi^3} \sin {m \pi x}​$  

**Fourier cosine series** $\phi(x) \equiv x^2, 0\leq x \leq 1 = l​$ 

so $x^2 = \frac{1}{2} A_0 + \sum_{n=1}^\infin A_n \cos n \pi x​$ 

$A_m = 2\int_0^1 x^2 \cos m\pi x \;dx$
Using Integration by parts: $\int uv' = uv - \int u'v$
let $u = x^2, v' = \cos m \pi x$, $u' = 2x, v = \frac{1}{m\pi}\sin m\pi x$ 
$2 [ (\frac{x^2}{m\pi}\sin m\pi x)_0^1 - \frac{1}{m\pi}\int_0^12x \sin m\pi x  \;dx]$ $ = 2[ - \frac{1}{m\pi} \int_0^1 2x \sin m\pi x \; dx]$ 
$ = - \frac{4}{m\pi}[-\frac{1}{m\pi}x\cos m\pi x|_0^1 + \frac{1}{m\pi}\int_0^1 \cos m\pi x \; dx]$ $= \frac{4}{m^2\pi^2} [ x \cos m \pi x|_0^1 - \int_0^1 \cos m \pi x \; dx]$ 
$= \frac{4}{m^2\pi^2}[(-1)^m - \frac{1}{m\pi}(\sin m \pi x)_0^1]$  $= (-1)^m\frac{4}{m^2\pi^2}$ 

when $m \neq 0$ for $m = 0$: $A_0 = 2 \int_0^1 x^2 dx$ $ = \frac{2}{3}$ 
$x^2 = \frac{1}{3} + \sum_{m=1}^\infin \frac{(-1)^m4}{m^2\pi^2} \cos m\pi x$ 

##### Exercise 3:

so from the examples in 5.1 we know the Fourier sine and cosine series for $x$ 

$x = \frac{2l}{\pi}(\sin \frac{\pi x}{l} - \frac{1}{2}\sin \frac{2\pi x}{l} + \frac{1}{3} \sin \frac{3 \pi x}{l} - \cdots)$

$x = \frac{l}{2} - \frac{4l}{\pi^2} (\cos \frac{\pi x}{l} + \frac{1}{9} \cos \frac{3\pi x}{l} + \frac{1}{25} \cos \frac{5 \pi x}{l} + \cdots)$ 

![image-20190221002203830](Math 126 Homework 4.assets/image-20190221002203830.png) 

Red-orange: cosine, dark blue: sine, light tan: $y = x$ 

Due to the $1/n^2$ terms in the cosine series, the cosine series seems to "converge" to $y = x$ faster than the sine series and $- \cos \pi x/l$'s shape already looks sort of like a straight line with a positive slope.

##### Exercise 4:

$|\sin x|$, $(-\pi, \pi)$, so let $\phi(x) = |\sin x|$, restricted to $(0, \pi)$ 

$l = \pi$, and so $A_m = \frac{2}{\pi} \int_0^\pi |\sin x|\cos mx \;dx $

and since $\sin x > 0$ for $x \in (0, \pi)$, we can remove the absolute value
$A_m = \frac{2}{\pi} \int_0^\pi \sin x \cos mx \; dx$ 
$\sin(x + mx) = \sin x \cos mx + \sin mx \cos x$
$\sin(x - mx) = \sin x \cos mx - \sin mx \cos x$  
so $\sin x \cos mx = \frac{1}{2} [\sin(x+mx) + \sin(x-mx)]$ 
$A_m = \frac{1}{\pi} \int_0^\pi \sin(x + mx) + \sin(x-mx) \; dx$ 
$ = \frac{1}{\pi}[ -\frac{1}{1+m} \cos(x+mx) -\frac{1}{1-m} \cos (x-mx)]_0^\pi$ 
$ = \frac{1}{\pi}[ -\frac{1}{1+m}(-1)^{m+1} - \frac{1}{1-m}(-1)^{1-m} + \frac{1}{1+m} + \frac{1}{1-m}]$

$ = \frac{1}{\pi}[ \frac{1+(-1)^m}{1+m} + \frac{1 + (-1)^m}{1-m}]$ $ = \frac{1}{\pi}[ \frac{(1+(-1)^m)(1-m) + (1+(-1)^m)(1+m)}{1-m^2}]$ 
if $m$ is positive and odd: $A_m = 0$ 
if $m$ is positive and even: $A_m = \frac{1}{\pi} \frac{2-2m + 2+ 2m}{1-m^2}$ $= \frac{1}{\pi} \frac{4}{1-m^2}$ 

if $m = 0​$ $A_0 = \frac{2}{\pi} \int_0^\pi \sin x \; dx​$ $ = \frac{2}{\pi}[-\cos(\pi)+\cos(0)]​$ $ = \frac{4}{\pi}​$ 
so $\frac{1}{2} A_0 = \frac{2}{\pi}​$  
$|\sin x|= \frac{2}{\pi} + \frac{4}{\pi} \sum_{m = 1}^{\infin} \frac{1}{1-(2m)^2} \cos (2m x)​$ 
when $x = 0​$: we obtain $0 = \frac{2}{\pi} + \frac{4}{\pi} \sum_{m=1}^\infin \frac{1}{1- 4m^2}​$ 
$\implies​$ $\frac{4}{\pi} \sum_{m=1}^\infin \frac{1}{4m^2 - 1} = \frac{2}{\pi}​$ $\implies​$ 
$\sum_{m=1}^\infin \frac{1}{1-4m^2} = \frac{1}{2}​$  

when $x = \frac{\pi}{2}$: we obtain $1 = \frac{2}{\pi} + \frac{4}{\pi} \sum_{m=1}^\infin \frac{1}{1-4m^2} (-1)^m$ 
$\implies$ $1 + \frac{4}{\pi} \sum_{m=1}^\infin \frac{(-1)^m}{4m^2-1} = \frac{2}{\pi}$ $\implies$

$\frac{4}{\pi} \sum_{n=1} \frac{(-1)^m}{4n^2 - 1} = 2/\pi - 1$

$\sum_{n} \frac{(-1)^m}{4m^2 - 1} = \frac{1}{2} - \frac{\pi}{4}$ 

 $\sum_{m=1}^\infin \frac{(-1)^m}{4m^2 - 1}  = \frac{1}{2} - \frac{\pi}{4}$ 

##### Exercise 8: 

$$
u_t = u_{xx} \qquad (0<x<1, 0<t<\infin)
\\\text{BC: } u(0, t) = 0 \quad \text{and} \quad u(1,t) = 1
\\ \text{IC: } u(x,0) = \begin{cases} \frac{5x}{2} & 0<x<\frac{2}3\\ 3-2x & \frac{2}3<x<1 \end{cases}
$$

**Equilibrium solution $\equiv$ steady-state**
we should have $\lim_{t \rightarrow \infin} u(x,t) = U(x)$ 
Using linearity: $u(x,t) = U(x) + v(x,t)$ 
but since $U(x)$ doesn't depend on time but still satisfies the PDE above: $v(x,t)$ satisfies
$$
v_t = v_{xx} 
\\ \text{BC: }v(0,t) = 0 \text{ and } v(1,t) = 1-1 = 0
\\ \text{IC: }v(x,0) = \phi(x) - U(x)
$$
Finding $U(x)$: 
$U''(x) = \frac{\partial U}{\partial t} = 0$ $\implies$ $U(x) = C + Dx$ 
$U(0) = C = 0$ and $U(1) = D = 1$ so $U(x) = x$ 
so $v(x,t) = \sum_{n = 1}^\infin A_n \exp(-(n\pi)^2 t) \sin n\pi x$ 
and $\phi(x)-x = \sum_{n=1}^\infin A_n \sin n\pi x$ 

where $A_n = 2 \int_0^1 [\phi(x) - x] \sin n\pi x \; dx$ 

so $A_n = 2[ \int_0^{2/3} \frac{3x}{2} \sin n\pi x \; dx + \int_{2/3}^1 (3-3x)\sin n \pi x \; dx]$ $=$  $3 \int_0^{2/3} x \sin n \pi x \; dx + 2 \int_{2/3}^1 (3-3x) \sin n\pi x \; dx$ 
Solving the left integral:

$3 [-\frac{1}{n\pi} x \cos n\pi x |_0^{2/3}+ \frac{1}{n\pi} \int_0^{2/3} \cos n\pi x\; dx]$ $ = \frac{3}{n\pi}[-x\cos n\pi x + \frac{1}{n\pi} \sin n \pi x ]_0^{2/3}$ $ = \frac{3}{n\pi}[-\frac{2}{3} \cos \frac{2n\pi}{3} + \frac{1}{n\pi}\sin \frac{2n\pi }{3} ]$ 

Solving the right integral:
$(2*3)\int_{2/3}^1\sin n\pi x \; dx -6 \int_{2/3}^1 x \sin n\pi x \; dx$ 
$-\frac{6}{n\pi}[\cos n\pi x]_{2/3}^1-\frac{6}{n\pi}[-x \cos n\pi x + \frac{1}{n\pi} \sin n \pi x]_{2/3}^{1}$ 

$ = -\frac{6}{n\pi} [ \cos n \pi - \cos \frac{2 n \pi }{3} - \cos n\pi + \frac{2}{3} \cos\frac{2n \pi }{3} - \frac{1}{n\pi} \sin \frac{2n\pi}{3}]$ 

Adding the two results:
$-\frac{2}{n\pi} \cos \frac{2n\pi}{3} + \frac{3}{n^2\pi^2} \sin \frac{2n\pi}3  + \frac{6}{n\pi} \cos \frac{2n\pi}3 -\frac{4}{n\pi}\cos \frac{2n\pi}{3} + \frac{6}{n^2\pi^2} \sin \frac{2n\pi}3$

$ = \frac{9}{n^2\pi^2} \sin \frac{2n \pi}3$

so $v(x,t) = \sum_{n = 1}^\infin \frac{9}{n^2\pi^2} \sin \frac{2n\pi}3 \exp(-n^2\pi^2 t) \sin n\pi x​$ 
Note:

if $n = 3k$, $\sin \frac{2n\pi}3 = 0$  
if $n = 3k+1$, $\sin \frac{2n\pi}{3} = \sin (2\pi k  + \frac{ 2\pi}{3}) = \sin \frac{2\pi}{3}$$ = \frac{\sqrt{3}}{2}$ 
if $n = 3k + 2$, $\sin \frac{2n\pi}3 = \sin (2 \pi k + \frac{4\pi}{3})$ $ = \sin \frac{4\pi}{3}$ $= -\frac{\sqrt{3}}{2}$ 

so:

$u(x,t) = x + \sum_{n = 1}^\infin \frac{9}{n^2\pi^2} \sin \frac{2n\pi}3 \exp(-n^2\pi^2 t) \sin n\pi x$ 

##### Exercise 10:

$$
u_{tt} = c^2u_{xx}
\\ u(0,t) = u(l,t) = 0 \text{ (due to fixed ends)}
\\ u(x,0) = 0 \qquad u_t(x,0) = \begin{cases} V & -\delta + l/2 \leq x \leq \delta + l/2 \\ 0 & x \not \in [-\delta+l/2, \delta + l/2] \end{cases}
$$

Using Formulas $4.1.9$, $4.1.10$, $4.1.11$: 

$u(x,t) = \sum_{n} (A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l}) \sin \frac{n \pi x }{l}$ 

$\phi(x) = \sum_{n} A_n \sin \frac{n \pi x}{l}$ $ = 0$ $\implies$ $A_n = 0$ (due to formula 5.1.4)  
$\psi(x) = \sum_{n} \frac{n \pi c}{l} B_n \sin \frac{n \pi x}{l}$ 

Then using formula 5.1.5:

$\frac{n\pi c}{l} B_n = \frac{2}{l} \int_0^l \psi(x) \sin \frac{n \pi x}{l} \; dx$ $ = \frac{2V}{l}\int _{l/2 - \delta}^{l/2+\delta} \sin \frac{n \pi x}{l} \; dx$ 
$= \frac{2V}{l} [ -\frac{l}{n\pi} \cos \frac{n \pi x}{l}]_{l/2-\delta}^{l/2+\delta}$ 
$\cos (\frac{n \pi l}{2l} + \frac{n\pi \delta}{l}) = \cos(\frac{n\pi}{2} + \frac{n\pi \delta}{l})$, $\cos (\frac{n\pi l}{2l} - \frac{n\pi \delta}{l}) = $$\cos(\frac{n\pi}{2} - \frac{n\pi \delta}{l})$ 

$\frac{n\pi c}{l}B_n = -\frac{2V}{n\pi}[\cos(\frac{n\pi}{2} + \frac{n\pi \delta}{l}) - \cos (\frac{n \pi}{2} - \frac{n \pi \delta}{l})]$  
$B_n = \frac{2Vl}{n^2\pi^2 c} [\cos(\frac{n\pi}{2} - \frac{n\pi \delta}{l}) - \cos (\frac{n\pi}{2} + \frac{n\pi\delta}{l})]$

since $2\sin a \sin b = \cos(a-b) - \cos(a+b)$ 
$B_n = \frac{4Vl}{n^2\pi^2c} \sin \frac{n\pi}{2} \sin\frac{n\pi \delta}{l}$ 
$u(x,t) = \sum_{n=1}^\infin \frac{4Vl}{n^2\pi^2c}\sin \frac{n\pi}2\sin \frac{n\pi \delta}{l} \sin \frac{n \pi c t}{l}\sin \frac{n \pi x}{l} $

$ = \frac{4Vl}{\pi^2c} \sum_{n=1}^\infin \frac{1}{n^2} \sin \frac{n\pi}2 \sin \frac{n \pi \delta}l \sin \frac{n\pi c t}{l} \sin \frac{n \pi x}l$ 

Note: for $n$ even, due to the $\sin \frac{n \pi }{2}$ term, the summan is $0$ 
so we end up with $\frac{4Vl}{\pi^2 c} \sum_{n=0}^\infin \frac{1}{(2n+1)^2} \sin \frac{(2n+1)\pi}{2} \sin \frac{(2n+1)\pi\delta}{l} \sin \frac{(2n + 1) \pi c t}{l} \sin \frac{(2n+1) \pi x}{l}$ 
and $\sin \frac{(2n+1)\pi}{2} = (-1)^{n}$ and $c = \sqrt{T/\rho}$ 
so $u(x,t) = \frac{4Vl}{\pi^2 \sqrt{T/\rho}} \sum_{n=0}^\infin \frac{(-1)^{n}}{(2n+1)^2}  \sin \frac{(2n+1)\pi\delta}{l} \sin \frac{(2n + 1) \pi (\sqrt{T/\rho}) t}{l} \sin \frac{(2n+1) \pi x}{l}$ 

The $n$th harmonic $h = h_n$ is the summand $\frac{4Vl}{cn^2\pi^2} \sin \frac{n\pi}{2} \sin \frac{n\pi\delta}{l} \sin \frac{cn \pi t}{l} \sin \frac{n \pi x}{l}$

To find Energy, we substitute the summand in for $h$: 

$E_n (h) = \frac{1}{2} \int_0^l [\rho (\frac{\partial h}{\partial t})^2 + T (\frac{\partial h}{\partial x})^2]dx\\ = \frac{1}{2}(\frac{4Vl}{cn^2\pi^2} \sin \frac{n\pi}{2} \sin \frac{n \pi \delta}l)^2  \int_0^l \rho (\frac{cn\pi}{l} \cos \frac{cn\pi t}l \sin \frac{n \pi x}{l})^2 + T( \frac{n \pi }{l} \sin \frac{cn \pi t}{l} \cos \frac{n \pi x}{l})^2 dx $

$ = \frac{1}{2} \frac{16V^2 l^2}{c^2n^4\pi^4}\frac{n^2 \pi^2}{l^2} \sin^2 \frac{n \pi}{2} \sin^2 \frac{n \pi \delta}{l}  \int_0^l \rho c^2 \cos^2 \frac{c n \pi t}{l} \sin^2 \frac{n \pi x}{l} + T \sin^2 \frac{cn\pi t}{l} \cos^2 \frac{n \pi x}{l} dx$ 
$ = \frac{8V^2}{c^2 n^2\pi^2} \sin^2 \frac{n \pi}2 \sin ^2\frac{n \pi \delta}{l} \int_0^l \rho c^2 \cos^2 \frac{cn\pi t}{l} \sin^2 \frac{ n \pi x}{l} + T \sin^2 \frac{cn\pi t}{l} \cos^2 \frac{n \pi x}{l} dx$ 

now replacing $c^2$ with $T/\rho$ 

$ = \frac{8V^2\rho}{T n^2 \pi^2} \sin^2 \frac{n \pi}2 \sin^2 \frac{n \pi \delta}{l} \int_0^l T \cos^2 \frac{c n \pi t}{l} \sin^2 \frac{n \pi x}{l} + T \sin^2 \frac{cn\pi t}{l} \cos^2 \frac{n \pi x}{l} dx$ 
$ = \frac{8V^2 \rho}{n^2\pi^2}\sin^2 \frac{n\pi}{2} \sin^2 \frac{n \pi\delta}{l} [ \cos^2 \frac{cn\pi t}{l} \int_0^l \sin^2 \frac{n \pi x}{l} dx + \sin^2\frac{cn \pi t}{l} \int_0^l \cos^2 \frac{n \pi x}{l} dx]$

And we know that the integral of $\cos^2 (n\pi x/l)$ (and $\sin^2 (n\pi x/l)$) over $-l$ to $l$ is $l$ and since these are both even functions this means that the integral of either one of them from $0$ to $l$ is $l/2$ 
So $E_n(h) = \frac{8 V^2 \rho}{n^2 \pi^2} \frac{l}{2} \sin^2 \frac{n \pi}{2} \sin^2 \frac{ n \pi \delta}{l}\underbrace{[\cos^2 \frac{cn\pi t}{l} + \sin^2 \frac{cn \pi t}{l}]}_{= 1}$ 
$$
E_n(h) = \begin{cases} 0 & n \text{ is even} \\ \frac{4V^2\rho l}{n^2\pi^2} \sin^2 \frac{n \pi \delta}{l}  & n \text{ is odd}\end{cases}
$$
and since $\sin x = x - \frac{x^3}{3!} + \frac{x^5}{5!} - \frac{x^7}{7!} + \cdots$ 

$\sin \frac{n \pi \delta}{l} = \frac{n \pi \delta}{l} - \frac{(n\pi \delta/l)^3}{3!} + \cdots$ 
for very small $\delta$, the terms $(n \pi \delta/l)^{2k+1}/(2k+1)!$ for $k > 0$ disappear

and we have $\sin \frac{n \pi \delta}{l} \approx \frac{n \pi \delta}{l}$ for small $\delta$ so $\frac{4V^2 \rho l}{n^2\pi^2} \sin^2 \frac{n \pi \delta}{l} \approx \frac{4V^2 \rho l}{n^2 \pi^2} \frac{n^2 \pi^2 \delta^2}{l^2}$ 
$$
E_n(h) = \begin{cases} 0 & n \text{ is even} \\ \frac{4V^2\rho \delta^2 }{l} & n \text{ is odd}\end{cases}
$$
The back of the book says for $n$ even, but for $n$ even, $\sin^2 \frac{n \pi}{2} = 0$… so I think this is a typo. 
so when $\delta$ is small, the first few overtones $(n = 3, n = 5)$ have the same amount of energy as the fundamental $(n = 1)$ 

#### Section 5.2: Exercises 4, 5, 11, 12

##### Exercise 4:

(5): $\int_{-l}^{l} (\text{odd}) dx = 0$ and $\int_{-l}^l (\text{even}) dx = 2\int_0^l (\text{even}) dx$ and suppose $\phi(x)$ is odd
we know that the **product of an even and odd function is odd**: page 114

$A_n = \frac{1}{l} \int_{-l}^l \phi(x) \cos \frac{n \pi x}{l} dx$
$B_n = \frac{1}l \int_{-l}^l \phi(x) \sin \frac{n \pi x}{l} dx$ 
$A_n$ are the coefficients for the cosine terms and $B_n$ are the coefficients for the sine terms. since $\phi$ is odd and cosine is even, their product is an odd function:
so $A_n = \frac{1}{l} \int_{-l}^{l} \phi(x) \cos \frac{n \pi x}{l} \; dx$ $= \frac{1}{l} \int_{-l}^l (\text{odd}) \; dx = 0$ 

Similalry, the product of two odd functions is an even function:
$B_n = \frac{2}{l}\int_0^l \phi(x) \sin \frac{n \pi x}{l} dx​$ (not necessarily 0)

Therefore, the full fourier series for an odd function only contains sine terms (on $(-l, l)$)

Now suppose $\phi(x)$ is even. Since sine is an odd function $\phi(x) \sin \frac{n \pi x}{l}$ is an odd function.
$B_n = \frac{1}{l} \int_{-l}^l \phi(x) \sin \frac{n \pi x}{l} \; dx \frac{1}{l} \int_{-l} ^l(\text{odd})\;dx= 0$

The product of two even functions is an even function:
$A_n = \frac{2}{l} \int_0^l \phi(x) \cos \frac{n \pi x}{l} dx$ (not necessarily 0)

Therefore, the full Fourier series for an even function on $(-l, l)$ only contains cosine terms

##### Exercise 5:

Let $\phi(x)$ be any continuous function on $(0,l)$ and let $\tilde \phi (x) = \begin{cases} \phi(x) & 0 < x < l \\ -\phi(-x) & -l<x<0 \\ 0 & x =0 \end{cases}$ 
The full Fourier series on $(-l,l)$ for $\tilde \phi(x) $is defined as
$\tilde \phi (x) = \frac{1}{2} A_0 + \sum_{n=1}^\infin (A_n \cos \frac{n \pi x}{l} + B_n \sin \frac{n \pi x}{l})$ (We are allowed to assume the sum of the series is $\tilde \phi(x)$ 
From Exercise 4 and because $\tilde \phi(x)$ is an **odd** extension (so an odd function), This series has only sine terms
$\tilde \phi (x) = \sum_{n=1}^\infin B_n \sin \frac{n \pi x}{l}$ with $B_n = \frac{1}{l}\int_{-l}^{l} \tilde \phi(x) \sin \frac{n \pi x}{l} dx = \frac{1}{l} \int_{-l}^0 -\phi(-x)\sin \frac{n \pi x}{l} dx + \frac{1}{l} \int_0^l \phi(x) \sin \frac{n \pi x}{l}dx$

and changing variables from $-x \rightarrow x$ in the first integral:
$B_n = \frac{1}{l} [ \int_l^0 \phi(x) (- \sin \frac{n \pi x}{l} )dx  + \int_0^l \phi(x) \sin \frac{n \pi x}{l} dx$ $ = \frac{2}{l} \int_0^l \phi(x) \sin \frac{n \pi x}{l} \; dx$ 
Which is exactly the formula for the coefficients of the Fourier sine series

so by restricting the domain to $0 <x < l$, $\tilde \phi(x) = \sum_{n=1}^\infin B_n \sin \frac{n \pi x}{l} $ is equivalent to the Fourier sine series of $\phi(x)$ 

##### Exercise 11: 

**Complex Fourier Series**: 

$e^x = \sum_{n= -\infin}^\infin c_n e^{in\pi x/l}$ 
$c_n = \frac{1}{2l} \int_{-l}^{l} e^xe^{-in\pi x/l}dx$ $ = \frac{1}{2l} \int_{-l}^l \exp[ x(1 - \frac{in \pi}{l})] dx$ $ = \frac{1}{2l} [\frac{1}{1 - \frac{in \pi}{l}} \exp[x(1-\frac{in\pi}{l})]]_{-l}^l$ 

$ = \frac{1}{2l - 2in\pi} (e^{l - in\pi} - e^{in\pi -l})$ = $\frac{i}{l-in\pi} \frac{e^{i(-il-n\pi)} - e^{-i(-il-n\pi)}}{2i}$

$ = \frac{i}{l-in\pi} \sin (-(il+n\pi))$ $ = -\frac{i}{l-in\pi} \sin(il+ n\pi)$ $ = -\frac{i}{l-in\pi} [\sin il \cos n\pi + \sin n\pi \cos il]$ 
$ = -\frac{i}{l-in\pi} [(-1)^n \sin il]$ 
$\sinh x = \frac{e^x - e^{-x}}{2}$  and $\sin x = \frac{e^{ix} - e^{-ix}}{2i}$ $\implies$ $\sin ix = \frac{e^{-x} - e^{x}}{2i}$ $ = -\frac{1}{i} \sinh x$ 
and $-\frac{1}{i} = i$ so $\sin il = i \sinh l$ 
$c_n = \frac{i}{l-in\pi} \frac{l + in\pi}{l+in\pi}(-1)(-1)^n i \sinh l$ $ = \frac{l + in \pi}{l^2 + n^2 \pi^2} (-1)^n\sinh l$

So $e^x = \sum_{n=-\infin}^\infin (-1)^n \frac{l + in \pi}{l^2 + n^2 \pi^2} \sinh l$ $e^{in\pi x/l}$ 

**Real Fourier Series**: 

$e^{x} = \sum_{n=-\infin}^{-1} (-1)^n \frac{l + in\pi}{l^2 + n^2 \pi^2} \sinh l e^{in\pi x/l} + \sum_{n=0}^\infin (-1)^n \frac{l +in\pi}{l^2 + n^2\pi^2} \sinh le^{in\pi x/l}$ 
in the first sum, we change $n$ to $-n$ and using $(-1)^{-n} = \frac{1}{(-1)^n} = (-1)^n​$

$= \sum_{n = 1}^{\infin} (-1)^{n} \frac{l - in\pi}{l^2 + n^2 \pi^2} \sinh l e^{-in\pi x/l}+ \sum_{n=0}^\infin (-1)^n \frac{l +in\pi}{l^2 + n^2\pi^2} \sinh le^{in\pi x/l}​$ 

$= \frac{\sinh l}{l} + \sinh l \sum_{n=1}^\infin (-1)^n\frac{1}{l^2+n^2\pi^2}[(l-in\pi) e^{-in\pi x/l}  + (l+in\pi) e^{i n \pi x/l}]$

then: $(l - in\pi)(\cos \frac{n\pi x}{l} - i\sin \frac{n \pi x}{l}) + (l+in\pi)(\cos \frac{n \pi x}{l} + i\sin \frac{n \pi x }{l})$ 
$ = 2l \cos \frac{n \pi x}{l} -2n\pi \sin \frac{n \pi x}{l}$ 

so $e^x = \frac{\sinh l}{l} + 2\sinh l \sum_{n=1}^\infin \frac{(-1)^n}{l^2 + n^2 \pi^2} [l\cos \frac{n\pi x}{l} -n\pi \sin \frac{n \pi x}{l}]$ 

##### Exercise 12:

$\cosh x= \frac{e^x + e^{-x}}{2} = \sum_{n=-\infin}^\infin c_n e^{in\pi x/l}$

**I want to assume we can use the formula(s) for exercise 11** and just plug in $-x$ when necessary

**Complex Fourier Series**: 

$e^{-x} = \sum_{n=-\infin}^\infin (-1)^n \frac{l + in\pi}{l^2 + n^2\pi^2} \sinh l e^{-in\pi x/l}$ 

switching from $+n$ to $-n$ and using $(-1)^{-n} = \frac{1}{(-1)^n} = (-1)^n$: $e^{-x} = \sum_{n = -\infin}^{\infin} (-1)^n \frac{l - in\pi}{l^2 + n^2\pi^2} \sinh l e^{in\pi x/l}$ 

$\frac{e^{x} + e^{-x}}{2} = \frac{1}{2}\sum_{n=-\infin}^\infin (-1)^n \frac{l + in \pi + l -in\pi}{l^2 + n^2\pi^2}\sinh l e^{in\pi x/l} ​$ 

$ \cosh = \sum_{n=-\infin}^\infin (-1)^n \frac{l}{l^2 + n^2\pi^2} \sinh (l) e^{in\pi x/l}$ 

**Real Fourier Series**: 

$e^{-x} = \frac{\sinh l}{l} + 2\sinh l \sum_{n=1}^\infin \frac{(-1)^n}{l^2 + n^2 \pi^2}[l\cos \frac{n\pi x}{l} + n\pi \sin \frac{n \pi x}{l}]​$

so: $\frac{e^x + e^{-x}}{2} = \frac{\sinh l + \sinh l}{2l} + \frac{2}{2}\sinh l \sum_{n=1}^\infin \frac{(-1)^n}{l^2 + n^2 \pi^2} [l \cos \frac{n \pi x}{l} - n\pi \sin \frac{n \pi x}{l} + l \cos \frac{n \pi x}{l} + n\pi \sin \frac{n \pi x}{l}]​$

$ = \frac{\sinh l}{l} + \sinh l \sum_{n=1}^\infin \frac{(-1)^n}{l^2 + n^2 \pi^2}[2l \cos \frac{n \pi x}{l}]$ $ = \frac{\sinh l}{l} + 2l \sinh l \sum_{n=1}^\infin \frac{(-1)^n}{l^2 + n^2 \pi^2}  \cos \frac{n \pi x}{l}$  
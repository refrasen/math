$F(x, u, p) =p^2(1+p)^2  =  p^2(1 + 2p + p^2) = p^2 + 2p^3 + p^4$ 
$F_u = 0$ 
$F_p = 2p + 6p^2 + 4p^3$ , then with $E-L$; 
$0 = \frac{d}{dx}[2u'(x) + 6u'(x)^2 + 4u'(x)^3]$ 
$0 = 2u''(x) + 6[2u'(x)u''(x)] + 4[3u'(x)^2u''(x)]$  
$0 = -[2u''(x) + 12u'(x)u''(x) + 12u'(x)^2u''(x)]$ 
$2u''(x)(-1 - 6u'(x) - 6u'(x)^2) = 0$ 

so first: $u''(x) = 0$ $\implies$ $u'(x) = a$ $\implies$ $u(x) = ax + b$ 

or solving: $-6u'^2 - 6u' - 1 = 0​$ 

$u' =  \frac{6 \pm \sqrt{36 - 4(-6)(-1)}}{-12}$ $ = -\frac{1}{2} \pm \frac{1}{\sqrt{12}}$ let $m_1 = - \frac{1}{2} + \frac{1}{\sqrt{12}}$ and $m_2 = -\frac{1}{2} - \frac{1}{\sqrt{12}}$ 
which gives another two solutions: $u = m_1 x + c_1$, $u = m_2 x + c_2$ 

Note: The Integrand must be $\geq 0$, and is equal to 0 when $h'(x) = 0$ or $h'(x) = -1$, 
this rules out the lines with $m_1, m_2$ as slopes as minimizing solutions. 

Due to the constraints: we can't have a constant function, nor is there a line with $u(x)= -x + b$ such that $u(0) = 1 \implies b = 1$ and $u(2) = 0 \implies b = 2$ 

so we construct a continuous piecewise function of lines with slopes $0$ and $-1$: 

If we used only $2$ lines to contstruct the PW function, then we'd be limited to:$u(x) = \begin{cases} 1 - x & 0 \leq x \leq 1 \\ 0 & 1 < x \leq 2 \end{cases}$ and $u(x) = \begin{cases} 1 & 0 \leq x \leq 1 \\ 2-x & 1 < x \leq 2\end{cases}$ 

However, if we used three lines we can descend from $1$, stay on some constant, then descend to $0$, each solution is determined by the constant and the intervals for the lines. 

since we know $u(0) = 1$ we have for our first line: $u_1(x) = -x +1$, for some interval $(0, s)$ and similarly, for the last line, $u_3(x) = -x + 2$ for some interval $(t, 2)$ 

so the constant in between, due to continuity, must be $1 - s = -t + 2$ 
so we can solve for $t$ in terms of $s$: $t = 1 +s$. So indeed, we have infinite solutions: 
$$
u_s(x) = \begin{cases} 1-x & 0 \leq x \leq s \\ 1-s & s < x \leq 1+s \\ 2-x & s < x \leq 2\end{cases}
$$
for $s \in [0, 1]$ (at the endpoints we only use 2 lines)




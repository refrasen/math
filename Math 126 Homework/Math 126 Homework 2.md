Renee Senining

Math 126

# Homework 2

### Section 1.3: Exercises 6, 9, 10

##### 6. Consider heat flow in a long circular cylinder where the temperature depends only on $t$ and on the distance $r$ to the axis of the cylinder. Here $r = \sqrt{x^2 + y^2}$ is the cylindrical coordinate. From the three-dimensional heat equation derive the equation $u_t = k(u_{rr}+u_r/r)$ 

$$u(t,r)$$ is temperature at time $t$, $r$ distances to the axis of the cylinder

I believe taking the gradient of $u$ only refers to the spatial variables in this case

$c \rho u_t = \nabla \cdot (\kappa \nabla u) = \nabla \cdot \kappa(u_x, u_y, u_z)$

$ = \kappa(u_{xx} + u_{yy} + u_{zz})$  

$= \kappa(\frac{\partial^2 u}{\partial x^2} + \frac{\partial ^2 u}{\partial y^2} + \frac{\partial ^2 u}{\partial z^2})$ 

From given:

$u_{zz} = 0$ (Since we have $u$ doesn't depend on $z$)

$u(t, r) = u(t, \sqrt{x^2 + y^2})$, and then using chain rule:

$u_{x} =\frac{\partial u}{\partial t}\frac{\partial t}{\partial x}+ \frac{\partial u}{\partial r}\frac{\partial r}{\partial x} = u_t\cdot0 + u_r\cdot(\frac{x}{\sqrt{x^2+y^2}})  = u_r\cdot \frac{x}{\sqrt{x^2 + y^2}}$

$u_y = u_r\cdot\frac{y}{\sqrt{x^2 +y^2}}$ 

$u_{xx} = u_{rr}\cdot \frac{x^2}{x^2+y^2} + u_r\cdot (\frac{\sqrt{x^2 + y^2} - x^2/\sqrt{x^2 + y^2}}{x^2+y^2})$

$ = u_{rr}\cdot \frac{x^2 }{x^2 +y^2} + u_r \cdot \frac{ y^2}{(x^2+y^2)^{3/2}} $

$u_{yy} = u_{rr}\cdot \frac{y^2}{x^2 + y^2} + u_r \cdot \frac{x^2}{(x^2 + y^2)^{3/2}}$ 

$u_{xx} +u_{yy} = u_{rr}\frac{x^2 + y^2}{x^2 + y^2} + u_r\frac{x^2 + y^2}{(x^2+y^2)\sqrt{x^2+y^2}}$ $= u_{rr} + u_r/r$ 

$c\rho u_t = \kappa(u_{rr} +u_r/r)$ 

let $k = \kappa/c\rho$ and we obtain $u_t = k(u_{rr} +u_r/r)$ 

##### 9. Verify the divergence theorem in the following case by calculating both sides separately: $\bold F = r^2 \bold x, \bold x = x\bold i + y \bold j + z \bold k, r^2 = x^2 + y^2 + z^2$, and $D = $ the ball of radius $a$ and center at the origin. 

Left Side: 

$\int \int \int _D \nabla \cdot \bold F \; d\bold x = \int\int\int_D \nabla \cdot (r^2 \bold x)$ and
$$
\nabla \cdot (r^2\bold x) = \nabla \cdot (x^3 +xy^2 + xz^2, x^2y + y^3 + yz^2, x^2z + y^2z + z^3) 
\\ = (3x^2 +y^2+z^2)+ (x^2 + 3y^2 + z^2) + (x^2 + y^2 + 3z^2)
\\ = 5(x^2+y^2 + z^2)
\\ = 5r^2
$$
And converting to spherical coordinates 
$$
\int\int\int_D \nabla \cdot \bold F \; d\bold x = \int_0^\pi \int_0^{2\pi}\int_0^a 5r^2 (r^2 \sin \phi) \; dr\, d\theta\, d\phi
\\ =\int_0^\pi \int_0^{2\pi} a^5 \sin \phi \; d\theta \, d\phi
\\ =\int_0^\pi 2\pi a^5 \sin \phi
\\ = [-2\pi a^5 \cos \phi]_0^\pi
\\ = -(-2\pi a^5) - (-2\pi a^5) = 4\pi a^5
$$
Right Side: $\bold n = \bold x/a$ 

$\bold F \cdot \bold n = $ $r^2 \bold x \cdot \bold x/a$ $= \bold x \cdot \bold x \cdot \bold x \cdot \bold x /a = r^4/a$  
$$
\int\int_{\partial D} \bold F \cdot \bold n\; dS = \int\int_{\partial D} \frac{1}{a}r^4 \; dS
$$
and on the boundary, $r= a$, so:

$\int\int_{\partial D}a^3 dS$ 

and surface area of a ball $= 4\pi a^2$ 

so we end up with $\int\int_{\partial D} \bold F \cdot \bold n = a^3[4\pi a^2] = 4\pi a^5$ , which matches the left side.

##### 10. If $\bold f(\bold x)$ is continuous and $|\bold f(\bold x)| \leq 1/(|\bold x|^3 + 1)$ for all $\bold x$, show that $\int\int\int_{\text{all space}} \nabla \cdot \bold f \; d\bold x = 0$ 

$D$: large ball with radius $a$, 
$$
\int\int\int_D \nabla \cdot \bold f \; d\bold x = \int\int_{\partial D}\bold f\cdot \bold n \; dS
$$
and $\bold n = \frac{\bold x}{a}$ 

$|\bold f(\bold x) \cdot \bold n| \leq \frac{|\bold x|}{a(|\bold x|^3 + 1)}$, and on the boundary, $ = \frac{1}{a^3 + 1}$ 

Better: Using Cauchy-Schwarz: $|<u, v>|\leq |u||v|​$ 

so using the triangle inequality for integrals:  $|\int \int _{\partial D} \bold f\cdot \bold n \; dS| = \leq \int \int _{\partial D}|\bold f \cdot \bold n| \; |dS| = \int \int _{\partial D}\frac{1}{a^3+1}\; |dS|$ $ = \frac{4\pi a^2}{a^3 + 1}$ 

and as $a \rightarrow \infin​$, $\lim_{a \rightarrow \infin} \frac{4\pi a^2}{a^3 + 1} =4\pi \lim_{a\rightarrow \infin} \frac{1}{a + 1/a^2} = 0​$

which means $| \int \int \int_{\text{all space}} \nabla \cdot \bold f \; d\bold x | \leq 0$ which $\implies$ $\int\int\int_{\text{all space}} \nabla \cdot \bold f \; d\bold x= 0 $ 

------

### Section 1.4: Exercises 3, 4

------

##### 3. A homogeneous body occupying the solid region $D$ is completely insulated. Its initial temperature is $f(\bold x)$. Find the steady-state temperature that it reaches after a long time. (*Hint*: No heat is gained or lost)

Insulated $\implies$ $\frac{\partial u}{\partial n} = 0$ (Neumann condition)

$u(\bold x, 0) = f(\bold x)$ (initial condition)

Heat equation:

$c\rho u_t = \nabla\cdot (\kappa \nabla u) $ 

$\frac{dH}{dt} = \int \int_{\partial D} \kappa(\bold n \cdot \nabla u) \; dS = \int \int _{\partial D} \kappa (\frac{\partial u}{\partial n}) \; dS  = \int \int_{\partial D} 0 \; dS = 0$

$\implies$ $\int \int \int_D c\rho u_t \; dx \, dy\, dz = 0$ 

$ = \frac{\partial}{\partial t}\int \int \int_D c \rho u \; dx \, dy\, dz = 0 = \frac{\partial H}{\partial t}$

so Heat, $H(t)$ remains constant for all time

so $\int \int \int_D c\ \rho f(\bold x) \; dx \, dy \, dz = \int \int \int _D c\rho u(\bold x, t)\; dx \, dy\, dz$ for all $t > 0$

Let $T$ be the steady-state temperature, a constant. ($T = \lim_{t \rightarrow \infin} u(\bold x, t)$) 
$$
\int \int \int_D f(\bold x) \; dx\, dy\, dz = \int \int \int_D T \; dx \, dy\, dz = T \int\int\int_D dx\, dy\,dz = T V
$$
where $V$ is the volume of the solid

$T = \frac{\int\int\int_D f(\bold x)}{V}$ = the average of the initial temeprature $f(\bold x)$ over the volume of the solid. 

##### 4. A rod occupying the interval $0 \leq x \leq l$ is subject to the heat source $f(x) = 0$ for $0 < x <\frac{l}{2}$  and $f(x) = H$ for $\frac{l}{2} < x < l$ where $H > 0$. The rod has physical constant $c = \rho = \kappa = 1$, and its ends are kept at zero temperature

(a) Find the steady state temperature of the rod

**Question**: What does a heat source mean?

As $t \rightarrow \infin$, the rod reaches its "steady state" temperature, a constant temperature, and $u$ no longer depends on $t$ implying $u_t = 0$ and the heat equation becomes:
$$
0 = u_{xx} + f(x) \qquad \text{where }f(x) \text{ is a source of heat}
\\ u(0, t) = 0 \qquad u(l, t) = 0
$$
and more specifically:
$$
0 = u_{xx} \qquad 0 < x < \frac{l}2
\\ 0 = u_{xx} + H \qquad \frac{l}2 < x < l
$$
Integrating, we get:

$u_x = A$ $\implies$ $u(x) = Ax + B$ for $0 < x < \frac{l}{2}$ 

$u_x = -Hx + C$ $\implies$ $u(x) = -\frac{Hx^2}{2} + Cx + D$ for $\frac{l}{2} < x < l$

Temperature must be a smooth function (since we need second order partial derivative $u_{xx}$) along the rod so:

$u(0) = \lim_{x \rightarrow 0^+} Ax + B = B = 0$, 

$u(l) = \lim_{x \rightarrow l^-} -\frac{Hx^2}2 + Cx + D$ $= -\frac{Hl^2}{2} + Cl + D = 0$ $\implies$ $D = \frac{Hl^2}{2}- Cl$ 

from the boundary conditions. And from the smoothness of the function:

$\lim_{x \rightarrow \frac{l}{2}^-} u(x) = \lim_{x \rightarrow \frac{l}{2}^+} u(x)$ $\implies$ $\frac{Al}{2} = -\frac{Hl^2}{8} + \frac{Cl}{2} + D = \frac{3Hl^2}{8} - \frac{Cl}{2}$

 $\lim_{x \rightarrow \frac{l}{2}^-} u_x(x) = \lim_{x \rightarrow \frac{l}{2}^+} u_x(x)$ $\implies$ $A = -\frac{Hl}{2} +C$ 
$$
 -\frac{Hl}{2}+C= \frac{3Hl}{4} - C
\\ 2C = \frac{3Hl}{4}+ \frac{Hl}{2} = \frac{5Hl}{4}
\\ C = \frac{5Hl}{8}
\\ \implies A = \frac{Hl}{8}
\\ \implies D = \frac{Hl^2}{2}- \frac{5Hl^2}{8} = -\frac{Hl^2}{8}
$$
So the steady-state temperature is given by:
$$
u(x) = \begin{cases} 
\frac{Hl}{8}x & 0 < x < \frac{l}{2} \\ 
-\frac{Hx^2}{2} + \frac{5Hl}{8}x - \frac{Hl^2}{8} & \frac{l}{2}< x <l
\end{cases}
$$
(b) Which point is the hottest, and what is the temperature there?

at the ends, we know $u(x) = 0$, so we look for critical points

for $0 < x <\frac{l}{2}$, $u_x = Hl/8 \neq 0$ 

$u_x(x) = 0$ when $x > \frac{l}{2}$ so: $u_x(x) = -Hx + 5Hl/8 = 0$

$x = 5l/8$, when x$ < 5l/8$, $u_x(x)  > 0$ and when $x > 5l/8$, $u_x(x) < 0$ 

so as we move from $x = 0$ to $x = 5l/8$ temp is increasing, then once we move away from $5l/8$ towards $x = l$, temp is decreasing, so $x = 5l/8$ is the hottest point. 

$u(5l/8) = -\frac{25 H l^2}{128} + \frac{25 H l ^2}{64} - \frac{Hl^2}{8}$ $= Hl^2 \frac{-25+50 - 16}{128}$ $= Hl^2 \frac{9}{128}$ is the temperature. 

------

### Section 1.5: Exercises 1, 4

##### 1. Consider the problem 

$$
\frac{d^2u}{dx^2} + u =0
\\ u(0) = 0 \qquad \text{and} \qquad u(L) = 0
$$

**consisting of an ODE and a pair of boundary conditions. Clearly, the function $u(x) \equiv 0$ is a solution. Is this solution *unique, or not*? Does the answer depend on $L$?** 

$u'' + u =0$ , second order linear homogeneous with constant coefficients:

$Ae^{r_1} +Be^{r_2}$ , $r = a\pm ib$, $Ae^a[c_1\cos(b) + c_1i\sin(b) + c_2 \cos(b) ]$ 

Roots: $r^2 + 1 = 0$, $r = 0\pm i$, so $u =e^0 [A\sin(x) + B\cos(x)] = A\sin(x)+B\cos(x)$ 

$u(0) = B = 0$ $\implies$ $u(x) = A\sin(x)$

$u(L) = A\sin(L) = 0$ 

Whether the solution is unique or not does depend on $L$: 

if $L = \pi k$, where $k$ is an integer, then $\sin(L) = 0$, and for any $A \in \R$, $u(x) = A\sin(x)$ is a solution to the ODE. 

if $L \neq \pi k$, for any integer $k$, then $\sin(L) \neq 0$, so $A$ must be $0$, and we get the trivial solution, making it unique. 

##### 4. Consider the Neumann problem

$$
\triangle u = f(x,y,z) \quad \text{in D}\\
\frac{\partial u}{\partial n} = 0 \quad \text{on }\partial D
$$

(a) What can we surely add to any solution to get another solution? So we don't have uniqueness. 

A solution to $\triangle u = 0$, since $\mathscr L u = \frac{\partial ^2 u}{\partial x^2} + \frac{\partial ^2 u}{\partial y^2} + \frac{\partial^2 u}{\partial z^2} $  is linear, so

if $v$ is a solution to $\triangle u = f$ and $g$ is a solution to $\triangle u = 0$ we obtain:
$$
\triangle (v + g) = (v+g)_{xx} + (v+g)_{yy} +(v+g)_{zz}
\\ = (v_{xx} +v_{yy} + v_{zz}) + (g_{xx} + g_{yy} + g_{zz})
\\ = f + 0 = f
$$
i.e., a constant, $g = c$ $\in \R$ 

(b) Use the divergence theorem and the PDE to show that
$$
\int\int \int _D f(x,y,z) \; dx\, dy\, dz = 0
$$
is a necessary condition for the Neumann problem to have a solution

So taking the triple integral on both sides of $\triangle u = f$ and using $\nabla \cdot \nabla u= \triangle u$ 
$$
\int\int\int_D \nabla\cdot\nabla u \; dx \, dy \, dz = \int\int\int_D f(x,y,z) \; dx\, dy\, dz \\
\\ \text{divergence theorem} \\
\int\int _{\partial D} \nabla u \cdot \bold n \; dS = \int\int\int_D f(x, y, z)\; dx\,dy\,dz \\
\text{Neumann condition}
\\
\int\int_{\partial D} \frac{\partial u}{\partial n} dS = \int\int_{\partial D} 0 dS = 0=\int\int\int _D f(x, y, z) \; dx \, dy\, dz
$$
$\implies$ $\int\int\int_D f(x,y,z) = 0$ in order for a solution to satisfy the neumann condition

(c) Can you give a physical interpretatio nof part (a) and/or (b) for either heat flow/diffusion?

Heat flow and diffusion are pretty similar, so in this case, explaining one sort of explains the other

$f(x,y,z)$ = heat source/mass source

(a):  ~~Adding a constant $\implies$ $D$ can be any temperature/ have any concentration~~

Temperature is relative… increasing the temperature throughout the universe does not instigate heat flow

(b): The neumann condition $\implies$ no heat can enter/leave $D$/ no mass can enter/leave

so we can't have an "infinite" heat $H$, or mass $M$ emanating from within $D$, else we have temperature/concentration rising/falling indefinitely

$\implies$ if heat/mass is generated in one point of $D$, heat/mass must be lost in another place in $D$ for Heat/Mass to remain finite. 

so the average value of $f(x,y,z)$ must come out to $0$ else if it is positive/negatve we will have infinite heat/mass in the positive/negative direction. 

------

## Section 2.1: Exercises 2, 5, 6, 8

##### 2. Solve $u_{tt} = c^2u_{xx}, u(x, 0) = \log(1 + x^2), u_t(x,0) = 4+x$ 

$\phi(x) = \log(1 + x^2)$, $\psi(x) = 4+x$ and using $(8)$ in the textbook, 
$$
u(x, t) = \frac{1}{2}[\log(1 + (x+ct)^2) + \log(1 + (x-ct)^2)] + \frac{1}{2c}\int_{x-ct}^{x+ct} 4 + s \; ds
\\ =\log([1 + (x+ct)]^{1/2}) + \log([1+(x-ct)^2]^{1/2}) + \frac{8(x+ct)- 8(x-ct) + (x+ct)^2 - (x-ct)^2}{4c}
\\ = \log([1+(x+ct)^{1/2}][1+(x-ct)]^{1/2}) + 4t  +\frac{x^2 + 2xct + c^2t^2 - x^2 + 2xct - c^2t^2}{4c}
\\ = \log([1+(x+ct)^{1/2}][1+(x-ct)]^{1/2}) + 4t + xt
$$

**Real Answer**: $u(x,t) = \log\sqrt{[1 + (x+ct)^2][1+(x-ct)^2]} + t(x+4)$

##### 5. Let $\phi(x) \equiv 0$ and $\psi(x) = 1$ for $|x| < a$ and $\psi(x) = 0$ for $|x| \geq a$. Sketch the string profile ($u$ versus $x$) at each of the successive instants $t = a/2c, a/c, 3a/2c, 2a/c, \text{ and } 5a/c$ 

$u(x, t) = \frac{1}{2}[0+0] + \frac{1}{2c}\int_{x-ct}^{x+ct} \psi(s) \; ds $ 

- $t = \frac{a}{2c}$, $x + ct = x + a/2$, and $x-ct = x - a/2$ 

$|x + a/2| \geq a$ and $|x-a/2| \geq a$ when $x \leq -\frac{3a}{2}$ 

so $u(x, a/2c)   = 0$ 

$|x + a/2| < a$ and $|x - a/2| \geq a$ when $-\frac{3a}{2}< x \leq -\frac{a}{2}$ 

$-a < x +a/2\leq 0$ and 

$-2a < x-a/2 \leq -a$  
$$
u(x, a/2c) = \frac{1}{2c}\int_{x-a/2}^{x+a/2} \psi(s) \; ds = \int_{x-a/2}^{-a} 0 \; ds + \int_{-a}^{x+a/2}1 \; ds 
\\ = \frac{1}{2c}[x+a/2 +a] \\ = \frac{1}{2c}[x + \frac{3a}{2}]
$$
$|x+a/2| < a \text{ and }|x-a/2| < a \text{ when } -\frac{a}{2} < x < \frac{a}{2}$ 
$$
\\ \forall x \in (-a/2, a/2), \phi(x-a/2) \text{ and } \phi(x+a/2) = 1
$$

$$
u(x, a/2c) = \frac{1}{2c}[x+a/2 - (x-a/2)] = \frac{1}{2c}(2a/2) = \frac{a}{2c}
$$

$|x+a/2| \geq a$ and $|x - a/2| < a$ when $\frac{a}{2} \leq x < \frac{3a}{2}$ 

$0 \leq x - a/2 < a$ and $a \leq x +a/2< 2a$, so:
$$
u(x, a/2c) = \frac{1}{2c}\int_{x-a/2}^{x+a/2}\psi(s)\; ds = \frac{1}{2c}[\int_{x-a/2}^a 1\; ds + \int_a^{x+a/2}0\; ds ]
\\ = \frac{a - (x-a/2)}{2c}
\\ = \frac{3a}{4c}- \frac{x}{2c}
$$
$|x + a/2| \geq a$ and $|x-a/2| \geq a$ when $x \geq \frac{3a}{2}$ 

so $u(x, a/2c) = 0$ 

- $t = \frac{a}{c}$, $x \pm ct = x \pm a$  

$|x\pm a| \geq a$ when $x \leq -2a$  

$x+a \leq -a$ and $x-a \leq -3a$ 

so $u(x, a/c)  =0 $ 

$|x + a| < a$ and $|x - a| \geq a$ when $-2a < x <0 $ 

$-a < x+a <a $ and $-3a < x < -2a$ 
$$
u(x, a/c) = \frac{1}{2c}\int_{x-a}^{x+a} \psi(s) \; ds = \frac{1}{2c}[\int_{x-a}^{-a} 0 \; ds + \int_{-a}^{x+a} 1\; ds]
 \\ = \frac{x+a - (-a) }{2c}
 \\ = \frac{x+2a}{2c} 
 \\ = \frac{x}{2c} + \frac{a}{c}
$$
$|x+a| = a$ And $|x-a| = a$ when $x = 0$ 

so $u(0, a/c) = 0$ 

$|x+a| > a$ and $|x-a| < a$ when $0 <x < 2a$

$a < x +a<3a$ and $-a < x-a < a$ 
$$
u(x, a/c) = \frac{1}{2c}\int_{x-a}^{x+a}\psi(s)\; ds= \frac{1}{2c}[\int_{x-a}^{a} 1\;ds +\int_{a}^{x+a} 0\; ds] \\ = \frac{a - (x-a)}{2c}
\\ = \frac{2a - x}{2c}
\\ = \frac{a}{c} - \frac{x}{2c}
$$
$|x\pm a| \geq a$ when $2a \leq x$ 

so $u(x, a/c) = 0$  

- $t = 3a/2c$, $x \pm ct = x \pm 3a/2$ 

$-a <  x + \frac{3a}{2} < a$ $\equiv$ $-\frac{5a}{2} < x < -\frac{a}{2}$ 

$-a < x - \frac{3a}{2} < a$ $\equiv$ $\frac{a}{2} < x < \frac{5a}{2}$

$|x\pm \frac{3a}2|\geq a $ when  $ x \leq -\frac{5a}{2}$ 

$u(x, 3a/2c) = 0$ 

$|x+ \frac{3a}{2}| < a$, $|x - \frac{3a}{2}| > a$  when $-\frac{5a}{2} < x < -\frac{a}{2}$
$$
u(x, 3a/2c) = \frac{1}{2c}\int_{x-3a/2}^{x+3a/2} \psi(s)\; ds = \frac{1}{2c}[\int_{x-3a/2}^{-a}0\; ds + \int_{-a}^{x +3a/2}1\; ds]
\\ = \frac{x + \frac{3a}{2}+a}{2c}
\\ = \frac{x}{2c} + \frac{5a}{4c}
$$
$|x + \frac{3a}{2}| \geq a, |x- \frac{3a}{2}| \geq a$ when $-\frac{a}{2} \leq x \leq \frac{a}{2}$ 

$a \leq x + \frac{3a}{2} \leq 2a$ and $-2a \leq x - \frac{3a}{2} \leq -a$ 
$$
u(x, 3a/2c) = \frac{1}{2c}[\int_{x-\frac{3a}{2}}^{-a} 0\; ds + \int_{-a}^{a}1\;ds + \int_{a}^{x+\frac{3a}{2}}0\; ds]
\\ = \frac{2a}{2c}
\\ = \frac{a}{c}
$$
$|x+ \frac{3a}{2}| > a$, $|x - \frac{3a}{2}| < a$ when $ \frac{a}{2}<x<\frac{5a}{2}$ 

$2a < x + \frac{3a}{2} < 4a$ and $-a < x + \frac{3a}{2} < a$ 
$$
u(x, 3a/2c) = \frac{1}{2c}\int_{x-3a/2}^{x+3a/2}\psi(s)\; ds = \frac{1}{2c}[\int_{x-3a/2}^{a}1\;ds + \int_{a}^{x+3a/2}0\; ds]
\\ = \frac{5a}{4c}-\frac{x}{2c}
$$
$|x \pm \frac{3a}{2}| \geq a$ when $x \geq \frac{5a}{2}$ 

$u(x, 3a/2c) = 0$ 

- $t = 2a/c$ , $x \pm ct = x \pm 2a$ 

$-a < x + 2a < a$ $\equiv$ $-3a < x < -a$ 

$-a < x - 2a < a$ $\equiv$ $a < x < 3a$ 

$|x \pm 2a| \geq a$ when $x \leq -3a$ 

$u(x, 2a/c) = 0$ 

$|x + 2a| < a$ and $|x - 2a| > a$ when $-3a < x < -a$ 
$$
u(x, 2a/c) = \frac{1}{2c}\int_{x-2a}^{-a} 0\; ds + \frac{1}{2c}\int_{-a}^{x+2a}1\; ds
\\ = \frac{x+2a -(-a) }{2c}
\\ = \frac{x + 3a}{2c}
$$
$|x \pm2a| \geq a$ when $-a \leq x \leq a$ 

$a \leq  x+2a \leq 3a$, $-3a \leq x -2a\leq -a$ 
$$
u(x, 2a/c) = \frac{1}{2c}[\int_{x-2a}^{-a} 0\; ds + \int_{-a}^{a}1\; ds + \int_{a}^{x+2a/c}0\; ds]
\\ = \frac{2a}{2c}
\\ = \frac{a}{c}
$$
$|x + 2a| > a$ and $|x-2a| < a$ when $a < x < 3a$ 
$$
u(x, 2a/c) = \frac{1}{2c}[\int_{x-2a}^{a} 1\; ds + \int_{a}^{x+2a}0\; ds]
\\ = \frac{a-(x-2a)}{2c}
\\ = \frac{3a-x}{2c}
$$
$|x\pm 2a| \geq a$ when $x \geq 3a$

so $u(x, 2a/c) = 0$ 

- $t = 5a/c$, $x \pm ct = x \pm 5a$ 

$-a < x + 5a < a$ $\equiv$ $-6a < x < -4a$ 

$-a < x - 5a < a$ $\equiv$ $4a < x < 6a$ 

for $x \leq -6a$ and $x \geq 6a$

$u(x, 5a/c) = 0$ 

$|x + 5a| < a$ and $|x - 5a| > a$ when $-6a < x < -4a$ 

$-a < x+5a < a$ and $-11a < x < -9a$ 
$$
u(x, 5a/c) = \frac{1}{2c}[\int_{x-5a}^{-a}0\; ds + \int_{-a}^{x+5a}1\;ds]
\\ = \frac{x+5a - (-a)}{2c}
\\ = \frac{x+6a}{2c}
$$
$|x\pm 5a|\geq a$ when $-4a \leq x \leq 4a$ 
$$
u(x, 5a/c) = \frac{1}{2c}[\int_{x-5a}^{-a} 0 \; ds + \int_{-a}^{a} 1\; ds + \int_{a}^{x+5a}0\;ds]
\\ = \frac{a}{c}
$$
$|x + 5a| > a$ and $|x - 5a| < a$ when $4a < x <6a$ 
$$
u(x, 5a/c) = \frac{1}{2c}[\int_{x-5a}^{a}1\; ds + \int_{a}^{x+5a}0\; ds]
\\ = \frac{a - (x-5a)}{2c}
\\ = \frac{6a - x}{2c}
$$
![image-20190206211816022](../Math%20126%20Homework/Math%20126%20Homework%202.assets/image-20190206211816022.png)

##### 6. In Exercise 5, find the greatest displacement, $max_x u(x, t)$, as a function of $t$ 

$u(x, t) = \int_{x-ct}^{x+ct} \psi(s)\; ds$ 

remebering that $\psi(x) = \begin{cases} 1 & |x| <a \\ 0 & |x| \geq a \end{cases}$

$x = 0$ always satisfies $|x| < a$ and from exercise 5, the max of $u(x,t)$ always occurs at $x = 0$, so:

$u(0, t) = \frac{1}{2c} \int_{-ct}^{ct}\psi(s)\; ds$ $ =\frac{1}{2c}[ \int_{-ct}^{-a} 0 \; ds + \int_{-a}^a 1 \; ds   + \int_{a}^{ct} 0\; ds]$ $ = \frac{a}{c}$ 

so this will happen whenever $-ct \leq -a$ and $ct \geq a$, so $t \geq \frac{a}{c}$ 

if we have $-ct > -a$ and therefore $t < \frac{a}{c}$, we have

$u(0, t) = \frac{1}{2c} \int_{-ct}^{ct} \psi(s) \; ds = $ $\frac{ct - (-ct)}{2c}$ $= \frac{2ct}{2c}$ $= t$ 

if $t = 0$: $u(x, 0) = \int_x^x \psi(s)\; ds = 0$

if $t < 0$: $\int_{-ct}^{ct} \psi(s)\; ds = -\int_{-c|t|}^{c|t|}\psi(s)\; ds \leq 0$ so

$\max_x u(x, t)=\begin{cases} 0 & t \leq 0 \\ t & 0 <t<\frac{a}{c} \\ \frac{a}{c} & t \geq \frac{a}{c}\end{cases} $ 

##### 8. A *spherical wave* is a solution of the three-dimensional wave equation of the form $u(r, t)$ where $r$ is the distance to the origin (the spherical coordinate). The wave equation takes the form

$$
u_{tt} = c^2(u_{rr}+ \frac{2}r u_r)\qquad (\text{ "spherical wave equation"})
$$

##### (a) Change variables $v = ru$ to get the equation for $v: v_{tt} = c^2 v_{rr}$ 

$v = ru$, $v_{tt} = ru_{tt}$ , $v_{r} = u + ru_r$, $v_{rr} = 2u_r + ru_{rr}$ 

$v_{tt} = ru_{tt} = c^2 (ru_{rr} +2 u_r) = c^2v_{rr}$  

so we now have $v_{tt} = c^2v_{rr}$ 

##### (b) Solve for $v$ using $(3)$ and thereby solve the spherical wave equation. 

From $(3)$ we obtain: $v(r, t) = f(r+ct) + g(r-ct)$  

and $v(r,t) = ru$, so $u(r, t) = \frac{f(r+ct)+g(r-ct)}{r}$ 

##### (c) Use $(8)$ to solve it with initial conditions $u(r, 0) = \phi(r),$ $u_t(r, 0) = \psi(r)$, taking both $\phi(r)$ and $\psi(r)$ to be even functions of $r$. 

so $\phi(r) = \frac{f(r) + g(r)}{r}$ and $\psi(r) = \frac{cf'(r)-cg'(r)}{r}$ and $\frac{1}{c}\psi(r) = \frac{f'(r)-g'(r)}{r}$ 

$[r\phi(r)]' = f'(r)+ g'(r)$ and $\frac{r}{c}\psi(r) = f'(r) - g'(r)$ 

adding: $[r\phi(r)]' + \frac{r}{c}\psi(r) = 2f'(r)$ 

subtracting: $[r\phi(r)]' - \frac{r}{c}\psi(r) = 2g'(r)$ 

$f'(r) = \frac{1}{2}([r\phi(r)]' + \frac{r}{c}\psi(r))$ , $g'(r) = \frac{1}{2}([r\phi(r)]' - \frac{r}{c}\psi(r))$ 

$f(r) = \frac{1}{2}[r\phi(r)] + \frac{1}{2c}\int_0^r s\psi(s) \; ds$ 

$g(r) = \frac{1}{2}[r\phi(r)] + \frac{1}{2c} \int_{r}^0 s\psi(s)\; ds$ 

$u(r, t) = \frac{1}{2r}[(r+ct)\phi(r+ct)+(r-ct)\phi(r-ct)] +\frac{1}{2rc}[\int_{r-ct}^{r+ct}s\psi(s)\; ds] $ 

— alternatively: 

## Section 2.2: Exercises 1, 2, 3

##### 1. Use the energy conservation of the wave equation to prove that the only solution with $\phi \equiv 0$ and $\psi \equiv 0$ is $u \equiv 0$ 

$E = \frac{1}{2}\int_{-\infin}^{+\infin}(\rho u_t^2 + Tu_x^2)\; dx$ 

is a constant independent of $t$. 

We have $u_t(x, 0) = 0$ and $u(x, 0) = 0$ 

so $KE = \frac{1}{2}\rho \int u_t^2 dx$ at $t = 0$ gives $KE = 0$ initially. 

and $PE = \frac{1}{2}T \int u_x^2 dx$ at $t = 0$ gives $PE = 0$ initially, since $u(x,0) = 0 \implies u_x(x, 0) = 0$ 

so $E = KE + PE = 0$ and since $E$ is constant, $E \equiv 0$ 

so we have $\frac{1}{2}\int_{-\infin}^\infin (\rho u_t^2 + Tu_x^2)dx = 0$ and by the first vanishing theorem, we have

$\rho u_t^2 + Tu_x^2 = 0$ and $\rho, T > 0$ ($\rho$ is a mass constant, and I believe we must have positive mass, and $T$ is the magnitude of the tension vector, and since $c = \sqrt{T/ \rho}$, in this case, $T$ must also be positive if $\rho$ is positive)

so $u_t^2 = u_x^2 = 0$ $\implies$ $u_t = 0$, $u_x = 0$ 

and $u_t = 0 \implies u = f(x)$ and $u_x = f'(x) = 0 \implies$ $u$ is constant

and from $u(x, 0) = 0$, $u = 0$ 

##### 2.For a solution $u(x,t)$ of the wave equation with $\rho = T = c = 1$, the energy density is defined as $e = \frac{1}{2}(u_t^2 + u_x^2)$ and the momentum density as $p = u_tu_x$ 

##### (a) Show that $\partial e/\partial t = \partial p/\partial x$ and $\partial p/\partial t= \partial e/\partial x$ 

$e = \frac{1}{2}(u_tu_t + u_xu_x)$ 

$\frac{\partial e}{\partial t} = \frac{1}{2}(2u_tu_{tt} + 2u_{x}u_{xt})$ $= u_tu_{tt} + u_x u_{xt}$ 

$\frac{\partial p}{\partial x} $ $= u_{tx}u_{x} + u_{t}u_{xx}$ $= u_{t}u_{xx} + u_xu_{xt}$ (since $u_{tx} = u_{xt}$)

and $u(x,t)$ is a solution of the wave equation $u_{tt} = u_{xx}$ $(c^2 = 1)$, so we may replace $u_{tt}$ with $u_{xx}$ in the expression for $\frac{\partial e}{\partial t}$ to show $\frac{\partial e}{\partial t}= \frac{\partial p}{\partial x}$ 

$\frac{\partial e}{\partial x} = \frac{1}{2}(2u_tu_{tx} + 2u_xu_{xx})$ $= u_tu_{tx} + u_xu_{xx}$ 

$\frac{\partial p}{\partial t} = u_{tt}u_x + u_tu_{xt}$ $ = u_tu_{tx} + u_xu_{tt}$ (since $u_{tx} = u_{xt}$)

and like in the first part, we can replace $u_{tt}$ with $u_{xx}$ in the second equation....

##### (b) Show that both $e(x,t)$ and $p(x,t)$ also satisfy the wave equation

Using $(a)$: 

$p_{tt} = (p_t)_t = (e_x)_t$ and $p_{xx} = (p_x)_x = (e_t)_x$ 

and $e_{xt} = e_{tx}$ so $p_{tt} = p_{xx}$, satisfying the wave equation

and similalry:

$e_{tt}  = (e_t)_t = (p_x)_t$ and $e_{xx} = (e_x)_x = (p_t)_x $ 

and $p_{xt} = p_{tx}$ $\implies$ $e_{tt} = e_{xx}$, satisfying the wave equation

##### 3. Show that the wave equation has the following invariance properties. 

##### (a) Any translate $u(x-y, t)$ where $y$ is fixed, is also a solution

Let $w = x-y$ and use chain rule: 
$$
u_{x} = \frac{\partial u}{\partial w}\frac{\partial w}{\partial x} + \frac{\partial u}{\partial t} \frac{\partial t}{\partial x}= u_w+0 = u_w \\ u_{xx} = \frac{\partial u_w}{\partial w}\frac{\partial w}{\partial x} + \frac{\partial u_w}{\partial t}\frac{\partial t}{\partial x} = u_{ww} \\
u_t = \frac{\partial u}{\partial w}\frac{\partial w}{\partial t} + \frac{\partial u}{\partial t}\frac{\partial t}{\partial t} = 0 + u_t = u_t \\
u_{tt} = \frac{\partial u_t}{\partial w}\frac{\partial w}{\partial t} + \frac{\partial u_t}{\partial t}\frac{\partial t}{\partial t} = 0 + u_{tt} = u_{tt}
$$
so substituting $u_{ww}$ for $u_{xx}$ in the wave equation gives:
$$
u_{tt}(w,t) = c^2u_{xx}(w, t) = c^2u_{ww}(w,t)
$$

##### (b) Any derivative, say $u_{x}$, of a solution is also a solution

so want to show $(u_{x})_{tt} = c^2(u_{x})_{xx}$ 

$(u_x)_{tt} = u_{xtt} = u_{ttx} = (u_{tt})_x  = (c^2u_{xx})_x = c^2 u_{xxx}$ 

$(u_x)_{xx} = u_{xxx}$ 

so $(u_x)_{tt} = c^2(u_x)_{xx}$ as desired. 

Similalrly: $(u_t)_{tt} = c^2(u_t)_{xx}$ 

$(u_t)_{tt} = u_{ttt}$ 

$(u_t)_{xx} = u_{txx} = u_{xxt} = (u_{xx})_t = (\frac{1}{c^2}u_{tt})_t = \frac{1}{c^2}u_{ttt} $

so $(u_t)_{tt} = c^2(u_t)_{xx}$ 

so for any solution, $u$ of the wave equation, the derivatives $u_x$ and $u_t$ are solutions

and since $u_x$ and $u_t$ are solutions, $u_{xx}, u_{xt}, u_{tx}, u_{tt}$ are solutions, etc.

##### (c) The dilated funciton $u(ax, at)$ is also a solution, for any constant $a$ 

Let $w = ax, z = at$ 
$$
u_{x}(w, z) = \frac{\partial u}{\partial w}\frac{\partial w}{\partial x} + \frac{\partial u}{\partial z}\frac{\partial z}{\partial x} = au_{w}(w, z) \\ 
u_{xx}(w, z) = \frac{\partial u_x}{\partial w}\frac{\partial w}{\partial x}+ \frac{\partial u_x}{\partial z}\frac{\partial z}{\partial x} = a^2u_{ww}(w, z)
\\ u_{t}(w, z) = \frac{\partial u}{\partial w}\frac{\partial w}{\partial t}+ \frac{\partial u}{\partial z}\frac{\partial z}{\partial t} = au_z(w, z)
\\ u_{tt}(w, z) \frac{\partial u_t}{\partial w}\frac{\partial w}{\partial t}+ \frac{\partial u_t}{\partial z}\frac{\partial z}{\partial t} = a^2u_{zz}(w, z)
$$
Take the wave equation and plug in $(w,z)$ for $(x,y)$:  $u_{tt}(w, z) = c^2u_{xx}(w, z)$ 

Then substitute in the above and we obtain: $a^2u_{zz}(w, z) = c^2a^2 u_{ww}(w, z)$ 

$\implies$ $u_{zz}(w, z) = c^2 u_{ww}(w, z)$, the dilated function also satisfies the wave equation
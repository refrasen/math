Renee Senining
Math 126
Homework 7

#### Section 7.1: Exercise 5

Prove Dirichlet's principle for the Neumann boundary condition. It asserts that among all real-valued functions $w(\bold x)$ on $D$ the quantity
$$
E[w] = \frac{1}{2}\iiint_D |\nabla w|^2 d\bold x - \iint_{\partial D} hw\, dS
$$
is the smallest for $w = u$, where $u$ is the solution of the Neumann problem
$$
- \Delta u = 0 \quad \text{in }D, \qquad \frac{\partial u}{\partial n} = h(\bold x) \quad \text{ on }\partial D
$$
It is required to assume that the average of the given function $h(\bold x)$ is zero (By Exercise 6.1.11)

Notice Three Features of this principle:

1. There is *no constraint at all* on the trial functions $w(\bold x)$ 
2. The function $h(\bold x)$ appears in the energy
3. The functional $E[w]$ does not change if a constant is added to $w(\bold x)$ 

Hint: Follow the method in Section $7.1$. 

---

Proving $\implies$: Assume $u$ is the unique (up to a constant) harmonic function that satisfies $\frac{\partial u}{\partial n} = h(\bold x)$ and $w$ is some function on $D$ 
First: Let $v = u - w$, then, plug this into the formula for $E[w]$ 
$E[w] = \frac{1}{2}\iiint_D |\nabla(u-v)|^2 d\bold x - \iint_{\partial D} h(u - v) \, dS$ $ = E[u] -  \iiint_D \nabla u \cdot \nabla v \, d\bold x + \iiint_D |\nabla v|^2 d\bold x + \iint_{\partial D} hv \, dS $
**Apply Green's First Identity** to $u$ and $v$  and using $\partial u/\partial n = h(\bold x)$ and that $\Delta u = 0$ 

$\iint_{\partial D} v \frac{\partial u}{\partial n} \, dS= \iint_{\partial D} hv \, dS = \iiint_{D} \nabla u \cdot \nabla v \, d\bold x + \iiint_{D} v\Delta u d \bold x = \iiint_D \nabla u\cdot \nabla v d \bold x​$  

$E[w] = E[u] - \iint_{\partial D} hv \,  dS  + \iiint_D |\nabla v|^2 d\bold x + \iint_{\partial D} hv\, dS = E[u] + \iiint_D |\nabla v|^2 d\bold x$ 
so since $\iiint_D |\nabla v|^2 d\bold x \geq 0$ with equality only when $\nabla v = 0$ $\implies$ $v$ is a constant, we have that $E[w] \geq E[u]$ 

Proving $\impliedby$: Let $u(\bold x)$ be a function that minimizes $E[w]$ and $v$ be any real-valued function
$E[u] \leq E[u + \epsilon v] = E[u] + E[\epsilon v] + \epsilon \iiint_D \nabla u \cdot \nabla v \, d\bold x$ 
$ = E[u] + E[\epsilon v] + \epsilon (\iint_{\partial D} v \frac{\partial u}{\partial n} - \iiint_D v \Delta u \,  d\bold x)$ 
Let $e(s) = E[u+s v]$ , which has a minimum when $s = 0$ (by assumption that $E[u]$ minimizes $E[w]$) 

$0 = e'(0) = \frac{d}{ds}_{| _{s = 0}} \iiint_{D} \frac{1}{2} |\nabla(u + sv)|^2 d\bold x - \iint_{\partial D} h(u + sv)\,  dS$ 

$ = \iiint_D \nabla u \cdot \nabla v \, d\bold x - \iint_{\partial D} hv dS$, and then using $(G1)$ 
$= \iint_{\partial D} hv \, dS + \iiint_D v \Delta u \, d\bold x - \iint_{\partial D} hv \, dS = \iiint_D v \Delta u d\bold x$, 
so $\iiint_D v \Delta u \, d\bold x =0$, and since $v$ was an arbitrary real-valued function, this is valid for any function $v$ in $D$. And this implies $\Delta u = 0$ (from Lecture 18: otherwise, if $\nabla u > 0$ at some point, choose $v$ concentrated at that point $\implies$ $\iiint_D \Delta u v \, d\bold x \neq 0$)

#### Section 7.3: Exercises 1, 2

##### Exercise 1

Show that the Green's function is unique. (Hint: Take the difference of two of them)

---

Suppose we have two Green's Function for $-\Delta$ and the domain $D$ at the point $\bold x_0$, $G_1(\bold x)$ and $G_2(\bold x)$ 
Let $G= G_1 - G_2$ Then

1. $G$ possesses continuous second derivatives and $\Delta G = 0$ in $D\setminus \{\bold x_0\}$, 
2.  $G(\bold x) = 0$ for $x \in \partial D$ 
3. and $H_1 = G_1 - v$ and $H_2 = G_2 - v$ are both finite at $\bold x_0$ and have continuous second derivatives everywhere and is harmonic at $\bold x_0$, so $H_1 - H_2 = G_1 - G_2$ is finite at $\bold x_0$ and has continuous second derivatives everywhere and is harmonic at $\bold x_0$ (Note: $v = -\frac{1}{4\pi |\bold x - \bold x_0|}$ and $\Delta v = 0$)   

So this means $G = G_1 - G_2$ is harmonic in $D$, and by the maximum and minimum principles of harmonic functions, the maximum and minimum of $G$ is obtained on the boundary of $D$, and since we have $G(\bold x) = 0$ for $x \in \partial D$: $0 \leq G(\bold x) \leq 0$ $\implies G \equiv 0$ so $G_1(\bold x) = G_2(\bold x)$ 

or alternatively: taking $\Delta G = 0$ and multiplying both sides by $G$: 
$G \Delta G = 0$ $\implies$ $0 = \int_{D} G \Delta G\, d\bold x = \underset{= 0}{\int_{\partial D}G \frac{\partial G}{\partial n}} \, dS - \int_{D} |\nabla G|^2 \, d \bold x$ and by First Vanishing Theorem: $|\nabla G|^2 = 0 \implies \nabla G = 0 \implies G \text{ is a constant}$, but since $G = 0$ on the boundary, we must then have $G = G_1 - G_2 = 0 \equiv G_1 = G_2$ 

##### Exercise 2

Prove Theorem 2:

The solution of the problem 

$\Delta u = f \qquad \text{in }D \qquad u = h \text{ on } \partial D$ 

is given by
$$
u(\bold x_0) = \iint_{\partial D}h(\bold x)\frac{\partial G(\bold x, \bold x_0)}{\partial n} dS + \iiint_D f(\bold x)G(\bold x, \bold x_0) \, d\bold x
$$

---

Using Green's Second Identity on $u$ and Green's function $G(\bold x, \bold x_0)$: 
$\int_{\partial D} (u \frac{\partial G}{\partial n} - G \frac{\partial u}{\partial n}) \, dS = \int_D (u \Delta G - G \Delta u) \, d\bold x$ 
then using $u = h$ on $\partial D$ and that $G = 0$ on $\partial D$ and $\Delta u= f$ in $D$
$\int_{\partial D} h \frac{\partial G}{\partial n} \, dS = \int_D u \Delta G - Gf\, d\bold x$ 
$\int_D u \Delta G \, d \bold x = \int_{\partial D} h \frac{\partial  G}{\partial n} + \int_D Gf d \bold x$ 

Somehow: $\int_D u \Delta G \, d\bold x = u(\bold x_0)$ 
and $\Delta G(\bold x, \bold x_0) = \delta(\bold x_0 - \bold x)$ due to symmetry, so we have $\int_D u(\bold x) \Delta G(\bold x, \bold x_0) \, d \bold x = \int_D u(\bold x) \delta(\bold x_0 - \bold x)\, d \bold x = u(\bold x_0)$ 
therefore $u(\bold x_0) = \int_{\partial D} h(\bold x) \frac{\partial G}{\partial n}(\bold x, \bold x_0) \, dS + \int_D f(\bold x) G(\bold x, \bold x_0)\, d\bold x$ 

#### Section 7.4: Exercises 6, 7, 8

##### Exercise 6

**(a)**: Find the Green's Function for the half-plane $\{(x,y): y > 0\}$ 
$G(\bold x, \bold x_0) = \frac{1}{2\pi} \log|\bold x - \bold x_0| - \frac{1}{2\pi} \log|\bold x - \bold x_0^*|$, where $\bold x_0^* = (x_0, -y_0)$ if $\bold x_0 = (x_0, y_0)$ 
$G$ is finite and smooth everywhere except at $(x_0, y_0)$ and
$G_{x} = \frac{1}{2\pi} \frac{(x-x_0)}{|\bold x - \bold x_0|^2} - \frac{1}{2\pi} \frac{(x-x_0)}{|\bold x - \bold x_0^*|^2}$, $G_y = \frac{1}{2\pi} \frac{(y-y_0)}{|\bold x - \bold x_0|^2} - \frac{1}{2\pi} \frac{(y+y_0)}{|\bold x - \bold x_0^*|^2}$ 
$G_{xx} = \frac{1}{2\pi}[\frac{1}{|\bold x - \bold x_0|^2} - \frac{2(x-x_0)^2}{|\bold x - \bold x_0|^4} - \frac{1}{|\bold x - \bold x_0^*|^2} + \frac{2(x-x_0)^2}{|\bold x - \bold x_0^*|^4}]$, $G_{yy} = \frac{1}{2\pi}[ \frac{1}{|\bold x - \bold x_0|^2} - \frac{2(y-y_0)^2}{|\bold x - \bold x_0|^4} - \frac{1}{|\bold x- \bold x_0^*|^2} + \frac{2(y+y_0)^2}{|\bold x - \bold x_0^*|^4}]$ 
$G_{xx} + G_{yy} = \frac{1}{2\pi}[ \frac{2}{|\bold x - \bold x_0|^2} - \frac{2}{|\bold x - \bold x_0|^2} - \frac{2}{|\bold x - \bold x_0^*|^2} + \frac{2}{|\bold x - \bold x_0^*|^2}] = 0 $ , so $\Delta G = 0$ everywhere in the domain except at $\bold x_0$ 

and at $\bold x = (x, 0)$, $|\bold x - \bold x_0| = |\bold x - \bold x_0^*|$, so $G = 0$ on the boundary

and in two dimensions, instead of $-(4\pi |\bold x - \bold x_0|)^{-1}$, we use $\frac{1}{2\pi} \log |\bold x - \bold x_0|$ as $v$, so$H = $ $G - v = -\frac{1}{2\pi} \log |\bold x - \bold x_0^*|$, which is smooth everywhere in the domain and harmonic, since  $\Delta H = \frac{1}{2\pi} [ -\frac{2}{|\bold x - \bold x_0^*|^2} + \frac{2}{|\bold x - \bold x_0^*|^2}] = 0$ and since $\bold x_0^*$ isn't in the domainso $|\bold x - \bold x_0^*| \neq 0​$ 

**(b)**: Use it to solve the Dirichlet problem in the half-plane with boundary values $h(x)​$ 

using formula 7.3.1: $u(\bold x_0) = \int_{x \in \R, y = 0} u(\bold x) \frac{\partial G(\bold x, \bold x_0)}{\partial n}\, ds$, and in this case, $\bold n = (0, -1)$, so $\nabla G \cdot \bold n = -G_y $, which on the boundary $ = \frac{1}{2\pi}[\frac{2y_0}{(x-x_0)^2 + y_0^2}] = \frac{1}{\pi}\frac{y_0}{(x-x_0)^2 + y_0^2}$ 
so the solution is $u(\bold x_0) = \int_{-\infin}^\infin h(\xi) \frac{y_0}{(\xi - x_0)^2 + y_0^2} \frac{d \xi }{\pi}$ 

**(c)**: Calculate the solution with $u(x,0) = 1$ 
$\int_{-\infin}^\infin \frac{y_0}{(\xi - x_0)^2 + y_0^2} \frac{d \xi }{\pi}$ $ = \frac{1}{\pi}\int_{-\infin}^\infin \frac{1}{(\xi-x_0)^2/y_0 + y_0} d\xi$ $ = \frac{1}{\pi} \frac{1}{y_0} \int_{-\infin}^\infin \frac{1}{(\xi - x_0)^2 + 1} d\xi$ $= \frac{1}{\pi }[ \arctan(\frac{\xi - x_0)}{y_0})]|_{-\infin}^\infin = 1$ since $\arctan(x)|_{-\infin}^\infin = \pi$ 

##### Exercise 7

**(a)**: If $u(x,y) = f(x/y)$ is a harmonic function, solve the ODE satisfied by $f$:
$u_{x} = \frac{1}{y}f'(x/y)$, $u_y = -\frac{x}{y^2}f'(x/y)$, $u_{xx} = \frac{1}{y^2} f''(x/y)$, $u_{yy} = \frac{2x}{y^3}f'(x/y) + \frac{x^2}{y^4}f''(x/y)$ 
$\implies$ $0 = \Delta u = (\frac{1}{y^2}+ \frac{x^2}{y^4}) f''(x/y) +\frac{2x}{y^3} f'(x/y)$ 
multiply both sides by $y^2$: $(1 + x^2/y^2)f''(x/y) + 2x/y f'(x/y)$ and let $s = x/y$: $(1 + s^2)f''(s) + 2s f'(s) = 0$ 
let $g(s) = f'(s)$: $(1+s^2)\frac{dg}{ds} = -2s g$ $\implies$ $\frac{1}{g} dg = -\frac{2s}{1 + s^2}ds$ and integrating both sides: $\ln g = -\ln(1 + s^2) + a$ $\implies$ $e^{\ln g} =g= e^a e^{-\ln(1 +s^2)} = \frac{A}{1 + s^2}$ where $A = e^a$is a constant

$f'(s) = \frac{A}{1 + s^2}$ $\implies$ $f(s) = A\arctan(s) + B$ 
**(b)**: Show that $\frac{\partial u}{\partial r} \equiv 0$, where $r = \sqrt{x^2 + y^2}$ as usual, with $x = r\cos \theta$ $y = r\sin\theta$ 
$u(x,y) = f(x/y) = A \arctan(x/y) + B  $ so $u_r = u_x \frac{\partial x}{\partial r} + u_y \frac{\partial y}{\partial r} = \frac{1}{y}\frac{x}{r}f'(x/y) -\frac{x}{y^2}\frac{y}{r} f'(x/y) $ $ = 0$ 
**(c)**: so if $v(x,y)$ is a function that has $\partial v/\partial r \equiv 0$ this means $v(r, \theta)$ relies only on $\theta$, but $\theta = \arctan(x/y)$, a function of $x/y$, so $v$ then is a function $g(x/y) =( f\circ \arctan)(x/y)$  
**(d)**: Find the boundary values $\lim_{y \rightarrow 0} u(x,y) = h(x)$ 
as $y \rightarrow 0$, $x/y \rightarrow \infin$ so $\arctan(x/y) \rightarrow \pm\pi$ so $u(x,y) = h(x) = \begin{cases} A\pi/2 +B & x > 0 \\ -A\pi/2 +B& x < 0 \\ B & x = 0 \end{cases}$ assuming $y \downarrow 0$, if $y \uparrow 0$, we simply change the sign in front of $A \pi/2$ 
**(e)**: plug in boundary data:
$\int_{-\infin}^0 (-A\pi/2 + B) \frac{y_0}{(x-x_0)^2 + y_0^2} \frac{dx}{\pi}$ $ + \int_0^\infin (A\pi/2 + B) \frac{y_0}{(x-x_0)^2 + y_0^2} \frac{dx}{\pi}$ $= (-A\pi/2+B)\arctan(\frac{x-x_0}{y_0})/\pi |_{-\infin}^0 + (A\pi/2+B)\arctan(\frac{x-x_0}{y_0})/\pi |_{0}^\infin$and using $\arctan(x)$ is odd along with $\arctan(x) \rightarrow \pm \pi/2 x \rightarrow \pm \infin$ 

$u(\bold x_0) = A \arctan(x_0/y_0) + C_2$ 

##### Exercise 8

**(a)** Use Exercise 7 to find the harmonic function in the half-plane $\{y >0\}$ with the boundary data $h(x) = 1$ for $x>0$, $h(x) = 0$ for $x < 0$ 
$A = 1/\pi$, $B = 1/2$ from $h(x) = $$\pm A\pi/2 + B$ $+$ when $x>0$, $-$ when $x< 0$ 
so using the solution from $7$: $u(x, y) = \frac{1}{\pi} \arctan(x/y) + 1/2$ 
**(b)** same as (a), but $h(x) = 1$ for $x > a$, $h(x) = 0$ for $x < a$ 
$u(x, y) = 1/\pi \arctan((x-a)/y) + 1/2$, from following the hint, so we have $x - a> 0$ when $x> a$ and $x - a< 0$ when $x< a$ 
**(c)** Use part $(b)$ to solve the same problem with the boundary data $h(x)$ where $h(x)$ is any step function

and $h(x) = c_j$ when $a_{j-1} <x<a_j$, for $j = 1, 2, 3, …, n$ 
for when $n =2 $: 

When $-\infin < x < a_1​$ we have $A_1\pi^{-1}\arctan((x-a_{1})/y) = -\frac{A_1}{2}​$ and when $x > a_1​$, $A_1 \pi^{-1}\arctan((x-a_1)/y) = \frac{A_1}{2}​$  

so we want $-\frac{A_1}{2} +B = c_1$ and $\frac{A_1}{2} +B= c_2$  
so $B = \frac{c_1 + c_2}{2}$ $\implies$ $A_1 = \frac{c_2 - c_1}{2}$ 
so in general we want 
$-A_1/2 - A_2/2 - \cdots - A_n/2 + B = c_1$ 
$A_1/2 - A_2/2 - \cdots - A_n/2 + B = c_2$ 

$\vdots$

$A_1/2 + \cdots + A_{j-1}/2 - A_j/2 -  - \cdots  - A_n/2+ B = c_j$ 
$\vdots$
$A_1/2 + \cdots A_n/2 + B = c_n$ 

$\implies$ first equation + last equation gives: $B = \frac{c_n + c_1}{2}$, adding the second equation with the last yields $A_1 + c_n + c_1 = c_2 + c_n$ $\implies$ $A_1 = c_2 - c_1$, then continuing in this way, we get $A_j = c_{j+1} - c_j$ 
so 

$\pi^{-1}\sum_{j=1}^{n-1} \frac{c_{j+1} - c_j}{2} \arctan((x-a_j)/y) + \frac{c_n  + c_1}{2}$ 

so that when $a_{j-1} < x < a_j$: we have $\frac{c_2 - c_1}{2} + \frac{c_3 - c_2}{2} + \cdots + \frac{c_j - c_{j-1}}{2} + \frac{c_j - c_{j+1}}{2} + \frac{c_{j+1} - c_{j+2}}{2} + \cdots \frac{c_{n-1} - c_n}{2} + \frac{c_n + c_1}{2}$ 
$ = c_j$. if $x < a_1$: $\frac{c_1 - c_2}{2} +  \cdots + \frac{c_n + c_1}{2} = c_1$ and $x > a_{n-1}$: $\frac{c_2 - c_1}{2} + \cdots + \frac{c_n - c_{n-1}}{2} + \frac{c_n + c_1}{2} = c_n$ 
so that $(-\infin, a_1)$ we have $c_1$, $(a_1, a_2)$ we have $c_2$ … $(a_{j-1}, a_j)$, we have $c_j$ (I noticed the book answer includes $c_0$, but the problem in the book states the problem in a way where there should only be $c_1$ through $c_n$ since $(a_0, a_1) \equiv (-\infin, a_1)$ is $c_1$) 

#### Section 9.1: Exercises 1, 2, 7, 8

##### Exercise 1

Find all the three dimensional plane waves; that is all the solutions of the wave equation of the form $u(\bold x, t) = f(\bold k \cdot \bold x - ct)$, where $\bold k$ is a fixed vector and $f$ is a function of one variable

---

so $u(\bold x, t) = f(k_1x + k_2 y + k_3 z - ct)$ 

The wave equation in 3 dimensions: $u_{tt} = c^2(u_{xx} + u_{yy} + u_{zz})$ 
$u_{tt} = c^2 f''(\bold k \cdot \bold x - ct)$, $u_{xx} = k_1^2 f''(\bold k \cdot \bold x - ct)$, $u_{yy} = k_2^2 f''(\bold k \cdot \bold x - ct)$, $u_{zz} = k_3^2 f''(\bold k \cdot \bold x - ct)$ 

$c^2 f''(\bold k \cdot \bold x - ct) = c^2|\bold k|^2 f''(\bold k \cdot \bold x - ct)$ 
$\implies |\bold k|^2 = 1$ or $f''(\bold k \cdot \bold x - ct) = 0$ 

so in the case that $|\bold k| \neq 1$, we must have $f''(\bold k \cdot \bold x - ct) = 0$, so that $u(\bold x, t)=$ $f(\bold k \cdot \bold x - ct) =  A(\bold k \cdot \bold x - ct) + B$, where $A, B$ are constants

or if $|\bold k| = 1$, $u(\bold x, t)$ is any arbitrary function of the form $f(\bold k \cdot \bold x - ct)$ 

##### Exercise 2

Verify that $(c^2t^2 - x^2 - y^2 - z^2)^{-1}$ satisfies the wave equation except **on** the light cone

---

Note: Need to know what the light cone is

From memory:

$|\bold x - \bold x_0|^2 \leq c^2|t-t_0|^2$ this is the solid light cone and the light cone

the light cone itself is the equality

$u(\bold x, t) = \frac{1}{c^2t^2 - x^2 - y^2 - z^2}$ 

$u_t = -(c^2t^2-x^2-y^2-z^2)^{-2}(2c^2t)$ 

$u_{tt} = 2(c^2t^2 - x^2-y^2 - z^2)^{-3}(2c^2t)^2 - (c^2t^2 - x^2 - y^2 - z^2)^{-2}(2c^2)$ $u_x = -(c^2t^2 - x^2 - y^2 - z^2)^{-2}(-2x)$ 

$u_{xx} = 2(c^2t^2 - x^2 - y^2 - z^2)^{-3}(-2x)^2 - (c^2t^2 - x^2 - y^2 - z^2)^{-2}(-2)$ $u_{xx} = 8x^2u^3 +2u^2$, $u_{yy} = 8y^2u^3 + 2u^2$ $u_{zz} = 8z^2u^3 + 2u^2$

Simplifying these equations further:

$u_{tt} = 8c^4t^2u^3 - 2c^2u^2$ $ = 8c^2t^2u^3 - 2c^2(c^2t^2-x^2-y^2 - z^2)u^3$ 
$ = (6c^4t^2 + 2c^2(x^2 + y^2 + z^2))u^3$ 

$\Delta u = u_{xx} + u_{yy} + u_{zz} = 8(x^2 + y^2 + z^2)u^3 + 6u^2$ 
$ = 8(x^2+y^2 + z^2)u^3 + 6(c^2t^2 - x^2 - y^2 - z^2)u^3$ $ = (2(x^2+y^2+z^2) + 6c^2t^2)u^3$ and multiplying this by $c^2$ gives $u_{tt} = c^2 \Delta u$ as desired, so it satisfies the wave equation. 

Unless: $c^2t^2 - x^2 - y^2 - z^2 = 0$, i.e., when $x^2 + y^2 + z^2 = c^2t^2$ , which is the light cone at $(\bold 0, 0)$ 

##### Exercise 7

For the boundary condition $\frac{\partial u}{\partial n} + b \frac{\partial u}{\partial t} = 0$ with $b > 0$, show that the energy defined by $(6)$ decreases. 

---

$E = \frac{1}{2} \iiint (u_t^2 + c^2 |\nabla u|^2) \, d\bold x$ and taking the derivative with respect to $t$: $\partial E/\partial t = \frac{1}{2} \iiint(u_t^2 + c^2 |\nabla u|^2)_t\, d\bold x = \iiint c^2 \nabla \cdot (u_t \nabla u)\, d\bold x$ (integrating over a domain $D$) 
where the right side is from $0 = (u_{tt} - c^2 \Delta u)u_t = \frac{1}{2}(u_t^2 + \frac{1}{2}c^2 |\nabla u|^2)_t - c^2 \nabla \cdot (u_t \nabla u)$ 

Then using Divergence Theorem: $\partial E/\partial t = \iint_{\partial D} c^2 (u_t \nabla u) \cdot \bold n \, d S = \iint_{\partial D} c^2 (u_t \frac{\partial u}{\partial n}) \, d S$ 
Now using the boundary condition $\partial E/\partial t =-b c^2\iint_{\partial D} u_t^2\, dS < 0$, since $c^2 \iint_{\partial D} u_t^2 \, dS > 0$, but $-b < 0$ , so we have that Energy decreases over time.

##### Exercise 8

Consider the equation $u_{tt} - c^2 \Delta u + m^2 u = 0$, where $m > 0$, known as the Klein-Gordon equation

(**a**) What is the energy? Show that it is a constant
$u_{tt}u_t - c^2(u_{xx}u_t + u_{yy}u_t + u_{zz} u_t ) + m^2 u u_t = 0$ (after multiplying both sides by $u_t$) 
$\frac{1}{2}(u_t^2 +c^2 |\nabla u|^2)_t - c^2 \nabla \cdot(u_t \nabla u) + m^2\frac{1}{2}(u^2)_t = 0$ 

Integrating over 3-space, the integral of $c^2 \nabla \cdot (u_t \nabla u)$ disappears if $u(\bold x, t) \rightarrow 0$ as $|\bold x| \rightarrow \infin$ and if we assume this, we obtain:
$ 0 = \frac{1}{2}\iiint_{\R^3} \frac{\partial}{\partial t}(u_t^2 +c^2|\nabla u|^2 + m^2 u^2)\, d \bold x$, and then pulling out the time derivative: **The total energy ** $E = \frac{1}{2} \iiint (u_t^2 + c^2|\nabla u|^2 + m^2 u^2 )\, d \bold x$ is a constant 

**(b)** Prove the causality principle for it
Start with $\partial_t(\frac{1}{2}u_t^2 + \frac{1}{2}c^2|\nabla u|^2 + \frac{1}{2}m^2 u^2) -c^2 \partial_x(u_t u_x) -c^2 \partial_y(u_t u_y) - c^2 \partial_z(u_t u_z) = 0$ 
Integrate this over the frustrum $F$ with top $T$, bottom $B$, and side $K$ ($F$ is a piece of the solid light cone): 

$\iiiint_{F} \nabla \cdot[\frac{1}{2}(u_t^2 + c^2|\nabla u|^2 + m^2u^2), -c^2u_t u_x, -c^2 u_tu_y, -c^2 u_tu_z]\, dx\, dy\, dz\, dt = 0​$
Using the four dimensional divergence theorem:
$\iiint_{\partial F} [\frac{1}{2}n_t (u_t^2 + c^2|\nabla u|^2 + m^2u^2- n_x(c^2u_tu_x) - n_y(c^2u_tu_y) - n_z(c^2u_tu_z)] \, dV = 0​$ And since $F = T \cup B \cup K​$ we can split the integral into three parts:
$\iiint_T (\frac{1}{2}u_t^2 + \frac{1}{2}c^2|\nabla u|^2 +m^2u^2)\, d \bold x​$ , since the normal on $T​$ is $n_t = 1, n_x = 0, n_y = 0, n_z = 0​$ , and similalry for $B​$: w/ $n_t = -1​$: 
$\iiint_{B}(-1)\frac{1}{2}(u_t^2 + c^2|\nabla u|^2 +m^2 u^2) \, d \bold x​$ $ = - \iiint_B \frac{1}{2}(\psi ^2 + c^2|\nabla \phi|^2 + m^2 \phi^2)\, d\bold x​$ 

on $K$, we use formula $(3)$ for $\bold n$ witht he plus sign because the outward normal vector has a positive $t$ component on $K$ 

$\frac{c}{\sqrt{c^2 + 1}} \iiint_D \frac{1}{2}(u_t^2 + c^2|\nabla u|^2 + m^2u^2) + \frac{x-x_0}{cr}(-c^2u_tu_x) + \frac{y-y_0}{cr}(-c^2u_tu_y) + \frac{z-z_0}{cr}(-c^2u_tu_z) \, dV$ and this integrand can be written: $I = \frac{1}{2} (u_t^2 + c^2|\nabla u|^2 + m^2 u^2 ) - c u_tu_r$, where $u_r = u_x \frac{x-x_0}{r} + u_y \frac{y-y_0}{r} + u_z \frac{z-z_0}{r}$ 
this is due to $x= x_0 + r \cos \theta \cos \phi$ $\frac{\partial x}{\partial r} = \cos \theta \cos \phi$ $ = \frac{x-x_0}{r}$, and similalry for $y$ and $z$. Then:
$I = \frac{1}{2}(u_t - cu_r)^2 + \frac{1}{2}(c^2|\nabla u|^2 - c^2u_r) +m^2u^2$ after completing the square.
$(|\nabla u|^2 - u_r^2) = u_x^2 + \cdots - (u_x^2 \frac{\partial x}{\partial r}^2 + 2( \frac{\partial y}{\partial r}\frac{\partial x}{\partial r}u_xu_y  + \frac{\partial x}{\partial r}\frac{\partial z}{\partial r}u_x u_z + \frac{\partial y}{\partial r}\frac{\partial z}{\partial r} u_y u_z) + u_y^2 \frac{\partial y}{\partial r}^2 + u_z^2 \frac{\partial z}{\partial r}^2)$

$ u_x(u_x - \frac{\partial x}{\partial r}(u_x\frac{\partial x}{\partial r} + u_y \frac{\partial y}{\partial r} + u_z \frac{\partial z}{\partial r})) + $

 $(u_x - \frac{\partial x}{\partial r}(u_x \frac{\partial x}{\partial r} + u_y \frac{\partial y}{\partial r} + u_z \frac{\partial z}{\partial r})$

then: $I = \frac{1}{2}(u_t - cu_r)^2 +m^2u^2 + \frac{1}{2}c^2|\nabla u - u_r \hat{\bold r}| > 0$, so $\iiint_K > 0$ 
$\implies$ $\iiint_T (\frac{1}{2} u_t^2 + \frac{1}{2}c^2 |\nabla u|^2 + m^2u^2) \, d \bold x \leq \iiint_B(\frac{1}{2} \psi^2 + c^2|\nabla \phi|^2 + m^2 \phi^2) \, d \bold x$ 
and if $\psi$ and $\phi$ vanish in $B$, by the first vanishing theorem, $\frac{1}{2}u_t^2 + \frac{1}{2}c^2 |\nabla u|^2 + m^2u^2 = 0$ in $T$ 
$\implies$ $u_t, |\nabla u|, u$ vanish in $T$, and since we can vary the height $T$ of $F$, we have that $u$ must vanish in all of the cone $C$ that lies above $B$ or more specifically, $u(x_0, y_0, z_0, t_0) = 0$. This means that when we take any two solutions of the Klein-gordon equation, $u$ and $v$ with $u = v$ in $B$ we have $u(\bold x_0, t_0) = v(\bold x_0, t_0)$. Therefore, the value of a solution $u$ at $(x_0, y_0, z_0, t_0)$ only depends on its values at $B$ $= \{ |\bold x - \bold x_0| \leq ct_0 \}$ 

#### Section 9.2: Exercises 5, 11

##### Exercise 5

Where does a three-dimensional wave have to vanish if its initial data $\phi$ and $\psi$ vanish outisde a sphere? 

---

Let $R$ be the radius of the sphere. We know that a point on or in this sphere $\bold x_1$ influences the solution only on the surface $\{|\bold x - \bold x_1| = ct\}$ (Huygen's principle) Generally, at points in space time that don't lie on such points. 

Let  $\bold x_0​$ be the center of this sphere, so a point $\bold x_1​$ on or in the sphere satisfies $|\bold x_1 - \bold x_0| \leq R​$, and due to Huygen's principle, the values of $\psi​$ and $\phi​$ at such a point $\bold x_1​$ influences the solution only on the set $\{\bold x: |\bold x - \bold x_1| = ct \}​$  and such points satisfy: $|\bold x - \bold x_0| = |\bold x - \bold x_1 + \bold x_1 - \bold x_0| \leq |\bold x - \bold x_1| + |\bold x_1 - \bold x_0| \leq  ct + R​$ for $t \geq 0​$. Then, points on the sphere meet at $\bold x_0​$ at time $t = R/c​$ (because that's the time it takes for them to travel distance $R​$ at speed $c​$) and following Huygen's principle again, since values of $\phi, \psi​$ at $\bold x_0​$ only affect values of $u​$ at $|\bold x - \bold x_0| = c(t - \frac{R}{c})​$ from $t > \frac{R}{c}​$ we have that $\phi, \psi​$ at points $|\bold x - \bold x_0| < c(t - \frac{R}{c})​$ 

are not affected by $\phi, \psi$ at $\bold x_0$ and therefore not affected by $\phi, \psi$ on or inside the sphere. 
so for $\{|\bold x - \bold x_0| < c(t - R/c)$ for $t > R/c\}$ and $\{|\bold x - \bold x_0| > ct + R $ for $t \geq 0\}$ the waves vanish

##### Exercise 11

Find all the spherical solutions of the three-dimensional wave equation; that is find the solutions that depend only on $r$ and $t$

---

(5): $(u)_{tt} = c^2( u)_{rr} + 2c^2 \frac{1}{r}(u)$  (but for $u$, not the mean of $u$) 

The solutions we're looking for satisfy $(5)$ as written above. 

and if $v = ru(r, t)$, $v_r = ru_r + u$ and $v_{rr} = ru_{rr} + 2u_r $, so $v$ satisfies $v_{tt} = c^2 v_{rr}$ 
this is the usual wave equation in one dimension, so $v = f(r + ct) + g(r - ct)$ so that $u = \frac{f(r + ct) + g(r-ct)}{r}​$ 
Renee Senining

Math 126

Homework 1

### Section 1.1: Exercises 2, 3, 4, 7, 8, 10, 11, 12

##### 2. Which of the following operators are linear?

Using that $\frac{\partial }{\partial x}(u+v) = \frac{\partial u}{\partial x} + \frac{\partial v}{\partial x}$  and $\frac{\partial }{\partial x}(cu) = c\frac{\partial u}{\partial x}$ 

1. $\mathscr{L}u = u_x + xu_y$ 

$$
\mathscr{L}(u+v) = (u+v)_x + x(u+v)_y 
\\ = u_x + v_x + xu_y + xv_y 
\\ = (u_x + xu_y) + (v_x + xv_y) 
\\= \mathscr{L}u + \mathscr{L}v
$$

$$
\mathscr{L}(cu) = (cu)_x + x(cu)_y = cu_x + xcu_y = c(u_x + xu_y)= c\mathscr{L}u
$$

**Linear** 

2. $\mathscr{L}u = u_x + uu_y$  

$$
\mathscr{L}(u+v) = (u+v)_x + (u+v)(u+v)_y 
\\ = u_x + v_x + uu_y + vu_y + uv_y + vv_y 
\\ = \mathscr{L}u + \mathscr{L}v + vu_y + uv_y
\neq \mathscr{L}u + \mathscr{L}v
$$

**not linear** 

3. $\mathscr{L}u = u_x + u_y^2$  

   differentiating $(u+v)$ w.r.t. to $y$ first then squaring

$$
\mathscr{L}(u+v) = (u+v)_x + [(u+v)_y]^2 
\\ = u_x + v_x + (u_y + v_y)^2 
\\ = u_x + v_x + u_y^2 + 2u_yv_y + v_y^2
\\ = \mathscr{L}u + \mathscr{L}v + 2u_y v_y 
\\ \neq \mathscr{L}u + \mathscr{L}
$$

**not linear** 

4. $\mathscr{L}u = u_x + u_y + 1$  

$$
\mathscr{L}(u+v) = (u+v)_x + (u+v)_y + 1 
\\ = u_x + v_x + u_y + v_y + 1
\\ \mathscr{L}u + v_x + v_y
\\ \neq \mathscr{L}u + \mathscr{L}v
\\
\mathscr{L}(cu) = (cu)_x + (cu)_y + 1 
\\ = cu_x + cu_y + 1 
\\ \neq c\mathscr{L}u = c(u_x + u_y + 1)
$$

**not linear**

5. $\mathscr{L}u = \sqrt{1 + x^2}(\cos y)u_x + u_{yxy} - [\arctan(x/y)]u$  

$$
\mathscr{L}(u+v) = \sqrt{1 + x^2}(\cos y)(u+v)_x + (u+v)_{yxy} - [\arctan(x/y)](u+v)
\\ = \sqrt{1+x^2}(\cos y)(u_x + v_x) + u_{yxy}+v_{yxy} - [\arctan(x/y)]u - [\arctan(x/y)]v
\\ = \mathscr{L}u+\mathscr{L}v
$$

$$
\mathscr{L}(cu) = \sqrt{1 + x^2}(\cos y)(cu)_x + (cu)_{yxy} - [\arctan(x/y)](cu)
\\ = \sqrt{1 + x^2}(\cos y)cu_x + cu_{yxy} - c[\arctan(x/y)]
\\ = c(\sqrt{1+x^2}(\cos y)u_x + u_{yxy} - [\arctan(x/y)])
\\ = c\mathscr{L}u
$$

**linear** 

##### 3. For each of the following equations, state the order and whether it is nonlinear, linear inhomogeneous, or linear homogeneous; provide reasons

1. $u_t - u_{xx} +1 = 0$ $\equiv$ $u_t - u_{xx} = -1$ 

   Order: 2, due to $u_{xx}$ being the term with the highest derivative

   Linear: yes, with $\mathscr{L}u = u_t - u_{xx}$ $\mathscr{L}(u+v) = u_t + v_t - u_{xx} - v_{xx}= \mathscr{L}u + \mathscr{L}v $ and obvious for $\mathscr{L}(cu) = c\mathscr{L}u$ 

   Homogeneous: no, because of $-1$ on righthand side

   **2nd order linear inhomogeneous**

2. $u_t - u_{xx} + xu = 0$ 

   Order: 2 due to $u_{xx}$ being the term with the highest derivative

   Linear: yes, with $\mathscr{L}u = u_t - u_{xx} + xu$ $\mathscr{L}(u+v) = u_t + v_t - u_{xx} - v_{xx} + xu + xv = \mathscr{L}u + \mathscr{L}v$ and obvious for $\mathscr{L}(cu) = c\mathscr{L}u$

   Homogeneous: yes, because we have $\mathscr{L}u = 0$

   **2nd order linear homogeneous** 

3. $u_t - u_{xxt} + uu_x = 0$

   Order: 3, due to $u_{xxt}$ being the term with the highest derivative

   Linear: no, because with $\mathscr{L}u = u_t - u_{xxt} + uu_x$, we have in $\mathscr{L}(u+v)$ terms of the form $uv_x$ and $vu_x$

   **3rd order Nonlinear**

4. $u_{tt} - u_{xx} + x^2 = 0$ $\equiv$ $u_{tt} - u_{xx} = -x^2$

   Order: 2, due to $u_{tt}$/$u_{xx}$ being the term w/ highest derivative

   Linear: yes, with $\mathscr{L}u = u_{tt} - u_{xx}$ $\mathscr{L}(u+v) = u_{tt} + v_{tt} - u_{xx} - v_{xx} = \mathscr{L}u + \mathscr{L}v$ and obvious for $\mathscr{L}(cu) = c\mathscr{L}u$ 

   Homogeneous: no, because $g = -x^2$ 

   **2nd order linear inhomogeneous** 

5. $iu_t - u_{xx} + u/x = 0$ 

   Order: 2, due to $u_{xx}$ being the term w/ highest derivative

   Linear: yes
   $$
   \mathscr{L}u = iu_t - u_{xx} + u/x 
   \\ \mathscr{L}(u+v) = i(u+v)_t - (u+v)_{xx} + (u+v)/x
   \\ = iu_t + iv_t - u_{xx} - v_{xx} + u/x + v/x
   \\ = (iu_t - u_{xx} + u/x) + (iv_t - v_{xx} + v/x)
   \\ = \mathscr{L}u + \mathscr{L}v
   $$

   $$
   \mathscr{L}(cu) = icu_t - cu_{xx} + cu/x = c(iu_t - u_{xx} + u/x) = c\mathscr L u
   $$

   Homogeneous: yes, we have $\mathscr L u = 0$ 

   **2nd order linear homogeneous**

6. $u_x(1 + u_x^2)^{-1/2} + u_y (1 + u_y^2)^{-1/2} = 0$ 

   Order: 1, highest derivative(s) are $u_x, u_y$ 

   Linear: no, due to $u_x^2$, $u_y^2$ terms and $(1 + u_x^2)^{-1/2}$ and $(1+u_y^2)^{-1/2}$ 

   **1st order nonlinear**

7. $u_x + e^y u_y = 0$ 

   Order: 1, highest derivative(s) are $u_x, u_y$ 

   Linear: yes
   $$
   \mathscr{L}u = u_x + e^y u_y 
   \\ \mathscr{L}(u+v) = (u+v)_x + e^y(u+v)_y
   \\ u_x + v_x + e^yu_y + e^yv_y
   \\ = (u_x + e^y u_y) + (v_x + e^y v_y ) 
   \\ = \mathscr{L}u + \mathscr L v
   $$

   $$
   \mathscr L (cu) = (cu)_x + e^y(cu)_y
   \\ = cu_x + e^ycu_y
   \\ = c(u_x + e^y u_y)
   \\ = c\mathscr L u
   $$

   Homogeneous: yes, we have $\mathscr L u = 0$  

   **1st order linear homogeneous** 

8. $u_t + u_{xxxx} + \sqrt{1 +u}= 0$  

   Order: 4, due to $u_{xxxx}$ being the highest derivative 

   Linear: no, otherwise, we must have $\mathscr{L}u =u_t + u_{xxxx} + \sqrt{1 + u}$ as the linear operator

   and $\mathscr L (u + v) = u_t + v_t + u_{xxxx} + v_{xxxx} + \sqrt{1 + u + v} \neq$ $\mathscr{L}u + \mathscr{L}v$ $ = u_t + u_{xxxx} + \sqrt{1 + u} + v_t + v_{xxxx} + \sqrt{1 + v}$ 

   **4th order nonlinear** 

##### 4. Show that the difference of two solutions of an inhomogeneous linear equation $\mathscr{L}u = g$ with the same $g$ is a solution of the homogeneous equation $\mathscr L u =0$ 

Suppose $u(x,y), v(x,y)$ are two solutions to $\mathscr{L}u = g$ 

$\implies$ $\mathscr L u = g$, $\mathscr L v = g$ 

Due to linearity we have: 
$$
\mathscr L (u - v) = \mathscr{L}u + \mathscr{L}(-v) 
\\ = \mathscr{L}u - \mathscr{L}v 
\\ = g - g 
\\ = 0
$$
which means $u-v$, the difference, is a solution of the homogeneous equation

note: $\mathscr{L}(v-u) = -\mathscr L (u-v) = 0$ 



##### 7. Are the functions $1+x, 1-x, $ and $1 + x + x^2$ linearly dependent or independent? Why?

These functions exist in the space of 2nd degree polynomials, which means we can express them in terms of vectors in $\R^3$/$\C^3$ (3 dimensions)

$ax^2 + bx + c \equiv [a, b, c]$ 

$[0, 1, 1]$, $[0, -1, 1]$, and $[1, 1, 1]$ 

These vectors are linearly independent, because they form a basis for the space of 2nd degree polynomials:

$\begin{pmatrix} 0 & 0 & 1 \\ 1 & - 1 & 1 \\ 1 & 1 & 1 \end{pmatrix}$ $\implies$ switching rows 1 and 3  $\implies$ $\begin{pmatrix} 1 & 1 & 1 \\ 1 & -1 & 1 \\ 0 & 0 & 1 \end{pmatrix}$ 

$\implies$ $\begin{pmatrix} 1 & 1 & 1 \\ 0 & -2 & 0 \\ 0 & 0 & 1 \end{pmatrix}$  $\implies$ multiplying row 2 by (-1/2), then row 1 + (-1)row 2, then row 1 - row 2 $\implies$ $\begin{pmatrix} 1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1\end{pmatrix}$ $\equiv$ the RREF of this matrix is $I$ $\equiv$ the matrix is invertible $\equiv$ the columns of the matrix, which in this case are associated with the functions, are linearly independent. 

##### 8. Find a vector that, together with the vectors [1, 1, 1] and [1, 2, 1], forms a basis of $\R^3$ 

A basis for a vector space is a set of linearly independent vectors that span the vector space. The number of vectors that make up a basis = the dimension of the vector space, and $\R^3$ is a vector space of dimension 3. $[1, 1, 1]$ and $[1, 2, 1]$ are obviously linearly independent as there is no scalar $c$ s.t. $c[1, 1, 1] = [1, 2, 1]$. The cross product of two vectors is a vector that is perpendicular to both vectors $\implies$ 

if $v_3 = v_1 \times v_2$:

Since the dot product of orthogonal vectors is 0: $v_1 \cdot v_3 = 0, v_2 \cdot v_3 = 0$. We then have $v_3 \cdot (av_1 + bv_2) = av_1\cdot v_3 + bv_2 \cdot v_3 = a(0) + b(0) = 0​$ 

$v_3 = v_1 \times v_2$ $= \begin{pmatrix} \vec i & \vec j & \vec k \\ 1 & 1 & 1 \\ 1 & 2 & 1 \end{pmatrix}$ $ [1-2, 1-1, 2 - 1] = [-1, 0, 1]$  is a vector together with $[1, 1, 1]$ and $[1, 2, 1]$ that will form a basis of $\R^3​$ 

##### 10. Show that the solutions of the differential equation $u''' - 3u'' + 4u = 0$ form a vector space. Find a basis of it

I'm assuming we are dealing with functions over real/complex numbers, and so scalars are going to be real/complex

**note**: this is linear: $\mathscr L(u+ v) = (u+v)''' - 3(u+v)'' + 4(u+v) = u''' - 3u'' + 4u + v''' - 3v'' + 4v = \mathscr L u + \mathscr L v$ and $\mathscr L (cu) = (cu)''' - 3(cu)'' + 4(cu) = cu''' - 3cu'' + 4cu = c\mathscr L u$ 

so **linear combinations of solutions are also solutions.**

so the set of solutions is closed under addition and scalar multiplication.

A vector space $V$ over a field $F​$ must satisfy the following:

1. associativity of addition

   Suppose $u, v,$ and $w$ are solutions to the differential equation above. 

   Since given two functions $f, g​$ we have $(f + g)(x) = f(x) + g(x)​$ 

   $\implies$
   $$
   (u+(v+w))(x) = u(x) + (v+w)(x)
   \\ u(x) + v(x) + w(x)
   \\\text{by associativity of real/complex numbers: } 
   \\ = (u(x) +v(x)) + w(x) 
   \\ = (u+v)(x) + w(x)
   \\ = ((u+v) + w)(x)
   $$

   Also:
   $$
   \mathscr L(u+(v+w)) = \mathscr L u + \mathscr L (v+w)
   \\ = \mathscr L u + \mathscr L v + \mathscr L w
   \\ = \mathscr L (u+ v) + \mathscr L w
   \\ = \mathscr L ((u+v)+w)
   $$

2. commutativity of addition

   Suppose $u, v$ are solutions of the differential equation above:
   $$
   (u+v)(x) = u(x) + v(x)
   \\ \text{by commutativity of real/complex numbers:}
   \\ = v(x) + u(x)
   \\ = (v+ u)(x)
   $$

3. contains additive identity element

   if we let $u \equiv 0$, we indeed have $0 - 3(0) +4(0) = 0$. So $0$ is in the vector space.

4. if $a, b$ are scalars in $F$, and $\vec v$ is in $V$: $a(b\vec v) = (ab)\vec v$ 

   let $a, b$ be scalars in $\R$ or $\C$ and let $u$ be a solution to the differential equation.

   We have from properties of functions, when given two functions $f, g$:

   $(f\cdot g)(x) = f(x)\cdot g(x)$, and below I first consider $f = a (\text{constant function}), g = bv$ and so on...
   $$
   (a(bv))(x) = a[(bv)(x)]
   \\ = a[bv(x)] 
   \\ = abv(x)
   \\ = (ab)v(x)
   \\ = ((ab)v)(x)
   $$

5. letting $1$ denote the multiplicative identity in $F$, must have $1v = v$ 

   we have $1 \in \R$ satisfying this for any function that is a solution to the differential equation. 

6. distributivity of scalar multiplication w.r.t. to vector addition $a(\vec u + \vec v) = a\vec u + a \vec v$ 

   If we have $u, v$ as two solutions and $a$ a scalar 
   $$
   (a(u+v))(x) = a[(u+v)(x)]
   \\ = a[u(x) + v(x)]
   \\ \text{distributivity holds with real/complex numbers:}
   \\ = au(x) + av(x)
   \\ = (au)(x) + (av)(x)
   \\ = (au + av)(x)
   $$

7. distributivity of scalar multiplication w.r.t. to field addition $(a + b)\vec v = a\vec v + b \vec v$ 

   If we have $a, b$ as two scalars and $u$ as a solution 
   $$
   ((a+b)u)(x) = (a+b)[u(x)]
   \\ = au(x) + bu(x)
   \\ = (au)(x) + (bu)(x)
   \\ = (au + bu)(x)
   $$
   

##### 11. Verify that $u(x,y) = f(x)g(y)$ is a solution of the PDE $uu_{xy} = u_x u_y$ for all pairs of (differentiable) functions $f$ and $g$ of one variable.

$$
u_x = f'(x)g(y)
\\u_y = f(x)g'(y)
\\u_{xy} = (u_x)_y = f'(x)g'(y)
\\\implies
\\ uu_{xy} = 
\\ [f(x)g(y)][f'(x)g'(y)]
\\ = f'(x)g(y)f(x)g'(y) 
\\ = u_xu_y
$$
$f$ and $g$ were arbitrary differentiable functions of one variable, so the proof is done. 

##### 12. Verify by direct substitution that $u_{n}(x,y) = \sin n x \sinh ny$ is a solution of $u_{xx} + u_{yy} = 0$ for every $n > 0$ 

$(u_n)_x = n\cos n x \sinh ny \\ (u_n)_{xx} = -n^2 \sin n x \sinh ny​$ 

Then, we have $\frac{d}{dy} \sinh (y) = \cosh (y)$ and $\frac{d}{dy}\cosh(y) = \sinh(y)$: from $\sinh(y) = (e^y - e^{-y})/2, \cosh(y) = (e^y + e^{-y})/2$ and the derivative of $e^y$ $\implies$ 

$(u_n)_y = \sin n x(n \cosh ny)$, $(u_n)_{yy} = \sin nx (n^2 \sinh ny)$ $ = n^2 \sin nx \sinh ny$ 

so we have $u_{xx} + u_{yy} = -n^2 \sin nx \sinh ny + n^2 \sin nx \sinh ny$ $ = 0$, finishing the proof. 

$n$ was arbitrary (non-complex), so this works for any $n > 0$ 

### Section 1.2: Exercises 2, 3, 6, 7

##### 2. Solve the first-order equation $2u_t + 3u_x = 0$ with the auxiliary condition $u = \sin x$ when $t = 0$ 

From lecture and chapter 1.2, we have $a = 2, b = 3$ and $u(t, x) = f(3t - 2x)$ 

so $u(0, x) = \sin x$ $\implies$ $f(-2x) = \sin x$ 

letting $w = -2x$, $x = -\frac{w}{2}$ 

$f(w) = \sin (-\frac{w}{2}) = -\sin \frac{w}{2}$ $\implies$ $f(3t - 2x) = -\sin \frac{3t - 2x}{2}$

$u_t = -\frac{3}{2} \cos \frac{3t - 2x}{2}$ , $u_x =  -(-1)\cos \frac{3t - 2x}{2} = \cos \frac{3t - 2x}{2}$ 

$2u_t + 3u_x = -3 \cos \frac{3t-2x}{2} + 3 \cos \frac{3t - 2x}{2}$ $ = 0$ 

##### 3. Solve the equation $(1 + x^2)u_x + u_y = 0$. Sketch some of the characteristic curves.

$[(1, + x^2), 1] \cdot \nabla u = 0​$, so curves satisfying

$\frac{d y}{dx} = \frac{1}{1 + x^2}$ are the characteristic curves ($u$ along these curves is constant)
$$
dy = \frac{1}{1 + x^2}dx
\\ \int dy = \int \frac{1}{1 + x^2}dx
\\ y = \arctan (x) + C
$$
or $x = \tan(y- C) $
$$
\frac{d}{dy}u(\tan(y-C), y) = u_x\cdot \sec^2(y-C)  + u_y 
\\ = u_x(\tan^2(y-C)+1) + u_y 
\\=  u_x(x^2 + 1) + u_y
\\ = 0
$$
Noting: $\frac{1}{\cos^2 x} = \frac{\sin^2x+\cos^2x}{\cos^2x} = \tan^2x + 1$

$u(x, y) = u(x, \arctan(x)+C) $ and since $u$ is constant along $y = \arctan(x) +C$ (for fixed $C$, we can choose an arbitrary point on the curve, e.g., $u(x, \arctan(x)+C) = u(0, C)$ 

and $C = y - \arctan(x)$, so $u(0, C) = u(0, y - \arctan(x))$ 

so $u(x, y) = f(y - \arctan(x)$)

![Sketch001](/Users/r/Documents/Sketch001.jpg)

##### 6. Solve the equation $\sqrt{1 - x^2}u_x + u_y = 0$ with the condition $u(0, y) = y$ 

$[\sqrt{1 - x^2}, 1]\cdot \nabla u = 0$

Note: the PDE is only defined for $x \in (-1, 1)$ because of $\sqrt{1 - x^2}$ 
$$
\frac{dy}{dx} = \frac{1}{\sqrt{1 - x^2}}
\\ dy = \frac{1}{\sqrt{1-x^2}}dx
\\ \int dy = \int \frac{1}{\sqrt{1 - x^2}}dx
\\ \text{let } x = \sin w, dx = \cos w  \, dw
\\ y = \int \frac{\cos w}{\sqrt{1 - \sin^ 2 w}}dw
\\ y = \int \frac{\cos w}{\cos w}dw
\\ y = w 
\\ y = \arcsin(x) + C
$$
or $x = \sin (y - C)​$, $x \in (-1, 1​$) 
$$
\frac{d}{dy}(u(\sin(y-C), y)) = u_x(\cos(y-C))+ u_y
\\ = u_x\sqrt{1 - \sin^2(y-C)} + u_y 
\\ = u_x \sqrt{1 - x^2} + u_y 
\\ = 0
$$
Since $u$ is constant along $y = \arcsin(x) + C$ for fixed $C$,

for any $x \in [-1, 1]$, $u(x, y) = u(x, \arcsin(x) + C) = u(0, C) = u(0, y - \arcsin(x))$

 $u(0, y ) = u(0, y-\arcsin(x)) = y - \arcsin(x)$

$u(x, y) = y - \arcsin(x)$ 

Checking my answer:

$u_x = - \frac{1}{\sqrt{1-x^2}}$ $u_y = 1​$ 

$\sqrt{1 - x^2}u_x + u_y = -\frac{\sqrt{1-x^2}}{\sqrt{1-x^2}} + 1$ $ = -1 + 1 =0$ 

$u(0, y) = y - \arcsin(0) = y$  

##### 7. 

1. Solve the equation $yu_x + xu_y = 0$ with $u(0, y) = e^{-y^2}$ 

   $(y, x)\cdot \nabla u = 0$ 
   $$
   \frac{dy}{dx} = \frac{x}{y}
   \\ y \; dy = x\; dx
   \\ \frac{y^2}{2} = \frac{x^2}2 + C
   \\  y^2 = x^2 + 2C
   \\ y = \sqrt{x^2 + 2C}, y = -\sqrt{x^2 + 2C}
   $$
   so $y^2 - x^2 = 2C$  are the characteristic curves and $C = \frac{y^2 - x^2}{2}$ 

   So: $u(x,y) = u(x, \pm \sqrt{x^2 + 2C})$ $= u(0, \pm \sqrt{2C})$ = $u(0, \pm \sqrt{y^2 - x^2})$ 

   or: $u(0, y^2 - x^2)  = f(y^2 - x^2)$

   $u(0, y) = u(0, \pm \sqrt{y^2 - x^2})​$ $ = e^{-y^2} = e^{-(y^2 - x^2)}​$ 

   $u(x,y) = e^{x^2 - y^2}$ 

   Checking:

   $u_x = 2xe^{x^2 - y^2}, u_y = -2ye^{x^2 - y^2}$ 

   $yu_x + xu_y = y2xe^{x^2 - y^2} + x(-2ye^{x^2-y^2})$ $ = 2xye^{x^2 - y^2} - 2xye^{x^2 - y^2} = 0$

   $u(0, y) = e^{0 - y^2} = e^{-y^2}$ 

2. In which region of the $xy$ plane is the solution uniquely determined

   $u(x, y) = e^{x^2 - y^2}$ 

   The characteristic curves pass through $x = 0$ exactly once when $y^2 - 0^2 = 2C$ $\implies$  $2C \geq 0$ $\implies$ 

   the solution we derived in the first part, which depended on using characteristic curves and on $x = 0$, is a unique solution when $y^2 - x^2 \geq 0$ 

   ---

   Also, we had $u(0, \pm \sqrt{2C})$ which implies $2C \geq 0​$ 

   
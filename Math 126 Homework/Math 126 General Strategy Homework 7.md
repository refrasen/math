# Math 126 General Strategy: Homework 7

#### 7.1 Exercise 5: Energy Function

There were some assumptions given, so don't worry about those too much

What was used beyond plugging in to $E[w]$: 

* Green's 1st Identity: **memorize** 
* Replacing $\partial u/\partial n$ with $h(\bold x)$, i.e., taking advantage of boundary condition
* Generally needed to follow a method in the book: Dirichlet's Principle **memorize this methodology** 
  * $\implies$: Plug in $w = u- v$ to $E[w]$ and use PDE and boundary conditions and Green's First identity
  * $\impliedby:$ Assume $u(\bold x)$ minimizes $E[w]$. and $v(\bold x)$ can be any function
    * let $e(s) = E[u + sv]$ and see that $e'(0) = 0$, since $E[u]$ is the minimum
    * Need to use Green's 1st Identity at some point
    * and we should arrive at $\iiint_D v \Delta u\, \bold x = 0$ 

#### Section 7.3: Exercises 1, 2

##### Exercise 1: Showing Green's Function is unique

* Need to **memorize** that Green's function is designated for $-\Delta$ and a domain $D$ and a point $\bold x_0 \in D$ 
  * $G$ has continuous second partial derivatives everywhere except for at $\bold x_0$ and is $\Delta G = 0$ in $D \setminus \{ \bold x_0 \}$ 
  * $G = 0$ on $\partial D$ 
  * $H = G - \frac{1}{4\pi|\bold x - \bold x_0|}$ has continuous second partial derivatives everywhere in $D$ and is harmonic in $D$ including at $\bold x_0$ 
* Used maximum principle and minimum principle for harmonic functions, so **memorize** it
* OR: Used Green's First theorem on $G = G_1 - G_2$ twice, so $u = v = G$ then the First Vanishing Theorem

##### Exercise 2: Prove Theorem 2

* This just used Green's Second Identity, which you should **memorize** ;)
* and ahem, also that $\int_D u \Delta G \, d \bold x = u(\bold x_0)$, this i'm still not sure about.

#### Section 7.4: Exercises 6, 7, 8

##### Exercise 6

* Problems: Didn't compute derivatives correctly
* In 2 dimensions, $v = \frac{1}{2\pi} \log |\bold x - \bold x_0|$ 
* we reflect since it's the half-plane, this was just copying the method in 7.4 for the half-space but in 2 dimensions
* Just plug in to formula 7.3.1
  * realize that $\bold n = (0, -1)$, so $\frac{\partial G}{\partial n} = -G_y$ and then evaluate on the boundary
* Needed to know that $y_0/((x-x_0)^2 + y_0^2)$ has antiderivative $\arctan((x-x_0)/y_0)$ 

* Arctan(x) as x tends towards infinity is $\pi/2$ and as x tends towards negative infinity is $-\pi/2$  

##### Exercise 7

* Chain rule, and having to factor out $y^2$ to get powers of $x/y$ and let $s = x/y$ 
* also required arctan(x)

##### Exercise 8

* needed to reference exercise 7
* was pretty simple in terms of math 126 knowledge, just needed to be sort of savy with translations and choice of constants...

#### Section 9.1: Exercises 1, 2, 7, 8

##### Exercise 1

just needed to know how to chain rule and use wave equation :)

then also know what it takes for equality that sometimes a function needs to equal 0 i.e.

$cx = x$ if either c = 1 or x = 0

##### Exercise 2

* Need to know that the light cone is when $|\bold x - \bold x_0| = c|t-t_0|$ and that the solid light cone is the inside: $|\bold x - \bold x-0| < c|t-t_0|$ 

##### Exercise 7

* Needed to use something from deriving the Energy equation i.e., the term that vanished when taking the integral over all of $\R^3$ for example

##### Exercise 8

Idk about this one. requried looking at the way the book did things on Energy and Principle of Causality

#### Section 9.2

##### Exercise 5

This was tricky, really required understanding Huygen's principle and what it means for the waves to travel etc. 

Also needed to know about the light cone again

##### Exercise 11

Easy, just use the laplacian for when it doesn't rely on $\phi$ or $\theta$ then let $v = ru$ and we get the one dimensional wave equation which we know how to solve 
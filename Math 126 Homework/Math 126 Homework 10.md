Renee Senining 
Math 126

### Homework 10

#### Section 11.1: Exercises 2, 3, 4, 5

##### Exercise 2

Theorem 1: Assuming $u(\bold x)$ is a solution of (MP), then the value of the minimum equals the first smallest eigenvalue $\lambda_1$ of (1) and $u(\bold x)$ is its eigenfunction:

$\lambda_1 = m = \min\{ \frac{\|\nabla w\|^2}{\|w\|^2}\}$ and $-\Delta u = \lambda_1 u$ in $D$ 

(9) comes about when we assume $w \in C^2$, $w = 0$ on $\partial D$, and $\int w^2 d\bold x = 1$ 
we have that $\|w\|^2 = \int |w|^2 \, d \bold x$. If $w$ is real valued, then $|w|^2 = w^2$ and:

starting with (5): $\lambda_1 = \min\{ \frac{\|\nabla w\|^2}{\|w\|^2}\}$ $ = \min\{ \frac{\int |\nabla w|^2 d\bold x}{\int |w|^2 d\bold x }   \} = \min \{ \int |\nabla w|^2 d\bold x \}$ which is formula (9) 

##### Exercise 3

(**a**) $[w'(x) - \pi w(x)\cot (\pi x)]^2$ $ = w'(x)^2 - 2 \pi w'(x)w(x)\cot(\pi x) + \pi^2 w^2(x)\cot^2(\pi x)$ 

So we have: $\int_0^1[w'(x) - \pi w(x) \cot(\pi x)]^2 \, dx$ $=$ 

$\int_0^1 w'(x)^2 \, dx + \int_0^1[-2 \pi w'(x)w(x)\cot(\pi x)] + \int_0^1 \pi ^2 w^2 (x)\cot^2(\pi x)\, dx​$ 

and the cross term is: 

$-\pi \int_0^12 w'(x)w(x)\cot(\pi x)\, dx$ $ = -\pi \int_0^1 [w^2]'\cot(\pi x)\, dx$ 

$ =-\pi[ w^2 \cot(\pi x)|_0^1 +\pi \int_0^1 w^2 \csc^2(\pi x)]dx$ 

so we end up with: 

$\int_0^1[w'(x) - \pi w(x) \cot(\pi x)]^2 \, dx$ $=$ $ \int_0^1 [w'(x)]^2 \, dx - \pi [w^2 \cot (\pi x)]_0^1 - \pi ^2 \int_0^1 w^2(x)[\csc^2(\pi x) - \cot^2(\pi x)]\, dx$   

$  \int_0^1 [w'(x)]^2 \, dx - \pi [w^2 \cot (\pi x)]_0^1 - \pi ^2 \int_0^1 w^2(x)[\frac{1 - \cos^2(\pi x)}{\sin^2(\pi x)}]\, dx$  

and since $\frac{1 - \cos^2 \pi x}{\sin ^2 \pi x} = \frac{\sin^2 \pi x + \cos^2 \pi x - \cos^2 \pi x}{\sin^2 \pi x} = 1​$ we obtain: 

$\int_0^1[w'(x) = \pi w(x) \cot(\pi x)]^2 \, dx =  \int_0^1 [w'(x)]^2 \, dx - \pi [w^2 \cot (\pi x)]_0^1 - \pi ^2 \int_0^1 w^2(x) dx​$ 

**(b)** We have that $w(0) = w(1) = 0$ and so according to the book's hint: $\lim_{x \rightarrow} w(x)/(x-1)$ is finite and similarly, as $x \rightarrow 0$, $w(x)/x$ is finite as well. 

$w^2(x) \cot(\pi x)$$= w(x)(w(x)\frac{\cos(\pi x)}{\sin(\pi x)})$ . We want to show that $w \cdot \cot(\pi x)$ is finite, since if $a_n \rightarrow 0$ and $b_n \rightarrow L < \infin$, we have that $a_n b_n \rightarrow 0$  

We want to use that: $\cot(\pi x) = O(\frac{1}{1-x})$ as $x \rightarrow 1$ and $\cot(\pi x) = O(\frac{1}{x})$ as $x \rightarrow 0$ 

$\equiv$ there exists an $M$ and $\delta$ s.t. $|\cot(\pi x)| \leq M|\frac{1}{1-x}|$  whenever $0 < |x-1| <\delta$ and similalrly there exists an $M'$ and $\delta'$ s.t. $|\cot(\pi x)| \leq M' |\frac{1}{x}|$ whenever $0 < |x| <\delta'$ 

$\equiv$ $|\cot(\pi x)(1-x)| < \infin$ as $x \rightarrow 1$ and $|\cot(\pi x)x| < \infin$ as $x \rightarrow 0$ 

---

Proof: 

$\lim_{x \rightarrow 0} \frac{\cot(\pi x)}{\frac{1}{x}} = \lim_{x \rightarrow 0} \frac{x \cdot \cos(\pi x)}{\sin (\pi x)}​$ , we have $\lim_{x \rightarrow 0} x\cdot (\cos \pi x) = \lim_{x \rightarrow 0} \sin(\pi x) = 0​$ 
So using L'Hopital's, the limit is the same as: $\frac{\cos\pi x+ \pi x(-\sin \pi x) }{\pi \cos(\pi x)}​$ and letting $ x\rightarrow 0​$: 
$\frac{-1}{-\pi} = \frac{1}{\pi}​$, so $|x \cot(\pi x)| < \infin​$ as $x \rightarrow 0​$  

and similarly for $|\cot(\pi x)(1-x)|​$ as $x \rightarrow 1​$, we have a finite limit

---

and so we have (from Big O notation and properties) 

there exists an $M$ and $\delta$ s.t.: $|w \cdot \cot(\pi x)| \leq M| \frac{w}{x}| < \infin$ when $0 < |x| <\delta$  (i.e. whenever $x$ is arbitrarily close to $0$ $w\cdot \cot(\pi x)$ is finite) 

and similarly for $x \rightarrow 1$; (with different $M$ and $\delta$)

 $|w \cdot \cot(\pi x)| \leq M |\frac{w}{x-1}| < \infin$  when $0 < |x-1| < \delta$ (whenever $x$ is arbitrarily close to $1$, $w \cdot \cot(\pi x)$ is finite)

$\implies$ $w \cdot(w \cdot \cot) = 0$ whenever $x \rightarrow 0$ or $1$ since $w \cdot \cot$ is finite as $x \rightarrow 0$ or $1$ and $w = 0$ as $ x \rightarrow 0$ or $1​$  

**(c)**  From **(a)** we obtained: 

$\int_0^1[w'(x) -\pi w(x) \cot(\pi x)]^2 \, dx =  \int_0^1 [w'(x)]^2 \, dx - \pi [w^2 \cot (\pi x)]_0^1 - \pi ^2 \int_0^1 w^2(x) dx​$ 

and from $(b)$ this becomes

$ \int_0^1[w'(x)]^2 \, dx - 0 - \pi^2 \int_0^1 w^2(x)\, dx​$ 

so we have $\int_0^1[w'(x)]^2 \,dx - \pi^2 \int_0^1 w^2(x)\, dx$ $ = \int_0^1 [w'(x)- \pi w(x)\cot \pi x]^2\, dx$ $\geq 0$ 

**(d)** if $w(x) = \sin \pi x$ we have equality: 

$\int_0^1[w'(x)- \pi w(x)\cot \pi x]^2 \, dx$ $ = \int_0^1 (\pi \cos \pi x - \pi \sin (\pi x)\cdot \frac{\cos \pi x}{\sin \pi x})^2\, dx = 0$ 

so

 $\int_0^1[w'(x)]^2 \, dx = \int_0^1 [\pi \cos(\pi x)]^2 \, dx  = \pi^2 \int_0^1 \sin \pi x \, dx$  $ = \pi^2 \int_0^1 [w(x)]^2 \, dx$ 


so we have that the minimum of $\int_0^1[w'(x)]^2 \, dx = \pi^2 \int_0^1[w(x)]^2 \, dx$ among all functions s.t. $\int_0^1 [w(x)]^2 \,dx = 1$ must $ = \pi^2 \cdot 1$ $= \pi^2$  

##### Exercise 4

Show that $m^* = \lambda_n$ 

we have that

$m^* = \min \{ \frac{\| \nabla w\|^2}{\| w\|^2}: w \not \equiv 0, w = 0 \text{ on }\partial D, w \in C^2, 0 = (w, v_1 ) = \cdots = (w, v_{n-1}) \}$

From the proof of Theorem 2, we deduced that $-\Delta u = m^* u$ so that $m^*$ is an eigenvalue. 
But which eigenvalue is $m^*$? 

From here, I assume that we cannot find other eigenvalues smaller than $\lambda_{n-1} \geq \ldots \geq \lambda_1$

Remember that we can take any linearly independent eigenfunction and orthogonalize it using gram schmidt. 

Therefore, we can have a set of eigenfunctions $\{ v_1, v_2, … \}$ that are orthogonal to each other. 
And the eigenfunctions $v_j$ (with $j \geq n$ and $v_j \not \equiv 0$, and $v_j = 0$ on $\partial D$ and are in $C^2$ since $-\Delta v_j = \lambda_j v_j$ in $D$) are among the functions we consider for the set $S = \{ \frac{\\| \nabla w \|^2}{\| w\|^2}: w \not \equiv 0, w = 0 \text{ on } \partial D, w \in C^2, 0 = (w, v_1) = \cdots = (w, v_{n-1}) \}$

So **of the eigenvalues** $\{\lambda_n, \lambda_{n+1}, \ldots \}$ with eigenfunctions (that satisfy the requirements for $S$), $\lambda_n$, being the smallest of these eigenvalues, is the only one that can be the minimum:
because $m^*$ is a minimum of $S$ and using $(G1)$, we end up with

$m^* \leq \frac{\int |\nabla v_j|^2}{\int v_j^2} = \frac{ \int (-\Delta v_j)(v_j)}{\int v_j^2}  = \frac{\int (\lambda_j v_j)v_j}{\int v_j^2} = \lambda _j$ 

so $m^*$ is smaller than or equal to any eigenvalue with $j \geq n$, and since $m^*$ is an eigenvalue it must be equal to $\lambda_n$ 

we also have that $m^* \neq \lambda_j$ for $j = 1, \ldots, n-1$, since the corresponding eigenfunctions $v_j$ do not satisfy $(v_j, v_j) = 0$. 

##### Exercise 5

**(a)** Robin boundary condition $\partial u/\partial n + a(\bold x)u = 0$ 

WTS: $\lambda_1 = \min\{ \frac{\int_D |\nabla w|^2 d \bold x + \int_{\partial D} aw^2 \, dS}{\int_D w^2 \, d\bold x}: w \not \equiv 0, \partial w/\partial n + a(\bold x)w = 0 \text{ on } \partial D, w \in C^2 \}$ 

let $m​$ be the minimum value and let $u​$ be the function that "solves" this minum problem. 

so $m = \frac{\int_D |\nabla u|^2 d \bold x + \int_{\partial D}au^2 dS }{\int_D u^2 d \bold x} \leq \frac{\int_D |\nabla w|^2 d \bold x + \int_{\partial D}aw^2 dS }{\int_D w^2 d \bold x}​$

for all functions $w$ with $w \not \equiv 0, \partial w/\partial n + a(\bold x)w = 0 \text{ on } \partial D, w \in C^2 $ 

Let $v(\bold x)$ be another such function. and let $w(\bold x) = u(\bold x) + \epsilon v(\bold x)$, where $\epsilon$ is a constant. 

Then

$f(\epsilon) = \frac{\int_D|\nabla(u + \epsilon v) d \bold x + \int_{\partial D}a\cdot(u + \epsilon v)^2 dS}{\int_D|u + \epsilon v|^2 \, d \bold x}$ has a minimum at $ \epsilon = 0$, which means $f'(0) = 0$ 

expanding the squares:

$f(\epsilon) = \frac{\int_D(|\nabla u|^2 + 2 \epsilon \nabla u \cdot \nabla v + \epsilon^2 |\nabla v|^2)\, d \bold x + \int_{\partial D} a(u^2 + 2\epsilon uv + \epsilon^2v^2)dS}{\int_D u^2 + 2\epsilon u v + \epsilon^2v^2 d\bold x}$ 

From here I shorten $\int_D d\bold x \rightarrow \int_D$ and similalrly for $\int_{\partial D}$ 

$f'(0) = \frac{(\int_D u^2)(2\int_D \nabla u \cdot \nabla v+ 2\int_{\partial D} auv) -(\int_D|\nabla u|^2 + \int_{\partial D}au^2)(2\int_D uv) }{(\int_D u^2)^2} = 0$

so $\int_D \nabla u \cdot \nabla v  + \int_{\partial D} auv= \frac{\int_D |\nabla u|^2 + \int_{\partial D}au^2}{\int_D u^2}\int_D uv = m \int uv​$

and by Green's first identity $\int_{\partial D} v \frac{\partial u}{\partial n} \, dS = \int_D \nabla v \cdot \nabla u \, d\bold x + \int_D v \Delta u \, d\bold x$ 

$\int_{\partial D} v(au + \frac{\partial u}{\partial n}) = \int_D v(\Delta u + mu)$

and since $au + \frac{\partial u}{\partial n} = 0$ by the boundary condition:

$ \int_D v(\Delta u + mu) d\bold x= 0$ 

And this is valid for all function $v$ satisfying $v \not \equiv 0$, $\partial v/\partial n + a(\bold x)v$ on $\partial D$, $v \in C^2$ and so $v(\bold x)$ can be chosein in an aribtrary way inside $D$ 

so we have $\Delta u + mu = 0$ in $D$ 

therefore $m$ is an eigenvalue of $-\Delta$ and $u(\bold x$) is its eigenfunction

$m$ is the smallest eigenvalue: let $\lambda_j$ be any eigenvalue with $-\Delta v_j = \lambda_j v_j$ 

$m \leq \frac{\int_D |\nabla v_j|^2 d \bold x + \int_{\partial D}av_j^2 dS }{\int_D v_j^2 d \bold x} = \frac{\int_D(-v_j\Delta v_j )d\bold x + \int_{\partial D}av_j^2 + v_j \frac{\partial v_j}{\partial n}dS}{\int _D v_j^2 \, d \bold x}$ 

$ = \frac{\int_D \lambda_j v_j^2 d \bold x + \int_{\partial D}v_j(av_j + \partial v_j /\partial n)dS}{\int_D v_j^2 d \bold x}$ 

and since $v_j$ is an eigenfunction satisfying the Robin BC: $av_j + \partial v_j /\partial n = 0$ 

we have $m \leq \frac{\int_D \lambda_j v_j^2 d \bold x}{\int_D v_j^2 d \bold x} = \lambda_j$ so $m$ is the smallest eigenvalue. 

#### Section 11.2: Exercises 1, 2, 3, 7

##### Exercise 1

$-u'' = \lambda u$ in the interval $(0, 1)$, with $u(0) = u(1)= 0$, choose pair of trial functions $x-x^2$ and $x^2 - x^3$ and compute the Rayleigh-Ritz approximation to the first two eigenvalues. Compare with the exact values. 

---

$\nabla(x-x^2) = 1 - 2x$, $\nabla(x^2 - x^3) = 2x - 3x^2$ 
$A = \begin{pmatrix} \int_0^1 1 - 4x + 4x^2 \, dx & \int_0^1 2x - 7x^2 +6x^3\, dx  \\ \int_0^1 2x - 7x^2 + 6x^3 \, dx & \int_0^1 4x^2 - 12x^3 + 9x^4  \end{pmatrix} $ $ = \left(
\begin{array}{cc}
 \frac{1}{3} & \frac{1}{6} \\
 \frac{1}{6} & \frac{2}{15} \\
\end{array}
\right)$ 

$B = \begin{pmatrix} \int_0^1 x^2 - 2x^3 + x^4 \, dx & \int_0^1 x^3 -2x^4 + x^5 \, dx \\ \int_0^1 x^3 - 2x^4 + x^5 \, dx & \int_0^1 x^4 - 2x^5 + x^6\end{pmatrix}​$ 

$ = \left(
\begin{array}{cc}
 \frac{1}{30} & \frac{1}{60} \\
 \frac{1}{60} & \frac{1}{105} \\
\end{array}
\right)$ 

$A - \lambda B = \begin{pmatrix} \frac{1}{3} - \frac{\lambda}{30} & \frac{1}{6} - \frac{\lambda}{60} \\ \frac{1}{6} - \frac{\lambda }{60} & \frac{2}{15} - \frac{\lambda}{105} \end{pmatrix}$ 

$\det(A - \lambda B) = \frac{1}{60} - \frac{13 \lambda}{6300} + \frac{\lambda^2}{25200} = 0$ 

$\frac{13/6300 \pm \sqrt{13^2/6300^2 - 1/(15*25200)}}{2/25200}$ 

$+$: 42, -: 10

$\lambda_1 \sim 10, \lambda_2 \sim 42​$ 

##### Exercise 2

Do the same with: $w_1(x) = x-x^2$, 

$(0, 0)$, $(1/4, 1)$, $(3/4, -1)$, $(1, 0)$ 

first slope: $\frac{1}{1/4} = 4$ so for $0 \leq x \leq \frac{1}{4}$: $4x$ 
Second slope: $\frac{-2}{1/2} = -4$ so $(w_2(x) - 1) = -4(x - 1/4)$ $w_2(x) = -4x + 2$ 

Third slope: $\frac{1}{1/4} = 4$, so $(w_2(x)+1) = 4(x - 3/4)$ $w_2(x) = 4x - 4$ 

$w_2(x) = \begin{cases} 4x & 0 \leq x \leq \frac{1}{4} \\ -4x + 2 & \frac{1}{4}\leq x \leq \frac{3}{4} \\ 4x - 4 & \frac{3}{4}\leq x \leq 1 \end{cases}$ 

$\nabla w_1 = 1 - 2x$, $\nabla w_2 = \begin{cases} 4 & 0 \leq x \leq \frac{1}{4} \\ -4 & \frac{1}{4} \leq x \leq \frac{3}{4} \\ 4 & \frac{3}{4} \leq x \leq 1 \end{cases}$ 

$A = \begin{pmatrix} \int_0^1 1 - 4x + 4x^2 \, dx & \int_0^{1/4} 4-8x \, dx + \int_{1/4}^{3/4} -4 + 8x \, dx + \int_{3/4}^1 4-8x\, dx \\ \int_0^{1/4} 4-8x \, dx + \int_{1/4}^{3/4} -4 + 8x \, dx + \int_{3/4}^1 4-8x\, dx & \int_0^116 \, dx \end{pmatrix}$

$= \begin{pmatrix} \frac{1}{3} & 0 \\ 0 & 16 \end{pmatrix}$ 

$B = \begin{pmatrix} \int_0^1 x^2 - 2x^3 +x^4 \, dx \\ \int_0^{1/4} 4x^2 - 4x^3 \, dx + \int_{1/4}^{3/4} (-4x + 2)(x-x^2) \, dx + \int_{3/4}^{1} (4x - 4)(x - x^2) \, dx \\ \int_0^{1/4} 4x^2 - 4x^3 \, dx + \int_{1/4}^{3/4} (-4x + 2)(x-x^2) \, dx + \int_{3/4}^{1} (4x - 4)(x - x^2) \, dx \\ \int_0^{1/4}16x^2 \,dx + \int_{1/4}^{3/4}(-4x + 2)^2\, dx + \int_{3/4}^1 (4x-4)^2 \, dx \end{pmatrix}$ 

(since it couldnt' fit in one page it goes $b_{11}, b_{12}, b_{21}, b_{22}$) 

$[\frac{4}{3}x^3 - x^4]_0^{1/4} + [x^2 -2x^3 +x^4]_{1/4}^{3/4}$ $+ [-2x^2 + \frac{8}{3}x^3 - x^4 ]_{3/4}^1$ 
$= 0$ 
$\int_0^{1/4}16 x^2 \, dx = \frac{16}{3}(\frac{1}{4})^3 $
$\int_{1/4}^{3/4} 16x^2 -16 x + 4 \, dx $ $= \frac{16}{3}(\frac{3}{4})^3 - 8 (\frac{3}{4})^2 + 4(\frac{3}{4})$ $ - \frac{16}{3}(\frac{1}{4})^3 + 8 (\frac{1}{4})^2 - 4(\frac{1}{4})$ 
$\int_{3/4}^1 16x^2 - 32 x + 16 \, dx$ $ = \frac{16}{3} + (-16 + 16) - \frac{16}{3}(\frac{3}{4})^3 + 16(\frac{3}{4})^2 - 16 \frac{3}{4}​$ 

$= \begin{pmatrix} \frac{1}{30} & 0 \\ 0 & \frac{1}{3} \end{pmatrix}$  

$A - \lambda B $ $= \begin{pmatrix} \frac{1}{3}- \frac{\lambda}{30} & 0 \\ 0 & 16 - \frac{\lambda}{3} \end{pmatrix}$ 
Determinant: $\frac{16}{3} - \frac{16 \lambda}{30} - \frac{\lambda}{9} + \frac{\lambda^2}{90}$ $= \frac{1}{90}\lambda^2 - \frac{29}{45}\lambda + \frac{16}{3}$ 

$\lambda = \frac{(29/45) \pm \sqrt{29^2/45^2 - 4(1/90)(16/3)}}{1/45}$ 

$\lambda_1 \sim 10$, $\lambda_2 \sim 48$ 

##### Exercise 3

Consider $-\Delta$ in the square $(0, \pi)^2$ with Dirichlet BCs. Compute the Rayleigh quotient with the trial function $xy(\pi -x )(\pi -y)$. Compare with the first eigenvalue. 

Trial function expanded: $xy(\pi^2 - \pi x - \pi y + xy)$ $= x^2y^2 - \pi xy^2 - \pi x^2y + \pi^2 xy$ 

so maybe we have $u(0, y) = u(\pi, y) = u(x, 0) = u(x, \pi) = 0$ ? 

$\nabla(xy(\pi - x)(\pi - y)) = (2xy^2 - \pi y^2 -2 \pi xy + \pi^2 y, 2x^2y -2\pi xy - \pi x^2 + \pi ^2 x)$

and then we obtain for one integral

$\int_0^\pi \int_0^\pi (2xy^2 - \pi y^2 - 2\pi xy + \pi^2 y)^2 + (2x^2 y - 2\pi x y - \pi x^2 + \pi^2 x)^2 \, dx \, dy$ 

$ = \int_0^\pi \int_0^\pi 4 x^4 y^2-4 \pi  x^4 y+\pi ^2 x^4-8 \pi  x^3 y^2+8 \pi ^2 x^3 y-2 \pi ^3 x^3+4 x^2 y^4-8 \pi  x^2 y^3+8 \pi ^2 x^2 y^2 \\-4 \pi ^3 x^2 y+\pi ^4 x^2-4 \pi  x y^4+8 \pi ^2 x y^3-4 \pi ^3 x y^2+\pi ^2 y^4-2 \pi ^3 y^3+\pi ^4 y^2\, dx \,dy​$ $\int_0^\pi [\frac{4 x^5 y^2}{5}-\frac{4}{5} \pi  x^5 y+\frac{\pi ^2 x^5}{5}-2 \pi  x^4 y^2+2 \pi ^2 x^4 y-\frac{\pi ^3 x^4}{2}+\frac{4 x^3 y^4}{3}-\frac{8}{3} \pi  x^3 y^3+\frac{8}{3} \pi ^2 x^3 y^2-\frac{4}{3} \pi ^3 x^3 y \\ +\frac{\pi ^4 x^3}{3}-2 \pi  x^2 y^4+4 \pi ^2 x^2 y^3-2 \pi ^3 x^2 y^2+\pi ^2 x y^4-2 \pi ^3 x y^3+\pi ^4 x y^2]_{x=0}^\pi \, dy​$  $ = \int_0^\pi \frac{1}{5} \pi ^5 \left(4 y^2-4 \pi  y+\pi ^2\right)+\frac{1}{4} \pi ^4 \left(-8 \pi  y^2+8 \pi ^2 y-2 \pi ^3\right)+\frac{1}{3} \pi ^3 \left(4 y^4-8 \pi  y^3+8 \pi ^2 y^2-4 \pi ^3 y \\ +\pi ^4\right)+\pi  \left(\pi ^2 y^4-2 \pi ^3 y^3+\pi ^4 y^2\right)+\frac{1}{2} \pi ^2 \left(-4 \pi  y^4+8 \pi ^2 y^3-4 \pi ^3 y^2\right) \, dy​$ $= [\frac{1}{30} \pi ^3 \left(2 y^5-5 \pi  y^4+\frac{14 \pi ^2 y^3}{3}-2 \pi ^3 y^2+\pi ^4 y\right)]_0^\pi​$ 

$= \frac{\pi^8}{45}​$ 

and for the other:

$\int_0^\pi \int_0^\pi (x^2y^2 - \pi x y^2 - \pi x^2 y + \pi ^2 xy)^2\, dx \,dy$  
$\int_0^\pi \int_0^\pi x^4 y^4-2 \pi  x^4 y^3+\pi ^2 x^4 y^2-2 \pi  x^3 y^4\\ +4 \pi ^2 x^3 y^3-2 \pi ^3 x^3 y^2+\pi ^2 x^2 y^4-2 \pi ^3 x^2 y^3+\pi ^4 x^2 y^2 \, dx \,dy$ 

$ = \int_0^\pi [\frac{x^5 y^4}{5}-\frac{2}{5} \pi  x^5 y^3+\frac{1}{5} \pi ^2 x^5 y^2-\frac{1}{2} \pi  x^4 y^4+\pi ^2 x^4 y^3\\-\frac{1}{2} \pi ^3 x^4 y^2+\frac{1}{3} \pi ^2 x^3 y^4-\frac{2}{3} \pi ^3 x^3 y^3+\frac{1}{3} \pi ^4 x^3 y^2]_{x = 0}^{\pi} \, dy$ 

$= \int_0^{\pi}\frac{1}{5} \pi ^5 \left(y^4-2 \pi  y^3+\pi ^2 y^2\right)+\frac{1}{3} \pi ^3 \left(\pi ^2 y^4-2 \pi ^3 y^3\\+\pi ^4 y^2\right)+\frac{1}{4} \pi ^4 \left(-2 \pi  y^4+4 \pi ^2 y^3-2 \pi ^3 y^2\right)\, dy$ 

$= [\frac{1}{30} \pi ^5 \left(\frac{y^5}{5}-\frac{\pi  y^4}{2}+\frac{\pi ^2 y^3}{3}\right)]_0^\pi$ 

$= \frac{\pi^{10}}{900}$ 

Rayleigh quotient: $\frac{900}{45}\frac{\pi^8}{\pi^{10}}$ $= 20/\pi ^2$  $\sim 2.026$ 
First eigenvalue: From section 10.4, the first eigenvalue is $2$ with eigenfunction $A \sin x \sin y$: 

$-\Delta(A \sin x \sin y) = -(-A\sin x \sin y - A \sin x \sin y) = 2A\sin(x+y)$ 

so this gave an ok approximation, error within $10^{-1}​$  

##### Exercise 7

**(a)** What is the exact value of the first eigenvalue of $-\Delta​$ in the unit disk with Dirichlet BCs?

From earlier in this section, the first eigenvalue is going to a square of one of the roots of the Bessel function $J_o$ 

$J_o(\rho) = \sum_{j=0}^\infin (-1)^j \frac{(\frac{1}{2}\rho)^{2j}}{j!*j!}$ 

and the value for this is 5.783 (which also comes from 11.2)

**(b)** 

$\nabla(1-r) = (-1, 0)​$ 

$\frac{d(x,y)}{d(r, \theta)}  = \begin{pmatrix} \cos \theta & -r \sin \theta \\ \sin \theta  & r \cos \theta \end{pmatrix}$

Determinant: $r\cos^2 + r \sin ^2 = r$ 

so $dx dy$ becomes $r \, dr \,d\theta$ when we change variables of integration. 

and since our trial function doesn't depend on $\theta​$, we then have:

so $2\pi \int_0^1(-1)^2 \cdot  r \, dr = \pi$ 

$2 \pi \int_0^1 (1- r)^2 r\, dr ​$ = $2 \pi \int_0^1 r - 2r^2 + r^3 \, dr​$=  $2\pi [\frac{1}{2} - \frac{2}{3} + \frac{1}{4}]​$ $ = \frac{\pi}{6}​$ 

$Q = \frac{\pi}{\pi/6} = 6$ , which is kinda close, the absolute error is around 0.21

#### Section 11.3: Exercises 2, 3

##### Exercise 2

Let $f(\bold x)$ be a function in $D$ and $g(\bold x)$ a function on $\partial D$. Consider the minimum of the functional
$$
\frac{1}{2}\iiint_D |\nabla w|^2\, d \bold x - \iiint_D f\, w\, d \bold x- \iint_{\partial D}g\, w\, dS
$$
among all $C^2$ functions $w(\bold x)$ for which $\iiint_D \, w^2 \, d \bold x =1$. Show that a solution of this minimum problem leads to a solution of the Neumann problem
$$
-\Delta u = f\quad \text{in }D, \quad \frac{\partial u}{\partial n}= g \text{ on }\partial D
$$

---

the solution to the minimum problem is a $C^2$ function $u$ such that

$m = \frac{1}{2}\int_D|\nabla u|^2\, d \bold x - \int_D f\, u\, d \bold x - \int_{\partial D}g \, u\, dS$ $\leq \frac{1}{2}\int_D |\nabla w|^2\, d \bold x - \int_D f\, w\, d\bold x- \int_{\partial D}g\, w \, dS$ 

where $m$ is the minimum value. 

Let $v(x)$ be another $C^2$ function s.t. 

$\int_D 2\epsilon u v + \epsilon ^2 v^2\, d \bold x =0 $ $(*)$

That way we still have $\iiint_D (u + \epsilon v)^2\,d\bold x = 1$

Define a funciton $f^*​$ s.t.

$f^*(\epsilon) = \frac{1}{2}\iiint_D |\nabla(u + \epsilon v)|^2 \, d \bold x - \iiint_D f (u + \epsilon v)\, d \bold x - \iint_{\partial D}g (u + \epsilon v)\, dS$ 

$= \frac{1}{2}\iiint_D |\nabla u|^2 + 2 \epsilon \nabla u \cdot \nabla v + \epsilon^2 |\nabla v|^2 \, d\bold x - \iiint_D f\, (u + \epsilon v)\, d \bold x - \iint_{\partial D}g\, (u +\epsilon v)\, dS$

$0=f^{*'}(0) = \iiint_D  \nabla u \cdot \nabla v \, d \bold x - \iiint_D f \, v \, d \bold x - \iint_{\partial D} g\, v \, d S$ 

$\iiint_D \nabla u \cdot \nabla v\, d \bold x = \iiint_D f\, v \, d \bold x + \iint_{\partial D} g\, v \, dS​$ 

$\iint_{\partial D} v \frac{\partial u}{\partial n}\, dS - \iiint_D v \Delta u\, d \bold x = \iiint_D f\, v \, d\bold x + \iint_{\partial D}g\, v \, dS​$ 

$\iiint_{D}v\, [-\Delta u - f]\, d \bold x + \iint_{\partial D} v\, [ \frac{\partial u}{\partial n} - g]\, dS= 0$ 

and this has to work **for every function** $v$ that satisfies $(*)$, which is only a restriction of $v$ in $D$, so $v$ can be arbitrary on $\partial D$: 

therefore: it wouldn't be possible to somehow have $\iiint_D v \, [-\Delta u - f]\, d \bold x = - \iint_{\partial D}v\, [\frac{\partial u}{\partial n}- g]\, dS$ for **every function $v$** that satisfies $(*)$ and can be arbitary on $\partial D$ 

 we must have $-\Delta u - f = 0$ in $D$ and $\frac{\partial u}{\partial n} - g = 0$ in $\partial D$ which means $u$ is a solution of the Neumann problem.

##### Exercise 3

Let $g(\bold x)$ be a function on $\partial D$. Consider the minimum of the functional
$$
\frac{1}{2} \iiint_D |\nabla w|^2 \, d \bold x - \iiint f\, w \, d \bold x
$$
among all $C^2$ functions $w(\bold x)$ for which $w = g$ on $\partial D$. Show that a solution of this minimum problem leads to a solution of the Dirichlet problem
$$
-\Delta u = f \quad \text{in D}\, \qquad u = g\quad \text{on }\partial D
$$

---

Note the solution to this MP must already satisfy $u = g$ on $\partial D$ so what's left to show is that $-\Delta u = f$ in $D$ 

Following the same method as before:

but let $v = 0$ on $\partial D$ so that $u + \epsilon v$ still satisfies $u + \epsilon v$ = $g$ on $\partial D$ 

$f^*(\epsilon) = \frac{1}{2}\iiint_D |\nabla (u + \epsilon v)^2\, d\bold x - \iiint_D f\, (u + \epsilon v)\, d \bold x$ 

$0 = f^{*'}(0) = \iiint_D \nabla u \cdot \nabla v\, d \bold x - \iiint_D f\, v \, d \bold x$ 

Then using G1 and the fact that $v = 0$ on $\partial D$ 

$\implies$ $\iiint_D v\, (\Delta u - f)\, d \bold x = 0$ 

since this must be the case for all $v$ satisfying $v = 0$ on $\partial D$, $v$ can be arbitrary in $D$ and therefore, along with the vanishing theorem so that $v (\Delta u - f) = 0$, $\Delta u - f = 0$ $\implies$ $\Delta u = f$ 

#### Section 10.1: Exercises 4, 6

##### Exercise 4

Consider the eigenvalue problem $-\Delta v = \lambda v$ in the unit square $D = \{ 0 <x < 1, 0 < y < 1\}$ with the Dirichlet BC $v = 0$ on the bottom and both vertical sides, and the robin BC $\partial v/\partial y = -v$ on the top $\{ y = 1 \}$ 

**(a)** Show that all the eigenvalues are positive

On the parts of the square with the homogeneous Dirichlet BC we have that for any eigenfunction $v$, from G1:
$$
\iiint_D (-\Delta v)\overline v \, d \bold x = \iiint_D |\nabla v|^2\, d \bold x - \iint_{\partial D}\overline{v}\frac{\partial v}{\partial n}\, dS
$$
Since $v = 0$, and using that $v$ is an eigenfunction:
$$
\lambda \iiint_D |v|^2\, d \bold x = \iiint_D |\nabla v|^2\, d \bold x \geq 0
$$
and we know from page 262 that the last integral cannot be zero else $v$ would be the 0 function, so $\lambda > 0$ 

The normal vector on the side with the robin BC should be a vector pointing straight up, so $(0, 1)$ which means $(0, 1)\cdot (v_x, v_y) = v_y$ 

so using G1 again, and using that $\partial v/\partial n = \partial v/\partial y = -v $ 
$$
\lambda \iiint_D |v|^2\, d \bold x = \iiint_D|\nabla v|^2\, d\bold x - \iint_{\partial D} (v)(-v)  \, dS
\\ = \iiint_D |\nabla v|^2 \, d \bold x + \iint_{\partial D}|v|^2\, dS \geq 0
$$
so similalry as before, if we have the right side = 0, it would mean that both $|\nabla v|^2 = 0$ and $|v|^2 = 0$  (as both are $\geq 0$)

so $\implies$ $v \equiv 0$ which cannot be an eigenfunction.

So $\lambda > 0$ 

**(b)** Find an equation for the eigenvalues $\lambda$. Show that they can be expressed in terms of the roots of the equation $s + \tan s = 0$ 

Since the sides of the square are parallel to the axes: $v = X(x)Y(y)$ 
$$
\frac{X''}{X} + \frac{Y''}{Y} = -\lambda
$$
$\frac{X''}{X} = -\lambda - \frac{Y''}{Y}​$ 

since both sides depend on different variables:

$\frac{X''}{X} = -\lambda - \frac{Y''}{Y} = \mu$, a constant

so we have $X'' = \mu X$ 

if $\mu$ is positive: then let $\sqrt{\mu} = \alpha$ 

$X(x) = C_1 e^{\alpha x}+ C_2 e^{-\alpha x}$ which can be rewritten in terms of $\cosh $ and $\sinh$: 

$X(x) = D_1 \cosh \alpha x + D_2 \sinh \alpha x$ 

$\implies$ $X(0) = D_1 = 0$ 

$X(1) = 0$ $\implies$ $D_2 \sinh \alpha = 0$ we would need $D_2 = 0$ since $\sinh \alpha \neq 0$ unless $\alpha = 0$, and in either case we still get $X(x)\equiv 0$ 

so $\mu$ is not positive

Suppose $\mu = 0$: 

Then $X'' = 0$ $\implies$ $X(x) = Ax + B$ 

$X(0) = B = 0$, $X(1) = A = 0$ so we still get the trivial solution

Now suppose $\mu = -\beta^2$, for some real $\beta$ 

then we get $X(x) = C_1 \cos \beta x + C_2 \sin \beta x$ 

$X(0) = 0 = C_1 $ and $X(1) = C_2 \sin \beta = 0$, and we don't want $C_2 = 0$ so $\sin \beta =0 $ $\implies$ $\beta = \pi n$ for $n = 1, 2, 3, …$ 

so $\mu = -\pi^2n^2$ so we obtain

$-\frac{Y''}{Y} = -\pi^2n^2 + \lambda$ $\equiv$ $\frac{Y''}{Y} = \pi^2n^2 - \lambda$ for $n = 1, 2, 3, …$ 

so $Y'' = (\pi^2n^2 - \lambda)Y$ 

we know that $\lambda > 0$ and suppose $\pi^2 n^2 - \lambda > 0$ :

$Y(y) = C_1 \cosh \sqrt{\pi^2 n^2 - \lambda} y + C_2 \sinh \sqrt{\pi^2 n^2 - \lambda}y$ 

$Y(0) = C_1 = 0$, and $Y'(1) = -Y(1)$

$Y'(1) = C_2 \cosh \sqrt{\pi^2 n^2 - \lambda} $ $= -C_2 \sinh \sqrt{\pi^2 n^2 - \lambda }$ which is only true if $C_2 = 0$, which gives $Y \equiv 0$ 

Suppose now that $\pi^2n^2 - \lambda = 0$ then we obtain:

$Y(y) = Ay + B$, and $Y(0) = 0$ so $B = 0$ 

$Y'(1) = A$ and $Y'(1) = A = -A = -Y(1)$, which is only true if $A = 0$, so we end up with $Y \equiv 0$ 

So $\pi^2 n^2 - \lambda < 0$: 

$Y(y)=C_1 \cos \sqrt{-(\pi^2n^2 - \lambda)}y + C_2 \sin \sqrt{-(\pi^2n^2 - \lambda)}y$ 

$Y(0) = C_1 = 0$ 

$Y'(y) = -C_1 \sqrt{-(\pi^2n^2 - \lambda)}  \sin \sqrt{-(\pi^2n^2 - \lambda)}y + C_2 \sqrt{-(\pi^2 n^2 - \lambda)}\cos\sqrt{-(\pi^2n^2 - \lambda)} y​$ 

$Y'(1) = -C_1 \sqrt{-(\pi^2n^2 - \lambda)}\sin \sqrt{-(\pi^2n^2 - \lambda)} + C_2 \sqrt{-(\pi^2 n^2 - \lambda)}\cos \sqrt{-(\pi^2n^2 - \lambda)}​$ $ = -C_1 \cos \sqrt{-(\pi^2 n^2 - \lambda)} - C_2 \sin \sqrt{-(\pi^2 n^2 - \lambda)}​$ $= - Y(1)​$ 

and $C_1 = 0$ and dividing by $C_2$ we obtain:

$\sqrt{-(\pi^2n^2 - \lambda)}\cos \sqrt{-(\pi^2 n^2 - \lambda)} = - \sin \sqrt{-(\pi^2 n^2 - \lambda)}$ 

let $s = \sqrt{-(\pi^2 n^2 - \lambda)}$  and dividing both sides by $\cos s$ 

$s = - \tan s​$ $\implies​$ $s + \tan s = 0​$ 

so $\lambda = s^2 + \pi^2 n^2$ where $s$ is a solution to $s + \tan s$ 

**(c)** Find the solutions of the last equation graphically. Find an approximate formula for the (m,n)th eigenvalue for large (m,n)

![image-20190421060945055](Math 126 Homework 10.assets/image-20190421060945055.png)

Once $x \rightarrow \infin$, the zeros of $x + \tan x$ occur approximately when $\tan x$ is not defined, i.e., when $\cos x = 0$, so when $x = \frac{\pi}{2}(2m + 1)$ for integer $m$ 

so $\lambda = [\frac{\pi}{2}(2m+1)]^2 + \pi^2 n^2$ for large $m, n$ 

##### Exercise 6

![image-20190421061202987](Math 126 Homework 10.assets/image-20190421061202987.png)

so as seen above, $w - \text{proj}_u w$ is orthogonal to $u$ 

![image-20190421061247716](Math 126 Homework 10.assets/image-20190421061247716.png)

And the same occurs in 3D space
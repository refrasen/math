# Math 126 Midterm Practice Problems

## Problem 1

$u_t + 3xu_x = 0$ $ = (1, 3x) \cdot \nabla u = 0$ so  we have that on the curves

with $\frac{dx}{dt} = \frac{3x}{1}$, $u$ is constant:

$\ln x = 3t + c$ $ = x = Ce^{3t}$ 

so $\frac{d}{dt}u(Ce^{3t}, t) = 3Ce^{3t}u_x + u_t = 3xu_x + u_t = 0$ 

and since the curves $x = Ce^{3t}$ cover the entire $(x,t)$ plane

and $u$ is constant along these curves, which are uniquely determined by the constant $C$, where $C = xe^{-3t}$ , so $u$ is a function of $xe^{-3t}$, which is a single variable(?)

$u = f(xe^{-3t})$ ...

$u(x, 0) = \sin(x)$, and so $u(x, 0) = f(x) = \sin(x) = \sin(xe^{-3t})$ 

**Find a better way to show the above**

$u(x, t) = \sin(xe^{-3t})$, $u_t = -3x^{-3t} \cos(xe^{-3t})$, $u_x = e^{-3t}\cos(xe^{-3t})$ 

so this is a solution… wow 

## Problem 2

Fourier sine coefficients for a function $f(x)$
$$
A_n = \frac{2}{l}\int_0^l f(x)\sin \frac{n \pi x}{l}\, dx
$$
so $l = \pi$, $f(x) = x^2$ 
$$
A_n = \frac{2}{\pi}\int_0^\pi x^2 \sin nx \, dx
\\ = \frac{2}{n\pi}[-x^2\cos nx|_0^\pi + \int_0^\pi2x \cos nx \, dx]
\\ = \frac{2}{n\pi}[-x^2\cos nx + \frac{2x}{n}\sin nx - \frac{2}{n}\int_0^\pi \sin nx \, dx]
\\ = \frac{2}{n\pi}[-\pi^2 (-1)^n + \frac{2}{n^2}(-1)^n - \frac{2}{n^2}]
$$
if $n$ odd: $A_n = \frac{2}{n\pi}[\pi^2 - \frac{4}{n^2}]$ 

else: $A_n = -\frac{2}{n}\pi$ 

$f(x)$ is $C^\infin$, and so by the theorem relating to uniform convergence, like theorem 2 or 3? The fourier sine series for $f(x)$ converges uniformly

## Problem 3***

Suppose $u = u(x,t)$ solves 
$$
\begin{align}
u_t - u_{xx} = f(u) && 0 <x<l, t \geq 0
\\ u(0, t) = u(l, t) = 0 && t \geq 0
\end{align}
$$
**~~Had no idea how to solve this one~~**: 

$\frac{d}{dt} \int_0^l \frac{1}{2}u_x^2(x, t) + F(u(x,t)) \, dx \leq 0​$ 

Checking out problem 4.1.6

Trying Separation of variables?

$u(x,t) = X(x)T(t)$ Gives

$X(x)T'(t) - X''(x)T(t) = f(u)$ 

---

$\int_0^l u_{xt}u_x - f(u)u_t \; dx$ $ = \int_0^l u_{xt}u_x - (u_t - u_{xx})u_t \, dx$ $ = \int_0^l  u_{xt}u_x - u_t^2 + u_{xx} u_t \, dx$ 

---

$\frac{d}{dt}\int_0^l \frac{1}{2} u_x^2(x,t) + F(u(x,t)) \, dx$ $ = \int_0^l \frac{1}{2}[u_{xt}u_x + u_x u_{xt}] - f(u)\cdot u_t, dx$ 

$ = \int_0^l u_xu_{xt} - u_t^2 + u_t u_{xx} \, dx$ 

$(u_xu_t)_x = u_{xx}u_t + u_xu_{tx}$ 

$\implies$ $\int_0^l (u_x u_t)_x - u_t^2 \; dx$ $ = [u_x u_t]_0^l - \int_0^l u_t^2 \, dx$ 

$ = u_x(l, t)u_t(l, t) - u_x(0, t)u_t(0, t) - \int_0^l (u_t)^2 \, dx$ 

and since $u(l,t) = 0 \implies u_t(l, t) = 0$ and $u(0, t) = 0 \implies u_t(0, t) = 0​$ so we have

 $- \int_0^l (u_t)^2 \, dx \leq 0$ 

## Problem 4

Let $u = u(x,y)$ be a solution to the eigenvalue problem
$$
\begin{cases} 
u_{xx} +u_{yy} = -\lambda u & \text{for }x^2 + y^2 < 1
\\ u= 0 & \text{for }x^2 + y^2 = 1
\end{cases}
$$
where $\lambda > 0$ 

### 1st part

State the polar form of the 2-dimensional Laplace operator

$r = \sqrt{x^2 + y^2}$, $\frac{dr}{dx} = \frac{x}{r} = \cos \theta$ , $\frac{dr}{dy} = \sin \theta$ 

$\theta = \arctan (\frac{y}{x})$ $\frac{d\theta}{d x} = \frac{1}{{1 + y^2/x^2}}\cdot -\frac{1}{x^2} = -\frac{1}{-x^2 -y^1} - \frac{y}{r^2} = -\frac{\sin \theta}{r}$ 

$\frac{d\theta}{dy} = \frac{1}{1 + y^2/x^2} \cdot \frac{1}{x}\cdot \frac{x}{x} = \frac{x}{r^2} = \frac{\cos \theta}{r}$ 

consider $u(r, \theta)​$ , $r = \sqrt{x^2 + y^2}​$, 

$u_x = u_r \frac{\partial r}{\partial x} + u_\theta \frac{\partial \theta}{\partial x}$ $ = u_r \cos \theta - u_\theta \frac{\sin \theta}{r}$ 

**Scratch this**! 

**Instead use operators** 

$\frac{\partial}{\partial x} = \cos \theta\frac{\partial}{\partial r}  - \frac{\sin \theta}{r}\frac{\partial}{\partial \theta} $

$\frac{\partial }{\partial y} = \sin \theta \frac{\partial }{\partial r} + \frac{\cos \theta}{r} \frac{\partial }{\partial \theta}$ 

do a lot of work:

$(\frac{\partial}{\partial x})^2 = \cos \theta \frac{\partial }{\partial r}(\cos \theta \cdot \frac{\partial }{\partial r} - \frac{\sin \theta}{r} \cdot \frac{\partial }{\partial \theta} ) - \frac{\sin \theta}{r} \frac{\partial }{\partial \theta}(\cos \theta \cdot \frac{\partial }{\partial r} - \frac{\sin \theta}{r} \cdot \frac{\partial }{\partial \theta} )$ $ = \cos \theta [\cos \theta \frac{\partial^2 }{\partial r^2} + \frac{\sin \theta}{r^2} \frac{\partial }{\partial \theta} - \frac{\sin \theta}{r} \frac{\partial ^2}{\partial r \partial \theta}] - \frac{\sin \theta}{r}[ - \sin \theta \frac{\partial }{\partial r} + \cos \theta \frac{\partial ^2}{\partial r \partial \theta} - \frac{\cos \theta}{r} \frac{\partial }{\partial \theta} - \frac{ \sin \theta}{r} \frac{\partial ^2}{\partial \theta^2}]$ $ = \cos^2 \theta \frac{\partial ^2}{\partial r^2} + 2\frac{ \cos \theta \sin \theta}{r^2} \frac{\partial }{\partial \theta} -2 \frac{\cos \theta \sin \theta}{r} \frac{\partial ^2}{\partial r \partial \theta} + \frac{\sin^2\theta}{r} \frac{\partial }{\partial r} + \frac{\sin^2\theta}{r^2} \frac{\partial ^2}{\partial \theta^2}$ 

either way:
$$
\Delta_2 = \frac{\partial ^2}{\partial r^2} + \frac{1}r \frac{\partial }{\partial r} + \frac{1}{r^2}\frac{\partial ^2}{\partial \theta^2}
$$

### 2nd part

$u(r, \theta) = R(r)\Theta(\theta)$ We must have:

$R''(r)\Theta(\theta) + \frac{1}{r} R'(r)\Theta(\theta) + \frac{1}{r^2}R(r)\Theta''(\theta) = -\lambda R(r)\Theta(\theta)$ and divide by $R(r)\Theta(\theta)$ 

$\frac{R''}{R} + \frac{1}{r}\frac{R'}{R} + \frac{1}{r^2}\frac{\Theta''}{\Theta} = -\lambda$ , multiply by $r^2​$ ?

$r^2 \frac{R''}{R} + r \frac{R'}{R} + \frac{\Theta''}{\Theta} = -\lambda r^2$ 

$r^2 \frac{R''}{R} + r \frac{R'}{R} + \lambda r^2 = -\frac{\Theta''}{\Theta} = \Lambda​$ 

---

$r^2 \frac{R''}{R} + r \frac{R'}{R} + \lambda r^2 = \Lambda$ $\equiv$ $r^2 R'' + rR' + \lambda r^2 R = \Lambda R $ $\equiv$ $r^2 R'' + rR' + (\lambda r^2 - \Lambda)R = 0$ 

$- \Theta''= \Lambda \Theta$  for $\Theta(\theta) = \Theta(\theta + 2\pi)$ 

### 3rd Part

$\Lambda = 0$: $\Theta = a\theta + b$ but $a\theta + b \not = a\theta + b + 2\pi a$ if $ a \neq 0$ so $\Lambda = 0$, we have $\Theta(\theta) = C$, some constant

$\Lambda = -\gamma^2$: $\Theta'' = \gamma^2 \Theta$ $\implies$ $C e^{\gamma \theta } + D e^{- \gamma \theta}$ 

we need: $Ce^{\gamma \theta} + De^{-\gamma \theta} = Ce^{\gamma \theta + \gamma 2\pi} + De^{-\gamma \theta - \gamma 2\pi}$ $\implies$ $e^{\gamma 2\pi} = 1$ 

$\implies$ $\text{Re}(\gamma) = 0$ so $\gamma = bi$, and we need $e^{2\pi b i} = e^{2\pi n i}$ so $\gamma = ni$ and $\Lambda = -(ni)^2 = n^2$ 

so we have $\Theta(\theta) = C \cos n\theta + D \sin n\theta$ 




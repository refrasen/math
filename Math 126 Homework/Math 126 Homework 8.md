Renee Senining
Math 126

#### Homework 8

#### Section 9.1: Exercise 5

Prove the principle of causality in two dimensions.

---

Two dimensional wave equation: $u_{tt} - c^2(u_{xx} + u_{yy}) = 0$ 
Multiply both sides by $u_t$: 
$u_{tt}u_t - c^2(u_{xx} + u_{yy})u_t = 0$ $\implies$ $\frac{1}{2}(u_t^2)_t - c^2[(u_xu_t)_x - \frac{1}{2}(u_x^2)_t + (u_yu_t)_y - \frac{1}{2}(u_y^2)_t]=0$ 
$\frac{1}{2}(u_t^2 + c^2u_x^2 + c^2u_y^2)_t -c^2 ((u_xu_t)_x + (u_y u_t)_y) = 0$ 

Integrate this over a solid cone frustrum $F$ in 3-dimensional space time with Top T, bottom B and side K and $\partial F$ is 2-dimensional. Let $(n_x, n_y, n_t)$ be the unit outward normal on $\partial F$ and let $dS$ denote the 2-dimensional surface integral over $\partial F$. Then (using divergence theorem, since the above equation states that the divergence of certain 3d vector field vanishes)

$\iint_{\partial F} [n_t (\frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2 + \frac{1}{2}c^2 u_y^2) - n_x(c^2u_xu_t) - n_y (c^2 u_y u_t)]dS  = 0$ 
$\partial F = T \cup B \cup K$ , so the integral has three parts: $\iint _T + \iint _B + \iint_K = 0$ 

On T:$ (n_x, n_y, n_t) = (0, 0, 1)$ so we have $\iint_T (\frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2 + \frac{1}{2}c^2 u_y^2) dS$ 
On $B$: $(n_x, n_y, n_t) = (0, 0, -1)$ and $t = 0$ so we have $-\iint_B \frac{1}{2}\psi^2 + \frac{1}{2}c^2 \phi_x^2 + \frac{1}{2}c^2 \phi_y^2 dS$ 

The 2-surface in 3-space light cone: $\phi(t, x, y) \equiv -c^2(t-t_0)^2 + (x-x_0)^2 + (y-y_0)^2 =0 $ , which is a level surface of $\phi$ and the normal to a level surface of $\phi$ is the gradient vector of $\phi$ 
$\bold n = \pm \frac{\nabla \phi}{|\nabla \phi|} = \pm \frac{c}{\sqrt{c^2 + 1}}(\frac{x-x_0}{cr}, \frac{y-y_0}{cr}, - \frac{t-t_0}{|t-t_0|})$ since $t < t_0$ and we need the time component of $\bold n$ to be positive on $K$ we use the plus sign. So on $K$: $\frac{c}{\sqrt{c^2 + 1}}\iint_K [ \frac{1}{2}u_t^2 + \frac{1}{2}c^2(u_x^2 + u_y^2) + \frac{x - x_0}{r}(-c^2u_x u_t) + \frac{y-y_0}{cr}(-c^2 u_y u_t)] \, dS$ let $\hat{ \bold r} = (\frac{x-x_0}{r}, \frac{y-y_0}{r})$ so we obtain $u_r = u_x \frac{\partial x}{\partial r} + u_y \frac{\partial y}{\partial r} = \hat{ \bold r} \cdot \nabla u$ 
the integrand is then: $\frac{1}{2}(u_t - cu_r)^2 + \frac{1}{2} c^2( u_x^2 + u_y^2 - u_r^2)$ $ = \frac{1}{2}(u_t - cu_r)^2 + \frac{1}{2}c^2| \sqrt{u_x^2 + u_y^2} - u_r \hat{\bold r}|^2$ $> 0$ so that from the entire  surface integral we end up with

$\iint_T \frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2 + \frac{1}{2}c^2 u_y^2 \, dS \leq \iint_B \frac{1}{2}\psi^2 + \frac{1}{2}c^2 \phi_x^2 + \frac{1}{2}c^2 \phi_y^2 \, dS$ 

If $u$ and $v$ are two solutions of the wave equation and if $u = v$ in $B$ then $w = u-v$ solves the wave equation with zero initial condition and $\iint_T \frac{1}{2}_T \frac{1}{2}w_t^2 + \frac{1}{2}c^2 w_x^2 + \frac{1}{2}w_y^2 \, dS \leq 0$ $\implies$ the integrand vanishes in $T$ $\implies$ $w_t, w_x, w_y$ vanish in $T$ Since we can vary the height of $F$ at will, $w_t$ and $w_x$ and $w_y$ vanish in the whole cone, so that $w$ is a constant. and from the initial condition, $w \equiv 0$ in all of the cone, in particular, $u(x_0, y_0, z_0, t_0) = v(x_0, y_0, z_0, t_0)$ 

#### Section 9.3: Exercises 3, 7

##### Exercise 3

Prove Theorem 2 in the one-dimensional case. That is if $\mathscr{C}​$ is a spacelike curve in the $xt​$ plane, there is a unique solution of $u_{tt} =c^2 u_{xx}​$ with $u = \phi​$ and $\frac{\partial u}{\partial n} = \psi​$ on $\mathscr{C}​$ 

---

Let $(x_0, t_0)$ be a point in the $xt$-plane. By the principle of casuality, the value of $u(x_0, t_0)$ depends only on the values of $\phi$ and $\psi$ on the base of the cone with tip $(x_0, t_0)$ 

In this case the base is the curve $\mathscr{C}$ from which light rays emanate to some point $(x_0, t_0)$ to make a triangle like shape. 

So We integrate over $F$ in 2-dimensional space-time with top $T$, bottom $B$ (a part of $\mathscr{C}$) and side $K$ which are segments of two lines that each connect $(x_0, t_0)$ to the curve $\mathscr{C}$ with slope $\pm c$  

So we have the energy identity in one-dimensional space:

$(\frac{1}{2}u_t^2 + \frac{1}{2}c^2|u_x|^2)_t + (-c^2u_tu_x)_x = 0$ and integrate using divergence theorem:

$\int_{\partial F} n_t(\frac{1}{2}u_t^2 + \frac{1}{2}c^2|u_x|^2) + n_x(-c^2 u_t u_x) \, ds = 0$ 
$\partial F$ has three parts: $\int_T + \int_B + \int_K = 0$ 
On T: $\bold n = (0, 1)$ so the integral here is $\int_T \frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2 \, ds$ 

and similalrly: $\int_B n_t(\frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2) + n_x(-c^2u_tu_x)\, ds $ 

 for $K$ just like in the 3d case, this is positive, so we have

$\int_T \frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2 \, ds \leq \int_\mathscr{C} n_t (\frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2) + n_x(-c^2 u_t u_x)$ $\, ds$ 

so since $\mathscr{C}$ is space like, all its normal vectors (which should really only be 2) satisfy $|n_t| > c|n_x|$ 

so using $()

$|\int_T \frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2 \, ds| \leq |\int_{\mathscr{C}} n_t(\frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2) + n_x(-c^2 u_t u_x) \, ds|$

$\leq \int_{\mathscr{C}} |n_t(\frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2)| + |n_x(-c^2 u_tu_x)| \, ds$ $\leq \int_{\mathscr{C}} |n_t||(\frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2)| + |n_t||cu_tu_x| \, ds$ 

$ = \int_{\mathscr{C}}|n_t|(|\frac{1}{2}u_t^2 + \frac{1}{2}c^2 u_x^2)| + |cu_t u_x|) \, ds$ 

$ \leq \int_{\mathscr{C}} |n_t|( \frac{1}{2}(|u_t|+|cu_x|)^2)\, ds = \int_{\mathscr{C}}|n_t|(\frac{1}{2}(|\phi_t| + |c\phi_x|)^2) \, ds$ 

since this is on $\mathscr{C}$ . suppose we have the conditions on $\mathscr{C}$ vanish, then by then from the above inequality and the vanishing theorem (if we change variables so that $T$ is parallel to some axis $x'$) we have

$\nabla u = 0$ so $u$ is a constant in the triangle-like shape (since we can vary $T$ at will). and since on $\mathscr{C}$ $u \equiv 0$, we must have $u \equiv 0$ on all of the cone and therefore at the point $u(x_0, t_0) = 0$ and this implies if we have two solutions $u, v$  to the above PDE with conditions on $\mathscr{C}$, the difference $w = u - v$ is a solution to the wave equation with 0 condition on $\mathscr{C}$ so $w = u- v \equiv 0$ $\implies$ $u = v$ at $(x_0, t_0)$ and since $(x_0, t_0)$)  was an arbitrary point, we have $u = v$ at any point $(x_0, t_0)$ which means uniqueness

##### Exercise 7 (difficult)

Solve $u_{tt} - c^2\Delta u = f(\bold x)​$, where $f(\bold x) = A​$ for $|\bold x | < \rho​$, $f(\bold x) = 0​$ for $|\bold x| > \rho​$, $A​$ is a constant, and the initial data are identically zero. Sketch the regions in space-time that illustrate your answer. (Hint: Use (13) and find the volume of intersectin of two balls or use (11) and exercise 9.2.6(b))

---

(13): $u(\bold x, t) = \frac{1}{4 \pi c^2} \iiint_{\{ |\bold \xi - \bold x| \leq ct \}} \frac{f(\bold \xi, t - |\bold \xi - \bold x|/c )}{|\bold \xi - \bold x|} \, d \bold \xi$ 

$u(\bold x, t) = \frac{1}{4 \pi c^2} \iiint_{ \{|\bold \xi - \bold x| \leq ct \} \cap \{ |\bold x| < \rho\} } \frac{A}{|\bold \xi - \bold x|} \, d \bold \xi$  

Starting with the sphere centered at origin with radius $\rho$, we draw straight lines (light rays) with slope c from every point

![image-20190403212301334](Math 126 Homework 8.assets/image-20190403212301334.png)

and the regions of interest are within the lines (past/future). Let $\bold \xi = (x_0, y_0, z_0)$. In the cone with base $|\bold x| < \rho$ and $t = 0$ with top at $(0, 0, 0, \frac{\rho}{c})$: for a point $(\bold x, t)$ we are integrating over the sphere $|\bold \xi - \bold x| \leq ct$, so we use spherical coordinates: $0 < r < ct$, $0 <\theta < \pi $, $0 <\phi < 2\pi$ and $(x_0 - x) = r\sin \theta \cos \phi$, $(y-y_0) = r \sin \theta \sin \phi$, $(z_0 - z) = r \cos \theta$ 

$u = \frac{A}{4 \pi c^2} \iiint \frac{d \xi}{ \sqrt{(x_0 - x)^2 + (y_0 - y)^2 + (z_0 - z)^2}} = \frac{A}{4 \pi c^2}(\int_0^{ct}r \, dr)(\int_0^{2\pi} d\phi)(\int_0^{\pi} \sin \theta \, d \theta)$ $= \frac{A}{4 \pi c^2}\frac{c^2t^2}{2}(2\pi)(2)$

$\frac{At^2}{2}$ and this works similarly for the same cone but reflected along the space axis. (so $t < 0$)

so we have:

$u = \frac{1}{2}At^2$ for $r < \rho - ct$ 
$u \equiv 0$ for $r > \rho + ct$ (this lies outside of the past and future of $|\bold x | < \rho​$)

and the rest are cubic expressions

for the future solid cone from $(\bold 0, \rho/c)$ and the past cone from $(\bold 0, -\rho/c)$ have the same kinds of integrals:

here $|t| > \rho/c​$ so we have $|\bold \xi - \bold x| \leq \rho​$ so we are mostly concerned 

with the intersection between $|\xi - \bold x| \leq \rho$ and $|\bold x| \leq \rho$, but this is $|\bold x| \leq \rho$ , then using law of cosines, we know $|\bold \xi - \bold x| = \sqrt{|\bold \xi|^2 - 2|\bold \xi||\bold x|\cos \alpha + |\bold x|}$ where $\alpha$ is the angle between the vectors $\bold x$, $\bold \xi$ . then we use spherical coordinates again:

but with $(r_0, \theta_0, \phi_0)$ for $\bold \xi$ and $(r, \theta, \phi)$ for 

$u(x, y, z, t) = \frac{A}{4 \pi c^2} \int_0^\pi \int_0^{2\pi} \int_0^{\rho} \frac{r^2 \sin \theta}{\sqrt{r_0^2 - 2rr_0 \cos \alpha +r^2}} dr_0 \, d\phi_0 \, d \theta_0$ 

with substitution and orienting the polar axis we wind up with:

$\frac{A}{2c^2 r}\int_0^\rho r_0(r_0 + r - |r_0 - r|) dr_0$ $ = \begin{cases} \frac{A}{6c^2}(3\rho^2 - r^2) & r < \rho \\ \frac{A \rho^3}{3c^2 r} & r > \rho \end{cases}$   

The other sections are done in a similar manner

#### Section 9.4: Exercises 1, 2, 3

##### Exercise 1

Find a simple formula for the solution of the three dimensional diffusion equation with $\phi(x,y, z) = xy^2 z​$ (Hint: See Exercise 2.4.9 or 2.4.10)

---

Assume the solution is in the form $u(x, y, z, t) = xz(v(y, t))$ where $v$ satisfies: $v_t = kv_{yy}$ with $v(y, 0) = y^2$. Such a $u$ does satisfy the 3d diffusion equation: $u_t = xzv_t = xz(kv_{yy})$, $u_{xx} = u_{yy}= 0$, $u_{yy} = xzv_{yy}$ so we indeed have $u_t = k\Delta u$ 
From Exercise 2.4.9, we know the solution to $v_t = kv_{yy}$ w/ $v(y, 0) = y^2$ is $v(y, t) = y^2 + 2kt$ 

so $u(x, y, z, t) = xzy^2 + 2kxzt$ is the solution
Checking: $u_{t} = 2kxz$ and $u_{xx} = 0 = u_{zz}$ and $u_{yy} = 2xz$ so this is the solution. 

##### Exercise 2

**(a)** (6): $\lim_{t \rightarrow 0} \iiint S_3(\bold x - \bold x', t) \phi(\bold x') d\bold x' = \phi(\bold x)$ 
$\lim_{t \rightarrow 0} \iiint S(x - x', t) S(y-y', t) S(z - z', t) \phi(x') \psi(y') \zeta(z') \, dx' \, dy' \, dz'$
$= \lim_{t \rightarrow 0} [ \int S(x-x', t) \phi(x')\, d x' ]\cdot [\int S(y-y', t) \psi(y')\, dy'] \cdot [\int S(z-z', t)\zeta(z')\, dz']$ and by theorem 3.5.1:
$ = \phi(x)\psi(y) \zeta(z)$ 

and obviously if we have $\Phi_1(\bold x) = \phi_1(x)\psi_1(y) \zeta_1(z$ )

then $\lim _{t \rightarrow 0} \iiint S_3(\bold x - \bold x', t)[\Phi_1(\bold x') + \cdots + \Phi_n(\bold x')] d\bold x' = $
$\sum_{k = 1}^n \lim_{t \rightarrow 0} \iiint S_3(\bold x - \bold x', t)\Phi_k(\bold x') d\bold x' = \sum_{k=1}^n \Phi_k(\bold x)$ 

**(b)** Let $\phi(\bold x)$ be a bounded continuous function. Then there is a sequence of finite sums of products of the form $\phi(x)\psi(y)\zeta(z)$ which converges uniformly to $\phi(\bold x)$. Since from $(a)$ we showed that $(6)$ is valid for finite sums of products of the form $\phi(x)\psi(y)\zeta(z)$, $(6)$ is valid for each finite sum in the sequence converging to $\phi(\bold x)$ and therefore valid for $\phi(\bold x)$ (any bounded continuous function) as well. 

##### Exercise 3

Find the solution of the diffusion equation in the half-space $\{(x, y, z, t): z > 0 \}$ with the Neumann condition $\frac{\partial u}{\partial z} = 0$ on $z = 0$ Hint: Use the method of reflection.

---

Our PDE: 
$$
\begin{align}
\frac{\partial u}{\partial t} & = k \Delta u & \{(x, y, z, t): z> 0
\}
\\ u(\bold x, 0)&= \phi(\bold x) &\frac{\partial u}{\partial z}(x, y, 0, t) &= 0
\end{align}
$$
**First**: Extend $\phi(\bold x)$ evenly: Define: $\Phi(\bold x) = \begin{cases} \phi(x, y, z, t) & z \geq 0 \\ \phi(x, y, -z, t) & z \leq 0 \end{cases}$  

Then we have a PDE on the whole 3-space:
$$
\begin{align} 
w_t = k \Delta w
\\ w(\bold x, 0) = \Phi(\bold x)
\end{align}
$$
we claim the restriction of a solution to this PDE, which is $w$, to the half-space is a solution to the original PDE. i.e. $u$ is the restrcition of $w$ to the half-space. 

* $u$ is essentially equal to $w$ on the half-space and $w$ satisfies the PDE everywhere so $u$ satisfies the PDE on the half-space

* $u(\bold x) = \Phi(\bold x) = \phi(\bold x)$ in the half-space, so $u$ satisfies the inital condition

* $w$ is even w.r.t. to $z$: $w(x, y, -z, t) - w(x, y, z, t)$ satisfies $w_t = k \Delta w$ with zero initial condition. Obviouslly $w(x, y, z, t)$ satisfies the PDE, and $w(x, y, -z , t)$ satisfies the PDE since $\partial_t w(x, y, -z, t) = w_t(x, y, -z, t)$ and $\partial _x^2 w(x, y, -z, t) = w_{xx}(x, y, -z, t)$ and similalry for $\partial_ y^2 w(x, y, -z, t) = w_{yy}$ and $\partial_z w(x, y, -z, t) = -w_z(x, y, -z ,t)$ $\implies$ $\partial_z^2 w(x, y, -z, t) = w_{zz}$ . And: $w(x, y, -z, 0) - w(x, y, z, 0) = \phi(x, y, |z|, t)- \phi(x, y, |z|, t) = 0$ and due to uniqueness: $w(x, y, -z, t) -w(x, y, z, t) = 0$ $\implies$ $w$ is even. 

* And since $w$ is even w.r.t. to $z$ $\frac{\partial w}{\partial z}$ is odd w.r.t. to $z$ so $\frac{\partial w}{\partial z} = 0$ at $z = 0$ $\implies$ $\frac{\partial u}{\partial z} = 0$ at $z = 0$ 

  so $w(\bold x, t) = \frac{1}{(4 \pi k t)^{3/2}} \iiint \exp( - \frac{| \bold x - \bold x'|^2}{4k t})\Phi(\bold x') d \bold x'$ 

   Splitting the integral: $\iiint_{z = -\infin}^0 \exp( - \frac{|\bold x - \bold x'|^2}{4kt})\phi(x', y', -z') \, dx' dy' dz'  + \iiint_{z = 0}^\infin \exp( - \frac{|\bold x - \bold x'|^2}{4kt})\phi(\bold x') d \bold x'​$ the first integral, with change of variables from $z \rightarrow -z​$ becomes:

  $\iiint_{z = 0}^\infin \exp( - \frac{|\bold x - \bold x'^*|^2}{4kt})\phi(x', y', z') d\bold x'$  where $\bold x'^* = (x',  y',- z')$ 

  $u(\bold x, t) = \frac{1}{(4 \pi kt)^{3/2}}\int_0^\infin \int_{-\infin}^\infin \int_{-\infin}^\infin (\exp(-\frac{|\bold x - \bold x'^*|^2}{4kt}) + \exp(- \frac{|\bold x - \bold x'|^2}{4kt}))\phi(\bold x') dx' \, dy' \, dz'​$ 

#### Section 9.5: Exercise 2

For the hydrogen atom if $\lambda > 0$, why would you expect equation $(4)$ not to have a solution that satisfies the condition at infinity?

---

letting the second and third terms in $(4)​$ go to $0​$ as $r \rightarrow \infin​$ 

we obtain: $-R'' = \lambda R​$ and if $\lambda > 0​$ 

this means we must have $\beta^2 = -\lambda$ so $\beta =  \sqrt{-\lambda}$, a complex number, so for $e^{\pm \beta r}$, we have $\beta = a + bi$, so that we have instead $e^{\pm ar}e^{\pm ibr}$ we still need this to vanish at infinity so we focus on $-\beta$: $e^{-ar}e^{-ibr} = e^{-ar}(\cos(br) - i\sin(br))$ but for any choice of $b$, $\cos(br) - i \sin(br)$ will keep oscillating as $r \rightarrow \infin$, so $e^{-\beta r}$ doesn't vanish. 

#### Section 8.1: Exercises 1, 2, 3

##### Exercise 1

The taylor exapnasion written in Section 8.1 is valid if $u$ is a $C^4$ function. If $u(x)$ is mereley a $C^3$ function, the best we can say is that the Taylor Expansion is valid only with a $o(\Delta x)^3$ error...

---

Noting: according to the book having a $u''(x)(\Delta x)^2$ is $O(\Delta x)$ 

and $u'''(x)(\Delta x)^3 $ is $O(\Delta x)^2$ 

**(a)**: $u(x)$ is $C^3$, so the Taylor expansion becomes:

$u(x + \Delta x) = u(x) + u'(x)(\Delta x) + \frac{1}{2}u''(x)(\Delta x)^2 + \frac{1}{6}u'''(x)(\Delta x)^3 + o(\Delta x)^3$ 

$u(x - \Delta x) = u(x) - u'(x)(\Delta x) + \frac{1}{2}u''(x)(\Delta x)^2 - \frac{1}{6}u'''(x)(\Delta x)^3 + o(\Delta x)^3$ 

We obtain the centered difference by subtracting the second taylor expansion by the first:

$u(x + \Delta x) - u(x - \Delta x) = 2u'(x)(\Delta x) + \frac{1}{3}u'''(x)(\Delta x)^3 +o(\Delta x)^3$  

$u'(x) = \frac{u(x + \Delta x)- u(x - \Delta x)}{2\Delta x} + O(\Delta x)^2$ ( $O(\Delta x)^2$ comes from dividing $\frac{1}{3}u'''(x)(\Delta x)^3 + o(\Delta x)^3$ by $\Delta x$) 

So the error is $O(\Delta x)^2$ 

**(b)**: for $u(x)$ a $C^2$ function:

$u(x + \Delta x) = u(x) + u'(x)(\Delta x) + \frac{1}{2}u''(x)(\Delta x)^2 + o(\Delta x)^2$ 

$u(x - \Delta x) = u(x) - u'(x)(\Delta x) + \frac{1}{2}u''(x)(\Delta x)^2 + o(\Delta x)^2​$ 

subtracting the second from the first:

$u'(x) = \frac{u(x+ \Delta x) - u(x - \Delta x)}{2 \Delta x} + O(\Delta x)$ (from dividing $o(\Delta x)^2$ by $\Delta x$ )

the error is $O(\Delta x)$ 

##### Exercise 2

**(a)**:  If $u(x)$ is $C^3$ what is the error in the second derivative due to its approximation by a centered second difference?

Taylor Expansions are as in exercise 1.a and we add to obtain:

$u(x + \Delta x) + u(x - \Delta x) = 2u(x) + u''(x)(\Delta x)^2 + o(\Delta x)^3$ 

$u''(x) = \frac{u(x + \Delta x) - 2u(x) + u(x - \Delta x)}{(\Delta x)^2} + O(\Delta x)$ 

error is $O(\Delta x)​$ 

**(b)**: if $u(x)$ is mereley a $C^2$ function:

adding the taylor expansions in 1.b:

$u(x + \Delta x) + u(x - \Delta x) = 2u(x) + u''(x)(\Delta x)^2 + o(\Delta x)^2$ 
$u''(x) = \frac{u(x + \Delta x) - 2u(x) + u(x - \Delta x)}{(\Delta x)^2} + O(1)$ 
where the $O(1)$ comes from dividing $o(\Delta x)^2$ by $(\Delta x)^2$ 

##### Exercise 3

Suppose that we wish to approximate the first derivative of $u'(x)$ of a very smooth function with an error of only $O(\Delta x)^4$. Which difference approximation could we use?

---

$u(x + \Delta x) = u(x) + u'(x)(\Delta x) + \frac{1}{2}u''(x)(\Delta x)^2 + \frac{1}{3!}u'''(x)(\Delta x)^3 + \frac{1}{4!}u''''(x)(\Delta x)^4 + O(\Delta x)^5$ $u(x - \Delta x) = u(x) - u'(x)(\Delta x) + \frac{1}{2}u''(x)(\Delta x)^2 - \frac{1}{3!}u'''(x)(\Delta x)^3 + \frac{1}{4!}u''''(x)(\Delta x)^4 + O(\Delta x)^5$ $u( x + 2\Delta x) = u(x) + 2u'(x)(\Delta x) + 2u''(x)(\Delta x)^2 + \frac{4}{3}u'''(x)(\Delta x)^3 + \frac{2}{3}u''''(x)(\Delta x)^4 + O(\Delta x)^5$

$u(x - 2\Delta x) = u(x) - 2u'(x)(\Delta x) + 2u''(x)(\Delta x)^2 - \frac{4}{3}u'''(x)(\Delta x)^3 + \frac{2}{3}u''''(x)(\Delta x)^4 + O(\Delta x)^5$ want to cancel out $u'', u''', u''''$ terms

$u(x + \Delta x) - u(x - \Delta x) = 2u'(x)(\Delta x) + \frac{1}{3}u'''(x)(\Delta x)^3 + O(\Delta x)^5$ 

$u(x + 2\Delta x) - u(x - 2\Delta x) = 4u'(x)(\Delta x) + \frac{8}{3}u'''(x)(\Delta x)^3 + O(\Delta x)^5$ 

multiplying the first equation by $-8$… and then adding both while also replacing $x$ with $j\Delta x$ 

$-8u_{j+1} +8u_{j-1} + u_{j+2} - u_{j-2} = -12u'_j(x)(\Delta x) + O(\Delta x)^5$ 

so now: $u_j'(x) = (\frac{2}{3}u_{j+1} - \frac{2}{3}u_{j-1} -\frac{1}{12}u_{j+2} + \frac{1}{12}u_{j-2})/\Delta x + O(\Delta x)^4$ 

#### Section 8.2: Exercises 1, 10 ,12

##### Exercise 1

**(a)**: 

Solve $u_t = u_{xx}$ in the interval $[0, 4]$ with $u = 0$ at both ends and $u(x, 0) = x(4-x)$ using the forward difference scheme with $\Delta x = 1$ and $\Delta t = 0.25$ calculate four time steps (up to t = 1)

---

$$
\frac{u_j^{n+1}-u_j^n}{\Delta t} - \frac{u_{j+1}^n - 2u_j^n + u_{j-1}^n}{(\Delta x)^2}
$$

letting $s = \frac{\Delta t}{(\Delta x)^2}$ we obtain: $u_j^{n+1} = s(u_{j+1}^n + u_{j-1}^n)+ (1-2s)u_j^n$ 
we have $u_0^n = u_J^n = 0$ for all $n = 0, 1, 2, 3, 4$ and $u_j^0 = \phi(j \Delta x)$ 

$\Delta t = \frac{1}{4}$ and $\Delta x = 1$ so $j = 0, 1, 2, 3, 4 = J$ 

$u_j^{n+1} = \frac{1}{4}(u_{j+1}^n + u_{j-1}^n) + \frac{1}{2}u_j^n​$ 



|         | $j = 0$ | $j= 1$                                          | $j = 2$                                                      | $j = 3$         | $j = 4$ |
| ------- | ------- | ----------------------------------------------- | ------------------------------------------------------------ | --------------- | ------- |
| $n = 0$ | 0       | $1(4-1) = 3$                                    | $2(4-2) = 4$                                                 | $3(4-3) = 3$    | 0       |
| $n = 1$ | 0       | $\frac{5}{2}$                                   | $\frac{7}{2}$                                                | $\frac{5}{2}$   | 0       |
| $n = 2$ | 0       | $\frac{17}{8}$                                  | $3$                                                          | $\frac{17}{8}$  | 0       |
| $n = 3$ | 0       | $\frac{3}{4} + \frac{17}{16} = \frac{29}{16}$   | $\frac{17}{16} + \frac{3}{2} = \frac{41}{16}$                | $\frac{29}{16}$ | 0       |
| $n = 4$ | 0       | $\frac{41}{64} + \frac{29}{32} = \frac{99}{64}$ | $\frac{29}{32} + \frac{41}{32} = \frac{70}{32} = \frac{35}{16}$ | $\frac{99}{64}$ | 0       |

**(b)**: 

Do the same with $\Delta x = 0.50$ and $\Delta t = 0.0625 = \frac{1}{16}$ so $s = \frac{1/16}{1/4} = \frac{4}{16} = \frac{1}{4}$ 

$u_j^{n+1} = \frac{1}{4}(u_{j+1}^n + u_{j-1}^n) + \frac{1}{2}u_j^n$ once more

|                       | $j =0$ | $j = 1$ $(x = 0.50)​$ | $j = 2 (x = 1)$   | $j = 3$ $(x = 1.5)$ | $j = 4$ ($x = 2$) |
| --------------------- | ------ | -------------------- | ----------------- | ------------------- | ----------------- |
| $n = 0$               | 0      | $\frac{7}{4}$        | $3$               | $\frac{15}{4}$      | $4$               |
| $n = 1$ ($t = 1/16​$)  | 0      | $\frac{13}{8}$       | $\frac{23}{8} $   | $\frac{29}{8}$      | $\frac{31}{8}$    |
| $n = 2$ ($t = 1/8$)   | 0      | $\frac{49}{32}$      | $\frac{44}{16} $  | $\frac{56}{16}$     | $\frac{60}{16}$   |
| $n = 3$  ($t = 3/16$) | 0      | $\frac{93}{64}$      | $\frac{337}{128}$ | $\frac{27}{8}$      | $\frac{29}{8}$    |
| $n = 4$ $( t = 1/4)$  | 0      | $\frac{709}{512}$    | $\frac{323}{128}$ | $\frac{1665}{512}$  | $\frac{7}{2}$     |

|         | $j = 5$ $(x = 2.5)​$ | $j = 6$ $(x = 3)$ | $j = 7$  $(x = 3.5)$ | $j = 8$  $(x = 4)$ |
| ------- | ------------------- | ----------------- | -------------------- | ------------------ |
| $n = 0$ | $\frac{15}{4} $     | 3                 | $\frac{7}{4}$        | 0                  |
| $n = 1$ | $\frac{29}{8}$      | $\frac{23}{8}$    | $\frac{13}{8}$       | 0                  |
| $n =2$  | $\frac{56}{16}$     | $\frac{44}{16}$   | $\frac{49}{32}$      | 0                  |
| $n =3$  | $\frac{27}{8}$      | $\frac{337}{128}$ | $\frac{93}{64}$      | 0                  |
| $n = 4$ | $\frac{1665}{512}$  | $\frac{323}{128}$ | $\frac{709}{512}$    | 0                  |

**(c)**: they are exactly the same, $7/2​$ 

##### Exercise 10

For the diffusion equation $u_t = u_{xx}$, use centered difference for both $u_t$ and $u_{xx}$ 

---

**(a)**: Write the scheme. Let $s = \Delta t/(\Delta x)^2$ 

$\frac{u_{j}^{n+1} - u_j^{n-1}}{2 \Delta t} = \frac{u_{j+1}^n - 2u_j^n + u_{j-1}^n}{(\Delta x)^2}$ 

$(u_j^{n+1} - u_j^{n-1}) = 2s(u_{j+1}^n - 2u_j^n + u_{j-1}^n)$ 

if we are solving for $u_j^{n+1}​$ it is explicit. 

**(b)**: Show that it is unstable, no matter what $\Delta x$ and $\Delta t$ are

$(u_j^{n+1} - u_j^{n-1}) = 2s(u_{j+1}^n - 2u_j^n + u_{j-1}^n)​$ 

Separating variables: $u_j^n = X_j T_n$ :

$X_j T_{n+1} - X_j T_{n-1} = 2s( X_{j+1}T_n - 2 X_j T_n + X_{j-1}T_n)$ 

dividing by $X_j T_n$: 

$\frac{T_{n+1} - T_{n-1}}{T_n} = 2s \frac{X_{j+1} + X_{j-1}}{X_j} - 4s$  

Let $T_n = \xi(k)^n$ and $X_j = (e^{i k \Delta x})^j$ 
$\xi(k) - \xi(k)^{-1} = 2s(e^{ik \Delta x} + e^{- i k \Delta x}) - 4s$ 

then: $\xi(k)^2 = -4s\xi(k)(1 - \cos(k \Delta x)) + 1$

divide both sides by $2$: $\frac{\xi(k)^2}{2} = -2s \xi(k)(1 - \cos (k\Delta x)) +\frac{1}{2}$ $\equiv$ $\frac{\xi(k)^2}{2} + 2s \xi(k)(1 - \cos(k \Delta x))- \frac{1}{2} = 0$ 

quadratic equation: $\xi(k) = -2s \xi(k)(1 - \cos(k \Delta x)) \pm \sqrt{4s^2(1- \cos(k\Delta x)^2 + 1)}$ 

letting $p = 2s(1 - \cos (k \Delta x))$: $\xi(k) = -p \pm \sqrt{p^2 + 1}$

and one of the roots: $-p - \sqrt{p^2 + 1} < 0 - \sqrt{1} = -1$ 

so we have $\xi(k) < -1$ but for stability we need $\xi(k) \geq -1$, so this scheme is unstable. (Only for $k$ s.t. $\cos(k \Delta x) = 1$ do we have the negative root for $\xi(k) = -1$, but for all other $k$ there will always be instability for any choice of $s$) 

##### Exercise 12

**(a)**: 

Solve by hand the nonlinaer PDE $u_t = u_{xx} + (u)^3$ for all $x$ using the standard forward difference scheme with $(u)^3$ treated as $u_j^n)^3$. Use $s = \frac{1}{4}$, $\Delta t = 1$ and initial data $u_j^0 = 1$ for $j = 0$ and $u_j^0 = 0$ for $j \neq 0$ Solve for $u_0^3$ 

The scheme: $u_j^{n+1} = s(u_{j+1}^n + u_{j-1}^n) + (1 - 2s)u_j^n + (u_j^n)^3$ 

with $s = \frac{1}{4}$: $u_j^{n+1} = \frac{1}{4}(u_{j+1}^n + u_{j-1}^n) + \frac{1}{2}u_j^n + (u_j^n)^3$ 


|         | $j = -2$       | $j = -1$        | $j = 0$                                                      | $j = 1$         | $j = 2$        |
| ------- | -------------- | --------------- | ------------------------------------------------------------ | --------------- | -------------- |
| $n = 0$ | 0              | 0               | 1                                                            | 0               | 0              |
| $n = 1$ | $0$            | $\frac{1}{4}$   | $\frac{3}{2}$                                                | $\frac{1}{4}$   | $0$            |
| $n = 2$ | $\frac{1}{16}$ | $\frac{33}{64}$ | $\frac{17}{4}$                                               | $\frac{33}{64}$ | $\frac{1}{16}$ |
| $n = 3$ |                |                 | $\frac{1}{4}(\frac{33}{32})+ \frac{17}{8} + \frac{17^3}{64}$ $= \frac{33}{128} + \frac{272}{128} + \frac{4913*2}{128}$ $ = \frac{10131}{128}$ $\approx 79.15$ |                 |                |

$u_0^3 = \frac{10131}{128} \approx 79.15$ 
**(b)**: without the nonlinear term, we have

|         | $j = -2$       | $j = -1$                                 | $j = 0$                                                     | $j= 1$        | $j = 2$        |
| ------- | -------------- | ---------------------------------------- | ----------------------------------------------------------- | ------------- | -------------- |
| $n = 0$ | 0              | 0                                        | 1                                                           | 0             | 0              |
| $n = 1$ | 0              | $\frac{1}{4}$                            | $\frac{1}{2}$                                               | $\frac{1}{4}$ | 0              |
| $n = 2$ | $\frac{1}{16}$ | $\frac{1}{8}+ \frac{1}{8} = \frac{1}{4}$ | $\frac{1}{8} + \frac{1}{4} = \frac{3}{8}$                   | $\frac{1}{4}$ | $\frac{1}{16}$ |
| $n = 3$ |                |                                          | $\frac{1}{8} + \frac{3}{16} = \frac{5}{16}$  $\approx 0.31$ |               |                |
|         |                |                                          |                                                             |               |                |

$u_0^3 \approx 0.31$ , significantly less than the previous answer

**(c)**: 

Exactly solve the ODE $dv/dt = v^3​$ with the condition $v(0) = 1​$ Use it to explain why $u_0^3​$ is so large in part $(a)​$ 

---

$\int \frac{1}{v^3} dv = \int 1 dt$ $\implies$ $- \frac{1}{2 v^2}  = t + A$ 
$\frac{-1}{t + A} = 2v^2$ $\implies$ $v^2 = -\frac{1}{2t+A}$ $\implies$ $v = \pm \sqrt{- \frac{1}{2t +A }}$ 

Checking: $v' = \pm \frac{2}{(2t+A)^2}\frac{1}{2}(- \frac{1}{2t+ A})^{-1/2}$ $ = \pm (\frac{1}{2t+A})^{2 - 1/2}$ and $2 - 1/2 = 3/2​$ 

$v = \sqrt{- \frac{1}{2t + A}}$ so that $v(0) = 10>$ and $v(0) = \sqrt{ - \frac{1}{A}}$ so $A = -1$ 
$v(t) = \sqrt{ \frac{1}{1 - 2t}}$ and for large $t$, this grows exponentially large because as $t \uparrow \frac{1}{2}$ $\frac{1}{1 - 2t} \rightarrow \infin+$ 

![image-20190402114854154](Math 126 Homework 8.assets/image-20190402114854154.png)

**(d)**: 

Repeat part $(a)$ with the same initial data but for the PDE $u_t = u_{xx} - u^3$. Compare with the answer in part $(a)$ and explain

---

|         | $j = -2$                                | $j = -1$                                                     | $j = 0$                                                      | $j = 1$        | $j = 2$        |
| ------- | --------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------------- | -------------- |
| $ n =0$ | 0                                       | 0                                                            | 1                                                            | 0              | 0              |
| $n = 1$ | 0                                       | $\frac{1}{4}$                                                | $\frac{1}{4}(\frac{1}{2}) + \frac{1}{2} - 1^3$ $= -\frac{3}{8}$ | $\frac{1}{4}$  | 0              |
| $n = 2$ | $\frac{1}{4}\frac{1}{4} = \frac{1}{16}$ | $-\frac{1}{4}\frac{3}{8} + \frac{1}{2}\frac{1}{4} - \frac{1}{4^3}$ $ = \frac{1}{64}$ | $\frac{1}{4}\frac{1}{2} - \frac{3}{16} - \frac{27}{8^3}$ $ = -\frac{59}{512}$ | $\frac{1}{64}$ | $\frac{1}{16}$ |
| $n = 3$ |                                         |                                                              | $\frac{1}{4}\frac{1}{32} - \frac{59}{1024} - \frac{59^3}{512^3}$ $\approx -0.051$ |                |                |
|         |                                         |                                                              |                                                              |                |                |

This new $u_0^3$ is much smaller than the anwer in part $(a)$ 

in the earlier computations $n =1​$, $n=2​$ , changing the sign on $u^3​$ results in terms being canceled out rather than added onto each other exponentially, so instead of getting exponentially larger, terms begin to get smaller over time. i.e., we prevent the introduction of terms like $33/16​$ early on and instead get terms smaller than 1, and all terms are then being "shrunk" by either $1/4​$, $1/2​$ or by itself (since we have all terms of magnitude <1)
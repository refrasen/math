Renee Senining
Math 126 

## Homework 9

#### Section 12.1: Exercises 1—9, 11, 12

##### Exercise 1

Verify directly from the definition that $\phi \mapsto \int_{-\infin}^\infin f(x)\phi(x)\, dx$ is a distribution if $f(x)$ is any function that is integrable on each bounded set. 

---

Linear: $(f, a\phi + b\psi) = \int_{-\infin}^\infin f(x)[a \phi + b \psi] \, dx$ $ = a\int_{-\infin}^\infin f(x)\phi(x)\, dx + b \int_{-\infin}^\infin f(x)b\psi(x)\, dx = a(f, \phi) + b(f, \psi)$

Continuity: Let $\{ \phi_n \}$ be a sequence of test functions that vanish outside a common interval and converge uniformly to $\phi$ 

$(f, \phi_n) = \int_{-\infin}^\infin f(x)\phi_n(x)\, dx$ $\underset{n \rightarrow \infin}{\rightarrow}$  $\int_{-\infin}^\infin f(x)\phi(x)\, dx$ $ = (f, \phi)$ 

##### Exercise 2

Let $f$ be any distribution. Verify that the function $f'$ defined by $(f', \phi) = -(f, \phi')$ satisfies the linearity and continuity properties and therefore is another distribution. 

---

Linearity: $(f', a\phi+ b \psi) = -(f, a \phi' + b\psi') = - a(f, \phi') - b(f, \psi') = a(f', \phi)+b(f', \psi) $ 

Continuity: let $\{ \phi_n \}$ and $\phi_n \rightarrow \phi$ , and from our notion of convergence $\phi'_n \rightarrow \phi'$ so: $(f', \phi_n) = -(f, \phi_n') \rightarrow -(f, \phi') = (f', \phi)$ 

##### Exercise 3

Verify that the derivative is a linear operator on the vector space of distributions. 

---

the derivative of a distribution $f$: $(f', \phi) = - f(, \phi')$

Let $f$ and $g$ be distributions:

we want $\frac{d}{dx}(af + bg) = a\frac{d}{dx}(f) + b \frac{d}{dx}(g)$

Since distributions are a vector space: we should be able to add distributions and multiply by constants: 

$(af + bg, \phi) = a(f, \phi) + b(g, \phi)$ and $af + bg$ is a distribution so:
so $((af + bg)', \phi) =  -(af + bg, \phi')  = -a(f, \phi') -b(g, \phi') = a(f', \phi) + b(g', \phi)$ 

##### Exercise 4

Denoting $p(x) = x^+$, show that $p' = H$ and $p'' = \delta$ 

---

$x^+ = \max(x, 0)$ $p(x) = \begin{cases} x & x \geq 0 \\ 0 & x\leq 0 \end{cases}$

$(p', \phi) = -(p, \phi') = - \int_{0}^\infin x \phi'\, dx = -[x \phi]_0^\infin + \int_{0}^\infin \phi(x)\, dx = \int_0^\infin \phi(x) = \int_{-\infin}^\infin H(x)\phi(x)\, dx$ so $p' = H$ 

$(p'', \phi) = -(p', \phi') = (p, \phi'') = \int_{0}^\infin x\phi'' \, dx$ $= [x\phi']_0^\infin - \int_0^\infin \phi' \, dx$ $ = [-\phi]_0^\infin = \phi(0)$ 
so $p'' = \delta$ 

Noting: used the fact that $\phi$ and its derivatves vanish at infinity in the integrals above 

##### Exercise 5

Verify, directly from the definition of a distribution, that the discontinuous function $u(x,t)= H(x-ct)​$ is a weak solution of the wave equation. 

---

so $H(x-ct) = \begin{cases} 1 & x > ct \\ 0 & x < ct \end{cases}​$ 

weak solution of wave equation is a distribution $u$ for which $(u, \phi_{tt}-c^2\phi_{xx}) = 0$ where $\phi(x, t)$ is a test function. 

So $(u, \phi_{tt} - c^2 \phi_{xx})$ $ = (H, \phi_{tt} - c^2 \phi_{xx})$ $ = (H, \phi_{tt}) - c^2 (H, \phi_{xx})$ by linearity and then integrating over all of $xt-$space:
$=\int_{-\infin}^\infin \int_{-\infin}^\infin H(x-ct)\phi_{tt}(x, t)\, dt\, dx - c^2 \int_{-\infin}^\infin\int_{-\infin}^\infin H(x-ct) \phi_{xx}(x, t)\, dx\, dt$ 
$ = \int_{-\infin}^\infin[H(x-ct)\phi_t(x, t)|_{t=-\infin}^\infin +c \int_{-\infin}^\infin \delta(x-ct) \phi_{t}\, dt]\, dx$ $ - c^2 \int_{-\infin}^\infin [H(x-ct)\phi_x(x, t)|_{x =-\infin}^\infin - \int_{-\infin}^\infin\delta(x-ct)\phi_x(x,t)\, dx] \, dt$ 

and since test functions disappear at $\pm \infin$ 

$ = c\int_{-\infin}^\infin \int_{-\infin}^\infin \delta(x - ct) \phi_t dt \, dx$ $+c^2 \int_{-\infin}^\infin\int_{-\infin}^\infin \delta(x-ct)\phi_x(x, t)\, dx dt$ 
$ = c \int_{-\infin}^\infin [\delta(x-ct)\phi(x,t)|_{t=-\infin}^\infin + c\int_{-\infin}^\infin \delta'(x-ct)\phi(x,t)dt]dx $ $+ c^2 \int_{-\infin}^\infin [\delta(x-ct)\phi(x,t)|_{x = -\infin}^\infin - \int_{-\infin}^\infin \delta'(x-ct)\phi(x,t)\, dx]dt$ 
and again, test functions disappaer at $\pm \infin$ so:

$ = c^2 \int_{-\infin}^\infin  \int_{-\infin}^\infin \delta'(x-ct)\phi(x,t)dt\, dx - c^2 \int_{-\infin}^\infin \int_{-\infin}^\infin \delta'(x-ct)\phi(x,t)\, dx \, dt$ 

and switching the order of integration we easily get the above $= 0$ 

##### Exercise 6

Use Chapter 5 directly to prove (19) for all $C^1$ function $\phi(x)$ taht vanish near $\pm \pi$ 

---

We want to show that

$\sum_{n \text{ odd}}\int_{-\pi}^\pi \phi(x)\cos nx \, dx = \frac{\pi \phi(0)}{2}$ 

so (18): $\sum_{n \text{ odd}}\frac{2}{\pi}\cos n x = \delta(x)$ in $(-\pi, \pi)$ 

$\sum_{n \text{ odd}}\frac{2}{\pi}\cos nx\cdot  \phi(x) = \delta (x) \cdot \phi(x)$ in $(-\pi, \pi)$

Integrating term by term on the left and integrating on the right:(over $(-\pi, \pi)​$)

$\frac{2}{\pi}\sum_{n \text{ odd}}\int_{-\pi}^\pi \phi(x)\cos nx \, dx =  \int_{-\pi}^\pi \phi(x)\delta(x)\, dx = \phi(0)$ 

and then multiplying both sides by $\frac{\pi}{2}$ we get (19)

##### Exercise 7

Let a sequence of $L^2​$ function $f_n(x)​$ converge to a function $f(x)​$ in the mean-square sense. Show that it also converges weakly in the sense of distributions

---

so $ \int_{-\infin}^\infin |f_n(x)|^2 \, dx < \infin$ and $\int_{-\infin}^\infin |f(x) - f_n(x)|^2 \, dx \rightarrow 0$ as $n \rightarrow \infin$ 

So $(f_n, \phi) = \int_{-\infin}^\infin f_n(x)\phi(x)\, dx$ 

$|\int_{-\infin}^\infin f(x)\phi(x)\, dx - \int_{-\infin}^\infin f_n(x)\phi(x)\, dx|^2​$ 
$ \leq \int_{-\infin}^\infin |f(x) - f_n(x)|^2\, dx \int_{-\infin}^\infin |\phi(x) |^2\, dx​$ (Cauchy Schwarz)

and $\int_{-\infin}^\infin |f(x) - f_n(x)|^2\, dx \rightarrow 0$ as $n \rightarrow \infin$ 

so $|\int_{-\infin}^\infin f(x) \phi(x)\, dx - \int_{-\infin}^\infin f_n(x) \phi(x)\, dx| \rightarrow 0$ as $n \rightarrow \infin$ so $f_n$ converges weakly in the sense of distributions

##### Exercise 8

**(a)** 

Show that the product $\delta(x)\delta(y)\delta(z)$ makes sense as a three-dimensional distribution


linearity: $(\delta(x)\delta(y)\delta(z), a\phi + b \psi) = \iiint \delta(x)\delta(y)\delta(z)[a \phi(\bold x)+ b \psi(\bold y)]\, d \bold x $
$= a\iiint \delta(x)\delta(y)\delta(z) \phi(\bold x)\, d\bold x + b \iiint \delta(x)\delta(y)\delta(z) \psi \, d \bold x\\= a(\delta(x)\delta(y)\delta(z), \phi) + b(\delta(x)\delta(y)\delta(z), \psi)$ 

Continuity: Assuming $\phi_n(\bold x) \rightarrow \phi(\bold x)$ uniformly for a sequence of test functions, we should have $|\iiint \delta(x)\delta(y)\delta(z) \phi_n(\bold x)\, d\bold x - \iiint\delta(x)\delta(y)\delta(z)\phi(\bold x) d\bold x| $ 
$\leq \iiint |\delta(x)\delta(y)\delta(z)||\phi_n(\bold x) - \phi(\bold x)|\, d\bold x$  and $|\phi_n(\bold x) - \phi(\bold x)| \rightarrow 0$ uniformly, so we have $\iiint \delta(x)\delta(y)\delta(z) \phi_n(\bold x) \, d \bold x\rightarrow \iiint \delta(x)\delta(y)\delta(z) \phi(\bold x)\, d \bold x$ 

**(b)** 

Show that $\delta(\bold x) = \delta(x)\delta(y)\delta(z)$, where the first delta function is the three-dimensional one

---

$\iiint \delta(x)\delta(y)\delta(z) \phi(\bold x)\, d \bold x$ For some test function $\phi(\bold x) = \phi(x, y, z)$ 

then we have: $\int_{-\infin}^\infin \delta(z) \int_{-\infin}^\infin \delta(y) [\int_{-\infin}^\infin \delta(x)\phi(x, y, z) \, dx] \, dy \, dz$ 
$ = \int_{-\infin}^\infin \delta(z) [\int_{-\infin}^\infin \delta(y) \phi(0, y, z)\, dy] \, dz$ 

$ = \int_{-\infin}^\infin \delta(z) \phi(0, 0, z)\, dz$ $= \phi(\bold 0)$ 

##### Exercise 9

Show that no sense can be made of the square $[\delta(x)]^2​$ as a distribution

---

Taking $\int_{-\infin}^\infin \delta(x)\cdot \delta (x) \, dx = \delta(0)$, but $\delta(0) = +\infin$ 
so $\int_{-\infin}^\infin \delta(x)\cdot(\delta(x)\phi(x))\, dx$ where $\phi(x)$ is a test function gives
$= \phi(0) \delta(0) = +\infin$ since $\phi(0)$ is finite
So no matter what: $(\delta^2, \phi) = +\infin$, which isn't in $\R$ 

##### Exercise 11

Verify that if $C$ is a smooth curve in space, then the line integral over $C$ defines the distribution $\phi \mapsto \int_C \phi \, ds$

---

Linearity: parametrizing $C = (f(t), g(t))​$ from $t =a ​$ to $t = b​$ 

$\int_C c \phi + d \psi \, ds = \int_a^b [c\phi(f(t), g(t)) + d \psi(f(t), g(t))]\sqrt{(\frac{df}{dt})^2 +(\frac{dg}{dt})^2 }dt​$

$ = c \int_a^b \phi(f(t), g(t))\sqrt{(\frac{df}{dt})^2 + (\frac{dg}{dt})^2} dt + d \int_a^b \psi(f(t), g(t))\sqrt{(\frac{df}{dt})^2 + (\frac{dg}{dt})^2}dt$ 
$= c \int_C \phi\, ds + d \int_C \psi \, ds$ 

Continuity: $\phi_n \rightarrow \phi$ uniformly: 
$|\int_C \phi_n \, ds - \int_C \phi \, ds| \leq |\int_C |\phi_n - \phi|\, ds$ $\rightarrow 0$ as $n \rightarrow \infin$ 

##### Exericse 12

Let $\chi_a(x) = 1/2a​$ for $-a <x <a​$ and $\chi_a(x) = 0​$ for $|x| > a​$ . Show that $\chi_a \rightarrow \delta​$ weakly as $a \rightarrow 0​$ 

---

$\int_{-\infin}^\infin \chi_a(x)\phi(x)\, dx = \int_{-a}^a \chi_a(x)\phi(x)\, dx$ = $\frac{1}{2a}\int_{-a}^a \phi(x)\, dx = \overline{\phi}(x)$ and as $a \rightarrow 0$, $\overline{\phi}(x) \rightarrow \phi(0)$ 

#### Section 12.2: Exercises 4—7, 13

##### Exercise 4 

Let $S(x, t)​$ be the source function (Riemann function) for the one-dimensional wave equation. Calculate $\partial S/\partial t​$ and find the PDE and initial conditions that it satisfies. 

---

$$
S(x, t) = \begin{cases} \frac{1}{2c}& \text{for }|x| <ct \\ 0 & \text{for }|x| > ct \end{cases}
$$





As $t \uparrow \infin$, $S(x,t) \rightarrow \frac{1}{2c}$ 

Along $|x| = ct$, $S_t(x, t) = \infin$ // or there's a jump here

Otherwise $S_t(x, t) = 0$ 

so $S_t(x, t) = \begin{cases} 0& \text{for }|x| < ct \\ 0 & \text{for }|x| > ct \\ \infin & \text{for }|x| = ct \end{cases}$ 

so $S_t(x, t) = \delta(x^2 - c^2t^2)$ or $S_t(x,t) = \delta(|x| - ct)$ for $t > 0$ (and $c > 0$? ) 

$S_{tt}(x, t) = -c \delta'(|x| - ct)$ (but $\delta$ isn't really differentiable, so I don't know how valid this is)

so starting with$S(x, 0)$, which is essentially just the  $0$ function, then increasing in time $t$, the first point to have $S(x,0)$ jump from $0$ to $\frac{1}{2c}$ is $x = 0$, so $S_t(x, 0) = \delta(x)​$. 

and from there the interval on the real line s.t. $S = \frac{1}{2c}$ stretches out from the middle. 

$t = 0$: $(0, 0)$, $t = 1$: $(-c, c)$ , $t = 2: (-2c, 2c)$… so the interval increases in length by $2c$ as $t$ increases by 1. 

Then Taking $(7)​$, in one dimension:

$S_{tt} = c^2 S_{xx}$ then
$(S_{tt})_t = (c^2 S_{xx})_t$ 
$S_{ttt} = c^2 S_{xxt}$ 

$(S_{t})_{tt} = c^2(S_t)_{xx}​$ 

##### Exercise 5

A force acting only at the origin leads to the wave equation $u_{tt} = c^2 \Delta u + \delta(\bold x)f(t)​$ with vanishing initial conditions. Find the solution

---

From chapter 9:

$u(\bold x, t) = \frac{1}{4 \pi c^2} \iiint_{\{|\bold \xi - \bold x| \leq ct \}}\frac{\delta(\bold \xi) f(t - |\bold \xi - \bold x|/c)}{|\bold \xi - \bold x|} d \bold \xi$ $= $ $\frac{1}{4 \pi c^2} \frac{f(t - |\bold x|/c)}{|\bold x|}$ let $r = |\bold x|​$ and we get the answer in the back of the book. 

(this is due to the nature of the dirac delta $\int \delta(\bold x) f(\bold x) \, d \bold x = f(0)$, but in this case we use $\bold \xi = 0$ 

so for $|\bold x| > ct$, $\xi$ for $|\xi - \bold x| \leq ct$ cannot be $\bold 0$, so $\delta(\xi) = 0$ 
and then for $|\bold x| < ct$ we get the above answer. 

##### Exercise 6

Find the formula for the general solution of the inhomogeneous wave equation in terms of the source function $S(\bold x, t)​$ 

---

$$
u_{tt} -c^2 \Delta u = f(\bold x, t) 
\\ u(\bold x, 0) = \phi \quad u_t(\bold x, 0) = \psi
$$

$u(\bold x, t) = \frac{1}{4 \pi c^2} \iiint_{\{|\bold \xi - \bold x| \leq ct \}} \frac{f(\bold \xi, t - |\bold \xi - \bold x|/c)}{|\bold \xi - \bold x|} \, d \bold \xi$ 

$\iiiint S(\bold x - \bold y, t-s) f(\bold y,s ) \, d \bold x \, d s$ $ = \frac{1}{4 \pi c^2} \iiint_{\{\bold \xi - \bold x| \leq ct \}} \frac{f(\bold \xi, t - |\bold \xi - \bold x|/c)}{|\bold \xi - \bold x|} d\bold \xi$ 

$S(\bold x, t) = \frac{1}{4 \pi c^2 t}\delta(ct - |\bold x|)$ $t \geq 0$ 

The homogeneous part:

$u(\bold x, t_0) = \frac{1}{4 \pi c^2 t_0} \iint_S \psi(\bold x)\, dS + \frac{\partial }{\partial t_0}[\frac{1}{4 \pi c^2 t_0}\iint_S \phi(\bold x)\, dS]$ 

$S(\bold x, t_0) = \frac{1}{4 \pi c^2 t_0} \delta(ct - |\bold x|)$  

$u_h(\bold x, t) = \int S(\bold x - \bold y, t) \psi(\bold y) \, d\bold y + \frac{\partial}{\partial t}[\int S(\bold x - \bold y, t) \phi(\bold y) \, d \bold y]​$ 

From chapter 9, section 3: (page 246)

Noting: $S(\bold y, t- s) = \frac{1}{4 \pi c^2(t-s)}\delta(c(t-s) - |\bold y|)$ 

and $\int_{\R^3} \delta(|\bold y|) f(\bold y, t) \, d \bold y = f(\bold 0, t)​$ 

$u_p(\bold x, t) = \int_0^t \int_{\R^3} \iint_{|\bold \xi - \bold x| = |\bold y|} S(\bold y , t-s) f(\bold \xi, s) \, dS_{\bold \xi} d \bold y \, ds$

$ = \int_0^t \frac{1}{4 \pi c^2 (t-s)} \int_{\R^3} \iint_{|\xi - \bold x| = |\bold y|} \delta(c(t-s)- |\bold y|) f(\bold \xi, s ) dS_{\bold \xi} d \bold y\, ds$ 

$\int_{\R^3} \delta(c(t-s) - |\bold y|)\iint_{|\bold \xi - \bold x| = |\bold y|}  f(\bold \xi, s)dS_{\bold \xi}d\bold y = $ $\iint_{|\xi - \bold x| = c(t-s)} f(\bold \xi, s)dS_{\xi} ds$ 

$u(\bold x, t) = \int S(\bold x - \bold y, t) \psi(\bold y) \, d\bold y + \frac{\partial}{\partial t}[\int S(\bold x - \bold y, t) \phi(\bold y) \, d \bold y] + \int_0^t \int_{\R^3} S(\bold y, t-s) \iint_{|\xi - \bold x| = |\bold y|} f(\bold \xi, s)\, dS_{\xi}d\bold y\, ds$ 

##### Exercise 7 

Let $R(x, t) = S(x - x_0, t - t_0)​$ for $t > t_0​$ and let $R(x,t) \equiv 0​$ for $t < t_0​$. Let $R(x, t_0)​$ remain undefined. Verify that $R​$ satisfies the inhomogeneous diffusion equation 
$$
R_t - k \Delta R = \delta(x-x_0)\delta(t-t_0)
$$

---

$R(x, t) = S(x-x_0, t-t_0)$  

$k\Delta R = R_t - \delta(x-x_0)\delta(t-t_0)$ 

$(R_t - k R_{xx}, \phi)​$ $= (R_t,\phi) - k(R_{xx}, \phi)​$ $=- (R, \phi_t) + k(R_{x}, \phi_{x})​$ 
$ = -(R, \phi_t) - k(R, \phi_{xx})​$ 

$\int_{-\infin}^\infin [-R \phi_t]\, dx - k \int_{-\infin}^\infin R \phi_{xx}\, dx$ $ = \int_{-\infin}^\infin -S(x-x_0, t-t_0)\phi_t(x, t)\, dx - k \int_{-\infin}^\infin S(x-x_0, t-t_0)\phi_{xx}(x, t)\, dx$ 

and as $t \rightarrow t_0$ , $\int_{-\infin}^\infin S(x-x_0, t-t_0)\phi(x, t)\, dx \rightarrow \phi(x_0, t_0)$

$ = - \phi_t(x_0, t_0) -k \phi_{xx}(x_0, t_0)$  

---

$D:$ $\{(x, t): t > t_0\}$ so the normal on this will be $(0, -1)$ 

So using Green's Second Identity:

$\int_{-\infin}^\infin  -\phi R_t  + R \phi_t \, dx$ $ = \int_{t_0}^\infin \int_{-\infin}^\infin \phi R_{xx} - R \phi_{xx} \, dx \, dt$ 

and since $R_{xx} = \frac{1}{k}R_t$ in $D$: 

$ = \int_{-t_0}^\infin \int_{-\infin}^\infin \frac{1}{k}\phi R_t - R \phi_{xx}\, dx \, dt​$ 

$ = \int_{t_0}^\infin \int_{-\infin}^\infin - \frac{1}{k}\phi_t R - R \phi_{xx}\, dx \, dt$ 

and noting: $(R_t - k R_{xx}, \phi)$ $= (R_t,\phi) - k(R_{xx}, \phi)$ $=- (R, \phi_t) + k(R_{x}, \phi_{x})$ 
$ = -(R, \phi_t) - k(R, \phi_{xx})$ 

$k \int_{-\infin}^\infin - \phi R_t + R \phi_t \, dx =  \int_{t_0}^\infin  \int_{-\infin}^\infin (R_t - k R_{xx})\phi \, dx \, dt​$

Again $R_t = k R_{xx}$, so:

$k\int_{-\infin}^\infin - \phi k R_{xx} + R \phi_t \, dx =\int_{t_0}^\infin  \int_{-\infin}^\infin (R_t - k R_{xx})\phi \, dx \, dt$

also: $(R, \phi_t) = - (R_t, \phi) = -(k R_{xx}, \phi)$ so:

$k^2 \int_{-\infin}^\infin - 2 \phi R_{xx}\, dx = \int_{t_0}^\infin  \int_{-\infin}^\infin (R_t - k R_{xx})\phi \, dx \, dt$ 

Left side = $-2k^2 (R_{xx}, \phi) = -2k^2(R, \phi_{xx})$ 

$ = -2k^2 \int_{-\infin}^\infin S(x-x_0, t-t_0)\phi_{xx}\, dx$= $-2k^2 \phi_{xx}(x_0, t_0)$ 

##### Exercise 13

Calculate the distribution $\Delta(\text{log }r)$ in two dimensions

---

Green's First identity holds in two dimensions since we still have $\nabla \cdot (v \nabla u) = \nabla v \cdot \nabla u + v \Delta u$  

so integrating  and using Green's Theorem: $\iint_D \nabla \cdot (v \nabla u) \, dx dy = \int_C v \frac{du}{dn}\, ds = \iint_D \nabla v \cdot \nabla  u\,  dx\, dy + \iint_D v \Delta u\, dx\, dy$ 

Then we can get Green's second Identity in 2 dimensions as well:

$\int_C u \frac{dv}{dn} - v \frac{du}{dn} \, ds = \iint_D u \Delta v - v \Delta u \, dx \, dy$ 

Letting $u = \phi$ in $D$ and let $v = \log r$ 

$\iint_D \phi \Delta (\log r) - \log r \Delta \phi \,dx\, dy$ $ = \int_C \phi \frac{d \log r}{dn} - \log r \frac{d\phi}{dn}\, ds$ 

Taking $D$ to be the set of points exterior to some circle of radius $R$ and using both 1: that $\log r$ is harmonic, $\Delta(\log r) = 0$ , and 2:  that the normal points towards the center on the circle: $\frac{\partial}{\partial n} = -\frac{\partial }{\partial r}$ 

$-\iint_{D} \log r \Delta \phi \, dx \, dy = \int_C -\phi \cdot \frac{1}{r} +\log r \frac{\partial \phi}{\partial r}\, ds​$ and on $C​$, $r = R​$ 

$- \iint_D \log r \Delta \phi \, dx \, dy = \int_C -\frac{\phi}{R} + \log R \frac{d\phi}{dr}\, ds$ 

$\iint_D \log r \Delta \phi \, dx \,dy = \frac{1}{R} \int_{r = R} \phi ds - \log R \int_{r = R} \frac{d \phi}{dr}\, ds$ 

$\frac{1}{2\pi } \iint_D \log r \Delta \phi \, dx \,dy = \frac{1}{ 2 \pi R} \int_{r = R}\phi \, ds - \frac{R \log R}{2 \pi R}\int_{r = R}\frac{d\phi}{dr}\, ds​$  

$ = \overline{\phi} - R \log R \overline{\frac{\partial \phi}{\partial r}}$ 
taking the limit as $R \rightarrow 0$, the second term vanishes and $\overline \phi \rightarrow \phi(\bold 0)$ 
so we obtain

$\iint_D \log r \Delta \phi \, dx \, dy = 2\pi \phi(\bold 0)​$ 

so we have $\Delta(\log r)$ (in terms of the book) is equal to $2\pi \delta(\bold x)$ in 2 dimensions

I suppose $\Delta( \frac{log r}{2\pi})$ would be $\delta(\bold x)$ in 2 dimensions
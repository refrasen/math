$(Z_1, Z_2) = \frac{1}{||Y_2||}(Z_1, X_2 - (X_2, Z_1)Z_1) = \frac{1}{||Y_2||}[(Z_1, X_2) - (Z_1, (X_2, Z_1) Z_1)]$ $= \frac{1}{||Y_2||} [(Z_1, X_2) - \overline{(X_2, Z_1)}(Z_1, Z_1)]$ $ = \frac{1}{||Y_2||}(Z_1, X_2)(1- (Z_1, Z_1))$
and since $(Z_1, Z_1) = (\frac{X_1}{||X_1||}, \frac{X_1}{||X_1||}) = \frac{1}{||X_1||^2}(X_1, X_1) = 1$, so $(Z_1, Z_2) = \frac{1}{||Y_2||}(Z_1, X_2)(1- (Z_1, Z_1)) = 0$  

Using the definition in the book fo an inner product:

$(Z_1, Z_2) = (Z_1, \frac{Y_2}{||Y_2||}) = \frac{1}{||Y_2||}\int_a^b Z_1 \overline{Y_2} \; dx$ $ = \frac{1}{||Y_2||}\int_a^b Z_1 \overline{(X_2 - (X_2, Z_1)Z_1} \; dx = \frac{1}{||Y_2||} \int_a^b Z_1\overline{X_2} - Z_1\overline{(X_2, Z_1)}Z_1\; dx $ 
and since $\overline{(f, g)} = \int_a^b \overline{f\overline g }\; dx = \int_a^b g \overline f \; dx = (g, f)$

$= \frac{1}{||Y_2||} \int_a^b Z_1\overline{X_2} - Z_1^2(Z_1, X_2)\; dx$ $ = \frac{1}{||Y_2||} [\int_a^b Z_1 \overline X_2 \; dx - (\int_a^b Z_1\overline X_2 \; dx ) \int_a^b Z_1^2 \; dx]$ 

(able to pull the $(Z_1,X_2)$ $= \int_a^b Z_1 \overline{X_2} \; dx$ out because it is a scalar.) and since $Z_1 = \frac{X_1}{||X_1||}$, $(Z_1, Z_1) = 1$, so we have  $ \frac{1}{||Y_2||} [\int_a^b Z_1 \overline X_2 \; dx - (\int_a^b Z_1\overline X_2 \; dx ) \int_a^b Z_1^2 \; dx] = \frac{1}{||Y_2||} [ \int_a^b Z_1 \overline{X_2} \; dx - \int_a^b Z_1 \overline{X_2} \; dx ] = 0$ 

**Using the properties of inner products**: $(x,y) = \overline{(y,x)}$, $(ax, y) = a(x,y)$, $(x, ay) = \overline a(x, y)$, $(x+y, z) = (x, z) + (y, z)$, $(x, x) = \overline{(x,x)}$, $(x, y+z) = (x, y) + (x, z)$, $(-x, x) = (x, -x)$ 

Note: if $a$ is real, then $(ax, y) = a(x,y) = (x, ay)$, and $(x,x)$ is real, so $||x||$ is real.

$(Z_1, Z_2) = \frac{1}{||Y_2||}(Z_1, X_2 - (X_2, Z_1)Z_1) = \frac{1}{||Y_2||}[(Z_1, X_2) - (Z_1, (X_2, Z_1) Z_1)]$ $= \frac{1}{||Y_2||} [(Z_1, X_2) - \overline{(X_2, Z_1)}(Z_1, Z_1)]$ $ = \frac{1}{||Y_2||}(Z_1, X_2)(1- (Z_1, Z_1))$
and since $(Z_1, Z_1) = (\frac{X_1}{||X_1||}, \frac{X_1}{||X_1||}) = \frac{1}{||X_1||^2}(X_1, X_1) = 1$, so $(Z_1, Z_2) = \frac{1}{||Y_2||}(Z_1, X_2)(1- (Z_1, Z_1)) = 0$ 



Let $\{x_1, …, x_n\}$ be the finite set of points where $f(x)$ has jump discontinuities. 

and $f(x)$ is continuous in $(x_{i-1}, x_{i})$, for $i = 2, …, n$, so we know the definite integral $\int_{x_{i-1}}^{x_{i}} f(s)\; ds$ exists and is finite since $f(x_i +)$ and $f(x_i -)$ exist and are finite, so there exists a maximum $M$ of $f(x)$ in $[x_{i-1}, x_i]$
We set $I_i = \int_{x_{i-1}}^{x_i} f(s) \; ds $ for $i = 2, …, n$ and let $I_1 = \int_{-l}^{x_1} f(s)\; ds$ 

for some $x_0 \in [-l, l]$, such that $x_1< …< x_k < x_0 < x_{k+1} < … < x_n$  

$\lim_{x \rightarrow x_0} F(x) = \lim_{x \rightarrow x_0} \int_{-l}^{x} f(s) \; ds$ $ = \lim_{x \rightarrow x_0} (\int_{-l}^{x_1} f(s) \; ds + \int_{x_1}^{x_2}f(s) \; ds + \cdots + \int_{x_k}^{x} f(s) \; ds) $ $ = \lim_{x \rightarrow x_0} (I_1 + … + I_k + \int_{x_k} ^x f(s) \; ds)​$ 

so $F(x_0) = (I_1 + … + I_k +

$x = \sum_{n=1}^\infin A_n \cos \frac{n \pi x}{l} + B_n \sin \frac{n \pi x}{l}$ 

$A_n = \frac{1}{l} \int_{-l}^l x \cos \frac{n \pi x}{l} \; dx$ $ = - \frac{1}{n\pi}\int_{-l}^l \sin \frac{n \pi x}{l} \; dx$ $= -\frac{1}{n\pi}\frac{l}{n\pi}[(-1)^n - (-1)^n] = 0 $ 

$B_n = \frac{1}{l} \int_{-l}^l x\sin \frac{n \pi x}{l} \; dx$ $ = \frac{1}{n\pi}[-l(-1)^n - l(-1)^n] + \frac{1}{n\pi}\int_{-l}^l\cos \frac{n\pi x}{l} \; dx $



$||X_n||^2 c_n^2 -2 (f, X_n)c_n​$ $ = (||X_n||c_n - \frac{(f, X_n)}{||X_n||})^2 - \frac{(f, X_n)^2}{||X_n||^2}​$ 
$= ||X_n||^2 (c_n - \frac{(f, X_n)}{||X_n||^2})^2 - \frac{(f,X_n)^2}{||X_n||^2}​$


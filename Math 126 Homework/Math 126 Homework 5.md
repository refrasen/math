Renee Senining
Math 126
**Homework 5** 

#### Section 5.3: Exercises 2, 3, 5, 8, 9, 10

##### **Exercise 2**

(**a**) $f(x) = x​$, $g(x) = c​$, where $c​$ is a constant
$(x, c) = \int_{-1}^1 xc\;  dx = c \int_{-1}^1 x \; dx = c \frac{x^2}{2}|_{-1}^1 = c[1/2 - 1/2] = c[0] = 0​$ 

**(b)** so let $f(x) = x^2$, and we can use gram-schmidt to make it orthogonal to $x$ and $1$ 
we know $x$ is orthogonal to $1$ by (a)
$f_{orth} = x^2 - \frac{(x, x^2)}{(x,x)} x - \frac{(1, x^2)}{(1,1)} $ 
so $(x, x^2) = \int_{-1}^{1} x^3 \; dx = \frac{1}{4} -\frac{1}{4} = 0$ , no need to compute $(x,x)$ then
$(1, x^2) = \int_{-1}^1 x^2 \; dx = \frac{1}{3} + \frac{1}{3} = \frac{2}{3}$ 
$(1,1) = \int_{-1}^1 1 \; dx = 2$ 
$f_{orth} = x^2 - \frac{1}{3}$ We could multiply this by $3$ to get the answer in the back of the book… since if $\int fg \; dx = 0$, $\int (cf)g \; dx = c\int fg \; dx = c*0 = 0$ 
**(c)** Using gram-schmidt again:

$x^3 - \frac{(3x^2 - 1, x^3)}{(3x^2-1, 3x^2 - 1)}(3x^2 - 1) - \frac{(x, x^3)}{(x, x)}x - \frac{(1, x^3)}{(1, 1)} $ 
$(3x^2-1, x^3)$ $= \int_{-1}^1 3x^5 - x^3 \; dx$, and $3x^5 - x^3$ is odd, so this is $0$
$(x, x^3) = \int_{-1}^1 x^4 \; dx$ $ = \frac{2}{5}$ ($x^4$ is even and $\int_0^1 x^4 dx = 1/5$ 
$(x, x)$ $= \int_{-1}^1 x^2 \; dx = \frac{2}{3}$ 
$(1, x^3)$ $= \int_{-1}^1 x^3 \; dx = 0$ 
$x^3 - 0 - \frac{2/5}{2/3}x - 0$ $= x^3 - \frac{3}{5}x$ and using the fact that if $(f,g) = 0$, $(cf, g) = 0$, so $Ax^3 - \frac{3}{5} Ax$ for any constant $A$ is orthogonal

##### Exercise 3

$$
u_{tt} = c^2 u_{xx} \qquad 0 <x<l
\\ \text{BC:  }u(0,t) = u_x(l, t) = 0 
\\ \text{IC: } u(x,0)  = x, u_t(x,0) = 0
$$

Separation of variables: $u(x,t) = X(x)T(t)$ 
$X(x)T''(t) = c^2 X''(x) T(t)$ $\implies$ $\frac{T''}{c^2 T} = \frac{X''}{X} = -\lambda$ 
Then from homework 4, problem 4.2.1:
we know our eigenvalues are positive and real, and they are $\lambda = \frac{ (n+1/2)^2 \pi^2}{l^2}$, so we set $\beta_n = \frac{(n+1/2) \pi }{l}$ so that our eigenfunctions are $\sin \frac{(n+1/2) \pi x}{l}$ or $\sin \beta_n x$, and we know that the solution to the ODE $T'' + \beta^2 c^2 T = 0$ is $A \cos \beta c t + B \sin \beta c t$  so
$u(x,t) = \sum_{n = 0}^ \infin (A_n \cos \beta_n c t + B_n \sin \beta_n c t) \sin \beta _n x$ 
$u_x(x,0) = \sum_{n=0}^\infin \frac{B_n}{\beta_n} \sin \beta_n x$ = $0$, so $B_n = 0$ 
$u(x,0) = x = \sum_{n=0}^\infin A_n \sin \beta_n x$, so 
$A_n = \frac{(x, \sin \beta_n x)}{(\sin \beta_n x \sin \beta_n x)}$
First: $\int_0^l \sin ^2 \beta_n x \; dx$ $= \frac{1}{2} \int_0^l 1 - \cos(2\beta_n x) \; dx = \frac{1}{2} [x - \frac{1}{2\beta_n}\sin 2\beta_n x]_0^l$, where $2 \beta_n l = (2n + 1) \pi$, so we end up with $\frac{l}{2}$ 
Second: $\int_0^l x \sin \beta_n x \; dx$ $ = \frac{1}{\beta_n}[- x \cos \beta_n x|_0^l + \int_0^l \cos \beta_n x \; dx]$ 
$ = \frac{1}{\beta_n}[-x \cos \beta_n x + \frac{1}{\beta_n} \sin \beta_n x]_0^l$ and since $\beta_n l = (n + 1/2) \pi$, 

$= \frac{1}{\beta_n}[\frac{1}{\beta_n} (-1)^n]$ $= \frac{l^2}{(n+1/2)^2 \pi^2} (-1)^n$ 
so $A_n = (-1)^n  \frac{2l}{(n+1/2)^2 \pi^2}$ 
$u(x,t) = \frac{2l}{\pi^2} \sum_{n=0}^\infin \frac{(-1)^n}{(n+1/2)^2} \cos \frac{(n+1/2) \pi c t}{l} \sin \frac{ (n+1/2) \pi x}{l}$ 
we can write $m = n - 1$ and multiply by $2^2/2^2$ so we get

$u(x,t) = \frac{8l}{\pi^2} \sum_{m = 1}^\infin \frac{(-1)^{m+1}}{(2m - 1)^2} \cos \frac{(2m-1) \pi c t}{2l} \sin \frac{(2m - 1) \pi x}{2l}$

##### Exercise 5 

**(a)**: $-X'' = \lambda X$ 
$\lambda$ <u>negative</u>: $\lambda = - \beta^2$, so $- X'' = -\beta^2 X$, so $r^2 = \beta^2$ $\implies$ $r = \pm \beta$ 
so we obtain $Ce^{\beta x} + D e^{-\beta x}$, but using new variables:
$C = \frac{C_1 + C_2}{2}, D = \frac{C_1 - C_2}{2}$, to get $C_1 \cosh x + C_2 \sinh x$ 
and at $x = 0$, $C_1 \cosh 0 + C_2 \sinh 0 = 0$ $\equiv$ $C_1 + C_2 = 0$ 
and at $x = l$, $C_1 \sinh l + C_2 \cosh l = 0$ $\implies$ $C_1(\sinh l - \cosh l ) = 0$, but $\sinh l - \cosh l = -e^{-l} \neq 0$ so $C_1 = 0 = -C_2$, but $0$ cannot be an eigenfunction. 
$\lambda$ = $0$: gives $-X'' = 0$, so $X(x) = Ax + B$, and $X(0) = B = 0$ and $X'(l) = A = 0$, and again, $0$ cannot be an eigenfunction

$\lambda$ <u>complex</u>: let $\gamma = \sqrt{-\lambda}$, so $\gamma = a + ib$, so we get $X(x) = Ce^{\gamma x} + D e^{-\gamma x}$ where $X(0) = C + D = 0$, so $-C = D$ and $X'(l) = \gamma(Ce^{\gamma l} - D e^{-\gamma l})$, since $\gamma \neq 0$, $Ce^{\gamma x } - De ^{-\gamma l} = 0$ 
$\equiv$ $Ce^{2\gamma l} - D = 0$, so we'd need $e^{2 \gamma l} = -1$, so $e^{2\gamma l} = e^{2 a l }(\cos 2bl + i \sin 2bl)$ $\implies$ $a = 0$, $2bl = (2n+1)\pi$   
so $b = \frac{(2n+1)}{2l} \pi$ but this means $\lambda  = -\gamma^2 = \frac{(2n+1)^2}{4l^2} \pi^2$, which is real and positive for $n = 0, 1, 2…$ 
so the eigenfunction: $A \sin bx + B \cos bx$, and $X(0) = B = 0$, so we get: $\sin bx =\sin \frac{(2n+1)}{2l} \pi$  as eigenfunctions ($n = 0, 1, 2…$) 

**(b)**: $\tilde \phi (x) = \begin{cases} \phi(x) & 0 \leq x \leq l \\ \phi(2l - x) & l \leq x \leq 2l \end{cases}$ 

(extending evenly across $x = l​$ so $\phi(l - x) = \phi(l + x)​$ for $x \in [0, l])​$   Finding the Fourier sine series for $\tilde \phi​$ on the interval $(0, 2l)​$ and its coefficients: 

$\tilde \phi(x) = \sum_{n=1}^\infin A_n \sin \frac{n \pi x}{2l}$, with $A_n = \frac{(\tilde \phi, \sin \frac{n \pi x}{2l})}{(\sin \frac{n \pi x}{2l}, \sin \frac{n \pi x}{2l})}$  
$(\sin \frac{n \pi x}{2l}, \sin \frac{n \pi x}{2l}) = \int_0^{2l} \sin ^2 \frac{n \pi x}{l} \; dx = \frac{2l}{2} = l$ 
$(\tilde \phi, \sin \frac{n \pi x}{2l}) = \int_0^{2l} \tilde \phi \sin \frac{n \pi x}{2l} \; dx$ so we obtain

$A_n = \frac{1}{l} \int_0^{2l} \tilde \phi(x) \sin \frac{n \pi x}{2l} \; dx$   

**(c)**: Show that every second coefficient vanishes

$A_n = \frac{1}{l} [ \int_0^l \tilde \phi(x) \sin \frac{n \pi x}{2l} \; dx + \int_l^{2l} \tilde \phi(x) \sin \frac{n \pi x}{2l}]$ 
$= \frac{1}{l}[ \int_0^l \phi(x) \sin \frac{n \pi x}{2l} \; dx + \int_l^{2l} \phi(2l - x) \sin \frac{n \pi x}{2l} \; dx]$ in the second integral, we use $u = 2l - x$, so $x = 2l - u$, $du = -dx$ 
focusing on second integral: $\int_0^l \phi(u) \sin \frac{n \pi (2l - u)}{2l} du​$ 

regarding the $\sin$ term: $\sin( \frac{n \pi 2l}{2l} - \frac{n \pi u}{2l})  = \sin (n\pi)\cos (n\pi u/2l) - \sin(n\pi u/2l)\cos(n\pi)\\ = (-1)^{n+1} \sin (n\pi u/2l)$  
so $A_n = \frac{1}{l} [ \int_0^l \phi(x)[\sin \frac{n \pi x}{2l} + (-1)^{n+1} \sin \frac{n \pi x}{2l}]dx]$

so when $n$ is even, $A_n$ disappears. so we must have
$\sum_{n = 0}^\infin C_n \sin \frac{(2n+1) \pi x}{2l} = \sum_{n=0}^\infin C_n \sin \frac{(n + 1/2) \pi x}{l}$

**(d)**: Rewrite the formula for $C_n$ as an integral of the original function $\phi(x)$ on the interval $(0,l)$ 
Returning to the integral in $(c)$ for $A_n$, we have that at odd $n$'s, 

$C_n = A_n = \frac{2}{l} \int_0^l \phi(x) \sin \frac{n \pi x}{2l} dx$ for $n$ odds, so we rewrite it as:
$C_n = \frac{2}{l} \int_0^l \phi(x) \sin \frac{(n + 1/2) \pi x}{l} \; dx$ 

##### Exercise 8

Robin Boundary Conditions are of the form
$$
X' - A_a X = 0
\\ X' + A_b X = 0
$$
so $X_1$ and $X_2$ both satisfy the above. Now consider:
$-X_1'(b)X_2(b) + X_1(b) X_2'(b) + X_1'(a)X_2(a) - X_1(a)X_2'(a)$ 
Since $X_1'(b) = -A_b X_1(b)$, $X_2' (b)= -A_b X_2(b)$ and
$X_2'(a) = A_a X_1(a), X_2'(a) = A_a X_2(a)$, we obtain:

$\underbrace{-(-A_bX_1(b))X_2(b) - A_b X_1(b) X_2(b)}_{=0} + \underbrace{A_aX_1(a)X_2(a) - A_1X_1(a)X_2(a)}_{= 0} = 0$  

##### Exercise 9

$X(b) = \alpha X(a) + \beta X'(a)$ and $X'(b) = \gamma X(a) + \delta X'(a)$, $\alpha \delta - \beta \gamma = 1$ 

$\alpha X(a) - X(b) + \beta X'(a) = 0 \\ \gamma X(a) +\delta X'(a) - X'(b) = 0$

let $f, g​$ satisfy the above pair of boundary conditions
$f'(b)g(b) - f(b)g'(b) - f'(a)g(a) + f(a)g'(a)​$ 
$g(b) = \alpha g(a) + \beta g'(a)​$, $g'(b) = \gamma g(a) + \delta g'(a)​$ and similalry for $f​$, 
$[\gamma f(a) + \delta f'(a)][\alpha g(a) + \beta g'(a)] - [\alpha f(a) + \beta f'(a)][\gamma g(a) + \delta g'(a)] - f'(a)g(a) + f(a)g'(a)​$

$ = \gamma \alpha [f(a) g(a) - f(a)g(a)] + \beta \gamma [f(a) g'(a) - f'(a)g(a)]+ \alpha \delta [f'(a) g(a) - f(a)g'(a)] + \beta \delta[ f'(a)g'(a) - f'(a)g'(a)] - f'(a)g(a) + f(a)g'(a)$ $ =[{\beta \gamma - \alpha \delta} + 1] f(a)g'(a) + [{\alpha \delta - \beta \gamma} - 1] f'(a)g(a) = 0$ 
Up until this point, we haven't used $\alpha \delta - \beta \gamma = 1$, but obviously:
if $\alpha \delta - \beta \gamma = 1$, it is symmetric. AND if it is symmetric, we need $\beta \gamma - \alpha \delta + 1 = 0$ $\implies$ $\alpha \delta - \beta \gamma = 1$ 
therefore, the above boundary conditions are symmetric if and only if $\alpha \delta - \beta \gamma = 1$ 

##### Exercise 10

**Using the definition in the book and proving properties of inner products**
$(f, g) = \overline{(g, f)}$: this is due to $\overline{g \overline{f}} = f \overline{g}$ (conjugate symmetry)
$(cf, g) = c(f, g)$: $\int_a^b (cf)\overline g\; dx = c \int_a^b f \overline g \; dx$ 
$(f, cg) = \overline{(cg, f)} = \overline c \overline{(g, f)} = \overline c (f, g)$ and if $c$ is real, $= c(f,g)$ 
$(f+g, h) = \int_a^b f\overline h + g \overline h \; dx = \int_a^b f \overline h \; dx + \int_a^b g\overline h \; dx = (f, h) + (g, h)$ 
$(f, g+h) = \int_a^b f\overline g + f \overline h \; dx = \int_a^b f \overline g \; dx + \int_a^b f \overline h \; dx = (f, g) + (f, h)​$ 

because $(f, g) = \overline{(g, f)}$, $(f,f) = \overline{(f, f)}$, so $(f, f)$ is real, and so is $||f||$ 

**Using Strong Induction:**

<u>Base case</u>: $i =2 $: then we have $Z_1 = \frac{X_1}{||X_1||}$, $Z_2 = \frac{Y_2}{||Y_2||}$, where $Y_2 = X_2 - (X_2, Z_1)Z_1$ 

$(Z_1, Z_2) = \frac{1}{||Y_2||}(Z_1, X_2 - (X_2, Z_1)Z_1) = \frac{1}{||Y_2||}[(Z_1, X_2) - (Z_1, (X_2, Z_1) Z_1)]$ $= \frac{1}{||Y_2||} [(Z_1, X_2) - \overline{(X_2, Z_1)}(Z_1, Z_1)]$ $ = \frac{1}{||Y_2||}(Z_1, X_2)(1- (Z_1, Z_1))$
and since $(Z_1, Z_1) = (\frac{X_1}{||X_1||}, \frac{X_1}{||X_1||}) = \frac{1}{||X_1||^2}(X_1, X_1) = 1$, so $(Z_1, Z_2) = \frac{1}{||Y_2||}(Z_1, X_2)(1- (Z_1, Z_1)) = 0$  

<u>IH</u>: Suppose the gram schmidt process works for $j-1 > 2$ . 
So $Z_1, … , Z_{j-1}$ are orthogonal to each other. We now show the gram schmidt process works for adding another linearly independent vector $X_j$. Define $Y_j = X_j - \sum_{k=1}^{j-1}(X_j, Z_k) Z_k$ and $Z_j = Y_j/||Y_j||$  

**Let** $1 \leq i < j$, we'll use the Indutive Hypothesis to show $(Z_i, Z_j) = 0$ $\implies$ $(Z_j, Z_i) = \overline{(Z_i, Z_j)} = 0$ and since each $Z_i = \frac{Y_i}{||Y_i||}$, $||Z_i|| = 1$ 
**We use the properties of inner products:** (as proven above using the integral definition) 

$(Z_i, Z_j)$ $ = \frac{1}{||Y_j||}(Z_i, X_j - \sum_{k = 1}^{j = 1} (Y_j, Z_k) Z_k ) =\frac{1}{||Y_j||}[ (Z_i, X_j) - (Z_i, \sum_{k=1}^{j-1}(X_j, Z_k)Z_k)]$ $= \frac{1}{||Y_j||}[(Z_i, X_j) - \sum_{k=1}^{j-1}(Z_i, (X_j, Z_k) Z_k)]$$ = \frac{1}{||Y_j||}[(Z_i, X_j) - \sum_{k=1}^{j-1} (Z_k, X_j)(Z_i, Z_k)]$  

since we have $k \leq j- 1$, we can use the Inductive Hypothesis: $(Z_i, Z_k) = 0$ for all $k \neq i$ 
$= \frac{1}{||Y_j||}[(Z_i, X_j) - (Z_i, X_j)(Z_i, Z_i)] = \frac{1}{||X_j||}  [(Z_i, X_j) - (Z_i, X_j)*1] =0$ , so the vectors $Z_1, …$ are orthogonal to each other 

**(b)** let $X_1 = \cos x + \cos 2x$, $X_2 = 3 \cos x - 4 \cos 2x$ 

$(X_1, X_1) = \int_0^\pi \cos^2 x + 2 \cos x \cos 2x + \cos^2 2x \; dx$ 
$\cos(a + b) = \cos a \cos b - \sin a \sin b$, $\cos(a - b) = \cos a \cos b + \sin a \sin b$ 
$\cos(3x) + \cos(x) = 2 \cos x \cos 2x$, $\frac{1}{2}[\cos(x + x) + \cos( x - x)] = \frac{1}{2}[\cos 2x + 1] = \cos^2 x$ , 
$\frac{1}{2}[\cos(4x) + 1] = \cos^2 2x$ 

$(X_1, X_1) = [\sin nx \text{ terms } + x]_0^\pi = \pi$ , where $n = 1, 2, 3, 4$ and so $Z_1 = X_1/ \sqrt{\pi}$  
$(X_2, Z_1) =  \frac{1}{\sqrt{\pi}}\int_0^\pi (3\cos x - 4 \cos 2x)(\cos x + \cos 2x)\; dx$ $ = \frac{1}{\sqrt \pi} \int_0^\pi 3 \cos^2 x - \cos x \cos 2x - 4 \cos ^2 2x \; dx =\frac{1}{\sqrt\pi} \int_0^\pi \frac{3}{2}[\cos(2x) + 1] - \frac{1}{2}\cos 3x + \frac{1}{2} \cos x - 2[\cos 4x + 1]\; dx$ since the cosine terms become sine terms of the form $\sin nx$, for $n = 1, 2, 3, 4$ and $\sin n \pi = 0$ we are left with: $\frac{1}{\sqrt\pi}(\frac{3}{2} - 2)\pi = -\frac{\sqrt\pi}{2}$  

$Y_2 = 3\cos x - 4\cos 2x - (X_2, Z_1)Z_1$ $ = 3\cos x - 4 \cos 2x +\frac{1}{2} (\cos x + \cos 2x )= \frac{7}{2} \cos x - \frac{7}{2} \cos 2x$  

$(Y_2, Y_2) = \frac{49}{4} \int_0^\pi \cos^2 x - 2 \cos x \cos 2x + \cos^2 2x \; dx = \frac{49\pi}{4}$ so $||Y_2|| = {7\sqrt\pi/2}$ 
$Z_2 = \frac{\cos x - \cos 2x}{\sqrt{\pi}}$ 
so since $(cZ_1, dZ_2) = cd \int_0^\pi Z_1 Z_2 \; dx = cd(0) = 0$, so we can multiply $Z_1, Z_2$ by $\sqrt{\pi}$ (or just keep $Y_1, Y_2$ as is)

 so $\cos x + \cos 2x$ and $\cos x - \cos 2x$ are orthogonal:
$\int_0^\pi \cos^2x - \cos^2 2x \; dx = \int_0^\pi \frac{1}{2} \cos 2x + \frac{1}{2} \cos 4x \; dx = 0$ 

#### Section 5.4: Exercises 2, 3, 4, 8, 11, 18

##### Excercise 2

Let $\{f_n\}$ be a set of functions on an interval $[a, b]$. Suppose $\sum_{n=1}^\infin f_n$ converges uniformly to a function $f$: $\max_{x \in [a,b]}|f - \sum_{n=1}^N f_n| \rightarrow 0$ as $N \rightarrow \infin $ 
WTS: It converges in the $L^2$ sense and in the pointwise sense

Using the Squeeze Theorem:

**Pointwise sense**: we have that for any $x \in (a,b)$, 
$|f - \sum_{n=1}^N f_n| \leq \max_{x \in [a,b]}|f - \sum_{n=1}^N f_n| \rightarrow 0$ as $N \rightarrow \infin$ 
and since $|f- \sum_{n=1}^N f_n| \geq 0$ for each $N$ , we must have
$0 \leq \lim_{N \rightarrow \infin}|f - \sum_{n=1}^N f_n| \leq \lim_{N \rightarrow \infin} (\max_{x \in [a,b]} |f- \sum_{n=1}^N f_n|) = 0$ $\implies$ $\lim_{N\rightarrow \infin}|f - \sum_{n=1}^N f_n| = 0$ , so we have pointwise convergence. 

**$L^2$ Sense**: 
$0 \leq \int_a^b| f(x) - \sum_{n=1}^N f_n(x)|^2 \; dx$ $\leq \int_a^b (\max_{x \in [a,b]} |f(x) - \sum_{n=1}^N f_n(x)|)^2\; dx$

and again, taking the limit as $N \rightarrow \infin$, we have $\int_a^b (\max_{x \in [a,b]} |f(x) - \sum_{n=1}^N f_n(x)|)^2 \; dx \rightarrow 0$ $\implies$ $\int_a^b |f(x) - \sum_{n=1}^N f_n(x)|^2 \; dx \rightarrow 0$ 

##### Exercise 3

Each $\gamma_n​$ is a constant, and $\gamma_n \rightarrow \infin​$ as $n \rightarrow \infin​$. And $f_n(x) = \begin{cases} 0 & x = \frac{1}{2} \\ \gamma_n & x \in [\frac{1}{2}- \frac{1}{n}, \frac{1}{2}) \\ -\gamma_n & x \in (\frac{1}{2}, \frac{1}{2} + \frac{1}{n}] \\ 0 & x \notin [\frac{1}{2} - \frac{1}{n},\frac{1}{2}) \cup(\frac{1}{2},  \frac{1}{2} + \frac{1}{n}]\end{cases}​$ 

**(a)** $f_n(x) \rightarrow 0$ pointwise: we know $f_n(x) = 0$ for $x\notin [\frac{1}{2} - \frac{1}{n},\frac{1}{2}) \cup(\frac{1}{2},  \frac{1}{2} + \frac{1}{n}]$  for all $n \in \N$. 
Because $\frac{1}{n} \rightarrow 0$ as $n \rightarrow \infin$, we have that for any $\epsilon > 0$ $\exists N \in \N$ s.t. for all $n \geq N$, $\frac{1}{n} < \epsilon$. 
Therefore, for **any** $x_0 < \frac{1}{2}$, $\frac{1}{2} - x_0 > 0$, so there exists an $N \in \N$ s.t. for all $n \geq N$,  $\frac{1}{n} < \frac{1}{2} - x_0$ $\implies$ $x_0 < \frac{1}{2} - \frac{1}{n}$, and for all $n \geq N$, $x_0 \notin [\frac{1}{2} - \frac{1}{n},\frac{1}{2}) \cup(\frac{1}{2},  \frac{1}{2} + \frac{1}{n}]$ , so for all $n \geq N$,   $f_n(x_0) = 0$ so for any $\epsilon > 0$, $|f_n(x)| < \epsilon$$\implies$ for all $x < \frac{1}{2}$, as $n \rightarrow \infin$, $f_n(x) \rightarrow 0$ 
and similarly for **any** $x_0 > \frac{1}{2}$, $x_0 - \frac{1}{2} > 0$ $\implies$ there exists an $N \in \N$ s.t. for all $n \geq N$, $x_0 > \frac{1}{2} + \frac{1}{n}$, and for all $n \geq N$, $x_0 \notin [\frac{1}{2} - \frac{1}{n},\frac{1}{2}) \cup(\frac{1}{2},  \frac{1}{2} + \frac{1}{n}]$ , so for all $n \geq N$, $f_n(x_0) = 0$, so for any $\epsilon > 0$, $|f_n(x)| < \epsilon$ $\implies$ for all $x > \frac{1}{2}$, as $n \rightarrow \infin$ $f_n(x) \rightarrow 0$ 

and since we have $f_n(1/2) = 0$, it is the case that for all $x$, $f_n(x) \rightarrow 0$ as $n \rightarrow \infin$ 

**(b)** The convergence is not uniform
$\max_{x} |f_n(x)|$ since $\frac{1}{n} \neq 0$ for any $n \in \N$, there is always an $x \in  [\frac{1}{2} - \frac{1}{n}, \frac{1}{2})\cup (\frac{1}{2}, \frac{1}{2} + \frac{1}{n}]$ 
so $f_n(x) = \pm |\gamma_n|$ so $\max_{x} |f_n(x)| = |\gamma_n|$, which we know tends towards $\infin$ as $n \rightarrow \infin$, not towards 0. So this doesn't converge uniformly.

**(c)** $f_n(x) \rightarrow 0$ in the $L^2$ sense if $\gamma_n = n^{1/3}$ 
$\int_{-\infin}^\infin |f_n(x)|^2 \; dx$ $ = \int_{\frac{1}{2} - \frac{1}{n}}^\frac{1}{2} |n^{1/3}|^2\; dx + \int_{\frac{1}{2}}^{\frac{1}{2} + \frac{1}{n}}|- n^{1/3}|^2 \; dx$ $ = n^{2/3}(\frac{1}{2} - \frac{1}{2} + \frac{1}{n}) + n^{2/3}(\frac{1}{2} + \frac{1}{n} - \frac{1}{2})$ $ = n^{2/3}(\frac{2}{n})$ $= 2(\frac{1}{n^{1/3}})$ 
and taking the limit: $\lim_{n\rightarrow \infin} 2 \frac{1}{n^{1/3}} = 2 \lim_{n\rightarrow \infin} \frac{1}{n^{1/3}} = 2\lim_{n\rightarrow \infin} \sqrt[3]{\frac{1}{n}}$ 
(from Real Analysis, we have that if $(a_n)_{n \in \N}$ is a sequence of nonnegative terms and $a_n \rightarrow a$ , then  for $k \in \N$,  $\sqrt[k]{a_n} \rightarrow \sqrt[k]{a}$ )

so since $(\frac{1}{n})_{n \in \N}$ is a sequence of nonnegative terms and $\frac{1}{n} \rightarrow 0$, then $\sqrt[3]{\frac{1}{n}} \rightarrow 0$ and since $2$ is bounded, $2*\lim_{n\rightarrow \infin} \frac{1}{n^{1/3}} = 0$. Which means $\int_{-\infin}^\infin |f_n(x)|^2 \; dx \rightarrow 0$ as $n \rightarrow \infin$ 

**(d)** $f_n(x)$ doesn't converge in the $L^2$ sense if $\gamma_n = n$ 
$\int_{-\infin}^\infin |f_n(x)|^2 \; dx = \int_{\frac{1}{2} - \frac{1}{n}} ^{\frac{1}{2}} |n|^2\; dx + \int_{\frac{1}{2}}^{\frac{1}{2} + \frac{1}{n}} |-n|^2 \; dx $ $ = n^2(\frac{2}{n})$ $ = 2n$ $\rightarrow \infin$, and $\not\rightarrow 0$, so $f_n(x)$ doesn't converge in the $L^2$ sense if $\gamma_n= n$ 

##### Exercise 4

$g_n(x) = \begin{cases} 1 & x \in [\frac{1}{4} - \frac{1}{n^2}, \frac{1}{4} + \frac{1}{n^2}) & n \text{ is odd} \\ 1 & x \in [\frac{3}{4} - \frac{1}{n^2}, \frac{3}{4} +\frac{1}{n^2})& n \text{ is even} \\ 0 & & \text{for all other } x \end{cases}$

**$L^2$**: $\int_{-\infin}^\infin |g_n(x)|^2 \; dx$. Since we end up with $\int_{a}^b 1^2\; dx = b-a$, where $a$ and $b$ depend on $n$, notice that $(\frac{1}{4} +\frac{1}{n^2}) - (\frac{1}{4} - \frac{1}{n^2}) = (\frac{3}{4} + \frac{1}{n^2} )- (\frac{3}{4} - \frac{1}{n^2}) = \frac{2}{n^2}$ 

so we have $\int_{-\infin}^{\infin}|g_n(x)|^2 \; dx = \frac{2}{n^2} \rightarrow 0, n \rightarrow \infin$, so we have convergence in $L^2$ 

**Pointwise**:Consider $|g_n(x)|$ for all $x$. 
If $n$ is odd: at $x = \frac{1}{4}$, $g_n(x) = 1$ and if $n$ is even: at $x = \frac{3}{4}$, $g_n(x) = 1$ 

We must have that if $a_n \rightarrow a$, then all subsequences $a_{k_n}$ where $k_n \in \N$, and  $1 \leq k_1 < k_2 < …$ must converge to $a$. 
*Proof*: Suppose $a_n \rightarrow a$ $\implies$ for all $\epsilon > 0$, there exists $N$ s.t. for all $n \geq N$, $|a_n - a| < \epsilon$ 
In a subsequence $k_n \geq n$ for all $n \in \N$. Base case: $k_1 \geq 1$, since $k_1 \in \N$, and suppose $k_m \geq m$ for some $m \in \N$, then $k_{m+1} > k_m +1 \geq m+1$. (since $k_{m+1}$ and $k_m \in \N$, we must have $k_{m+1} > k_m + 1$) 
so since there exists an $N$ s.t. for all $n \geq N$, $|a_n - a| < \epsilon$, we have that for all $k_n \geq n \geq N$, $|a_{k_n} - a| < \epsilon$ so $a_{k_n} \rightarrow a$ $\square$ 

Taking the odd $n's$, we have a subsequence $g_{2k+1}(\frac{1}{4}) = 1$ for $k = 0, 1, 2, …$, which doesn't converge to $0$, so we have a contradiction since subsequences must have the same limit as the original sequence. So the sequence doesn't converge pointwise.

##### Exercise 8

**(a)**: $f(x)$ $= x^3$, and this is a polynomial, so $f$ is $C^\infin$, so by **Theorem 2**, this converges uniformly, and by excercise 5.4.2, it converges in the $L^2$ sense and pointwise sense (**provided** it satisfies boundary conditions.) By **Theorem 4**, it converges in the pointwise sense. Since $\int_0^l| x^3|^2 \; dx$ $ = \frac{l^7}{7}$ $< \infin$, by **Theorem 3**, it converges in the $L^2$ sense. 
**(b)**: Another polynomial: $lx - x^2$ on $(0,l)$. We have the same discussion as in **(a)** in terms of **Theorem 2** and **Theorem 4**: converges pointwise, and if it satisfies boundary conditions, converges uniformly. $\int_{0}^l |lx-x^2|^2 \; dx \leq \int_0^l l^2x^2 + 2lx^3 + x^4 \; dx = \frac{l^5}{3} + \frac{l^5}{2} + \frac{l^5}{5} < \infin$. So by **Theorem 3**, it converges in the $L^2$ sense
**(c)**: $f(x) = \frac{1}{x^2}$ on $(0,l)$. We can't say it converges uniformly on $[0, l]$, because $f$ is not continuous at $x = 0$. The rightside limit of $\frac{1}{x^2}$ is infinity, so in terms of $\R$, the limit $f(0+)$ doesn't exist, and so it isn't piecewise continuous. We cannot then use **Theorem 4** for pointwise continuity, unless tending towards infinity counts as a limit existing, then both $f(x)$ and $f'(x)$ would be piecewise continuous and we can use **Theorem 4**. $\int_0^l |\frac{1}{x^2}|^4 \; dx = \int_0^l \frac{1}{x^4} \; dx = -\frac{1}{3x^3}|_0^l = -\frac{1}{3l^3} + \infin$, so we may not use **Theorem 3**, since $\int_a^b |f(x)|^2\; dx$ is infinite in this case

##### Exercise 11. (Term by term integration)

**(a)** $f(x)$ is piecewise continuous in $[-l, l]$. Show that $F(x) = \int_{-l}^x f(s) \; ds$ has a full Fourier series that converges pointwise. 

Let $\{x_1, …, x_n\}​$ be the finite set of points where $f(x)​$ has jump discontinuities. 

and $f(x)$ is continuous in $(x_{i-1}, x_{i})$, for $i = 2, …, n$, so we know the definite integral $\int_{x_{i-1}}^{x_{i}} f(s)\; ds$ exists and is finite since $f(x_i +)$ and $f(x_i -)$ exist and are finite, so there exists a maximum $M$ of $f(x)$ in $[x_{i-1}, x_i]$. (The value of $f$ at the jumps is the difference $f(x_i+) - f(x_i-)$). So taking $\int_{-l}^{x_i}f(s) \; ds = \int_{-l}^{x_1} f(s) \; ds + … + \int_{x_{i-1}}^{x_i} f(s) \; ds$, where each integral is known and finite, and there are finitely many integrals, so $\int_{-l}^{x_i} f(s)\; ds​$ exists and is finite. 

And, in the points in $[-l, l]$ where $f(x)$ is continuous, we have that $F(x)$ is differentiable (due to the Fundemental theorem of calculus: i.e., if $f(x)$ is continuous in $(a,b) = (x_{i-1},x_{i})$, then $F(x) = \underbrace{\int_{-l}^a f(s)\; ds}_{\text{constant}} + \int_a^x f(s)\; ds = C + \int_a^x f(s) \; ds$, and since $g(x) = \int_a^x f(s) \; ds$ is continuous (and differentiable) in $(a,b)$ and constants are differentiable, we have that $F(x)$, as the sum of two differentiable functions, is differentiable in $(a,b)$)  
At the points where $f(x)$ is discontinuous, we have that $F(x)$ is continuous and differentiable:
Let $x_k \in \{x_1, …, x_n\}$, and consider $F(x_k -) = \lim_{x \uparrow x_k}\int_{-l}^{x} f(s) \; ds$,  $ = \int_{-l}^{x_k} f(s) \; ds$ and $F(x_k+) = \lim_{x \downarrow x_k} \int_{-l}^x f(s) \; ds = \lim_{x\downarrow x_k} \int_{-l}^{x_k} f(s)\; ds + \int_{x_k}^x f(s) \; ds$ $ = \int_{-l}^{x_k} f(s) \; ds + \int_{x_k}^{x_k} f(s)\; ds = \int_{-l}^{x_k} f(s) \; ds$ 

$F'(x_k) = f(x_k+) - f(x_k-)$ 

So since $F$ is continuous and $F' = f$ is piecewise continuous, by **Theorem 4** the classical fourier series (full or sine or cosine) of $F$ converges pointwise on $[-l, l]$. 

**(b)** Write this convergent series for $f(x)$ explicitly in terms of the Fourier coefficients $a_0, a_n, b_n$ of $f(x)$ 

The Full Fourier Series : $F(x) = \int_{-l}^x f(x) \; ds = \int_{-l}^x \frac{a_0}{2} +\sum_{n=1}^\infin a_n \cos (n\pi s/l) + b_n \sin (n\pi s/l) \; ds​$ $= \int_{-l}^x \frac{a_0}{2}\; ds + \sum_{n=1}^\infin \int_{-l}^x a_n \cos(n\pi s/l) + b_n \sin (n\pi s/l) \; ds​$ $ = a_0\frac{x+l}{2} +  \sum_{n=1}^\infin \frac{l}{n\pi}a_n \sin (n\pi x/l) - \frac{l}{n\pi} b_n \cos(n\pi x/l) + \frac{l}{n\pi} b_n (-1)^n​$ 

**$f(x)$**: 

$f(x) = \frac{1}{2} a_0 + \sum_{n=1}^\infin (a_n \cos \frac{n \pi x}{l} + b_n \sin \frac{n \pi x}{l})$ 

$a_0 = \frac{1}{l} \int_{-l}^l f(x) \; dx = \frac{1}{l} F(l)$ $\implies$ $F(l) = a_0 l$ , which matches with the above: $F(l) = a_0 l $ 

$a_n = \frac{(f, \cos (n\pi x/l)}{l} = $  $\frac{1}{l} \int_{-l}^l f(x) \cos \frac{n \pi x}{l} \; dx$ 

$b_n = \frac{(f, \sin (n\pi x/l)}{l}$ $= \frac{1}{l}\int_{-l}^l f(x) \sin \frac{n \pi x}{l} \; dx$ 

---

$F(x) = \frac{1}{2} A_0 + \sum_{n=1}^\infin (A_n \cos \frac{n \pi x}{l} + B_n \sin \frac{n \pi x}{l})$ 

$A_0 = \frac{1}{l} \int_{-l}^l F(x) \; dx​$ $A_0 = \frac{1}{l} \int_{-l}^l (\int_{-l}^x f(s)\; ds)dx​$ 

$A_n = \frac{1}{l} \int_{-l}^l F(x) \cos \frac{n \pi x}{l} \; dx​$ , now using IBP $ = \frac{1}{l} [ \frac{l} {n\pi}F(x) \sin \frac{n \pi x}{l}|_{-l}^l - \frac{l} {n \pi }\int_{-l}^l f(x) \sin \frac{n \pi x}{l} \; dx]​$

and $\sin (\pm n \pi ) = 0​$ so: $ = \frac{1}{l}\frac{l} {n \pi }[0 - \int_{-l}^l f(x) \sin \frac {n \pi x}{l}  \; dx]​$ $= -\frac{l} {n\pi} \frac{1}{l}\int_{-l}^l f(x) \sin \frac{ n\pi x}{l}\; dx​$ $= -\frac{l} {n \pi } b_n​$ for $n \neq 0​$, 

$B_n = \frac{1}{l} \int_{-l}^l F(x) \sin \frac{n \pi x}{l} \; dx​$ $= \frac{1}{l}[ -\frac{l} {n \pi} F(x)\cos \frac{n \pi x}{l}|_{-l}^l + \frac{l} {n \pi}\int_{-l}^l f(x) \cos \frac{n \pi x}{l} \; dx]​$ 

$ = \frac{1}{l}[-\frac{l}{n\pi} \int_{-l}^lf(s)\; ds (-1)^n + \frac{l}{n\pi} \int_{-l}^{-l} f(s)\; ds(-1)^n  + \frac{l}{n\pi}\int_{-l}^l f(x) \cos \frac{n \pi x}{l} \; dx]$ 
 $ =(-1)^{n+1} \frac{l} {n \pi} \frac{1}{l}F(l) + \frac{l} {n \pi }a_n$ $ = (-1)^{n+1} \frac{l} {n \pi } a_0 + \frac{l} {n \pi} a_n$, for $n = 1, 2, 3, …$  

$F(x) = \frac{1}{2}A_0 + \sum_{n=1}^\infin \frac{l} {n \pi}[-b_n \cos \frac{n \pi x}{l} + ((-1)^{n+1}a_0 + a_n )\sin \frac{n \pi x}{l}]​$ 

$F(l) = \frac{1}{2} A_0 + \sum_{n=1}^\infin \frac{l}{n\pi} b_n(-1)^{n+1}​$ $ = l a_0​$ 

$\implies$ $\frac{1}{2}A_0 = la_0 + \sum_{n=1}^\infin \frac{l}{n\pi} b_n(-1)^{n}$ 

$F(x) = la_0 + \sum_{n=1}^\infin \frac{l}{n\pi} b_n(-1)^n +  \sum_{n=1}^\infin \frac{l} {n \pi}[-b_n \cos \frac{n \pi x}{l} + ((-1)^{n+1}a_0 + a_n )\sin \frac{n \pi x}{l}]$ 

##### Exercise 18

$$
u_{tt} = u_{xx} \qquad 0\leq x \leq l
\\ u(0, t) = u(l, t) = 0\text{ or } u_x(0, t) = u_x(l, t) = 0
$$

**(a)** $E = \frac{1}{2} \int_0^l (u_t^2 + u_x^2)\; dx​$ is a constant:
$\frac{\partial E}{\partial t} = \frac{1}{2}\frac{\partial}{\partial t}\int_0^l (u_t^2 + u_x^2) \; dx​$ $ = \frac{1}{2}[\int_0^l \frac{\partial}{\partial t}(u_t^2 + u_x^2) \; dx ]​$ 
$\frac{1}{2} \int_0^l u_{tt}u_t + u_{t}u_{tt} + u_{xt}u_x + u_xu_{xt}\; dx ​$  $= \frac{1}{2}\int_0^l 2u_{t}u_{tt} + 2u_{x}u_{xt} \; dx​$ $ = \int_0^l u_tu_{tt} + u_x u_{xt} \; dx​$ 

$ = \int_0^l u_tu_{xx} + u_x u_{xt} \; dx$ $ = \int_0^l (u_xu_t)_x \; dx$ because: $(u_xu_t)_x = u_{xx}u_t + u_xu_{xt}$ 
$ = u_x(l, t)u_t(l, t) - u_x(0, t)u_t(0,t)$, Whether Dirichlet or Neumann, we have $u_x(l, t) = u_x(0, t) = 0$ so $\frac{\partial E}{\partial t} = 0$ $\implies$ Energy is constant over time. 

**(b)** Let $E_n(t)​$ be the energy of its nth harmonic (nth term in the expansion). Show $E = \sum E_n​$. 
For Dirichlet BCs: $u(x,t) = \sum_{n = 1}^\infin (A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l} ) \sin \frac{ n \pi x }{l}​$ 

$u_t(x,t) = ​$ $\frac{\pi c}{l} \sum_{n=1}^\infin n(B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l}) \sin \frac{n \pi x}{l}​$ 
$u_x(x,t) = \frac{\pi}{l} \sum_{n=1}^\infin n(A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l}) \cos \frac{n \pi x}{l}​$ 

$E =\frac{1}{2}\int_0^l (\frac{\pi c}{l} \sum_{n=1}^\infin n (B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l}) \sin \frac{n \pi x}{l})^2 + ( \frac{\pi}{l} \sum_{n=1}^\infin n(A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l}) \cos \frac{n \pi x}{l})^2 \; dx$

Note, $(\sum_{n=1}^\infin x_n)^2 = x_1x_1 + x_1x_2 + … + x_2x_1 + x_2x_2 + … +..$ and because we can integrate term by term, we'll have integrals of the form :$\frac{nm \pi c}{l}(B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l})(B_m \cos \frac{n \pi c t}{l} - A_m \sin \frac{n \pi c t}{l}) \frac{1}{2}\int_0^l \sin \frac{n \pi x}{l} \sin \frac{m \pi x }{l} \; dx = 0$ and
$\frac{nm\pi}{l}(A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l})(A_m \cos \frac{m \pi c t}{l} + B_m \sin \frac{m \pi c t}{l}) \frac{1}{2}\int_0^l \cos \frac{n \pi x}{l} \cos \frac{m \pi x}{l} \; dx = 0$ 

for $m \neq n​$, due to Orthoganlity. So, we're left with:

$E = \sum_{n=1}^\infin \frac{1}{2} \int_0^l [\frac{n^2 \pi^2 c^2}{l^2}  (B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l})^2 \sin^2 \frac{n \pi x}{l} + \frac{n^2 \pi^2}{l^2} \; (A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t }{l} ) \cos^2 \frac{n \pi x}{l} ]dx ​$

and each term: $ (A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l} ) \sin \frac{ n \pi x }{l}​$ has $E_n = \frac{1}{2} \int_0^l \frac{n^2 \pi^2 c^2}{l^2}(B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l})^2\sin^2 \frac{n \pi x}{l} + \frac{n^2 \pi^2}{l^2} (A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l})^2 \cos^2 \frac{n \pi x}{l} \; dx​$ 

so we indeed have $E = \sum_n E_n​$ 

For Neumann BCs: $u(x,t) = \frac{1}{2} A_0 + \frac{1}{2} B_0 t + \sum_{n=1}^\infin (A_n \cos \frac{ n\pi c t}{l} + B_n \sin \frac{n \pi c t}{l} ) \cos \frac{ n \pi x }{l}​$ 

$u_t(x,t) = \frac{1}{2} B_0 + \sum_{n=1}^\infin \frac{n \pi c}{l}(B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l})\cos \frac{n \pi x }{l}​$ 
$u_x(x,t) = \sum_{n=1}^\infin -\frac{n \pi }{l}(A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l}) \sin \frac{n \pi x}{l}​$ 

$E = \frac{1}{2} \int_0^l (\frac{1}{2} B_0 + \sum_{n=1}^\infin \frac{n \pi c}{l}(B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l})\cos \frac{n \pi x }{l})^2 + (\sum_{n=1}^\infin -\frac{n \pi }{l}(A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l}) \sin \frac{n \pi x}{l})^2 \; dx$ Because we can integrate term by term, and using orthogonality like above:
$E =\frac{1}{2}\int_0^l \frac{1}{4}B_0^2 \; dx +  \sum_{n=1}^\infin \frac{1}{2}\int_0^l[\frac{n^2 \pi^2 c^2}{l^2}(B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l})^2 \cos^2 \frac{n \pi x }{l} + \frac{n^2 \pi^2}{l^2}(A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l})^2 \sin^2 \frac{n \pi x}{l} ]\; dx$

and $E_0 = \frac{1}{2} \int_0^l (\frac{1}{2}B_0)^2 + 0^2 \; dx = \frac{1}{2}\int_0^l \frac{1}{4} B_0^2 \; dx$  and $E_n = \frac{1}{2} \int_0^l \frac{n^2\pi^2c^2}{l^2}(B_n \cos \frac{n \pi c t}{l} - A_n \sin \frac{n \pi c t}{l})^2 \cos^2 \frac{n \pi x}{l} + \frac{n^2 \pi^2 }{l^2} (A_n \cos \frac{n \pi c t}{l} + B_n \sin \frac{n \pi c t}{l})^2 \sin^2 \frac{n \pi x}{l} \; dx$ for $n = 1, 2, 3, ..$ 
so we have $E = \sum_n E_n$ 

#### Section 5.5: Exercises 3, 5, 12

##### Exercise 3

**Schwarz Inequality**: $|(f,g)| \leq ||f|| \cdot ||g||$, 

so using $f'(x)$ and $1$: $\int_0^l f'(x) \; dx  = f(l) - f(0)\leq (\int_0^l (f'(x))^2 \; dx)^{1/2}(\underbrace{\int_0^l 1^2 \; dx}_{=l})^{1/2}$ and squaring both sides: $(f(l) - f(0))^2 \leq \int_0^l(f'(x))^2 \; dx \cdot l$ as desired.

##### Exercise 5

Prove Schwarz inequality for infinite series: $\sum a_nb_n \leq (\sum a_n^2)^{1/2}(\sum b_n^2)^{1/2}$ 

Hint from Exercise 2: Consider $|| f + tg||^2​$ where $t​$ is a scalar. This expression is a quadratic polynomial of $t​$. Find the value of $t​$ where it is a minimum. 

In this case: $([a_1, …, a_N],[b_1, …,  b_N]) = \sum a_n b_n$. so $(\bold a + t\bold b) = (a_1 + tb_1, a_2 + tb_2, …, a_N + tb_N)$ so $(\bold a + t\bold b, \bold a + t \bold b) = \sum( a_n + tb_n)^2$ 
so let $p(t) = \sum_{n=1}^N(a_n + tb_n)^2$ = $(b_1^2 + b_2^2 + … b_N^2)t^2 + 2(a_1b_1 + … +a_Nb_N)t + (a_1^2 + … + a_n^2)$ since it is the sum of nonnegative terms (due to the terms being squared), $p(t) \geq 0$ for $t \in \R$. This also means it is possible that there are no real roots for $p(t)$ unless it occurs at exactly one point $t$< $p(t) = 0$ for a double root. Explanation: $at^2 + bt + c \geq 0$, with $a, b, c> 0$, so $p'(t) = 2at + b$. We only have one critical point: $t = -b/2a$. When $t <-b/2a$, $p(t)$ is decreasing, and when $t > -b/2a$, $p(t)$ is increasing. So we have exactly one minimum at $-b/2a$. Therefore, if we have two real roots, then the minimum will be negative, a contradiction of $p(t) \geq 0$, therefore the discriminant must be at most 0: (positive means we have two real roots)

Discriminant: $(2\sum a_n b_n)^2 - 4\sum a_n^2\sum b_n^2 \leq 0$ $\implies$ $(\sum a_n b_n)^2 \leq \sum a_n^2 \sum b_n^2$ $\implies$ $\sum_{n \leq N} a_n b_n \leq (\sum_{n \leq N} a_n^2)^{1/2}(\sum_{n \leq N}b_n^2)^{1/2}$ and this remains true as $N \rightarrow \infin$ 

##### Exercise 12

$f(x)$ is $C^1$ in $[-\pi, \pi]$ and satisfies the periodic BC, and if $\int_{-\pi}^\pi f(x) \; dx = 0$, then $\int_{-\pi}^\pi |f|^2 \; dx \leq \int_{-\pi}^\pi |f'|^2\; dx$ 

Periodic Boundary Conditions: $f(-\pi) = f(\pi), f'(-\pi) = f'(\pi)$ 

If we're using the classical fourier series, we don't need $f''(x)$ to exist in order to use **Theorem 5.4.2**, so we have that the Fourier series $\sum A_n X_n$ of $f(x)$ converges uniformly on $[-\pi, \pi]$. 

so using the Full Fourier series: $\phi(x) = \frac{1}{2}A_0 + \sum_{n=1}^\infin (A_n \cos \frac{n \pi x}{l} + B_n \sin \frac{ n \pi x}{l})$ 

$A_0 = \int_{-\pi}^\pi f(x) \; dx = 0$, where $A_0$ is a Fourier Coefficient of $f$ and similarly for $A_0' = 0$ because of the Peroidic BC 

and we know from earlier that $A_n = - \frac{1}{n}B_n'$ and $B_n = \frac{1}{n} A_n'$ $\implies$ $|A_n| \leq |B_n'|$ and $|B_n| \leq |A_n'|$, where $B_n', A_n'$ are the Fourier Coefficients of $f'​$ 

and for $\int_{-\pi}^\pi \cos^2 \frac{n \pi x}{l} = \pi = \int_{-\pi}^\pi \sin^2 \frac{n \pi x}{l}$

and since uniform convergence $\implies$ $L^2$ convergence (Earlier exercise showed this), and $L^2$ convergence implies parseval's equality:

$\sum_{n=1}^\infin |A_n|^2\int_{-\pi}^\pi \cos^2(nx)\; dx + |B_n|^2 \int_{-\pi}^\pi \sin^2 (nx) = \sum_{n=1}^\infin\pi(|A_n|^2+|B_n|^2) = \int_{-\pi}^{\pi}|f|^2\; dx​$

$\sum_{n=1}^\infin |A_n'|^2\int_{-\pi}^\pi \cos^2(nx)\; dx + |B_n'|^2\int_{-\pi}^\pi \sin^2 (nx)\; dx = \sum_{n=1}^\infin \pi(|A_n'|^2 + |B_n'|^2) = \int_{-\pi}^\pi |f'|^2\; dx​$ and recalling that $|A_n| <|B_n'|​$ and $|B_n| < |A_n'|​$:

$\int_{-\pi}^{\pi}|f|^2\; dx = \sum_{n=1}^\infin\pi(|A_n|^2+|B_n|^2)   \leq \sum_{n=1}^\infin \pi(|A_n'|^2 + |B_n'|^2) = \int_{-\pi}^\pi |f'|^2\; dx$ 
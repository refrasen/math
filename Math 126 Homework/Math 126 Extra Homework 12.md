and we want to minimize the lengths of these vectors: 

$f(x, y, z) = \sqrt{(x-x_p)^2 + (y-y_p)^2 + (z-z_p)^2} + \sqrt{(x_q - x)^2 +(y_q-y)^2 + (z_q - z)^2}$

subject to the constraint: $g(x, y, z) = ax + by + cz = 0$ 

Find all values of $x, y, z$, $\lambda$ 

$\nabla f = \lambda \nabla g$ for some 

and $g(x, y, z) = 0$ 

$D_1 = \sqrt{(x-x_p)^2 + (y-y_p)^2 + (z-z_p)^2}$

$D_2 = \sqrt{(x_q-x)^2 + (y_q - y)^2 + (z_q - z)^2}$

$f_x = \frac{x-x_p}{\sqrt{(x-x_p)^2 + (y-y_p)^2 + (z-z_p)^2}} + \frac{x_q - x}{\sqrt{(x_q-x)^2 + (y_q - y)^2 + (z_q - z)^2}}$ $ = \lambda a$ 
$f_y = \frac{y-y_p}{\sqrt{(x-x_p)^2 + (y-y_p)^2 + (z-z_p)^2}} + \frac{y_q - y}{\sqrt{(x_q - x)^2 + (y_q - y)^2 + (z_q - z)^2}}$ $= \lambda b$ 
$f_z = \frac{z-z_p}{\sqrt{(x-x_p)^2 + (y-y_p)^2 + (z-z_p)^2}} + \frac{z_q - z}{\sqrt{(x_q - x)^2 + (y_q - y)^2 + (z_q - z)^2}}$ $ = \lambda c$ 

$(x-x_p)D_2 + (x_q - x)D_1 = \lambda a D_1 D_2$ 
$(y-y_p)D_2 + (y_q - y)D_1 = \lambda b D_1 D_2$ 
$(z-z_p)D_2 + (z_q - z)D_1 = \lambda c D_1 D_2$ 

$(D_2 - D_1)x = \lambda aD_1 D_2 + D_2 x_p - D_1 x_q$ 
$(D_2 - D_1)y = \lambda b D_1 D_2 + D_2 y_p - D_1 y_q$ 
$(D_2 - D_1)x = \lambda cD_1 D_2 + D_2 z_p - D_1 z_q$ 
multiplying so that we have:
$\begin{align} (D_2 - D_1)xyz &= yz(\lambda aD_1 D_2 + D_2 x_p - D_1 x_q) \\ &= xz(\lambda b D_1 D_2 + D_2 y_p - D_1 y_q) \\ &= xy( \lambda cD_1 D_2 + D_2 z_p - D_1 z_q)  \end{align}$

if $\lambda = 0$ $\implies$ 
$(D_2 - D_1)x = D_2x_p  - D_1 x_q $ 
$\implies$ $D_2(x - x_p) = D_1(x  - x_q)$
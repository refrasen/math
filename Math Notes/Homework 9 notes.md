Homework 9 notes

$S_{tx} = -2ctx \delta'(c^2t^2 - x^2)\text{sgn}(t) - \frac{x}{c}\delta(c^2t^2 - x^2)\frac{d}{dt}(\text{sgn}(t))$ $= S_{xt}$ 

$S_{txx}(x, t) = -2ct \delta'(c^2t^2 - x^2)\text{sgn}(t) -4ctx^2 \delta''(c^2t^2 -x^2)\text{sgn}(t) - \frac{1}{c}\delta(c^2t^2 - x^2)\frac{d}{dt}(\text{sgn}(t)) + \frac{2x^2}{c}\delta'(c^2t^2 - x^2)\frac{d}{dt}(\text{sgn}(t))$ 

$S_{tt} = c^2 S_{xx}$ (from (7))

$S_x(x, t) = -\frac{x}{c}\delta(c^2t^2 - x^2)\text{sgn}(t)$ 



$S_{xx}(x, t) = -\frac{1}{c}\delta(c^2t^2 - x^2)\text{sgn}(t) + \frac{2x^2}{c}\delta'(c^2t^2 - x^2)\text{sgn(t)}$







$\frac{\partial R}{\partial t}(x, t) = S_t(x-x_0, t-t_0) = k \Delta S(x-x_0, t-t_0)$

$\frac{\partial R}{\partial x}(x, t) = S_x(x-x_0, t-t_0)$ $\frac{\partial^2 R}{\partial x^2}(x, t) = S_{xx}(x-x_0, t-t_0)$ 

$\Delta R(x, t) = \Delta S(x-x_0, t-t_0)$ 

Should technically have: $R_t - k \Delta R = S_t(x-x_0, t-t_0) - k\Delta S(x-x_0, t-t_0)$ 

$\delta(x-x_0) = 0$ when $x \neq x_0$ 

$\delta(t-t_0) = 0$ when $t \neq t_0$ 

so $\delta(x-x_0)\delta(t-t_0) = 0$ whenever $(x, t) \neq (x_0, t_0)$ 

$R = \frac{1}{\sqrt{4 \pi k (t-t_0)}}e^{-(x-x_0)^2/4k(t-t_0)}$  
$R_t =- \frac{2 \pi k}{(4 \pi k(t-t_0))^{3/2}}e^{-(x-x_0)^2/4k(t-t_0)} + \frac{1}{\sqrt{4\pi k(t-t_0)}}\cdot \frac{(x-x_0)^2}{4k(t-t_0)^2}e^{-(x-x_0)^2/4k(t-t_0)}$ 

$R_{x} = \frac{1}{\sqrt{4 \pi k(t-t_0)}}(-\frac{2(x-x_0)}{4k(t-t_0)})e^{-(x-x_0)^2/4k(t-t_0)}$ $ = -\frac{2(x-x_0)}{\sqrt{4 \pi k(t-t_0)}4k(t-t_0)}e^{-(x-x_0)^2/4k(t-t_0)}$ 

$R_{xx} =- \frac{2}{\sqrt{4\pi k(t-t_0)}4k(t-t_0)}e^{-(x-x_0)^2/4k(t-t_0)} + \frac{4(x-x_0)^2}{\sqrt{4 \pi k(t-t_0)}(4k(t-t_0))^2}$ 

$kR_{xx} = - \frac{2}{\sqrt{4 \pi k(t-t_0)}4(t-t_0)}e^{-(x-x_0)^2/4k(t-t_0)} + \frac{4(x-x_0)^2}{\sqrt{4 \pi k(t-t_0)}16k(t-t_0)^2}$ 

$R_t - kR_{xx} = \frac{1}{\sqrt{4 \pi k(t-t_0)}}e^{-(x-x_0)^2/4k(t-t_0)}[-\frac{2\pi k}{4\pi k(t-t_0)} + \frac{(x-x_0)^2}{4k(t-t_0)^2}+ \frac{2}{4(t-t_0)} - \frac{4(x-x_0)^2}{16 k (t-t_0)^2}]$ 

The part in brackets is equal to $0$: 

$R_t - kR_{xx} = 0​$ 





$(R, \phi_{t} - c^2 \phi_{xx})$ 

Distributions satisfy: $(R, a\phi + b \psi) = a(R, \phi) + b(R, \psi)$ 

so $(R, \phi_t - k \phi_{xx}) = (R, \phi_t) - k(R, \phi_{xx}) $ 

$-(R_t, \phi) + k(R_{xx}, \phi) = -(R_t - k \Delta R, \phi)$ (since earlier we got that distributions are vector sapces)

$(R_t - k R_{xx}, \phi) = (R_t, \phi) - k(R_{xx}, \phi) = -(R, \phi_t) + k(R_{x}, \phi_x)$ $=-(R, \phi_t) - k(R, \phi_{xx})$ 

We have that $R_t - k \Delta R = 0$ away from $(x_0, t_0)$ 

but for $(x_0, t_0)$: $(R, \phi_t - k \phi_{xx})$ = $(R, \phi_t) - k(R, \phi_{xx})$ $ = \int_{-\infin}^\infin S(x-x_0, t-t_0)\phi_t(x, t)\, dt - k \int_{-\infin}^\infin S(x-x_0, t-t_0)\phi_{xx}\, dx$ 
$ = [S(x-x_0, t-t_0)\phi(x, t)]_{-\infin}^\infin  - \int_{-\infin}^\infin S_t(x-x_0, t-t_0)\phi(x, t)dt$ 
$ - k[S(x-x_0, t-t_0)]\phi_{x}]_{-\infin}^\infin + \int_{-\infin}^\infin S_{x}(x-x_0, t-t_0)\phi_{x}\, dx$ 
and since $\phi$ and its derivatives disappear at $\pm \infin$: 
$= -(S_t, \phi) + (S_x, \phi_x)$ $ = (S)


$ = - \int_{-\infin}^\infin S_t(x -x_0, t-t_0)\phi dt + k \int_{-\infin}^\infin S_{xx}(x-x_0, t-t_0)\phi dx$ 

$ -[S(x-x_0, t-t_0)\phi]_{-\infin}^\infin + \int_{-\infin}^\infin S(x-x_0, t-t_0) \phi_t \, dt$ 
$ + [k S_{x}(x-x_0, t-t_0)\phi ]_{-\infin}^\infin - k\int_{-\infin}^\infin S_{x}(x-x_0, t-t_0)\phi_x(x, t)\, dx $

Now, since $\phi$ dissapears at $\pm \infin$:  

We now have:

$-(R_t - k R_{xx}, \phi)$ = $-(R_t, \phi) + k(R_{xx}, \phi)$ 
$ = 0 + \int_{-\infin}^\infin S(x-x_0, t-t_0)\phi_t \, dt$ $- k \int_{-\infin}^\infin S_x(x-x_0, t-t_0)\phi_x(x, t)\, dx$ 
$ = (S, \phi_t) - k(S_x, \phi_x)$ = $(S, \ph_t) - k(S)

$(R_t -k R_{xx}, \phi) = (R_t, \phi) - k(R_{xx}, \phi)$ 
$= \int_{-\infin}^\infin S_t(x-x_0, t-t_0)\phi \, dt - k \int_{-\infin}^\infin S_{xx}(x-x_0, t-t_0)\phi\, dx$ 
$ = [S(x-x_0, t-t_0)\phi]_{t=-\infin}^\infin - \int_{-\infin}^\infin S(x-x_0, t-t_0)\phi_t \, dt$ 
$ - k[S_{x}(x-x_0, t-t_0)\phi] + k\int_{-\infin}^\infin S_x(x-x_0, t-t_0)\phi_x \, dx$ 
$\phi$ dissapears at $\pm \infin$ : 
$ = -(S(x-x_0, t-t_0), \phi_t(x, t)) +k(S_x(x-x_0, t-t_0), \phi_x(x, t))$ 
$= -(S(x-x_0, t-t_0), \phi_t(x, t)) - k(S(x-x_0, t-t_0), \phi_{xx}(x, t))​$ 
$ \rightarrow \phi_t()



$\int_{-\infin}^\infin S_x(x-)

Green's Formulas:

$(vu_x) _x = v_x u_x + vu_{xx}$ 

$[vu_{x}]_a^b $ $ = \int_a^b v_x u_x \,dx + \int_a^b vu_{xx}\, dx$ 

$\int_{\partial D} v \frac{\partial u}{\partial n}\, ds = \iint_D \nabla v \cdot \nabla u \, d x\, dt + \iint_D v \Delta u\, d  x\, d t$ 

$\iint_{D}(u \Delta v - v \Delta u)\, dx \, dt = \int_{\partial D}( u \frac{\partial v}{\partial n} - v \frac{\partial u}{\partial n})\, ds$ 



$D$ is the exterior of a circle of radius $S$ 

$R \phi_x$, $R \phi_{xx}$ 

$R_x \phi$, $R_{xx}\phi$ 
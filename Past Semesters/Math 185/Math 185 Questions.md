# Math 185 Questions

##### Difference between Closed and Exact?

For example, why can we do on page 225 (1.3) for a closed differential?

the whole $\int_{\gamma} d \arg(f(z)) = \arg f(\gamma(b)) - \arg f(\gamma(a))$ 

I thought we could only do this for exact ones?

Is this related to $\arg f(z)$  being single-valued determination?

##### Also, we need to review poles more, make sure we understand that more thoroughly

##### Residue Theorem, that was recent and difficult enough

##### When using the argument principle, when do we have to understand how the curve of $f(\gamma(t))$ works and when do we just use the terminal and initial points?

##### When do we know when a function has isolated zeros? I guess see the Uniqueness Principle in section 5? 

##### General note: Try to "memorize" how to evaluate certain integrals

* Residue Theorem
* Cauchy Integral

##### Types of Convergence?

##### Relationship between a meromorphic function being bounded at infinity and being meromorphic on the complex plane (see Chapter 8, sec. 4 exercise 6)

Huge weakness:

There were many theorems I just didn't remember from chapter 6 and 7 regarding

poles, meromorphism, and rationality

These are just general rules about how you know something is a pole and what not.
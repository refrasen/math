# One Argument for Math 185 problem 6 in chapter 3 section 2

We subdivide $t \in [0,1]$ in such a way that at each $t_j$, 

this denotes when the path changes direction

So for each subdivision:

$t_{j-1} \leq t \leq t_j$ for $j = 1, . . . , n$ 

$\theta_j(t)$ denotes $\arg \gamma(t)$ for this subdivision, and is monotonically decreasing (clockwise) or increasing (counter-clockwise)

So let us consider the total change of angle at each subdivision:

$\theta_j(t_j) - \theta_j(t_{j-1})$ 

if this is going counter-clockwise, we get a positive value

If this is going clockwise, we get a negative value

What happens if we were to consider the "total change" of $\theta$ over $t \in[0, 1]$ 

Since $\gamma(t)$ is closed, this should be equal to some $2\pi m$, where $m$ is an integer.

Obviously:

1. $\gamma$ is continuous, and we're keeping $r = |\gamma(t)|$ constant, so $(r\cos\theta_j(t_j), r\sin\theta_j(t_j)$ = $ (r\cos\theta_{j+1}(t_j), r\sin\theta_{j+1}(t_j)) $ so: $\theta_j(t_j) = \theta_{j+1}(t_j)$ 

1. $\theta_n(t_n) = \theta_n(1) = \theta(1)$ and $\theta_1(t_0) = \theta_1(0) = \theta(0)$ 
2. $\gamma$ is closed so $\theta(1) \equiv \theta(0) \mod 2\pi$ 

We have: $\theta_n(t_n) - \theta_n(t_{n-1}) + \theta_{n-1}(t_{n-1}) - \theta_{n-1}(t_{n-2}) + ... \theta_1(t_1)-\theta_1(t_0) =\\ \theta_n(t_n) - \theta_1(t_0) = \theta(1)-\theta(0) = 2\pi i m$ 

 This tells us that moving along this closed path is equivalent to moving along a circular path
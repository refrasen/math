$a_1x + b_1 y = c_1 \\ a_2 x + b_2 y = c_2$

and $\frac{a_1}{a_2} = \frac{b_1}{b_2}$$\equiv \frac{a_1}{b_1}= \frac{a_2}{b_2}$ 

so $a_1x_1 + b_1 y_1 = c_1 \\ a_2 x_1 + b_2 y_1 = c_2$

and $c_1 = c_2$, since these lines meet at $(x_1, y_1)$

---

let $L_1$ be $y = m_1 x + b_1$ and $L_2$ be $y = m_2 x + b_2$ distinct

we have $(x_1, y_1)$ $(x_2, y_2)$ be two distinct points where they meet

$y_1 = m_1x_1 + b_1 \\ y_1 = m_2x_1 + b_2$ and $y_2 = m_1x_2 + b_1 \\ y_2 = m_2x_2 + b_2$ 

so $m_1x_1 + b_1 = m_2x_1 + b_2$

$\implies b_1 = m_2x_1 + b_2 - m_1x_1$ $(i)$ 

and $m_1x_2 + b_1 = m_2x_2 + b_2$ 

combining the two by substituting $b_1$ in the second equation with $(i)$ $\implies$

$m_1x_2 + m_2x_1 + b_2 - m_1x_1 = m_2x_2 + b_2$

$\equiv$

$m_1x_2 + m_2x_1 - m_1x_1 = m_2x_2$

$m_1(x_2 - x_1) = m_2(x_2 - x_1)$

if $x_2 \neq x_1$ 

so $m_1 = m_2$ $\implies b_1 = b_2$ 

Contradicts that they are two distinct lines

if $x_2 = x_1$, we have instead from the first line:

$y_1 = m_1x_1 + b_1$

and $y_2 = m_1x_2 + b_1 = m_1x_1 + b_1$

implying $y_1 = y_2$

contradicting distinct points


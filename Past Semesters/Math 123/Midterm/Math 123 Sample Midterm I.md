# Math 123 Sample Midterm I

1. Consider the differential equation $x' = x - 2\sin t$
   1. Find the general solution
      1. Homogeneous solution
         1. $x' = x$
         2. $x_h(t) = \alpha e^t$
      2. Particular solution
         1. $\beta \sin t + \gamma \cos t$
         2. $\beta \cos t -\gamma \sin t = \beta \sin t + \gamma \cos t - 2 \sin t$
         3. $\beta = \gamma$
         4. $-\gamma = \beta - 2$
         5. $\beta - \gamma = 0$
         6. $\beta + \beta - 2 = 0$
         7. $\beta = 1$
         8. $\gamma = 1$
         9. $x_p(t) = \sin t +  \cos t$
         10. $x(t) = \alpha e^t + \sin t + \cos t$
         11. $x'(t) = \alpha e^t + \cos t - \sin t$
      3. Flow:
         1. $x(0) = \alpha + 1$, so $\alpha = x_0 -1$
         2. $\phi(x_0, t) = (x_0 -1)e^t + \cos t + \sin t$ 
   2. Show that there is a unique periodic solution
      1. $F(x,t) = x - 2\sin t$ is periodic in $t, T = 2\pi$ 
         1. $F(x, t+2\pi) = F(x, t)$ 
         2. suppose $x_1(t)$ is the solution defined for $0 \leq t \leq 2\pi$ satisfying $x_1(0) = x_0$
         3. Suppose $x_2(t)$is the solution satisfying $x_2(0) = x_1(2\pi)$
         4. Then we may extend the solution $x_1$ by defining $x_1(t+2\pi) = x_2(t)$, for $t \in [0, 2\pi]$ 
         5. The extended function is a solution:
         6. $x_1'(t+2\pi)  = x_{2}'(t) = F(x_2(t), t) = F(x_1(t+2\pi), t+2\pi)$ 
      2. There exists a periodic solution if there is a fixed point for the Poincare map?
      3. $P(x_0) = x_1$ etc...
      4. so $\exists$ $x$ s.t. $P(x) = x$ 
      5. But what time interval?
      6. so $x(t+ T) = \alpha e^{t+T} + \cos (t+T) + \sin(t+T) = \alpha e^t + \cos(t) + \sin(t) $
      7. Take $\alpha = 0$
      8. We can look at $P'(x_0)$
      9. $P(x_0) = \phi(2\pi, x_0) = (x_0 -1)e^{2\pi} + 1$
      10. When does $P(x_0) = x_0$
      11. $x_0e^{2\pi} - e^{2\pi} + 1 = x_0$
      12. When $(x_0-1)(e^{2\pi}-1) = 0$
      13. which happens when $x_0 = 1$
      14. so $\phi(1, t) = \cos t + \sin t$ is in fact a periodic solution with period $2\pi$
      15. Is this a unique solution?
      16. $\frac{dP}{dx_0} = \frac{d\phi(2\pi, x_0)}{dx_0} = e^{2\pi}$ 
          1. increases monotonically
      17. $\frac{d^2P}{dx_0^2} = 0$
          1. and the slope doesn't change
      18. i mean, obviously, $P(x_0)$ is a line with slope $e^{2\pi}$ 
          1. only crosses $P(x_0) = x_0$ once
      19. Suppose $P(x_0)
2. Consider the planar system of differential equations in polar coordinates: $r' = r(r-1) \\ \theta' = -1$
   1. Sketch the phase portrait
      1. so first of all, equilibrium solutions?
         1. only when $r = 0$, since $\theta' \neq 0$ 
      2. Invariant solutions:
         1. $r = 1$: when $r = 1$, $r' = 0$, and so we remain in the unit circle
         2. moving clockwise
      3. when $r > 1$, $r' > 0$, so solutions spiral out clockwise if they start outside the unit circle
      4. When $r < 1$, $r' < 0$, which means solutions that start inside the unit circle spiral in to equilibrium clockwise
3. Let $A = \begin{pmatrix} -3 & 10 \\ -1 & 3 \end{pmatrix}$ 
   1. Find the eigenvalues and eigenvectors
      1. $A - \lambda I = \begin{pmatrix} -3 -\lambda & 10 \\ -1 & 3 - \lambda \end{pmatrix}$ 
      2. $(a - \lambda)(d-\lambda) - bc$
         1. $\lambda^2 - (a+d)\lambda + ad - bc$
         2. so it is $\frac{T \pm \sqrt{T^2 - 4D}}{2}$ 
      3. $(-3 - \lambda)(3-\lambda) + 10$
      4. $\lambda^2 -9 + 10$ 
      5. Eigenvalues are $\frac{\pm \sqrt{0 - 4(1)}}{2} = \frac{\pm 2i}{2} = \pm i$ 
      6. $A - iI = \begin{pmatrix} -3 - i & 10 \\ -1 & 3 -i\end{pmatrix}$ 
      7. $(-3-i )x+ 10y = 0$ 
      8. $x = (-3 + i)$ $\implies$ $y = -1$ 
      9. $(-3 - i)(-3 + i) + 10 y = 0 \equiv 10 + 10y = 0$ 
      10. $V_1 = \begin{pmatrix} -3 + i \\ -1\end{pmatrix}$, so $V_1 = X_1 + iY_1$, $X_1 = (-3, -1), Y_2 = (1, 0)$ 
      11. $\overline{V_1} = X_1 - iY_1 = \begin{pmatrix} -3 -i \\ -1\end{pmatrix} = V_2$, eigenvalue corresponding to $-i$
   2. Find a matrix $T$ that puts $A$ in canonical form
      1. $T = \begin{pmatrix} -3 & 1 \\ -1 & 0\end{pmatrix}$ 
      2. $A(X_1 + iY_1) = AX_1 + iAY_1 = iX_1 - Y_1$
      3. $A(X_1 + iY_1) = AX_1 + iAY_1 = (\alpha + i\beta)X_1 + (\alpha i -\beta)Y_1 $
         1. $= (\alpha X_1 - \beta Y_1) + i(\alpha Y_1 + \beta X_1)$
      4. and $AT = [AX_1 | AY_1] = [-Y_1 | X_1]$
      5. $T^{-1}AT = $ $\begin{pmatrix} 0 & 1 \\ -1 & 0\end{pmatrix}$
         1. since $T^{-1}T = [T^{-1}X_1 | T^{-1}Y_1 ] = [E_1 | E_2]$ 
   3. Find the general solution of both $X' = AX$, $Y' = BY$, where $B = T^{-1}AT$ 
      1. $Y' = \begin{pmatrix}0 & 1 \\ -1 & 0 \end{pmatrix}Y$
         1. $Y(t) = e^{it}\begin{pmatrix} 1 \\ i\end{pmatrix}$
            1. $Y_R = \begin{pmatrix} \cos t \\ -\sin t \end{pmatrix}$ 
            2. $Y_I = \begin{pmatrix}  \sin t \\ \cos t \end{pmatrix}$ 
         2. $Y' = Y_R' + iY_I ' = AY_R + i AY_i $ 
         3. Solution: $Y(t) = \begin{pmatrix} \cos t & \sin t \\ -\sin t & \cos t\end{pmatrix}\begin{pmatrix} x_0 \\ y_0 \end{pmatrix}$ 
      2. Finding a solution for $X' = AX$ means
         1. $X(t) = TY$
         2. since $X'(t) = TY' = TBY = TT^{-1}ATY = A(TY) = AX$ 
      3. so $TY(t) = \begin{pmatrix} -3 & 1 \\ -1 & 0 \end{pmatrix}\begin{pmatrix} \cos t & \sin t \\ -\sin t & \cos t \end{pmatrix} \begin{pmatrix} x_ 0 \\ y_0 \end{pmatrix}$ 
      4. $= \begin{pmatrix} -3\cos t - \sin t & -3 \sin t + \cos t \\ -\cos t & -\sin t\end{pmatrix}\begin{pmatrix} x_0 \\ y_0\end{pmatrix}$ 
      5. derivative matrix is $\begin{pmatrix} 3\sin t - \cos t  & -3\cos t - \sin t \\ \sin t & - \cos t\end{pmatrix}$ 
   4. Sketch the phase portraits of both systems
      1. for $Y' = BY$ 
         1. we should have a center going clockwise
      2. for $X' = AX$
         1.  Make the line of $(-3, -1)$ the $x-axis$
         2. make the $x$-axis, the new $y$-axis 
4. Give an example of aplanar system $X' = AX$ with the following properties
   1. The $y$-axis consists entirely of equilibria
   2. so we want
   3. $A = \begin{pmatrix} -1 & 0 \\ 0 &0 \end{pmatrix}$ 
   4. $X(t) = e^{-t} \begin{pmatrix} x_0 \\ 0\end{pmatrix} + \begin{pmatrix} 0 \\ y_0\end{pmatrix}$ 
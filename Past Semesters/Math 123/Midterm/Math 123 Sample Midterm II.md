# Math 123 Sample Midterm II

1. Problem 1
   1. Compute the flow of the system $x' = x - 4y^3 \\ y' = -y$ 
      1. $y = y_0 e^{-t}$
      2. $x' = x - 4y_0^3e^{-3t}$ 
      3. $x_h(t) = \alpha e^{t}$ 
      4. $x_p(t) = \beta e^{-3t}$ (Guessing)
      5. $x'_p(t) = -3\beta e^{-3t} = \beta e^{-3t} -4y_0^3 e^{-3t}$
      6. $\equiv$ $-3\beta = \beta - 4y_0^3$
      7. $-4\beta = -4y_0^3$
      8. $\beta = y_0^3$ 
      9. $x(t) = \alpha e^t + y_0^3 e^{-3t}$
      10. $x(0) = \alpha + y_0^3 = x_0$ 
      11. $\alpha = x_0 -y_0^3$ 
      12. $x(t) = (x_0 - y_0^3)e^t + y_0^3 e^{-3t}$ 
      13. $\phi_t(X_0) = \begin{pmatrix} (x_0 - y_0^3)e^t + y_0^3 e^{-3t} \\y_0e^{-t}\end{pmatrix} $
   2. Describe the stable and unstable curves associated with the equilibrium at the origin
      1. The only equilibrium is origin
      2. Consider the curve $x - y^3 = 0$, or $x = y^3$ 
      3. if we have a solution starting on this curve we obtain
      4. $\phi_t(X_0) = \begin{pmatrix} y_0^3 e^{-3t} \\  y_0e^{-t}\end{pmatrix}$
      5. which satisfies $x(t) = y(t)^3$ 
      6. and as $t \rightarrow \infin$, $\phi_t(X_0) \rightarrow 0$, so $W^s(0)$ is the curve $x = y^3$ 
      7. and if we let $y_0 = 0$, i.e, start on the $x-$axis
         1. all solutions stay on the $x-$axis and tend towards origin when $ t\rightarrow - \infin$ 
      8. for general $x_0, y_0$
         1. $y(t) \rightarrow 0$ as $t \rightarrow \infin$
         2. $y(t) \rightarrow \pm \infin$ as $t \rightarrow -\infin$
         3. that is, if $y_0 \neq 0$ 
         4. as long as $x_0 \neq y_0^3$, $x(t) \rightarrow \pm \infin$ as $t \rightarrow \infin$ 
            1. and if $y_0 \neq 0$, $x(t) \rightarrow \pm \infin$ as $t \rightarrow - \infin$ 
         5. so for $x_0, y_0$ not on the curves described above, we don't have stable or unstable curves
2. Consider the planar linear system $X' = AX,$ where $A = \begin{pmatrix} -4 & -2 \\ -1 &  -3 \end{pmatrix}$ 
   1. Find the eigenvalues and eigenvectors of $A$ 
      1. eigenvalues are equal to $\frac{T \pm \sqrt{T^2 - 4D}}{2}$ 
      2. $T = -7$, $D = (-4)(-3) - (-2)(-1) = 12 - 2 = 10$
      3. $T^2 - 4D = 49 - 40 = 9$
      4. Eigenvalues are then $\frac{-7 \pm 3}{2}$
         1. so -10/2 or -4/2
         2. -5 or -2
      5. $A + 5I  = \begin{pmatrix} 1 & -2 \\ -1 & 2 \end{pmatrix}$, so $V_1 = (2, 1)$  
      6. $A + 2I = \begin{pmatrix} -2 & -2 \\ -1 & -1\end{pmatrix}$, $V_2 = (1, -1)$  
   2. Find a matrix $T$ that puts $A$ into canonical form. Write down the canonical form $B$ of $A$
      1. $T = \begin{pmatrix}2 & 1 \\ 1 & -1 \end{pmatrix}$ 
         1. $T^{-1}AT = T^{-1}[-5V_1| -2 V_2] = [-5E_1 | -2 E_2]$ 
      2. $B = \begin{pmatrix} -5 & 0 \\ 0 & -2 \end{pmatrix}$ 
   3. Find the general solution of both $X' = AX$ and $Y' = BY$ 
      1. $Y' = BY$
         1. $Y(t) = e^{-5t}(x_0,0) + e^{-2t}(0,y_0)$ 
      2. and $X(t) = TY$ :
         1. $X(t) = e^{-5t}\begin{pmatrix} 2x_0 \\ x_0\end{pmatrix} + e^{-2t}\begin{pmatrix} y_0 \\ -y_0\end{pmatrix}$
         2. where the initial value is $(2x_0 + y_0, x_0 - y_0)$ 
         3. $X(t) = C_1e^{-5t}(2, 1) + C_2e^{-2t}(1, -1)$
         4. and $2C_1 + C_2 = x_0$, $C_1 - C_2 = y_0$
            1. $3C_1 = x_0 + y_0$, $C_1 = (x_0/3 + y_0/3)$ 
            2. $C_2 = (x_0/3  - 2y_0/3)$ 
            3. plugging in the new intial value
            4. $C_2 = $$2x_0/3 + y_0/3 - 2x_0/3 + 2y_0/3 = y_0$ 
            5. $C_1 = 2x_0/3 + y_0/3 + x_0/3 - y_0/3 = x_0$ 
   4. Sketch the phase portrait of $X' = AX$. What type of phase portrait is it?
      1. So along the lines $(2,1)$ and $(-1, 1)$, solutions tend towards origin
      2. $-2$ is the "weak" eigenvalue, so as $ t\rightarrow \infin$, the solutions look more like $e^{-2t}(y_0, -y_0)$ 
      3. $\frac{dy}{dx} = \frac{-10x_0e^{-5t} -2y_0e^{-2t}}{-5x_0e^{-5t}+2y_0e^{-2t}}$ 
      4. multiplying top and bottom by $e^{2t}$
         1. $\frac{-10x_0e^{-3t}-2y_0}{-5x_0e^{-3t}+2y_0}$ so as $ t\rightarrow \infin$, we have $\frac{dy}{dx} = -1$, which is indeed the slope of the eigenvector corresponding to $-2$ 
         2. as long as $y_0 \neq 0$
         3. or i mean, in general if we have
         4. $\alpha e^{-5t}V_1 + \beta e^{-2t}V_2$
            1. then if $\beta \neq 0$ we have solutions tending towards origin tangentially to $V_2$ 
      5. Sink
3. Problem 3??????????????????????
   1. Show that if $X(t)$ is a solution to a linear system $X' = AX$, then so is $Y(t) = X(t+a)$ for any real number $a$ 
      1. Want to show $Y'(t) = AY(t)$ 
      2. $Y'(t) = X'(t+a) = AX(t+a) = AY(t+a)$ 
      3. because taking $f(t) = X(t)$ and $g(t) = t+a$
         1. $(f\circ g)'(t) = f'(g(t))g'(t) = AX(t+a)$ since $g'(t) = 1$ 
   2. Suppose that $X_1(t)$ and $X_2(t)$ are both solutions to $X' = AX$ If $X_1(t_1) = X_2(t_2)$, for some real numbers $t_1, t_2$, what is the relation between $X_1$ and $X_2$? in particular, what can be said about the solution curves defined by $X_1$ and $X_2$
      1. they lie on the same curve
      2. $X_1(t) = X_2(t + t_2 - t_1)$ 
      3. This is because of unique solutions for $X' = AX$?
      4. If we were to say $Y(t)$ is the solution to $X' = AX$ such that $Y(0) = X_2(t_2)$ 
         1. we'd have $X_1(t_1) = Y_0 = X_2(t_2)$ 
      5. $Y(t) = X_2(t_2 + t)?$ is this true?
         1. Well, $Y'(t) = AY(t) = X'_2(t_2 + t) = AX_2(t_2 + t)$
      6. If $X_1$ and $X_2$ were different curves:
         1. Then with $Y_0 = X_1(t_1) = X_2(t_2)$ 
         2. we should be able to say there are two curves that start at $Y_0$ 
         3. basically any two curves starting at $X_1(t_1), X_2(t_2)$ are the same...
         4. and $\phi_s(\phi_t(X_0)) = \phi_{s+t}(X_0)$
         5. so let $X_{1,0}$ be the initial conditoin for $X_1(t)$ and $X_2(t)$ have initial condition $X_{2, 0}$ 
         6. we should have
         7. $\phi_{t_1}(X_{1,0}) = \phi_{t_2}(X_2, 0)$
         8. $\phi_{t - t_1}(\phi_{t_2}(X_{2, 0})) = \phi_{t+t_2 - t_1}(X_{2, 0})$
         9. $ = \phi_{t-t_1}(\phi_{t_1}(X_{1,0})) = \phi_{t}(X_{1,0})$ 
         10. See chapter 7 for more details
      7. $X' = AX, X(0) = X_0$, solution is $X(t) = \exp(tA)X_0$ 
      8. and $F(X) = AX$ is smooth
4. Given are linear systems $X' = A_kX$ $(k = 1, . . ., 4)$ where $A_1 = \begin{pmatrix} 1 & -1 \\ -1 & 1 \end{pmatrix}$, $A_2 = \begin{pmatrix} -3 & 1 \\ 1 & -2 \end{pmatrix}$, $A_3 = \begin{pmatrix} 1 & -3 \\ -1 & 3 \end{pmatrix}$, $A_4 = \begin{pmatrix} -2 & -1 \\ 2 & -3 \end{pmatrix}$ Determine which systems are topologically conjugate to each other and explain why
   1. We know that in hyperbolic systems
      1. systems that have both negative real parts are conjugate
      2. systems that have both positive real parts are conjugate
      3. and there's only one case where we have one negative, one positive real: a saddle and those are conjugate to each other
   2. Hyperbolic systems (HW)
      1. Centers are conjugate to each other if they have the same eigenvalues
      2. One zero eigenvalue
         1. other is negative
         2. other is positive
      3. two zero eigenvalues
         1. two linearly independent eigenvector
         2. one linearly independent eigenvector
   3. $A_1$, $T = 2$, $D = 1 - 1 = 0$
      1. Eigenvalues: $\frac{2 \pm 2}{2} = 1 \pm 1$ 
      2. $2, 0$ 
   4. $A_2$, $T = -5$, $D = 6 - 1 = 5$
      1. Eigenvalues: $\frac{-5 \pm \sqrt{25 - 20}}{2} = \frac{-5 \pm \sqrt{5}}2$
      2. two negative eigenvalues
   5. $A_3$, $T = 4$, $D = 3 - 3 = 0$ 
      1. Eigenvalues: $\frac{4 \pm 4}{2} = 2 \pm 2$
      2. $4, 0$
   6. $A_4$, $T = -5$, $D = 6 +2 = 8$
      1. $\frac{-5 \pm \sqrt{25 - 32}}{2}$ 
      2. Still a sink
   7. So $A_1, A_3$ 
   8. $A_2, A_4​$ 
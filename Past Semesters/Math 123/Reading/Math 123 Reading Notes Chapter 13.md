# Math 123 Reading Notes Chapter 13

## 13 Applications in Mechanics

### 13.1 Newton's Second Law

#### Notation

##### force field F:

$F$: a vector field on the (configuration) space of the particle, which in our case is $\R^n$ 

$F(X)$ is the force exerted on a particle located at position $X$

---

Connection between physical concept of force field and mathematical concept of diff. eq.: Newton's second law: $F = ma$ 

$mX'' = F(X)$ 

---

As a system:

$X' = V \\ V' = \frac{1}{m}F(X)$   

where $V = V(t)$ is the velocity of the particle

This is a system on $\R^n \times \R^n$ 

often called a *mechanical system with n degrees of freedom* 

---

A solution $X(t) \sub \R^n$ of the second-order equation is said to lie in *configuration space*

The solution $(X(t), V(t))$ $\sub \R^n \times \R^n$ lies in the *phase space* or *state space* 

---

#### Examples

##### Example: Undamped Oscillator 

mass moves in one dimension 

$x(t)$: position at time $t$, $x: \R \rightarrow \R$ 

$mx'' = -kx$ is the diff. eqn. governing this motion

$k > 0$

Force field at point $x \in \R$ is thus $-kx$ 

##### Example: two dimensional harmonic oscillator

$X(t) = (x_1(t), x_2(t))$ $\in \R^2$

The force field is $F(X) = -kX$, with $mX'' = -kX$  

with solutions in configuration space

$x_1(t) = c_1\cos(\sqrt{k/m}t) + c_2 \sin(\sqrt{k/m}t)$

$x_2(t) = c_3 \cos(\sqrt{k/m}t) + c_4 \sin (\sqrt{k/m}t)$ 

for some choices of $c_j$, use chapter 6 methods to check this

---

#### Concepts from Multivariable Calculus

##### dot product (or inner product) of two vectors

$X, Y \in \R^n$, denoted: $X\cdot Y$ 

Defined by: $X\cdot Y = \sum_{i =1}^n x_iy_i$ 


# Math 123 Cheat Sheet

Find $y$ in terms of $x$ by $\frac{dy}{dx}$ and integrating

Change of variables: $u = f(x,y), v = g(x,y)$ 

$u'(t) = x'(t)\frac{\partial f}{\partial x} + y'(t)\frac{\partial f}{\partial y}$ 

$(DF_X )_{ij}= (\frac{\partial f_i}{\partial j})$, $F = (f_1(x_1, . . ., x_n), . . ., f_n(x_1, . . ., x_n))$ 

**Stable Curve theorem:**

$x' = \lambda x + f_1 \\ y' = -\mu y + f_2​$ , $f_j/r \rightarrow 0​$, there is an $\epsilon > 0​$ defined for $|y| < \epsilon​$, satisfies $h^s(0) = 0​$ 

Curve is invariant $t \geq 0$, tends towards origin, passes through origin tangent to $y-axis$, all other solutions off the curve in the disk of radius $\epsilon$ leave the disk eventually.

$x = h^s(y)$ is the local stable curve at 0, can find complete stable curve by going backwards in time. 

A similar unstable curve theorem: $y = h^7(x)$, tangent to $x$-axis at origin, all solutions tend to origin as $t \rightarrow - \infin$ 

**Stability** 

Suppose $X^* \in \R^n$ is an equilibrium point for the diff. eqn. $X' = F(X)$ 

Then $X^*$ is a stable equilibrium point *if* 

$\forall$ neighborhood $\mathcal{O}$ of $X^*$ in $\R^n$, there is a neighborhood $\mathcal{O_1}$ of $X^*$ in $\mathcal{O}$ such that every solution $X(t)$ with $X(0) = X_0 \in \mathcal{O_1}$ is defined and remains in $\mathcal{O}$ for all $t > 0$ 

#### asymptotic stability

If $\mathcal{O_1}$ can be chosen above so that we have $\lim_{t\rightarrow \infin} X(t) = X^*$, then we say that $X^*$ is asymptotically stable

$X^*$ is an equilibrium that isn't stable $\equiv$ $X^*$ is an unstable equilibrium

Sinks are asymptotically stable, sources and saddles are unstable.

**The Existence and Uniqueness Theorem**

Consider the IVP:

$X' = F(X)$,    $X(0) = X_0$ 

Suppose $F: \R^n \rightarrow \R^n$   is $C^1$ 

There exists a unique solution of this IVP

More preciesely: There exists $a > 0$ and a unique solution

$X:(-a,a) \rightarrow \R^n$ 

of this diff. eqn. satisfying the initial condition

Space derivative $D_{\phi_t}(X)$ of the map $\phi_t$ at $X$

$\frac{d}{dt}(D\phi_t(X_0)) = DF_{\phi_t(X_0)}D\phi_t(X_0)$ 

norm: $|DF_X| = \sup_{|U| = 1}|DF_X(U)|$, $|DF_X(V)| \leq |DF_X||V|$ 

Lipschitz constant $K$: $|F(Y)-F(X)| \leq K|Y-X|$ 

A $C^1$ function from open set to $\R^n$ is locally Lipschitz

$F$ is locally Lipschitz if each point in $\mathcal{O}$ has a neighborhood $\mathcal{O'}$ in $\mathcal{O}$ such that 

the restriction $F$ to $\mathcal{O'}$ is Lipschitz

the Lipschitz constant of $F|\mathcal{O'}$ may vary with each neighborhood $\mathcal{O'}$ 

If $\mathcal{O}$ is convex and if $|DF_X| \leq K$ for all $X \in \mathcal{O}$ then $K$ is a Lipschitz constant for $F|\mathcal{O}$  

$u_0 = x_0$, $u_k = x_0 + \int_0^t F(u_{k-1}(s))ds$ 

Variational equation along solution $X(t)$: $A(t) = DF_{X(t)}$, $U' = A(t)U$  

$Y(t), Y(0) = X_0 + U_0$, $\lim_{U_0 \rightarrow 0}\frac{|Y(t)-[X(t)+U(t)] }{|U_0|}$ converges to 0 uniformly for $t \in [\alpha, \beta]$ 

$X' = F(X)$ is diff. eqn. system. $F$ is $C^1$, $X(t)$ is solution, $X(0) = X_0$, $t \in [\alpha, \beta]$ 

$U(t, U_0)$ is solution to variational eqn. along $X(t)$ satisfy $U(0, U_0) = U_0$ $\implies D_{\phi_t}(X_0)U_0 = U(t, U_0)$ 

$B = T^{-1}AT$, $\exp(B) = T^{-1}\exp(A)T$

$AB = BA, \exp(A+B) = \exp(A)\exp(B)$

$\exp(-A) = (\exp(A))^{-1}$ 

if $AV = \lambda V$, $\exp(A)V = e^\lambda V$ 

$\frac{d}{dt}\exp(tA)= A\exp(tA)= \exp(tA)A$ 




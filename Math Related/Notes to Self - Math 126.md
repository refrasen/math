Helpful notes to self:

* Really look over multivariable calculus: chapter 15, 16 are good to look at, anything relating to integrals really helps. 
  * Average values
  * parametric functions
  * spherical coordinates
  * normal vector calculation
* Understand that smoothness may be used to impose a condition on functions
* Understand what "sources" mean with PDEs, example: heat sources for heat equation means adding the source to the right side: $k\triangle u$ and for force... I suppose it's the same... So how do we know when something becomes inhomogeneous or how a "source term" affects the equation. 
* Know what you're trying to solve for: example, solving for $u(x,t)$ 
* Don't be dumb, know when you are only dealing with simple cases. 
  * example, on the boundary of a sphere, we can let $r = \text{radius}$ constant
  * Example: $u_{xx} = 0$, when we negate all other independent variables, is treated like integrating a function of one variable. 
* 
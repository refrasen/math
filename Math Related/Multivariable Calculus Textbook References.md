# Multivariable Calculus Textbook References

Page 1129: **Flux of $\bold F$ aross S** (Definition)

$\bold F$: continuous vector field defined on an oriented surface $S$ with normal vector $\bold n$ 

Surface integral of $\bold F$ over $S$ is
$$
\int \int_S \bold F \cdot d\bold S = \int \int_S \bold F \cdot \bold n \; dS
$$
Because from page 1127: the unit normal vector to a surface $S$: $\bold n = \frac{\bold r_u \times \bold r_v}{\bold r_u \times \bold r_v}$, where $\bold r(u,v)$ is a vector function "tracing out" the surface $S$ / $S$ is parameterized by $\bold r$ 

and from page 1122: $\bold r(u,v) = [x(u,v), y(u,v), z(u,v)]$ 

---

### Topic: Triple Integrals in Spherical Coordinates

page 1046 ish
$$
x = r\sin\phi \cos \theta \qquad y = r \sin \phi \sin\theta \qquad z = r\cos \phi 
$$
$r^2 = x^2 + y^2 + z^2$ 

where $r, \theta$ is exactly as defined before, and $\phi$ is the angle between the positive $z$axis and the line segment from origin $O$ to point $P$, $OP$ 

and
$$
\int\int\int_E f(x,y,z)\; dV 
\\= \int _c^d \int_\alpha ^\beta\int_a^b f(r\sin\phi \cos\theta, r\sin\phi \sin \theta, r\cos\phi) r^2 \sin \phi \; dr\, d\theta \, d\phi
$$

---

$| \sum_{k=1}^n \sum_{j =1}^m f(\overline x_i, \overline y_j) dA|$ $\leq \sum_{k=1}^n |\sum_{j=1}^m f(\overline x_i, \overline y_j)||dA| \leq \sum_{k=1}^n \sum_{j=1}^m |f||dA|​$  